#ifndef ADRPARSE_H
#define ADRPARSE_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <boost/smart_ptr/intrusive_ptr.hpp>
#include <limits>
#include <libxml++/libxml++.h>

#include "adr.hh"
#include "adrdb.hh"
#include "adraerodrome.hh"
#include "adrunit.hh"
#include "adrpoint.hh"
#include "adrroute.hh"
#include "adrairspace.hh"
#include "adrrestriction.hh"

namespace ADR {

class ParseXML : public xmlpp::SaxParser {
public:
	ParseXML(Database& db, bool verbose = true);
	virtual ~ParseXML();

	void error(const std::string& text);
	void error(const std::string& child, const std::string& text);
	void warning(const std::string& text);
	void warning(const std::string& child, const std::string& text);
	void info(const std::string& text);
	void info(const std::string& child, const std::string& text);

	unsigned int get_errorcnt(void) const { return m_errorcnt; }
	unsigned int get_warncnt(void) const { return m_warncnt; }

	Database& get_db(void) { return m_db; }

protected:
	class NodeIgnore;
	class NodeText;
	class NodeLink;
	class NodeAIXMElementBase;
	class NodeAIXMExtensionBase;
	class NodeAIXMAvailabilityBase;
	class NodeADRWeekDayPattern;
	class NodeADRSpecialDayPattern;
	class NodeADRTimePatternCombination;
	class NodeADRTimeTable1;
	class NodeADRTimeTable;
	class NodeADRLevels;
	class NodeADRPropertiesWithScheduleExtension;
	class NodeADRDefaultApplicability;
	class NodeADRAirspaceExtension;
	class NodeADRNonDefaultApplicability;
	class NodeADRRouteAvailabilityExtension;
	class NodeADRProcedureAvailabilityExtension;
	class NodeADRRouteSegmentExtension;
	class NodeADRIntermediateSignificantPoint;
	class NodeADRIntermediatePoint;
	class NodeADRRoutePortionExtension;
	class NodeADRStandardInstrumentDepartureExtension;
	class NodeADRStandardInstrumentArrivalExtension;
	class NodeAIXMExtension;
	class NodeAIXMAirspaceActivation;
	class NodeAIXMActivation;
	class NodeAIXMElevatedPoint;
	class NodeAIXMPoint;
	class NodeAIXMARP;
	class NodeAIXMLocation;
	class NodeAIXMCity;
	class NodeAIXMServedCity;
	class NodeGMLValidTime;
	class NodeAIXMFeatureLifetime;
	class NodeGMLPosition;
	class NodeGMLBeginPosition;
	class NodeGMLEndPosition;
	class NodeGMLTimePeriod;
	class NodeGMLLinearRing;
	class NodeGMLInterior;
	class NodeGMLExterior;
	class NodeGMLPolygonPatch;
	class NodeGMLPatches;
	class NodeAIXMSurface;
	class NodeAIXMHorizontalProjection;
	class NodeAIXMAirspaceVolumeDependency;
	class NodeAIXMContributorAirspace;
	class NodeAIXMAirspaceVolume;
	class NodeAIXMTheAirspaceVolume;
	class NodeAIXMAirspaceGeometryComponent;
	class NodeAIXMGeometryComponent;
	class NodeAIXMEnRouteSegmentPoint;
	class NodeAIXMTerminalSegmentPoint;
	class NodeAIXMStart;
	class NodeAIXMEnd;
	class NodeAIXMStartPoint;
	class NodeAIXMEndPoint;
	class NodeADRConnectingPoint;
	class NodeADRInitialApproachFix;
	class NodeAIXMAirspaceLayer;
	class NodeAIXMLevels;
	class NodeAIXMRouteAvailability;
	class NodeAIXMProcedureAvailability;
	class NodeAIXMAvailability;
	class NodeAIXMClientRoute;
	class NodeAIXMRoutePortion;
	class NodeAIXMDirectFlightClass;
	class NodeAIXMFlightConditionCircumstance;
	class NodeAIXMOperationalCondition;
	class NodeAIXMFlightRestrictionLevel;
	class NodeAIXMFlightLevel;
	class NodeADRFlightConditionCombinationExtension;
	class NodeAIXMDirectFlightSegment;
	class NodeAIXMElementRoutePortionElement;
	class NodeAIXMElementDirectFlightElement;
	class NodeAIXMFlightRoutingElement;
	class NodeAIXMRouteElement;
	class NodeAIXMFlightRestrictionRoute;
	class NodeAIXMRegulatedRoute;
	class NodeAIXMAircraftCharacteristic;
	class NodeAIXMFlightConditionAircraft;
	class NodeAIXMFlightCharacteristic;
	class NodeAIXMFlightConditionFlight;
	class NodeAIXMFlightConditionOperand;
	class NodeAIXMFlightConditionRoutePortionCondition;
	class NodeAIXMFlightConditionDirectFlightCondition;
	class NodeADRAirspaceBorderCrossingObject;
	class NodeADRBorderCrossingCondition;
	class NodeADRFlightConditionElementExtension;
	class NodeAIXMFlightConditionElement;
	class NodeAIXMElement;
	class NodeAIXMFlightConditionCombination;
	class NodeAIXMFlight;
	class NodeAIXMAnyTimeSlice;
	class NodeAIXMAirportHeliportTimeSlice;
	class NodeAIXMAirportHeliportCollocationTimeSlice;
	class NodeAIXMDesignatedPointTimeSlice;
	class NodeAIXMNavaidTimeSlice;
	class NodeAIXMAngleIndicationTimeSlice;
	class NodeAIXMDistanceIndicationTimeSlice;
	class NodeAIXMAirspaceTimeSlice;
	class NodeAIXMStandardLevelTableTimeSlice;
	class NodeAIXMStandardLevelColumnTimeSlice;
	class NodeAIXMRouteSegmentTimeSlice;
	class NodeAIXMRouteTimeSlice;
	class NodeAIXMStandardInstrumentDepartureTimeSlice;
	class NodeAIXMStandardInstrumentArrivalTimeSlice;
	class NodeAIXMDepartureLegTimeSlice;
	class NodeAIXMArrivalLegTimeSlice;
	class NodeAIXMOrganisationAuthorityTimeSlice;
	class NodeAIXMSpecialDateTimeSlice;
	class NodeAIXMUnitTimeSlice;
	class NodeAIXMAirTrafficManagementServiceTimeSlice;
	class NodeAIXMFlightRestrictionTimeSlice;
	class NodeAIXMTimeSlice;
	class NodeAIXMObject;
	class NodeAIXMAirportHeliport;
	class NodeAIXMAirportHeliportCollocation;
	class NodeAIXMDesignatedPoint;
	class NodeAIXMNavaid;
	class NodeAIXMAngleIndication;
	class NodeAIXMDistanceIndication;
	class NodeAIXMAirspace;
	class NodeAIXMStandardLevelTable;
	class NodeAIXMStandardLevelColumn;
	class NodeAIXMRouteSegment;
	class NodeAIXMRoute;
	class NodeAIXMStandardInstrumentArrival;
	class NodeAIXMStandardInstrumentDeparture;
	class NodeAIXMDepartureLeg;
	class NodeAIXMArrivalLeg;
	class NodeAIXMOrganisationAuthority;
	class NodeAIXMSpecialDate;
	class NodeAIXMUnit;
	class NodeAIXMAirTrafficManagementService;
	class NodeAIXMFlightRestriction;
	class NodeADRMessageMember;

	class Node {
	public:
		typedef boost::intrusive_ptr<Node> ptr_t;
		typedef boost::intrusive_ptr<const Node> const_ptr_t;

		Node(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		virtual ~Node();

		unsigned int breference(void) const { return ++m_refcount; }
		unsigned int bunreference(void) const { return --m_refcount; }
		friend inline void intrusive_ptr_add_ref(const Node* expr) { expr->breference(); }
		friend inline void intrusive_ptr_release(const Node* expr) { if (!expr->bunreference()) delete expr; }

		virtual void on_characters(const Glib::ustring& characters);
		virtual void on_subelement(const ptr_t& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeIgnore>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeText>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeLink>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeADRWeekDayPattern>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeADRSpecialDayPattern>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeADRTimePatternCombination>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeADRTimeTable1>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeADRTimeTable>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeADRLevels>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeADRDefaultApplicability>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeADRNonDefaultApplicability>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeADRIntermediateSignificantPoint>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeADRIntermediatePoint>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMExtensionBase>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMExtension>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMAirspaceActivation>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMActivation>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMElevatedPoint>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMPoint>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMARP>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMLocation>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMCity>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMServedCity>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeGMLValidTime>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMFeatureLifetime>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeGMLBeginPosition>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeGMLEndPosition>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeGMLTimePeriod>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeGMLLinearRing>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeGMLInterior>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeGMLExterior>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeGMLPolygonPatch>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeGMLPatches>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMSurface>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMHorizontalProjection>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMAirspaceVolumeDependency>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMContributorAirspace>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMAirspaceVolume>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMTheAirspaceVolume>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMAirspaceGeometryComponent>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMGeometryComponent>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMEnRouteSegmentPoint>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMTerminalSegmentPoint>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMStart>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMEnd>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMStartPoint>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMEndPoint>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeADRConnectingPoint>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeADRInitialApproachFix>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMAirspaceLayer>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMLevels>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMAvailabilityBase>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMAvailability>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMClientRoute>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMRoutePortion>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMDirectFlightClass>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMFlightConditionCircumstance>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMOperationalCondition>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMFlightRestrictionLevel>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMFlightLevel>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMDirectFlightSegment>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMElementRoutePortionElement>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMElementDirectFlightElement>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMFlightRoutingElement>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMRouteElement>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMFlightRestrictionRoute>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMRegulatedRoute>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMAircraftCharacteristic>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMFlightConditionAircraft>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMFlightCharacteristic>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMFlightConditionFlight>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMFlightConditionOperand>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMFlightConditionRoutePortionCondition>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMFlightConditionDirectFlightCondition>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeADRAirspaceBorderCrossingObject>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeADRBorderCrossingCondition>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMElementBase>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMElement>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMFlightConditionCombination>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMFlight>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMAnyTimeSlice>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMTimeSlice>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMObject>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeADRMessageMember>& el);
		virtual const std::string& get_element_name(void) const = 0;

		virtual bool is_ignore(void) const { return false; }

		void error(const std::string& text) const;
		void error(const_ptr_t child, const std::string& text) const;
		void warning(const std::string& text) const;
		void warning(const_ptr_t child, const std::string& text) const;
		void info(const std::string& text) const;
		void info(const_ptr_t child, const std::string& text) const;

		const std::string& get_tagname(void) const { return m_tagname; }
		unsigned int get_childnum(void) const { return m_childnum; }
		unsigned int get_childcount(void) const { return m_childcount; }

		const std::string& get_id(void) const { return m_id; }

	protected:
		ParseXML& m_parser;
		std::string m_tagname;
		std::string m_id;
		mutable std::atomic<unsigned int> m_refcount;
		unsigned int m_childnum;
		unsigned int m_childcount;
	};

	class NodeIgnore : public Node {
	public:
		typedef boost::intrusive_ptr<NodeIgnore> ptr_t;

		NodeIgnore(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties, const std::string& name);

		virtual void on_characters(const Glib::ustring& characters) {}
		virtual void on_subelement(const Node::ptr_t& el) {}
		virtual const std::string& get_element_name(void) const { return m_elementname; }
		virtual bool is_ignore(void) const { return true; }

	protected:
		std::string m_elementname;
	};

	class NodeNoIgnore : public Node {
	public:
		NodeNoIgnore(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) : Node(parser, tagname, childnum, properties) {}

		virtual void on_subelement(const boost::intrusive_ptr<NodeIgnore>& el);
	};

	class NodeText : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeText> ptr_t;

		typedef enum {
			tt_first,
			tt_adrconditionalroutetype = tt_first,
			tt_adrday,
			tt_adrenabled,
			tt_adrenddate,
			tt_adrendtime,
			tt_adrexclude,
			tt_adrflexibleuse,
			tt_adrlevel1,
			tt_adrlevel2,
			tt_adrlevel3,
			tt_adrprocessingindicator,
			tt_adrspecialday,
			tt_adrstartdate,
			tt_adrstarttime,
			tt_adrtimeoperand,
			tt_adrtimereference,
			tt_aixmangle,
			tt_aixmangletype,
			tt_aixmcontroltype,
			tt_aixmdateday,
			tt_aixmdateyear,
			tt_aixmdependency,
			tt_aixmdesignator,
			tt_aixmdesignatoriata,
			tt_aixmdesignatoricao,
			tt_aixmdesignatornumber,
			tt_aixmdesignatorprefix,
			tt_aixmdesignatorsecondletter,
			tt_aixmdirection,
			tt_aixmdistance,
			tt_aixmengine,
			tt_aixmexceedlength,
			tt_aixmindex,
			tt_aixminstruction,
			tt_aixminterpretation,
			tt_aixmlocaltype,
			tt_aixmlocationindicatoricao,
			tt_aixmlogicaloperator,
			tt_aixmlowerlevel,
			tt_aixmlowerlevelreference,
			tt_aixmlowerlimit,
			tt_aixmlowerlimitaltitude,
			tt_aixmlowerlimitreference,
			tt_aixmmilitary,
			tt_aixmmultipleidentifier,
			tt_aixmname,
			tt_aixmnavigationspecification,
			tt_aixmnumberengine,
			tt_aixmoperation,
			tt_aixmordernumber,
			tt_aixmpurpose,
			tt_aixmreferencelocation,
			tt_aixmrelationwithlocation,
			tt_aixmseries,
			tt_aixmstandardicao,
			tt_aixmstatus,
			tt_aixmtype,
			tt_aixmtypeaircrafticao,
			tt_aixmupperlevel,
			tt_aixmupperlevelreference,
			tt_aixmupperlimit,
			tt_aixmupperlimitaltitude,
			tt_aixmupperlimitreference,
			tt_aixmverticalseparationcapability,
			tt_gmlidentifier,
			tt_gmlpos,
			tt_invalid
		} texttype_t;

		NodeText(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties, const std::string& name);
		NodeText(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties, texttype_t tt = tt_invalid);

		virtual void on_characters(const Glib::ustring& characters);

		virtual const std::string& get_element_name(void) const { return get_static_element_name(m_texttype); }
		static const std::string& get_static_element_name(texttype_t tt);
		static void check_type_order(void);
		static texttype_t find_type(const std::string& name);
		texttype_t get_type(void) const { return m_texttype; }
		const std::string& get_text(void) const { return m_text; }
		long get_long(void) const;
		unsigned long get_ulong(void) const;
		double get_double(void) const;
		Point::coord_t get_lat(void) const;
		Point::coord_t get_lon(void) const;
		UUID get_uuid(void) const;
		Point get_coord_deg(void) const;
		std::pair<Point,int32_t> get_coord_3d(void) const;
		int get_timehhmm(void) const; // in minutes
		timetype_t get_dateyyyymmdd(void) const;
		int get_daynr(void) const;
		int get_yesno(void) const;
		void update(AltRange& ar);

	protected:
		std::string m_text;
		double m_factor;
		unsigned long m_factorl;
		texttype_t m_texttype;

		void check_attr(const AttributeList& properties);
	};

	class NodeLink : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeLink> ptr_t;

		typedef enum {
			lt_first,
			lt_adrenteredairspace = lt_first,
			lt_adrexitedairspace,
			lt_adrpointchoicefixdesignatedpoint,
			lt_adrpointchoicenavaidsystem,
			lt_adrreferencedprocedure,
			lt_aixmairportheliport,
			lt_aixmarrival,
			lt_aixmauthority,
			lt_aixmclientairspace,
			lt_aixmdeparture,
			lt_aixmdependentairport,
			lt_aixmdiscretelevelseries,
			lt_aixmelementairspaceelement,
			lt_aixmelementstandardinstrumentarrivalelement,
			lt_aixmelementstandardinstrumentdepartureelement,
			lt_aixmendairportreferencepoint,
			lt_aixmendfixdesignatedpoint,
			lt_aixmendnavaidsystem,
			lt_aixmfix,
			lt_aixmflightconditionairportheliportcondition,
			lt_aixmflightconditionairspacecondition,
			lt_aixmflightconditionstandardinstrumentarrivalcondition,
			lt_aixmflightconditionstandardinstrumentdeparturecondition,
			lt_aixmhostairport,
			lt_aixmleveltable,
			lt_aixmpointchoiceairportreferencepoint,
			lt_aixmpointchoicefixdesignatedpoint,
			lt_aixmpointchoicenavaidsystem,
			lt_aixmpointelementfixdesignatedpoint,
			lt_aixmpointelementnavaidsystem,
			lt_aixmreferencedroute,
			lt_aixmrouteformed,
			lt_aixmserviceprovider,
			lt_aixmsignificantpointconditionfixdesignatedpoint,
			lt_aixmsignificantpointconditionnavaidsystem,
			lt_aixmstartairportreferencepoint,
			lt_aixmstartfixdesignatedpoint,
			lt_aixmstartnavaidsystem,
			lt_aixmtheairspace,
			lt_gmlpointproperty,
			lt_invalid
		} linktype_t;

		NodeLink(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties, const std::string& name);
		NodeLink(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties, linktype_t lt = lt_invalid);

		virtual const std::string& get_element_name(void) const { return get_static_element_name(m_linktype); }
		static const std::string& get_static_element_name(linktype_t lt);
		static void check_type_order(void);
		static linktype_t find_type(const std::string& name);
		linktype_t get_type(void) const { return m_linktype; }
		const UUID& get_uuid(void) const { return m_uuid; }

	protected:
		UUID m_uuid;
		linktype_t m_linktype;

		void process_attr(const AttributeList& properties);
	};

	class NodeAIXMElementBase : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMElementBase> ptr_t;

		NodeAIXMElementBase(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
	};

	class NodeAIXMExtensionBase : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMExtensionBase> ptr_t;

		NodeAIXMExtensionBase(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
	};

	class NodeAIXMAvailabilityBase : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMAvailabilityBase> ptr_t;

		NodeAIXMAvailabilityBase(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
	};

	class NodeADRWeekDayPattern : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeADRWeekDayPattern> ptr_t;

		NodeADRWeekDayPattern(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeADRWeekDayPattern(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const TimePattern& get_timepat(void) const { return m_timepat; }

	protected:
		static const std::string elementname;
		TimePattern m_timepat;
	};

	class NodeADRSpecialDayPattern : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeADRSpecialDayPattern> ptr_t;

		NodeADRSpecialDayPattern(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeADRSpecialDayPattern(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const TimePattern& get_timepat(void) const { return m_timepat; }

	protected:
		static const std::string elementname;
		TimePattern m_timepat;
	};

	class NodeADRTimePatternCombination : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeADRTimePatternCombination> ptr_t;

		NodeADRTimePatternCombination(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeADRTimePatternCombination(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeADRWeekDayPattern>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeADRSpecialDayPattern>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		TimeTableElement get_timetable(void) const;

	protected:
		static const std::string elementname;
		TimeTableElement m_timetable;
	};

	class NodeADRTimeTable1 : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeADRTimeTable1> ptr_t;

		NodeADRTimeTable1(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeADRTimeTable1(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeADRTimePatternCombination>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const TimeTableElement& get_timetable(void) const { return m_timetable; }

	protected:
		static const std::string elementname;
		TimeTableElement m_timetable;
	};

	class NodeADRTimeTable : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeADRTimeTable> ptr_t;

		NodeADRTimeTable(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeADRTimeTable(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeADRTimeTable1>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const TimeTable& get_timetable(void) const { return m_timetable; }

	protected:
		static const std::string elementname;
		TimeTable m_timetable;
	};

	class NodeADRLevels : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeADRLevels> ptr_t;

		NodeADRLevels(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeADRLevels(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeADRTimeTable>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const TimeTable& get_timetable(void) const { return m_timetable; }
		const AltRange& get_altrange(void) const { return m_altrange; }

	protected:
		static const std::string elementname;
		TimeTable m_timetable;
		AltRange m_altrange;
	};

	class NodeADRPropertiesWithScheduleExtension : public NodeAIXMExtensionBase {
	public:
		typedef boost::intrusive_ptr<NodeADRPropertiesWithScheduleExtension> ptr_t;

		NodeADRPropertiesWithScheduleExtension(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeADRPropertiesWithScheduleExtension(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeADRTimeTable>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const TimeTable& get_timetable(void) const { return m_timetable; }

	protected:
		static const std::string elementname;
		TimeTable m_timetable;
	};

	class NodeADRDefaultApplicability : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeADRDefaultApplicability> ptr_t;

		NodeADRDefaultApplicability(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeADRDefaultApplicability(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		TimeTable get_timetable(void) const;

	protected:
		static const std::string elementname;
		TimeTableElement m_timetable;
	};

	class NodeADRAirspaceExtension : public NodeAIXMExtensionBase {
	public:
		typedef boost::intrusive_ptr<NodeADRAirspaceExtension> ptr_t;

		NodeADRAirspaceExtension(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeADRAirspaceExtension(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		bool is_flexibleuse(void) const { return m_flexibleuse; }
		bool is_level1(void) const { return m_level1; }
		bool is_level2(void) const { return m_level2; }
		bool is_level3(void) const { return m_level3; }

	protected:
		static const std::string elementname;
		bool m_flexibleuse;
		bool m_level1;
		bool m_level2;
		bool m_level3;
	};

	class NodeADRNonDefaultApplicability : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeADRNonDefaultApplicability> ptr_t;

		NodeADRNonDefaultApplicability(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeADRNonDefaultApplicability(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeADRTimeTable>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const TimeTable& get_timetable(void) const { return m_timetable; }

	protected:
		static const std::string elementname;
		TimeTable m_timetable;
	};

	class NodeADRRouteAvailabilityExtension : public NodeAIXMExtensionBase {
	public:
		typedef boost::intrusive_ptr<NodeADRRouteAvailabilityExtension> ptr_t;

		NodeADRRouteAvailabilityExtension(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeADRRouteAvailabilityExtension(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeADRNonDefaultApplicability>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeADRDefaultApplicability>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const TimeTable& get_timetable(void) const { return m_timetable; }
		unsigned int get_cdr(void) const { return m_cdr; }

	protected:
		static const std::string elementname;
		TimeTable m_timetable;
		unsigned int m_cdr;
	};

	class NodeADRProcedureAvailabilityExtension : public NodeAIXMExtensionBase {
	public:
		typedef boost::intrusive_ptr<NodeADRProcedureAvailabilityExtension> ptr_t;

		NodeADRProcedureAvailabilityExtension(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeADRProcedureAvailabilityExtension(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeADRTimeTable>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const TimeTable& get_timetable(void) const { return m_timetable; }

	protected:
		static const std::string elementname;
		TimeTable m_timetable;
	};

	class NodeADRRouteSegmentExtension : public NodeAIXMExtensionBase {
	public:
		typedef boost::intrusive_ptr<NodeADRRouteSegmentExtension> ptr_t;

		NodeADRRouteSegmentExtension(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeADRRouteSegmentExtension(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeADRLevels>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const TimeTable& get_timetable(void) const { return m_timetable; }
		const AltRange& get_altrange(void) const { return m_altrange; }

	protected:
		static const std::string elementname;
		TimeTable m_timetable;
		AltRange m_altrange;
	};

	class NodeADRIntermediateSignificantPoint : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeADRIntermediateSignificantPoint> ptr_t;

		NodeADRIntermediateSignificantPoint(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeADRIntermediateSignificantPoint(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeLink::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const UUID& get_uuid(void) const { return m_uuid; }

	protected:
		static const std::string elementname;
		UUID m_uuid;
	};

	class NodeADRIntermediatePoint : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeADRIntermediatePoint> ptr_t;

		NodeADRIntermediatePoint(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeADRIntermediatePoint(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeADRIntermediateSignificantPoint>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const UUID& get_uuid(void) const { return m_uuid; }

	protected:
		static const std::string elementname;
		UUID m_uuid;
	};

	class NodeADRRoutePortionExtension : public NodeAIXMExtensionBase {
	public:
		typedef boost::intrusive_ptr<NodeADRRoutePortionExtension> ptr_t;

		NodeADRRoutePortionExtension(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeADRRoutePortionExtension(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeLink::ptr_t& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeADRIntermediatePoint>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const UUID& get_refproc(void) const { return m_refproc; }
		typedef std::vector<UUID> intermpt_t;
		const intermpt_t& get_intermpt(void) const { return m_intermpt; }


	protected:
		static const std::string elementname;
		UUID m_refproc;
		intermpt_t m_intermpt;
	};
 
	class NodeADRStandardInstrumentDepartureExtension : public NodeAIXMExtensionBase {
	public:
		typedef boost::intrusive_ptr<NodeADRStandardInstrumentDepartureExtension> ptr_t;

		NodeADRStandardInstrumentDepartureExtension(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeADRStandardInstrumentDepartureExtension(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeADRConnectingPoint>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		typedef std::set<UUID> connpt_t;
		const connpt_t& get_connpt(void) const { return m_connpt; }

	protected:
		static const std::string elementname;
		connpt_t m_connpt;
	};

	class NodeADRStandardInstrumentArrivalExtension : public NodeAIXMExtensionBase {
	public:
		typedef boost::intrusive_ptr<NodeADRStandardInstrumentArrivalExtension> ptr_t;

		NodeADRStandardInstrumentArrivalExtension(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeADRStandardInstrumentArrivalExtension(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeADRConnectingPoint>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeADRInitialApproachFix>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		typedef std::set<UUID> connpt_t;
		const connpt_t& get_connpt(void) const { return m_connpt; }
		const UUID& get_iaf(void) const { return m_iaf; }

	protected:
		static const std::string elementname;
		connpt_t m_connpt;
		UUID m_iaf;
	};

	class NodeAIXMExtension : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMExtension> ptr_t;

		NodeAIXMExtension(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMExtension(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMExtensionBase>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const boost::intrusive_ptr<NodeAIXMExtensionBase>& get_el(void) const { return m_el; }
		template<typename T> boost::intrusive_ptr<T> get(void) const { return boost::dynamic_pointer_cast<T>(get_el()); }

	protected:
		static const std::string elementname;
		boost::intrusive_ptr<NodeAIXMExtensionBase> m_el;
	};

	class NodeAIXMAirspaceActivation : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMAirspaceActivation> ptr_t;

		NodeAIXMAirspaceActivation(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMAirspaceActivation(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMExtension>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const TimeTable& get_timetable(void) const { return m_timetable; }

	protected:
		static const std::string elementname;
		TimeTable m_timetable;
	};

	class NodeAIXMActivation : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMActivation> ptr_t;

		NodeAIXMActivation(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMActivation(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMAirspaceActivation>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const boost::intrusive_ptr<Node>& get_el(void) const { return m_el; }
		template<typename T> boost::intrusive_ptr<T> get(void) const { return boost::dynamic_pointer_cast<T>(get_el()); }

	protected:
		static const std::string elementname;
		boost::intrusive_ptr<Node> m_el;
	};

	class NodeAIXMElevatedPoint : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMElevatedPoint> ptr_t;

		NodeAIXMElevatedPoint(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMElevatedPoint(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const Point& get_coord(void) const { return m_coord; }
		int32_t get_elev(void) const { return m_elev; }

	protected:
		static const std::string elementname;
		Point m_coord;
		int32_t m_elev;
	};

	class NodeAIXMPoint : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMPoint> ptr_t;

		NodeAIXMPoint(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMPoint(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const Point& get_coord(void) const { return m_coord; }

	protected:
		static const std::string elementname;
		Point m_coord;
	};

	class NodeAIXMARP : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMARP> ptr_t;

		NodeAIXMARP(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMARP(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeAIXMElevatedPoint::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const Point& get_coord(void) const { return m_coord; }
		int32_t get_elev(void) const { return m_elev; }

	protected:
		static const std::string elementname;
		Point m_coord;
		int32_t m_elev;
	};

	class NodeAIXMLocation : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMLocation> ptr_t;

		NodeAIXMLocation(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMLocation(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeAIXMElevatedPoint::ptr_t& el);
		virtual void on_subelement(const NodeAIXMPoint::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const Point& get_coord(void) const { return m_coord; }
		int32_t get_elev(void) const { return m_elev; }

	protected:
		static const std::string elementname;
		Point m_coord;
		int32_t m_elev;
	};

	class NodeAIXMCity : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMCity> ptr_t;

		NodeAIXMCity(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMCity(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const std::string& get_city(void) const { return m_city; }

	protected:
		static const std::string elementname;
		std::string m_city;
	};

	class NodeAIXMServedCity : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMServedCity> ptr_t;

		NodeAIXMServedCity(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMServedCity(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeAIXMCity::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		typedef AirportTimeSlice::servedcities_t servedcities_t;
		const servedcities_t get_cities(void) const { return m_cities; }

	protected:
		static const std::string elementname;
		servedcities_t m_cities;
	};

	class NodeGMLValidTime : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeGMLValidTime> ptr_t;

		NodeGMLValidTime(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeGMLValidTime(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeGMLTimePeriod>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		long get_start(void) const { return m_start; }
		long get_end(void) const { return m_end; }

	protected:
		static const std::string elementname;
		long m_start;
		long m_end;
	};

	class NodeAIXMFeatureLifetime : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMFeatureLifetime> ptr_t;

		NodeAIXMFeatureLifetime(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMFeatureLifetime(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeGMLTimePeriod>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		long get_start(void) const { return m_start; }
		long get_end(void) const { return m_end; }

	protected:
		static const std::string elementname;
		long m_start;
		long m_end;
	};

	class NodeGMLPosition : public NodeNoIgnore {
	public:
		static const long invalid;
		static const long unknown;

		NodeGMLPosition(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);

		virtual void on_characters(const Glib::ustring& characters);

		long get_time(void) const;

	protected:
		std::string m_text;
		bool m_unknown;
	};

	class NodeGMLBeginPosition : public NodeGMLPosition {
	public:
		typedef boost::intrusive_ptr<NodeGMLBeginPosition> ptr_t;

		NodeGMLBeginPosition(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeGMLBeginPosition(parser, tagname, childnum, properties));
		}

		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		long get_time(void) const;

	protected:
		static const std::string elementname;
	};

	class NodeGMLEndPosition : public NodeGMLPosition {
	public:
		typedef boost::intrusive_ptr<NodeGMLEndPosition> ptr_t;

		NodeGMLEndPosition(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeGMLEndPosition(parser, tagname, childnum, properties));
		}

		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		long get_time(void) const;

	protected:
		static const std::string elementname;
	};

	class NodeGMLTimePeriod : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeGMLTimePeriod> ptr_t;

		NodeGMLTimePeriod(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeGMLTimePeriod(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeGMLBeginPosition::ptr_t& el);
		virtual void on_subelement(const NodeGMLEndPosition::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		long get_start(void) const { return m_start; }
		long get_end(void) const { return m_end; }

	protected:
		static const std::string elementname;
		long m_start;
		long m_end;
	};

	class NodeGMLLinearRing : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeGMLLinearRing> ptr_t;

		NodeGMLLinearRing(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeGMLLinearRing(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeText>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeLink>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const PolygonSimple& get_poly(void) const { return m_poly; }
		const AirspaceTimeSlice::Component::pointlink_t& get_pointlink(void) const { return m_pointlink; }

	protected:
		static const std::string elementname;
		PolygonSimple m_poly;
		AirspaceTimeSlice::Component::pointlink_t m_pointlink;
	};

	class NodeGMLInterior : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeGMLInterior> ptr_t;

		NodeGMLInterior(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeGMLExterior(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeGMLLinearRing>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const PolygonSimple& get_poly(void) const { return m_poly; }
		const AirspaceTimeSlice::Component::pointlink_t& get_pointlink(void) const { return m_pointlink; }

	protected:
		static const std::string elementname;
		PolygonSimple m_poly;
		AirspaceTimeSlice::Component::pointlink_t m_pointlink;
	};

	class NodeGMLExterior : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeGMLExterior> ptr_t;

		NodeGMLExterior(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeGMLExterior(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeGMLLinearRing>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const PolygonSimple& get_poly(void) const { return m_poly; }
		const AirspaceTimeSlice::Component::pointlink_t& get_pointlink(void) const { return m_pointlink; }

	protected:
		static const std::string elementname;
		PolygonSimple m_poly;
		AirspaceTimeSlice::Component::pointlink_t m_pointlink;
	};

	class NodeGMLPolygonPatch : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeGMLPolygonPatch> ptr_t;

		NodeGMLPolygonPatch(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeGMLPolygonPatch(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeGMLInterior>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeGMLExterior>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const PolygonHole& get_poly(void) const { return m_poly; }
		const AirspaceTimeSlice::Component::pointlink_t& get_pointlink(void) const { return m_pointlink; }

	protected:
		static const std::string elementname;
		PolygonHole m_poly;
		AirspaceTimeSlice::Component::pointlink_t m_pointlink;
	};

	class NodeGMLPatches : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeGMLPatches> ptr_t;

		NodeGMLPatches(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeGMLPatches(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeGMLPolygonPatch>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const MultiPolygonHole& get_poly(void) const { return m_poly; }
		const AirspaceTimeSlice::Component::pointlink_t& get_pointlink(void) const { return m_pointlink; }

	protected:
		static const std::string elementname;
		MultiPolygonHole m_poly;
		AirspaceTimeSlice::Component::pointlink_t m_pointlink;
	};

	class NodeAIXMSurface : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMSurface> ptr_t;

		NodeAIXMSurface(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMSurface(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeGMLPatches>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const MultiPolygonHole& get_poly(void) const { return m_poly; }
		const AirspaceTimeSlice::Component::pointlink_t& get_pointlink(void) const { return m_pointlink; }

	protected:
		static const std::string elementname;
		MultiPolygonHole m_poly;
		AirspaceTimeSlice::Component::pointlink_t m_pointlink;
	};

	class NodeAIXMHorizontalProjection : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMHorizontalProjection> ptr_t;

		NodeAIXMHorizontalProjection(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMHorizontalProjection(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMSurface>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const MultiPolygonHole& get_poly(void) const { return m_poly; }
		const AirspaceTimeSlice::Component::pointlink_t& get_pointlink(void) const { return m_pointlink; }

	protected:
		static const std::string elementname;
		MultiPolygonHole m_poly;
		AirspaceTimeSlice::Component::pointlink_t m_pointlink;
	};

	class NodeAIXMAirspaceVolumeDependency : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMAirspaceVolumeDependency> ptr_t;

		NodeAIXMAirspaceVolumeDependency(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMAirspaceVolumeDependency(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeText>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeLink>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const AirspaceTimeSlice::Component& get_comp(void) const { return m_comp; }

	protected:
		static const std::string elementname;
		AirspaceTimeSlice::Component m_comp;
	};

	class NodeAIXMContributorAirspace : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMContributorAirspace> ptr_t;

		NodeAIXMContributorAirspace(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMContributorAirspace(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMAirspaceVolumeDependency>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const AirspaceTimeSlice::Component& get_comp(void) const { return m_comp; }

	protected:
		static const std::string elementname;
		AirspaceTimeSlice::Component m_comp;
	};

	class NodeAIXMAirspaceVolume : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMAirspaceVolume> ptr_t;

		NodeAIXMAirspaceVolume(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMAirspaceVolume(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeText>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMHorizontalProjection>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMContributorAirspace>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const AirspaceTimeSlice::Component& get_comp(void) const { return m_comp; }

	protected:
		static const std::string elementname;
		AirspaceTimeSlice::Component m_comp;
	};

	class NodeAIXMTheAirspaceVolume : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMTheAirspaceVolume> ptr_t;

		NodeAIXMTheAirspaceVolume(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMTheAirspaceVolume(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMAirspaceVolume>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const AirspaceTimeSlice::Component& get_comp(void) const { return m_comp; }

	protected:
		static const std::string elementname;
		AirspaceTimeSlice::Component m_comp;
	};

	class NodeAIXMAirspaceGeometryComponent : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMAirspaceGeometryComponent> ptr_t;

		NodeAIXMAirspaceGeometryComponent(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMAirspaceGeometryComponent(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeText>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMTheAirspaceVolume>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const AirspaceTimeSlice::Component& get_comp(void) const { return m_comp; }

	protected:
		static const std::string elementname;
		AirspaceTimeSlice::Component m_comp;
	};

	class NodeAIXMGeometryComponent : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMGeometryComponent> ptr_t;

		NodeAIXMGeometryComponent(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMGeometryComponent(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMAirspaceGeometryComponent>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const AirspaceTimeSlice::Component& get_comp(void) const { return m_comp; }

	protected:
		static const std::string elementname;
		AirspaceTimeSlice::Component m_comp;
	};

	class NodeAIXMEnRouteSegmentPoint : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMEnRouteSegmentPoint> ptr_t;

		NodeAIXMEnRouteSegmentPoint(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMEnRouteSegmentPoint(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeLink::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const UUID& get_uuid(void) const { return m_uuid; }

	protected:
		static const std::string elementname;
		UUID m_uuid;
	};

	class NodeAIXMTerminalSegmentPoint : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMTerminalSegmentPoint> ptr_t;

		NodeAIXMTerminalSegmentPoint(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMTerminalSegmentPoint(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeLink::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const UUID& get_uuid(void) const { return m_uuid; }

	protected:
		static const std::string elementname;
		UUID m_uuid;
	};

	class NodeAIXMStart : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMStart> ptr_t;

		NodeAIXMStart(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMStart(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMEnRouteSegmentPoint>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const UUID& get_uuid(void) const { return m_uuid; }

	protected:
		static const std::string elementname;
		UUID m_uuid;
	};

	class NodeAIXMEnd : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMEnd> ptr_t;

		NodeAIXMEnd(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMEnd(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMEnRouteSegmentPoint>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const UUID& get_uuid(void) const { return m_uuid; }

	protected:
		static const std::string elementname;
		UUID m_uuid;
	};

	class NodeAIXMStartPoint : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMStartPoint> ptr_t;

		NodeAIXMStartPoint(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMStartPoint(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMTerminalSegmentPoint>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const UUID& get_uuid(void) const { return m_uuid; }

	protected:
		static const std::string elementname;
		UUID m_uuid;
	};

	class NodeAIXMEndPoint : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMEndPoint> ptr_t;

		NodeAIXMEndPoint(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMEndPoint(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMTerminalSegmentPoint>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const UUID& get_uuid(void) const { return m_uuid; }

	protected:
		static const std::string elementname;
		UUID m_uuid;
	};

	class NodeADRConnectingPoint : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeADRConnectingPoint> ptr_t;

		NodeADRConnectingPoint(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeADRConnectingPoint(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMTerminalSegmentPoint>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const UUID& get_uuid(void) const { return m_uuid; }

	protected:
		static const std::string elementname;
		UUID m_uuid;
	};

	class NodeADRInitialApproachFix : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeADRInitialApproachFix> ptr_t;

		NodeADRInitialApproachFix(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeADRInitialApproachFix(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMTerminalSegmentPoint>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const UUID& get_uuid(void) const { return m_uuid; }

	protected:
		static const std::string elementname;
		UUID m_uuid;
	};

	class NodeAIXMAirspaceLayer : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMAirspaceLayer> ptr_t;

		NodeAIXMAirspaceLayer(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMAirspaceLayer(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual void on_subelement(const NodeLink::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const RouteSegmentTimeSlice::Availability& get_availability(void) const { return m_availability; }

	protected:
		static const std::string elementname;
		RouteSegmentTimeSlice::Availability m_availability;
	};

	class NodeAIXMLevels : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMLevels> ptr_t;

		NodeAIXMLevels(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMLevels(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMAirspaceLayer>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const RouteSegmentTimeSlice::Availability& get_availability(void) const { return m_availability; }

	protected:
		static const std::string elementname;
		RouteSegmentTimeSlice::Availability m_availability;
	};

	class NodeAIXMRouteAvailability : public NodeAIXMAvailabilityBase {
	public:
		typedef boost::intrusive_ptr<NodeAIXMRouteAvailability> ptr_t;

		NodeAIXMRouteAvailability(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMRouteAvailability(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMLevels>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMExtension>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const RouteSegmentTimeSlice::Availability& get_availability(void) const { return m_availability; }

	protected:
		static const std::string elementname;
		RouteSegmentTimeSlice::Availability m_availability;
	};

	class NodeAIXMProcedureAvailability : public NodeAIXMAvailabilityBase {
	public:
		typedef boost::intrusive_ptr<NodeAIXMProcedureAvailability> ptr_t;

		NodeAIXMProcedureAvailability(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMProcedureAvailability(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMExtension>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const TimeTable& get_timetable(void) const { return m_timetable; }
		StandardInstrumentTimeSlice::status_t get_status(void) const { return m_status; }

	protected:
		static const std::string elementname;
		TimeTable m_timetable;
		StandardInstrumentTimeSlice::status_t m_status;
	};

	class NodeAIXMAvailability : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMAvailability> ptr_t;

		NodeAIXMAvailability(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMAvailability(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMAvailabilityBase>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const boost::intrusive_ptr<NodeAIXMAvailabilityBase>& get_el(void) const { return m_el; }
		template<typename T> boost::intrusive_ptr<T> get(void) const { return boost::dynamic_pointer_cast<T>(get_el()); }

	protected:
		static const std::string elementname;
		boost::intrusive_ptr<NodeAIXMAvailabilityBase> m_el;
	};

	class NodeAIXMClientRoute : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMClientRoute> ptr_t;

		NodeAIXMClientRoute(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMClientRoute(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMRoutePortion>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const UUID& get_route(void) const { return m_route; }
		const UUID& get_start(void) const { return m_start; }
		const UUID& get_end(void) const { return m_end; }

	protected:
		static const std::string elementname;
		UUID m_route;
		UUID m_start;
		UUID m_end;
	};

	class NodeAIXMRoutePortion : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMRoutePortion> ptr_t;

		NodeAIXMRoutePortion(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMRoutePortion(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeLink::ptr_t& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMExtension>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const UUID& get_route(void) const { return m_route; }
		const UUID& get_start(void) const { return m_start; }
		const UUID& get_end(void) const { return m_end; }

	protected:
		static const std::string elementname;
		UUID m_route;
		UUID m_start;
		UUID m_end;
	};

	class NodeAIXMDirectFlightClass : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMDirectFlightClass> ptr_t;

		NodeAIXMDirectFlightClass(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMDirectFlightClass(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		double get_length(void) const { return m_length; }

	protected:
		static const std::string elementname;
		double m_length;
	};

	class NodeAIXMFlightConditionCircumstance : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMFlightConditionCircumstance> ptr_t;

		NodeAIXMFlightConditionCircumstance(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMFlightConditionCircumstance(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		bool is_refloc(void) const { return m_refloc; }

		typedef enum {
			locrel_dep,
			locrel_arr,
			locrel_act,
			locrel_avbl,
			locrel_xng,
			locrel_invalid
		} locrel_t;
		locrel_t get_locrel(void) const { return m_locrel; }
		static const std::string& get_locrel_string(locrel_t lr);
		const std::string& get_locrel_string(void) const { return get_locrel_string(get_locrel()); }

	protected:
		static const std::string elementname;
		bool m_refloc;
		locrel_t m_locrel;
	};

	class NodeAIXMOperationalCondition : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMOperationalCondition> ptr_t;

		NodeAIXMOperationalCondition(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMOperationalCondition(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMFlightConditionCircumstance>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		bool is_refloc(void) const { return m_refloc; }
		NodeAIXMFlightConditionCircumstance::locrel_t get_locrel(void) const { return m_locrel; }

	protected:
		static const std::string elementname;
		bool m_refloc;
		NodeAIXMFlightConditionCircumstance::locrel_t m_locrel;
	};

	class NodeAIXMFlightRestrictionLevel : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMFlightRestrictionLevel> ptr_t;

		NodeAIXMFlightRestrictionLevel(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMFlightRestrictionLevel(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const AltRange& get_altrange(void) const { return m_altrange; }

	protected:
		static const std::string elementname;
		AltRange m_altrange;
	};

	class NodeAIXMFlightLevel : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMFlightLevel> ptr_t;

		NodeAIXMFlightLevel(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMFlightLevel(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMFlightRestrictionLevel>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const AltRange& get_altrange(void) const { return m_altrange; }

	protected:
		static const std::string elementname;
		AltRange m_altrange;
	};

	class NodeADRFlightConditionCombinationExtension : public NodeAIXMExtensionBase {
	public:
		typedef boost::intrusive_ptr<NodeADRFlightConditionCombinationExtension> ptr_t;

		NodeADRFlightConditionCombinationExtension(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeADRFlightConditionCombinationExtension(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeADRTimeTable>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const TimeTable& get_timetable(void) const { return m_timetable; }

	protected:
		static const std::string elementname;
		TimeTable m_timetable;
	};

	class NodeAIXMDirectFlightSegment : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMDirectFlightSegment> ptr_t;

		NodeAIXMDirectFlightSegment(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMDirectFlightSegment(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeLink::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const UUID& get_start(void) const { return m_start; }
		const UUID& get_end(void) const { return m_end; }

	protected:
		static const std::string elementname;
		UUID m_start;
		UUID m_end;
	};

	class NodeAIXMElementRoutePortionElement : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMElementRoutePortionElement> ptr_t;

		NodeAIXMElementRoutePortionElement(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMElementRoutePortionElement(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMRoutePortion>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const UUID& get_route(void) const { return m_route; }
		const UUID& get_start(void) const { return m_start; }
		const UUID& get_end(void) const { return m_end; }

	protected:
		static const std::string elementname;
		UUID m_route;
		UUID m_start;
		UUID m_end;
	};

	class NodeAIXMElementDirectFlightElement : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMElementDirectFlightElement> ptr_t;

		NodeAIXMElementDirectFlightElement(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMElementDirectFlightElement(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMDirectFlightSegment>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const UUID& get_start(void) const { return m_start; }
		const UUID& get_end(void) const { return m_end; }

	protected:
		static const std::string elementname;
		UUID m_start;
		UUID m_end;
	};

	class NodeAIXMFlightRoutingElement : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMFlightRoutingElement> ptr_t;

		NodeAIXMFlightRoutingElement(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMFlightRoutingElement(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual void on_subelement(const NodeLink::ptr_t& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMFlightLevel>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMElementRoutePortionElement>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMElementDirectFlightElement>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		unsigned int get_ordernumber(void) const { return m_ordernumber; }
		RestrictionElement::ptr_t get_elem(void) const;

	protected:
		static const std::string elementname;
		unsigned int m_ordernumber;
		AltRange m_altrange;
		UUID m_uuid;
		UUID m_start;
		UUID m_end;
		RestrictionElement::type_t m_type;
		bool m_star;
	};

	class NodeAIXMRouteElement : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMRouteElement> ptr_t;

		NodeAIXMRouteElement(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMRouteElement(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMFlightRoutingElement>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		unsigned int get_ordernumber(void) const { return m_ordernumber; }
		const RestrictionElement::ptr_t& get_elem(void) const { return m_elem; }

	protected:
		static const std::string elementname;
		RestrictionElement::ptr_t m_elem;
		unsigned int m_ordernumber;
	};

	class NodeAIXMFlightRestrictionRoute : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMFlightRestrictionRoute> ptr_t;

		NodeAIXMFlightRestrictionRoute(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMFlightRestrictionRoute(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMRouteElement>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		RestrictionSequence get_seq(void) const;

	protected:
		static const std::string elementname;
		typedef std::pair<RestrictionElement::ptr_t,unsigned int> elem_t;
		typedef std::vector<elem_t> elems_t;
		elems_t m_elems;
	};

	class NodeAIXMRegulatedRoute : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMRegulatedRoute> ptr_t;

		NodeAIXMRegulatedRoute(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMRegulatedRoute(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMFlightRestrictionRoute>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const Restrictions& get_res(void) const { return m_res; }

	protected:
		static const std::string elementname;
		Restrictions m_res;
	};

	class NodeAIXMAircraftCharacteristic : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMAircraftCharacteristic> ptr_t;

		NodeAIXMAircraftCharacteristic(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMAircraftCharacteristic(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		ConditionAircraft::acfttype_t get_acfttype(void) const { return m_acfttype; }
		ConditionAircraft::engine_t get_engine(void) const { return m_engine; }
		ConditionAircraft::navspec_t get_navspec(void) const { return m_navspec; }
		ConditionAircraft::vertsep_t get_vertsep(void) const { return m_vertsep; }
		const std::string& get_icaotype(void) const { return m_icaotype; }
		unsigned int get_engines(void) const { return m_engines; }

	protected:
		static const std::string elementname;
		std::string m_icaotype;
		unsigned int m_engines;
		ConditionAircraft::acfttype_t m_acfttype;
		ConditionAircraft::engine_t m_engine;
		ConditionAircraft::navspec_t m_navspec;
		ConditionAircraft::vertsep_t m_vertsep;
	};

	class NodeAIXMFlightConditionAircraft : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMFlightConditionAircraft> ptr_t;

		NodeAIXMFlightConditionAircraft(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMFlightConditionAircraft(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMAircraftCharacteristic>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		ConditionAircraft::acfttype_t get_acfttype(void) const { return m_acfttype; }
		ConditionAircraft::engine_t get_engine(void) const { return m_engine; }
		ConditionAircraft::navspec_t get_navspec(void) const { return m_navspec; }
		ConditionAircraft::vertsep_t get_vertsep(void) const { return m_vertsep; }
		const std::string& get_icaotype(void) const { return m_icaotype; }
		unsigned int get_engines(void) const { return m_engines; }

	protected:
		static const std::string elementname;
		std::string m_icaotype;
		unsigned int m_engines;
		ConditionAircraft::acfttype_t m_acfttype;
		ConditionAircraft::engine_t m_engine;
		ConditionAircraft::navspec_t m_navspec;
		ConditionAircraft::vertsep_t m_vertsep;
	};

	class NodeAIXMFlightCharacteristic : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMFlightCharacteristic> ptr_t;

		NodeAIXMFlightCharacteristic(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMFlightCharacteristic(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		ConditionFlight::civmil_t get_civmil(void) const { return m_civmil; }
		ConditionFlight::purpose_t get_purpose(void) const { return m_purpose; }

	protected:
		static const std::string elementname;
		ConditionFlight::civmil_t m_civmil;
		ConditionFlight::purpose_t m_purpose;
	};

	class NodeAIXMFlightConditionFlight : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMFlightConditionFlight> ptr_t;

		NodeAIXMFlightConditionFlight(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMFlightConditionFlight(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMFlightCharacteristic>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		ConditionFlight::civmil_t get_civmil(void) const { return m_civmil; }
		ConditionFlight::purpose_t get_purpose(void) const { return m_purpose; }

	protected:
		static const std::string elementname;
		ConditionFlight::civmil_t m_civmil;
		ConditionFlight::purpose_t m_purpose;
	};

	class NodeAIXMFlightConditionOperand : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMFlightConditionOperand> ptr_t;

		NodeAIXMFlightConditionOperand(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMFlightConditionOperand(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMFlightConditionCombination>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const Condition::ptr_t& get_cond(void) const { return m_cond; }

	protected:
		static const std::string elementname;
		Condition::ptr_t m_cond;
	};

	class NodeAIXMFlightConditionStandardInstrumentArrivalCondition : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMFlightConditionStandardInstrumentArrivalCondition> ptr_t;

		NodeAIXMFlightConditionStandardInstrumentArrivalCondition(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMFlightConditionStandardInstrumentArrivalCondition(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMDirectFlightClass>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

	protected:
		static const std::string elementname;
	};

	class NodeAIXMFlightConditionStandardInstrumentDepartureCondition : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMFlightConditionStandardInstrumentDepartureCondition> ptr_t;

		NodeAIXMFlightConditionStandardInstrumentDepartureCondition(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMFlightConditionStandardInstrumentDepartureCondition(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMDirectFlightClass>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

	protected:
		static const std::string elementname;
	};

	class NodeAIXMFlightConditionRoutePortionCondition : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMFlightConditionRoutePortionCondition> ptr_t;

		NodeAIXMFlightConditionRoutePortionCondition(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMFlightConditionRoutePortionCondition(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMRoutePortion>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const UUID& get_route(void) const { return m_route; }
		const UUID& get_start(void) const { return m_start; }
		const UUID& get_end(void) const { return m_end; }

	protected:
		static const std::string elementname;
		UUID m_route;
		UUID m_start;
		UUID m_end;
	};

	class NodeAIXMFlightConditionDirectFlightCondition : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMFlightConditionDirectFlightCondition> ptr_t;

		NodeAIXMFlightConditionDirectFlightCondition(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMFlightConditionDirectFlightCondition(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMDirectFlightClass>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMDirectFlightSegment>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const UUID& get_start(void) const { return m_start; }
		const UUID& get_end(void) const { return m_end; }
		double get_length(void) const { return m_length; }

	protected:
		static const std::string elementname;
		UUID m_start;
		UUID m_end;
		double m_length;
	};

	class NodeADRAirspaceBorderCrossingObject : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeADRAirspaceBorderCrossingObject> ptr_t;

		NodeADRAirspaceBorderCrossingObject(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeADRAirspaceBorderCrossingObject(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeLink::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const UUID& get_start(void) const { return m_start; }
		const UUID& get_end(void) const { return m_end; }

	protected:
		static const std::string elementname;
		UUID m_start;
		UUID m_end;
	};

	class NodeADRBorderCrossingCondition : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeADRBorderCrossingCondition> ptr_t;

		NodeADRBorderCrossingCondition(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeADRBorderCrossingCondition(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeADRAirspaceBorderCrossingObject>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const UUID& get_start(void) const { return m_start; }
		const UUID& get_end(void) const { return m_end; }

	protected:
		static const std::string elementname;
		UUID m_start;
		UUID m_end;
	};

	class NodeADRFlightConditionElementExtension : public NodeAIXMExtensionBase {
	public:
		typedef boost::intrusive_ptr<NodeADRFlightConditionElementExtension> ptr_t;

		NodeADRFlightConditionElementExtension(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeADRFlightConditionElementExtension(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeADRBorderCrossingCondition>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const UUID& get_start(void) const { return m_start; }
		const UUID& get_end(void) const { return m_end; }

	protected:
		static const std::string elementname;
		UUID m_start;
		UUID m_end;
	};

	class NodeAIXMFlightConditionElement : public NodeAIXMElementBase {
	public:
		typedef boost::intrusive_ptr<NodeAIXMFlightConditionElement> ptr_t;

		NodeAIXMFlightConditionElement(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMFlightConditionElement(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual void on_subelement(const NodeLink::ptr_t& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMFlightConditionOperand>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMFlightConditionDirectFlightCondition>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMFlightConditionAircraft>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMFlightConditionRoutePortionCondition>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMFlightConditionFlight>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMOperationalCondition>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMFlightLevel>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMExtension>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		unsigned int get_index(void) const { return m_index; }
		const Condition::ptr_t& get_cond(void);

		bool is_refloc(void) const { return m_refloc; }
		NodeAIXMFlightConditionCircumstance::locrel_t get_locrel(void) const { return m_locrel; }

	protected:
		static const std::string elementname;
		AltRange m_altrange;
		UUID m_uuid;
		UUID m_start;
		UUID m_end;
		Condition::ptr_t m_cond;
		double m_length;
		unsigned int m_index;
		bool m_refloc;
		NodeAIXMFlightConditionCircumstance::locrel_t m_locrel;
		Condition::type_t m_type;
		bool m_arr;
		std::string m_icaotype;
		unsigned int m_engines;
		ConditionAircraft::acfttype_t m_acfttype;
		ConditionAircraft::engine_t m_engine;
		ConditionAircraft::navspec_t m_navspec;
		ConditionAircraft::vertsep_t m_vertsep;
		ConditionFlight::civmil_t m_civmil;
		ConditionFlight::purpose_t m_purpose;
	};

	class NodeAIXMElement : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMElement> ptr_t;

		NodeAIXMElement(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMElement(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMElementBase>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const boost::intrusive_ptr<NodeAIXMElementBase>& get_el(void) const { return m_el; }
		template<typename T> boost::intrusive_ptr<T> get(void) const { return boost::dynamic_pointer_cast<T>(get_el()); }

	protected:
		static const std::string elementname;
		boost::intrusive_ptr<NodeAIXMElementBase> m_el;
 	};

	class NodeAIXMFlightConditionCombination : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMFlightConditionCombination> ptr_t;

		NodeAIXMFlightConditionCombination(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMFlightConditionCombination(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMElement>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMExtension>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		Condition::ptr_t get_cond(void) const;
		const TimeTable& get_timetable(void) const { return m_timetable; }

	protected:
		static const std::string elementname;
		TimeTable m_timetable;
		typedef std::pair<Condition::ptr_t,unsigned int> element_t;
		typedef std::vector<element_t> elements_t;
		elements_t m_elements;
		typedef enum {
			operator_and,
			operator_andnot,
			operator_or,
			operator_seq,
			operator_invalid
		} operator_t;
		operator_t m_operator;

		operator_t get_operator(void) const { return m_operator; }
	};

	class NodeAIXMFlight : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMFlight> ptr_t;

		NodeAIXMFlight(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMFlight(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMFlightConditionCombination>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const Condition::ptr_t& get_cond(void) const { return m_cond; }
		const TimeTable& get_timetable(void) const { return m_timetable; }

	protected:
		static const std::string elementname;
		Condition::ptr_t m_cond;
		TimeTable m_timetable;
	};

	class NodeADRFlightRestrictionExtension : public NodeAIXMExtensionBase {
	public:
		typedef boost::intrusive_ptr<NodeADRFlightRestrictionExtension> ptr_t;

		NodeADRFlightRestrictionExtension(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeADRFlightRestrictionExtension(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		bool is_enabled(void) const { return m_enabled; }
		FlightRestrictionTimeSlice::procind_t get_procind(void) const { return m_procind; }

	protected:
		static const std::string elementname;
		bool m_enabled;
		FlightRestrictionTimeSlice::procind_t m_procind;
	};

	class NodeAIXMAnyTimeSlice : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMAnyTimeSlice> ptr_t;

		NodeAIXMAnyTimeSlice(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual void on_subelement(const NodeGMLValidTime::ptr_t& el);
		virtual void on_subelement(const NodeAIXMFeatureLifetime::ptr_t& el);

		typedef enum {
			interpretation_baseline,
			interpretation_permdelta,
			interpretation_tempdelta,
			interpretation_invalid
		} interpretation_t;

		interpretation_t get_interpretation(void) const { return m_interpretation; }
		long get_validstart(void) const { return m_validstart; }
		long get_validend(void) const { return m_validend; }
		long get_featurestart(void) const { return m_featurestart; }
		long get_featureend(void) const { return m_featureend; }

	protected:
		long m_validstart;
		long m_validend;
		long m_featurestart;
		long m_featureend;
		interpretation_t m_interpretation;

		virtual TimeSlice& get_tsbase(void) { return *(TimeSlice *)0; }
	};

	class NodeAIXMAirportHeliportTimeSlice : public NodeAIXMAnyTimeSlice {
	public:
		typedef boost::intrusive_ptr<NodeAIXMAirportHeliportTimeSlice> ptr_t;

		NodeAIXMAirportHeliportTimeSlice(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMAirportHeliportTimeSlice(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual void on_subelement(const NodeAIXMServedCity::ptr_t& el);
		virtual void on_subelement(const NodeAIXMARP::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const AirportTimeSlice& get_timeslice(void) const { return m_timeslice; }

	protected:
		static const std::string elementname;
		AirportTimeSlice m_timeslice;

		virtual TimeSlice& get_tsbase(void) { return m_timeslice; }
	};

	class NodeAIXMAirportHeliportCollocationTimeSlice : public NodeAIXMAnyTimeSlice {
	public:
		typedef boost::intrusive_ptr<NodeAIXMAirportHeliportCollocationTimeSlice> ptr_t;

		NodeAIXMAirportHeliportCollocationTimeSlice(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMAirportHeliportCollocationTimeSlice(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeLink::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const AirportCollocationTimeSlice& get_timeslice(void) const { return m_timeslice; }

	protected:
		static const std::string elementname;
		AirportCollocationTimeSlice m_timeslice;

		virtual TimeSlice& get_tsbase(void) { return m_timeslice; }
	};

	class NodeAIXMDesignatedPointTimeSlice : public NodeAIXMAnyTimeSlice {
	public:
		typedef boost::intrusive_ptr<NodeAIXMDesignatedPointTimeSlice> ptr_t;

		NodeAIXMDesignatedPointTimeSlice(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMDesignatedPointTimeSlice(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual void on_subelement(const NodeAIXMLocation::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const DesignatedPointTimeSlice& get_timeslice(void) const { return m_timeslice; }

	protected:
		static const std::string elementname;
		DesignatedPointTimeSlice m_timeslice;

		virtual TimeSlice& get_tsbase(void) { return m_timeslice; }
	};

	class NodeAIXMNavaidTimeSlice : public NodeAIXMAnyTimeSlice {
	public:
		typedef boost::intrusive_ptr<NodeAIXMNavaidTimeSlice> ptr_t;

		NodeAIXMNavaidTimeSlice(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMNavaidTimeSlice(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual void on_subelement(const NodeAIXMLocation::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const NavaidTimeSlice& get_timeslice(void) const { return m_timeslice; }

	protected:
		static const std::string elementname;
		NavaidTimeSlice m_timeslice;

		virtual TimeSlice& get_tsbase(void) { return m_timeslice; }
	};

	class NodeAIXMAngleIndicationTimeSlice : public NodeAIXMAnyTimeSlice {
	public:
		typedef boost::intrusive_ptr<NodeAIXMAngleIndicationTimeSlice> ptr_t;

		NodeAIXMAngleIndicationTimeSlice(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMAngleIndicationTimeSlice(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual void on_subelement(const NodeLink::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const AngleIndicationTimeSlice& get_timeslice(void) const { return m_timeslice; }

	protected:
		static const std::string elementname;
		AngleIndicationTimeSlice m_timeslice;

		virtual TimeSlice& get_tsbase(void) { return m_timeslice; }
	};

	class NodeAIXMDistanceIndicationTimeSlice : public NodeAIXMAnyTimeSlice {
	public:
		typedef boost::intrusive_ptr<NodeAIXMDistanceIndicationTimeSlice> ptr_t;

		NodeAIXMDistanceIndicationTimeSlice(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMDistanceIndicationTimeSlice(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual void on_subelement(const NodeLink::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const DistanceIndicationTimeSlice& get_timeslice(void) const { return m_timeslice; }

	protected:
		static const std::string elementname;
		DistanceIndicationTimeSlice m_timeslice;

		virtual TimeSlice& get_tsbase(void) { return m_timeslice; }
	};

	class NodeAIXMAirspaceTimeSlice : public NodeAIXMAnyTimeSlice {
	public:
		typedef boost::intrusive_ptr<NodeAIXMAirspaceTimeSlice> ptr_t;

		NodeAIXMAirspaceTimeSlice(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMAirspaceTimeSlice(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeText::ptr_t& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMGeometryComponent>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMActivation>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMExtension>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const AirspaceTimeSlice& get_timeslice(void) const;

	protected:
		static const std::string elementname;
		AirspaceTimeSlice m_timeslice;

		virtual TimeSlice& get_tsbase(void) { return m_timeslice; }
	};

	class NodeAIXMStandardLevelTableTimeSlice : public NodeAIXMAnyTimeSlice {
	public:
		typedef boost::intrusive_ptr<NodeAIXMStandardLevelTableTimeSlice> ptr_t;

		NodeAIXMStandardLevelTableTimeSlice(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMStandardLevelTableTimeSlice(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeText>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const StandardLevelTableTimeSlice& get_timeslice(void) const { return m_timeslice; }

	protected:
		static const std::string elementname;
		StandardLevelTableTimeSlice m_timeslice;

		virtual TimeSlice& get_tsbase(void) { return m_timeslice; }
	};

	class NodeAIXMStandardLevelColumnTimeSlice : public NodeAIXMAnyTimeSlice {
	public:
		typedef boost::intrusive_ptr<NodeAIXMStandardLevelColumnTimeSlice> ptr_t;

		NodeAIXMStandardLevelColumnTimeSlice(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMStandardLevelColumnTimeSlice(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeText>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeLink>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const StandardLevelColumnTimeSlice& get_timeslice(void) const { return m_timeslice; }

	protected:
		static const std::string elementname;
		StandardLevelColumnTimeSlice m_timeslice;

		virtual TimeSlice& get_tsbase(void) { return m_timeslice; }
	};

	class NodeAIXMRouteTimeSlice : public NodeAIXMAnyTimeSlice {
	public:
		typedef boost::intrusive_ptr<NodeAIXMRouteTimeSlice> ptr_t;

		NodeAIXMRouteTimeSlice(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMRouteTimeSlice(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeText>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const RouteTimeSlice& get_timeslice(void) const { return m_timeslice; }

	protected:
		static const std::string elementname;
		RouteTimeSlice m_timeslice;
		std::string m_desigprefix;
		std::string m_desigsecondletter;
		std::string m_designumber;
		std::string m_multiid;

		virtual TimeSlice& get_tsbase(void) { return m_timeslice; }
	};

	class NodeAIXMRouteSegmentTimeSlice : public NodeAIXMAnyTimeSlice {
	public:
		typedef boost::intrusive_ptr<NodeAIXMRouteSegmentTimeSlice> ptr_t;

		NodeAIXMRouteSegmentTimeSlice(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMRouteSegmentTimeSlice(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeText>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeLink>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMStart>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMEnd>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMAvailability>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMExtension>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const RouteSegmentTimeSlice& get_timeslice(void) const { return m_timeslice; }

	protected:
		static const std::string elementname;
		RouteSegmentTimeSlice m_timeslice;

		virtual TimeSlice& get_tsbase(void) { return m_timeslice; }
	};

	class NodeAIXMStandardInstrumentDepartureTimeSlice : public NodeAIXMAnyTimeSlice {
	public:
		typedef boost::intrusive_ptr<NodeAIXMStandardInstrumentDepartureTimeSlice> ptr_t;

		NodeAIXMStandardInstrumentDepartureTimeSlice(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMStandardInstrumentDepartureTimeSlice(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeText>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeLink>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMAvailability>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMExtension>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const StandardInstrumentDepartureTimeSlice& get_timeslice(void) const { return m_timeslice; }

	protected:
		static const std::string elementname;
		StandardInstrumentDepartureTimeSlice m_timeslice;

		virtual TimeSlice& get_tsbase(void) { return m_timeslice; }
	};

	class NodeAIXMStandardInstrumentArrivalTimeSlice : public NodeAIXMAnyTimeSlice {
	public:
		typedef boost::intrusive_ptr<NodeAIXMStandardInstrumentArrivalTimeSlice> ptr_t;

		NodeAIXMStandardInstrumentArrivalTimeSlice(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMStandardInstrumentArrivalTimeSlice(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeText>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeLink>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMAvailability>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMExtension>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const StandardInstrumentArrivalTimeSlice& get_timeslice(void) const { return m_timeslice; }

	protected:
		static const std::string elementname;
		StandardInstrumentArrivalTimeSlice m_timeslice;

		virtual TimeSlice& get_tsbase(void) { return m_timeslice; }
	};

	class NodeAIXMDepartureLegTimeSlice : public NodeAIXMAnyTimeSlice {
	public:
		typedef boost::intrusive_ptr<NodeAIXMDepartureLegTimeSlice> ptr_t;

		NodeAIXMDepartureLegTimeSlice(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMDepartureLegTimeSlice(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeText>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeLink>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMStartPoint>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMEndPoint>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const DepartureLegTimeSlice& get_timeslice(void) const { return m_timeslice; }

	protected:
		static const std::string elementname;
		DepartureLegTimeSlice m_timeslice;

		virtual TimeSlice& get_tsbase(void) { return m_timeslice; }
	};

	class NodeAIXMArrivalLegTimeSlice : public NodeAIXMAnyTimeSlice {
	public:
		typedef boost::intrusive_ptr<NodeAIXMArrivalLegTimeSlice> ptr_t;

		NodeAIXMArrivalLegTimeSlice(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMArrivalLegTimeSlice(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeText>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeLink>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMStartPoint>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMEndPoint>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const ArrivalLegTimeSlice& get_timeslice(void) const { return m_timeslice; }

	protected:
		static const std::string elementname;
		ArrivalLegTimeSlice m_timeslice;

		virtual TimeSlice& get_tsbase(void) { return m_timeslice; }
	};

	class NodeAIXMOrganisationAuthorityTimeSlice : public NodeAIXMAnyTimeSlice {
	public:
		typedef boost::intrusive_ptr<NodeAIXMOrganisationAuthorityTimeSlice> ptr_t;

		NodeAIXMOrganisationAuthorityTimeSlice(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMOrganisationAuthorityTimeSlice(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeText>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const OrganisationAuthorityTimeSlice& get_timeslice(void) const { return m_timeslice; }

	protected:
		static const std::string elementname;
		OrganisationAuthorityTimeSlice m_timeslice;

		virtual TimeSlice& get_tsbase(void) { return m_timeslice; }
	};

	class NodeAIXMSpecialDateTimeSlice : public NodeAIXMAnyTimeSlice {
	public:
		typedef boost::intrusive_ptr<NodeAIXMSpecialDateTimeSlice> ptr_t;

		NodeAIXMSpecialDateTimeSlice(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMSpecialDateTimeSlice(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeText>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeLink>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const SpecialDateTimeSlice& get_timeslice(void) const { return m_timeslice; }

	protected:
		static const std::string elementname;
		SpecialDateTimeSlice m_timeslice;

		virtual TimeSlice& get_tsbase(void) { return m_timeslice; }
	};

	class NodeAIXMUnitTimeSlice : public NodeAIXMAnyTimeSlice {
	public:
		typedef boost::intrusive_ptr<NodeAIXMUnitTimeSlice> ptr_t;

		NodeAIXMUnitTimeSlice(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMUnitTimeSlice(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeText>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const UnitTimeSlice& get_timeslice(void) const { return m_timeslice; }

	protected:
		static const std::string elementname;
		UnitTimeSlice m_timeslice;

		virtual TimeSlice& get_tsbase(void) { return m_timeslice; }
	};

	class NodeAIXMAirTrafficManagementServiceTimeSlice : public NodeAIXMAnyTimeSlice {
	public:
		typedef boost::intrusive_ptr<NodeAIXMAirTrafficManagementServiceTimeSlice> ptr_t;

	        NodeAIXMAirTrafficManagementServiceTimeSlice(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMAirTrafficManagementServiceTimeSlice(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeLink>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMClientRoute>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const AirTrafficManagementServiceTimeSlice& get_timeslice(void) const { return m_timeslice; }

	protected:
		static const std::string elementname;
		AirTrafficManagementServiceTimeSlice m_timeslice;

                virtual TimeSlice& get_tsbase(void) { return m_timeslice; }
	};

	class NodeAIXMFlightRestrictionTimeSlice : public NodeAIXMAnyTimeSlice {
	public:
		typedef boost::intrusive_ptr<NodeAIXMFlightRestrictionTimeSlice> ptr_t;

	        NodeAIXMFlightRestrictionTimeSlice(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMFlightRestrictionTimeSlice(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const boost::intrusive_ptr<NodeText>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMFlight>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMRegulatedRoute>& el);
		virtual void on_subelement(const boost::intrusive_ptr<NodeAIXMExtension>& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		const FlightRestrictionTimeSlice& get_timeslice(void) const { return m_timeslice; }

	protected:
		static const std::string elementname;
		FlightRestrictionTimeSlice m_timeslice;

		virtual TimeSlice& get_tsbase(void) { return m_timeslice; }
	};

	class NodeAIXMTimeSlice : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMTimeSlice> ptr_t;

		NodeAIXMTimeSlice(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMTimeSlice(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeAIXMAnyTimeSlice::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		NodeAIXMAnyTimeSlice::ptr_t operator[](unsigned int i) { return m_nodes[i]; }
		unsigned int size(void) const { return m_nodes.size(); }
		bool empty(void) const { return m_nodes.empty(); }

	protected:
		static const std::string elementname;
		typedef std::vector<NodeAIXMAnyTimeSlice::ptr_t> nodes_t;
		nodes_t m_nodes;
	};

	class NodeAIXMObject : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeAIXMObject> ptr_t;

		NodeAIXMObject(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);

		virtual void on_subelement(const NodeText::ptr_t& el);

	protected:
		UUID m_uuid;

		template<typename T> void fake_uuid(const T& el);
		template<typename BT, typename TS> void on_timeslice(const NodeAIXMTimeSlice::ptr_t& el);
	};

	class NodeAIXMAirportHeliport : public NodeAIXMObject {
	public:
		typedef boost::intrusive_ptr<NodeAIXMAirportHeliport> ptr_t;

		NodeAIXMAirportHeliport(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMAirportHeliport(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeAIXMTimeSlice::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

	protected:
		static const std::string elementname;
	};

	class NodeAIXMAirportHeliportCollocation : public NodeAIXMObject {
	public:
		typedef boost::intrusive_ptr<NodeAIXMAirportHeliportCollocation> ptr_t;

		NodeAIXMAirportHeliportCollocation(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMAirportHeliportCollocation(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeAIXMTimeSlice::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

	protected:
		static const std::string elementname;
	};

	class NodeAIXMDesignatedPoint : public NodeAIXMObject {
	public:
		typedef boost::intrusive_ptr<NodeAIXMDesignatedPoint> ptr_t;

		NodeAIXMDesignatedPoint(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMDesignatedPoint(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeAIXMTimeSlice::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

	protected:
		static const std::string elementname;
	};

	class NodeAIXMNavaid : public NodeAIXMObject {
	public:
		typedef boost::intrusive_ptr<NodeAIXMNavaid> ptr_t;

		NodeAIXMNavaid(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMNavaid(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeAIXMTimeSlice::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

	protected:
		static const std::string elementname;
	};

	class NodeAIXMAngleIndication : public NodeAIXMObject {
	public:
		typedef boost::intrusive_ptr<NodeAIXMAngleIndication> ptr_t;

		NodeAIXMAngleIndication(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMAngleIndication(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeAIXMTimeSlice::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

	protected:
		static const std::string elementname;
	};

	class NodeAIXMDistanceIndication : public NodeAIXMObject {
	public:
		typedef boost::intrusive_ptr<NodeAIXMDistanceIndication> ptr_t;

		NodeAIXMDistanceIndication(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMDistanceIndication(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeAIXMTimeSlice::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

	protected:
		static const std::string elementname;
	};

	class NodeAIXMAirspace : public NodeAIXMObject {
	public:
		typedef boost::intrusive_ptr<NodeAIXMAirspace> ptr_t;

		NodeAIXMAirspace(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMAirspace(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeAIXMTimeSlice::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

	protected:
		static const std::string elementname;
	};

	class NodeAIXMStandardLevelTable : public NodeAIXMObject {
	public:
		typedef boost::intrusive_ptr<NodeAIXMStandardLevelTable> ptr_t;

		NodeAIXMStandardLevelTable(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMStandardLevelTable(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeAIXMTimeSlice::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

	protected:
		static const std::string elementname;
	};

	class NodeAIXMStandardLevelColumn : public NodeAIXMObject {
	public:
		typedef boost::intrusive_ptr<NodeAIXMStandardLevelColumn> ptr_t;

		NodeAIXMStandardLevelColumn(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMStandardLevelColumn(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeAIXMTimeSlice::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

	protected:
		static const std::string elementname;
	};

	class NodeAIXMRoute : public NodeAIXMObject {
	public:
		typedef boost::intrusive_ptr<NodeAIXMRoute> ptr_t;

		NodeAIXMRoute(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMRoute(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeAIXMTimeSlice::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

	protected:
		static const std::string elementname;
	};

	class NodeAIXMRouteSegment : public NodeAIXMObject {
	public:
		typedef boost::intrusive_ptr<NodeAIXMRouteSegment> ptr_t;

		NodeAIXMRouteSegment(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMRouteSegment(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeAIXMTimeSlice::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

	protected:
		static const std::string elementname;
	};

	class NodeAIXMStandardInstrumentDeparture : public NodeAIXMObject {
	public:
		typedef boost::intrusive_ptr<NodeAIXMStandardInstrumentDeparture> ptr_t;

		NodeAIXMStandardInstrumentDeparture(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMStandardInstrumentDeparture(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeAIXMTimeSlice::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

	protected:
		static const std::string elementname;
	};

	class NodeAIXMStandardInstrumentArrival : public NodeAIXMObject {
	public:
		typedef boost::intrusive_ptr<NodeAIXMStandardInstrumentArrival> ptr_t;

		NodeAIXMStandardInstrumentArrival(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMStandardInstrumentArrival(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeAIXMTimeSlice::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

	protected:
		static const std::string elementname;
	};

	class NodeAIXMDepartureLeg : public NodeAIXMObject {
	public:
		typedef boost::intrusive_ptr<NodeAIXMDepartureLeg> ptr_t;

		NodeAIXMDepartureLeg(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMDepartureLeg(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeAIXMTimeSlice::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

	protected:
		static const std::string elementname;
	};

	class NodeAIXMArrivalLeg : public NodeAIXMObject {
	public:
		typedef boost::intrusive_ptr<NodeAIXMArrivalLeg> ptr_t;

		NodeAIXMArrivalLeg(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMArrivalLeg(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeAIXMTimeSlice::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

	protected:
		static const std::string elementname;
	};

	class NodeAIXMOrganisationAuthority : public NodeAIXMObject {
	public:
		typedef boost::intrusive_ptr<NodeAIXMOrganisationAuthority> ptr_t;

		NodeAIXMOrganisationAuthority(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMOrganisationAuthority(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeAIXMTimeSlice::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

	protected:
		static const std::string elementname;
	};

	class NodeAIXMSpecialDate : public NodeAIXMObject {
	public:
		typedef boost::intrusive_ptr<NodeAIXMSpecialDate> ptr_t;

		NodeAIXMSpecialDate(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMSpecialDate(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeAIXMTimeSlice::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

	protected:
		static const std::string elementname;
	};

	class NodeAIXMUnit : public NodeAIXMObject {
	public:
		typedef boost::intrusive_ptr<NodeAIXMUnit> ptr_t;

		NodeAIXMUnit(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMUnit(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeAIXMTimeSlice::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

	protected:
		static const std::string elementname;
	};

	class NodeAIXMAirTrafficManagementService : public NodeAIXMObject {
	public:
		typedef boost::intrusive_ptr<NodeAIXMAirTrafficManagementService> ptr_t;

		NodeAIXMAirTrafficManagementService(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMAirTrafficManagementService(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeAIXMTimeSlice::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

	protected:
		static const std::string elementname;
	};

	class NodeAIXMFlightRestriction : public NodeAIXMObject {
	public:
		typedef boost::intrusive_ptr<NodeAIXMFlightRestriction> ptr_t;

		NodeAIXMFlightRestriction(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeAIXMFlightRestriction(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeAIXMTimeSlice::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

	protected:
		static const std::string elementname;
	};

	class NodeADRMessageMember : public NodeNoIgnore {
	public:
		typedef boost::intrusive_ptr<NodeADRMessageMember> ptr_t;

		NodeADRMessageMember(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);
		static Node::ptr_t create(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties) {
			return Node::ptr_t(new NodeADRMessageMember(parser, tagname, childnum, properties));
		}

		virtual void on_subelement(const NodeAIXMObject::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

		NodeAIXMObject::ptr_t get_obj(void) const { return m_obj; }

	protected:
		static const std::string elementname;
		NodeAIXMObject::ptr_t m_obj;
	};

	class NodeADRMessage : public Node {
	public:
		typedef boost::intrusive_ptr<NodeADRMessage> ptr_t;

		NodeADRMessage(ParseXML& parser, const std::string& tagname, unsigned int childnum, const AttributeList& properties);

		virtual void on_subelement(const NodeADRMessageMember::ptr_t& el);
		virtual const std::string& get_element_name(void) const { return elementname; }
		static const std::string& get_static_element_name(void) { return elementname; }

	protected:
		static const std::string elementname;
	};

	virtual void on_start_document(void);
	virtual void on_end_document(void);
	virtual void on_start_element(const Glib::ustring& name, const AttributeList& properties);
	virtual void on_end_element(const Glib::ustring& name);
	virtual void on_characters(const Glib::ustring& characters);
	virtual void on_comment(const Glib::ustring& text);
	virtual void on_warning(const Glib::ustring& text);
	virtual void on_error(const Glib::ustring& text);
	virtual void on_fatal_error(const Glib::ustring& text);

	static const bool tracestack = false;

	Database& m_db;

	typedef Node::ptr_t (*factoryfunc_t)(ParseXML&, const std::string&, unsigned int, const AttributeList&);
	typedef std::map<std::string,factoryfunc_t> factory_t;
	factory_t m_factory;

	typedef std::map<std::string,std::string> namespacemap_t;
	namespacemap_t m_namespacemap;
	namespacemap_t m_namespaceshort;

	typedef std::vector<Node::ptr_t > parsestack_t;
	parsestack_t m_parsestack;

	unsigned int m_errorcnt;
	unsigned int m_warncnt;

	bool m_verbose;
};

};

#endif /* ADRPARSE_H */
