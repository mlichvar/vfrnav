//
// C++ Implementation: osm2sdb
//
// Description: Convert OpenStreetMap static files to proprietary database files
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "sysdeps.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>
#include <exception>
#include <glibmm.h>
#include <dirent.h>
#include <glib/gstdio.h>

#if defined(HAVE_GDAL)
#if defined(HAVE_GDAL2)
#include <gdal.h>
#include <ogr_geometry.h>
#include <ogr_feature.h>
#include <ogrsf_frmts.h>
#else
#include <ogrsf_frmts.h>
#endif
#endif

#ifdef HAVE_SYSEXITS_H
#include <sysexits.h>
#else
#define EX_USAGE   64
#define EX_OK      0
#endif

#include <getopt.h>

#include "hibernate.h"
#include "geom.h"
#include "osmsdb.h"

static void usage(void)
{
	std::cerr << "usage: osm2sdb [-d <dir>] [-o <osm.sdb>]" << std::endl;
}

int main(int argc, char *argv[])
{
	try {
		std::string outputname("xx.sdb");
		OSMStaticDB::ImportSourceFiles filenames;
		OSMStaticDB::importflags_t flags(OSMStaticDB::importflags_t::none);
		enum class mode_t : uint8_t {
			info,
			import,
			reindex,
			removeindex,
			statistics,
			printindex
		};
		mode_t mode(mode_t::import);
		OSMStaticDB::layer_t layer(OSMStaticDB::layer_t::first);
		// R* index parameters
		size_t max_elements = OSMStaticDB::rtreedefaultmaxelperpage;
		size_t min_elements = std::numeric_limits<size_t>::max();
		size_t reinserted_elements = std::numeric_limits<size_t>::max();
		size_t overlap_cost_threshold = 32;
		{
			static struct option long_options[] = {
				{ "output",                             required_argument, 0, 'o' },
				{ "directory",                          required_argument, 0, 'd' },
				{ "keep",                               no_argument,       0, 'k' },
				{ "noindex",                            no_argument,       0, 'I' },
				{ "info",                               no_argument,       0, 'i' },
				{ "admin-boundary-file",                required_argument, 0, 0x400 + static_cast<unsigned int>(OSMStaticDB::layer_t::boundarylinesland) },
				{ "antarctica-polygons-file",           required_argument, 0, 0x400 + static_cast<unsigned int>(OSMStaticDB::layer_t::icesheetpolygons) },
				{ "antarctica-outlines-file",           required_argument, 0, 0x400 + static_cast<unsigned int>(OSMStaticDB::layer_t::icesheetoutlines) },
				{ "water-polygons-file",                required_argument, 0, 0x400 + static_cast<unsigned int>(OSMStaticDB::layer_t::waterpolygons) },
				{ "simple-water-polygons-file",         required_argument, 0, 0x400 + static_cast<unsigned int>(OSMStaticDB::layer_t::simplifiedwaterpolygons) },
				{ "max_elements",                       required_argument, 0, 0x420 },
				{ "min_elements",                       required_argument, 0, 0x421 },
				{ "reinserted_elements",                required_argument, 0, 0x422 },
				{ "overlap_cost_threshold",             required_argument, 0, 0x423 },
				{ "reindex",                            no_argument,       0, 0x430 + static_cast<unsigned int>(mode_t::reindex) },
				{ "removeindex",                        no_argument,       0, 0x430 + static_cast<unsigned int>(mode_t::removeindex) },
				{ "statistics",                         no_argument,       0, 0x430 + static_cast<unsigned int>(mode_t::statistics) },
				{ "printindex-boundarylinesland",       no_argument,       0, 0x440 + static_cast<unsigned int>(OSMStaticDB::layer_t::boundarylinesland) },
				{ "printindex-icesheetpolygons",        no_argument,       0, 0x440 + static_cast<unsigned int>(OSMStaticDB::layer_t::icesheetpolygons) },
				{ "printindex-icesheetoutlines",        no_argument,       0, 0x440 + static_cast<unsigned int>(OSMStaticDB::layer_t::icesheetoutlines) },
				{ "printindex-waterpolygons",           no_argument,       0, 0x440 + static_cast<unsigned int>(OSMStaticDB::layer_t::waterpolygons) },
				{ "printindex-simplifiedwaterpolygons", no_argument,       0, 0x440 + static_cast<unsigned int>(OSMStaticDB::layer_t::simplifiedwaterpolygons) },
				{0, 0, 0, 0}
			};
			int c, err(0);
			std::string stylef;

			while ((c = getopt_long(argc, argv, "o:d:kIi", long_options, 0)) != EOF) {
				switch (c) {
				case 'o':
					if (optarg)
						outputname = optarg;
					break;

				case 'd':
					if (optarg)
						filenames.set_default(optarg);
					break;

				case 'k':
		        		flags |= OSMStaticDB::importflags_t::keep;
					break;

				case 'I':
					flags |= OSMStaticDB::importflags_t::noindex;
					break;

				case 'i':
					mode = mode_t::info;
					break;

				case 0x400 + static_cast<unsigned int>(OSMStaticDB::layer_t::boundarylinesland):
				case 0x400 + static_cast<unsigned int>(OSMStaticDB::layer_t::icesheetpolygons):
				case 0x400 + static_cast<unsigned int>(OSMStaticDB::layer_t::icesheetoutlines):
				case 0x400 + static_cast<unsigned int>(OSMStaticDB::layer_t::waterpolygons):
				case 0x400 + static_cast<unsigned int>(OSMStaticDB::layer_t::simplifiedwaterpolygons):
					if (optarg)
						filenames.set_file(static_cast<OSMStaticDB::layer_t>(c - 0x400), optarg);
					break;

				case 0x420:
					if (optarg)
						max_elements = strtoul(optarg, 0, 0);
					break;

				case 0x421:
					if (optarg)
						min_elements = strtoul(optarg, 0, 0);
					break;

				case 0x422:
					if (optarg)
						reinserted_elements = strtoul(optarg, 0, 0);
					break;

				case 0x423:
					if (optarg)
						overlap_cost_threshold = strtoul(optarg, 0, 0);
					break;

				case 0x430 + static_cast<unsigned int>(mode_t::reindex):
				case 0x430 + static_cast<unsigned int>(mode_t::removeindex):
				case 0x430 + static_cast<unsigned int>(mode_t::statistics):
					mode = static_cast<mode_t>(c - 0x430);
					break;

				case 0x440 + static_cast<unsigned int>(OSMStaticDB::layer_t::boundarylinesland):
				case 0x440 + static_cast<unsigned int>(OSMStaticDB::layer_t::icesheetpolygons):
				case 0x440 + static_cast<unsigned int>(OSMStaticDB::layer_t::icesheetoutlines):
				case 0x440 + static_cast<unsigned int>(OSMStaticDB::layer_t::waterpolygons):
				case 0x440 + static_cast<unsigned int>(OSMStaticDB::layer_t::simplifiedwaterpolygons):
					mode = mode_t::printindex;
					layer = static_cast<OSMStaticDB::layer_t>(c - 0x440);
					break;

				default:
					++err;
					break;
				}
			}
			if (err) {
				usage();
				return EX_USAGE;
			}
		}
		OGRRegisterAll();
		// cache directory
		std::string tmpdir;
		switch (mode) {
		default:
		case mode_t::import:
		case mode_t::reindex:			
		{
			gchar *tmpdir1(g_dir_make_tmp("osm2odb-XXXXXX", NULL));
			tmpdir = tmpdir1;
			g_free(tmpdir1);
		}

		case mode_t::info:
		case mode_t::removeindex:
		case mode_t::statistics:
		case mode_t::printindex:
			break;
		}
		switch (mode) {
		case mode_t::import:
		default:
			OSMStaticDB::import(tmpdir, outputname, filenames, flags);
			break;

		case mode_t::reindex:
			OSMStaticDB::reindex(tmpdir, outputname, max_elements, min_elements, reinserted_elements, overlap_cost_threshold);
			break;

		case mode_t::removeindex:
			OSMStaticDB::removeindex(outputname);
			break;

		case mode_t::statistics:
		{
			static const char * const objnames[5] = {
				"boundarylinesland:       ",
				"icesheetpolygons:        ",
				"icesheetoutlines:        ",
				"waterpolygons:           ",
				"simplifiedwaterpolygons: "
			};
			OSMStaticDB db(outputname);
			db.open();
			OSMStaticDB::Statistics st(db.get_statistics());
			std::cout << "Database:                " << outputname << std::endl
				  << "Tag Keys:                " << st.get_tagkeys() << std::endl
				  << "Tag Names:               " << st.get_tagnames() << std::endl;
			for (OSMStaticDB::layer_t l(OSMStaticDB::layer_t::first); l <= OSMStaticDB::layer_t::last;
			     l = static_cast<OSMStaticDB::layer_t>(static_cast<unsigned int>(l) + 1)) {
				std::cout << objnames[static_cast<unsigned int>(l)] << st.get_objects(l);
				const OSMStaticDB::Statistics::RTree& rt(st.get_rtree(l));
				if (rt.is_valid())
					std::cout << "  R-Tree: " << rt.to_str();
				std::cout << std::endl;
			}
			break;
		}

		case mode_t::info:
			for (OSMStaticDB::layer_t l(OSMStaticDB::layer_t::first); l <= OSMStaticDB::layer_t::last;
			     l = static_cast<OSMStaticDB::layer_t>(static_cast<unsigned int>(l) + 1))
				OSMStaticDB::shapeinfo(std::cout << "File \"" << filenames.get_file(l) << "\" for layer " << l << std::endl, filenames.get_file(l), 3);
			break;

		case mode_t::printindex:
		{
			OSMStaticDB db(outputname);
			db.open();
			db.print_index(std::cout << "Database: " << outputname << " " << layer << " Index" << std::endl, layer);
			break;
		}
		}
		if (!tmpdir.empty() && (flags & OSMStaticDB::importflags_t::keep) == OSMStaticDB::importflags_t::none)
			g_remove(tmpdir.c_str());
		return EX_OK;
	} catch (const std::exception& e) {
		std::cerr << e.what() << std::endl;
		return EX_USAGE;
	} catch (...) {
		std::cerr << "Unknown Error" << std::endl;
		return EX_USAGE;
	}
	return EX_OK;
}
