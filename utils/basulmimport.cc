//
// C++ Implementation: basulmimport
//
// Description: Import basulm gps files
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2007, 2009, 2012, 2013, 2018
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "sysdeps.h"

#include <libxml++/libxml++.h>
#include <sigc++/sigc++.h>
#include <unistd.h>
#include <getopt.h>
#include <iostream>
#include <sstream>
#include <limits>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <stdexcept>
#include <stdarg.h>
#include <cassert>
#if defined(HAVE_GDAL)
#if defined(HAVE_GDAL2)
#include <gdal.h>
#include <ogr_geometry.h>
#include <ogr_feature.h>
#include <ogrsf_frmts.h>
#else
#include <ogrsf_frmts.h>
#endif
#endif

#include "dbobj.h"

class DbXmlImporter : public xmlpp::SaxParser {
public:
	enum class filter_t {
		icao = 1,
		ulm  = 2
	};

	DbXmlImporter(const Glib::ustring& output_dir, bool pgsql, filter_t filter);
	virtual ~DbXmlImporter();

protected:
	virtual void on_start_document();
	virtual void on_end_document();
	virtual void on_start_element(const Glib::ustring& name,
				      const AttributeList& properties);
	virtual void on_end_element(const Glib::ustring& name);
	virtual void on_characters(const Glib::ustring& characters);
	virtual void on_comment(const Glib::ustring& text);
	virtual void on_warning(const Glib::ustring& text);
	virtual void on_error(const Glib::ustring& text);
	virtual void on_fatal_error(const Glib::ustring& text);

	void open_airports_db(void);

	void process_airport(void);

	static std::string& trimleft(std::string& s);
	static std::string& trimright(std::string& s);
	static std::string& trim(std::string& s);
	static Glib::ustring& trimleft(Glib::ustring& s);
	static Glib::ustring& trimright(Glib::ustring& s);
	static Glib::ustring& trim(Glib::ustring& s);
	static bool is_lf_icao(const std::string& id);
	static bool is_lf_ulm(const std::string& id);

private:
	typedef enum {
		state_document_c,
		state_gpx_c,
		state_wpt_c,
		state_ele_c,
		state_name_c,
		state_desc_c
	} state_t;
	state_t m_state;

	Glib::ustring m_outputdir;
	bool m_purgedb;
	filter_t m_filter;
#ifdef HAVE_PQXX
	typedef std::unique_ptr<pqxx::connection> pgconn_t;
	pgconn_t m_pgconn;
#endif
	std::unique_ptr<AirportsDbQueryInterface> m_airportsdb;
	time_t m_starttime;

	Point m_rec_point;
	Glib::ustring m_rec_ele;
	Glib::ustring m_rec_name;
	Glib::ustring m_rec_desc;
};

inline DbXmlImporter::filter_t operator|(DbXmlImporter::filter_t x, DbXmlImporter::filter_t y) { return (DbXmlImporter::filter_t)((unsigned int)x | (unsigned int)y); }
inline DbXmlImporter::filter_t operator&(DbXmlImporter::filter_t x, DbXmlImporter::filter_t y) { return (DbXmlImporter::filter_t)((unsigned int)x & (unsigned int)y); }
inline DbXmlImporter::filter_t operator^(DbXmlImporter::filter_t x, DbXmlImporter::filter_t y) { return (DbXmlImporter::filter_t)((unsigned int)x ^ (unsigned int)y); }
inline DbXmlImporter::filter_t operator~(DbXmlImporter::filter_t x) { return (DbXmlImporter::filter_t)~(unsigned int)x; }
inline DbXmlImporter::filter_t& operator|=(DbXmlImporter::filter_t& x, DbXmlImporter::filter_t y) { x = x | y; return x; }
inline DbXmlImporter::filter_t& operator&=(DbXmlImporter::filter_t& x, DbXmlImporter::filter_t y) { x = x & y; return x; }
inline DbXmlImporter::filter_t& operator^=(DbXmlImporter::filter_t& x, DbXmlImporter::filter_t y) { x = x ^ y; return x; }
inline bool operator!(DbXmlImporter::filter_t x) { return !(unsigned int)x; }

DbXmlImporter::DbXmlImporter(const Glib::ustring & output_dir, bool pgsql, filter_t filter)
        : xmlpp::SaxParser(false), m_state(state_document_c), m_outputdir(output_dir), m_purgedb(false),
	  m_filter(filter), m_starttime(0), m_rec_point(Point::invalid)
{
#ifdef HAVE_PQXX
	if (pgsql) {
		m_pgconn = pgconn_t(new pqxx::connection(m_outputdir));
		if (m_pgconn->get_variable("application_name").empty())
			m_pgconn->set_variable("application_name", "basulmimport");
	}
#else
	if (pgsql)
		throw std::runtime_error("postgres support not compiled in");
#endif
	time(&m_starttime);
}

DbXmlImporter::~DbXmlImporter()
{
}

void DbXmlImporter::on_start_document()
{
        m_state = state_document_c;
}

void DbXmlImporter::on_end_document()
{
	if (m_airportsdb) {
		m_airportsdb->set_exclusive(false);
		m_airportsdb->close();
		m_airportsdb.reset();
	}
	if (m_state != state_document_c)
		throw std::runtime_error("XML Parser: end document and not document state");
}

void DbXmlImporter::on_start_element(const Glib::ustring& name, const AttributeList& properties)
{
	if (false)
		std::cerr << "on_start_element: " << name << " state " << m_state << std::endl;
        switch (m_state) {
	case state_document_c:
		if (name == "gpx") {
			m_state = state_gpx_c;
			return;
		}
		break;

	case state_gpx_c:
		if (name == "wpt") {
			m_state = state_wpt_c;
			m_rec_ele.clear();
			m_rec_name.clear();
			m_rec_desc.clear();
			m_rec_point = Point::invalid;
			double lat(std::numeric_limits<double>::quiet_NaN());
			double lon(std::numeric_limits<double>::quiet_NaN());
			for (AttributeList::const_iterator i(properties.begin()), e(properties.end()); i != e; ++i) {
				if (i->name == "lat") {
					char *ep(0);
					lat = strtod(i->value.c_str(), &ep);
					if (*ep || ep == i->value.c_str())
						throw std::runtime_error("XML Parser: cannot parse lat " + i->value);
					continue;
				}
				if (i->name == "lon") {
					char *ep(0);
					lon = strtod(i->value.c_str(), &ep);
					if (*ep || ep == i->value.c_str())
						throw std::runtime_error("XML Parser: cannot parse lon " + i->value);
					continue;
				}
			}
			if (std::isnan(lat) || std::isnan(lon))
				return;
			m_rec_point.set_lat_deg_dbl(lat);
			m_rec_point.set_lon_deg_dbl(lon);
			return;
		}
		break;

	case state_wpt_c:
		if (name == "ele") {
			m_state = state_ele_c;
			return;
		}
		if (name == "name") {
			m_state = state_name_c;
			return;
		}
		if (name == "desc") {
			m_state = state_desc_c;
			return;
		}
		break;

	default:
		break;
        }
        std::ostringstream oss;
        oss << "XML Parser: Invalid element " << name << " in state " << (int)m_state;
        throw std::runtime_error(oss.str());
}

void DbXmlImporter::on_end_element(const Glib::ustring& name)
{
	if (false)
		std::cerr << "on_end_element: " << name << " state " << m_state << std::endl;
        switch (m_state) {
	case state_gpx_c:
		m_state = state_document_c;
		return;

	case state_wpt_c:
		m_state = state_gpx_c;
		process_airport();
		return;

	case state_ele_c:
		m_state = state_wpt_c;
		return;

	case state_name_c:
		m_state = state_wpt_c;
		return;

	case state_desc_c:
		m_state = state_wpt_c;
		return;

	default:
		break;
        }
        std::ostringstream oss;
        oss << "XML Parser: Invalid end element in state " << (int)m_state;
        throw std::runtime_error(oss.str());
}

void DbXmlImporter::on_characters(const Glib::ustring& characters)
{
       switch (m_state) {
       case state_ele_c:
	       m_rec_ele += characters;
	       break;

       case state_name_c:
	       m_rec_name += characters;
	       break;

       case state_desc_c:
	       m_rec_desc += characters;
	       break;

       case state_document_c:
       case state_gpx_c:
       case state_wpt_c:
	       break;

       default:
	       std::cerr << "XML Parser: unknown text \"" << characters << "\" in state " << m_state << std::endl;
       }
}

void DbXmlImporter::on_comment(const Glib::ustring& text)
{
}

void DbXmlImporter::on_warning(const Glib::ustring& text)
{
        std::cerr << "XML Parser: warning: " << text << std::endl;
        //throw std::runtime_error("XML Parser: warning: " + text);
}

void DbXmlImporter::on_error(const Glib::ustring& text)
{
        std::cerr << "XML Parser: error: " << text << std::endl;
        //throw std::runtime_error("XML Parser: error: " + text);
}

void DbXmlImporter::on_fatal_error(const Glib::ustring& text)
{
        throw std::runtime_error("XML Parser: fatal error: " + text);
}

std::string& DbXmlImporter::trimleft(std::string& s)
{
	std::string::size_type n(s.size()), x(0);
	while (x < n && std::isspace(s[x]))
		++x;
	s.erase(0, x);
	return s;
}

std::string& DbXmlImporter::trimright(std::string& s)
{
	std::string::size_type n(s.size()), x(n);
	while (x && std::isspace(s[x-1]))
		--x;
	s.erase(x);
	return s;
}

std::string& DbXmlImporter::trim(std::string& s)
{
	return trimright(trimleft(s));
}

Glib::ustring& DbXmlImporter::trimleft(Glib::ustring& s)
{
	Glib::ustring::size_type n(s.size()), x(0);
	while (x < n && std::isspace(s[x]))
		++x;
	s.erase(0, x);
	return s;
}

Glib::ustring& DbXmlImporter::trimright(Glib::ustring& s)
{
	Glib::ustring::size_type n(s.size()), x(n);
	while (x && std::isspace(s[x-1]))
		--x;
	s.erase(x);
	return s;
}

Glib::ustring& DbXmlImporter::trim(Glib::ustring& s)
{
	return trimright(trimleft(s));
}

bool DbXmlImporter::is_lf_icao(const std::string& id)
{
	if (AirportsDb::Airport::is_fpl_zzzz(id))
		return false;
	return id[0] == 'L' && id[1] == 'F';
}

bool DbXmlImporter::is_lf_ulm(const std::string& id)
{
	std::string::const_iterator i(id.begin()), e(id.end());
	if (i == e)
		return false;
	if (*i++ != 'L')
		return false;
	if (i == e)
		return false;
	if (*i++ != 'F')
		return false;
	if (i == e)
		return false;
	for (; i != e; ++i)
		if (!std::isdigit(*i))
			return false;
	return true;
}

void DbXmlImporter::process_airport(void)
{
	if (m_rec_point.is_invalid())
		return;
	std::string icao;
	Glib::ustring name(m_rec_name);
	{
		std::string::size_type i(name.find(" - "));
		if (i == std::string::npos)
			return;
		icao = name.substr(0, i);
		name.erase(0, i + 3);
	}
	trim(icao);
	trim(name);
	trim(m_rec_desc);
	int32_t elev;
	{
		trim(m_rec_ele);
		char *ep(0);
		elev = strtol(m_rec_ele.c_str(), &ep, 10);
		if (*ep || ep == m_rec_ele.c_str())
			return;
	}
	if (is_lf_icao(icao)) {
		if (!(m_filter & DbXmlImporter::filter_t::icao))
			return;
	} else if (is_lf_ulm(icao)) {
		if (!(m_filter & DbXmlImporter::filter_t::ulm))
			return;
	} else {
		return;
	}
        Glib::ustring vfrrmk("https://basulm.ffplum.fr/PDF/");
	vfrrmk +=  icao + ".pdf";
	if (!m_rec_desc.empty())
		vfrrmk += '\n' + m_rec_desc;
	if (!false)
		std::cerr << "Airport: " << icao << ' ' << m_rec_point.get_lat_str2() << ' ' << m_rec_point.get_lon_str2()
			  << " name " << (std::string)name << " desc " << (std::string)m_rec_desc << " elev " << elev << std::endl;
	open_airports_db();
	static constexpr double maxdist = 5.;
	// check for existing airport
	{
		AirportsDb::elementvector_t ev(m_airportsdb->find_by_rect(m_rec_point.simple_box_nmi(maxdist), 0, AirportsDb::element_t::subtables_all));
		for (AirportsDb::elementvector_t::iterator i(ev.begin()), e(ev.end()); i != e; ) {
			if (i->get_coord().is_invalid() || i->get_coord().spheric_distance_nmi_dbl(m_rec_point) > maxdist) {
				i = ev.erase(i);
				e = ev.end();
				continue;
			}
			++i;
		}
		{
			AirportsDb::elementvector_t ev2(m_airportsdb->find_by_icao(icao));
			for (AirportsDb::elementvector_t::iterator i(ev2.begin()), e(ev2.end()); i != e; ) {
				if (i->get_coord().is_invalid() || i->get_icao() != icao) {
					i = ev.erase(i);
					e = ev.end();
					continue;
				}
				++i;
			}
			ev.insert(ev.begin(), ev2.begin(), ev2.end());
		}
		AirportsDb::elementvector_t::iterator i(ev.begin()), e(ev.end());
		if (i == e) {
			AirportsDb::element_t el;
			el.set_sourceid(icao + "@BASULM");
			el.set_label_placement(AirportsDb::element_t::label_e);
			el.set_icao(icao);
			el.set_name(name);
			el.set_coord(m_rec_point);
			el.set_elevation(elev);
			el.set_typecode('A');
			el.set_vfrrmk(vfrrmk);
			el.set_flightrules(AirportsDb::element_t::flightrules_arr_vfr | AirportsDb::element_t::flightrules_dep_vfr);
			el.set_modtime(time(0));
			el.recompute_bbox();
			if (true)
				std::cerr << "New Airport " << (std::string)el.get_icao() << ' ' << (std::string)el.get_name() << ' '
					  << el.get_coord().get_lat_str2() << ' ' << el.get_coord().get_lon_str2()
					  << " elev " << el.get_elevation() << " remark " << (std::string)el.get_vfrrmk()
					  << ' ' << (std::string)el.get_sourceid() << std::endl;
			try {
				m_airportsdb->save(el);
			} catch (const std::exception& e) {
				std::cerr << "I/O Error: " << e.what() << std::endl;
				throw;
			}
			return;
		}
		i->set_sourceid(icao + "@BASULM");
		i->set_icao(icao);
		i->set_name(name);
		i->set_coord(m_rec_point);
		i->set_elevation(elev);
		i->set_vfrrmk(vfrrmk);
		i->set_modtime(time(0));
		i->recompute_bbox();
		if (true)
			std::cerr << "Replacing Airport " << (std::string)i->get_icao() << ' ' << (std::string)i->get_name() << ' '
				  << i->get_coord().get_lat_str2() << ' ' << i->get_coord().get_lon_str2()
				  << " elev " << i->get_elevation() << " remark " << (std::string)i->get_vfrrmk() << ' '
				  << (std::string)i->get_sourceid() << " id " << i->get_id() << std::endl;
		try {
			m_airportsdb->save(*i);
		} catch (const std::exception& e) {
			std::cerr << "I/O Error: " << e.what() << std::endl;
			throw;
		}
		for (++i; i != e; ++i) {
			if (true)
				std::cerr << "Erasing Airport " << (std::string)i->get_icao() << ' ' << (std::string)i->get_name() << ' '
					  << i->get_coord().get_lat_str2() << ' ' << i->get_coord().get_lon_str2()
					  << " elev " << i->get_elevation() << " remark " << (std::string)i->get_vfrrmk()
					  << ' ' << (std::string)i->get_sourceid() << " id " << i->get_id() << std::endl;
			try {
				m_airportsdb->erase(*i);
			} catch (const std::exception& e) {
				std::cerr << "I/O Error: " << e.what() << std::endl;
				throw;
			}
		}
	}
}

void DbXmlImporter::open_airports_db(void)
{
	if (m_airportsdb)
		return;
#ifdef HAVE_PQXX
	if (m_pgconn)
		m_airportsdb.reset(new AirportsPGDb(*m_pgconn));
	else
#endif
		m_airportsdb.reset(new AirportsDb(m_outputdir));
	m_airportsdb->set_exclusive(true);
	m_airportsdb->sync_off();
	if (m_purgedb)
		m_airportsdb->purgedb();
}

int main(int argc, char *argv[])
{
	static struct option long_options[] = {
		{ "dir", required_argument, 0, 'd' },
#ifdef HAVE_PQXX
		{ "pgsql", required_argument, 0, 'p' },
#endif
		{ "icao", no_argument, 0, 0x100 },
		{ "no-icao", no_argument, 0, 0x101 },
		{ "ulm", no_argument, 0, 0x102 },
		{ "no-ulm", no_argument, 0, 0x103 },
		{0, 0, 0, 0}
	};
	Glib::ustring db_dir(".");
	bool pgsql(false);
	DbXmlImporter::filter_t filter(DbXmlImporter::filter_t::ulm);
	int c, err(0);

	while ((c = getopt_long(argc, argv, "d:p:", long_options, 0)) != EOF) {
		switch (c) {
		case 'd':
			if (optarg) {
				db_dir = optarg;
				pgsql = false;
			}
			break;

#ifdef HAVE_PQXX
		case 'p':
			if (optarg) {
				db_dir = optarg;
				pgsql = true;
			}
			break;
#endif

		case 0x100:
			filter |= DbXmlImporter::filter_t::icao;
			break;

		case 0x101:
			filter &= ~DbXmlImporter::filter_t::icao;
			break;

		case 0x102:
			filter |= DbXmlImporter::filter_t::ulm;
			break;

		case 0x103:
			filter &= ~DbXmlImporter::filter_t::ulm;
			break;

		default:
			err++;
			break;
		}
	}
	if (err) {
		std::cerr << "usage: vfrdbbasulmimport [-d <dir>] [-p <pgconn>]" << std::endl;
		return EX_USAGE;
	}
	try {
		DbXmlImporter parser(db_dir, pgsql, filter);
		parser.set_validate(false); // Do not validate, we do not have a DTD
		parser.set_substitute_entities(true);
		for (; optind < argc; optind++) {
			std::cerr << "Parsing file " << argv[optind] << std::endl;
			parser.parse_file(argv[optind]);
		}
	} catch (const xmlpp::exception& ex) {
		std::cerr << "libxml++ exception: " << ex.what() << std::endl;
		return EX_DATAERR;
	} catch (const std::exception& ex) {
		std::cerr << "exception: " << ex.what() << std::endl;
		return EX_DATAERR;
	}
	return EX_OK;
}
