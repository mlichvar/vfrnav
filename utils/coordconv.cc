//
// C++ Implementation: windcalc
//
// Description: Wind Calculation Testbench
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2019
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "sysdeps.h"

#include <getopt.h>
#include <iomanip>
#include <iostream>
#include <stdexcept>
#include <glibmm.h>

#include "geom.h"

int main(int argc, char *argv[])
{
        static struct option long_options[] = {
		{ "webmercatorzoomlevel", required_argument, 0, 'w' },
		{ "maidenheaddigits", required_argument, 0, 'm' },
		{ "olcdigits", required_argument, 0, 'o' },
		{ 0, 0, 0, 0 }
        };
        int c, err(0);
	unsigned int webmerczoom(14);
	unsigned int maidenheaddigits(10);
	unsigned int olcdigits(11);

	Glib::init();
        while ((c = getopt_long(argc, argv, "w:m:o:", long_options, 0)) != EOF) {
                switch (c) {
		case 'w':
			if (optarg)
				webmerczoom = strtoul(optarg, 0, 0);
			break;

 		case 'm':
			if (optarg)
				maidenheaddigits = strtoul(optarg, 0, 0);
			break;

 		case 'o':
			if (optarg)
				olcdigits = strtoul(optarg, 0, 0);
			break;

 		default:
			++err;
			break;
                }
        }
        if (err) {
                std::cerr << "usage: coordconv [<coords...>]" << std::endl;
                return EX_USAGE;
        }
        try {
		for (; optind < argc; ++optind) {
			Point pt;
			if ((Point::setstr_lat | Point::setstr_lon) & ~pt.set_str(argv[optind])) {
				std::cerr << "coordconv: cannot parse coordinate \"" << argv[optind] << "\"" << std::endl;
				continue;
			}
			std::cout << "Input:   \"" << argv[optind] << "\"" << std::endl
				  << "Native:  " << pt.get_lat() << ' ' << pt.get_lon() << std::endl
				  << "Decimal: " << pt.get_lat_deg_dbl() << ' ' << pt.get_lon_deg_dbl() << std::endl
				  << "DM:      " << pt.get_lat_str2() << ' ' << pt.get_lon_str2() << std::endl
				  << "FPL:     " << pt.get_fpl_str(Point::fplstrmode_degoptminsec) << std::endl
				  << "Locator: " << pt.get_locator(maidenheaddigits) << std::endl
				  << "OLC:     " << pt.get_openlocationcode(olcdigits) << std::endl
				  << "WebMerc: " << pt.get_lat_webmercator(webmerczoom) << '/' << pt.get_lon_webmercator(webmerczoom) << std::endl;
			{
				float x, y;
				if (pt.get_ch1903(x, y))
					std::cout << "CH1903:  " << x << ' ' << y << std::endl;
			}
			{
				double x, y;
				if (pt.get_ch1903p(x, y))
					std::cout << "CH1903+: " << x << ' ' << y << std::endl;
			}
			std::cout << std::endl;
		}
        } catch (const Glib::Exception& ex) {
                std::cerr << "Glib exception: " << ex.what() << std::endl;
                return EX_DATAERR;
        } catch (const std::exception& ex) {
                std::cerr << "exception: " << ex.what() << std::endl;
                return EX_DATAERR;
        }
	return EX_OK;
}
