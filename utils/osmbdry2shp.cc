//
// C++ Implementation: osmbdry2shp
//
// Description: Convert OpenStreetMap country boundaries to shape files
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2018
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "sysdeps.h"
#include <iostream>
#include <exception>

// For assembling multipolygons
#include <osmium/area/assembler.hpp>
#include <osmium/area/multipolygon_manager.hpp>

// For the DynamicHandler class
#include <osmium/dynamic_handler.hpp>

// For the OGR factory
#include <osmium/geom/ogr.hpp>

// For the Dump handler
#include <osmium/handler/dump.hpp>

// For the NodeLocationForWays handler
#include <osmium/handler/node_locations_for_ways.hpp>

// Allow any format of input files (XML, PBF, ...)
#include <osmium/io/any_input.hpp>

// For osmium::apply()
#include <osmium/visitor.hpp>

// For the location index. There are different types of indexes available.
// This will work for all input files keeping the index in memory.
#include <osmium/index/map/flex_mem.hpp>

#include "ogrsf_frmts.h"

// The type of index used. This must match the include file above
using index_type = osmium::index::map::FlexMem<osmium::unsigned_object_id_type, osmium::Location>;

// The location handler always depends on the index type
using location_handler_type = osmium::handler::NodeLocationsForWays<index_type>;

class MyHandler : public osmium::handler::Handler {
public:
	typedef std::map<osmium::object_id_type,std::string> borderfeatures_t;

protected:
	OGRFieldDefn m_field_intname;
	OGRFieldDefn m_field_iso3166_2;
	OGRFieldDefn m_field_iso3166_3;
	OGRFieldDefn m_field_iso3166_n;
	OGRFieldDefn m_field_osmid;
	GDALDriver *m_driver;
	GDALDataset *m_ds;
	OGRLayer *m_layer;
	borderfeatures_t m_borderfeatures;

public:
	MyHandler(const char *out_filename = "osmbdry.shp")
		: m_field_intname("int_name", OFTString),
		  m_field_iso3166_2("iso_alpha2", OFTString),
		  m_field_iso3166_3("iso_alpha3", OFTString),
		  m_field_iso3166_n("iso_num", OFTInteger),
		  m_field_osmid("osmid", OFTInteger64),
		  m_driver(0), m_ds(0), m_layer(0) {
		GDALAllRegister();
		m_driver = GetGDALDriverManager()->GetDriverByName("ESRI Shapefile");
		if (!m_driver)
			throw std::runtime_error("cannot get ESRI Shapefile driver");
		m_ds = m_driver->Create(out_filename, 0, 0, 0, GDT_Unknown, NULL);
		if (!m_ds)
			throw std::runtime_error(std::string("cannot create shapefile ") + out_filename);
		OGRSpatialReference *sref = new OGRSpatialReference();
		sref->importFromEPSG(4326);
		m_layer = m_ds->CreateLayer("boundary", sref, wkbMultiPolygon, NULL);
		sref->Release();
		if (!m_layer)
			throw std::runtime_error("cannot create layer boundary");
		m_field_intname.SetWidth(32);
		m_field_iso3166_2.SetWidth(2);
		m_field_iso3166_3.SetWidth(3);
		if (m_layer->CreateField(&m_field_intname) != OGRERR_NONE)
			throw std::runtime_error("cannot create int_name field");
		if (m_layer->CreateField(&m_field_iso3166_2) != OGRERR_NONE)
			throw std::runtime_error("cannot create iso_alpha2 field");
		if (m_layer->CreateField(&m_field_iso3166_3) != OGRERR_NONE)
			throw std::runtime_error("cannot create iso_alpha3 field");
		if (m_layer->CreateField(&m_field_iso3166_n) != OGRERR_NONE)
			throw std::runtime_error("cannot create iso_num field");
		if (m_layer->CreateField(&m_field_osmid) != OGRERR_NONE)
			throw std::runtime_error("cannot create osmid field");
	}

	~MyHandler() {
		if (m_ds)
			GDALClose(m_ds);
	}

	const borderfeatures_t& get_borderfeatures(void) const noexcept { return m_borderfeatures; }

	bool is_borderfeature(osmium::object_id_type id) const noexcept {
		return m_borderfeatures.find(id) != m_borderfeatures.end();
	}

	bool filter(const osmium::TagList& tags) const noexcept {
		const char *bdry(tags.get_value_by_key("boundary"));
		if (!bdry)
			return false;
		if (strcmp(bdry, "administrative"))
			return false;
		const char *alvls(tags.get_value_by_key("admin_level"));
		if (!alvls)
			return false;
		unsigned int alvl(strtoul(alvls, 0, 10));
		if (alvl != 2)
			return false;
		return true;
	}

	void way(const osmium::Way& way) {
		return;
		if (!filter(way.tags()))
			return;
		std::cout << "way " << way.id() << std::endl;
		for (const osmium::Tag& t : way.tags()) {
			std::cout << t.key() << "=" << t.value() << std::endl;
		}
	}

	void relation(const osmium::Relation& rel) {
		if (!filter(rel.tags()))
			return;
		std::cout << "relation " << rel.id() << std::endl;
		for (const osmium::Tag& t : rel.tags()) {
			std::cout << t.key() << "=" << t.value() << std::endl;
		}
	}

	void area(const osmium::Area& area) {
		if (!filter(area.tags()))
			return;
		const char *intname(area.tags().get_value_by_key("int_name"));
		const char *iso3166_2(area.tags().get_value_by_key("ISO3166-1"));
		if (!iso3166_2)
			iso3166_2 = area.tags().get_value_by_key("ISO3166-1:alpha2");
		const char *iso3166_3(area.tags().get_value_by_key("ISO3166-1:alpha3"));
		const char *iso3166_n(area.tags().get_value_by_key("ISO3166-1:numeric"));
		if (m_layer && intname && iso3166_2 && iso3166_3 && iso3166_n) {
			OGRFeature *ftr = OGRFeature::CreateFeature(m_layer->GetLayerDefn());
			ftr->SetField("int_name", intname);
			ftr->SetField("iso_alpha2", iso3166_2);
			ftr->SetField("iso_alpha3", iso3166_3);
			ftr->SetField("iso_num", (int)strtoll(iso3166_n, 0, 10));
			ftr->SetField("osmid", (GIntBig)area.id());
			{
				osmium::geom::OGRFactory<> factory;
				std::unique_ptr<OGRMultiPolygon> geom(factory.create_multipolygon(area));
				ftr->SetGeometryDirectly(geom.release());
			}
			if (m_layer->CreateFeature(ftr) != OGRERR_NONE) {
				if (false) {
					OGRFeature::DestroyFeature(ftr);
					throw std::runtime_error(std::string("cannot create feature ") + intname);
				}
				std::cerr << "cannot create feature " << intname << std::endl;
			} else {
				m_borderfeatures.insert(borderfeatures_t::value_type(area.id(), iso3166_3));
			}
			OGRFeature::DestroyFeature(ftr);
		}
		std::cout << "area " << area.id() << std::endl;
		for (const osmium::Tag& t : area.tags()) {
			std::cout << t.key() << "=" << t.value() << std::endl;
		}
		std::cout << std::endl;
	}

	void node(const osmium::Node& node) {
		if (false)
			std::cout << "node " << node.id() << std::endl;
	}

};

int main(int argc, char *argv[])
{
	if (argc < 2) {
		std::cerr << "Usage: osmbdry2shp <infile> [<outfile>]" << std::endl;
		return 1;
	}
	try {
		// Initialize an empty DynamicHandler. Later it will be associated
		// with one of the handlers. You can think of the DynamicHandler as
		// a kind of "variant handler" or a "pointer handler" pointing to the
		// real handler.
		//osmium::handler::DynamicHandler handler;
		//handler.set<MyHandler>();
		//handler.set<osmium::handler::Dump>(std::cout);
		MyHandler handler(argc >= 3 ? argv[2] : "osmbdry.shp");

		osmium::io::File input_file{argv[1]};

		// Configuration for the multipolygon assembler. Here the default settings
		// are used, but you could change multiple settings.
		osmium::area::Assembler::config_type assembler_config;

		// Set up a filter matching only forests. This will be used to only build
		// areas with matching tags.
		osmium::TagsFilter filter{false};
		filter.add_rule(false, "admin_level", "1");
		filter.add_rule(false, "admin_level", "3");
		filter.add_rule(false, "admin_level", "4");
		filter.add_rule(false, "admin_level", "5");
		filter.add_rule(false, "admin_level", "6");
		filter.add_rule(false, "admin_level", "7");
		filter.add_rule(false, "admin_level", "8");
		filter.add_rule(false, "admin_level", "9");
		filter.add_rule(false, "admin_level", "10");
		filter.add_rule(false, "admin_level", "11");
		filter.add_rule(false, "admin_level", "12");
		filter.add_rule(true, "boundary", "administrative");

		// Initialize the MultipolygonManager. Its job is to collect all
		// relations and member ways needed for each area. It then calls an
		// instance of the osmium::area::Assembler class (with the given config)
		// to actually assemble one area. The filter parameter is optional, if
		// it is not set, all areas will be built.
		osmium::area::MultipolygonManager<osmium::area::Assembler> mp_manager{assembler_config, filter};

		// We read the input file twice. In the first pass, only relations are
		// read and fed into the multipolygon manager.
		std::cerr << "Pass 1..." << std::endl;
		osmium::relations::read_relations(input_file, mp_manager);
		std::cerr << "Pass 1 done" << std::endl;

		// Output the amount of main memory used so far. All multipolygon relations
		// are in memory now.
		std::cerr << "Memory:" << std::endl;
		osmium::relations::print_used_memory(std::cerr, mp_manager.used_memory());

		// The index storing all node locations.
		index_type index;

		// The handler that stores all node locations in the index and adds them
		// to the ways.
		location_handler_type location_handler{index};

		// If a location is not available in the index, we ignore it. It might
		// not be needed (if it is not part of a multipolygon relation), so why
		// create an error?
		location_handler.ignore_errors();

		// On the second pass we read all objects and run them first through the
		// node location handler and then the multipolygon collector. The collector
		// will put the areas it has created into the "buffer" which are then
		// fed through our "handler".
		std::cerr << "Pass 2..." << std::endl;
		osmium::io::Reader reader{input_file};
		osmium::apply(reader, location_handler, mp_manager.handler([&handler](osmium::memory::Buffer&& buffer) {
					osmium::apply(buffer, handler);
				}));
		reader.close();
		std::cerr << "Pass 2 done" << std::endl;

		// Output the amount of main memory used so far. All complete multipolygon
		// relations have been cleaned up.
		std::cerr << "Memory:" << std::endl;
		osmium::relations::print_used_memory(std::cerr, mp_manager.used_memory());

		// If there were multipolgyon relations in the input, but some of their
		// members are not in the input file (which often happens for extracts)
		// this will write the IDs of the incomplete relations to stderr.
		std::vector<osmium::object_id_type> incomplete_relations_ids;
		mp_manager.for_each_incomplete_relation([&](const osmium::relations::RelationHandle& handle){
				incomplete_relations_ids.push_back(handle->id());
			});
		if (!incomplete_relations_ids.empty()) {
			std::cerr << "Warning! Some member ways missing for these multipolygon relations:";
			for (const auto id : incomplete_relations_ids) {
				std::cerr << ' ' << id;
				if (handler.is_borderfeature(id))
					std::cerr << "(B)";
			}
			std::cerr << std::endl;
		}
		if (!handler.get_borderfeatures().empty()) {
			std::cerr << std::endl << "Countries(" << handler.get_borderfeatures().size() << "):";
			for (const auto& bf : handler.get_borderfeatures()) {
				std::cerr << ' ' << bf.second << '(' << bf.first << ')';
			}
			std::cerr << std::endl;
		}
	} catch (const std::exception& e) {
		std::cerr << "exception: " << e.what() << std::endl;
		return 1;
	}
	return 0;
}
