/*****************************************************************************/

/*
 *      osmsdbimport.cc  --  OpenStreetMap proprietary static database importer.
 *
 *      Copyright (C) 2020  Thomas Sailer (t.sailer@alumni.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/*****************************************************************************/

#define BOOST_GEOMETRY_ENABLE_ASSERT_HANDLER

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <zlib.h>
#include <iterator>
#include <iomanip>
#include <fstream>

#include "osmsdb.h"
#include "hibernate.h"
#include "geomboost.h"
#include "mapfile.h"
#include <boost/geometry/index/rtree.hpp>
#include <boost/interprocess/managed_mapped_file.hpp>

namespace boost { namespace geometry
{
	inline void assertion_failed(char const * expr, char const * function, char const * file, long line) {
		std::ostringstream oss;
		oss << expr << ": " << function << ": " << file << ':' << line;
		throw std::runtime_error(oss.str());
	}

	inline void assertion_failed_msg(char const * expr, char const * msg, char const * function, char const * file, long line) {
		std::ostringstream oss;
		oss << expr << '(' << msg << "): " << function << ": " << file << ':' << line;
		throw std::runtime_error(oss.str());
	}
} }

class OSMStaticDB::Index {
public:
	Index(const std::string& tmpdir, int fd,
	      size_t max_elements = rtreedefaultmaxelperpage, size_t min_elements = std::numeric_limits<size_t>::max(),
	      size_t reinserted_elements = std::numeric_limits<size_t>::max(), size_t overlap_cost_threshold = 32, bool bulkload = true);
	~Index();

	void addindex(void);

protected:
	static constexpr bool dumprtree = false;

	typedef std::pair<Rect, BinFileRtreeLeafNode::index_t> area_rtree_value_t;
	//typedef boost::geometry::index::rstar<rtreemaxelperpage> rtree_area_params_t;
	typedef boost::geometry::index::dynamic_rstar rtree_area_params_t;
	typedef boost::geometry::index::indexable<area_rtree_value_t> rtree_area_indexable_t;
	typedef boost::geometry::index::equal_to<area_rtree_value_t> rtree_area_equal_to_t;
	typedef boost::geometry::index::rtree<area_rtree_value_t, rtree_area_params_t, rtree_area_indexable_t, rtree_area_equal_to_t,
					      boost::interprocess::allocator<area_rtree_value_t, boost::interprocess::managed_mapped_file::segment_manager> > area_rtree_t;

	template <typename Value, typename Options, typename Translator, typename Box, typename Allocators, typename IndexEntry>
	class write_rtree_visitor : public boost::geometry::index::detail::rtree::visitor<Value, typename Options::parameters_type, Box, Allocators, typename Options::node_tag, true>::type {
	protected:
		typedef typename boost::geometry::index::detail::rtree::internal_node<Value, typename Options::parameters_type, Box, Allocators, typename Options::node_tag>::type internal_node;
		typedef typename boost::geometry::index::detail::rtree::leaf<Value, typename Options::parameters_type, Box, Allocators, typename Options::node_tag>::type leaf;

		const IndexEntry *m_indexb;
		const IndexEntry *m_indexe;
		typedef std::map<const void *, std::pair<Rect, uint64_t> > pagemap_t;
		pagemap_t m_pagemap;
		uint64_t& m_addr;
		uint64_t m_rootaddr;
		bool m_rootleaf;
		int m_fd;

	public:
		write_rtree_visitor(uint64_t& addr, int fd, const IndexEntry *idxb, const IndexEntry *idxe)
			: m_indexb(idxb), m_indexe(idxe), m_addr(addr), m_rootaddr(0), m_rootleaf(true), m_fd(fd) {}

		uint64_t get_rootaddr(void) const { return m_rootaddr; }
		bool is_rootleaf(void) const { return m_rootleaf; }

		void operator()(const internal_node& n) {
			typedef typename boost::geometry::index::detail::rtree::elements_type<internal_node>::type elements_type;
			elements_type const& elements = boost::geometry::index::detail::rtree::elements(n);

			for (typename elements_type::const_iterator it = elements.begin(); it != elements.end(); ++it)
				boost::geometry::index::detail::rtree::apply_visitor(*this, *(it->second));

			m_addr += aligninc;
			m_addr &= alignmask;
			{
				bool haveleaf(false), haveint(false);
				BinFileRtreeInternalNode ent;
				Rect bbox;
				uint16_t node(0);
				ent.set_nodes(node);
				bbox.set_invalid();
				for (typename elements_type::const_iterator it = elements.begin(); it != elements.end(); ++it) {
					const void *nn(0);
					{
						const void *nn1(boost::get<leaf>(&*(it->second)));
						if (nn1) {
							haveleaf = true;
							nn = nn1;
						}
					}
					{
						const void *nn1(boost::get<internal_node>(&*(it->second)));
						if (nn1) {
							haveint = true;
							nn = nn1;
						}
					}
					uint64_t addr1(0);
					Rect bbox1;
					{
						pagemap_t::const_iterator i(m_pagemap.find(nn));
						if (i == m_pagemap.end()) {
							std::cerr << "R-Tree: cannot find page info for " << (uint64_t)nn << std::endl;
							continue;
						}
						bbox1 = i->second.first;
						addr1 = i->second.second;
					}
					++node;
					ent.set_nodes(node);
					ent.set_bbox(node - 1, bbox1);
					ent.set_addroffs(node - 1, addr1 - m_addr);
					if (bbox.is_invalid())
							bbox = bbox1;
						else
							bbox = bbox.add(bbox1);
				}
				if (ent.get_nodes() != node)
					throw std::runtime_error("R-Tree: page size error");
				if (haveleaf && haveint)
					throw std::runtime_error("R-Tree: page has leaf and non-leaf pointers");
				ent.set_pointstoleaf(haveleaf);
				if (pwrite(m_fd, &ent, ent.get_size(), m_addr) != static_cast<ssize_t>(ent.get_size()))
					MapFile::throw_strerror("Write error");
				m_rootaddr = m_addr;
				m_rootleaf = false;
				m_addr += ent.get_size();
				m_pagemap[&n] = std::make_pair(bbox, m_rootaddr);
			}

			if (dumprtree) {
				std::cout << "  Internal Node: " << (uint64_t)(&n) << std::endl;
				for (typename elements_type::const_iterator it = elements.begin(); it != elements.end(); ++it)
					dump_box(to_geom(it->first), boost::get<leaf>(&*(it->second)), boost::get<internal_node>(&*(it->second)));
			}
		}

		void operator()(leaf const& n) {
			typedef typename boost::geometry::index::detail::rtree::elements_type<leaf>::type elements_type;
			const elements_type& elements = boost::geometry::index::detail::rtree::elements(n);

			{
				typedef std::vector<BinFileRtreeLeafNode::index_t> ids_t;
				ids_t ids;
				for (typename elements_type::const_iterator it = elements.begin(); it != elements.end(); ++it)
					ids.push_back(it->second);
				std::sort(ids.begin(), ids.end());
				{
					BinFileRtreeLeafNode ent;
					ent.set_nodes(ids.size());
					if (ent.get_nodes() != ids.size())
						throw std::runtime_error("R-Tree: page size error");
					for (ids_t::size_type i(0), n(ids.size()); i < n; ++i)
						ent.set_index(i, ids[i]);
					m_addr += aligninc;
					m_addr &= alignmask;
					if (pwrite(m_fd, &ent, ent.get_size(), m_addr) != static_cast<ssize_t>(ent.get_size()))
						MapFile::throw_strerror("Write error");
					m_rootaddr = m_addr;
					m_rootleaf = true;
					m_addr += ent.get_size();
				}
				{
					Rect bbox;
					bbox.set_invalid();
					for (const auto& id : ids) {
						const IndexEntry *oi(m_indexb + id);
						if (oi >= m_indexe) {
							std::cerr << "R-Tree: object index " << id << " out of range 0..."
								  << (m_indexe - m_indexb - 1) << std::endl;
							continue;
						}
						if (bbox.is_invalid())
							bbox = oi->get_bbox();
						else
							bbox = bbox.add(oi->get_bbox());
					}
					if (bbox.is_invalid())
						std::cerr << "R-Tree: no bbox" << std::endl;
					m_pagemap[&n] = std::make_pair(bbox, m_rootaddr);
				}
			}

			if (dumprtree) {
				std::cout << "  Leaf Node: " << (uint64_t)(&n) << std::endl;
				for (typename elements_type::const_iterator it = elements.begin(); it != elements.end(); ++it)
					dump_value(*it);
			}
		}

		void dump_box(const Rect& b, void *x, void *y) {
			std::cout << "    B " << (uint64_t)(x) << '/' << (uint64_t)(y) << ": " << b << std::endl;
		}

		void dump_value(const area_rtree_value_t& v) {
			std::cout << "    I " << v.second << ": " << v.first << std::endl;
		}
	};

	template<typename T>
	class IndexIterator : public std::iterator<std::forward_iterator_tag, area_rtree_value_t, ptrdiff_t, area_rtree_value_t *, area_rtree_value_t> {
	public:
		IndexIterator(const T *b = nullptr, const T *c = nullptr, const T *e = nullptr) : m_begin(b), m_cur(c), m_end(e), m_second(false) { skipinvalid(); }
		IndexIterator& operator++() {
			if (m_cur == m_end)
				return *this;
			m_cur += m_second;
			m_second = !m_second;
			skipinvalid();
			return *this;
		}
		IndexIterator operator++(int) { IndexIterator r(*this); operator++(); return r; }
		bool operator==(const IndexIterator& rhs) const { return m_cur == rhs.m_cur; }
		bool operator!=(const IndexIterator& rhs) const { return m_cur != rhs.m_cur; }
		area_rtree_value_t operator*() {
	        	area_rtree_value_t r(m_cur->get_bbox(), m_cur - m_begin);
			if (r.first.is_invalid() || r.first.get_south() > r.first.get_north()) {
				r.first.set_invalid();
				return r;
			}
			if (m_second) {
				if (r.first.get_west() <= r.first.get_east()) {
					r.first.set_invalid();
					return r;
				}
				r.first.set_west(std::numeric_limits<int32_t>::min());
				if (r.first.get_west() == r.first.get_east())
					r.first.set_invalid();
				return r;
			}
			if (r.first.get_west() > r.first.get_east()) {
				r.first.set_east(std::numeric_limits<int32_t>::max());
				if (r.first.get_west() == r.first.get_east())
					r.first.set_invalid();
			}
			return r;
		}
		bool is_at_end(void) const { return m_cur == m_end; }
		bool is_second_half(void) const { return m_second; }

	protected:
		const T *m_begin;
		const T *m_cur;
		const T *m_end;
		bool m_second;

		void skipinvalid(void) {
			for (; m_cur != m_end; ) {
				if (!operator*().first.is_invalid())
					return;
				m_cur += m_second;
				m_second = !m_second;
			}
		}
	};

	std::string m_tmpdir;
	uint64_t m_addr;
	size_t m_max_elements;
	size_t m_min_elements;
	size_t m_reinserted_elements;
	size_t m_overlap_cost_threshold;
	int m_fd;
	bool m_bulkload;

	template<typename T> uint64_t do_rtree(const T *idxb, const T *idxe, bool bulkload);
};

constexpr bool OSMStaticDB::Index::dumprtree;

OSMStaticDB::Index::Index(const std::string& tmpdir, int fd, size_t max_elements, size_t min_elements,
			  size_t reinserted_elements, size_t overlap_cost_threshold, bool bulkload)
	: m_tmpdir(tmpdir), m_addr(0), m_max_elements(std::min(max_elements, static_cast<size_t>(rtreemaxelperpage))),
	  m_min_elements(min_elements), m_reinserted_elements(reinserted_elements), m_overlap_cost_threshold(overlap_cost_threshold),
	  m_fd(fd), m_bulkload(bulkload)
{
}

OSMStaticDB::Index::~Index()
{
}

template<typename T>
uint64_t OSMStaticDB::Index::do_rtree(const T *idxb, const T *idxe, bool bulkload)
{
	if (!idxb || idxb == idxe)
		return 0;
	uint64_t rootaddr(0);
	std::string rtreefilename(Glib::build_filename(m_tmpdir, "rtrees"));
	{
		// set up rtrees
		boost::interprocess::managed_mapped_file rtreefile(boost::interprocess::open_or_create, rtreefilename.c_str(), 16*1024*1024*1024L);
		area_rtree_t *rtree(0);
		if (bulkload) {
			IndexIterator<T> ib(idxb, idxb, idxe), ie(idxb, idxe, idxe);
			std::cerr << "  bulk loading " << (idxe - idxb) << " objects..." << std::endl;
			rtree = rtreefile.construct<area_rtree_t>("rtree")(ib, ie,
									   rtree_area_params_t(m_max_elements, m_min_elements,
											       m_reinserted_elements, m_overlap_cost_threshold),
									   rtree_area_indexable_t(),
									   rtree_area_equal_to_t(), rtreefile.get_segment_manager());
		} else {
			rtree = rtreefile.construct<area_rtree_t>("rtree")(rtree_area_params_t(m_max_elements, m_min_elements,
											       m_reinserted_elements, m_overlap_cost_threshold),
									   rtree_area_indexable_t(),
									   rtree_area_equal_to_t(), rtreefile.get_segment_manager());
			{
				unsigned int cnt(0), added(0), totcnt(idxe - idxb);
				BinFileRtreeLeafNode::index_t lastindex(-1);
				for (IndexIterator<T> ib(idxb, idxb, idxe); !ib.is_at_end(); ++ib, ++cnt) {
					if (!(cnt & ((1U << 16) - 1U)))
						std::cerr << "  adding object " << cnt << '/' << totcnt << " to index" << std::endl;
					area_rtree_value_t rtv(*ib);
					if (!false && rtv.second == lastindex)
						std::cerr << "Entry ID " << lastindex << " (" << idxb[lastindex].get_id() << ") bbox "
							  << idxb[lastindex].get_bbox() << " crosses 180deg meridian, splitting" << std::endl;
					++lastindex;
					if (lastindex != rtv.second) {
						std::cerr << "Entry ID";
						char sep(' ');
						for (BinFileRtreeLeafNode::index_t x(lastindex); x != rtv.second; ++x, sep = ',')
							std::cerr << sep << x;
						std::cerr << ' ';
						sep = '(';
						for (BinFileRtreeLeafNode::index_t x(lastindex); x != rtv.second; ++x, sep = ',')
							std::cerr << sep << idxb[x].get_id();
						std::cerr << ") has invalid bbox ";
						sep = '(';
						for (BinFileRtreeLeafNode::index_t x(lastindex); x != rtv.second; ++x, sep = ',')
							std::cerr << sep << idxb[x].get_bbox();
						std::cerr << "), omitting" << std::endl;
					}
					lastindex = rtv.second;
					try {
						rtree->insert(rtv);
						++added;
					} catch (const std::exception& e) {
						std::cerr << "Cannot add entry ID " << rtv.second << '/' << static_cast<unsigned int>(ib.is_second_half())
							  << " (" << idxb[rtv.second].get_id() << ") bbox " << rtv.first << ", omitting" << std::endl;
					}
				}
				std::cerr << "  added " << added << '/' << totcnt << " objects to index" << std::endl;
			}
		}
		typedef boost::geometry::index::detail::rtree::utilities::view<area_rtree_t> RTV;
		RTV rtv(*rtree);
		write_rtree_visitor<typename RTV::value_type,
				    typename RTV::options_type,
				    typename RTV::translator_type,
				    typename RTV::box_type,
				    typename RTV::allocators_type,
				    T> v(m_addr, m_fd, idxb, idxe);
		rtv.apply_visitor(v);
		if (v.is_rootleaf())
			throw std::runtime_error("R-Tree has leaf root node");
		rootaddr = v.get_rootaddr();
	}
	std::remove(rtreefilename.c_str());
	return rootaddr;
}

void OSMStaticDB::Index::addindex(void)
{
	m_addr = dbsizenoindex(m_fd);
	if (ftruncate(m_fd, m_addr))
		MapFile::throw_strerror("truncate error");
	MapFile bin(m_fd, false, "odb size");
	const BinFileHeader& hdr(*bin.begin_as<BinFileHeader>());
	if (!hdr.check_signature())
		throw std::runtime_error("Error: invalid file signature");
	BinFileHeader hdr1(hdr);
	// rtree
	{
		std::cerr << "Writing R-Tree " << layer_t::boundarylinesland << "..." << std::endl;
		uint64_t addrb(m_addr);
		auto idx(hdr.get_objdir_range<layer_t::boundarylinesland>());
		hdr1.set_rtreeoffs(layer_t::boundarylinesland, do_rtree(idx.first, idx.second, m_bulkload));
		std::cerr << "R-Tree " << layer_t::boundarylinesland << " size " << (m_addr - addrb) << std::endl;
	}
	{
		std::cerr << "Writing R-Tree " << layer_t::icesheetpolygons << "..." << std::endl;
		uint64_t addrb(m_addr);
		auto idx(hdr.get_objdir_range<layer_t::icesheetpolygons>());
		hdr1.set_rtreeoffs(layer_t::icesheetpolygons, do_rtree(idx.first, idx.second, m_bulkload));
		std::cerr << "R-Tree " << layer_t::icesheetpolygons << " size " << (m_addr - addrb) << std::endl;
	}
	{
		std::cerr << "Writing R-Tree " << layer_t::icesheetoutlines << "..." << std::endl;
		uint64_t addrb(m_addr);
		auto idx(hdr.get_objdir_range<layer_t::icesheetoutlines>());
		hdr1.set_rtreeoffs(layer_t::icesheetoutlines, do_rtree(idx.first, idx.second, m_bulkload));
		std::cerr << "R-Tree " << layer_t::icesheetoutlines << " size " << (m_addr - addrb) << std::endl;
	}
	{
		std::cerr << "Writing R-Tree " << layer_t::waterpolygons << "..." << std::endl;
		uint64_t addrb(m_addr);
		auto idx(hdr.get_objdir_range<layer_t::waterpolygons>());
		hdr1.set_rtreeoffs(layer_t::waterpolygons, do_rtree(idx.first, idx.second, m_bulkload));
		std::cerr << "R-Tree " << layer_t::waterpolygons << " size " << (m_addr - addrb) << std::endl;
	}
	{
		std::cerr << "Writing R-Tree " << layer_t::simplifiedwaterpolygons << "..." << std::endl;
		uint64_t addrb(m_addr);
		auto idx(hdr.get_objdir_range<layer_t::simplifiedwaterpolygons>());
		hdr1.set_rtreeoffs(layer_t::simplifiedwaterpolygons, do_rtree(idx.first, idx.second, m_bulkload));
		std::cerr << "R-Tree " << layer_t::simplifiedwaterpolygons << " size " << (m_addr - addrb) << std::endl;
	}
	// write header
	if (pwrite(m_fd, &hdr1, sizeof(hdr1), 0) != sizeof(hdr1))
		MapFile::throw_strerror("write error");
	m_addr = 0;
}

uint64_t OSMStaticDB::dbsizenoindex(int fd)
{
	MapFile bin(fd, false, "odb size");
	const BinFileHeader& hdr(*bin.begin_as<BinFileHeader>());
	if (!hdr.check_signature())
		throw std::runtime_error("Error: invalid file signature");
	uint64_t sz(0);
	{
		const char *cp(0);
		for (const BinFileTagKeyEntry *i(hdr.get_tagkey_begin()), *e(hdr.get_tagkey_end()); i != e; ++i)
			cp = std::max(cp, i->get_strptr());
		for (const BinFileTagNameEntry *i(hdr.get_tagname_begin()), *e(hdr.get_tagname_end()); i != e; ++i)
			cp = std::max(cp, i->get_strptr());
		if (cp)
			sz = (cp - reinterpret_cast<char *>(bin.begin())) + strlen(cp) + 1;
	}
	sz = std::max(sz, hdr.get_objdiroffs(layer_t::boundarylinesland) + hdr.get_objdirentries(layer_t::boundarylinesland) * sizeof(layer_types<layer_t::boundarylinesland>::IndexType));
	sz = std::max(sz, hdr.get_objdiroffs(layer_t::icesheetpolygons) + hdr.get_objdirentries(layer_t::icesheetpolygons) * sizeof(layer_types<layer_t::icesheetpolygons>::IndexType));
	sz = std::max(sz, hdr.get_objdiroffs(layer_t::icesheetoutlines) + hdr.get_objdirentries(layer_t::icesheetoutlines) * sizeof(layer_types<layer_t::icesheetoutlines>::IndexType));
	sz = std::max(sz, hdr.get_objdiroffs(layer_t::waterpolygons) + hdr.get_objdirentries(layer_t::waterpolygons) * sizeof(layer_types<layer_t::waterpolygons>::IndexType));
	sz = std::max(sz, hdr.get_objdiroffs(layer_t::simplifiedwaterpolygons) + hdr.get_objdirentries(layer_t::simplifiedwaterpolygons) * sizeof(layer_types<layer_t::simplifiedwaterpolygons>::IndexType));
	{
		auto idx(hdr.get_objdir_range<layer_t::boundarylinesland>());
		for (; idx.first != idx.second; ++idx.first)
			sz = std::max(sz, (reinterpret_cast<const uint8_t *>(idx.first) - bin.begin()) + idx.first->get_dataoffs() + idx.first->get_datasize());
	}
	{
		auto idx(hdr.get_objdir_range<layer_t::icesheetpolygons>());
		for (; idx.first != idx.second; ++idx.first)
			sz = std::max(sz, (reinterpret_cast<const uint8_t *>(idx.first) - bin.begin()) + idx.first->get_dataoffs() + idx.first->get_datasize());
	}
	{
		auto idx(hdr.get_objdir_range<layer_t::icesheetoutlines>());
		for (; idx.first != idx.second; ++idx.first)
			sz = std::max(sz, (reinterpret_cast<const uint8_t *>(idx.first) - bin.begin()) + idx.first->get_dataoffs() + idx.first->get_datasize());
	}
	{
		auto idx(hdr.get_objdir_range<layer_t::waterpolygons>());
		for (; idx.first != idx.second; ++idx.first)
			sz = std::max(sz, (reinterpret_cast<const uint8_t *>(idx.first) - bin.begin()) + idx.first->get_dataoffs() + idx.first->get_datasize());
	}
	{
		auto idx(hdr.get_objdir_range<layer_t::simplifiedwaterpolygons>());
		for (; idx.first != idx.second; ++idx.first)
			sz = std::max(sz, (reinterpret_cast<const uint8_t *>(idx.first) - bin.begin()) + idx.first->get_dataoffs() + idx.first->get_datasize());
	}
	return sz;
}

void OSMStaticDB::removeindex(const std::string& fname)
{
	int fd(::open(fname.c_str(), O_CLOEXEC | O_NOATIME | O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP));
	if (fd == -1)
		MapFile::throw_strerror("odb removeindex");
	try {
		removeindex(fd);
	} catch (...) {
		::close(fd);
		throw;
	}
	::close(fd);
}

void OSMStaticDB::removeindex(int fd)
{
	struct stat statbuf;
	if (fstat(fd, &statbuf))
		MapFile::throw_strerror("odb removeindex");
	uint64_t maxsz(dbsizenoindex(fd));
	if (true)
		std::cout << "Database file size " << statbuf.st_size << " without index " << maxsz << std::endl;
	BinFileHeader hdr;
	if (pread(fd, &hdr, sizeof(BinFileHeader), 0) != sizeof(BinFileHeader))
		MapFile::throw_strerror("odb removeindex reading header");
	for (layer_t l(layer_t::first); l <= layer_t::last; l = static_cast<layer_t>(static_cast<unsigned int>(l) + 1))
		hdr.set_rtreeoffs(l, 0);
	if (pwrite(fd, &hdr, sizeof(BinFileHeader), 0) != sizeof(BinFileHeader))
		MapFile::throw_strerror("odb removeindex writing header");
	if (maxsz < static_cast<uint64_t>(statbuf.st_size))
		ftruncate(fd, maxsz);
}

void OSMStaticDB::reindex(const std::string& tmpdir, const std::string& fname, size_t max_elements, size_t min_elements,
			  size_t reinserted_elements, size_t overlap_cost_threshold, bool bulkload)
{
	int fd(::open(fname.c_str(), O_CLOEXEC | O_NOATIME | O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP));
	if (fd == -1)
		MapFile::throw_strerror("odb reindex");
	try {
		reindex(tmpdir, fd, max_elements, min_elements, reinserted_elements, overlap_cost_threshold, bulkload);
	} catch (...) {
		::close(fd);
		throw;
	}
	::close(fd);
}

void OSMStaticDB::reindex(const std::string& tmpdir, int fd, size_t max_elements, size_t min_elements,
			  size_t reinserted_elements, size_t overlap_cost_threshold, bool bulkload)
{
	Index idx(tmpdir, fd, max_elements, min_elements, reinserted_elements, overlap_cost_threshold, bulkload);
	idx.addindex();
}

void OSMStaticDB::rtree_print_visitor(std::ostream& os, const BinFileRtreeInternalNode *node, const void *fbegin,
				      const BinFilePointEntry *tblb, const BinFilePointEntry *tble)
{
	if (!node)
		return;
	uint16_t n(node->get_nodes());
	os << "Internal Node " << (reinterpret_cast<const uint8_t *>(node) - reinterpret_cast<const uint8_t *>(fbegin))
	   << ", " << n << " children" << std::endl;
	if (node->is_pointstoleaf()) {
		for (uint16_t i(0); i < n; ++i) {
			Rect bbox(node->get_bbox(i));
			os << "  leaf child " << (reinterpret_cast<const uint8_t *>(node->get_leafnode(i)) - reinterpret_cast<const uint8_t *>(fbegin))
			   << ' ' << bbox.get_southwest().get_lat_str2() << ' ' << bbox.get_southwest().get_lon_str2()
			   << ' ' << bbox.get_northeast().get_lat_str2() << ' ' << bbox.get_northeast().get_lon_str2() << std::endl;
		}
		os << std::endl;
		for (uint16_t i(0); i < n; ++i)
			rtree_print_visitor(os, node->get_leafnode(i), fbegin, tblb, tble);
	} else {
		for (uint16_t i(0); i < n; ++i) {
			Rect bbox(node->get_bbox(i));
			os << "  internal child " << (reinterpret_cast<const uint8_t *>(node->get_leafnode(i)) - reinterpret_cast<const uint8_t *>(fbegin))
			   << ' ' << bbox.get_southwest().get_lat_str2() << ' ' << bbox.get_southwest().get_lon_str2()
			   << ' ' << bbox.get_northeast().get_lat_str2() << ' ' << bbox.get_northeast().get_lon_str2() << std::endl;
		}
		os << std::endl;
		for (uint16_t i(0); i < n; ++i)
			rtree_print_visitor(os, node->get_internalnode(i), fbegin, tblb, tble);
	}
}

void OSMStaticDB::rtree_print_visitor(std::ostream& os, const BinFileRtreeLeafNode *node, const void *fbegin,
				      const BinFilePointEntry *tblb, const BinFilePointEntry *tble)
{
	if (!node)
		return;
	uint16_t n(node->get_nodes());
	os << "Leaf Node " << (reinterpret_cast<const uint8_t *>(node) - reinterpret_cast<const uint8_t *>(fbegin))
	   << ", " << n << " children" << std::endl;
	for (uint16_t i(0); i < n; ++i) {
		os << "  object child " << node->get_index(i);
		const BinFilePointEntry *oi(tblb + node->get_index(i));
		if (oi < tble) {
			Point pt(oi->get_location());
			os << " ID " << oi->get_id() << ' ' << pt.get_lat_str2() << ' ' << pt.get_lon_str2();
		}
		os << std::endl;
	}
	os << std::endl;
}

void OSMStaticDB::rtree_print_visitor(std::ostream& os, const BinFileRtreeInternalNode *node, const void *fbegin,
				      const BinFileLineAreaEntry *tblb, const BinFileLineAreaEntry *tble)
{
	if (!node)
		return;
	uint16_t n(node->get_nodes());
	os << "Internal Node " << (reinterpret_cast<const uint8_t *>(node) - reinterpret_cast<const uint8_t *>(fbegin))
	   << ", " << n << " children" << std::endl;
	if (node->is_pointstoleaf()) {
		for (uint16_t i(0); i < n; ++i) {
			Rect bbox(node->get_bbox(i));
			os << "  leaf child " << (reinterpret_cast<const uint8_t *>(node->get_leafnode(i)) - reinterpret_cast<const uint8_t *>(fbegin))
			   << ' ' << bbox.get_southwest().get_lat_str2() << ' ' << bbox.get_southwest().get_lon_str2()
			   << ' ' << bbox.get_northeast().get_lat_str2() << ' ' << bbox.get_northeast().get_lon_str2() << std::endl;
		}
		os << std::endl;
		for (uint16_t i(0); i < n; ++i)
			rtree_print_visitor(os, node->get_leafnode(i), fbegin, tblb, tble);
	} else {
		for (uint16_t i(0); i < n; ++i) {
			Rect bbox(node->get_bbox(i));
			os << "  internal child " << (reinterpret_cast<const uint8_t *>(node->get_leafnode(i)) - reinterpret_cast<const uint8_t *>(fbegin))
			   << ' ' << bbox.get_southwest().get_lat_str2() << ' ' << bbox.get_southwest().get_lon_str2()
			   << ' ' << bbox.get_northeast().get_lat_str2() << ' ' << bbox.get_northeast().get_lon_str2() << std::endl;
		}
		os << std::endl;
		for (uint16_t i(0); i < n; ++i)
			rtree_print_visitor(os, node->get_internalnode(i), fbegin, tblb, tble);
	}
}

void OSMStaticDB::rtree_print_visitor(std::ostream& os, const BinFileRtreeLeafNode *node, const void *fbegin,
				      const BinFileLineAreaEntry *tblb, const BinFileLineAreaEntry *tble)
{
	if (!node)
		return;
	uint16_t n(node->get_nodes());
	os << "Leaf Node " << (reinterpret_cast<const uint8_t *>(node) - reinterpret_cast<const uint8_t *>(fbegin))
	   << ", " << n << " children" << std::endl;
	for (uint16_t i(0); i < n; ++i) {
		os << "  object child " << node->get_index(i);
		const BinFileLineAreaEntry *oi(tblb + node->get_index(i));
		if (oi < tble) {
			Rect bbox(oi->get_bbox());
			os << " ID " << oi->get_id() << ' ' << bbox.get_southwest().get_lat_str2() << ' ' << bbox.get_southwest().get_lon_str2()
			   << ' ' << bbox.get_northeast().get_lat_str2() << ' ' << bbox.get_northeast().get_lon_str2();
		}
		os << std::endl;
	}
	os << std::endl;
}

std::ostream& OSMStaticDB::print_index(std::ostream& os, layer_t layer) const
{
	if (!m_bin.is_open())
		return os;
	const BinFileHeader& hdr(*m_bin.begin_as<BinFileHeader>());
	switch (layer) {
	case layer_t::boundarylinesland:
	{
		layer_types<layer_t::boundarylinesland>::IndexRangeType idx(hdr.get_objdir_range<layer_t::boundarylinesland>());
		rtree_print_visitor(os, hdr.get_rtree_root(layer_t::boundarylinesland), &hdr, idx.first, idx.second);
		return os;
	}

	case layer_t::icesheetpolygons:
	{
		layer_types<layer_t::icesheetpolygons>::IndexRangeType idx(hdr.get_objdir_range<layer_t::icesheetpolygons>());
		rtree_print_visitor(os, hdr.get_rtree_root(layer_t::icesheetpolygons), &hdr, idx.first, idx.second);
		return os;
	}

	case layer_t::icesheetoutlines:
	{
		layer_types<layer_t::icesheetoutlines>::IndexRangeType idx(hdr.get_objdir_range<layer_t::icesheetoutlines>());
		rtree_print_visitor(os, hdr.get_rtree_root(layer_t::icesheetoutlines), &hdr, idx.first, idx.second);
		return os;
	}

	case layer_t::waterpolygons:
	{
		layer_types<layer_t::waterpolygons>::IndexRangeType idx(hdr.get_objdir_range<layer_t::waterpolygons>());
		rtree_print_visitor(os, hdr.get_rtree_root(layer_t::waterpolygons), &hdr, idx.first, idx.second);
		return os;
	}

	case layer_t::simplifiedwaterpolygons:
	{
		layer_types<layer_t::simplifiedwaterpolygons>::IndexRangeType idx(hdr.get_objdir_range<layer_t::simplifiedwaterpolygons>());
		rtree_print_visitor(os, hdr.get_rtree_root(layer_t::simplifiedwaterpolygons), &hdr, idx.first, idx.second);
		return os;
	}

	default:
		return os;
	}
}
