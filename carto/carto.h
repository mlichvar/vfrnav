//
// C++ Interface: carto
//
// Description: Carto system header
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef CARTO_HH
#define CARTO_HH

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <limits>
#include <vector>
#include <set>
#include <map>
#include <glibmm.h>

#include "geom.h"
#include "osmdb.h"
#include "osmsdb.h"
#include "cartocss.h"
#include "cartosql.h"

class HibernateWriteStream;
class HibernateReadStream;
class HibernateReadBuffer;

class Carto {
public:
	typedef CartoCSS::zoom_t zoom_t;

	class LayerBase {
	public:
		enum class layertype_t : uint8_t {
			null,
			osm,
			osmstatic,
			samelayer,
		};

		typedef OSMStaticDB::layer_t staticlayer_t;
		typedef CartoSQL::ExprTable::const_ptr_t sqlptr_t;
		typedef std::size_t samelayer_t;

		LayerBase(void);
		LayerBase(staticlayer_t sl);
		LayerBase(const sqlptr_t& expr);
		LayerBase(samelayer_t samelayer);
		LayerBase(const LayerBase& x);
		LayerBase(LayerBase&& x);
		~LayerBase();

		LayerBase& operator=(staticlayer_t sl);
		LayerBase& operator=(const sqlptr_t& expr);
		LayerBase& operator=(samelayer_t samelayer);
		LayerBase& operator=(const LayerBase& x);
		LayerBase& operator=(LayerBase&& x);

		layertype_t get_layertype(void) const { return m_layertype; }
		staticlayer_t get_staticlayer(void) const;
		const sqlptr_t& get_sqlexpr(void) const;
		samelayer_t get_samelayer(void) const;

	private:
		typedef std::string string_t;
		union {
			staticlayer_t m_staticlayer;
			sqlptr_t m_sqlexpr;
		        samelayer_t m_samelayer;
		};
		layertype_t m_layertype;
		void dealloc(void);
	};

	class Layer : protected LayerBase {
	public:
		using LayerBase::layertype_t;
		using LayerBase::staticlayer_t;
		using LayerBase::sqlptr_t;
		using LayerBase::samelayer_t;
		using LayerBase::get_layertype;
		using LayerBase::get_staticlayer;
		using LayerBase::get_sqlexpr;
		using LayerBase::get_samelayer;

		typedef Geometry::type_t geometry_t;

		Layer(void);

		const std::string& get_id(void) const { return m_id; };
		zoom_t get_minzoom(void) const { return m_minzoom; }
		zoom_t get_maxzoom(void) const { return m_maxzoom; }
		geometry_t get_geometry(void) const { return m_geometry; }
		bool is_save(void) const { return m_save; }

		void set_staticlayer(staticlayer_t sl) { LayerBase::operator=(sl); }
		void set_sqlexpr(const sqlptr_t& expr) { LayerBase::operator=(expr); }
		void set_samelayer(samelayer_t samelayer) { LayerBase::operator=(samelayer); }
		void set_id(const std::string& id) { m_id = id; }
		void set_minzoom(zoom_t z) { m_minzoom = z; }
		void set_maxzoom(zoom_t z) { m_maxzoom = z; }
		void set_geometry(geometry_t g) { m_geometry = g; }
		void set_save(bool s) { m_save = s; }

	protected:
		std::string m_id;
		zoom_t m_minzoom;
		zoom_t m_maxzoom;
		geometry_t m_geometry;
		bool m_save;
	};

	class Layers : public std::vector<Layer> {
	public:
	};

	class Error : public std::runtime_error {
	public:
		explicit Error(const std::string& x);
	};

	class SQLError : public CartoSQL::SQLError {
	public:
		SQLError(const CartoSQL::SQLError& x, const std::string& layerid, const std::string& expr);

		const std::string& get_layerid(void) const { return m_layerid; }
		const std::string& get_sqlexpr(void) const { return m_sqlexpr; }

	protected:
		std::string m_layerid;
		std::string m_sqlexpr;
	};

	Carto(void);
	Carto(const std::string& fn);

	void load_project(const std::string& fn, std::ostream *msg = nullptr);

	void parse_project(std::istream& is, const std::string& dirname, std::ostream *msg = nullptr);
	void parse_project(const std::string& fn, std::ostream *msg = nullptr);
	void parse_style(std::istream& is, std::ostream *msg = nullptr) { m_sql.parse_style(is, msg); }
	void parse_style(const std::string& fn, std::ostream *msg = nullptr) { m_sql.parse_style(fn, msg); }

	const CartoCSS& get_css(void) const { return m_css; }
	const CartoSQL& get_sql(void) const { return m_sql; }
	const Layers& get_layers(void) const { return m_layers; }
	const std::string& get_srs(void) const { return m_srs; }
	zoom_t get_minzoom(void) const { return m_minzoom; }
	zoom_t get_maxzoom(void) const { return m_maxzoom; }

	const OSMDB *get_osmdb(void) const { return m_sql.get_osmdb(); }
	void set_osmdb(const OSMDB *odb = nullptr) { m_sql.set_osmdb(odb); }
	const OSMStaticDB *get_osmstaticdb(void) const { return m_osmstaticdb; }
	void set_osmstaticdb(const OSMStaticDB *sdb = nullptr) { m_osmstaticdb = sdb; }

protected:
	CartoCSS m_css;
	CartoSQL m_sql;
	Layers m_layers;
	std::string m_srs;
	const OSMStaticDB *m_osmstaticdb;
	zoom_t m_minzoom;
	zoom_t m_maxzoom;
};

const std::string& to_str(Carto::LayerBase::layertype_t x);
inline std::ostream& operator<<(std::ostream& os, Carto::LayerBase::layertype_t x) { return os << to_str(x); }

#endif /* CARTO_HH */
