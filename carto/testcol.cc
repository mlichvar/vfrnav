/*****************************************************************************/

/*
 *      testcol.cc  --  Test Colors
 *
 *      Copyright (C) 2020  Thomas Sailer (t.sailer@alumni.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/*****************************************************************************/

#include <getopt.h>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <cstring>

#ifdef HAVE_SYSEXITS_H
#include <sysexits.h>
#else
#define EX_USAGE   64
#define EX_DATAERR 65
#define EX_OK      0
#endif

#include "cartocss.h"

static inline double sqr(double x) { return x * x; }

static int hsltest(void)
{
	static constexpr unsigned int nrtest = 10000;
	static constexpr double tol = 0.000001;
	static constexpr bool verbose = false;
	int ret(EX_OK);
	for (unsigned int i = 0; i < nrtest; ++i) {
		CartoCSS::Color col1(rand() * (1.0 / RAND_MAX), rand() * (1.0 / RAND_MAX), rand() * (1.0 / RAND_MAX), 1);
		CartoCSS::Color col2(0, 0, 0, 1);
		double h, s, l;
		col1.get_hsl(h, s, l);
		col2.set_hsl(h, s, l);
		double e(sqr(col1.get_red() - col2.get_red()) +
			 sqr(col1.get_green() - col2.get_green()) +
			 sqr(col1.get_blue() - col2.get_blue()));
		if (e > tol)
			ret = EX_DATAERR;
		if (!verbose && e <= tol)
			continue;
		std::cerr << std::setprecision(3) << std::fixed
			  << "Color: " << col1.get_red() << ' ' << col1.get_green() << ' ' << col1.get_blue()
			  << " hsl: " << h << ' ' << s << ' ' << l
			  << " rgb: " << col2.get_red() << ' ' << col2.get_green() << ' ' << col2.get_blue()
			  << " err: " << e << std::endl;
	}
	return ret;	
}

int main(int argc, char *argv[])
{
	static struct option long_options[] = {
		{ "hsl",     required_argument, 0, 'H' },
		{ "red",     required_argument, 0, 'r' },
		{ "green",   required_argument, 0, 'g' },
		{ "blue",    required_argument, 0, 'b' },
		{ "alpha",   required_argument, 0, 'a' },
		{ "rgb",     required_argument, 0, 0x400 },
		{ "hsltest", optional_argument, 0, 0x401 },
		{ 0, 0, 0, 0 }
	};
	int c, err(0);
	CartoCSS::Color col(0, 0, 0, 1);
	enum class mode_t : uint8_t {
		colconv,
		hsltest
	};
	mode_t mode(mode_t::colconv);
	
	while ((c = getopt_long(argc, argv, "H:", long_options, 0)) != EOF) {
		switch (c) {
		case 'H':
		{
			if (!optarg)
				break;
			double h(0), s(0), l(0), a(0);
			char *cp1(0);
			h = strtod(optarg, &cp1);
			if (cp1 == optarg || (*cp1 && *cp1 != ',')) {
				std::cerr << "Error parsing hsl: \"" << optarg << '"' << std::endl;
				break;
			}
			if (!*cp1) {
				col.set_hsl(h, s, l);
				break;
			}
			const char *cp(cp1 + 1);
			s = strtod(cp, &cp1);
			if (cp1 == cp || (*cp1 && *cp1 != ',')) {
				std::cerr << "Error parsing hsl: \"" << optarg << '"' << std::endl;
				break;
			}
			if (!*cp1) {
				col.set_hsl(h, s, l);
				break;
			}
			cp = cp1 + 1;
			l = strtod(cp, &cp1);
			if (cp1 == cp || (*cp1 && *cp1 != ',')) {
				std::cerr << "Error parsing hsl: \"" << optarg << '"' << std::endl;
				break;
			}
			col.set_hsl(h, s, l);
			if (!*cp1)
				break;
			cp = cp1 + 1;
			a = strtod(cp, &cp1);
			if (cp1 == cp || *cp1) {
				std::cerr << "Error parsing hsl: \"" << optarg << '"' << std::endl;
				break;
			}
			col.set_alpha(a);
			break;
		}

		case 0x400:
		{
			if (!optarg)
				break;
			char *cp1(0);
			if (*optarg == '#') {
				size_t sz(strlen(optarg + 1));
				unsigned long val(strtoul(optarg + 1, &cp1, 16));
				if (cp1 == optarg + 1 || *cp1) {
					std::cerr << "Error parsing rgb: \"" << optarg << '"' << std::endl;
					break;
				}
				switch (sz) {
				case 3:
					col.set_rgba_uint4((val >> 8) & 15, (val >> 4) & 15, val & 15);
					break;

				case 4:
					col.set_rgba_uint4((val >> 12) & 15, (val >> 8) & 15, (val >> 4) & 15, val & 15);
					break;

				case 6:
					col.set_rgba_uint8((val >> 16) & 255, (val >> 8) & 255, val & 255);
					break;

				case 8:
					col.set_rgba_uint8((val >> 24) & 255, (val >> 16) & 255, (val >> 8) & 255, val & 255);
					break;

				default:
					std::cerr << "Error parsing rgb: \"" << optarg << '"' << std::endl;
					break;
				}
				break;
			}
			double x(strtod(optarg, &cp1));
			if (cp1 == optarg || (*cp1 && *cp1 != ',')) {
				std::cerr << "Error parsing rgb: \"" << optarg << '"' << std::endl;
				break;
			}
			col.set_red(x);
			if (!*cp1)
				break;
			const char *cp(cp1 + 1);
			x = strtod(cp, &cp1);
			if (cp1 == cp || (*cp1 && *cp1 != ',')) {
				std::cerr << "Error parsing rgb: \"" << optarg << '"' << std::endl;
				break;
			}
			col.set_green(x);
			if (!*cp1)
				break;
			cp = cp1 + 1;
			x = strtod(cp, &cp1);
			if (cp1 == cp || (*cp1 && *cp1 != ',')) {
				std::cerr << "Error parsing rgb: \"" << optarg << '"' << std::endl;
				break;
			}
			col.set_blue(x);
			if (!*cp1)
				break;
			cp = cp1 + 1;
			x = strtod(cp, &cp1);
			if (cp1 == cp || *cp1) {
				std::cerr << "Error parsing rgb: \"" << optarg << '"' << std::endl;
				break;
			}
			col.set_alpha(x);
			break;
		}

		case 'r':
		{
			if (!optarg)
				break;
			char *cp1(0);
			double x(strtod(optarg, &cp1));
			if (cp1 == optarg || *cp1) {
				std::cerr << "Error parsing red: \"" << optarg << '"' << std::endl;
				break;
			}
			col.set_red(x);
			break;
		}

		case 'g':
		{
			if (!optarg)
				break;
			char *cp1(0);
			double x(strtod(optarg, &cp1));
			if (cp1 == optarg || *cp1) {
				std::cerr << "Error parsing green: \"" << optarg << '"' << std::endl;
				break;
			}
			col.set_green(x);
			break;
		}

		case 'b':
		{
			if (!optarg)
				break;
			char *cp1(0);
			double x(strtod(optarg, &cp1));
			if (cp1 == optarg || *cp1) {
				std::cerr << "Error parsing blue: \"" << optarg << '"' << std::endl;
				break;
			}
			col.set_blue(x);
			break;
		}

		case 'a':
		{
			if (!optarg)
				break;
			char *cp1(0);
			double x(strtod(optarg, &cp1));
			if (cp1 == optarg || *cp1) {
				std::cerr << "Error parsing alpha: \"" << optarg << '"' << std::endl;
				break;
			}
			col.set_alpha(x);
			break;
		}

		case 0x401:
			if (optarg)
				srand(strtoul(optarg, 0, 0));
			mode = mode_t::hsltest;
			break;

		default:
			++err;
			break;
		}
	}
	if (err) {
		std::cerr << "usage: testcol" << std::endl;
		return EX_USAGE;
	}
	try {
		switch (mode) {
		default:
		{
			double h, s, l;
			col.get_hsl(h, s, l);
			std::cout << "VALID:  " << (col.is_valid() ? "YES" : "NO") << std::endl << std::fixed << std::setprecision(3)
				  << "RED:    " << col.get_red() << " (" << static_cast<int>(col.get_red() * 255) << ')' << std::endl
				  << "GREEN:  " << col.get_green() << " (" << static_cast<int>(col.get_green() * 255) << ')' << std::endl
				  << "BLUE:   " << col.get_blue() << " (" << static_cast<int>(col.get_blue() * 255) << ')' << std::endl
				  << "ALPHA:  " << col.get_alpha() << " (" << static_cast<int>(col.get_alpha() * 255) << ')' << std::endl
				  << "HUE:    " << h << std::endl
				  << "SAT:    " << s << std::endl
				  << "LIGHT:  " << l << std::endl
				  << "WEB:    #" << std::hex
				  << std::setw(2) << std::setfill('0') << static_cast<int>(col.get_red() * 255)
				  << std::setw(2) << std::setfill('0') << static_cast<int>(col.get_green() * 255)
				  << std::setw(2) << std::setfill('0') << static_cast<int>(col.get_blue() * 255)
				  << std::setw(2) << std::setfill('0') << static_cast<int>(col.get_alpha() * 255) << std::endl;
			break;
		}

		case mode_t::hsltest:
			return hsltest();
		}
	} catch (const std::exception& e) {
		std::cerr << "Test failure: " << e.what() << std::endl;
		return EX_DATAERR;
	}
	return EX_OK;
}
