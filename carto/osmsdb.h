//
// C++ Interface: osmsdb
//
// Description: OpenStreetMap proprietary database for static data
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef OSMSDB_H
#define OSMSDB_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "sysdeps.h"
#include "mapfile.h"
#include "hibernate.h"
#include "geom.h"

#include <tuple>
#include <initializer_list>
#include <boost/smart_ptr/intrusive_ptr.hpp>

class OSMStaticDB {
protected:
	class BinFileHeader;
	class BinFileRtreeInternalNode;
	class BinFileRtreeLeafNode;

public:
	enum class layer_t : uint8_t {
		first = 0,
		boundarylinesland = first,
		icesheetpolygons,
		icesheetoutlines,
		waterpolygons,
		simplifiedwaterpolygons,
		last = simplifiedwaterpolygons
	};

	class Object {
	public:
		typedef boost::intrusive_ptr<Object> ptr_t;
		typedef boost::intrusive_ptr<const Object> const_ptr_t;

		typedef int64_t id_t;

		enum class tagkey_t : uint16_t {
			invalid = static_cast<std::underlying_type<tagkey_t>::type>(~0U)
		};

		enum class tagname_t : uint32_t {
			invalid = static_cast<std::underlying_type<tagname_t>::type>(~0U)
		};

		typedef std::tuple<tagkey_t, tagname_t> tag_t;
		typedef std::vector<tag_t> tags_t; // must be sorted by key

		class TagSorter {
		public:
			bool operator()(tagkey_t a, tagkey_t b) const { return a < b; }
			bool operator()(const tag_t& a, tagkey_t b) const { return std::get<tagkey_t>(a) < b; }
			bool operator()(tagkey_t a, const tag_t& b) const { return a < std::get<tagkey_t>(b); }
			bool operator()(const tag_t& a, const tag_t& b) const { return std::get<tagkey_t>(a) < std::get<tagkey_t>(b); }
		};

		enum class type_t {
			point,
			line,
			area
		};

		Object(id_t id = 0, const tags_t& tags = tags_t());
		virtual ~Object();

		id_t get_id(void) const { return m_id; }
		void set_id(id_t id) { m_id = id; }
		const tags_t& get_tags(void) const { return m_tags; }
		tags_t& get_tags(void) { return m_tags; }
		void set_tags(const tags_t& tags) { m_tags = tags; }
		void sort_tags(void) { sort_tags(m_tags); }
		static void sort_tags(tags_t& t) { std::sort(t.begin(), t.end(), TagSorter()); }
		tagname_t find_tag(tagkey_t key) const;

		virtual type_t get_type(void) const = 0;
		virtual Rect get_bbox(void) const = 0;

		unsigned int breference(void) const { return ++m_refcount; }
		unsigned int bunreference(void) const { return --m_refcount; }
		unsigned int get_refcount(void) const { return m_refcount; }
		ptr_t get_ptr(void) { return ptr_t(this); }
		const_ptr_t get_ptr(void) const { return const_ptr_t(this); }
		friend inline void intrusive_ptr_add_ref(const Object* expr) { expr->breference(); }
		friend inline void intrusive_ptr_release(const Object* expr) { if (!expr->bunreference()) delete expr; }

		enum class printflags_t {
			none      = 0,
			tags      = 1 << 0,
			coords    = 1 << 1,
			skyvector = 1 << 2,
			default_  = tags
		};

		virtual std::ostream& print(std::ostream& os, const OSMStaticDB& db, printflags_t flags = printflags_t::default_) const;

		virtual void load(HibernateReadStream& ar) = 0;
		virtual void load(HibernateReadBuffer& ar) = 0;
		virtual void save(HibernateWriteStream& ar) const = 0;
		virtual void loaddb(HibernateReadStream& ar) = 0;
		virtual void loaddb(HibernateReadBuffer& ar) = 0;
		virtual void savedb(HibernateWriteStream& ar) const = 0;
		static ptr_t create(type_t t);

		template<class Archive> void hibernate_noid(Archive& ar) {
			uint32_t n(m_tags.size());
			ar.ioleb(n);
			if (ar.is_load())
				m_tags.resize(n);
			for (tags_t::iterator i(m_tags.begin()), e(m_tags.end()); i != e; ++i) {
				ar.io(reinterpret_cast<std::underlying_type<tagkey_t>::type&>(std::get<tagkey_t>(*i)));
				ar.ioleb(reinterpret_cast<std::underlying_type<tagname_t>::type&>(std::get<tagname_t>(*i)));
			}
		}

		template<class Archive> void hibernate(Archive& ar) {
			ar.io(m_id);
			hibernate_noid(ar);
		}

	protected:
		tags_t m_tags;
		id_t m_id;
		mutable std::atomic<unsigned int> m_refcount;
	};

	class ObjPoint : public Object {
	public:
		typedef boost::intrusive_ptr<ObjPoint> ptr_t;
		typedef boost::intrusive_ptr<const ObjPoint> const_ptr_t;
		static constexpr type_t object_type = type_t::point;
		static const std::string object_name;

		ObjPoint(id_t id = 0, const tags_t& tags = tags_t(), const Point& loc = Point::invalid);

		const Point& get_location(void) const { return m_location; }
		void set_location(const Point& loc) { m_location = loc; }

		virtual type_t get_type(void) const { return object_type; }
		virtual Rect get_bbox(void) const { return Rect(m_location, m_location); }

		virtual std::ostream& print(std::ostream& os, const OSMStaticDB& db, printflags_t flags = printflags_t::default_) const;

		virtual void load(HibernateReadStream& ar);
		virtual void load(HibernateReadBuffer& ar);
		virtual void save(HibernateWriteStream& ar) const;
		virtual void loaddb(HibernateReadStream& ar);
		virtual void loaddb(HibernateReadBuffer& ar);
		virtual void savedb(HibernateWriteStream& ar) const;

		template<class Archive> void hibernate_noid(Archive& ar) {
			Object::hibernate_noid(ar);
			m_location.hibernate_binary(ar);
		}

		template<class Archive> void hibernate(Archive& ar) {
			Object::hibernate(ar);
			m_location.hibernate_binary(ar);
		}

	protected:
		Point m_location;
	};

	class ObjLine : public Object {
	public:
		typedef boost::intrusive_ptr<ObjLine> ptr_t;
		typedef boost::intrusive_ptr<const ObjLine> const_ptr_t;
		static constexpr type_t object_type = type_t::line;
		static const std::string object_name;

		ObjLine(id_t id = 0, const tags_t& tags = tags_t(), const MultiLineString& ln = MultiLineString());

		const MultiLineString& get_line(void) const { return m_line; }
		void set_line(const MultiLineString& ln) { m_line = ln; }

		virtual type_t get_type(void) const { return object_type; }
		virtual Rect get_bbox(void) const { return m_line.get_bbox(); }

		virtual std::ostream& print(std::ostream& os, const OSMStaticDB& db, printflags_t flags = printflags_t::default_) const;

		virtual void load(HibernateReadStream& ar);
		virtual void load(HibernateReadBuffer& ar);
		virtual void save(HibernateWriteStream& ar) const;
		virtual void loaddb(HibernateReadStream& ar);
		virtual void loaddb(HibernateReadBuffer& ar);
		virtual void savedb(HibernateWriteStream& ar) const;

		template<class Archive> void hibernate_noid(Archive& ar) {
			Object::hibernate_noid(ar);
			m_line.hibernate_binary(ar);
		}

		template<class Archive> void hibernate(Archive& ar) {
			Object::hibernate(ar);
			m_line.hibernate_binary(ar);
		}

	protected:
		MultiLineString m_line;
	};

	class ObjArea : public Object {
	public:
		typedef boost::intrusive_ptr<ObjArea> ptr_t;
		typedef boost::intrusive_ptr<const ObjArea> const_ptr_t;
		static constexpr type_t object_type = type_t::area;
		static const std::string object_name;

		ObjArea(id_t id = 0, const tags_t& tags = tags_t(), const MultiPolygonHole& a = MultiPolygonHole());

		const MultiPolygonHole& get_area(void) const { return m_area; }
		void set_area(const MultiPolygonHole& a) { m_area = a; }

		virtual type_t get_type(void) const { return object_type; }
		virtual Rect get_bbox(void) const { return m_area.get_bbox(); }

		virtual std::ostream& print(std::ostream& os, const OSMStaticDB& db, printflags_t flags = printflags_t::default_) const;

		virtual void load(HibernateReadStream& ar);
		virtual void load(HibernateReadBuffer& ar);
		virtual void save(HibernateWriteStream& ar) const;
		virtual void loaddb(HibernateReadStream& ar);
		virtual void loaddb(HibernateReadBuffer& ar);
		virtual void savedb(HibernateWriteStream& ar) const;

		template<class Archive> void hibernate_noid(Archive& ar) {
			Object::hibernate_noid(ar);
			m_area.hibernate_binary(ar);
		}

		template<class Archive> void hibernate(Archive& ar) {
			Object::hibernate(ar);
			m_area.hibernate_binary(ar);
		}

	protected:
		MultiPolygonHole m_area;
	};

	class Statistics {
	public:
		class RTree {
		public:
			RTree(void);
			RTree(const BinFileRtreeInternalNode *node);

			size_t get_bytes(void) const { return m_bytes; }
			unsigned int get_objects(void) const { return m_objects; }
			unsigned int get_pages(void) const { return m_pages; }
			unsigned int get_depth(void) const { return m_depth; }

			bool is_valid(void) const;
			std::string to_str(void) const;

		protected:
			size_t m_bytes;
			unsigned int m_objects;
			unsigned int m_pages;
			unsigned int m_depth;

			void depth_first_visit(const BinFileRtreeInternalNode *node, unsigned int depth);
			void depth_first_visit(const BinFileRtreeLeafNode *node, unsigned int depth);
		};

		Statistics(void);
		Statistics(const BinFileHeader& hdr, bool dortree = true);

		unsigned int get_objects(layer_t layer) const;
		const RTree& get_rtree(layer_t layer) const;
		unsigned int get_tagkeys(void) const { return m_tagkeys; }
		unsigned int get_tagnames(void) const { return m_tagnames; }

		std::string to_str(void) const;

	protected:
		RTree m_rtrees[static_cast<unsigned int>(layer_t::last) + 1];
		unsigned int m_objects[static_cast<unsigned int>(layer_t::last) + 1];
		unsigned int m_tagkeys;
		unsigned int m_tagnames;
	};

	OSMStaticDB(const std::string& path = "");
	~OSMStaticDB();

	const std::string& get_path(void) const { return m_path; }
	void set_path(const std::string& path = "");
	void open(void);
	void close(void);
	bool is_open(void) const { return m_bin.is_open(); }

	typedef std::vector<Object::const_ptr_t> objects_t;
	objects_t find(layer_t layer) const;
	objects_t find(const Rect& bbox, layer_t layer) const;
	objects_t find(Object::id_t id, layer_t layer) const;

	Object::const_ptr_t get(uint32_t index, layer_t layer) const;

	const char *find_tagkey(Object::tagkey_t key) const;
	Object::tagkey_t find_tagkey(const std::string& tag) const;
	bool set_tagkey(std::string& v, Object::tagkey_t key) const;

	const char *find_tagname(Object::tagname_t key) const;
	Object::tagname_t find_tagname(const std::string& tag) const;
	bool set_tagname(std::string& v, Object::tagname_t key) const;

	class TagNameSet : public std::set<Object::tagname_t> {
	public:
		bool is_in_set(Object::tagname_t x) const { return find(x) != end(); }
	};
		
	TagNameSet create_tagnameset(const std::vector<std::string>& x) const;
	TagNameSet create_tagnameset(const char * const *x) const;
	TagNameSet create_tagnameset(std::initializer_list<const char *> x) const;

	Statistics get_statistics(bool dortree = true) const;
	std::ostream& print_index(std::ostream& os, layer_t layer) const;

	static Object::type_t get_objtype(layer_t layer);

	enum class importflags_t {
		none       = 0x00,
		keep       = 0x01,
		noindex    = 0x02
	};

	class ImportSourceFiles {
	public:
		ImportSourceFiles(void);
		void set_default(void);
		void set_default(const std::string& dir);
		const std::string& get_file(layer_t layer) const;
		void set_file(layer_t layer, const std::string& file);

	protected:
		static const char * const default_filenames[];
		std::string m_file[static_cast<unsigned int>(OSMStaticDB::layer_t::last) + 1];
	};

	static void import(const std::string& tmpdir, const std::string& outfname, const ImportSourceFiles& infnames, importflags_t flags = importflags_t::none);
	static void import(const std::string& tmpdir, int outfd, const ImportSourceFiles& infnames, importflags_t flags = importflags_t::none);
	static uint64_t dbsizenoindex(int fd);
	static constexpr unsigned int rtreedefaultmaxelperpage = 32;
	static void reindex(const std::string& tmpdir, const std::string& fname,
			    size_t max_elements = rtreedefaultmaxelperpage, size_t min_elements = std::numeric_limits<size_t>::max(),
			    size_t reinserted_elements = std::numeric_limits<size_t>::max(), size_t overlap_cost_threshold = 32,
			    bool bulkload = true);
	static void reindex(const std::string& tmpdir, int fd,
			    size_t max_elements = rtreedefaultmaxelperpage, size_t min_elements = std::numeric_limits<size_t>::max(),
			    size_t reinserted_elements = std::numeric_limits<size_t>::max(), size_t overlap_cost_threshold = 32,
			    bool bulkload = true);
	static void removeindex(const std::string& fname);
	static void removeindex(int fd);

	static std::ostream& shapeinfo(std::ostream& os, const std::string& name, unsigned int maxfeatures = std::numeric_limits<unsigned int>::max());

	template<layer_t layer> struct layer_data_type {
	};

protected:
	static constexpr uint64_t aligninc = 15U;
	static constexpr uint64_t alignmask = ~aligninc;

	template<Object::type_t typ> struct obj_types {
	};

	template<layer_t layer> struct layer_types {
		static const Object::type_t objtype = layer_data_type<layer>::objtype;
		typedef typename obj_types<objtype>::ObjType ObjType;
		typedef typename obj_types<objtype>::IndexType IndexType;
		typedef typename obj_types<objtype>::IndexRangeType IndexRangeType;
		typedef typename obj_types<objtype>::BinFileEntry BinFileEntry;
	};

	template<int sz> class BinFileEntry {
	public:
		static const unsigned int size = sz;

		BinFileEntry(void);

		uint8_t readu8(unsigned int idx) const;
		uint16_t readu16(unsigned int idx) const;
		uint32_t readu24(unsigned int idx) const;
		uint32_t readu32(unsigned int idx) const;
		uint64_t readu40(unsigned int idx) const;
		uint64_t readu48(unsigned int idx) const;
		uint64_t readu64(unsigned int idx) const;
		int8_t reads8(unsigned int idx) const { return readu8(idx); }
		int16_t reads16(unsigned int idx) const { return readu16(idx); }
		int32_t reads24(unsigned int idx) const { return sext24(readu24(idx)); }
		int32_t reads32(unsigned int idx) const { return readu32(idx); }
		int64_t reads40(unsigned int idx) const { return sext48(readu40(idx)); }
		int64_t reads48(unsigned int idx) const { return sext48(readu48(idx)); }
		int64_t reads64(unsigned int idx) const { return readu64(idx); }

		void writeu8(unsigned int idx, uint8_t v);
		void writeu16(unsigned int idx, uint16_t v);
		void writeu24(unsigned int idx, uint32_t v);
		void writeu32(unsigned int idx, uint32_t v);
		void writeu40(unsigned int idx, uint64_t v);
		void writeu48(unsigned int idx, uint64_t v);
		void writeu64(unsigned int idx, uint64_t v);
		void writes8(unsigned int idx, int8_t v) { writeu8(idx, v); }
		void writes16(unsigned int idx, int16_t v) { writeu16(idx, v); }
		void writes24(unsigned int idx, int32_t v) { writeu24(idx, v); }
		void writes32(unsigned int idx, int32_t v) { writeu32(idx, v); }
		void writes40(unsigned int idx, int64_t v) { writeu40(idx, v); }
		void writes48(unsigned int idx, int64_t v) { writeu48(idx, v); }
		void writes64(unsigned int idx, int64_t v) { writeu64(idx, v); }

	protected:
		uint8_t m_data[sz];

		static inline int32_t sext24(uint32_t v) {
			v &= 0x00ffffff;
			v |= -(v & 0x00800000);
			return v;
		}

		static inline int64_t sext40(uint64_t v) {
			v &= 0x000000ffffffffff;
			v |= -(v & 0x0000008000000000);
			return v;
		}

		static inline int64_t sext48(uint64_t v) {
			v &= 0x0000ffffffffffff;
			v |= -(v & 0x0000800000000000);
			return v;
		}
	};

	class BinFilePointEntry;
	class BinFileLineAreaEntry;
	class BinFileTagKeyEntry;
	class BinFileTagNameEntry;

	class BinFileHeader : public BinFileEntry<256> {
	public:
		bool check_signature(void) const;
		void set_signature(void);

		uint64_t get_objdiroffs(layer_t layer) const;
		void set_objdiroffs(layer_t layer, uint64_t offs);
		uint32_t get_objdirentries(layer_t layer) const;
		void set_objdirentries(layer_t layer, uint32_t n);

		template<layer_t layer>
		typename layer_types<layer>::IndexRangeType get_objdir_range(void) const;

		uint64_t get_rtreeoffs(layer_t layer) const;
		void set_rtreeoffs(layer_t layer, uint64_t offs);
		const BinFileRtreeInternalNode *get_rtree_root(layer_t layer) const;

		uint64_t get_tagkeyoffs(void) const;
		void set_tagkeyoffs(uint64_t offs);
		uint32_t get_tagkeyentries(void) const;
		void set_tagkeyentries(uint32_t n);
		const BinFileTagKeyEntry *get_tagkey_begin(void) const;
		const BinFileTagKeyEntry *get_tagkey_end(void) const;

		uint64_t get_tagnameoffs(void) const;
		void set_tagnameoffs(uint64_t offs);
		uint32_t get_tagnameentries(void) const;
		void set_tagnameentries(uint32_t n);
		const BinFileTagNameEntry *get_tagname_begin(void) const;
		const BinFileTagNameEntry *get_tagname_end(void) const;

	protected:
		static const char signature[];
	};

	class BinFilePointEntry : public BinFileEntry<24> {
	public:
		void set_id(Object::id_t id);
		Object::id_t get_id(void) const;
		bool is_match(const Rect& bbox) const { return bbox.is_inside(get_location()); }
		Rect get_bbox(void) const { return Rect(get_location(), get_location()); }
		Point get_location(void) const;
		void set_location(const Point& location);
		uint64_t get_dataoffs(void) const;
		void set_dataoffs(uint64_t offs);
		const uint8_t *get_dataptr(void) const;
		uint32_t get_datasize(void) const;
		void set_datasize(uint32_t sz);
	};

	class BinFileLineAreaEntry : public BinFileEntry<32> {
	public:
		void set_id(Object::id_t id);
		Object::id_t get_id(void) const;
		bool is_match(const Rect& bbox) const { return bbox.is_intersect(get_bbox()); }
		Rect get_bbox(void) const;
		void set_bbox(const Rect& bbox);
		uint64_t get_dataoffs(void) const;
		void set_dataoffs(uint64_t offs);
		const uint8_t *get_dataptr(void) const;
		uint32_t get_datasize(void) const;
		void set_datasize(uint32_t sz);
	};

	struct BinFileIDSorter {
		bool operator()(const Object::id_t& a, const Object::id_t& b) const { return a < b; }
		bool operator()(const BinFilePointEntry& a, const Object::id_t& b) const { return operator()(a.get_id(), b); }
		bool operator()(const Object::id_t& a, const BinFilePointEntry& b) const { return operator()(a, b.get_id()); }
		bool operator()(const BinFilePointEntry& a, const BinFilePointEntry& b) const { return operator()(a.get_id(), b.get_id()); }
		bool operator()(const BinFileLineAreaEntry& a, const Object::id_t& b) const { return operator()(a.get_id(), b); }
		bool operator()(const Object::id_t& a, const BinFileLineAreaEntry& b) const { return operator()(a, b.get_id()); }
		bool operator()(const BinFileLineAreaEntry& a, const BinFileLineAreaEntry& b) const { return operator()(a.get_id(), b.get_id()); }
	};

	class BinFileTagKeyEntry : public BinFileEntry<4> {
	public:
		uint64_t get_stroffs(void) const;
		void set_stroffs(uint64_t offs);
		const char *get_strptr(void) const;
	};

	struct BinFileTagKeySorter {
		bool operator()(const char *a, const char *b) const;
		bool operator()(const BinFileTagKeyEntry& a, const char *b) const { return operator()(a.get_strptr(), b); }
		bool operator()(const char *a, const BinFileTagKeyEntry& b) const { return operator()(a, b.get_strptr()); }
		bool operator()(const BinFileTagKeyEntry& a, const BinFileTagKeyEntry& b) const { return operator()(a.get_strptr(), b.get_strptr()); }
	};

	class BinFileTagNameEntry : public BinFileEntry<4> {
	public:
		uint64_t get_stroffs(void) const;
		void set_stroffs(uint64_t offs);
		const char *get_strptr(void) const;
	};

	struct BinFileTagNameSorter {
		bool operator()(const char *a, const char *b) const;
		bool operator()(const BinFileTagNameEntry& a, const char *b) const { return operator()(a.get_strptr(), b); }
		bool operator()(const char *a, const BinFileTagNameEntry& b) const { return operator()(a, b.get_strptr()); }
		bool operator()(const BinFileTagNameEntry& a, const BinFileTagNameEntry& b) const { return operator()(a.get_strptr(), b.get_strptr()); }
	};

	static constexpr unsigned int rtreemaxelperpage = 1024;

	class BinFileRtreeLeafNode : public BinFileEntry<2+rtreemaxelperpage*4> {
	public:
		uint16_t get_nodes(void) const;
		void set_nodes(uint16_t n);
		std::size_t get_size(void) const;
		typedef uint32_t index_t;
		index_t get_index(uint16_t n) const;
		void set_index(uint16_t n, index_t index);
	};

	class BinFileRtreeInternalNode : public BinFileEntry<2+rtreemaxelperpage*24> {
	public:
		uint16_t get_nodes(void) const;
		void set_nodes(uint16_t n);
		bool is_pointstoleaf(void) const;
		void set_pointstoleaf(bool l);
		std::size_t get_size(void) const;
		int64_t get_addroffs(uint16_t n) const;
		void set_addroffs(uint16_t n, int64_t addr);
		const BinFileRtreeLeafNode *get_leafnode(uint16_t n) const;
		const BinFileRtreeInternalNode *get_internalnode(uint16_t n) const;
		Rect get_bbox(uint16_t n) const;
		void set_bbox(uint16_t n, const Rect& bbox);
	};

	class Import;
	class Index;

protected:
	std::string m_path;
	MapFile m_bin;

	template<OSMStaticDB::Object::type_t typ>
	typename obj_types<typ>::ObjType::const_ptr_t load_object(const typename obj_types<typ>::IndexType *pe) const;

	template<OSMStaticDB::layer_t layer>
	void find_object(objects_t& obj) const;

	template<OSMStaticDB::layer_t layer>
	void find_object(objects_t& obj, Object::id_t id) const;

	template<OSMStaticDB::layer_t layer>
	void find_objects(objects_t& obj, const Rect& bbox) const;

	typedef std::vector<BinFileRtreeLeafNode::index_t> rtree_ids_t;
	static void rtree_visitor(rtree_ids_t& ids, const Rect& bbox, const BinFileRtreeInternalNode *node);
	static void rtree_visitor(rtree_ids_t& ids, const BinFileRtreeLeafNode *node);

	static void rtree_print_visitor(std::ostream& os, const BinFileRtreeInternalNode *node, const void *fbegin,
					const BinFilePointEntry *tblb, const BinFilePointEntry *tble);
	static void rtree_print_visitor(std::ostream& os, const BinFileRtreeLeafNode *node, const void *fbegin,
					const BinFilePointEntry *tblb, const BinFilePointEntry *tble);

	static void rtree_print_visitor(std::ostream& os, const BinFileRtreeInternalNode *node, const void *fbegin,
					const BinFileLineAreaEntry *tblb, const BinFileLineAreaEntry *tble);
	static void rtree_print_visitor(std::ostream& os, const BinFileRtreeLeafNode *node, const void *fbegin,
					const BinFileLineAreaEntry *tblb, const BinFileLineAreaEntry *tble);
};

template<> struct OSMStaticDB::obj_types<OSMStaticDB::Object::type_t::point> {
	typedef OSMStaticDB::ObjPoint ObjType;
	typedef OSMStaticDB::BinFilePointEntry IndexType;
	typedef std::pair<const IndexType *, const IndexType *> IndexRangeType;
	typedef OSMStaticDB::BinFilePointEntry BinFileEntry;
};

template<> struct OSMStaticDB::obj_types<OSMStaticDB::Object::type_t::line> {
	typedef OSMStaticDB::ObjLine ObjType;
	typedef OSMStaticDB::BinFileLineAreaEntry IndexType;
	typedef std::pair<const IndexType *, const IndexType *> IndexRangeType;
	typedef OSMStaticDB::BinFileLineAreaEntry BinFileEntry;
};

template<> struct OSMStaticDB::obj_types<OSMStaticDB::Object::type_t::area> {
	typedef OSMStaticDB::ObjArea ObjType;
	typedef OSMStaticDB::BinFileLineAreaEntry IndexType;
	typedef std::pair<const IndexType *, const IndexType *> IndexRangeType;
	typedef OSMStaticDB::BinFileLineAreaEntry BinFileEntry;
};

template<> struct OSMStaticDB::layer_data_type<OSMStaticDB::layer_t::boundarylinesland> {
	static const Object::type_t objtype = Object::type_t::line;
	static const std::string name;
};

template<> struct OSMStaticDB::layer_data_type<OSMStaticDB::layer_t::icesheetpolygons> {
	static const Object::type_t objtype = Object::type_t::area;
	static const std::string name;
};

template<> struct OSMStaticDB::layer_data_type<OSMStaticDB::layer_t::icesheetoutlines> {
	static const Object::type_t objtype = Object::type_t::line;
	static const std::string name;
};

template<> struct OSMStaticDB::layer_data_type<OSMStaticDB::layer_t::waterpolygons> {
	static const Object::type_t objtype = Object::type_t::area;
	static const std::string name;
};

template<> struct OSMStaticDB::layer_data_type<OSMStaticDB::layer_t::simplifiedwaterpolygons> {
	static const Object::type_t objtype = Object::type_t::area;
	static const std::string name;
};

const std::string& to_str(OSMStaticDB::Object::type_t t);
inline std::ostream& operator<<(std::ostream& os, OSMStaticDB::Object::type_t t) { return os << to_str(t); }

const std::string& to_str(OSMStaticDB::layer_t l);
inline std::ostream& operator<<(std::ostream& os, OSMStaticDB::layer_t l) { return os << to_str(l); }

inline OSMStaticDB::importflags_t operator|(OSMStaticDB::importflags_t x, OSMStaticDB::importflags_t y) { return (OSMStaticDB::importflags_t)((unsigned int)x | (unsigned int)y); }
inline OSMStaticDB::importflags_t operator&(OSMStaticDB::importflags_t x, OSMStaticDB::importflags_t y) { return (OSMStaticDB::importflags_t)((unsigned int)x & (unsigned int)y); }
inline OSMStaticDB::importflags_t operator^(OSMStaticDB::importflags_t x, OSMStaticDB::importflags_t y) { return (OSMStaticDB::importflags_t)((unsigned int)x ^ (unsigned int)y); }
inline OSMStaticDB::importflags_t operator~(OSMStaticDB::importflags_t x){ return (OSMStaticDB::importflags_t)~(unsigned int)x; }
inline OSMStaticDB::importflags_t& operator|=(OSMStaticDB::importflags_t& x, OSMStaticDB::importflags_t y) { x = x | y; return x; }
inline OSMStaticDB::importflags_t& operator&=(OSMStaticDB::importflags_t& x, OSMStaticDB::importflags_t y) { x = x & y; return x; }
inline OSMStaticDB::importflags_t& operator^=(OSMStaticDB::importflags_t& x, OSMStaticDB::importflags_t y) { x = x ^ y; return x; }

inline OSMStaticDB::Object::printflags_t operator|(OSMStaticDB::Object::printflags_t x, OSMStaticDB::Object::printflags_t y) { return (OSMStaticDB::Object::printflags_t)((unsigned int)x | (unsigned int)y); }
inline OSMStaticDB::Object::printflags_t operator&(OSMStaticDB::Object::printflags_t x, OSMStaticDB::Object::printflags_t y) { return (OSMStaticDB::Object::printflags_t)((unsigned int)x & (unsigned int)y); }
inline OSMStaticDB::Object::printflags_t operator^(OSMStaticDB::Object::printflags_t x, OSMStaticDB::Object::printflags_t y) { return (OSMStaticDB::Object::printflags_t)((unsigned int)x ^ (unsigned int)y); }
inline OSMStaticDB::Object::printflags_t operator~(OSMStaticDB::Object::printflags_t x){ return (OSMStaticDB::Object::printflags_t)~(unsigned int)x; }
inline OSMStaticDB::Object::printflags_t& operator|=(OSMStaticDB::Object::printflags_t& x, OSMStaticDB::Object::printflags_t y) { x = x | y; return x; }
inline OSMStaticDB::Object::printflags_t& operator&=(OSMStaticDB::Object::printflags_t& x, OSMStaticDB::Object::printflags_t y) { x = x & y; return x; }
inline OSMStaticDB::Object::printflags_t& operator^=(OSMStaticDB::Object::printflags_t& x, OSMStaticDB::Object::printflags_t y) { x = x ^ y; return x; }

#endif /* OSMSDB_H */

/* Local Variables: */
/* mode: c++ */
/* End: */
