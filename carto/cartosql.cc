//
// C++ Implementation: cartosql
//
// Description: CartoSQL
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <numeric>

#include "geom.h"
#include "cartosql.h"

const char *CartoSQL::PrintContextSQL::get_osmkey(OSMDB::Object::tagkey_t key) const
{
	if (!m_csql || !m_csql->get_osmdb())
		return nullptr;
	return m_csql->get_osmdb()->find_tagkey(key);
}

const char *CartoSQL::PrintContextSQL::get_osmvalue(OSMDB::Object::tagname_t name) const
{
	if (!m_csql || !m_csql->get_osmdb())
		return nullptr;
	return m_csql->get_osmdb()->find_tagname(name);
}

std::string CartoSQL::PrintContextSQL::get_tablecolumn(unsigned int tblnr, unsigned int colnr) const
{
	std::ostringstream oss;
	oss << "?table" << tblnr << "?.?column" << colnr << '?';
	return oss.str();
}

std::string CartoSQL::PrintContextSubtables::get_tablecolumn(unsigned int tblnr, unsigned int colnr) const
{
	if (!m_subtables)
		return PrintContextOverride::get_tablecolumn(tblnr, colnr);
	const ExprTable * const *subtbl(m_subtables);
	for (unsigned int i(tblnr); i > 0 && subtbl[0]; --i, ++subtbl);
	std::ostringstream oss;
	if (!subtbl[0]) {
		oss << "?column" << tblnr << '.' << colnr << '?';
	} else if (colnr >= subtbl[0]->get_columns().size()) {
		oss << "?column" << tblnr << '.' << colnr << '?';
		if (!false) {
			oss << std::endl << "  -- table " << tblnr << std::endl;
			for (const auto& col : subtbl[0]->get_columns())
				oss << "  -- col: " << col.to_str() << " type " << col.get_type()
				    << (col.is_wildcard() ? " wildcard" : "")
				    << ((col.get_expr() && col.get_expr()->is_aggregate()) ? " aggregate" : "") << std::endl;
		}
	} else {
		const ExprTable::Column& col(subtbl[0]->get_columns()[colnr]);
		if ((tblnr || subtbl[1]) && col.has_table())
			oss << col.get_table() << '.';
		oss << col.get_name();
	}
	return oss.str();
}

std::string CartoSQL::PrintContextStackSubtables::get_tablecolumn(unsigned int tblnr, unsigned int colnr) const
{
	if (!m_subtables)
		return PrintContextOverride::get_tablecolumn(tblnr, colnr);
	const ExprTable * const *subtbl(m_subtables);
	for (unsigned int i(tblnr); i > 0 && subtbl[0]; --i, ++subtbl);
	if (!subtbl[0])
		return PrintContextOverride::get_tablecolumn(tblnr - (subtbl - m_subtables), colnr);
	std::ostringstream oss;
	if (colnr >= subtbl[0]->get_columns().size()) {
		oss << "?column" << tblnr << '.' << colnr << '?';
		if (!false) {
			oss << std::endl << "  -- table " << tblnr << std::endl;
			for (const auto& col : subtbl[0]->get_columns())
				oss << "  -- col: " << col.to_str() << " type " << col.get_type()
				    << (col.is_wildcard() ? " wildcard" : "")
				    << ((col.get_expr() && col.get_expr()->is_aggregate()) ? " aggregate" : "") << std::endl;
		}
	} else {
		const ExprTable::Column& col(subtbl[0]->get_columns()[colnr]);
		if ((tblnr || subtbl[1]) && col.has_table())
			oss << col.get_table() << '.';
		oss << col.get_name();
	}
	return oss.str();
}

std::string CartoSQL::PrintContextSubtableVector::get_tablecolumn(unsigned int tblnr, unsigned int colnr) const
{
	std::ostringstream oss;
	if (tblnr >= m_subtables.size() || !m_subtables[tblnr]) {
		oss << "?column" << tblnr << '.' << colnr << '?';
	} else if (colnr >= m_subtables[tblnr]->get_columns().size()) {
		oss << "?column" << tblnr << '.' << colnr << '?';
		if (!false) {
			oss << std::endl << "  -- table " << tblnr << std::endl;
			for (const auto& col : m_subtables[tblnr]->get_columns())
				oss << "  -- col: " << col.to_str() << " type " << col.get_type()
				    << (col.is_wildcard() ? " wildcard" : "")
				    << ((col.get_expr() && col.get_expr()->is_aggregate()) ? " aggregate" : "") << std::endl;
		}
	} else {
		const ExprTable::Column& col(m_subtables[tblnr]->get_columns()[colnr]);
		if ((tblnr || !m_subtables.empty()) && col.has_table())
			oss << col.get_table() << '.';
		oss << col.get_name();
	}
	return oss.str();
}

std::string CartoSQL::PrintContextHideFirstTable::get_tablecolumn(unsigned int tblnr, unsigned int colnr) const
{
	if (tblnr)
		return PrintContextOverride::get_tablecolumn(tblnr, colnr);
	std::ostringstream oss;
	oss << "?column" << tblnr << '.' << colnr << '?';
	return oss.str();
}

std::ostream& CartoSQL::print_expr(std::ostream& os, const Expr::const_ptr_t& expr, unsigned int indent) const
{
	if (!expr)
		return os << "?unsupported?";
	PrintContextSQL ctx(this);
	return expr->print(os, indent, ctx);
}

std::ostream& CartoSQL::print_expr(std::ostream& os, const ExprTable::const_ptr_t& expr, unsigned int indent) const
{
	if (!expr)
		return os << "?unsupported?";
	PrintContextSQL ctx(this);
	return expr->print(os, indent, ctx);
}

std::ostream& CartoSQL::print_table(std::ostream& os, const Table& tbl, const ExprTable::const_ptr_t& expr, unsigned int indent,
				    const std::string& delim, Value::sqlstringflags_t flags)
{
	typedef std::vector<std::string::size_type> colwidth_t;
	colwidth_t colwidth;
	if (expr) {
		TableRow::size_type n(expr->get_columns().size());
		if (colwidth.size() < n)
			colwidth.resize(n, static_cast<std::string::size_type>(0));
		for (TableRow::size_type i(0); i < n; ++i) {
			const ExprTable::Column& col(expr->get_columns()[i]);
			colwidth[i] = std::max(colwidth[i], col.get_name().size());
		}
	}
	std::string::size_type osmidwidth(0);
	for (const TableRow& row : tbl) {
		if (colwidth.size() < row.size())
			colwidth.resize(row.size(), static_cast<std::string::size_type>(0));
		osmidwidth = std::max(osmidwidth, row.get_osmidset_string().size());
		for (TableRow::size_type i(0), n(row.size()); i < n; ++i)
			colwidth[i] = std::max(colwidth[i], row[i].get_sqlstring(flags).size());
	}
	if (colwidth.empty())
		return os;
	std::ios_base::fmtflags flg(os.setf(std::ios_base::left, std::ios_base::adjustfield));
	if (expr) {
		os << std::string(indent, ' ');
		if (osmidwidth)
			os << std::right << std::setw(osmidwidth) << ((osmidwidth >= 7) ? "osm_ids" : (osmidwidth >= 2) ? "id" : "") << std::left << delim;
		for (TableRow::size_type i(0), n(expr->get_columns().size()); i < n; ++i) {
			const ExprTable::Column& col(expr->get_columns()[i]);
			if (i)
				os << delim;
			os << std::setw(colwidth[i]) << col.get_name();
		}
		colwidth_t::value_type totcol(std::accumulate(colwidth.begin(), colwidth.end(), static_cast<colwidth_t::value_type>(0)));
		totcol += (colwidth.size() - 1) * delim.size();
		if (osmidwidth)
			totcol += osmidwidth + delim.size();
		os << std::endl << std::string(indent, ' ') << std::string(totcol, '-') << std::endl;
	}
	for (const TableRow& row : tbl) {
		os << std::string(indent, ' ');
		if (osmidwidth)
			os << std::right << std::setw(osmidwidth) << row.get_osmidset_string() << std::left << delim;
		for (TableRow::size_type i(0), n(row.size()); i < n; ++i) {
			if (i)
				os << delim;
			os << std::setw(colwidth[i]) << row[i].get_sqlstring(flags);
		}
		os << std::endl;
	}
	os.flags(flg);
	return os;
}

std::ostream& CartoSQL::print_table(std::ostream& os, const IndexTable& tbl, unsigned int indent, const std::string& delim, Value::sqlstringflags_t flags)
{
	typedef std::vector<std::string::size_type> colwidth_t;
	colwidth_t colwidth;
	std::string::size_type indexwidth(0);
	for (const IndexTableRow& row : tbl) {
		if (colwidth.size() < row.size())
			colwidth.resize(row.size(), static_cast<std::string::size_type>(0));
		{
			std::ostringstream oss;
			oss << row.get_index();
			indexwidth = std::max(indexwidth, oss.str().size());
		}
		for (IndexTableRow::size_type i(0), n(row.size()); i < n; ++i)
			colwidth[i] = std::max(colwidth[i], row[i].get_sqlstring(flags).size());
	}
	if (colwidth.empty())
		return os;
	std::ios_base::fmtflags flg(os.setf(std::ios_base::left, std::ios_base::adjustfield));
	for (const IndexTableRow& row : tbl) {
		os << std::string(indent, ' ');
		if (indexwidth) {
			std::ostringstream oss;
			oss << row.get_index();
			os << std::right << std::setw(indexwidth) << oss.str() << std::left << delim;
		}
		for (IndexTableRow::size_type i(0), n(row.size()); i < n; ++i) {
			if (i)
				os << delim;
			os << std::setw(colwidth[i]) << row[i].get_sqlstring(flags);
		}
		os << std::endl;
	}
	os.flags(flg);
	return os;
}
