//
// C++ Interface: cartocss
//
// Description: CartoCSS style file parser
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef CARTOCSS_HH
#define CARTOCSS_HH

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <boost/smart_ptr/intrusive_ptr.hpp>
#include <atomic>
#include <limits>
#include <vector>
#include <set>
#include <map>

class HibernateWriteStream;
class HibernateReadStream;
class HibernateReadBuffer;

class CartoCSS {
public:
	class LayerID : public std::string {
	public:
		LayerID(const std::string& x) : std::string(x) {}
		LayerID(std::string&& x) : std::string(x) {}
		LayerID(void) {}
	};

	class ClassID : public std::string {
	public:
		ClassID(const std::string& x) : std::string(x) {}
		ClassID(std::string&& x) : std::string(x) {}
		ClassID(void) {}
	};

	enum class compositeop_t : uint8_t {
		clear,
		src,
		dst,
		srcover,
		dstover,
		srcin,
		dstin,
		srcout,
		dstout,
		srcatop,
		dstatop,
		xor_,
		plus,
		minus,
		multiply,
		screen,
		overlay,
		darken,
		lighten,
		colordodge,
		colorburn,
		hardlight,
		softlight,
		difference,
		exclusion,
		contrast,
		invert,
		invertrgb,
		grainmerge,
		grainextract,
		hue,
		saturation,
		color,
		value,
		invalid = 0xff
	};

	enum class placement_t : uint8_t {
		line,
		point,
		interior,
		invalid = 0xff
	};

	enum class variable_t : uint8_t {
		first_,
		commonfirst_ = first_,
		compop = commonfirst_,
		imagefilters,
		opacity,
		commonlast_ = opacity,
		mapfirst_,
		mapbackgroundcolor = mapfirst_,
		maplast_ = mapbackgroundcolor,
		linefirst_,
		linecap = linefirst_,
		lineclip,
		linecolor,
		linedasharray,
		linejoin,
		lineoffset,
		lineopacity,
		linesimplify,
		linesimplifyalgorithm,
		linewidth,
		linelast_ = linewidth,
		linepatternfirst_,
		linepatternfile = linepatternfirst_,
		linepatternlast_ = linepatternfile,
		markerfirst_,
		markerallowoverlap = markerfirst_,
		markerclip,
		markerfile,
		markerfill,
		markerheight,
		markerignoreplacement,
		markerlinecolor,
		markerlinewidth,
		markermaxerror,
		markeropacity,
		markerplacement,
		markerspacing,
		markerwidth,
		markerlast_ = markerwidth,
		polygonfirst_,
		polygonclip = polygonfirst_,
		polygonfill,
		polygongamma,
		polygonlast_ = polygongamma,
		polygonpatternfirst_,
		polygonpatternalignment = polygonpatternfirst_,
		polygonpatternfile,
		polygonpatterngamma,
		polygonpatternopacity,
		polygonpatternlast_ = polygonpatternopacity,
		shieldfirst_,
		shieldcharacterspacing = shieldfirst_,
		shieldclip,
		shieldfacename,
		shieldfile,
		shieldfill,
		shieldhalofill,
		shieldhaloradius,
		shieldhorizontalalignment,
		shieldlinespacing,
		shieldmargin,
		shieldname,
		shieldopacity,
		shieldplacement,
		shieldplacementtype,
		shieldplacements,
		shieldrepeatdistance,
		shieldsize,
		shieldspacing,
		shieldtextdx,
		shieldtextdy,
		shieldunlockimage,
		shieldverticalalignment,
		shieldwrapcharacter,
		shieldwrapwidth,
		shieldlast_ = shieldwrapwidth,
		textfirst_,
		textcharacterspacing = textfirst_,
		textclip,
		textdx,
		textdy,
		textfacename,
		textfill,
		texthalofill,
		texthaloradius,
		texthorizontalalignment,
		textlargestbboxonly,
		textlinespacing,
		textmargin,
		textmaxcharangledelta,
		textmindistance,
		textname,
		textopacity,
		textplacement,
		textplacementtype,
		textplacements,
		textrepeatdistance,
		textsize,
		textspacing,
		textupright,
		textverticalalignment,
		textwrapcharacter,
		textwrapwidth,
		textlast_ = textwrapwidth,
		last_ = textlast_,
		invalid = static_cast<std::underlying_type<variable_t>::type>(~0)
	};

	typedef unsigned int zoom_t;

	class Color {
	public:
		Color(void);
		Color(double r, double g, double b, double a = 1);
		double get_red(void) const { return m_r; }
		double get_green(void) const { return m_g; }
		double get_blue(void) const { return m_b; }
		double get_alpha(void) const { return m_a; }
		void set_red(double r) { m_r = r; }
		void set_green(double g) { m_g = g; }
		void set_blue(double b) { m_b = b; }
		void set_alpha(double a) { m_a = a; }
		void set_rgba_uint8(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 255);
		void set_rgba_uint4(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 15);
		void set_hsl(double h, double s, double l);
		void get_hsl(double& h, double& s, double& l);
		bool is_valid(void) const;
		static double clamp(double x);
		Color clamp(void) const;
		std::string to_str(void) const;
		std::string to_mss_str(void) const;

		template<class Archive> void hibernate(Archive& ar) {
			ar.io(m_r);
			ar.io(m_g);
			ar.io(m_b);
			ar.io(m_a);
		}

	protected:
		double m_r;
		double m_g;
		double m_b;
		double m_a;

		static double hslvalue(double n1, double n2, double hue);
	};

	class LayerAllocator {
	public:
		enum class layer_t : uint8_t {
			first    = 0x00,
			invalid  = 0xff
		};

		LayerAllocator(void);
		layer_t allocate(const std::string& name);
		layer_t get_nextlayer(void) const { return m_nextlayer; }
		layer_t get_default(void) const;
		const std::string& get_name(layer_t l) const;
		static inline layer_t inc(layer_t l) {
			typedef std::underlying_type<layer_t>::type ul_t;
			return static_cast<layer_t>(static_cast<ul_t>(l) + 1);
		}

	protected:
		typedef std::map<std::string,layer_t> layermap_t;
		layermap_t m_layermap;
		layer_t m_nextlayer;
	};

	class PrefixAllocator {
	public:
		enum class prefix_t : uint8_t {
			first    = 0x00,
			invalid  = 0xff
		};

		PrefixAllocator(void);
		prefix_t allocate(const std::string& name);
		prefix_t get_nextprefix(void) const { return m_nextprefix; }
		prefix_t get_default(void) const;
		const std::string& get_name(prefix_t l) const;
		static inline prefix_t inc(prefix_t l) {
			typedef std::underlying_type<prefix_t>::type ul_t;
			return static_cast<prefix_t>(static_cast<ul_t>(l) + 1);
		}

	protected:
		typedef std::map<std::string,prefix_t> prefixmap_t;
		prefixmap_t m_prefixmap;
		prefix_t m_nextprefix;
	};

	typedef std::map<CartoCSS::LayerAllocator::layer_t,PrefixAllocator> prefixallocators_t;

	class Expr;

	class Value {
	public:
		class ValueError : public std::runtime_error {
		public:
			explicit ValueError(const std::string& x);
		};

		Value(void);
		Value(const std::string& val);
		Value(const char *val);
		Value(double val);
		Value(int64_t val);
		Value(bool val);
		Value(const Value& v);
		Value(Value&& v);
		~Value();

		Value& operator=(const std::string& v);
		Value& operator=(const char *v);
		Value& operator=(double v);
		Value& operator=(int64_t v);
		Value& operator=(bool v);
		Value& operator=(const Value& v);
		Value& operator=(Value&& v);

		bool is_null(void) const { return m_type == type_t::null; }
		bool is_boolean(void) const { return m_type == type_t::boolean; }
		bool is_integer(void) const { return m_type == type_t::integer; }
		bool is_double(void) const { return m_type == type_t::floatingpoint; }
		bool is_string(void) const { return m_type == type_t::string; }
		bool is_integral(void) const { return is_boolean() || is_integer(); }
		bool is_numeric(void) const { return is_boolean() || is_integer() || is_double(); }

		operator bool(void) const;
		operator int64_t(void) const;
		operator double(void) const;
		operator std::string(void) const;

		boost::intrusive_ptr<Expr> get_expr(void) const;

		int compare(const Value& x) const;
		bool operator==(const Value& x) const { return !compare(x); }
		bool operator!=(const Value& x) const { return !operator==(x); }
		bool operator<(const Value& x) const { return compare(x) < 0; }
		bool operator<=(const Value& x) const { return compare(x) <= 0; }
		bool operator>(const Value& x) const { return compare(x) > 0; }
		bool operator>=(const Value& x) const { return compare(x) >= 0; }

		template<class Archive> void hibernate(Archive& ar) {
			if (ar.is_load())
				dealloc();
			ar.ioenum(m_type);
			switch (m_type) {
			case type_t::string:
				if (ar.is_load()) {
					std::string x;
					ar.io(x);
					::new(&m_string) string_t(x);
					break;
				}
				ar.io(m_string);
				break;

			case type_t::floatingpoint:
				ar.io(m_double);
				break;

			case type_t::integer:
				ar.ioleb(m_int);
				break;

			case type_t::boolean:
				ar.iouint8(m_bool);
				break;

			default:
				break;
			}
		}

	protected:
		typedef std::string string_t;
		union {
			string_t m_string;
			double m_double;
			int64_t m_int;
			bool m_bool;
		};
		enum class type_t : uint8_t {
			null = 0,
			string,
			floatingpoint,
			integer,
			boolean
		};
		type_t m_type;

		void dealloc(void);
	};

	class Fields : public std::map<std::string,Value> {
	public:
		template<class Archive> void hibernate(Archive& ar) {
			size_type n(size());
			ar.ioleb(n);
			if (ar.is_load()) {
				clear();
				for (; n; --n) {
				        std::string x;
					ar.io(x);
					std::pair<iterator,bool> ins(insert(value_type(x, Value())));
					ins.first->second.hibernate(ar);
				}
			} else {
				for (iterator i(begin()), e(end()); i != e; ++i) {
					ar.io(const_cast<std::string&>(i->first));
					i->second.hibernate(ar);
				}
			}
		}
	};

	class Visitor;

	class Expr {
	public:
		class ExprError : public std::runtime_error {
		public:
			explicit ExprError(const std::string& x);
		};

		typedef boost::intrusive_ptr<Expr> ptr_t;
		typedef boost::intrusive_ptr<const Expr> const_ptr_t;

		enum class type_t : uint8_t {
			null,
			keyword,
			compositeop,
			layerid,
			classid,
			boolean,
			integer,
			floatingpoint,
			string,
			color,
			url,
			directory,
			varref,
			field,
			copylayer,
			list,
			function,
			compare,
			invalid = static_cast<std::underlying_type<type_t>::type>(~0)
		};

		enum class keyword_t : uint8_t {
		        first,
			auto_ = first,
			bevel,
			bottom,
			butt,
			global,
			interior,
			left,
			left_only,
			line,
			middle,
			miter,
			point,
			right,
			right_only,
			round,
			simple,
			square,
			top,
			visvalingamwhyatt,
			last = visvalingamwhyatt,
			invalid = static_cast<std::underlying_type<keyword_t>::type>(~0)
		};

		enum class func_t : uint8_t {
			first,
			// 1 function argument
			// 2 function arguments
			collighten = first,
			coldarken,
			colsaturate,
			coldesaturate,
			colfadein,
			colfadeout,
			colspin,
			plus,
			minus,
			mul,
			div,
			url,
			// 2 or 3 arguments
			colmix,
			// 3 function arguments
			colrgb,
			colhsl,
			// 4 function arguments
			colrgba,
			colhsla,
			// 5 function arguments
			// 6 function arguments
			// 7 function arguments
			// 8 function arguments
			scalehsla,
			last = scalehsla,
			invalid = static_cast<std::underlying_type<func_t>::type>(~0)
		};

		enum class compop_t : uint8_t {
			greater,
			less,
			greaterthan,
			lessthan,
			equal,
			unequal,
			invalid = static_cast<std::underlying_type<compop_t>::type>(~0)
		};

		typedef std::vector<std::string> stringlist_t;
		typedef std::vector<double> doublelist_t;
		typedef std::vector<const_ptr_t> exprlist_t;
		typedef std::map<std::string, const_ptr_t> variables_t;

		virtual ~Expr();

		unsigned int breference(void) const { return ++m_refcount; }
		unsigned int bunreference(void) const { return --m_refcount; }
		unsigned int get_refcount(void) const { return m_refcount; }
		ptr_t get_ptr(void) { return ptr_t(this); }
		const_ptr_t get_ptr(void) const { return const_ptr_t(this); }
		friend inline void intrusive_ptr_add_ref(const Expr *expr) { expr->breference(); }
		friend inline void intrusive_ptr_release(const Expr *expr) { if (!expr->bunreference()) delete expr; }

		virtual type_t get_type(void) const = 0;

		virtual const_ptr_t simplify(void) const { return ptr_t(); }
		virtual const_ptr_t simplify(const std::string& field, const Value& value) const { return ptr_t(); }
		virtual const_ptr_t simplify(const Fields& fields) const { return ptr_t(); }
		virtual const_ptr_t simplify(const std::string& var, const const_ptr_t& expr) const { return ptr_t(); }
		virtual const_ptr_t simplify(const variables_t& vars) const { return ptr_t(); }
		virtual const_ptr_t simplify(const LayerID& layer) const { return ptr_t(); }
		virtual const_ptr_t simplify(const ClassID& cls) const { return ptr_t(); }
		virtual const_ptr_t simplify(Visitor& v) const { return v.modify(*this); }
		virtual void visit(Visitor& v) const { v.visit(*this); v.visitend(*this); }

		virtual bool is_const(void) const { return false; }
		virtual bool is_type(void) const { return false; }
		virtual bool is_type(bool& v) const { return false; }
		virtual bool is_type(keyword_t& v) const { return false; }
		virtual bool is_type(compositeop_t& v) const { return false; }
		virtual bool is_type(LayerID& v) const { return false; }
		virtual bool is_type(ClassID& v) const { return false; }
		virtual bool is_type(int64_t& v) const { return false; }
		virtual bool is_type(double& v) const { return false; }
		virtual bool is_type(std::string& v) const { return false; }
		virtual bool is_type(stringlist_t& v) const { return false; }
		virtual bool is_type(exprlist_t& v) const { return false; }
		virtual bool is_type(Color& v) const { return false; }
		virtual bool is_type(func_t& func, Expr::const_ptr_t& args) const { return false; }
		virtual bool is_type(Expr::const_ptr_t& arg0, compop_t& op, Expr::const_ptr_t& arg1) const { return false; }
		virtual bool is_varref(std::string& v) const { return false; }
		virtual bool is_field(std::string& v) const { return false; }
		virtual bool is_copylayer(std::string& v) const { return false; }
		virtual bool is_url(std::string& v) const { return false; }
		virtual bool is_directory(std::string& v) const { return false; }

		bool is_convertible(bool& v) const;
		bool is_convertible(int64_t& v) const;
		bool is_convertible(double& v) const;
		bool is_convertible(std::string& v) const;
		bool is_convertible(stringlist_t& v) const;
		bool is_convertible(doublelist_t& v) const;
		bool is_convertible(exprlist_t& v) const;
		bool is_convertible(Value& v) const;

		bool as_bool(void) const;
		keyword_t as_keyword(void) const;
		compositeop_t as_compositeop(void) const;
		int64_t as_int(void) const;
		double as_double(void) const;
		std::string as_string(void) const;
		doublelist_t as_doublelist(void) const;
		stringlist_t as_stringlist(void) const;
		exprlist_t as_exprlist(void) const;
		Color as_color(void) const;
		std::string as_url(void) const;
		Value as_value(void) const;

		static const_ptr_t create(void);
		static const_ptr_t create(bool v);
		static const_ptr_t create(keyword_t v);
		static const_ptr_t create(compositeop_t v);
		static const_ptr_t create(const LayerID& v);
		static const_ptr_t create(const ClassID& v);
		static const_ptr_t create(int64_t v);
		static const_ptr_t create(double v);
		static const_ptr_t create(const std::string& v);
		static const_ptr_t create(const Color& v);
		static const_ptr_t create(const Value& v);
		static const_ptr_t create(const exprlist_t& v);
		static const_ptr_t create(func_t func, const const_ptr_t& args);
		static const_ptr_t create(const const_ptr_t& v0, compop_t op, const const_ptr_t& v1);
		static const_ptr_t create_url(const std::string& url);
		static const_ptr_t create_directory(const std::string& dir);
		static const_ptr_t create_varref(const std::string& var);
		static const_ptr_t create_field(const std::string& field);
		static const_ptr_t create_copylayer(const std::string& field);
		static const_ptr_t create_number(const std::string& v);

		static const_ptr_t create(HibernateReadStream& ar);
		static const_ptr_t create(HibernateReadBuffer& ar);

		virtual void save(HibernateWriteStream& ar) const = 0;
		virtual void load(HibernateReadStream& ar) = 0;
		virtual void load(HibernateReadBuffer& ar) = 0;

		virtual std::string to_str(void) const = 0;

		static compop_t reverse(compop_t op);

	protected:
		mutable std::atomic<unsigned int> m_refcount;

		Expr(void);
		static ptr_t create(type_t t);
		template<class Archive> void hibernate(Archive& ar) {
			if (ar.is_save()) {
				type_t typ(get_type());
				ar.ioenum(typ);
			}
		}
	};

	class MarkerParams {
	public:
		MarkerParams(void);

		bool set_variable(variable_t var, const Expr::const_ptr_t& expr);
		std::ostream& print(std::ostream& os, unsigned int indent = 0) const;
		std::ostream& print_mss(std::ostream& os, const std::string& pfx = "", unsigned int indent = 0) const;

		const Color& get_fill(void) const { return m_fill; }
		const Color& get_linecolor(void) const { return m_linecolor; }
		const std::string& get_file(void) const { return m_file; }
		double get_height(void) const { return m_height; }
		double get_linewidth(void) const { return m_linewidth; }
		double get_maxerror(void) const { return m_maxerror; }
		double get_opacity(void) const { return m_opacity; }
		double get_spacing(void) const { return m_spacing; }
		double get_width(void) const { return m_width; }
		placement_t get_placement(void) const { return m_placement; }
		bool get_allowoverlap(void) const { return m_allowoverlap; }
		bool get_clip(void) const { return m_clip; }
		bool get_ignoreplacement(void) const { return m_ignoreplacement; }

		void set_fill(const Color& x) { m_fill = x; }
		void set_linecolor(const Color& x) { m_linecolor = x; }
		void set_file(const std::string& x) { m_file = x; }
		void set_height(double x) { m_height = x; }
		void set_linewidth(double x) { m_linewidth = x; }
		void set_maxerror(double x) { m_maxerror = x; }
		void set_opacity(double x) { m_opacity = x; }
		void set_spacing(double x) { m_spacing = x; }
		void set_width(double x) { m_width = x; }
		void set_placement(placement_t x) { m_placement = x; }
		void set_allowoverlap(bool x) { m_allowoverlap = x; }
		void set_clip(bool x) { m_clip = x; }
		void set_ignoreplacement(bool x) { m_ignoreplacement = x; }

		template<class Archive> void hibernate(Archive& ar) {
			m_fill.hibernate(ar);
			m_linecolor.hibernate(ar);
			ar.io(m_file);
			ar.io(m_height);
			ar.io(m_linewidth);
			ar.io(m_maxerror);
			ar.io(m_opacity);
			ar.io(m_spacing);
			ar.io(m_width);
			ar.ioenum(m_placement);
			ar.ioenum(m_allowoverlap);
			ar.ioenum(m_clip);
			ar.ioenum(m_ignoreplacement);
		}

	protected:
		Color m_fill;
		Color m_linecolor;
		std::string m_file;
		double m_height;
		double m_linewidth;
		double m_maxerror;
		double m_opacity;
		double m_spacing;
		double m_width;
		placement_t m_placement;
		bool m_allowoverlap;
		bool m_clip;
		bool m_ignoreplacement;
	};

	class LineParams {
	public:
		enum class cap_t : uint8_t {
			butt,
			round,
			square,
			invalid = 0xff
		};

		enum class join_t : uint8_t {
			bevel,
			miter,
			round,
			invalid = 0xff
		};

		enum class simplifyalgo_t : uint8_t {
			visvalingamwhyatt,
			invalid = 0xff
		};

		typedef std::vector<double> dasharray_t;
		
		LineParams(void);

		bool set_variable(variable_t var, const Expr::const_ptr_t& expr);
		std::ostream& print(std::ostream& os, unsigned int indent = 0) const;
		std::ostream& print_mss(std::ostream& os, const std::string& pfx = "", unsigned int indent = 0) const;

		const Color& get_color(void) const { return m_color; }
		const dasharray_t& get_dasharray(void) const { return m_dasharray; }
		double get_offset(void) const { return m_offset; }
		double get_opacity(void) const { return m_opacity; }
		double get_simplify(void) const { return m_simplify; }
		double get_width(void) const { return m_width; }
		cap_t get_cap(void) const { return m_cap; }
		join_t get_join(void) const { return m_join; }
		simplifyalgo_t get_simplifyalgo(void) const { return m_simplifyalgo; }
		bool get_clip(void) const { return m_clip; }

		void set_color(const Color& x) { m_color = x; }
		void set_dasharray(dasharray_t x) { m_dasharray = x; }
		void set_offset(double x) { m_offset = x; }
		void set_opacity(double x) { m_opacity = x; }
		void set_simplify(double x) { m_simplify = x; }
		void set_width(double x) { m_width = x; }
		void set_cap(cap_t x) { m_cap = x; }
		void set_join(join_t x) { m_join = x; }
		void set_simplifyalgo(simplifyalgo_t x) { m_simplifyalgo = x; }
		void set_clip(bool x) { m_clip = x; }

		template<class Archive> void hibernate(Archive& ar) {
			m_color.hibernate(ar);
			{
				dasharray_t::size_type sz(m_dasharray.size());
				ar.ioleb(sz);
				if (ar.is_load())
					m_dasharray.resize(sz);
				for (typename dasharray_t::iterator i(m_dasharray.begin()), e(m_dasharray.end()); i != e; ++i)
					ar.io(*i);
			}
			ar.io(m_offset);
			ar.io(m_opacity);
			ar.io(m_simplify);
			ar.io(m_width);
			ar.ioenum(m_cap);
			ar.ioenum(m_join);
			ar.ioenum(m_simplifyalgo);
			ar.ioenum(m_clip);
		}

	protected:
		Color m_color;
		dasharray_t m_dasharray;
		double m_offset;
		double m_opacity;
		double m_simplify;
		double m_width;
		cap_t m_cap;
		join_t m_join;
		simplifyalgo_t m_simplifyalgo;
		bool m_clip;
	};

	class LinePatternParams {
	public:
		LinePatternParams(void);

		bool set_variable(variable_t var, const Expr::const_ptr_t& expr);
		std::ostream& print(std::ostream& os, unsigned int indent = 0) const;
		std::ostream& print_mss(std::ostream& os, const std::string& pfx = "", unsigned int indent = 0) const;

		const std::string& get_file(void) const { return m_file; }
		void set_file(const std::string& x) { m_file = x; }

		template<class Archive> void hibernate(Archive& ar) {
			ar.io(m_file);
		}

	protected:
		std::string m_file;
	};

	class PolygonParams {
	public:
		PolygonParams(void);

		bool set_variable(variable_t var, const Expr::const_ptr_t& expr);
		std::ostream& print(std::ostream& os, unsigned int indent = 0) const;
		std::ostream& print_mss(std::ostream& os, const std::string& pfx = "", unsigned int indent = 0) const;

		const Color& get_fill(void) const { return m_fill; }
		double get_gamma(void) const { return m_gamma; }
		bool get_clip(void) const { return m_clip; }

		void set_fill(const Color& x) { m_fill = x; }
		void set_gamma(double x) { m_gamma = x; }
		void set_clip(bool x) { m_clip = x; }

		template<class Archive> void hibernate(Archive& ar) {
			m_fill.hibernate(ar);
			ar.io(m_gamma);
			ar.ioenum(m_clip);
		}

	protected:
		Color m_fill;
		double m_gamma;
		bool m_clip;
	};

	class PolygonPatternParams {
	public:
		enum class alignment_t : uint8_t {
			global,
			invalid = 0xff
		};

		PolygonPatternParams(void);

		bool set_variable(variable_t var, const Expr::const_ptr_t& expr);
		std::ostream& print(std::ostream& os, unsigned int indent = 0) const;
		std::ostream& print_mss(std::ostream& os, const std::string& pfx = "", unsigned int indent = 0) const;

		const std::string& get_file(void) const { return m_file; }
		double get_gamma(void) const { return m_gamma; }
		double get_opacity(void) const { return m_opacity; }
		alignment_t get_alignment(void) const { return m_alignment; }

		void set_file(const std::string& x) { m_file = x; }
		void set_gamma(double x) { m_gamma = x; }
		void set_opacity(double x) { m_opacity = x; }
		void set_alignment(alignment_t x) { m_alignment = x; }

		template<class Archive> void hibernate(Archive& ar) {
			ar.io(m_file);
			ar.io(m_gamma);
			ar.io(m_opacity);
			ar.ioenum(m_alignment);
		}

	protected:
		std::string m_file;
		double m_gamma;
		double m_opacity;
		alignment_t m_alignment;
	};

	class BasicTextParams {
	public:
		enum class horizontalalignment_t : uint8_t {
			auto_,
			left,
			middle,
			right,
			invalid = 0xff
		};

		enum class verticalalignment_t : uint8_t {
			auto_,
			top,
			middle,
			bottom,
			invalid = 0xff
		};

		enum class placementtype_t : uint8_t {
			dummy,
			simple,
			invalid = 0xff
		};

		class PlacementOrigin {
		public:
			PlacementOrigin(horizontalalignment_t ha = horizontalalignment_t::invalid,
					verticalalignment_t va = verticalalignment_t::invalid, unsigned int fontsz = 0) : m_fontsize(fontsz), m_ha(ha), m_va(va) {}
			horizontalalignment_t get_horizontalalignment(void) const { return m_ha; }
			verticalalignment_t get_verticalalignment(void) const { return m_va; }
			unsigned int get_fontsize(void) const { return m_fontsize; }
			void set_horizontalalignment(horizontalalignment_t ha) { m_ha = ha; }
			void set_verticalalignment(verticalalignment_t va) { m_va = va; }
			void set_fontsize(unsigned int fontsz) { m_fontsize = fontsz; }

		protected:
			unsigned int m_fontsize;
			horizontalalignment_t m_ha;
			verticalalignment_t m_va;
		};

		typedef std::vector<PlacementOrigin> placementorigins_t;
		typedef std::vector<std::string> facenames_t;

		BasicTextParams(void);

		bool set_variable(variable_t var, const Expr::const_ptr_t& expr);
		std::ostream& print(std::ostream& os, unsigned int indent = 0, const char *tpfx = "text-") const;
		std::ostream& print_mss(std::ostream& os, const std::string& pfx = "", unsigned int indent = 0, const char *tpfx = "text-") const;

		const Color& get_fill(void) const { return m_fill; }
		const Color& get_halofill(void) const { return m_halofill; }
		facenames_t get_facename(void) const { return m_facename; }
		const std::string& get_name(void) const { return m_name; }
		const std::string& get_placements(void) const { return m_placements; }
		static placementorigins_t parse_placements(const std::string& pl);
		placementorigins_t parse_placements(void) const { return parse_placements(get_placements()); }
		double get_characterspacing(void) const { return m_characterspacing; }
		double get_haloradius(void) const { return m_haloradius; }
		double get_linespacing(void) const { return m_linespacing; }
		double get_margin(void) const { return m_margin; }
		double get_opacity(void) const { return m_opacity; }
		double get_size(void) const { return m_size; }
		double get_wrapwidth(void) const { return m_wrapwidth; }
		char get_wrapcharacter(void) const { return m_wrapcharacter; }
		horizontalalignment_t get_horizontalalignment(void) const { return m_horizontalalignment; }
		verticalalignment_t get_verticalalignment(void) const { return m_verticalalignment; }
		placementtype_t get_placementtype(void) const { return m_placementtype; }

		void set_fill(const Color& x) { m_fill = x; }
		void set_halofill(const Color& x) { m_halofill = x; }
		void set_facename(facenames_t x) { m_facename = x; }
		void set_name(const std::string& x) { m_name = x; }
		void set_placements(const std::string& x) { m_placements = x; }
		void set_characterspacing(double x) { m_characterspacing = x; }
		void set_haloradius(double x) { m_haloradius = x; }
		void set_linespacing(double x) { m_linespacing = x; }
		void set_margin(double x) { m_margin = x; }
		void set_opacity(double x) { m_opacity = x; }
		void set_size(double x) { m_size = x; }
		void set_wrapwidth(double x) { m_wrapwidth = x; }
		void set_wrapcharacter(char x) { m_wrapcharacter = x; }
		void set_horizontalalignment(horizontalalignment_t x) { m_horizontalalignment = x; }
		void set_verticalalignment(verticalalignment_t x) { m_verticalalignment = x; }
		void set_placementtype(placementtype_t x) { m_placementtype = x; }

		template<class Archive> void hibernate(Archive& ar) {
			m_fill.hibernate(ar);
			m_halofill.hibernate(ar);
			{
				facenames_t::size_type sz(m_facename.size());
				ar.ioleb(sz);
				if (ar.is_load())
					m_facename.resize(sz);
				for (typename facenames_t::iterator i(m_facename.begin()), e(m_facename.end()); i != e; ++i)
					ar.io(*i);
			}
 			ar.io(m_name);
			ar.io(m_placements);
			ar.io(m_characterspacing);
			ar.io(m_haloradius);
			ar.io(m_linespacing);
			ar.io(m_margin);
			ar.io(m_opacity);
			ar.io(m_size);
			ar.io(m_wrapwidth);
			ar.ioint8(m_wrapcharacter);
			ar.ioenum(m_horizontalalignment);
			ar.ioenum(m_verticalalignment);
			ar.ioenum(m_placementtype);
		}

	protected:
		Color m_fill;
		Color m_halofill;
		facenames_t m_facename;
   		std::string m_name;
		std::string m_placements;
		double m_characterspacing;
		double m_haloradius;
		double m_linespacing;
		double m_margin;
		double m_opacity;
		double m_size;
		double m_wrapwidth;
		char m_wrapcharacter;
		horizontalalignment_t m_horizontalalignment;
		verticalalignment_t m_verticalalignment;
		placementtype_t m_placementtype;
	};

	class ShieldParams : public BasicTextParams {
	public:
		ShieldParams(void);

		bool set_variable(variable_t var, const Expr::const_ptr_t& expr);
		std::ostream& print(std::ostream& os, unsigned int indent = 0) const;
		std::ostream& print_mss(std::ostream& os, const std::string& pfx = "", unsigned int indent = 0) const;

   		const std::string& get_file(void) const { return m_file; }
		double get_repeatdistance(void) const { return m_repeatdistance; }
        	double get_spacing(void) const { return m_spacing; }
		double get_textdx(void) const { return m_textdx; }
		double get_textdy(void) const { return m_textdy; }
		placement_t get_placement(void) const { return m_placement; }
		bool get_clip(void) const { return m_clip; }
		bool get_unlockimage(void) const { return m_unlockimage; }

		void set_file(const std::string& x) { m_file = x; }
		void set_repeatdistance(double x) { m_repeatdistance = x; }
		void set_spacing(double x) { m_spacing = x; }
		void set_textdx(double x) { m_textdx = x; }
		void set_textdy(double x) { m_textdy = x; }
		void set_placement(placement_t x) { m_placement = x; }
		void set_clip(bool x) { m_clip = x; }
		void set_unlockimage(bool x) { m_unlockimage = x; }

		template<class Archive> void hibernate(Archive& ar) {
			ar.io(m_file);
			ar.io(m_repeatdistance);
			ar.io(m_spacing);
			ar.io(m_textdx);
			ar.io(m_textdy);
			ar.ioenum(m_placement);
			ar.ioenum(m_clip);
			ar.ioenum(m_unlockimage);
		}

	protected:
		std::string m_file;
		double m_repeatdistance;
		double m_size;
		double m_spacing;
		double m_textdx;
		double m_textdy;
		double m_wrapwidth;
		placement_t m_placement;
		bool m_clip;
		bool m_unlockimage;
	};

	class TextParams : public BasicTextParams {
	public:
		enum class upright_t : uint8_t {
			auto_,
			left,
			right,
			left_only,
			right_only,
			invalid = 0xff
		};

		TextParams(void);

		bool set_variable(variable_t var, const Expr::const_ptr_t& expr);
		std::ostream& print(std::ostream& os, unsigned int indent = 0) const;
		std::ostream& print_mss(std::ostream& os, const std::string& pfx = "", unsigned int indent = 0) const;

		double get_dx(void) const { return m_dx; }
		double get_dy(void) const { return m_dy; }
		double get_maxcharangledelta(void) const { return m_maxcharangledelta; }
		double get_mindistance(void) const { return m_mindistance; }
		double get_repeatdistance(void) const { return m_repeatdistance; }
		double get_spacing(void) const { return m_spacing; }
		placement_t get_placement(void) const { return m_placement; }
		upright_t get_upright(void) const { return m_upright; }
		bool get_clip(void) const { return m_clip; }
		bool get_largestbboxonly(void) const { return m_largestbboxonly; }

		void set_dx(double x) { m_dx = x; }
		void set_dy(double y) { m_dy = y; }
		void set_maxcharangledelta(double x) { m_maxcharangledelta = x; }
		void set_mindistance(double x) { m_mindistance = x; }
		void set_repeatdistance(double x) { m_repeatdistance = x; }
		void set_spacing(double x) { m_spacing = x; }
		void set_placement(placement_t x) { m_placement = x; }
		void set_upright(upright_t x) { m_upright = x; }
		void set_clip(bool x) { m_clip = x; }
		void set_largestbboxonly(bool x) { m_largestbboxonly = x; }

		template<class Archive> void hibernate(Archive& ar) {
			BasicTextParams::hibernate(ar);
			ar.io(m_dx);
			ar.io(m_dy);
			ar.io(m_maxcharangledelta);
			ar.io(m_mindistance);
			ar.io(m_repeatdistance);
			ar.ioenum(m_placement);
			ar.ioenum(m_upright);
			ar.ioenum(m_clip);
			ar.ioenum(m_largestbboxonly);
		}

	protected:
		double m_dx;
		double m_dy;
		double m_maxcharangledelta;
		double m_mindistance;
		double m_repeatdistance;
		double m_spacing;
		placement_t m_placement;
		upright_t m_upright;
		bool m_clip;
		bool m_largestbboxonly;
	};

	class ImageFilter {
	public:
		enum class filter_t : uint8_t {
			scalehsla,
			invalid = 0xff
		};

		ImageFilter(void);
		ImageFilter(filter_t filt, const std::tuple<double,double,double,double,double,double,double,double>& par);

		filter_t get_filter(void) const { return m_filter; }
		double operator[](unsigned int idx) const { return (idx < 8) ? m_par[idx] : std::numeric_limits<double>::quiet_NaN(); }
		std::string to_str(void) const;

		template<class Archive> void hibernate(Archive& ar) {
			ar.ioenum(m_filter);
			for (unsigned int i = 0; i < 8; ++i)
				ar.io(m_par[i]);
		}

	protected:
		filter_t m_filter;
		double m_par[8];
	};

	class GlobalParameters {
	public:
		GlobalParameters(void);

		bool set_variable(variable_t var, const Expr::const_ptr_t& expr);
		std::ostream& print(std::ostream& os, unsigned int indent = 0) const;
		std::ostream& print_mss(std::ostream& os, unsigned int indent = 0) const;

		const ImageFilter& get_imagefilters(void) const { return m_imagefilters; }
		double get_opacity(void) const { return m_opacity; }
		compositeop_t get_compop(void) const { return m_compop; }

		void set_imagefilters(const ImageFilter& x) { m_imagefilters = x; }
		void set_opacity(double x) { m_opacity = x; }
		void set_compop(compositeop_t x) { m_compop = x; }

		template<class Archive> void hibernate(Archive& ar) {
			m_imagefilters.hibernate(ar);
			ar.io(m_opacity);
			ar.ioenum(m_compop);
		}

	protected:
		ImageFilter m_imagefilters;
		double m_opacity;
		compositeop_t m_compop;
	};

	class Parameters;

	class ObjectParameters {
	public:
		typedef std::map<PrefixAllocator::prefix_t,MarkerParams> marker_t;
		typedef std::map<PrefixAllocator::prefix_t,LineParams> line_t;
		typedef std::map<PrefixAllocator::prefix_t,LinePatternParams> linepattern_t;
		typedef std::map<PrefixAllocator::prefix_t,PolygonParams> polygon_t;
		typedef std::map<PrefixAllocator::prefix_t,PolygonPatternParams> polygonpattern_t;
		typedef std::map<PrefixAllocator::prefix_t,ShieldParams> shield_t;
		typedef std::map<PrefixAllocator::prefix_t,TextParams> text_t;

		ObjectParameters(void);

		bool set_variable(variable_t var, PrefixAllocator::prefix_t prefix, const Expr::const_ptr_t& expr);
		std::ostream& print(std::ostream& os, const PrefixAllocator& palloc, unsigned int indent = 0) const;
		std::ostream& print_mss(std::ostream& os, const PrefixAllocator& palloc, unsigned int indent = 0) const;
		std::ostream& print(std::ostream& os, unsigned int indent = 0) const;
		std::ostream& print_mss(std::ostream& os, unsigned int indent = 0) const;

		const marker_t& get_marker(void) const { return m_marker; }
		const line_t& get_line(void) const { return m_line; }
		const linepattern_t& get_linepattern(void) const { return m_linepattern; }
		const polygon_t& get_polygon(void) const { return m_polygon; }
		const polygonpattern_t& get_polygonpattern(void) const { return m_polygonpattern; }
		const shield_t& get_shield(void) const { return m_shield; }
		const text_t& get_text(void) const { return m_text; }

		marker_t& get_marker(void) { return m_marker; }
		line_t& get_line(void) { return m_line; }
		linepattern_t& get_linepattern(void) { return m_linepattern; }
		polygon_t& get_polygon(void) { return m_polygon; }
		polygonpattern_t& get_polygonpattern(void) { return m_polygonpattern; }
		shield_t& get_shield(void) { return m_shield; }
		text_t& get_text(void) { return m_text; }

		bool is_marker(PrefixAllocator::prefix_t x) const;
		bool is_line(PrefixAllocator::prefix_t x) const;
		bool is_linepattern(PrefixAllocator::prefix_t x) const;
		bool is_polygon(PrefixAllocator::prefix_t x) const;
		bool is_polygonpattern(PrefixAllocator::prefix_t x) const;
		bool is_shield(PrefixAllocator::prefix_t x) const;
		bool is_text(PrefixAllocator::prefix_t x) const;

		const MarkerParams& get_marker(PrefixAllocator::prefix_t x) const;
		const LineParams& get_line(PrefixAllocator::prefix_t x) const;
		const LinePatternParams& get_linepattern(PrefixAllocator::prefix_t x) const;
		const PolygonParams& get_polygon(PrefixAllocator::prefix_t x) const;
		const PolygonPatternParams& get_polygonpattern(PrefixAllocator::prefix_t x) const;
		const ShieldParams& get_shield(PrefixAllocator::prefix_t x) const;
		const TextParams& get_text(PrefixAllocator::prefix_t x) const;

		MarkerParams& get_marker(PrefixAllocator::prefix_t x) { return m_marker[x]; }
		LineParams& get_line(PrefixAllocator::prefix_t x) { return m_line[x]; }
		LinePatternParams& get_linepattern(PrefixAllocator::prefix_t x) { return m_linepattern[x]; }
		PolygonParams& get_polygon(PrefixAllocator::prefix_t x) { return m_polygon[x]; }
		PolygonPatternParams& get_polygonpattern(PrefixAllocator::prefix_t x) { return m_polygonpattern[x]; }
		ShieldParams& get_shield(PrefixAllocator::prefix_t x) { return m_shield[x]; }
		TextParams& get_text(PrefixAllocator::prefix_t x) { return m_text[x]; }

		std::set<PrefixAllocator::prefix_t> get_sets(void) const;
		bool empty(void) const;

		template<class Archive> void hibernate(Archive& ar) {
			hibernate_one(ar, m_marker);
			hibernate_one(ar, m_line);
			hibernate_one(ar, m_linepattern);
			hibernate_one(ar, m_polygon);
			hibernate_one(ar, m_polygonpattern);
			hibernate_one(ar, m_shield);
			hibernate_one(ar, m_text);
		}

	protected:
		marker_t m_marker;
		line_t m_line;
		linepattern_t m_linepattern;
		polygon_t m_polygon;
		polygonpattern_t m_polygonpattern;
		shield_t m_shield;
		text_t m_text;

		template<class Archive, typename T> void hibernate_one(Archive& ar, T& m) {
			typename T::mapped_type::size_type n(m.size());
			ar.ioleb(n);
			if (ar.is_load()) {
				m.clear();
				for (; n; --n) {
					PrefixAllocator::prefix_t x;
					ar.ioenum(x);
					std::pair<typename T::mapped_type::iterator,bool> ins(m.insert(typename T::mapped_type::value_type(x, T())));
					ins.first->second.hibernate(ar);
				}
			} else {
				for (typename T::mapped_type::iterator i(m.begin()), e(m.end()); i != e; ++i) {
					ar.ioenum(const_cast<PrefixAllocator::prefix_t&>(i->first));
					i->second.hibernate(ar);
				}
			}
		}

	};

	class LayerParameters : public std::map<LayerAllocator::layer_t,ObjectParameters> {
	public:
		LayerParameters(void);
		std::ostream& print(std::ostream& os, const LayerAllocator& lalloc, const PrefixAllocator& palloc, unsigned int indent = 0) const;
		std::ostream& print(std::ostream& os, const LayerAllocator& lalloc, const prefixallocators_t& pallocs, unsigned int indent = 0) const;

		template<class Archive> void hibernate(Archive& ar) {
			size_type n(size());
			ar.ioleb(n);
			if (ar.is_load()) {
				clear();
				for (; n; --n) {
				        LayerAllocator::layer_t x;
					ar.ioenum(x);
					std::pair<iterator,bool> ins(insert(value_type(x, Parameters())));
					ins.first->second.hibernate(ar);
				}
			} else {
				for (iterator i(begin()), e(end()); i != e; ++i) {
					ar.ioenum(const_cast<LayerAllocator::layer_t&>(i->first));
					i->second.hibernate(ar);
				}
			}
		}			
	};

	class LayerGlobalParameters : public std::map<LayerAllocator::layer_t,GlobalParameters> {
	public:
		LayerGlobalParameters(void);
		std::ostream& print(std::ostream& os, const LayerAllocator& lalloc, unsigned int indent = 0) const;

		template<class Archive> void hibernate(Archive& ar) {
			size_type n(size());
			ar.ioleb(n);
			if (ar.is_load()) {
				clear();
				for (; n; --n) {
				        LayerAllocator::layer_t x;
					ar.ioenum(x);
					std::pair<iterator,bool> ins(insert(value_type(x, Parameters())));
					ins.first->second.hibernate(ar);
				}
			} else {
				for (iterator i(begin()), e(end()); i != e; ++i) {
					ar.ioenum(const_cast<LayerAllocator::layer_t&>(i->first));
					i->second.hibernate(ar);
				}
			}
		}			
	};

	class MapParameters {
	public:
		MapParameters(void);

		bool set_variable(variable_t var, const Expr::const_ptr_t& expr);

		std::ostream& print(std::ostream& os, unsigned int indent = 0) const;
		std::ostream& print_mss(std::ostream& os, unsigned int indent = 0) const;

		const Color& get_backgroundcol(void) const { return m_backgroundcol; }

		void set_backgroundcol(const Color& x) { m_backgroundcol = x; }

		template<class Archive> void hibernate(Archive& ar) {
			m_backgroundcol.hibernate(ar);
		}

	protected:
		Color m_backgroundcol;
	};

	class Statement {
	public:
		class ApplyErrorVisitor;

		typedef boost::intrusive_ptr<Statement> ptr_t;
		typedef boost::intrusive_ptr<const Statement> const_ptr_t;
		typedef Expr::variables_t variables_t;

		virtual ~Statement();

		unsigned int breference(void) const { return ++m_refcount; }
		unsigned int bunreference(void) const { return --m_refcount; }
		unsigned int get_refcount(void) const { return m_refcount; }
		ptr_t get_ptr(void) { return ptr_t(this); }
		const_ptr_t get_ptr(void) const { return const_ptr_t(this); }
		friend inline void intrusive_ptr_add_ref(const Statement *expr) { expr->breference(); }
		friend inline void intrusive_ptr_release(const Statement *expr) { if (!expr->bunreference()) delete expr; }

		virtual const_ptr_t simplify(void) const { return ptr_t(); }
		virtual const_ptr_t simplify(const std::string& field, const Value& value) const { return ptr_t(); }
		virtual const_ptr_t simplify(const Fields& fields) const { return ptr_t(); }
		virtual const_ptr_t simplify(const std::string& var, const Expr::const_ptr_t& expr) const { return ptr_t(); }
		virtual const_ptr_t simplify(const variables_t& vars) const { return ptr_t(); }
		virtual const_ptr_t simplify(const LayerID& layer) const { return ptr_t(); }
		virtual const_ptr_t simplify(const ClassID& cls) const { return ptr_t(); }
		virtual const_ptr_t simplify(Visitor& v) const { return ptr_t(); }
		virtual const_ptr_t simplify_cb(Visitor& v) const = 0;
		virtual void visit(Visitor& v) const = 0;

		virtual Statement::const_ptr_t kill_variable_assignment(variable_t var, const std::string& pfx = "") const = 0;
		virtual Statement::const_ptr_t merge(const const_ptr_t& p) const = 0;
		virtual bool is_empty(void) const { return false; }
		virtual bool has_condition(void) const { return false; }
		void apply(ApplyErrorVisitor& vis, LayerParameters& par, LayerAllocator& lalloc, prefixallocators_t& pallocs) const;
		void apply(ApplyErrorVisitor& vis, LayerGlobalParameters& par, LayerAllocator& lalloc) const;
		virtual void apply(ApplyErrorVisitor& vis, LayerParameters& par, LayerAllocator& lalloc, prefixallocators_t& pallocs, const std::string& layer) const = 0;
		virtual void apply(ApplyErrorVisitor& vis, LayerGlobalParameters& par, LayerAllocator& lalloc, const std::string& layer, bool layeruncertain) const = 0;
		virtual std::ostream& print(std::ostream& os, unsigned int indent) const = 0;

	protected:
		mutable std::atomic<unsigned int> m_refcount;

		Statement(void);
	};

	class Statements : public Statement {
	public:
		typedef boost::intrusive_ptr<Statements> ptr_t;
		typedef boost::intrusive_ptr<const Statements> const_ptr_t;
		typedef std::vector<Statement::const_ptr_t> statements_t;

		Statements(void);
		Statements(const statements_t& stmt);
		Statements(const statements_t&& stmt);

		virtual Statement::const_ptr_t simplify(void) const;
		virtual Statement::const_ptr_t simplify(const std::string& field, const Value& value) const;
		virtual Statement::const_ptr_t simplify(const Fields& fields) const;
		virtual Statement::const_ptr_t simplify(const std::string& var, const Expr::const_ptr_t& expr) const;
		virtual Statement::const_ptr_t simplify(const variables_t& vars) const;
		virtual Statement::const_ptr_t simplify(const LayerID& layer) const;
		virtual Statement::const_ptr_t simplify(const ClassID& cls) const;
		virtual Statement::const_ptr_t simplify(Visitor& v) const;
		virtual Statement::const_ptr_t simplify_cb(Visitor& v) const { return v.modify(*this); }
		virtual void visit(Visitor& v) const;

		virtual Statement::const_ptr_t kill_variable_assignment(variable_t var, const std::string& pfx = "") const;
		virtual Statement::const_ptr_t merge(const Statement::const_ptr_t& p) const;
		virtual bool is_empty(void) const { return m_statements.empty(); }
		virtual void apply(ApplyErrorVisitor& vis, LayerParameters& par, LayerAllocator& lalloc, prefixallocators_t& pallocs, const std::string& layer) const;
		virtual void apply(ApplyErrorVisitor& vis, LayerGlobalParameters& par, LayerAllocator& lalloc, const std::string& layer, bool layeruncertain) const;
		virtual std::ostream& print(std::ostream& os, unsigned int indent) const;

		const statements_t& get_statements(void) const { return m_statements; }
		void add_statement(const Statement::const_ptr_t& p);
		void add_statements(const statements_t& stmt);

	protected:
		statements_t m_statements;

		static bool simplify_stmt_more(statements_t& stmt);
		template <typename... Args> bool simplify_stmt(statements_t& stmt, Args&&... args) const;
		template <typename... Args> ptr_t simplify_int(Args&&... args) const;
		bool kill_variable_assignment_stmt(statements_t& stmt, variable_t var, const std::string& pfx = "") const;
		void apply_int(ApplyErrorVisitor& vis, LayerParameters& par, LayerAllocator& lalloc, prefixallocators_t& pallocs, const std::string& layer) const;
		void apply_int(ApplyErrorVisitor& vis, LayerGlobalParameters& par, LayerAllocator& lalloc, const std::string& layer, bool layeruncertain) const;
	};

	class VariableAssignments : public Statement {
	public:
		class Assignment {
		public:
			Assignment(variable_t var = variable_t::invalid, const Expr::const_ptr_t& expr = Expr::const_ptr_t(), const std::string& pfx = std::string());

			const std::string& get_prefix() const { return m_prefix; }
			const Expr::const_ptr_t& get_expr() const { return m_expr; }
			variable_t get_var() const { return m_var; }

			bool simplify(Assignment& a) const;
			bool simplify(Assignment& a, const std::string& field, const Value& value) const;
			bool simplify(Assignment& a, const Fields& fields) const;
			bool simplify(Assignment& a, const std::string& var, const Expr::const_ptr_t& expr) const;
			bool simplify(Assignment& a, const variables_t& vars) const;
			bool simplify(Assignment& a, const LayerID& layer) const;
			bool simplify(Assignment& a, const ClassID& cls) const;
			bool simplify(Assignment& a, Visitor& v) const;

			bool apply(ApplyErrorVisitor& vis, ObjectParameters& par, PrefixAllocator& palloc) const;
			bool apply(ApplyErrorVisitor& vis, GlobalParameters& par, bool layeruncertain) const;
			std::ostream& print(std::ostream& os, unsigned int indent) const;

		protected:
			std::string m_prefix;
			Expr::const_ptr_t m_expr;
			variable_t m_var;

			template <typename... Args> bool simplify_int(Assignment& a, Args&&... args) const;
		};

		typedef boost::intrusive_ptr<VariableAssignments> ptr_t;
		typedef boost::intrusive_ptr<const VariableAssignments> const_ptr_t;
		typedef std::vector<Assignment> assignments_t;

		VariableAssignments(void);
		VariableAssignments(const assignments_t& a);
		VariableAssignments(assignments_t&& a);

		virtual Statement::const_ptr_t simplify(void) const;
		virtual Statement::const_ptr_t simplify(const std::string& field, const Value& value) const;
		virtual Statement::const_ptr_t simplify(const Fields& fields) const;
		virtual Statement::const_ptr_t simplify(const std::string& var, const Expr::const_ptr_t& expr) const;
		virtual Statement::const_ptr_t simplify(const variables_t& vars) const;
		virtual Statement::const_ptr_t simplify(const LayerID& layer) const;
		virtual Statement::const_ptr_t simplify(const ClassID& cls) const;
		virtual Statement::const_ptr_t simplify(Visitor& v) const;
		virtual Statement::const_ptr_t simplify_cb(Visitor& v) const { return v.modify(*this); }
		virtual void visit(Visitor& v) const;

		virtual Statement::const_ptr_t kill_variable_assignment(variable_t var, const std::string& pfx = "") const;
		virtual Statement::const_ptr_t merge(const Statement::const_ptr_t& p) const;
		virtual bool is_empty(void) const { return m_assignments.empty(); }
		virtual void apply(ApplyErrorVisitor& vis, LayerParameters& par, LayerAllocator& lalloc, prefixallocators_t& pallocs, const std::string& layer) const;
		virtual void apply(ApplyErrorVisitor& vis, LayerGlobalParameters& par, LayerAllocator& lalloc, const std::string& layer, bool layeruncertain) const;
		virtual std::ostream& print(std::ostream& os, unsigned int indent) const;

		const assignments_t& get_assignments(void) const { return m_assignments; }
		void add_assignment(const Assignment& a);
		void add_assignment(variable_t var = variable_t::invalid, const Expr::const_ptr_t& expr = Expr::const_ptr_t(), const std::string& pfx = std::string()) {
			add_assignment(Assignment(var, expr, pfx));
		}

	protected:
		assignments_t m_assignments;

		static bool remove_superseded_assignments(assignments_t& a);
		template <typename... Args> ptr_t simplify_int(Args&&... args) const;
	};

	class ConditionChain;
	class Conditions;

	class SubBlock : public Statements {
	public:
		class ConditionChain : public Expr::exprlist_t {
		public:
			typedef Expr::exprlist_t base_t;
			enum class simplify_t : uint8_t {
				unchanged,
				changed,
				false_,
				true_
			};

			simplify_t simplify(ConditionChain& c) const;
			simplify_t simplify(ConditionChain& c, const std::string& field, const Value& value) const;
			simplify_t simplify(ConditionChain& c, const Fields& fields) const;
			simplify_t simplify(ConditionChain& c, const std::string& var, const Expr::const_ptr_t& expr) const;
			simplify_t simplify(ConditionChain& c, const variables_t& vars) const;
			simplify_t simplify(ConditionChain& c, const LayerID& layer) const;
			simplify_t simplify(ConditionChain& c, const ClassID& cls) const;
			simplify_t simplify(ConditionChain& c, Visitor& v) const;

			bool is_copylayer(std::string& layer) const;
			bool is_condcopylayer(std::string& layer) const;

			std::ostream& print(std::ostream& os) const;

		protected:
			template <typename... Args> simplify_t simplify_int(ConditionChain& c, Args&&... args) const;
		};

		class Conditions : public std::vector<ConditionChain> {
		public:
			typedef std::vector<ConditionChain> base_t;
			typedef ConditionChain::simplify_t simplify_t;

			simplify_t simplify(Conditions& c) const;
			simplify_t simplify(Conditions& c, const std::string& field, const Value& value) const;
			simplify_t simplify(Conditions& c, const Fields& fields) const;
			simplify_t simplify(Conditions& c, const std::string& var, const Expr::const_ptr_t& expr) const;
			simplify_t simplify(Conditions& c, const variables_t& vars) const;
			simplify_t simplify(Conditions& c, const LayerID& layer) const;
			simplify_t simplify(Conditions& c, const ClassID& cls) const;
			simplify_t simplify(Conditions& c, Visitor& v) const;

			bool is_copylayer(std::set<std::string>& layer) const;
			bool is_condcopylayer(std::set<std::string>& layer) const;

			std::ostream& print(std::ostream& os, unsigned int indent) const;

		protected:
			template <typename... Args> simplify_t simplify_int(Conditions& c, Args&&... args) const;
		};


		typedef boost::intrusive_ptr<SubBlock> ptr_t;
		typedef boost::intrusive_ptr<const SubBlock> const_ptr_t;

		SubBlock(void);
		SubBlock(const Conditions& cond, const statements_t& stmt);
		SubBlock(Conditions&& cond, statements_t&& stmt);
		
		virtual Statement::const_ptr_t simplify(void) const;
		virtual Statement::const_ptr_t simplify(const std::string& field, const Value& value) const;
		virtual Statement::const_ptr_t simplify(const Fields& fields) const;
		virtual Statement::const_ptr_t simplify(const std::string& var, const Expr::const_ptr_t& expr) const;
		virtual Statement::const_ptr_t simplify(const variables_t& vars) const;
		virtual Statement::const_ptr_t simplify(const LayerID& layer) const;
		virtual Statement::const_ptr_t simplify(const ClassID& cls) const;
		virtual Statement::const_ptr_t simplify(Visitor& v) const;
		virtual Statement::const_ptr_t simplify_cb(Visitor& v) const { return v.modify(*this); }
		virtual void visit(Visitor& v) const;

		virtual Statement::const_ptr_t kill_variable_assignment(variable_t var, const std::string& pfx = "") const;
		virtual Statement::const_ptr_t merge(const Statement::const_ptr_t& p) const { return ptr_t(); }
		virtual bool has_condition(void) const { return true; }
		virtual void apply(ApplyErrorVisitor& vis, LayerParameters& par, LayerAllocator& lalloc, prefixallocators_t& pallocs, const std::string& layer) const;
		virtual void apply(ApplyErrorVisitor& vis, LayerGlobalParameters& par, LayerAllocator& lalloc, const std::string& layer, bool layeruncertain) const;
		virtual std::ostream& print(std::ostream& os, unsigned int indent) const;

		const Conditions& get_conditions(void) const { return m_conditions; }
		void add_conditions(const ConditionChain& c);
		void add_conditions(const Conditions& c);

	protected:
		Conditions m_conditions;

		template <typename... Args> Statements::ptr_t simplify_int(Args&&... args) const;
	};

	class Visitor {
	public:
		virtual void visit(const Expr& e) {}
		virtual void visitend(const Expr& e) {}
		virtual void visit(const Statements& s) {}
		virtual void visitend(const Statements& s) {}
		virtual void visit(const VariableAssignments& v) {}
		virtual void visitend(const VariableAssignments& v) {}
		virtual void visit(const SubBlock& s) {}
		virtual void visitstatements(const SubBlock& s) {}
		virtual void visitend(const SubBlock& s) {}
		virtual Expr::const_ptr_t modify(const Expr& e) { return Expr::const_ptr_t(); }
		virtual Statement::const_ptr_t modify(const Statements& s) { return Statement::const_ptr_t(); }
		virtual Statement::const_ptr_t modify(const VariableAssignments& v) { return Statement::const_ptr_t(); }
		virtual Statement::const_ptr_t modify(const SubBlock& s) { return Statement::const_ptr_t(); }
	};

	class Statement::ApplyErrorVisitor {
	public:
		ApplyErrorVisitor(void) {}
		virtual void visit(const Statements& s) {}
		virtual void visitend(const Statements& s) {}
		virtual void visit(const VariableAssignments& v) {}
		virtual void visitend(const VariableAssignments& v) {}
		virtual void visit(const SubBlock& s) {}
		virtual void visitend(const SubBlock& s) {}
		virtual void error(const VariableAssignments::Assignment& assignment) {}
		virtual void error(const VariableAssignments::Assignment& assignment, const std::exception& e) {}
		virtual void error(const SubBlock& s, const std::set<std::string>& layers) {}
	};

	void parse_stylefiles(const std::vector<std::string>& fns, const std::string& dirname, std::ostream *msg = nullptr);
	void parse_project(std::istream& is, const std::string& dirname, std::ostream *msg = nullptr);
	void parse_project(const std::string& fn, std::ostream *msg = nullptr);

	MapParameters& get_mapparameters(void) { return m_mapparameters; }
	const MapParameters& get_mapparameters(void) const { return m_mapparameters; }

	const Statement::const_ptr_t& get_statements(void) const { return m_statements; }
	void set_statements(const Statement::const_ptr_t& p) { m_statements = p; }

protected:
	class ExprBase;
	class ExprNull;
	class ExprKeyword;
	class ExprCompositeOp;
	class ExprLayerID;
	class ExprClassID;
	class ExprBool;
	class ExprInt;
	class ExprDouble;
	class ExprString;
	class ExprColor;
	class ExprURL;
	class ExprDirectory;
	class ExprVarRef;
	class ExprField;
	class ExprCopyLayer;
	class ExprList;
	class ExprFunction;
	class ExprCompare;

	MapParameters m_mapparameters;
	Statement::const_ptr_t m_statements;
};

const std::string& to_str(CartoCSS::compositeop_t x);
inline std::ostream& operator<<(std::ostream& os, CartoCSS::compositeop_t x) { return os << to_str(x); }
const std::string& to_str(CartoCSS::placement_t x);
inline std::ostream& operator<<(std::ostream& os, CartoCSS::placement_t x) { return os << to_str(x); }
const std::string& to_str(CartoCSS::variable_t x);
inline std::ostream& operator<<(std::ostream& os, CartoCSS::variable_t x) { return os << to_str(x); }
const std::string& to_str(CartoCSS::Expr::keyword_t x);
inline std::ostream& operator<<(std::ostream& os, CartoCSS::Expr::keyword_t x) { return os << to_str(x); }
const std::string& to_str(CartoCSS::Expr::func_t x);
inline std::ostream& operator<<(std::ostream& os, CartoCSS::Expr::func_t x) { return os << to_str(x); }
const std::string& to_str(CartoCSS::Expr::compop_t x);
inline std::ostream& operator<<(std::ostream& os, CartoCSS::Expr::compop_t x) { return os << to_str(x); }
const std::string& to_str(CartoCSS::LineParams::cap_t x);
inline std::ostream& operator<<(std::ostream& os, CartoCSS::LineParams::cap_t x) { return os << to_str(x); }
const std::string& to_str(CartoCSS::LineParams::join_t x);
inline std::ostream& operator<<(std::ostream& os, CartoCSS::LineParams::join_t x) { return os << to_str(x); }
const std::string& to_str(CartoCSS::LineParams::simplifyalgo_t x);
inline std::ostream& operator<<(std::ostream& os, CartoCSS::LineParams::simplifyalgo_t x) { return os << to_str(x); }
const std::string& to_str(CartoCSS::PolygonPatternParams::alignment_t x);
inline std::ostream& operator<<(std::ostream& os, CartoCSS::PolygonPatternParams::alignment_t x) { return os << to_str(x); }
const std::string& to_str(CartoCSS::BasicTextParams::horizontalalignment_t x);
inline std::ostream& operator<<(std::ostream& os, CartoCSS::BasicTextParams::horizontalalignment_t x) { return os << to_str(x); }
const std::string& to_str(CartoCSS::BasicTextParams::verticalalignment_t x);
inline std::ostream& operator<<(std::ostream& os, CartoCSS::BasicTextParams::verticalalignment_t x) { return os << to_str(x); }
const std::string& to_str(CartoCSS::BasicTextParams::placementtype_t x);
inline std::ostream& operator<<(std::ostream& os, CartoCSS::BasicTextParams::placementtype_t x) { return os << to_str(x); }
const std::string& to_str(CartoCSS::TextParams::upright_t x);
inline std::ostream& operator<<(std::ostream& os, CartoCSS::TextParams::upright_t x) { return os << to_str(x); }
const std::string& to_str(CartoCSS::ImageFilter::filter_t x);
inline std::ostream& operator<<(std::ostream& os, CartoCSS::ImageFilter::filter_t x) { return os << to_str(x); }

#endif /* CARTOCSS_HH */
