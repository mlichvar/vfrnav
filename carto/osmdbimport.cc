/*****************************************************************************/

/*
 *      osmdbimport.cc  --  OpenStreetMap proprietary database importer.
 *
 *      Copyright (C) 2020  Thomas Sailer (t.sailer@alumni.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/*****************************************************************************/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <iomanip>
#include <fstream>
#include <zlib.h>

#include "osmdb.h"
#include "osmstyle.h"
#include "hibernate.h"
#include "mapfile.h"

#ifdef HAVE_OSMIUM

#include <osmium/handler.hpp>
#include <osmium/io/any_input.hpp>
#include <osmium/osm/node.hpp>
#include <osmium/osm/way.hpp>
#include <osmium/osm/relation.hpp>
#include <osmium/visitor.hpp>

class OSMDB::Import : public osmium::handler::Handler {
public:
	Import(const OsmStyle& style, const std::string& tmpdir, importflags_t flags = importflags_t::none);
	~Import();

	void osmimport(int outfd, const std::string& infname);
	void osmimport(const std::string& outfname, const std::string& infname);

	void node(const osmium::Node& node);
	void way(const osmium::Way& way);
	void relation(const osmium::Relation& rel);

protected:
	class NodeTableEntry : public BinFileEntry<16> {
	public:
		void set_id(Object::id_t id);
		Object::id_t get_id(void) const;
		Point get_location(void) const;
		void set_location(const Point& location);
	};

	class WayTableEntry : public BinFileEntry<20> {
	public:
		void set_id(Object::id_t id);
		Object::id_t get_id(void) const;
		uint64_t get_dataoffs(void) const;
		void set_dataoffs(uint64_t offs);
		uint32_t get_datasize(void) const;
		void set_datasize(uint32_t sz);
	};

	struct FileIDSorter {
		bool operator()(const Object::id_t& a, const Object::id_t& b) const { return a < b; }
		bool operator()(const NodeTableEntry& a, const Object::id_t& b) const { return operator()(a.get_id(), b); }
		bool operator()(const Object::id_t& a, const NodeTableEntry& b) const { return operator()(a, b.get_id()); }
		bool operator()(const NodeTableEntry& a, const NodeTableEntry& b) const { return operator()(a.get_id(), b.get_id()); }
		bool operator()(const WayTableEntry& a, const Object::id_t& b) const { return operator()(a.get_id(), b); }
		bool operator()(const Object::id_t& a, const WayTableEntry& b) const { return operator()(a, b.get_id()); }
		bool operator()(const WayTableEntry& a, const WayTableEntry& b) const { return operator()(a.get_id(), b.get_id()); }
	};

	template<typename T> class TagRemap {
	public:
		typedef T key_t;
		typedef typename std::underlying_type<T>::type underlying_t;
		typedef std::map<std::string, key_t> ms2k_t;

		TagRemap(void);
		key_t find(const std::string& x) const;
		key_t find(const std::string& x);
		key_t highest(void) const;
		const std::string& find(key_t k) const;
		void compute_remap(std::vector<key_t>& table) const;
		void clear(void);
		const ms2k_t& alltags(void) const { return m_ms2k; }

	protected:
		ms2k_t m_ms2k;
		typedef std::map<key_t, std::string> mk2s_t;
		mk2s_t m_mk2s;
	};

	class OsmPolygon {
	public:
		typedef std::vector<Object::id_t> nodes_t;

		OsmPolygon(void);
		OsmPolygon(const osmium::WayNodeList& nl);

		const nodes_t& get_nodes(void) const { return m_nodes; }
		nodes_t& get_nodes(void) { return m_nodes; }
		const PolygonSimple& get_poly(void) const { return m_poly; }
		PolygonSimple& get_poly(void) { return m_poly; }
		void set_poly(const PolygonSimple& p) { m_poly = p; }

		bool resolve(const NodeTableEntry *nodetblb, const NodeTableEntry *nodetble, Object::id_t wayid);

		bool is_closed(void) const;

		void reverse(void);
		int64_t area2(void) const { return m_poly.area2(); }
		void make_counterclockwise(void);
		void make_clockwise(void);
		bool is_resolved(void) const { return m_nodes.size() == m_poly.size(); }
		bool joinline(const OsmPolygon& p);

		template<class Archive> void hibernate(Archive& ar) {
			{
				uint32_t sz(m_nodes.size());
				ar.ioleb(sz);
				if (ar.is_load())
					m_nodes.resize(sz);
				for (typename nodes_t::iterator i(m_nodes.begin()), e(m_nodes.end()); i != e; ++i)
					ar.io(*i);
			}
			m_poly.hibernate_binary(ar);
		}

	protected:
		nodes_t m_nodes;
		PolygonSimple m_poly;
	};

	struct LayerEntry {
		const char *m_highway;
		int m_offset;
		bool m_roads;
	};

	static const LayerEntry layers[];

	struct ValueTableEntry {
		const char *m_tag;
		const char * const *m_values;
		unsigned int m_nrvalues;
	};

	static const char * const polygon_value_aerialway[1];
	static const char * const polygon_value_boundary[3];
	static const char * const polygon_value_highway[2];
	static const char * const polygon_value_junction[1];
	static const char * const polygon_value_railway[1];
	static const ValueTableEntry polygon_values[5];
	static const char * const linestring_value_golf[3];
	static const char * const linestring_value_emergency[5];
	static const char * const linestring_value_historic[1];
	static const char * const linestring_value_leisure[2];
	static const char * const linestring_value_man_made[5];
	static const char * const linestring_value_natural[5];
	static const char * const linestring_value_power[3];
	static const char * const linestring_value_tourism[1];
	static const char * const linestring_value_waterway[9];
	static const ValueTableEntry linestring_values[9];
	static const char * const polygon_keys[32];
	static const char * const delete_keys[];
	static const char * const delete_prefixes[];
	static const char * const excluded_railway_service[];

	struct RoadTableTagValueEntry {
		const char *m_value;
		int32_t m_z;
		bool m_road;
	};

	struct RoadTableEntry {
		const char *m_tag;
		const RoadTableTagValueEntry *m_values;
		unsigned int m_nrvalues;
	};

	static const RoadTableTagValueEntry road_table_highway[];
	static const RoadTableTagValueEntry road_table_railway[];
	static const RoadTableTagValueEntry road_table_aeroway[];
	static const RoadTableTagValueEntry road_table_boundary[];
	static const RoadTableEntry road_table[];

	struct CharPtrCompare {
		bool operator()(const char *a, const char *b) const;
		bool operator()(const ValueTableEntry& a, const char *b) const;
		bool operator()(const char *a, const ValueTableEntry& b) const;
		bool operator()(const ValueTableEntry& a, const ValueTableEntry& b) const;
		bool operator()(const RoadTableTagValueEntry& a, const char *b) const;
		bool operator()(const char *a, const RoadTableTagValueEntry& b) const;
		bool operator()(const RoadTableTagValueEntry& a, const RoadTableTagValueEntry& b) const;
		bool operator()(const RoadTableEntry& a, const char *b) const;
		bool operator()(const char *a, const RoadTableEntry& b) const;
		bool operator()(const RoadTableEntry& a, const RoadTableEntry& b) const;
	};


	static constexpr OsmStyleEntry::flags_t seflag_road = static_cast<OsmStyleEntry::flags_t>(0x80);

	static void check_tables(void);
	static void check_table(const char * const *tb, const char * const *te, const std::string& tname);
	static void check_table(const ValueTableEntry *tb, const ValueTableEntry *te, const std::string& tname);
	static void check_table(const RoadTableEntry *tb, const RoadTableEntry *te, const std::string& tname);
	void clear(void);
	void clear_nofiles(void);
	OsmStyleEntry::flags_t setobj(Object& o, const osmium::OSMObject& obj, OsmStyleEntry::entity_t objtype);
	static std::string get_tag_string(const osmium::TagList& tags, const char *key, const std::string& dflt);
	static int get_tag_int(const osmium::TagList& tags, const char *key, int dflt);
	static bool get_tag_bool(const osmium::TagList& tags, const char *key, bool dflt);
	static bool is_multipolygon(const osmium::TagList& tags);
	static bool is_multiline(const osmium::TagList& tags);
	static bool is_way_area(const std::string& key, const std::string& value);
	template<typename EntryType> void save_object_data(EntryType& ent, const std::string& data);
	template<typename ObjType> void save_object(const ObjType& obj);
	void reportosmimport(bool force = false);
	void mapnodes(void);
	void mapways(void);
	bool resolve_polygons(MultiPolygonHole& mph, std::vector<OsmPolygon> polys);
	uint32_t copy(Object::type_t objtyp, int fd, uint64_t addr, uint64_t source, uint32_t len) const;

	BinFileHeader m_hdr;
	static constexpr bool compressionstatistics = false;
	TagRemap<Object::tagkey_t> m_tagkeys;
	TagRemap<Object::tagname_t> m_tagnames;
	std::vector<Object::tagkey_t> m_tagkeyremap;
	std::vector<Object::tagname_t> m_tagnameremap;
	const OsmStyle& m_style;
	std::string m_tmpdir;
	MapFile m_nodetbl;
	MapFile m_waytbl;
	std::size_t m_comprsave;
	int m_nodetblfd;
	int m_waytblfd;
	int m_waydatafd;
	int m_datafd;
	int m_objfd[4];
	unsigned int m_osmpass;
	unsigned int m_osmstat[6];
	importflags_t m_flags;
};

void OSMDB::Import::NodeTableEntry::set_id(Object::id_t id)
{
	writes64(0, id);
}

OSMDB::Object::id_t OSMDB::Import::NodeTableEntry::get_id(void) const
{
	return reads64(0);
}

Point OSMDB::Import::NodeTableEntry::get_location(void) const
{
	return Point(reads32(8), reads32(12));
}

void OSMDB::Import::NodeTableEntry::set_location(const Point& location)
{
	writes32(8, location.get_lon());
	writes32(12, location.get_lat());
}

void OSMDB::Import::WayTableEntry::set_id(Object::id_t id)
{
	writes64(0, id);
}

OSMDB::Object::id_t OSMDB::Import::WayTableEntry::get_id(void) const
{
	return reads64(0);
}

uint64_t OSMDB::Import::WayTableEntry::get_dataoffs(void) const
{
	return readu64(8);
}

void OSMDB::Import::WayTableEntry::set_dataoffs(uint64_t offs)
{
	writeu64(8, offs);
}

uint32_t OSMDB::Import::WayTableEntry::get_datasize(void) const
{
	return readu32(16);
}

void OSMDB::Import::WayTableEntry::set_datasize(uint32_t sz)
{
	writeu32(16, sz);
}

template <typename T>
OSMDB::Import::TagRemap<T>::TagRemap(void)
{
}

template <typename T>
typename OSMDB::Import::TagRemap<T>::key_t OSMDB::Import::TagRemap<T>::find(const std::string& x) const
{
	typename ms2k_t::const_iterator i(m_ms2k.find(x));
	if (i != m_ms2k.end())
		return i->second;
	return key_t::invalid;
}

template <typename T>
typename OSMDB::Import::TagRemap<T>::key_t OSMDB::Import::TagRemap<T>::find(const std::string& x)
{
	{
		typename ms2k_t::const_iterator i(m_ms2k.find(x));
		if (i != m_ms2k.end())
			return i->second;
	}
	key_t k(static_cast<key_t>(0));
	if (!m_mk2s.empty())
		k = static_cast<key_t>(static_cast<underlying_t>(highest()) + 1);
	m_ms2k[x] = k;
	m_mk2s[k] = x;
	return k;
}

template <typename T>
typename OSMDB::Import::TagRemap<T>::key_t OSMDB::Import::TagRemap<T>::highest(void) const
{
	return m_mk2s.rbegin()->first;
}

template <typename T>
const std::string& OSMDB::Import::TagRemap<T>::find(key_t k) const
{
	typename mk2s_t::const_iterator i(m_mk2s.find(k));
	if (i != m_mk2s.end())
		return i->second;
	static const std::string empty;
	return empty;
}

template <typename T>
void OSMDB::Import::TagRemap<T>::compute_remap(std::vector<key_t>& table) const
{
	table.clear();
	if (m_ms2k.empty())
		return;
	table.resize(static_cast<underlying_t>(highest()) + 1, key_t::invalid);
	underlying_t count(0);
	for (const auto& k : m_ms2k)
		table[static_cast<underlying_t>(k.second)] = static_cast<key_t>(count++);
}

template <typename T>
void OSMDB::Import::TagRemap<T>::clear(void)
{
	m_ms2k.clear();
	m_mk2s.clear();
}

OSMDB::Import::OsmPolygon::OsmPolygon(void)
{
}

OSMDB::Import::OsmPolygon::OsmPolygon(const osmium::WayNodeList& nl)
{
	m_nodes.reserve(nl.size());
	for (const auto& x : nl)
		m_nodes.push_back(x.ref());
}

bool OSMDB::Import::OsmPolygon::resolve(const NodeTableEntry *nodetblb, const NodeTableEntry *nodetble, Object::id_t wayid)
{
	m_poly.clear();
	if (!nodetblb)
		return false;
	static constexpr bool logerror(true);
	std::vector<Object::id_t> nodenotfound;
	{
		bool err(false);
		for (const auto& nid : get_nodes()) {
			const NodeTableEntry *np(std::lower_bound(nodetblb, nodetble, nid, FileIDSorter()));
			if (np == nodetble || np->get_id() != nid) {
				if (logerror)
					nodenotfound.push_back(nid);
				err = true;
				break;
			}
			m_poly.push_back(np->get_location());
		}
		if (!err)
			return true;
	}
	if (logerror && !nodenotfound.empty()) {
		std::sort(nodenotfound.begin(), nodenotfound.end());
		std::cerr << "Error: way " << wayid << std::endl;
		if (!nodenotfound.empty()) {
			std::cerr << "  Nonexisting nodes";
			for (auto id : nodenotfound)
				std::cerr << ' ' << id;
			std::cerr << std::endl;
		}
	}
	return false;
}

bool OSMDB::Import::OsmPolygon::is_closed(void) const
{
	if (m_nodes.empty())
		return false;
	return m_nodes.front() == m_nodes.back();
}

void OSMDB::Import::OsmPolygon::reverse(void)
{
	std::reverse(m_nodes.begin(), m_nodes.end());
	m_poly.reverse();
}

void OSMDB::Import::OsmPolygon::make_counterclockwise(void)
{
	if (area2() >= 0)
		return;
	reverse();
}

void OSMDB::Import::OsmPolygon::make_clockwise(void)
{
	if (area2() <= 0)
		return;
	reverse();
}

bool OSMDB::Import::OsmPolygon::joinline(const OsmPolygon& p)
{
	if (!is_resolved() || !p.is_resolved() || p.get_nodes().size() < 2)
		return false;
	if (m_nodes.empty()) {
		m_nodes = p.m_nodes;
		m_poly = p.m_poly;
		return true;
	}
	if (m_nodes.size() == 1)
		return false;
	if (m_nodes.back() == p.get_nodes().front()) {
		m_nodes.insert(m_nodes.end(), p.get_nodes().begin() + 1, p.get_nodes().end());
		m_poly.insert(m_poly.end(), p.get_poly().begin() + 1, p.get_poly().end());
		return true;
	}
	if (m_nodes.back() == p.get_nodes().back()) {
		m_nodes.insert(m_nodes.end(), p.get_nodes().rbegin() + 1, p.get_nodes().rend());
		m_poly.insert(m_poly.end(), p.get_poly().rbegin() + 1, p.get_poly().rend());
		return true;
	}
	if (m_nodes.front() == p.get_nodes().back()) {
		m_nodes.insert(m_nodes.begin(), p.get_nodes().begin(), p.get_nodes().end() - 1);
		m_poly.insert(m_poly.begin(), p.get_poly().begin(), p.get_poly().end() - 1);
		return true;
	}
	if (m_nodes.front() == p.get_nodes().front()) {
		m_nodes.insert(m_nodes.begin(), p.get_nodes().rbegin(), p.get_nodes().rend() - 1);
		m_poly.insert(m_poly.begin(), p.get_poly().rbegin(), p.get_poly().rend() - 1);
		return true;
	}
	return false;
}

bool OSMDB::Import::CharPtrCompare::operator()(const char *a, const char *b) const
{
	if (!b)
		return false;
	if (!a)
		return true;
	return strcmp(a, b) < 0;
}

bool OSMDB::Import::CharPtrCompare::operator()(const ValueTableEntry& a, const char *b) const
{
	return (*this)(a.m_tag, b);
}

bool OSMDB::Import::CharPtrCompare::operator()(const char *a, const ValueTableEntry& b) const
{
	return (*this)(a, b.m_tag);
}

bool OSMDB::Import::CharPtrCompare::operator()(const ValueTableEntry& a, const ValueTableEntry& b) const
{
	return (*this)(a.m_tag, b.m_tag);
}

bool OSMDB::Import::CharPtrCompare::operator()(const RoadTableTagValueEntry& a, const char *b) const
{
	return (*this)(a.m_value, b);
}

bool OSMDB::Import::CharPtrCompare::operator()(const char *a, const RoadTableTagValueEntry& b) const
{
	return (*this)(a, b.m_value);
}

bool OSMDB::Import::CharPtrCompare::operator()(const RoadTableTagValueEntry& a, const RoadTableTagValueEntry& b) const
{
	return (*this)(a.m_value, b.m_value);
}

bool OSMDB::Import::CharPtrCompare::operator()(const RoadTableEntry& a, const char *b) const
{
	return (*this)(a.m_tag, b);
}

bool OSMDB::Import::CharPtrCompare::operator()(const char *a, const RoadTableEntry& b) const
{
	return (*this)(a, b.m_tag);
}

bool OSMDB::Import::CharPtrCompare::operator()(const RoadTableEntry& a, const RoadTableEntry& b) const
{
	return (*this)(a.m_tag, b.m_tag);
}


const OSMDB::Import::LayerEntry OSMDB::Import::layers[] = {
	{ "proposed", 1, false },       { "construction", 2, false },
	{ "steps", 10, false },         { "cycleway", 10, false },
	{ "bridleway", 10, false },     { "footway", 10, false },
	{ "path", 10, false },          { "track", 11, false },
	{ "service", 15, false },
	
	{ "tertiary_link", 24, false }, { "secondary_link", 25, true },
	{ "primary_link", 27, true },   { "trunk_link", 28, true },
	{ "motorway_link", 29, true },
	
	{ "raceway", 30, false },       { "pedestrian", 31, false },
	{ "living_street", 32, false }, { "road", 33, false },
	{ "unclassified", 33, false },  { "residential", 33, false },
	{ "tertiary", 34, false },      { "secondary", 36, true },
	{ "primary", 37, true },        { "trunk", 38, true },
	{ "motorway", 39, true }
};

// Objects with any of the following key/value combinations will be treated as polygon
const char * const OSMDB::Import::polygon_value_aerialway[1] = {
	"station"
};

const char * const OSMDB::Import::polygon_value_boundary[3] = {
	"aboriginal_lands", "national_park", "protected_area"
};

const char * const OSMDB::Import::polygon_value_highway[2] = {
	"rest_area", "services"
};

const char * const OSMDB::Import::polygon_value_junction[1] = {
	"yes"
};

const char * const OSMDB::Import::polygon_value_railway[1] = {
	"station"
};

const OSMDB::Import::ValueTableEntry OSMDB::Import::polygon_values[5] = {
	{ "aerialway", polygon_value_aerialway, sizeof(polygon_value_aerialway)/sizeof(polygon_value_aerialway[0]) },
	{ "boundary", polygon_value_boundary, sizeof(polygon_value_boundary)/sizeof(polygon_value_boundary[0]) },
	{ "highway", polygon_value_highway, sizeof(polygon_value_highway)/sizeof(polygon_value_highway[0]) },
	{ "junction", polygon_value_junction, sizeof(polygon_value_junction)/sizeof(polygon_value_junction[0]) },
	{ "railway", polygon_value_railway, sizeof(polygon_value_railway)/sizeof(polygon_value_railway[0]) }
};

// Objects with any of the following key/value combinations will be treated as linestring
const char * const OSMDB::Import::linestring_value_golf[3] = {
	"cartpath", "hole", "path"
};

const char * const OSMDB::Import::linestring_value_emergency[5] = {
	"designated", "destination", "no", "official", "yes"
};

const char * const OSMDB::Import::linestring_value_historic[1] = {
	"citywalls"
};

const char * const OSMDB::Import::linestring_value_leisure[2] = {
	"slipway", "track"
};

const char * const OSMDB::Import::linestring_value_man_made[5] = {
	"breakwater", "cutline", "embankment", "groyne", "pipeline"
};

const char * const OSMDB::Import::linestring_value_natural[5] = {
	"arete", "cliff", "earth_bank", "ridge", "tree_row"
};

const char * const OSMDB::Import::linestring_value_power[3] = {
	"cable", "line", "minor_line"
};

const char * const OSMDB::Import::linestring_value_tourism[1] = {
	"yes"
};

const char * const OSMDB::Import::linestring_value_waterway[9] = {
	"canal", "derelict_canal", "ditch", "drain", "river", "stream", "tidal_channel", "wadi", "weir"
};

const OSMDB::Import::ValueTableEntry OSMDB::Import::linestring_values[9] = {
	{ "emergency", linestring_value_emergency, sizeof(linestring_value_emergency)/sizeof(linestring_value_emergency[0]) },
	{ "golf", linestring_value_golf, sizeof(linestring_value_golf)/sizeof(linestring_value_golf[0]) },
	{ "historic", linestring_value_historic, sizeof(linestring_value_historic)/sizeof(linestring_value_historic[0]) },
	{ "leisure", linestring_value_leisure, sizeof(linestring_value_leisure)/sizeof(linestring_value_leisure[0]) },
	{ "man_made", linestring_value_man_made, sizeof(linestring_value_man_made)/sizeof(linestring_value_man_made[0]) },
	{ "natural", linestring_value_natural, sizeof(linestring_value_natural)/sizeof(linestring_value_natural[0]) },
	{ "power", linestring_value_power, sizeof(linestring_value_power)/sizeof(linestring_value_power[0]) },
	{ "tourism", linestring_value_tourism, sizeof(linestring_value_tourism)/sizeof(linestring_value_tourism[0]) },
	{ "waterway", linestring_value_waterway, sizeof(linestring_value_waterway)/sizeof(linestring_value_waterway[0]) }
};

// Objects with any of the following keys will be treated as polygon
const char * const OSMDB::Import::polygon_keys[32] = {
	"abandoned:aeroway",
	"abandoned:amenity",
	"abandoned:building",
	"abandoned:landuse",
	"abandoned:power",
	"aeroway",
	"allotments",
	"amenity",
	"area:highway",
	"building",
	"building:part",
	"club",
	"craft",
	"emergency",
	"golf",
	"harbour",
	"healthcare",
	"historic",
	"landuse",
	"leisure",
	"man_made",
	"military",
	"natural",
	"office",
	"place",
	"power",
	"public_transport",
	"shop",
	"tourism",
	"water",
	"waterway",
	"wetland"
};

// The following keys will be deleted
const char * const OSMDB::Import::delete_keys[] = {
	"OBJTYPE",               // misc
	"OPPDATERIN",            // OPPDATERIN (NO)
	"SK53_bulk:load",        // misc
	"accuracy:meters",       // NHN (CA)
	"addr:city:simc",        // Various imports (PL)
	"addr:street:sym_ul",    // Various imports (PL)
	"attribution",
	"building:ruian:type",   // RUIAN (CZ) 
	"building:usage:pl",     // Various imports (PL)
	"building:use:pl",       // Various imports (PL)
	"chicago:building_id",   // Chicago Building Import (US)
	"comment",
	"created_by",            // Tags generally dropped by editors, not otherwise covered
	"dcgis:gis_id",          // DCGIS (Washington DC, US)
	"dibavod:id",            // DIBAVOD (CZ)
	"fixme",
	"gst:feat_id",           // GST (DK)
	"import",                // misc
	"import_uuid",           // misc
	"linz2osm:objectid",     // LINZ (NZ)
	"lojic:bgnum",           // Louisville, Kentucky/Building Outlines Import (US)
	"maaamet:ETAK",          // Maa-amet (EE)
	"massgis:way_id",        // MassGIS (Massachusetts, US)
	"note",
	"nycdoitt:bin",          // Building Identification Number (New York, US)
	"odbl",                  // Tags generally dropped by editors, not otherwise covered
	"osak:identifier",       // osak (DK)
	"project:eurosha_2012",  // EUROSHA (Various countries)
	"raba:id",               // RABA (SK)
	"ref:FR:FANTOIR",        // FANTOIR (FR)
	"ref:UrbIS",             // UrbIS (Brussels, BE)
	"ref:ruian",             // RUIAN (CZ)
	"ref:ruian:addr",        // RUIAN (CZ)
	"source",
	"source_ref",
	"statscan:rbuid",        // StatsCan (CA)
	"teryt:simc",            // TERYT (PL)
	"uir_adr:ADRESA_KOD",    // UIR-ADR (CZ)
	"waterway:type"          // NHN (CA)
};

// Prefixes to delete
const char * const OSMDB::Import::delete_prefixes[] = {
	"CLC:",         // Corine (CLC) (Europe)
	"KSJ2:",        // KSJ2 (JA); See also note:ja and source_ref above
	"LINZ2OSM:",    // LINZ (NZ)
	"LINZ:",        // LINZ (NZ)
	"NHD:",         // National Hydrography Dataset (US)
	"WroclawGIS:",  // WroclawGIS (PL)
	"canvec:",      // CanVec (CA)
	"geobase:",     // Geobase (CA)
	"gnis:",        // GNIS (US)
	"it:fvg:",      // Friuli Venezia Giulia (IT)
	"kms:",         // kms (DK)
	"mvdgis:",      // mvdgis (Montevideo, UY)
	"naptan:",      // Naptan (UK)
	"ngbe:",        // ngbe (ES);  See also note:es and source:file above
	"nhd:",         // National Hydrography Dataset (US)
	"note:",
	"source:",
	"tiger:",       // TIGER (US)
	"yh:"           // Yahoo/ALPS (JA)
};

const OSMDB::Import::RoadTableTagValueEntry OSMDB::Import::road_table_highway[] = {
	{ "bridleway",       100, false },
	{ "cycleway",        100, false },
	{ "footway",         100, false },
	{ "living_street",   320, false },
	{ "motorway",        380, true },
	{ "motorway_link",   240, true },
	{ "path",            100, false },
	{ "pedestrian",      310, false },
	{ "platform",        90,  false },
	{ "primary",         360, true },
	{ "primary_link",    220, true },
	{ "raceway",         300, false },
	{ "residential",     330, false },
	{ "road",            330, false },
	{ "secondary",       350, true },
	{ "secondary_link",  210, true },
	{ "service",         150, false },
	{ "steps",           90,  false },
	{ "tertiary",        340, false },
	{ "tertiary_link",   200, false },
	{ "track",           110, false },
	{ "trunk",           370, true },
	{ "trunk_link",      230, true },
	{ "unclassified",    330, false }
};

const OSMDB::Import::RoadTableTagValueEntry OSMDB::Import::road_table_railway[] = {
	{ "construction",    400, false },
	{ "disused",         400, false },
	{ "funicular",       420, true },
	{ "light_rail",      420, true },
	{ "miniature",       420, false },
	{ "monorail",        420, false },
	{ "narrow_gauge",    420, true },
	{ "platform",        90,  false },
	{ "preserved",       420, false },
	{ "rail",            440, true },
	{ "subway",          420, true },
	{ "tram",            410, false },
	{ "turntable",       420, false }
};

const OSMDB::Import::RoadTableTagValueEntry OSMDB::Import::road_table_aeroway[] = {
	{ "runway",          60,  false },
	{ "taxiway",         50,  false },
};

const OSMDB::Import::RoadTableTagValueEntry OSMDB::Import::road_table_boundary[] = {
	{ "administrative",  0,  true }
};

const OSMDB::Import::RoadTableEntry OSMDB::Import::road_table[] = {
	{ "aeroway", road_table_aeroway, sizeof(road_table_aeroway)/sizeof(road_table_aeroway[0]) },
	{ "boundary", road_table_boundary, sizeof(road_table_boundary)/sizeof(road_table_boundary[0]) },
	{ "highway", road_table_highway, sizeof(road_table_highway)/sizeof(road_table_highway[0]) },
	{ "railway", road_table_railway, sizeof(road_table_railway)/sizeof(road_table_railway[0]) }
};

const char * const OSMDB::Import::excluded_railway_service[] = {
	"siding",
	"spur",
	"yard"
};

constexpr bool OSMDB::Import::compressionstatistics;
constexpr OsmStyleEntry::flags_t OSMDB::Import::seflag_road;

OSMDB::Import::Import(const OsmStyle& style, const std::string& tmpdir, importflags_t flags)
	: m_style(style), m_tmpdir(tmpdir), m_comprsave(0), m_nodetblfd(-1), m_waytblfd(-1),
	  m_waydatafd(-1), m_datafd(-1), m_osmpass(0), m_flags(flags)
{
	for (unsigned int i = 0; i < 4; ++i)
		m_objfd[i] = -1;
	for (unsigned int i = 0; i < 6; ++i)
		m_osmstat[i] = 0;
	check_tables();
	// create files
	static const char * const fntable[8] = {
		"points",
		"lines",
		"roads",
		"areas",
		"data",
		"nodes",
		"ways",
		"waydata"
	};
	for (unsigned int i = 0; i < 8; ++i) {
		std::string ffn(Glib::build_filename(m_tmpdir, fntable[i]));
		int fd = ::open(ffn.c_str(), O_CREAT | O_TRUNC | O_EXCL | O_CLOEXEC | O_NOATIME | O_RDWR, S_IRUSR | S_IWUSR);
		if (fd == -1)
			MapFile::throw_strerror(("Cannot create file " + ffn).c_str());
		switch (i) {
		case 0:
		case 1:
		case 2:
		case 3:
			m_objfd[i] = fd;
			break;

		case 4:
			m_datafd = fd;
			break;

		case 5:
			m_nodetblfd = fd;
			break;

		case 6:
			m_waytblfd = fd;
			break;

		case 7:
			m_waydatafd = fd;
			break;

		default:
			break;
		}
		if ((m_flags & importflags_t::keep) == importflags_t::none)
			unlink(ffn.c_str());
	}
}

OSMDB::Import::~Import()
{
	clear();
	for (unsigned int i = 0; i < 4; ++i)
		if (m_objfd[i] != -1)
			::close(m_objfd[i]);
	if (m_nodetblfd != -1)
		::close(m_nodetblfd);
	if (m_waytblfd != -1)
		::close(m_waytblfd);
	if (m_waydatafd != -1)
		::close(m_waydatafd);
	if (m_datafd != -1)
		::close(m_datafd);
}

void OSMDB::Import::check_table(const char * const *tb, const char * const *te, const std::string& tname)
{
	if (tb == te)
		return;
	for (const char * const *p(tb + 1); p != te; ++p)
		if (!CharPtrCompare()(p[-1], p[0]) || CharPtrCompare()(p[0], p[-1]))
			throw std::runtime_error(tname + " table sort error, " + p[-1] + "/" + p[0]);
}

void OSMDB::Import::check_table(const ValueTableEntry *tb, const ValueTableEntry *te, const std::string& tname)
{
	if (tb == te)
		return;
	for (const ValueTableEntry *p(&tb[1]); p != te; ++p)
		if (!CharPtrCompare()(p[-1].m_tag, p[0].m_tag) || CharPtrCompare()(p[0].m_tag, p[-1].m_tag))
			throw std::runtime_error(tname + " table sort error, " + p[-1].m_tag + "/" + p[0].m_tag);
	for (const ValueTableEntry *p(tb); p != te; ++p)
		for (unsigned int i(1); i < p->m_nrvalues; ++i)
			if (!CharPtrCompare()(p->m_values[i-1], p->m_values[i]) || CharPtrCompare()(p->m_values[i], p->m_values[i-1]))
				throw std::runtime_error(tname + " subtable " + p->m_tag + " sort error, " + p->m_values[i-1] + "/" + p->m_values[i]);
}

void OSMDB::Import::check_table(const RoadTableEntry *tb, const RoadTableEntry *te, const std::string& tname)
{
	if (tb == te)
		return;
	for (const RoadTableEntry *p(&tb[1]); p != te; ++p)
		if (!CharPtrCompare()(p[-1].m_tag, p[0].m_tag) || CharPtrCompare()(p[0].m_tag, p[-1].m_tag))
			throw std::runtime_error(tname + " table sort error, " + p[-1].m_tag + "/" + p[0].m_tag);
	for (const RoadTableEntry *p(tb); p != te; ++p)
		for (unsigned int i(1); i < p->m_nrvalues; ++i)
			if (!CharPtrCompare()(p->m_values[i-1].m_value, p->m_values[i].m_value) || CharPtrCompare()(p->m_values[i].m_value, p->m_values[i-1].m_value))
				throw std::runtime_error(tname + " subtable " + p->m_tag + " sort error, " + p->m_values[i-1].m_value + "/" + p->m_values[i].m_value);
}

void OSMDB::Import::check_tables(void)
{
	check_table(polygon_keys, &polygon_keys[sizeof(polygon_keys)/sizeof(polygon_keys[0])],"polygon_keys");
	check_table(delete_keys, &delete_keys[sizeof(delete_keys)/sizeof(delete_keys[0])], "delete_keys");
	check_table(delete_prefixes, &delete_prefixes[sizeof(delete_prefixes)/sizeof(delete_prefixes[0])], "delete_prefixes");
	check_table(excluded_railway_service, &excluded_railway_service[sizeof(excluded_railway_service)/sizeof(excluded_railway_service[0])], "excluded_railway_service");
       	check_table(polygon_values, &polygon_values[sizeof(polygon_values)/sizeof(polygon_values[0])], "polygon_values");
	check_table(linestring_values, &linestring_values[sizeof(linestring_values)/sizeof(linestring_values[0])], "linestring_values");
	check_table(road_table, &road_table[sizeof(road_table)/sizeof(road_table[0])], "road_table");
}

void OSMDB::Import::clear_nofiles(void)
{
	m_hdr.set_signature();
	for (Object::type_t t(Object::type_t::point); t <= Object::type_t::area; t = static_cast<Object::type_t>(static_cast<unsigned int>(t) + 1)) {
		m_hdr.set_objdiroffs(t, 0);
		m_hdr.set_objdirentries(t, 0);
	}
	m_tagkeys.clear();
	m_tagnames.clear();
	m_tagkeyremap.clear();
	m_tagnameremap.clear();
	// unmap file views
	m_nodetbl.close();
	m_waytbl.close();
	// statistics
	m_comprsave = 0;
	for (unsigned int i = 0; i < 6; ++i)
		m_osmstat[i] = 0;	
}

void OSMDB::Import::clear(void)
{
	clear_nofiles();
	// truncate files
	for (unsigned int i = 0; i < 4; ++i) {
		if (m_objfd[i] != -1) {
			lseek(m_objfd[i], 0, SEEK_SET);
			ftruncate(m_objfd[i], 0);
		}
	}
	if (m_nodetblfd != -1) {
		lseek(m_nodetblfd, 0, SEEK_SET);
		ftruncate(m_nodetblfd, 0);
	}
	if (m_waytblfd != -1) {
		lseek(m_waytblfd, 0, SEEK_SET);
		ftruncate(m_waytblfd, 0);
	}
	if (m_waydatafd != -1) {
		lseek(m_waydatafd, 0, SEEK_SET);
		ftruncate(m_waydatafd, 0);
	}
	if (m_datafd != -1) {
		lseek(m_datafd, 0, SEEK_SET);
		ftruncate(m_datafd, 0);
	}
}

void OSMDB::Import::reportosmimport(bool force)
{
	if (!force && (m_osmstat[0] + m_osmstat[2] + m_osmstat[4]) & ((1U << 17)-1U))
		return;
	std::cerr << "Reading " << m_osmstat[0] << '/' << m_osmstat[1] << " nodes "
		  << m_osmstat[2] << '/' << m_osmstat[3] << " ways "
		  << m_osmstat[4] << '/' << m_osmstat[5] << " relations" << std::endl;
}

std::string OSMDB::Import::get_tag_string(const osmium::TagList& tags, const char *key, const std::string& dflt)
{
	const char *cp(tags.get_value_by_key(key, 0));
	if (cp)
		return cp;
	return dflt;
}

int OSMDB::Import::get_tag_int(const osmium::TagList& tags, const char *key, int dflt)
{
	const char *cp(tags.get_value_by_key(key, 0));
	if (!cp)
		return dflt;
	char *cp1;
	int v(strtol(cp, &cp1, 10));
	while (std::isspace(*cp1))
		++cp1;
	if (*cp1)
		return dflt;
	return v;
}

bool OSMDB::Import::get_tag_bool(const osmium::TagList& tags, const char *key, bool dflt)
{
	const char *cp(tags.get_value_by_key(key, 0));
	if (!cp)
		return dflt;
	if (!dflt && (!strcmp(cp, "yes") || !strcmp(cp, "true") || !strcmp(cp, "1")))
		return true;
	if (dflt && (!strcmp(cp, "no") || !strcmp(cp, "false") || !strcmp(cp, "0")))
		return false;
	return dflt;
}

bool OSMDB::Import::is_multipolygon(const osmium::TagList& tags)
{
	const char *cp(tags.get_value_by_key("type", 0));
	if (!cp)
		return false;
	return !strcmp(cp, "multipolygon");
}

bool OSMDB::Import::is_multiline(const osmium::TagList& tags)
{
	const char *cp(tags.get_value_by_key("type", 0));
	if (!cp)
		return false;
	return !strcmp(cp, "route") || !strcmp(cp, "boundary");
}

bool OSMDB::Import::is_way_area(const std::string& key, const std::string& value)
{
	if (value != "no") {
		const char * const *polygon_keys_end = polygon_keys + sizeof(polygon_keys)/sizeof(polygon_keys[0]);
		const char * const *pk(std::lower_bound(polygon_keys, polygon_keys_end, key.c_str(), CharPtrCompare()));
		if (pk != polygon_keys_end && key == *pk) {
			const ValueTableEntry *linestring_values_end = linestring_values + sizeof(linestring_values)/sizeof(linestring_values[0]);
			const ValueTableEntry *lsv(std::lower_bound(linestring_values, linestring_values_end, key.c_str(), CharPtrCompare()));
			if (lsv == linestring_values_end || lsv->m_tag != key)
				return true;
			const char * const *values_begin = lsv->m_values;
			const char * const *values_end = values_begin + lsv->m_nrvalues;
			const char * const *val(std::lower_bound(values_begin, values_end, value.c_str(), CharPtrCompare()));
			if (val == values_end || *val != value)
				return true;
		}
	}
	const ValueTableEntry *polygon_values_end = polygon_values + sizeof(polygon_values)/sizeof(polygon_values[0]);
	const ValueTableEntry *psv(std::lower_bound(polygon_values, polygon_values_end, key.c_str(), CharPtrCompare()));
	if (psv != polygon_values_end && psv->m_tag == key) {
		const char * const *values_begin = psv->m_values;
		const char * const *values_end = values_begin + psv->m_nrvalues;
		const char * const *val(std::lower_bound(values_begin, values_end, value.c_str(), CharPtrCompare()));
		if (val != values_end && *val == value)
			return true;
	}
	return false;
}

OsmStyleEntry::flags_t OSMDB::Import::setobj(Object& o, const osmium::OSMObject& obj, OsmStyleEntry::entity_t objtype)
{
	o.set_id(obj.id());
	OsmStyleEntry::flags_t r(OsmStyleEntry::flags_t::none);
	bool boundary(false);
	if (m_style.empty()) {
		// use tables from https://raw.githubusercontent.com/gravitystorm/openstreetmap-carto/master/openstreetmap-carto.lua
		Object::tags_t tags;
		bool hasarea(false);
		if (objtype != OsmStyleEntry::entity_t::relation)
			r = OsmStyleEntry::flags_t::linear;
		for (const auto& t : obj.tags()) {
			switch (objtype) {
			case OsmStyleEntry::entity_t::way:
				if (!strcmp(t.key(), "area")) {
					r = strcmp(t.value(), "yes") ? OsmStyleEntry::flags_t::linear : OsmStyleEntry::flags_t::polygon;
					hasarea = true;
				}
				if (!hasarea && is_way_area(t.key(), t.value()))
					r = OsmStyleEntry::flags_t::polygon;
				break;

			case OsmStyleEntry::entity_t::relation:
				boundary = boundary || !strcmp(t.key(), "boundary");
				if (strcmp(t.key(), "type"))
					break;
				if (!strcmp(t.value(), "multipolygon")) {
					r = OsmStyleEntry::flags_t::polygon;
					continue;
				}
				if (!strcmp(t.value(), "route")) {
					r = OsmStyleEntry::flags_t::linear;
					continue;
				}
				if (!strcmp(t.value(), "boundary")) {
					r = OsmStyleEntry::flags_t::linear;
					boundary = true;
					continue;
				}
				continue;

			default:
				break;
			}
			// check whether to delete the key
			{
				const char * const *delete_keys_end = delete_keys + sizeof(delete_keys)/sizeof(delete_keys[0]);
				const char * const *dk(std::lower_bound(delete_keys, delete_keys_end, t.key(), CharPtrCompare()));
				if (dk != delete_keys_end && *dk == t.key())
					continue;
			}
			{
				const char * const *delete_prefixes_end = delete_prefixes + sizeof(delete_prefixes)/sizeof(delete_prefixes[0]);
				const char * const *dp(std::upper_bound(delete_prefixes, delete_prefixes_end, t.key(), CharPtrCompare()));
				if (dp != delete_prefixes) {
					--dp;
					if (!strncmp(t.key(), *dp, strlen(*dp)))
						continue;
				}
			}
			// special case handling for layer tag
			if (!strcmp(t.key(), "layer")) {
				const char *cp(t.value());
				char *cp1(0);
				long l(strtoll(cp, &cp1, 10));
				if (cp1 == cp || *cp1 || l < -100 || l > 100)
					continue;
				std::ostringstream oss;
				oss << l;
				tags.push_back(std::make_tuple(m_tagkeys.find(t.key()), m_tagnames.find(oss.str())));
				continue;
			}				
			tags.push_back(std::make_tuple(m_tagkeys.find(t.key()), m_tagnames.find(t.value())));
		}
		if (r == OsmStyleEntry::flags_t::none)
			return OsmStyleEntry::flags_t::none;
		if (boundary)
			r = OsmStyleEntry::flags_t::linear;
		if (tags.empty() && (m_flags & importflags_t::nofilter) == importflags_t::none)
			return OsmStyleEntry::flags_t::none;
		o.set_tags(tags);
	} else {
		Object::tags_t tags;
		for (const auto& t : obj.tags()) {
			const OsmStyleEntry *se(m_style.find(t.key()));
			if (se) {
				if ((se->get_osmtype() & objtype) != OsmStyleEntry::entity_t::none) {
					r |= se->get_flags();
					if ((se->get_flags() & OsmStyleEntry::flags_t::delete_) != OsmStyleEntry::flags_t::none)
						continue;
				}
			}
			tags.push_back(std::make_tuple(m_tagkeys.find(t.key()), m_tagnames.find(t.value())));
		}
		if (objtype == OsmStyleEntry::entity_t::way && get_tag_bool(obj.tags(), "area", false))
			r |= OsmStyleEntry::flags_t::polygon;
		o.set_tags(tags);
		r &= OsmStyleEntry::flags_t::polylin;
		if (r == OsmStyleEntry::flags_t::none) {
			if ((m_flags & importflags_t::nofilter) == importflags_t::none)
				return OsmStyleEntry::flags_t::none;
			r |= OsmStyleEntry::flags_t::linear;
		}
		if (objtype == OsmStyleEntry::entity_t::relation) {
			if (is_multipolygon(obj.tags()))
				r = OsmStyleEntry::flags_t::polygon;
			else if (is_multiline(obj.tags()))
				r = OsmStyleEntry::flags_t::linear;
			else
				return OsmStyleEntry::flags_t::none;
		}
		{
			const char *bdry(obj.tags().get_value_by_key("boundary", 0));
			boundary = bdry && !strcmp(bdry, "administrative");
		}
	}
	// compute zorder
	int32_t zorder(0);
	bool roads(false);
	if (m_style.empty()) {
		const RoadTableEntry *road_table_end = road_table + sizeof(road_table)/sizeof(road_table[0]);
		bool hwyconstr(false), railway(false), servicerailwayexclude(false);
		const RoadTableTagValueEntry *construction(0);
		for (const auto& t : obj.tags()) {
			hwyconstr = hwyconstr || !(strcmp(t.key(), "highway") || strcmp(t.value(), "construction"));
			railway = railway || !strcmp(t.key(), "railway");
			if (!strcmp(t.key(), "construction")) {
				const RoadTableEntry *rt(std::lower_bound(road_table, road_table_end, "highway", CharPtrCompare()));
				if (rt != road_table_end && !strcmp(rt->m_tag, "highway")) {
					const RoadTableTagValueEntry *values = rt->m_values;
					const RoadTableTagValueEntry *values_end = values + rt->m_nrvalues;
					const RoadTableTagValueEntry *rtv(std::lower_bound(values, values_end, t.value(), CharPtrCompare()));
					if (rtv != values_end && !strcmp(rtv->m_value, t.value()))
						construction = rtv;
				}
			}
			if (!strcmp(t.key(), "service")) {
				const char * const *excluded_railway_service_end = excluded_railway_service + sizeof(excluded_railway_service)/sizeof(excluded_railway_service[0]);
				const char * const *e(std::lower_bound(excluded_railway_service, excluded_railway_service_end, t.value(), CharPtrCompare()));
				servicerailwayexclude = servicerailwayexclude || (e != excluded_railway_service_end && !strcmp(*e, t.value()));
			}
			const RoadTableEntry *rt(std::lower_bound(road_table, road_table_end, t.key(), CharPtrCompare()));
			if (rt == road_table_end || strcmp(rt->m_tag, t.key()))
				continue;
			const RoadTableTagValueEntry *values = rt->m_values;
			const RoadTableTagValueEntry *values_end = values + rt->m_nrvalues;
			const RoadTableTagValueEntry *rtv(std::lower_bound(values, values_end, t.value(), CharPtrCompare()));
			if (rtv == values_end || strcmp(rtv->m_value, t.value()))
				continue;
			zorder = std::max(zorder, rtv->m_z);
			roads = roads || rtv->m_road;
		}
		if (hwyconstr)
			zorder = std::max(zorder, construction ? (construction->m_z / 10) : 33);
		if (railway && servicerailwayexclude)
			roads = false;
	} else {
		zorder = get_tag_int(obj.tags(), "layer", 0) * 100;
		{
			const char *highway(obj.tags().get_value_by_key("highway", 0));
			if (highway) {
				const unsigned int nlayers = sizeof(layers)/sizeof(layers[0]);
				for (unsigned int i = 0; i < nlayers; i++) {
					if (!strcmp(highway, layers[i].m_highway)) {
						zorder += layers[i].m_offset;
						roads = layers[i].m_roads;
						break;
					}
				}
			}
		}

		{
			const char *railway(obj.tags().get_value_by_key("railway", 0));
			if (railway) {
				zorder += 35;
				roads = true;
			}
		}
		// Administrative boundaries are rendered at low zooms so we prefer to use the roads table
		if (boundary)
			roads = true;
		if (get_tag_bool(obj.tags(), "bridge", false))
			zorder += 100;

		if (get_tag_bool(obj.tags(), "tunnel", false))
			zorder -= 100;
	}
	if (roads)
		r |= seflag_road;
	else
		r &= ~seflag_road;
	o.set_zorder(zorder);
	return r;
}

template<typename EntryType>
void OSMDB::Import::save_object_data(EntryType& ent, const std::string& data)
{
	ent.set_dataoffs(lseek(m_datafd, 0, SEEK_CUR));
	ent.set_datasize(data.size());
	if (ent.get_datasize() != data.size()) {
		std::ostringstream oss;
		oss << "Data size truncated, blob size " << data.size();
		throw std::runtime_error(oss.str());
	}
	if (write(m_datafd, data.c_str(), ent.get_datasize()) != ent.get_datasize())
		MapFile::throw_strerror("Data Write error");
	if (compressionstatistics && data.size() > 16) {
		z_stream c_stream;
		memset(&c_stream, 0, sizeof(c_stream));
		c_stream.zalloc = (alloc_func)0;
		c_stream.zfree = (free_func)0;
		c_stream.opaque = (voidpf)0;
		int err(deflateInit(&c_stream, Z_BEST_COMPRESSION));
		if (!err) {
			uint8_t dout[data.size() + 64];
			c_stream.next_out = (Bytef *)dout;
                        c_stream.avail_out = sizeof(dout);
                        c_stream.next_in = (Bytef *)data.c_str();
                        c_stream.avail_in = data.size();
                        err = deflate(&c_stream, Z_FINISH);
                        if (err == Z_STREAM_END &&
			    c_stream.avail_in == 0 &&
			    c_stream.total_in == data.size() &&
			    c_stream.total_out < data.size())
				m_comprsave += data.size() - c_stream.total_out;
		}
	}
}

template<typename ObjType>
void OSMDB::Import::save_object(const ObjType& obj)
{
	BinFileLineAreaEntry ent;
	ent.set_id(obj.get_id());
	ent.set_bbox(obj.get_bbox());
	std::ostringstream blob;
	{
		HibernateWriteStream ar(blob);
		const_cast<ObjType&>(obj).hibernate_noid(ar);
	}
	save_object_data(ent, blob.str());
	if (write(m_objfd[static_cast<unsigned int>(ObjType::object_type)], &ent, sizeof(ent)) != sizeof(ent))
		MapFile::throw_strerror(ObjType::object_name + " table write error");
	m_hdr.set_objdirentries(ObjType::object_type, m_hdr.get_objdirentries(ObjType::object_type) + 1);	
}

template<>
void OSMDB::Import::save_object<OSMDB::ObjPoint>(const ObjPoint& obj)
{
	typedef OSMDB::ObjPoint ObjType;
	BinFilePointEntry ent;
	ent.set_id(obj.get_id());
	ent.set_location(obj.get_location());
	std::ostringstream blob;
	{
		HibernateWriteStream ar(blob);
		const_cast<ObjType&>(obj).Object::hibernate_noid(ar);
	}
	save_object_data(ent, blob.str());
	if (write(m_objfd[static_cast<unsigned int>(ObjType::object_type)], &ent, sizeof(ent)) != sizeof(ent))
		MapFile::throw_strerror(ObjType::object_name + " table write error");
	m_hdr.set_objdirentries(ObjType::object_type, m_hdr.get_objdirentries(ObjType::object_type) + 1);	
}

void OSMDB::Import::mapnodes(void)
{
	if (m_nodetbl.is_open() || m_nodetblfd == -1)
		return;
	m_nodetbl.open(m_nodetblfd, true, "node table");
	if (false)
		for (const NodeTableEntry *x(m_nodetbl.begin_as<NodeTableEntry>()), *xe(m_nodetbl.end_as<NodeTableEntry>()); x != xe; ++x)
			std::cerr << "Node " << x->get_id() << ": " << x->get_location().get_lat_str2()
				  << ' ' << x->get_location().get_lat_str2() << std::endl;
}

void OSMDB::Import::mapways(void)
{
	if (m_waytbl.is_open() || m_waytblfd == -1)
		return;
	m_waytbl.open(m_waytblfd, true, "way table");
}

void OSMDB::Import::node(const osmium::Node& node)
{
	if (!(m_osmpass & 1U) || node.deleted())
		return;
	++m_osmstat[0];	
	// location
	const osmium::Location& loc(node.location());
	if (!loc || !loc.valid()) {
		reportosmimport();
		return;
	}
	Point pt;
	pt.set_lon_deg_dbl(loc.lon_without_check());
	pt.set_lat_deg_dbl(loc.lat_without_check());
	if (pt.is_invalid()) {
		reportosmimport();
		return;
	}
	// unmap table
	m_nodetbl.close();
	// write to node table
	{
		NodeTableEntry ent;
		ent.set_id(node.id());
		ent.set_location(pt);
		if (write(m_nodetblfd, &ent, sizeof(ent)) != sizeof(ent))
			MapFile::throw_strerror("Node table write error");
	}
	// check if object needs to be stored in database
	ObjPoint obj;
	OsmStyleEntry::flags_t flg(setobj(obj, node, OsmStyleEntry::entity_t::node));
	if (flg == OsmStyleEntry::flags_t::none) {
		reportosmimport();
		return;
	}
	obj.set_location(pt);
	// save object to database
	save_object(obj);
	++m_osmstat[1];	
	reportosmimport();
}

void OSMDB::Import::way(const osmium::Way& way)
{
	if (!(m_osmpass & 2U) || way.deleted())
		return;
	++m_osmstat[2];	
	// unmap table
	m_waytbl.close();
	// look up nodes, resolve node references
	mapnodes();
	OsmPolygon p(way.nodes());
	if (!p.resolve(m_nodetbl.begin_as<NodeTableEntry>(), m_nodetbl.end_as<NodeTableEntry>(), way.id())) {
		reportosmimport();
		return;
	}
	// write to way table
	{
		std::ostringstream blob;
		{
			HibernateWriteStream ar(blob);
			p.hibernate(ar);
		}
		WayTableEntry ent;
		ent.set_id(way.id());
		ent.set_dataoffs(lseek(m_waydatafd, 0, SEEK_CUR));
		ent.set_datasize(blob.str().size());
		if (write(m_waydatafd, blob.str().c_str(), ent.get_datasize()) != ent.get_datasize())
			MapFile::throw_strerror("Way Data Write error");		
		if (write(m_waytblfd, &ent, sizeof(ent)) != sizeof(ent))
			MapFile::throw_strerror("Way table write error");
	}
	// check if object needs to be stored in database
	if (p.get_poly().empty()) {
		reportosmimport();
		return;
	}
	ObjRoad obj;
	OsmStyleEntry::flags_t flg(setobj(obj, way, OsmStyleEntry::entity_t::way));
	if (flg == OsmStyleEntry::flags_t::none) {
		reportosmimport();
		return;
	}
	if (false && obj.get_id() == 189298289) {
		std::cerr << "Wagerenhof: flg " << static_cast<unsigned int>(flg) << " closed " << (p.is_closed() ? "yes" : "no") << std::endl;
		for (const auto& t : way.tags())
			std::cerr << "  \"" << t.key() << "\" = \"" << t.value() << "\" is_way_area " << (is_way_area(t.key(), t.value()) ? "yes" : "no")
				  << std::endl;
	}
	if ((flg & OsmStyleEntry::flags_t::polygon) != OsmStyleEntry::flags_t::none && p.is_closed()) {
		ObjArea obja(obj.get_id(), obj.get_tags(), obj.get_zorder());
		{
			PolygonSimple ps(p.get_poly());
			ps.resize(ps.size() - 1);
			if (ps.size() < 3) {
				reportosmimport();
				return;
			}
			MultiPolygonHole mph;
			mph.push_back(PolygonHole(ps));
			obja.set_area(mph);
		}
		// save object to database
		save_object(obja);
	} else {
		obj.set_line(p.get_poly());
		// save object to database
		if ((flg & seflag_road) != OsmStyleEntry::flags_t::none)
			save_object(obj);
		else
			save_object<ObjLine>(obj);
	}
	++m_osmstat[3];	
	reportosmimport();
}

bool OSMDB::Import::resolve_polygons(MultiPolygonHole& mph, std::vector<OsmPolygon> polys)
{
	static constexpr bool debug(false);
	// first pass: store closed components
	for (std::vector<OsmPolygon>::iterator i(polys.begin()), e(polys.end()); i != e; ) {
		if (!i->is_resolved() || i->get_nodes().size() < 2) {
			i = polys.erase(i);
			e = polys.end();
			continue;
		}
		if (i->is_closed()) {
			i->make_counterclockwise();
			PolygonSimple ps(i->get_poly());
			ps.resize(ps.size() - 1);
			mph.push_back(PolygonHole(ps));
			i = polys.erase(i);
			e = polys.end();
			continue;
		}
		++i;
	}
	// second pass: try to construct closed path
	if (debug && !polys.empty()) {
		std::cerr << "  Polygon lines:" << std::endl;
		for (const auto& p : polys)
			std::cerr << "    " << p.get_nodes().front() << "->" << p.get_nodes().back() << std::endl;
	}
	while (!polys.empty()) {
		OsmPolygon p(*polys.begin());
		polys.erase(polys.begin());
		if (debug)
			std::cerr << "join: begin " << p.get_nodes().front() << "->" << p.get_nodes().back() << std::endl;
		for (std::vector<OsmPolygon>::iterator i(polys.begin()), e(polys.end()); i != e && !p.is_closed(); ) {
			if (!p.joinline(*i)) {
				++i;
				continue;
			}
			if (debug)
				std::cerr << "join: add " << i->get_nodes().front() << "->" << i->get_nodes().back()
					  << " result " << p.get_nodes().front() << "->" << p.get_nodes().back() << std::endl;
			polys.erase(i);
			i = polys.begin();
			e = polys.end();
		}
		if (!p.is_closed())
			return false;
		p.make_counterclockwise();
		PolygonSimple ps(p.get_poly());
		ps.resize(ps.size() - 1);
		mph.push_back(PolygonHole(ps));
	}
	return true;
}

void OSMDB::Import::relation(const osmium::Relation& rel)
{
	static constexpr bool logerror(true);
	if (!(m_osmpass & 4U) || rel.deleted())
		return;
	++m_osmstat[4];
	// check if multipolygon or multiline
	bool multipoly(is_multipolygon(rel.tags()));
	if (!(multipoly || is_multiline(rel.tags()))) {
		reportosmimport();
		return;
	}
	// check if object needs to be stored in database
	ObjArea obja;
	OsmStyleEntry::flags_t flg(setobj(obja, rel, OsmStyleEntry::entity_t::relation));
	if (flg == OsmStyleEntry::flags_t::none) {
		reportosmimport();
		return;
	}
	// look up ways, resolve way references
	mapways();
	typedef std::vector<OsmPolygon> polygons_t;
	polygons_t polygons[2];
	{
		bool err(false);
		std::vector<Object::id_t> nonwaymember, noninnerouterrolemember, unlinkedwaymember, unresolvedwaymember;
		for (const auto& mem : rel.members()) {
			if (mem.full_member())
				throw std::runtime_error("Relation with full member");
			switch (mem.type()) {
			case osmium::item_type::way:
				{
					bool inner(multipoly && !strcmp(mem.role(), "inner"));
					if (!multipoly && !inner && strcmp(mem.role(), "outer")) {
						if (logerror)
							noninnerouterrolemember.push_back(mem.ref());
						continue;
					}
					OsmPolygon p;
					{
						const WayTableEntry *wb(m_waytbl.begin_as<WayTableEntry>());
						const WayTableEntry *we(m_waytbl.end_as<WayTableEntry>());
						const WayTableEntry *wp(std::lower_bound(wb, we, mem.ref(), FileIDSorter()));
						if (wp == we || wp->get_id() != mem.ref()) {
							if (logerror)
								unlinkedwaymember.push_back(mem.ref());
							err = true;
							break;
						}
						uint8_t buf[wp->get_datasize()];
						if (pread(m_waydatafd, buf, wp->get_datasize(), wp->get_dataoffs()) != wp->get_datasize())
							MapFile::throw_strerror("Way data read error");
						HibernateReadBuffer ar(buf, buf + wp->get_datasize());
						p.hibernate(ar);
					}
					if (!p.is_resolved()) {
						if (logerror)
							unresolvedwaymember.push_back(mem.ref());
						err = true;
						break;
					}
					if (p.get_nodes().size() >= 2)
						polygons[inner].push_back(p);
					break;
				}

			case osmium::item_type::node:
			case osmium::item_type::relation:
				if (!multipoly)
					break;
				if (logerror)
					nonwaymember.push_back(mem.ref());
				break;

			default:
				throw std::runtime_error("Invalid relation member");
			}
			if (err)
				break;
		}
		if (err) {
			if (logerror && (!nonwaymember.empty() || !noninnerouterrolemember.empty() ||
					 !unlinkedwaymember.empty() || !unresolvedwaymember.empty())) {
				std::sort(nonwaymember.begin(), nonwaymember.end());
				std::sort(noninnerouterrolemember.begin(), noninnerouterrolemember.end());
				std::sort(unlinkedwaymember.begin(), unlinkedwaymember.end());
				std::sort(unresolvedwaymember.begin(), unresolvedwaymember.end());
				std::cerr << (err ? "Error" : "Warning") << ": relation " << rel.id() << std::endl;
				if (!nonwaymember.empty()) {
					std::cerr << "  Non way members";
					for (auto id : nonwaymember)
						std::cerr << ' ' << id;
					std::cerr << std::endl;
				}
				if (!noninnerouterrolemember.empty()) {
					std::cerr << "  Non outer/inner role members";
					for (auto id : noninnerouterrolemember)
						std::cerr << ' ' << id;
					std::cerr << std::endl;
				}
				if (!unlinkedwaymember.empty()) {
					std::cerr << "  Unlinked way members";
					for (auto id : unlinkedwaymember)
						std::cerr << ' ' << id;
					std::cerr << std::endl;
				}
				if (!unresolvedwaymember.empty()) {
					std::cerr << "  Unresolved way members";
					for (auto id : unresolvedwaymember)
						std::cerr << ' ' << id;
					std::cerr << std::endl;
				}
			}
			reportosmimport();
			return;
		}
	}
	// resolve multipolygon
	if (multipoly) {
		MultiPolygonHole mphouter, mphinner;
		if (!resolve_polygons(mphouter, polygons[0])) {
			if (logerror)
				std::cerr << "Error: relation " << rel.id() << " has unclosable outer boundary" << std::endl;
			reportosmimport();
			return;
		}
		if (!resolve_polygons(mphinner, polygons[1])) {
			if (logerror)
				std::cerr << "Error: relation " << rel.id() << " has unclosable inner boundary" << std::endl;
			reportosmimport();
			return;
		}
		mphouter.geos_subtract(mphinner);
		if (mphouter.empty()) {
			reportosmimport();
			return;
		}
		obja.set_area(mphouter);
		// save object to database
		save_object(obja);
		++m_osmstat[5];	
		reportosmimport();
		return;
	}
	polygons_t xpolygons;
	polygons_t xpolylines;
	for (const auto& p : polygons[0]) {
		if (p.get_nodes().size() < 2)
			continue;
		// if line is closed, add it to polygons
		if (p.is_closed()) {
			xpolygons.push_back(p);
			continue;
		}
		// try to add line to existing line
		{
			polygons_t::iterator i(xpolylines.begin()), e(xpolylines.end());
			for (; i != e; ++i)
				if (i->joinline(p))
					break;
			if (i != e) {
				if (!i->is_closed())
					continue;
				xpolygons.push_back(*i);
				xpolylines.erase(i);
				continue;
			}
		}
		xpolylines.push_back(p);
	}
	// store in MultiPolygonHole
	bool xset(false);
	if ((flg & OsmStyleEntry::flags_t::polygon) != OsmStyleEntry::flags_t::none) {
		MultiPolygonHole mph;
		for (polygons_t::const_iterator i(xpolygons.begin()), e(xpolygons.end()); i != e; ++i) {
			PolygonSimple ps(i->get_poly());
			ps.make_counterclockwise();
			mph.push_back(PolygonHole(ps));
		}
		if (!mph.empty()) {
			obja.set_area(mph);
			// save object to database
			save_object(obja);
			xset = true;
		}
	} else {
		for (polygons_t::const_iterator i(xpolygons.begin()), e(xpolygons.end()); i != e; ++i) {
			if (i->get_poly().empty())
				continue;
			ObjRoad obj(obja.get_id(), obja.get_tags(), obja.get_zorder());
			obj.set_line(i->get_poly());
			if ((flg & seflag_road) != OsmStyleEntry::flags_t::none)
				save_object(obj);
			else
				save_object<ObjLine>(obj);
			xset = true;
		}	
	}
	for (polygons_t::const_iterator i(xpolylines.begin()), e(xpolylines.end()); i != e; ++i) {
		if (i->get_poly().empty())
			continue;
		ObjRoad obj(obja.get_id(), obja.get_tags(), obja.get_zorder());
		obj.set_line(i->get_poly());
		// save object to database
		if ((flg & seflag_road) != OsmStyleEntry::flags_t::none)
			save_object(obj);
		else
			save_object<ObjLine>(obj);
		xset = true;
	}
	m_osmstat[5] += xset;	
	reportosmimport();
}

uint32_t OSMDB::Import::copy(Object::type_t objtyp, int fd, uint64_t addr, uint64_t source, uint32_t len) const
{
	Object::ptr_t p(Object::create(objtyp));
	if (!p)
		return 0;
	{
		uint8_t buf[len];
		if (pread(m_datafd, buf, len, source) != len)
			MapFile::throw_strerror(to_str(objtyp) + " object data read error");
		HibernateReadBuffer ar(buf, buf + len);
		p->loaddb(ar);
	}
	{
		Object::tags_t& tags(p->get_tags());
		for (Object::tags_t::iterator i(tags.begin()), e(tags.end()); i != e; ) {
			auto tk(static_cast<std::underlying_type<Object::tagkey_t>::type>(std::get<Object::tagkey_t>(*i)));
			if (tk >= m_tagkeyremap.size()) {
				std::cerr << "tag key " << tk << " outside of remap table " << m_tagkeyremap.size() << std::endl;
				i = tags.erase(i);
				e = tags.end();
				continue;
			}
			auto tn(static_cast<std::underlying_type<Object::tagname_t>::type>(std::get<Object::tagname_t>(*i)));
			if (tn >= m_tagnameremap.size()) {
				std::cerr << "tag name " << tn << " outside of remap table " << m_tagnameremap.size() << std::endl;
				i = tags.erase(i);
				e = tags.end();
				continue;
			}
			Object::tagkey_t tkm(m_tagkeyremap[tk]);
			if (tkm == Object::tagkey_t::invalid) {
				std::cerr << "tag key " << tk << " maps to invalid" << std::endl;
				i = tags.erase(i);
				e = tags.end();
				continue;
			}
			Object::tagname_t tnm(m_tagnameremap[tn]);
			if (tnm == Object::tagname_t::invalid) {
				std::cerr << "tag name " << tn << " maps to invalid" << std::endl;
				i = tags.erase(i);
				e = tags.end();
				continue;
			}
			std::get<Object::tagkey_t>(*i) = tkm;
			std::get<Object::tagname_t>(*i) = tnm;
			++i;
		}
		// sort
		Object::sort_tags(tags);
		// check for duplicate
		Object::tagkey_t lastkey(Object::tagkey_t::invalid);
		for (Object::tags_t::iterator i(tags.begin()), e(tags.end()); i != e; ) {
			Object::tagkey_t key(std::get<Object::tagkey_t>(*i));
			if (key != lastkey) {
				lastkey = key;
				++i;
				continue;
			}
			std::cerr << "duplicate key " << static_cast<std::underlying_type<Object::tagkey_t>::type>(key) << std::endl;
			i = tags.erase(i);
			e = tags.end();
		}
		if (false)
			std::cerr << "Object has " << tags.size() << " tags" << std::endl;
	}
	std::ostringstream blob;
	{
		HibernateWriteStream ar(blob);
		p->savedb(ar);
	}
	p.reset();
	if (pwrite(fd, blob.str().c_str(), blob.str().size(), addr) != static_cast<ssize_t>(blob.str().size()))
		MapFile::throw_strerror(to_str(objtyp) + " object data write error");
	return blob.str().size();
}

void OSMDB::Import::osmimport(int outfd, const std::string& infname)
{
	static constexpr bool dolog = true;
	static constexpr uint64_t copychunksize = 1ULL << 30;
	clear();
	if (lseek(outfd, 0, SEEK_SET) == static_cast<off_t>(-1))
		MapFile::throw_strerror("seeking output file");
	if (ftruncate(outfd, 0))
		MapFile::throw_strerror("truncating output file");
	// read OSM objects
	for (m_osmpass = ((m_flags & importflags_t::singlepass) != importflags_t::none) ? 7U : 1U; !(m_osmpass & ~7U); m_osmpass <<= 1) {
		osmium::osm_entity_bits::type otypes(osmium::osm_entity_bits::nothing);
		if (m_osmpass & 1U)
			otypes |= osmium::osm_entity_bits::node;
		if (m_osmpass & 2U)
			otypes |= osmium::osm_entity_bits::way;
		if (m_osmpass & 4U)
			otypes |= osmium::osm_entity_bits::relation;
		osmium::io::Reader reader{infname, otypes};
		osmium::apply(reader, *this);
		reader.close();
		reportosmimport(true);
	}
	// save some memory
	m_nodetbl.close();
	m_waytbl.close();
	if ((m_flags & importflags_t::keep) == importflags_t::none) {
		ftruncate(m_nodetblfd, 0);
		ftruncate(m_waytblfd, 0);
		ftruncate(m_waydatafd, 0);
	}
	// put together the output file
	uint64_t addr(sizeof(m_hdr));
	if (dolog)
		std::cerr << "Writing tag keys..." << std::endl;
	m_hdr.set_tagkeyentries(m_tagkeys.alltags().size());
	m_hdr.set_tagkeyoffs(addr);
	{
		uint64_t taddr(addr);
		addr += m_hdr.get_tagkeyentries() * sizeof(BinFileTagKeyEntry);
		for (const auto& n : m_tagkeys.alltags()) {
			BinFileTagKeyEntry te;
			te.set_stroffs(addr - taddr);
			ssize_t sz(n.first.size() + 1);
			if (pwrite(outfd, n.first.c_str(), sz, addr) != sz)
				MapFile::throw_strerror("Tag Key Write error");
			addr += sz;
			if (pwrite(outfd, &te, sizeof(te), taddr) != sizeof(te))
				MapFile::throw_strerror("Tag Key Entry Write error");
			taddr += sizeof(te);
		}
		addr += aligninc;
		addr &= alignmask;
		if (dolog)
			std::cerr << "Tag key table size " << (addr - m_hdr.get_tagkeyoffs())
				  << " bytes" << std::endl;
	}
	if (dolog)
		std::cerr << "Writing tag names..." << std::endl;
	m_hdr.set_tagnameentries(m_tagnames.alltags().size());
	m_hdr.set_tagnameoffs(addr);
	{
		uint64_t taddr(addr);
		addr += m_hdr.get_tagnameentries() * sizeof(BinFileTagNameEntry);
		for (const auto& n : m_tagnames.alltags()) {
			BinFileTagNameEntry te;
			te.set_stroffs(addr - taddr);
			ssize_t sz(n.first.size() + 1);
			if (pwrite(outfd, n.first.c_str(), sz, addr) != sz)
				MapFile::throw_strerror("Tag Name Write error");
			addr += sz;
			if (pwrite(outfd, &te, sizeof(te), taddr) != sizeof(te))
				MapFile::throw_strerror("Tag Name Entry Write error");
			taddr += sizeof(te);
		}
		addr += aligninc;
		addr &= alignmask;
		if (dolog)
			std::cerr << "Tag name table size " << (addr - m_hdr.get_tagnameoffs())
				  << " bytes" << std::endl;
	}
	// save more memory
	m_tagkeys.compute_remap(m_tagkeyremap);
	m_tagnames.compute_remap(m_tagnameremap);
	m_tagkeys.clear();
	m_tagnames.clear();
	// update header
	for (Object::type_t t(Object::type_t::point); t <= Object::type_t::area; t = static_cast<Object::type_t>(static_cast<unsigned int>(t) + 1)) {
		std::size_t entsz(t == Object::type_t::point ? sizeof(BinFilePointEntry) : sizeof(BinFileLineAreaEntry));
		m_hdr.set_objdiroffs(t, addr);
		addr += m_hdr.get_objdirentries(t) * entsz;
		addr += aligninc;
		addr &= alignmask;
		m_hdr.set_rtreeoffs(t, 0);
	}
	// write header
	if (pwrite(outfd, &m_hdr, sizeof(m_hdr), 0) != sizeof(m_hdr))
		MapFile::throw_strerror("Header write error");
	// point objects
	{
		MapFile mmap(m_objfd[0], true, "point directory");
		BinFilePointEntry *dirb(mmap.begin_as<BinFilePointEntry>());
		BinFilePointEntry *dire(mmap.end_as<BinFilePointEntry>());
		// sorting
		if (dolog)
			std::cerr << "Sorting " << Object::type_t::point << "s..." << std::endl;
		std::sort(dirb, dire, BinFileIDSorter());
		// copy object data and fix up addresses
		if (dolog)
			std::cerr << "Copying " << Object::type_t::point << " objects..." << std::endl;
		for (BinFilePointEntry *x(dirb); x != dire; ++x) {
			uint32_t sz(copy(Object::type_t::point, outfd, addr, x->get_dataoffs(), x->get_datasize()));
			x->set_dataoffs(addr - m_hdr.get_objdiroffs(Object::type_t::point) - (x - dirb) * sizeof(BinFilePointEntry));
			x->set_datasize(sz);
			addr += sz;
		}
		// copy object directories
		{
			uint64_t sz(m_hdr.get_objdirentries(Object::type_t::point) * sizeof(BinFilePointEntry));
			uint64_t addr(m_hdr.get_objdiroffs(Object::type_t::point));
			const uint8_t *sptr(mmap.begin());
			std::cerr << "Writing " << Object::type_t::point << " table";
			while (sz > 0) {
				ssize_t sz1(std::min(sz, copychunksize));
				ssize_t ret(pwrite(outfd, sptr, sz1, addr));
				if (ret != sz1)
					MapFile::throw_strerror(to_str(Object::type_t::point) + " table write error");
				addr += sz1;
				sptr += sz1;
				sz -= sz1;
				std::cerr << '.';
			}
			std::cerr << std::endl;
		}
		// save memory
		mmap.close();
		if ((m_flags & importflags_t::keep) == importflags_t::none)
			ftruncate(m_objfd[0], 0);
	}
	// other objects
	for (Object::type_t t(Object::type_t::line); t <= Object::type_t::area; t = static_cast<Object::type_t>(static_cast<unsigned int>(t) + 1)) {
		MapFile mmap(m_objfd[static_cast<unsigned int>(t)], true, "object directory");
		BinFileLineAreaEntry *dirb(mmap.begin_as<BinFileLineAreaEntry>());
		BinFileLineAreaEntry *dire(mmap.end_as<BinFileLineAreaEntry>());
		// sorting
		if (dolog)
			std::cerr << "Sorting " << t << "s..." << std::endl;
		std::sort(dirb, dire, BinFileIDSorter());
		// copy object data and fix up addresses
		if (dolog)
			std::cerr << "Copying " << t << " objects..." << std::endl;
		for (BinFileLineAreaEntry *x(dirb); x != dire; ++x) {
			uint32_t sz(copy(t, outfd, addr, x->get_dataoffs(), x->get_datasize()));
			x->set_dataoffs(addr - m_hdr.get_objdiroffs(t) - (x - dirb) * sizeof(BinFileLineAreaEntry));
			x->set_datasize(sz);
			addr += sz;
		}
		// copy object directories
		{
			uint64_t sz(m_hdr.get_objdirentries(t) * sizeof(BinFileLineAreaEntry));
			uint64_t addr(m_hdr.get_objdiroffs(t));
			const uint8_t *sptr(mmap.begin());
			std::cerr << "Writing " << t << " table";
			while (sz > 0) {
				ssize_t sz1(std::min(sz, copychunksize));
				ssize_t ret(pwrite(outfd, sptr, sz1, addr));
				if (ret != sz1)
					MapFile::throw_strerror(to_str(t) + " table write error");
				addr += sz1;
				sptr += sz1;
				sz -= sz1;
				std::cerr << '.';
			}
			std::cerr << std::endl;
		}
		// save memory
		mmap.close();
		if ((m_flags & importflags_t::keep) == importflags_t::none)
			ftruncate(m_objfd[static_cast<unsigned int>(t)], 0);
	}
	// delete temporaries
	if ((m_flags & importflags_t::keep) == importflags_t::none)
		clear();
	else
		clear_nofiles();
	if ((m_flags & importflags_t::noindex) == importflags_t::none)
		reindex(m_tmpdir, outfd);
	// compression statistics
	if (compressionstatistics)
		std::cerr << "Compression Statistics: " << m_comprsave
			  << " bytes could be saved with deflate(best)" << std::endl;
}

void OSMDB::Import::osmimport(const std::string& outfname, const std::string& infname)
{
	int outfd(::open(outfname.c_str(), O_CREAT | O_TRUNC | O_CLOEXEC | O_NOATIME | O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP));
	if (outfd == -1)
		MapFile::throw_strerror(("Cannot create file " + outfname).c_str());
	try {
		osmimport(outfd, infname);
	} catch (...) {
		::close(outfd);
		throw;
	}
	::close(outfd);
}

void OSMDB::import(const OsmStyle& style, const std::string& tmpdir, const std::string& outfname, const std::string& infname, importflags_t flags)
{
	Import imp(style, tmpdir, flags);
	imp.osmimport(outfname, infname);
}

void OSMDB::import(const OsmStyle& style, const std::string& tmpdir, int outfd, const std::string& infname, importflags_t flags)
{
	Import imp(style, tmpdir, flags);
	imp.osmimport(outfd, infname);
}

#else /* HAVE_OSMIUM */

void OSMDB::import(const OsmStyle& style, const std::string& tmpdir, const std::string& outfname, const std::string& infname, importflags_t flags)
{
	throw std::runtime_error("OSM import must be compiled with osmium");
}

void OSMDB::import(const OsmStyle& style, const std::string& tmpdir, int outfd, const std::string& infname, importflags_t flags)
{
	throw std::runtime_error("OSM import must be compiled with osmium");
}

#endif /* HAVE_OSMIUM */
