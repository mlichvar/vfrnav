//
// C++ Implementation: cartosql
//
// Description: CartoSQL
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cmath>

#include "geom.h"
#include "cartosql.h"

CartoSQL::Value::ValueError::ValueError(const std::string& x)
	: std::runtime_error(x)
{
}

const std::string& to_str(CartoSQL::Value::type_t t)
{
	switch (t) {
	case CartoSQL::Value::type_t::null:
	{
		static const std::string r("null");
		return r;
	}

	case CartoSQL::Value::type_t::string:
	{
		static const std::string r("string");
		return r;
	}

	case CartoSQL::Value::type_t::floatingpoint:
	{
		static const std::string r("floatingpoint");
		return r;
	}

	case CartoSQL::Value::type_t::integer:
	{
		static const std::string r("integer");
		return r;
	}

	case CartoSQL::Value::type_t::boolean:
	{
		static const std::string r("boolean");
		return r;
	}

	case CartoSQL::Value::type_t::array:
	{
		static const std::string r("array");
		return r;
	}

	case CartoSQL::Value::type_t::tags:
	{
		static const std::string r("tags");
		return r;
	}

	case CartoSQL::Value::type_t::points:
	{
		static const std::string r("points");
		return r;
	}

	case CartoSQL::Value::type_t::lines:
	{
		static const std::string r("lines");
		return r;
	}

	case CartoSQL::Value::type_t::polygons:
	{
		static const std::string r("polygons");
		return r;
	}

	case CartoSQL::Value::type_t::rectangle:
	{
		static const std::string r("rectange");
		return r;
	}

	case CartoSQL::Value::type_t::osmdbobject:
	{
		static const std::string r("osmdbobject");
		return r;
	}

	default:
	{
		static const std::string r("?");
		return r;
	}
	}
}

CartoSQL::Value::Value(void)
	: m_type(type_t::null)
{
}

CartoSQL::Value::Value(const std::string& val)
	: m_type(type_t::string)
{
	::new(&m_string) string_t(val);
}

CartoSQL::Value::Value(const char *val)
	: m_type(type_t::null)
{
	if (!val)
		return;
	m_type = type_t::string;
	::new(&m_string) string_t(val);
}

CartoSQL::Value::Value(double val)
	: m_double(val), m_type(type_t::floatingpoint)
{
}

CartoSQL::Value::Value(int64_t val)
	: m_int(val), m_type(type_t::integer)
{
}

CartoSQL::Value::Value(bool val)
	: m_bool(val), m_type(type_t::boolean)
{
}

CartoSQL::Value::Value(const array_t& v)
	: m_type(type_t::null)
{
	operator=(v);
}

CartoSQL::Value::Value(array_t&& v)
	: m_type(type_t::null)
{
	operator=(v);
}

CartoSQL::Value::Value(const tags_t& v)
	: m_type(type_t::null)
{
	operator=(v);
}

CartoSQL::Value::Value(tags_t&& v)
	: m_type(type_t::null)
{
	operator=(v);
}

CartoSQL::Value::Value(const MultiPoint& v)
	: m_type(type_t::null)
{
	operator=(v);
}

CartoSQL::Value::Value(MultiPoint&& v)
	: m_type(type_t::null)
{
	operator=(v);
}

CartoSQL::Value::Value(const MultiLineString& v)
	: m_type(type_t::null)
{
	operator=(v);
}

CartoSQL::Value::Value(MultiLineString&& v)
	: m_type(type_t::null)
{
	operator=(v);
}

CartoSQL::Value::Value(const MultiPolygonHole& v)
	: m_type(type_t::null)
{
	operator=(v);
}

CartoSQL::Value::Value(MultiPolygonHole&& v)
	: m_type(type_t::null)
{
	operator=(v);
}

CartoSQL::Value::Value(const Rect& v)
	: m_type(type_t::null)
{
	operator=(v);
}

CartoSQL::Value::Value(const OSMDB::Object::const_ptr_t& v)
	: m_type(type_t::null)
{
	operator=(v);
}

CartoSQL::Value::Value(OSMDB::Object::const_ptr_t&& v)
	: m_type(type_t::null)
{
	operator=(v);
}

CartoSQL::Value::Value(const Value& v)
	: m_type(type_t::null)
{
	operator=(v);
}

CartoSQL::Value::Value(Value&& v)
	: m_type(type_t::null)
{
	operator=(v);
}

CartoSQL::Value::~Value()
{
	set_null();
}

void CartoSQL::Value::set_null(void)
{
	switch (m_type) {
	case type_t::string:
		m_string.~string_t();
		break;

	case type_t::array:
		m_array.~array_t();
		break;

	case type_t::tags:
		m_tags.~tags_t();
		break;

	case type_t::points:
		m_points.~MultiPoint();
		break;

	case type_t::lines:
		m_lines.~MultiLineString();
		break;

	case type_t::polygons:
		m_polygons.~MultiPolygonHole();
		break;

	case type_t::rectangle:
		m_rectangle.~Rect();
		break;

	case type_t::osmdbobject:
		m_osmdbobject.~osmdbobjptr_t();
		break;

	default:
		break;
	}
	m_type = type_t::null;
}

CartoSQL::Value& CartoSQL::Value::operator=(const std::string& v)
{
	set_null();
	m_type = type_t::string;
	::new(&m_string) string_t(v);
	return *this;
}

CartoSQL::Value& CartoSQL::Value::operator=(const char *v)
{
	set_null();
	if (!v)
		return *this;
	m_type = type_t::string;
	::new(&m_string) string_t(v);
	return *this;
}

CartoSQL::Value& CartoSQL::Value::operator=(double v)
{
	set_null();
	m_type = type_t::floatingpoint;
	m_double = v;
	return *this;
}

CartoSQL::Value& CartoSQL::Value::operator=(int64_t v)
{
	set_null();
	m_type = type_t::integer;
	m_int = v;
	return *this;
}

CartoSQL::Value& CartoSQL::Value::operator=(bool v)
{
	set_null();
	m_type = type_t::boolean;
	m_bool = v;
	return *this;
}

CartoSQL::Value& CartoSQL::Value::operator=(const array_t& v)
{
	set_null();
	m_type = type_t::array;
	::new(&m_array) array_t(v);
	return *this;
}

CartoSQL::Value& CartoSQL::Value::operator=(array_t&& v)
{
	set_null();
	m_type = type_t::array;
	::new(&m_array) array_t();
	m_array.swap(v);
	return *this;
}

CartoSQL::Value& CartoSQL::Value::operator=(const tags_t& v)
{
	set_null();
	m_type = type_t::tags;
	::new(&m_tags) tags_t(v);
	return *this;
}

CartoSQL::Value& CartoSQL::Value::operator=(tags_t&& v)
{
	set_null();
	m_type = type_t::tags;
	::new(&m_tags) tags_t();
	m_tags.swap(v);
	return *this;
}

CartoSQL::Value& CartoSQL::Value::operator=(const MultiPoint& v)
{
	set_null();
	m_type = type_t::points;
	::new(&m_points) MultiPoint(v);
	return *this;
}

CartoSQL::Value& CartoSQL::Value::operator=(MultiPoint&& v)
{
	set_null();
	m_type = type_t::points;
	::new(&m_points) MultiPoint();
	m_points.swap(v);
	return *this;
}

CartoSQL::Value& CartoSQL::Value::operator=(const MultiLineString& v)
{
	set_null();
	m_type = type_t::lines;
	::new(&m_lines) MultiLineString(v);
	return *this;
}

CartoSQL::Value& CartoSQL::Value::operator=(MultiLineString&& v)
{
	set_null();
	m_type = type_t::lines;
	::new(&m_lines) MultiLineString();
	m_lines.swap(v);
	return *this;
}

CartoSQL::Value& CartoSQL::Value::operator=(const MultiPolygonHole& v)
{
	set_null();
	m_type = type_t::polygons;
	::new(&m_polygons) MultiPolygonHole(v);
	return *this;
}

CartoSQL::Value& CartoSQL::Value::operator=(MultiPolygonHole&& v)
{
	set_null();
	m_type = type_t::polygons;
	::new(&m_polygons) MultiPolygonHole();
	m_polygons.swap(v);
	return *this;
}

CartoSQL::Value& CartoSQL::Value::operator=(const Rect& v)
{
	set_null();
	m_type = type_t::rectangle;
	::new(&m_rectangle) Rect(v);
	return *this;
}

CartoSQL::Value& CartoSQL::Value::operator=(const OSMDB::Object::const_ptr_t& v)
{
	set_null();
	if (!v)
		return *this;
	m_type = type_t::osmdbobject;
	::new(&m_osmdbobject) osmdbobjptr_t(v);
	return *this;
}

CartoSQL::Value& CartoSQL::Value::operator=(OSMDB::Object::const_ptr_t&& v)
{
	set_null();
	if (!v)
		return *this;
	m_type = type_t::osmdbobject;
	::new(&m_osmdbobject) osmdbobjptr_t();
	m_osmdbobject.swap(v);
	return *this;
}

CartoSQL::Value& CartoSQL::Value::operator=(const Value& v)
{
	set_null();
	m_type = v.m_type;
	switch (m_type) {
	case type_t::string:
		::new(&m_string) string_t(v.m_string);
		break;

	case type_t::floatingpoint:
		m_double = v.m_double;
		break;

	case type_t::integer:
		m_int = v.m_int;
		break;

	case type_t::boolean:
		m_bool = v.m_bool;
		break;

	case type_t::array:
		::new(&m_array) array_t(v.m_array);
		break;

	case type_t::tags:
		::new(&m_tags) tags_t(v.m_tags);
		break;

	case type_t::points:
		::new(&m_points) MultiPoint(v.m_points);
		break;

	case type_t::lines:
		::new(&m_lines) MultiLineString(v.m_lines);
		break;

	case type_t::polygons:
		::new(&m_polygons) MultiPolygonHole(v.m_polygons);
		break;

	case type_t::rectangle:
		::new(&m_rectangle) Rect(v.m_rectangle);
		break;

	case type_t::osmdbobject:
		::new(&m_osmdbobject) osmdbobjptr_t(v.m_osmdbobject);
		break;

	default:
		break;
	}
	return *this;
}

CartoSQL::Value& CartoSQL::Value::operator=(Value&& v)
{
	set_null();
	m_type = v.m_type;
	switch (m_type) {
	case type_t::string:
		::new(&m_string) string_t();
		m_string.swap(v.m_string);
		break;

	case type_t::floatingpoint:
		m_double = v.m_double;
		break;

	case type_t::integer:
		m_int = v.m_int;
		break;

	case type_t::boolean:
		m_bool = v.m_bool;
		break;

	case type_t::array:
		::new(&m_array) array_t();
		m_array.swap(v.m_array);
		break;

	case type_t::tags:
		::new(&m_tags) tags_t();
		m_tags.swap(v.m_tags);
		break;

	case type_t::points:
		::new(&m_points) MultiPoint();
		m_points.swap(v.m_points);
		break;

	case type_t::lines:
		::new(&m_lines) MultiLineString();
		m_lines.swap(v.m_lines);
		break;

	case type_t::polygons:
		::new(&m_polygons) MultiPolygonHole();
		m_polygons.swap(v.m_polygons);
		break;

	case type_t::rectangle:
		::new(&m_rectangle) Rect(v.m_rectangle);
		break;

	case type_t::osmdbobject:
		::new(&m_osmdbobject) osmdbobjptr_t();
		m_osmdbobject.swap(v.m_osmdbobject);
		break;

	default:
		break;
	}
	return *this;
}

int CartoSQL::Value::compare(const Value& x) const
{
	if (m_type < x.m_type)
		return -1;
	if (x.m_type < m_type)
		return 1;
	switch (m_type) {
	case type_t::string:
		return m_string.compare(x.m_string);

	case type_t::floatingpoint:
		if (m_double < x.m_double)
			return -1;
		if (x.m_double < m_double)
			return 1;
		return 0;

	case type_t::integer:
		if (m_int < x.m_int)
			return -1;
		if (x.m_int < m_int)
			return 1;
		return 0;

	case type_t::boolean:
		if (m_bool < x.m_bool)
			return -1;
		if (x.m_bool < m_bool)
			return 1;
		return 0;

	case type_t::array:
	{
		array_t::size_type i(x.m_array.size()), n(m_array.size());
		if (n < i)
			return -1;
		if (i < n)
			return 1;
		for (i = 0; i < n; ++i) {
			int c(m_array[i].compare(x.m_array[i]));
			if (c)
				return c;
		}
		return 0;
	}

	case type_t::tags:
	{
		for (tags_t::const_iterator i0(m_tags.begin()), e0(m_tags.end()), i1(x.m_tags.begin()), e1(x.m_tags.end());; ++i0, ++i1) {
			if (i0 == e0)
				return (i1 == e1) ? 0 : -1;
			if (i1 == e1)
				return 1;
			int c(i0->first.compare(i1->first));
			if (c)
				return c;
			c = i0->second.compare(i1->second);
			if (c)
				return c;
		}
		return 0;
	}

	case type_t::points:
		return m_points.compare(x.m_points);

	case type_t::lines:
		return m_lines.compare(x.m_lines);

	case type_t::polygons:
		return m_polygons.compare(x.m_polygons);

	case type_t::rectangle:
		return m_rectangle.compare(x.m_rectangle);

	case type_t::osmdbobject:
		return m_osmdbobject->compare(*x.m_osmdbobject);

	default:
		return 0;
	}
}

int CartoSQL::Value::compare_value(const Value& x) const
{
	// null comes first
	if (m_type == type_t::null)
		return (x.m_type == type_t::null) ? 0 : -1;
	if (x.m_type == type_t::null)
		return 1;
	if (m_type == x.m_type) {
		switch (m_type) {
		case type_t::null:
			return 0;

		case type_t::string:
			return m_string.compare(x.m_string);

		case type_t::floatingpoint:
			if (m_double < x.m_double)
				return -1;
			if (x.m_double < m_double)
				return 1;
			return 0;

		case type_t::integer:
			if (m_int < x.m_int)
				return -1;
			if (x.m_int < m_int)
				return 1;
			return 0;

		case type_t::boolean:
			if (m_bool < x.m_bool)
				return -1;
			if (x.m_bool < m_bool)
				return 1;
			return 0;

		case type_t::array:
		{
			array_t::size_type i(x.m_array.size()), n(m_array.size());
			if (n < i)
				return -1;
			if (i < n)
				return 1;
			for (i = 0; i < n; ++i) {
				int c(m_array[i].compare_value(x.m_array[i]));
				if (c)
					return c;
			}
			return 0;
		}

		case type_t::tags:
		{
			for (tags_t::const_iterator i0(m_tags.begin()), e0(m_tags.end()), i1(x.m_tags.begin()), e1(x.m_tags.end());; ++i0, ++i1) {
				if (i0 == e0)
					return (i1 == e1) ? 0 : -1;
				if (i1 == e1)
					return 1;
				int c(i0->first.compare(i1->first));
				if (c)
					return c;
				c = i0->second.compare_value(i1->second);
				if (c)
					return c;
			}
			return 0;
		}

		case type_t::points:
			return m_points.compare(x.m_points);

		case type_t::lines:
			return m_lines.compare(x.m_lines);

		case type_t::polygons:
			return m_polygons.compare(x.m_polygons);

		case type_t::rectangle:
			return m_rectangle.compare(x.m_rectangle);

		case type_t::osmdbobject:
			return m_osmdbobject->compare(*x.m_osmdbobject);
		}
	}
	if (is_integral() && x.is_integral()) {
		int64_t v0(*this), v1(x);
		if (v0 < v1)
			return -1;
		if (v1 < v0)
			return 1;
		return 0;
	}
	if (is_numeric() && x.is_numeric()) {
		double v0(*this), v1(x);
		if (v0 < v1)
			return -1;
		if (v1 < v0)
			return 1;
		return 0;
	}
	std::string v0(*this), v1(x);
	if (v0 < v1)
		return -1;
	if (v1 < v0)
		return 1;
	return 0;
}

bool CartoSQL::Value::is_integral(void) const
{
	switch (m_type) {
	default:
		return false;

	case type_t::integer:
	case type_t::boolean:
		return true;
	}
}

bool CartoSQL::Value::is_numeric(void) const
{
	switch (m_type) {
	default:
		return false;

	case type_t::floatingpoint:
	case type_t::integer:
	case type_t::boolean:
		return true;
	}
}

bool CartoSQL::Value::is_geometry(void) const
{
	switch (m_type) {
	default:
		return false;

	case type_t::points:
	case type_t::lines:
	case type_t::polygons:
	case type_t::rectangle:
	case type_t::osmdbobject:
		return true;
	}
}

bool CartoSQL::Value::is_geom_point(void) const
{
	switch (m_type) {
	default:
		return false;

	case type_t::osmdbobject:
		switch (m_osmdbobject->get_type()) {
		default:
			return false;

		case OSMDB::Object::type_t::point:
			return true;
		}
	}
}

bool CartoSQL::Value::is_geom_points(void) const
{
	switch (m_type) {
	default:
		return false;

	case type_t::points:
		return true;
	}
}

bool CartoSQL::Value::is_geom_line(void) const
{
	switch (m_type) {
	default:
		return false;

	case type_t::osmdbobject:
		switch (m_osmdbobject->get_type()) {
		default:
			return false;

		case OSMDB::Object::type_t::line:
		case OSMDB::Object::type_t::road:
			return true;
		}
	}
}

bool CartoSQL::Value::is_geom_lines(void) const
{
	switch (m_type) {
	default:
		return false;

	case type_t::lines:
		return true;
	}
}

bool CartoSQL::Value::is_geom_polygons(void) const
{
	switch (m_type) {
	default:
		return false;

	case type_t::polygons:
		return true;

	case type_t::osmdbobject:
		switch (m_osmdbobject->get_type()) {
		default:
			return false;

		case OSMDB::Object::type_t::area:
			return true;
		}
	}
}

CartoSQL::Value::operator bool(void) const
{
	switch (m_type) {
	default:
		throw ValueError("operator bool: type is " + to_str(m_type));

	case type_t::string:
		if (m_string == "true")
			return true;
		if (m_string == "false")
			return false;
		throw ValueError("operator bool: type is string (" + m_string + ")");

	case type_t::floatingpoint:
		return !!m_double;

	case type_t::integer:
		return !!m_int;

	case type_t::boolean:
		return m_bool;
	}
}

CartoSQL::Value::operator int64_t(void) const
{
	switch (m_type) {
	default:
		throw ValueError("operator int64_t: type is " + to_str(m_type));

	case type_t::string:
	{
		char *cp(0);
		int64_t v(strtoll(m_string.c_str(), &cp, 10));
		if (*cp)
			throw ValueError("operator int64_t: type is string (" + m_string + ")");
		return v;
	}

	case type_t::floatingpoint:
		return m_double;

	case type_t::integer:
		return m_int;

	case type_t::boolean:
		return m_bool;
	}
}

CartoSQL::Value::operator double(void) const
{
	switch (m_type) {
	default:
		throw ValueError("operator double: type is " + to_str(m_type));

	case type_t::string:
	{
		char *cp(0);
		double v(strtod(m_string.c_str(), &cp));
		if (*cp)
			throw ValueError("operator double: type is string (" + m_string + ")");
		return v;
	}

	case type_t::floatingpoint:
		return m_double;

	case type_t::integer:
		return m_int;

	case type_t::boolean:
		return m_bool;

	case type_t::array:
		throw ValueError("operator double: type is array");
	}
}

CartoSQL::Value::operator std::string(void) const
{
	switch (m_type) {
	default:
		throw ValueError("operator string: type is " + to_str(m_type));

	case type_t::string:
		return m_string;

	case type_t::floatingpoint:
	{
		std::ostringstream v;
		v << m_double;
		return v.str();
	}

	case type_t::integer:
	{
		std::ostringstream v;
		v << m_int;
		return v.str();
	}

	case type_t::boolean:
		return m_bool ? "true" : "false";
	}
}

bool CartoSQL::Value::get_boolean(void) const
{
	if (m_type == type_t::boolean)
		return m_bool;
	throw ValueError("get_boolean: type is " + to_str(m_type));
}

int64_t CartoSQL::Value::get_integer(void) const
{
	if (m_type == type_t::integer)
		return m_int;
	throw ValueError("get_integer: type is " + to_str(m_type));
}

double CartoSQL::Value::get_double(void) const
{
	if (m_type == type_t::floatingpoint)
		return m_double;
	throw ValueError("get_double: type is " + to_str(m_type));
}

const std::string& CartoSQL::Value::get_string(void) const
{
	if (m_type == type_t::string)
		return m_string;
	throw ValueError("get_string: type is " + to_str(m_type));
}

const CartoSQL::Value::array_t& CartoSQL::Value::get_array(void) const
{
	if (m_type == type_t::array)
		return m_array;
	throw ValueError("get_array: type is " + to_str(m_type));
}

const CartoSQL::Value::tags_t& CartoSQL::Value::get_tags(void) const
{
	if (m_type == type_t::tags)
		return m_tags;
	throw ValueError("get_tags: type is " + to_str(m_type));
}

const Point& CartoSQL::Value::get_point(void) const
{
	if (m_type == type_t::osmdbobject) {
		switch (m_osmdbobject->get_type()) {
		case OSMDB::Object::type_t::point:
			return static_cast<const OSMDB::ObjPoint&>(*m_osmdbobject).get_location();

		default:
			break;
		}
		throw ValueError("get_point: type is " + to_str(m_type) + "/" + to_str(m_osmdbobject->get_type()));
	}
	throw ValueError("get_point: type is " + to_str(m_type));
}

const MultiPoint& CartoSQL::Value::get_points(void) const
{
	if (m_type == type_t::points)
		return m_points;
	throw ValueError("get_points: type is " + to_str(m_type));
}

const LineString& CartoSQL::Value::get_line(void) const
{
	if (m_type == type_t::osmdbobject) {
		switch (m_osmdbobject->get_type()) {
		case OSMDB::Object::type_t::line:
		case OSMDB::Object::type_t::road:
			return static_cast<const OSMDB::ObjLine&>(*m_osmdbobject).get_line();

		default:
			break;
		}
		throw ValueError("get_line: type is " + to_str(m_type) + "/" + to_str(m_osmdbobject->get_type()));
	}
	throw ValueError("get_line: type is " + to_str(m_type));
}

const MultiLineString& CartoSQL::Value::get_lines(void) const
{
	if (m_type == type_t::lines)
		return m_lines;
	throw ValueError("get_lines: type is " + to_str(m_type));
}

const MultiPolygonHole& CartoSQL::Value::get_polygons(void) const
{
	switch (m_type) {
	case type_t::polygons:
		return m_polygons;

	case type_t::osmdbobject:
		switch (m_osmdbobject->get_type()) {
		case OSMDB::Object::type_t::area:
			return static_cast<const OSMDB::ObjArea&>(*m_osmdbobject).get_area();

		default:
			break;
		}
		throw ValueError("get_polygons: type is " + to_str(m_type) + "/" + to_str(m_osmdbobject->get_type()));

	default:
		throw ValueError("get_polygons: type is " + to_str(m_type));
	}
}

const Rect& CartoSQL::Value::get_rectangle(void) const
{
	if (m_type == type_t::rectangle)
		return m_rectangle;
	throw ValueError("get_rectangle: type is " + to_str(m_type));
}

const OSMDB::Object::const_ptr_t& CartoSQL::Value::get_osmdbobject(void) const
{
	if (m_type == type_t::osmdbobject)
		return m_osmdbobject;
	throw ValueError("get_osmdbobject: type is " + to_str(m_type));
}

const OSMDB::Object::tags_t& CartoSQL::Value::get_osmdbobject_tags(void) const
{
	if (m_type == type_t::osmdbobject)
		return m_osmdbobject->get_tags();
	throw ValueError("get_osmdbobject_tags: type is " + to_str(m_type));
}

CartoSQL::Value CartoSQL::Value::as_boolean(void) const
{
	switch (m_type) {
	default:
		return Value();

	case type_t::string:
		if (m_string == "true")
			return Value(true);
		if (m_string == "false")
			return Value(false);
		return Value();

	case type_t::floatingpoint:
		if (false) {
			if (!m_double)
				return Value(false);
			if (m_double == 1)
				return Value(true);
		}
		return Value();

	case type_t::integer:
		if (false) {
			if (!m_int)
				return Value(false);
			if (m_int == 1)
				return Value(true);
		}
		return Value();

	case type_t::boolean:
		return *this;
	}
}

CartoSQL::Value CartoSQL::Value::as_integer(void) const
{
	switch (m_type) {
	default:
		return Value();

	case type_t::string:
	{
		char *cp(0);
		int64_t v(strtoll(m_string.c_str(), &cp, 10));
		if (*cp)
			return Value();
		return Value(v);
	}

	case type_t::floatingpoint:
		if (false) {
			int64_t v(m_double);
			if (v == m_double)
				return Value(v);
		}
		return Value();

	case type_t::integer:
		return *this;

	case type_t::boolean:
		if (false)
			return Value(static_cast<int64_t>(m_bool));
		return Value();
	}
}

CartoSQL::Value CartoSQL::Value::as_double(void) const
{
	switch (m_type) {
	default:
		return Value();

	case type_t::string:
	{
		char *cp(0);
		double v(strtod(m_string.c_str(), &cp));
		if (*cp)
			return Value();
		return Value(v);
	}

	case type_t::floatingpoint:
		return *this;

	case type_t::integer:
		return Value(static_cast<double>(m_int));

	case type_t::boolean:
		if (false)
			return Value(static_cast<double>(m_bool));
		return Value();
	}
}

CartoSQL::Value CartoSQL::Value::as_string(void) const
{
	switch (m_type) {
	default:
		return Value();

	case type_t::string:
		return *this;

	case type_t::floatingpoint:
	{
		std::ostringstream v;
		v << m_double;
		return Value(v.str());
	}

	case type_t::integer:
	{
		std::ostringstream v;
		v << m_int;
		return Value(v.str());
	}

	case type_t::boolean:
		return Value(m_bool ? "true" : "false");
	}
}

CartoSQL::Value CartoSQL::Value::as_type(type_t typ) const
{
	switch (typ) {
	default:
		return Value();

	case type_t::string:
		return as_string();

	case type_t::floatingpoint:
		return as_double();

	case type_t::integer:
		return as_integer();

	case type_t::boolean:
		return as_boolean();
	}
}

const std::string& CartoSQL::Value::get_sqltypename(type_t typ)
{
	switch (typ) {
	case Value::type_t::boolean:
	{
		static const std::string r("boolean");
		return r;
	}

	case Value::type_t::integer:
	{
		static const std::string r("integer");
		return r;
	}

	case Value::type_t::string:
	{
		static const std::string r("text");
		return r;
	}

	case Value::type_t::floatingpoint:
	{
		static const std::string r("real");
		return r;
	}

	case Value::type_t::array:
	{
		static const std::string r("?array?");
		return r;
	}

	case Value::type_t::tags:
	{
		static const std::string r("?tags?");
		return r;
	}

	case Value::type_t::points:
	{
		static const std::string r("?points?");
		return r;
	}

	case Value::type_t::lines:
	{
		static const std::string r("?lines?");
		return r;
	}

	case Value::type_t::polygons:
	{
		static const std::string r("?polygons?");
		return r;
	}

	case Value::type_t::rectangle:
	{
		static const std::string r("?rectangle?");
		return r;
	}

	case Value::type_t::osmdbobject:
	{
		static const std::string r("?osmdbobject?");
		return r;
	}

	default:
	{
		static const std::string r("?unsupported?");
		return r;
	}
	}
}

std::string CartoSQL::Value::get_sqlstring(sqlstringflags_t flags) const
{
	switch (m_type) {
	case Value::type_t::null:
		return "null";

	case Value::type_t::boolean:
		return m_bool ? "true" : "false";

	case Value::type_t::integer:
	{
		std::ostringstream oss;
		oss << m_int;
		return oss.str();
	}

	case Value::type_t::floatingpoint:
	{
		std::ostringstream oss;
		oss << m_double;
		return oss.str();
	}

	case Value::type_t::string:
	{
		Glib::ustring str(m_string);
		std::ostringstream oss;
		oss << "E'" << std::hex;
		bool reg(true);
		for (gunichar ch : str) {
			if (ch == '\'') {
				oss << "''";
				continue;
			}
			if (ch >= ' ' && ch < 0x7f) {
				oss << static_cast<char>(ch);
				continue;
			}
			reg = false;
			switch (ch) {
			case '\b':
				oss << "\\b";
				break;

			case '\t':
				oss << "\\t";
				break;

			case '\n':
				oss << "\\n";
				break;

			case '\f':
				oss << "\\f";
				break;

			case '\r':
				oss << "\\r";
				break;

			default:
			{
				uint32_t ch1(ch & 0xffffffff);
				int digit(8);
				char ch2('U');
				if (ch1 < 0x100) {
					ch2 = 'x';
					digit = 2;
				} else if (ch1 < 0x10000) {
					ch2 = 'u';
					digit = 4;
				}
				oss << '\\' << ch2 << std::setw(digit) << std::setfill('0') << ch1;
				break;
			}
			}
		}
		oss << '\'';
		return oss.str().substr(reg);
	}

	case type_t::array:
	{
		if ((flags & sqlstringflags_t::array) == sqlstringflags_t::none)
			return "ARRAY";
		std::string r("array[");
		bool subseq(false);
		for (const Value& v : m_array) {
			if (subseq)
				r.push_back(',');
			subseq = true;
			r.append(v.get_sqlstring());
		}
		r.push_back(']');
		return r;
	}

	case type_t::tags:
	{
		if ((flags & sqlstringflags_t::tags) == sqlstringflags_t::none)
			return "TAGS";
		std::string r;
		bool subseq(false);
		for (const auto& v : m_tags) {
			if (subseq)
				r.push_back(',');
			subseq = true;
			r.push_back('"');
			r.append(v.first);
			r.append("\"=>");
			r.append(v.second.get_sqlstring());
		}
		return r;
	}

	case Value::type_t::points:
	{
		if ((flags & sqlstringflags_t::geometry) == sqlstringflags_t::none)
			return "MULTIPOINT";
		std::ostringstream oss;
		m_points.to_wkt(oss);
		return oss.str();
	}

	case Value::type_t::lines:
	{
		if ((flags & sqlstringflags_t::geometry) == sqlstringflags_t::none)
			return "MULTILINESTRING";
		std::ostringstream oss;
		m_lines.to_wkt(oss);
		return oss.str();
	}

	case Value::type_t::polygons:
	{
		if ((flags & sqlstringflags_t::geometry) == sqlstringflags_t::none)
			return "MULTIPOLYGON";
		std::ostringstream oss;
		m_polygons.to_wkt(oss);
		return oss.str();
	}

	case Value::type_t::rectangle:
	{
		if ((flags & sqlstringflags_t::geometry) == sqlstringflags_t::none)
			return "RECTANGLE";
		std::ostringstream oss;
		oss << "RECTANGLE(";
		m_rectangle.get_southwest().to_wkt(oss);
		oss << ',';
		m_rectangle.get_northeast().to_wkt(oss);
		oss << ')';
		return oss.str();
	}

	case Value::type_t::osmdbobject:
	{
		static const std::string r("OSMDBOBJECT");
		return r;
	}

	default:
	{
		static const std::string r("?unsupported?");
		return r;
	}
	}
}
