//
// C++ Implementation: cartosqlfunc
//
// Description: CartoSQL ExprFunc implementation
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cmath>

#include <openssl/md5.h>

#include "cartosql.h"

const std::string& to_str(CartoSQL::ExprFunc::func_t t)
{
	switch (t) {
	// unary operators
	case CartoSQL::ExprFunc::func_t::unaryplus:
	{
		static const std::string r("+");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::unaryminus:
	{
		static const std::string r("-");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::bitwisenot:
	{
		static const std::string r("~");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::logicalnot:
	{
		static const std::string r("not");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::factorial:
	{
		static const std::string r("!!");
		return r;
	}

	// binary operators
	case CartoSQL::ExprFunc::func_t::equal:
	{
		static const std::string r("=");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::unequal:
	{
		static const std::string r("<>");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::lessthan:
	{
		static const std::string r("<");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::lessorequal:
	{
		static const std::string r("<=");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::greaterthan:
	{
		static const std::string r(">");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::greaterorequal:
	{
		static const std::string r(">=");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::binaryplus:
	{
		static const std::string r("+");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::binaryminus:
	{
		static const std::string r("-");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::binarymul:
	{
		static const std::string r("*");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::binarydiv:
	{
		static const std::string r("/");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::binarymod:
	{
		static const std::string r("%");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::bitwiseand:
	{
		static const std::string r("&");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::bitwiseor:
	{
		static const std::string r("|");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::bitwisexor:
	{
		static const std::string r("#");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::bitwiseshiftleft:
	{
		static const std::string r("<<");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::bitwiseshiftright:
	{
		static const std::string r(">>");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::logicaland:
	{
		static const std::string r("and");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::logicalor:
	{
		static const std::string r("or");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::stringconcat:
	{
		static const std::string r("||");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::regex:
	{
		static const std::string r("~");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::notregex:
	{
		static const std::string r("!~");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::regexnocase:
	{
		static const std::string r("~*");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::notregexnocase:
	{
		static const std::string r("!~*");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::overlap:
	{
		static const std::string r("&&");
		return r;
	}

	// postfix operators

	case CartoSQL::ExprFunc::func_t::isnull:
	{
		static const std::string r("is null");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::isnotnull:
	{
		static const std::string r("is not null");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::isfalse:
	{
		static const std::string r("is false");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::isnotfalse:
	{
		static const std::string r("is not false");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::istrue:
	{
		static const std::string r("is true");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::isnottrue:
	{
		static const std::string r("is not true");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::isunknown:
	{
		static const std::string r("is unknown");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::isnotunknown:
	{
		static const std::string r("");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::isdistinct:
	{
		static const std::string r("");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::isnotdistinct:
	{
		static const std::string r("");
		return r;
	}

	// math
	case CartoSQL::ExprFunc::func_t::abs:
	{
		static const std::string r("abs");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::cbrt:
	{
		static const std::string r("cbrt");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::ceil:
	{
		static const std::string r("ceil");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::degrees:
	{
		static const std::string r("degrees");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::exp:
	{
		static const std::string r("exp");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::floor:
	{
		static const std::string r("floor");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::ln:
	{
		static const std::string r("ln");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::log:
	{
		static const std::string r("log");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::log10:
	{
		static const std::string r("log10");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::radians:
	{
		static const std::string r("radians");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::round:
	{
		static const std::string r("round");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::sign:
	{
		static const std::string r("sign");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::sqrt:
	{
		static const std::string r("sqrt");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::trunc:
	{
		static const std::string r("trunc");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::acos:
	{
		static const std::string r("acos");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::asin:
	{
		static const std::string r("asin");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::atan:
	{
		static const std::string r("atan");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::cos:
	{
		static const std::string r("cos");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::cot:
	{
		static const std::string r("cot");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::sin:
	{
		static const std::string r("sin");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::tan:
	{
		static const std::string r("tan");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::acosd:
	{
		static const std::string r("acosd");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::asind:
	{
		static const std::string r("asind");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::atand:
	{
		static const std::string r("atand");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::cosd:
	{
		static const std::string r("cosd");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::cotd:
	{
		static const std::string r("cotd");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::sind:
	{
		static const std::string r("sind");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::tand:
	{
		static const std::string r("tand");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::sinh:
	{
		static const std::string r("sinh");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::cosh:
	{
		static const std::string r("cosh");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::tanh:
	{
		static const std::string r("tanh");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::asinh:
	{
		static const std::string r("asinh");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::acosh:
	{
		static const std::string r("acosh");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::atanh:
	{
		static const std::string r("atanh");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::div:
	{
		static const std::string r("div");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::pi:
	{
		static const std::string r("pi");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::pow:
	{
		static const std::string r("pow");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::atan2:
	{
		static const std::string r("atan2");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::atan2d:
	{
		static const std::string r("atan2d");
		return r;
	}

	// string
	case CartoSQL::ExprFunc::func_t::ascii:
	{
		static const std::string r("ascii");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::bit_length:
	{
		static const std::string r("bit_length");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::char_length:
	{
		static const std::string r("char_length");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::chr:
	{
		static const std::string r("chr");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::length:
	{
		static const std::string r("length");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::lower:
	{
		static const std::string r("lower");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::md5:
	{
		static const std::string r("md5");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::octet_length:
	{
		static const std::string r("octet_length");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::upper:
	{
		static const std::string r("upper");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::overlay:
	{
		static const std::string r("overlay");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::substring:
	{
		static const std::string r("substring");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::btrim:
	{
		static const std::string r("btrim");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::concat:
	{
		static const std::string r("concat");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::concat_ws:
	{
		static const std::string r("concat_ws");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::left:
	{
		static const std::string r("left");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::ltrim:
	{
		static const std::string r("ltrim");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::replace:
	{
		static const std::string r("replace");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::right:
	{
		static const std::string r("right");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::rtrim:
	{
		static const std::string r("rtrim");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::strpos:
	{
		static const std::string r("strpos");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::substr:
	{
		static const std::string r("substr");
		return r;
	}

	// regex
	case CartoSQL::ExprFunc::func_t::like:
	{
		static const std::string r("like");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::notlike:
	{
		static const std::string r("not like");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::ilike:
	{
		static const std::string r("ilike");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::notilike:
	{
		static const std::string r("not ilike");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::similar:
	{
		static const std::string r("similar to");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::notsimilar:
	{
		static const std::string r("not similar to");
		return r;
	}

	// set
	case CartoSQL::ExprFunc::func_t::between:
	{
		static const std::string r("between");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::notbetween:
	{
		static const std::string r("not between");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::betweensymmetric:
	{
		static const std::string r("between symmetric");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::notbetweensymmetric:
	{
		static const std::string r("not between symmetric");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::inset:
	{
		static const std::string r("in");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::notinset:
	{
		static const std::string r("not in");
		return r;
	}

	// conditional
	case CartoSQL::ExprFunc::func_t::coalesce:
	{
		static const std::string r("coalesce");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::nullif:
	{
		static const std::string r("nullif");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::greatest:
	{
		static const std::string r("greatest");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::least:
	{
		static const std::string r("least");
		return r;
	}

	// array
	case CartoSQL::ExprFunc::func_t::array:
	{
		static const std::string r("array");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::array_to_string:
	{
		static const std::string r("array_to_string");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::string_to_array:
	{
		static const std::string r("string_to_array");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::array_length:
	{
		static const std::string r("array_length");
		return r;
	}

	// GIS
	case CartoSQL::ExprFunc::func_t::st_pointonsurface:
	{
		static const std::string r("st_pointonsurface");
		return r;
	}

	case CartoSQL::ExprFunc::func_t::st_dwithin:
	{
		static const std::string r("st_dwithin");
		return r;
	}

	default:
	{
		static const std::string r("?");
		return r;
	}
	}
}

CartoSQL::Expr::const_ptr_t CartoSQL::ExprFunc::simplify_tail(const CartoSQL *csql, func_t func, const exprlist_t& expr, bool modif)
{
	{
		Calculate c(csql);
		Value v;
		if (calculate(c, v, func, expr, nullptr))
			return ptr_t(new ExprValue(v));
	}
	if (expr.size() == 1 && expr[0] && expr[0]->get_type() == Expr::type_t::function) {
		const ExprFunc& e(static_cast<const ExprFunc&>(*expr[0]));
		switch (func) {
		case func_t::logicalnot:
			switch (e.get_func()) {
			case func_t::logicaland:
			{
				exprlist_t el;
				el.reserve(e.get_expr().size());
				for (const const_ptr_t& pe : e.get_expr()) {
					exprlist_t el1;
					el1.push_back(pe);
					el.push_back(ptr_t(new ExprFunc(func_t::logicalnot, el1)));
				}
				return ptr_t(new ExprFunc(func_t::logicalor, el));
			}

			case func_t::logicalor:
			{
				exprlist_t el;
				el.reserve(e.get_expr().size());
				for (const const_ptr_t& pe : e.get_expr()) {
					exprlist_t el1;
					el1.push_back(pe);
					el.push_back(ptr_t(new ExprFunc(func_t::logicalnot, el1)));
				}
				return ptr_t(new ExprFunc(func_t::logicaland, el));
			}

			default:
			{
				func_t func1(notfunc(e.get_func()));
				if (func1 != func_t::invalid)
					return ptr_t(new ExprFunc(func1, e.get_expr()));
				break;
			}
			}
			break;

		case func_t::unaryplus:
		case func_t::unaryminus:
			if (e.get_func() == func_t::unaryplus || e.get_func() == func_t::unaryminus)
				return ptr_t(new ExprFunc((func == e.get_func()) ? func_t::unaryplus : func_t::unaryminus, e.get_expr()));
			break;

		default:
			break;
		}
	}
	if (modif)
		return ptr_t(new ExprFunc(func, expr));
	return ptr_t();
}

template <typename... Args>
CartoSQL::Expr::const_ptr_t CartoSQL::ExprFunc::simplify_int(const CartoSQL *csql, Args&&... args) const
{
	bool work(false);
	exprlist_t e;
	e.reserve(m_expr.size());
	for (const const_ptr_t& p : m_expr) {
		const_ptr_t p1(p->simplify(csql, std::forward<Args>(args)...));
		if (p1)
			work = true;
		else
			p1 = p;
		e.push_back(p1);
	}
	return simplify_tail(csql, m_func, e, work);
}

CartoSQL::Expr::const_ptr_t CartoSQL::ExprFunc::simplify(const CartoSQL *csql, Visitor& v) const
{
	v.visit(*this);
	const_ptr_t p;
	if (v.descend(*this))
		p = simplify_int(csql, v);
	const_ptr_t p1(p ? p->visitor_call_modify(csql, v) : visitor_call_modify(csql, v));
	v.visitend(*this);
	return p1 ? p1 : p;
}

void CartoSQL::ExprFunc::visit(Visitor& v) const
{
	v.visit(*this);
	if (v.descend(*this))
		for (const const_ptr_t& p : m_expr)
			p->visit(v);
	v.visitend(*this);
}

std::ostream& CartoSQL::ExprFunc::print(std::ostream& os, unsigned int indent, const PrintContext& ctx) const
{
	if (m_func >= func_t::unaryoperatorbegin && m_func <= func_t::unaryoperatorend &&
	    m_expr.size() == 1)
		return print_expr(os << m_func, m_expr[0], indent, ctx);
	if (m_func >= func_t::binaryoperatorbegin && m_func <= func_t::binaryoperatorend &&
	    m_expr.size() == 2)
		return print_expr(print_expr(os, m_expr[0], indent, ctx) << ' ' << m_func << ' ',
				  m_expr[1], indent, ctx);
	if (m_func >= func_t::postfixoperatorbegin && m_func <= func_t::postfixoperatorend &&
	    m_expr.size() == 1)
		return print_expr(os, m_expr[0], indent, ctx) << ' ' << m_func;
	switch (m_func) {
		// string
	case func_t::overlay:
		if (m_expr.size() < 3 || m_expr.size() > 4)
			break;
		print_expr(os << m_func << '(', m_expr[0], indent, ctx, false);
		print_expr(os << " placing ", m_expr[1], indent, ctx, false);
		print_expr(os << " from ", m_expr[2], indent, ctx, false);
		if (m_expr.size() >= 4)
			print_expr(os << " for ", m_expr[3], indent, ctx, false);
		return os << ')';

	case func_t::substring:
		if (m_expr.size() < 2 || m_expr.size() > 3)
			break;
		print_expr(os << m_func << '(', m_expr[0], indent, ctx, false);
		print_expr(os << " from ", m_expr[1], indent, ctx, false);
		if (m_expr.size() >= 3)
			print_expr(os << " for ", m_expr[2], indent, ctx, false);
		return os << ')';

		// regex
	case func_t::like:
	case func_t::notlike:
	case func_t::ilike:
	case func_t::notilike:
	case func_t::similar:
	case func_t::notsimilar:
		if (m_expr.size() < 2 || m_expr.size() > 3)
			break;
		print_expr(os, m_expr[0], indent, ctx);
		print_expr(os << ' ' << m_func << ' ', m_expr[1], indent, ctx);
		if (m_expr.size() >= 3)
			print_expr(os << " escape ", m_expr[2], indent, ctx);
		return os;

		// set
	case func_t::between:
	case func_t::notbetween:
	case func_t::betweensymmetric:
	case func_t::notbetweensymmetric:
		if (m_expr.size() != 3)
			break;
		print_expr(os, m_expr[0], indent, ctx);
		print_expr(os << ' ' << m_func << ' ', m_expr[1], indent, ctx);
		print_expr(os << " and ", m_expr[2], indent, ctx);
		return os;

	case func_t::inset:
	case func_t::notinset:
	{
		if (m_expr.empty())
			break;
		print_expr(os, m_expr[0], indent, ctx) << ' ' << m_func << " (";
		const char *sep = "";
		for (exprlist_t::size_type i(1), n(m_expr.size()); i != n; ++i) {
			print_expr(os << sep, m_expr[i], indent, ctx, false);
			sep = ", ";
		}
		return os << ')';
	}

	case func_t::array:
	{
		os << m_func << '[';
		bool subseq(false);
		for (const auto& e : m_expr) {
			if (subseq)
				os << ", ";
			subseq = true;
			print_expr(os, e, indent, ctx, false);
		}
		return os << ']';
	}

	default:
		break;
	}
	os << m_func << '(';
	bool subseq(false);
	for (const auto& e : m_expr) {
		if (subseq)
			os << ", ";
		subseq = true;
		print_expr(os, e, indent, ctx, false);
	}
	return os << ')';
}

bool CartoSQL::ExprFunc::is_paren(void) const
{
	if (m_func >= func_t::unaryoperatorbegin && m_func <= func_t::unaryoperatorend &&
	    m_expr.size() == 1)
		return false;
	if (m_func >= func_t::binaryoperatorbegin && m_func <= func_t::binaryoperatorend &&
	    m_expr.size() == 2)
		return true;
	if (m_func >= func_t::postfixoperatorbegin && m_func <= func_t::postfixoperatorend &&
	    m_expr.size() == 1)
		return true;
	switch (m_func) {
	case func_t::like:
	case func_t::notlike:
	case func_t::ilike:
	case func_t::notilike:
	case func_t::similar:
	case func_t::notsimilar:
	case func_t::between:
	case func_t::notbetween:
	case func_t::betweensymmetric:
	case func_t::notbetweensymmetric:
	case func_t::inset:
	case func_t::notinset:
		return true;

	default:
		return false;
	}
}

bool CartoSQL::ExprFunc::like(Glib::ustring::const_iterator ti, Glib::ustring::const_iterator te,
			      Glib::ustring::const_iterator pi, Glib::ustring::const_iterator pe, gunichar escape)
{
	if (false)
		std::cerr << "ExprFunc::like('" << Glib::ustring(ti, te) << "', '" << Glib::ustring(pi, pe) << "')" << std::endl;
	while (pi != pe) {
		if (false) {
			std::cerr << "ExprFunc::like: pattern '" << static_cast<char>(*pi) << '\'';
			if (ti != te)
				std::cerr << " text '" << static_cast<char>(*ti) << '\'';
			std::cerr << std::endl;
		}
		if (*pi == '_') {
			if (ti == te)
				return false;
			++pi;
			++ti;
			continue;
		}
		if (*pi == '%') {
			++pi;
			if (pi == pe)
				return true;
			for (;;) {
				if (like(ti, te, pi, pe, escape))
					return true;
				if (ti == te)
					return false;
				++ti;
			}
		}
		if (escape && *pi == escape) {
			++pi;
			if (pi == pe)
				break;
		}
		if (ti == te)
			return false;
		if (*pi != *ti)
			return false;
		++pi;
		++ti;
	}
	return ti == te;
}

CartoSQL::Value CartoSQL::ExprFunc::calculate_pointonsurface(const Point& g)
{
	MultiPoint r;
	if (!g.is_invalid())
		r.push_back(g);
	return r;
}

CartoSQL::Value CartoSQL::ExprFunc::calculate_pointonsurface(const MultiPoint& g)
{
	MultiPoint r;
	if (!g.empty())
		r.push_back(g.front());
	return r;
}

CartoSQL::Value CartoSQL::ExprFunc::calculate_pointonsurface(const LineString& g)
{
	MultiPoint r;
	if (!g.empty())
		r.push_back(g[g.size() / 2]);
	return r;
}

CartoSQL::Value CartoSQL::ExprFunc::calculate_pointonsurface(const MultiLineString& g)
{
	MultiPoint r;
	for (const LineString& ls : g) {
		if (ls.empty())
			continue;
		r.push_back(ls[ls.size() / 2]);
		break;
	}
	return r;
}

CartoSQL::Value CartoSQL::ExprFunc::calculate_pointonsurface(const MultiPolygonHole& g)
{
	typedef std::multimap<int64_t,PolygonHole> polys_t;
	polys_t polys;
	for (const auto& ph : g)
		polys.insert(polys_t::value_type(std::abs(ph.area2()), ph));
	MultiPoint r;
	for (polys_t::const_reverse_iterator i(polys.rbegin()), e(polys.rend()); i != e; ++i) {
		Point pt(i->second.get_exterior().centroid());
		if (i->second.windingnumber(pt)) {
			r.push_back(pt);
			break;
		}
		uint32_t bestdist(0);
		PolygonHole::ScanLine sl(i->second.scanline(pt.get_lat()));
		for (PolygonHole::ScanLine::const_iterator si(sl.begin()), se(sl.end()); si != se; ) {
			PolygonHole::ScanLine::const_iterator si0(si);
			++si;
			if (!si0->second)
				continue;
			Point::coord_t lonb(si0->first), lone(lonb);
			if (si == se) {
				// wraparound
				for (PolygonHole::ScanLine::const_iterator si1(sl.begin()); si1 != se; ++si1) {
					if (si1->second)
						continue;
					lone = si1->first;
					break;
				}
			} else {
				lone = si->first;
			}
			uint32_t dist(lone - lonb);
			if (dist <= bestdist)
				continue;
			bestdist = dist;
			pt.set_lon(lonb + (dist >> 1));
		}
		if (!bestdist)
			continue;
		if (!i->second.windingnumber(pt))
			continue;
		r.push_back(pt);
		break;
	}
	return r;
}

CartoSQL::Value CartoSQL::ExprFunc::calculate_pointonsurface(const Rect& g)
{
	MultiPoint r;
	if (!g.is_invalid())
		r.push_back(g.get_southwest().halfway(g.get_northeast()));
	return r;
}

int CartoSQL::ExprFunc::compare(const Expr& x) const
{
	int c(Expr::compare(x));
	if (c)
		return c;
	const ExprFunc& xx(static_cast<const ExprFunc&>(x));
	if (m_func < xx.m_func)
		return -1;
	if (xx.m_func < m_func)
		return 1;
	c = Expr::compare(m_expr, xx.m_expr);
	if (c)
		return c;
	return 0;
}

template <typename T>
bool CartoSQL::ExprFunc::calc_comparison(Value& v, func_t func, T arg0, T arg1)
{
	switch (func) {
	case func_t::equal:
		v = Value(arg0 == arg1);
		return true;

	case func_t::unequal:
		v = Value(arg0 != arg1);
		return true;

	case func_t::lessthan:
		v = Value(arg0 < arg1);
		return true;

	case func_t::lessorequal:
		v = Value(arg0 <= arg1);
		return true;

	case func_t::greaterthan:
		v = Value(arg0 > arg1);
		return true;

	case func_t::greaterorequal:
		v = Value(arg0 >= arg1);
		return true;

	default:
		return false;
	}
}

template <typename T>
bool CartoSQL::ExprFunc::calc_math_unary(Value& v, func_t func, T arg)
{
	switch (func) {
	case func_t::unaryplus:
		v = Value(arg);
		return true;

	case func_t::unaryminus:
		if (!std::numeric_limits<T>::is_signed)
			return false;
		v = Value(-arg);
		return true;

	case func_t::bitwisenot:
		if (!std::numeric_limits<T>::is_integer)
			return false;
		v = Value(~static_cast<int64_t>(arg));
		return true;

	case func_t::factorial:
		if (!std::numeric_limits<T>::is_integer)
			return false;
		{
			int64_t r(1);
			for (int64_t x = 2; x <= arg && x <= 30; ++x)
				r *= x;
			v = Value(r);
		}
		return true;

	case func_t::abs:
		v = Value(std::abs(arg));
		return true;

	case func_t::cbrt:
		v = Value(std::cbrt(arg));
		return true;

	case func_t::ceil:
		v = Value(std::ceil(arg));
		return true;

	case func_t::degrees:
		v = Value(arg * (180.0 / M_PI));
		return true;

	case func_t::exp:
		v = Value(std::exp(arg));
		return true;

	case func_t::floor:
		v = Value(std::floor(arg));
		return true;

	case func_t::ln:
		v = Value(std::log(arg));
		return true;

	case func_t::log10:
		v = Value(std::log10(arg));
		return true;

	case func_t::radians:
		v = Value(arg * (M_PI / 180.0));
		return true;

	case func_t::round:
		if (std::numeric_limits<T>::is_integer)
			v = Value(arg);
		else
			v = Value(std::round(arg));
		return true;

	case func_t::sign:
		if (arg < 0)
			v = Value(static_cast<T>(-1));
		else if (arg < 0)
			v = Value(static_cast<T>(1));
		else
			v = Value(static_cast<T>(0));
		return true;

	case func_t::sqrt:
		v = Value(std::sqrt(arg));
		return true;

	case func_t::trunc:
		v = Value(std::trunc(arg));
		return true;

	case func_t::acos:
		v = Value(std::acos(arg));
		return true;

	case func_t::asin:
		v = Value(std::asin(arg));
		return true;

	case func_t::atan:
		v = Value(std::atan(arg));
		return true;

	case func_t::cos:
		v = Value(std::cos(arg));
		return true;

	case func_t::cot:
		v = Value(1.0 / std::tan(arg));
		return true;

	case func_t::sin:
		v = Value(std::sin(arg));
		return true;

	case func_t::tan:
		v = Value(std::tan(arg));
		return true;

	case func_t::acosd:
		v = Value(std::acos(arg) * (180.0 / M_PI));
		return true;

	case func_t::asind:
		v = Value(std::asin(arg) * (180.0 / M_PI));
		return true;

	case func_t::atand:
		v = Value(std::atan(arg) * (180.0 / M_PI));
		return true;

	case func_t::cosd:
		v = Value(std::cos(arg * (M_PI / 180.0)));
		return true;

	case func_t::cotd:
		v = Value(1.0 / std::tan(arg * (M_PI / 180.0)));
		return true;

	case func_t::sind:
		v = Value(std::sin(arg * (M_PI / 180.0)));
		return true;

	case func_t::tand:
		v = Value(std::tan(arg * (M_PI / 180.0)));
		return true;

	case func_t::sinh:
		v = Value(std::sinh(arg));
		return true;

	case func_t::cosh:
		v = Value(std::cosh(arg));
		return true;

	case func_t::tanh:
		v = Value(std::tanh(arg));
		return true;

	case func_t::asinh:
		v = Value(std::asinh(arg));
		return true;

	case func_t::acosh:
		v = Value(std::acosh(arg));
		return true;

	case func_t::atanh:
		v = Value(std::atanh(arg));
		return true;

	default:
		return false;
	}
}

template <typename T>
bool CartoSQL::ExprFunc::calc_math_binary(Value& v, func_t func, T arg0, T arg1)
{
	switch (func) {
	case func_t::binaryplus:
		v = Value(arg0 + arg1);
		return true;

	case func_t::binaryminus:
		v = Value(arg0 - arg1);
		return true;

	case func_t::binarymul:
		v = Value(arg0 * arg1);
		return true;

	case func_t::binarydiv:
		if (!arg1)
			return false;
		v = Value(arg0 / arg1);
		return true;

	case func_t::binarymod:
		if (!arg1)
			return false;
		if (std::numeric_limits<T>::is_integer)
			v = Value(static_cast<int64_t>(arg0) % static_cast<int64_t>(arg1));
		else
			v = Value(std::fmod(arg0, arg1));
		return true;

	case func_t::bitwiseand:
		if (!std::numeric_limits<T>::is_integer)
			return false;
		v = Value(static_cast<int64_t>(arg0) & static_cast<int64_t>(arg1));
		return true;

	case func_t::bitwiseor:
		if (!std::numeric_limits<T>::is_integer)
			return false;
		v = Value(static_cast<int64_t>(arg0) | static_cast<int64_t>(arg1));
		return true;

	case func_t::bitwisexor:
		if (!std::numeric_limits<T>::is_integer)
			return false;
		v = Value(static_cast<int64_t>(arg0) ^ static_cast<int64_t>(arg1));
		return true;

	case func_t::bitwiseshiftleft:
		if (!std::numeric_limits<T>::is_integer)
			return false;
		v = Value(static_cast<int64_t>(arg0) << static_cast<int64_t>(arg1));
		return true;

	case func_t::bitwiseshiftright:
		if (!std::numeric_limits<T>::is_integer)
			return false;
		v = Value(static_cast<int64_t>(arg0) >> static_cast<int64_t>(arg1));
		return true;

	case func_t::div:
		if (!std::numeric_limits<T>::is_integer)
			return false;
		if (!arg1)
			return false;
		v = Value(arg0 / arg1);
		return true;

	case func_t::log:
		v = Value(std::log(arg0) / std::log(arg1));
		return true;

	case func_t::pow:
		v = Value(std::pow(arg0, arg1));
		return true;

	case func_t::atan2:
		v = Value(std::atan2(arg0, arg1));
		return true;

	case func_t::atan2d:
		v = Value(std::atan2(arg0, arg1) * (180.0 / M_PI));
		return true;

	default:
		return false;
	}
}

bool CartoSQL::ExprFunc::calculate(Calc& c, Value& v, func_t func, const exprlist_t& e, const Expr *expr)
{
	valuelist_t val;
	val.resize(e.size());
	bool ret(true);
	for (valuelist_t::size_type i(0), n(e.size()); i != n; ++i) {
		if (!e[i]) {
			ret = false;
			if (expr)
				c.error(*expr, "argument error");
			continue;
		}
		ret = e[i]->calculate(c, val[i]) && ret;
	}
	if (!ret) {
		calculate(v, func, val);
		return false;
	}
	if (calculate(v, func, val))
		return true;
	if (expr)
		c.error(*expr, "function error");
	return false;
}

bool CartoSQL::ExprFunc::calculate_comparison(Value& v, func_t func, const Value& val0, const Value& val1)
{
	v.set_null();
	if (val0.is_null() || val1.is_null())
		return true;
	if (val0.is_boolean() && val1.is_boolean())
		return calc_comparison(v, func, val0.get_boolean(), val1.get_boolean());
	if (val0.is_integer() && val1.is_integer())
		return calc_comparison(v, func, val0.get_integer(), val1.get_integer());
	if (val0.is_double() && val1.is_double())
		return calc_comparison(v, func, val0.get_double(), val1.get_double());
	if (val0.is_string() && val1.is_string())
		return calc_comparison(v, func, val0.get_string(), val1.get_string());
	if (val0.is_array() && val1.is_array())
		return calc_comparison(v, func, val0.get_array(), val1.get_array());
	if (val0.is_double() || val1.is_double()) {
		Value v0(val0.as_double());
		Value v1(val1.as_double());
		if (v0.is_double() && v1.is_double())
			return calc_comparison(v, func, v0.get_double(), v1.get_double());
		return false;
	}
	if (val0.is_integer() || val1.is_integer()) {
		Value v0(val0.as_integer());
		Value v1(val1.as_integer());
		if (v0.is_integer() && v1.is_integer())
			return calc_comparison(v, func, v0.get_integer(), v1.get_integer());
		return false;
	}
	if (val0.is_boolean() || val1.is_boolean()) {
		Value v0(val0.as_boolean());
		Value v1(val1.as_boolean());
		if (v0.is_boolean() && v1.is_boolean())
			return calc_comparison(v, func, v0.get_boolean(), v1.get_boolean());
		return false;
	}
	{
		Value v0(val0.as_string());
		Value v1(val1.as_string());
		if (v0.is_string() && v1.is_string())
			return calc_comparison(v, func, static_cast<Glib::ustring>(v0.get_string()),
					       static_cast<Glib::ustring>(v1.get_string()));
	}
	return false;
}

bool CartoSQL::ExprFunc::calculate(Value& v, func_t func, const valuelist_t& val)
{
	if (false) {
		std::cerr << "ExprFunc::calculate: " << func << " args";
		for (const Value& v : val)
			std::cerr << ' ' << v.get_sqlstring();
		std::cerr << std::endl;
	}
	v.set_null();
	switch (func) {
		// comparisons
	case func_t::equal:
	case func_t::unequal:
	case func_t::lessthan:
	case func_t::lessorequal:
	case func_t::greaterthan:
	case func_t::greaterorequal:
		if (val.size() != 2)
			return false;
		return calculate_comparison(v, func, val[0], val[1]);

		// logical
		// unary logical
	case func_t::logicalnot:
		if (val.size() != 1)
			return false;
		if (val[0].is_null())
			return true;
		{
			Value v0(val[0].as_boolean());
        		if (!v0.is_boolean())
				return false;
			v = Value(!v0.get_boolean());
			return true;
		}
		return false;

		// binary logical
	case func_t::logicaland:
	case func_t::logicalor:
	{
		if (val.size() != 2)
			return false;
		Value v0, v1;
		if (!val[0].is_null()) {
			v0 = val[0].as_boolean();
			if (!v0.is_boolean())
				return false;
		}
		if (!val[1].is_null()) {
			v1 = val[1].as_boolean();
			if (!v1.is_boolean())
				return false;
		}
		if (v0.is_null()) {
			if (v1.is_null())
				return true;
			switch (func) {
			case func_t::logicaland:
				if (!v1.get_boolean())
					v = Value(false);
				return true;

			case func_t::logicalor:
				if (v1.get_boolean())
					v = Value(true);
				return true;

			default:
				return false;
			}
		}
		if (v1.is_null()) {
			switch (func) {
			case func_t::logicaland:
				if (!v0.get_boolean())
					v = Value(false);
				return true;

			case func_t::logicalor:
				if (v0.get_boolean())
					v = Value(true);
				return true;

			default:
				return false;
			}
		}
		if (!v0.is_boolean() || !v1.is_boolean())
			return false;
		switch (func) {
		case func_t::logicaland:
			v = Value(v0.get_boolean() && v1.get_boolean());
			return true;

		case func_t::logicalor:
			v = Value(v0.get_boolean() || v1.get_boolean());
			return true;

		default:
			break;
		}
		return false;
	}

		// postfix operators
	case func_t::isnull:
		if (val.size() != 1)
			return false;
	        v = Value(val[0].is_null());
		return true;

	case func_t::isnotnull:
		if (val.size() != 1)
			return false;
	        v = Value(!val[0].is_null());
		return true;

	case func_t::isfalse:
	case func_t::isnotfalse:
	case func_t::istrue:
	case func_t::isnottrue:
	case func_t::isunknown:
	case func_t::isnotunknown:
		if (val.size() != 1)
			return false;
		if (val[0].is_null()) {
			switch (func) {
			case func_t::isfalse:
			case func_t::istrue:
			case func_t::isunknown:
				v = Value(false);
				return true;

			case func_t::isnotfalse:
			case func_t::isnottrue:
			case func_t::isnotunknown:
				v = Value(true);
				return true;

			default:
				return false;
			}
		}
		{
			Value v(val[0].as_boolean());
			if (!v.is_boolean())
				return false;
			switch (func) {
			case func_t::isnotfalse:
			case func_t::istrue:
				v = Value(v.get_boolean());
				return true;

			case func_t::isfalse:
			case func_t::isnottrue:
				v = Value(!v.get_boolean());
				return true;

			case func_t::isunknown:
				v = Value(false);
				return true;

			case func_t::isnotunknown:
				v = Value(true);
				return true;

			default:
				return false;
			}
		}

	case func_t::isdistinct:
	case func_t::isnotdistinct:
		if (val.size() != 2)
			return false;
		if (val[0].is_null() || val[1].is_null()) {
			switch (func) {
			case func_t::isdistinct:
				v = Value(val[0].is_null() != val[1].is_null());
				return true;

			case func_t::isnotdistinct:
				v = Value(val[0].is_null() != val[1].is_null());
				return true;

			default:
				return false;
			}
		}
		if (val[0].is_double() || val[1].is_double()) {
			Value v0(val[0].as_double());
			Value v1(val[1].as_double());
			if (v0.is_double() && v1.is_double())
				return calc_comparison(v, (func == func_t::isdistinct) ? func_t::unequal : func_t::equal, v0.get_double(), v1.get_double());
			return false;
		}
		if (val[0].is_integer() || val[1].is_integer()) {
			Value v0(val[0].as_integer());
			Value v1(val[1].as_integer());
			if (v0.is_integer() && v1.is_integer())
				return calc_comparison(v, (func == func_t::isdistinct) ? func_t::unequal : func_t::equal, v0.get_integer(), v1.get_integer());
			return false;
		}
		if (val[0].is_boolean() || val[1].is_boolean()) {
			Value v0(val[0].as_boolean());
			Value v1(val[1].as_boolean());
			if (v0.is_boolean() && v1.is_boolean())
				return calc_comparison(v, (func == func_t::isdistinct) ? func_t::unequal : func_t::equal, v0.get_boolean(), v1.get_boolean());
			return false;
		}
		{
			Value v0(val[0].as_string());
			Value v1(val[1].as_string());
			if (v0.is_string() && v1.is_string())
				return calc_comparison(v, (func == func_t::isdistinct) ? func_t::unequal : func_t::equal, static_cast<Glib::ustring>(v0.get_string()),
						       static_cast<Glib::ustring>(v1.get_string()));
		}
		return false;

		// math
		// no arguments
	case func_t::pi:
		if (!val.empty())
			return false;
		v = Value(M_PI);
		return true;

		// unary functions
	case func_t::unaryplus:
	case func_t::unaryminus:
	case func_t::bitwisenot:
	case func_t::abs:
	case func_t::cbrt:
	case func_t::ceil:
	case func_t::degrees:
	case func_t::exp:
	case func_t::floor:
	case func_t::ln:
	case func_t::log10:
	case func_t::radians:
	case func_t::round:
	case func_t::sign:
	case func_t::sqrt:
	case func_t::trunc:
	case func_t::acos:
	case func_t::asin:
	case func_t::atan:
	case func_t::cos:
	case func_t::cot:
	case func_t::sin:
	case func_t::tan:
	case func_t::acosd:
	case func_t::asind:
	case func_t::atand:
	case func_t::cosd:
	case func_t::cotd:
	case func_t::sind:
	case func_t::tand:
	case func_t::sinh:
	case func_t::cosh:
	case func_t::tanh:
	case func_t::asinh:
	case func_t::acosh:
	case func_t::atanh:
		if (val.size() != 1)
			return false;
		switch (val[0].get_type()) {
		case Value::type_t::null:
			return true;

		case Value::type_t::floatingpoint:
			return calc_math_unary(v, func, val[0].get_double());

		case Value::type_t::integer:
			return calc_math_unary(v, func, val[0].get_integer());

		default:
		{
			Value v0(val[0].as_integer());
			if (v0.is_integer())
				return calc_math_unary(v, func, val[0].get_integer());
			v0 = val[0].as_double();
			if (v0.is_double())
				return calc_math_unary(v, func, val[0].get_double());
			return false;
		}
		}
		return false;

		// binary functions
	case func_t::binaryplus:
	case func_t::binaryminus:
	case func_t::binarymul:
	case func_t::binarydiv:
	case func_t::binarymod:
	case func_t::bitwiseand:
	case func_t::bitwiseor:
	case func_t::bitwisexor:
	case func_t::bitwiseshiftleft:
	case func_t::bitwiseshiftright:
	case func_t::div:
	case func_t::log:
	case func_t::pow:
	case func_t::atan2:
	case func_t::atan2d:
		if (val.size() != 2)
			return false;
		if (val[0].is_null() || val[1].is_null())
			return true;
		if (val[0].is_double() || val[1].is_double()) {
			Value v0(val[0].as_double());
			Value v1(val[1].as_double());
			if (v0.is_double() && v1.is_double())
				return calc_math_binary(v, func, v0.get_double(), v1.get_double());
			return false;
		}
		if (val[0].is_integer() || val[1].is_integer()) {
			Value v0(val[0].as_integer());
			Value v1(val[1].as_integer());
			if (v0.is_integer() && v1.is_integer())
				return calc_math_binary(v, func, v0.get_integer(), v1.get_integer());
			return false;
		}
		{
			Value v0(val[0].as_integer());
			Value v1(val[1].as_integer());
			if (v0.is_integer() && v1.is_integer())
				return calc_math_binary(v, func, v0.get_integer(), v1.get_integer());
		}
		{
			Value v0(val[0].as_double());
			Value v1(val[1].as_double());
			if (v0.is_double() && v1.is_double())
				return calc_math_binary(v, func, v0.get_double(), v1.get_double());
		}
		return false;

		// math
	case func_t::chr:
		if (val.size() != 1)
			return false;
		if (val[0].is_null())
			return true;
		{
			Value v0(val[0].as_integer());
			if (!v0.is_integer())
				return false;
			v = Value(static_cast<std::string>(Glib::ustring(1, static_cast<gunichar>(v0.get_integer()))));
		}
		return true;

		// string
	case func_t::ascii:
	case func_t::length:
	case func_t::bit_length:
	case func_t::char_length:
	case func_t::octet_length:
	case func_t::lower:
	case func_t::md5:
	case func_t::upper:
		if (val.size() != 1)
			return false;
		if (val[0].is_null())
			return true;
		{
			Value v0(val[0].as_string());
			if (!v0.is_string())
				return false;
			switch (func) {
			case func_t::bit_length:
				v = Value(static_cast<int64_t>(v0.get_string().size() * 8));
				return true;

			case func_t::octet_length:
				v = Value(static_cast<int64_t>(v0.get_string().size()));
				return true;

			case func_t::md5:
			{
				unsigned char md[MD5_DIGEST_LENGTH];
				const std::string& s(v0.get_string());
				MD5(reinterpret_cast<const unsigned char *>(s.c_str()), s.size(), md);
				std::ostringstream oss;
				oss << std::hex;
				for (unsigned int i = MD5_DIGEST_LENGTH; i; ) {
					--i;
					oss << std::setw(2) << std::setfill('0') << (md[i] & 0xff);
				}
				v = Value(oss.str());
				return true;
			}

			default:
				break;
			}
			Glib::ustring u0(v0.get_string());
			switch (func) {
			case func_t::ascii:
				if (u0.empty())
					v = Value(static_cast<int64_t>(0));
				else
					v = Value(static_cast<int64_t>(u0[0]));
				return true;

			case func_t::length:
			case func_t::char_length:
				v = Value(static_cast<int64_t>(u0.size()));
				return true;

			case func_t::lower:
				u0 = u0.lowercase();
				v = Value(static_cast<std::string>(u0));
				return true;

			case func_t::upper:
				u0 = u0.uppercase();
				v = Value(static_cast<std::string>(u0));
				return true;

			default:
				break;
			}
		}
		return false;

	case func_t::overlay:
		if (val.size() < 3 || val.size() > 4)
			return false;
		for (const auto& v : val)
			if (v.is_null())
				return true;
		{
			Value str(val[0].as_string());
			Value substr(val[1].as_string());
			Value from(val[2].as_integer());
			if (!str.is_string() || !substr.is_string() || !from.is_integer() || from.get_integer() < 1)
				return false;
			Glib::ustring xstr(str.get_string());
			Glib::ustring xsubstr(substr.get_string());
			Glib::ustring::size_type n(xstr.size());
			Glib::ustring::size_type nsub(xsubstr.size());
			Glib::ustring::size_type i0(static_cast<Glib::ustring::size_type>(from.get_integer() - 1));
			Glib::ustring::size_type i1(i0 + nsub);
			if (val.size() >= 4) {
				Value for_(val[3].as_integer());
				if (!for_.is_integer())
					return false;
				if (for_.get_integer() < -static_cast<int64_t>(i0))
					i1 = 0;
				else
					i1 = i0 + for_.get_integer();
			}
			i0 = std::min(i0, n);
			i1 = std::min(i1, n);
			Glib::ustring s(xstr, 0, i0);
			s.append(xsubstr);
			s.append(xstr, i1, n - i1);
			v = Value(static_cast<std::string>(s));
		}
		return true;

	case func_t::substring:
		if (val.size() < 2 || val.size() > 3)
			return false;
		for (const auto& v : val)
			if (v.is_null())
				return true;
		{
			Value str(val[0].as_string());
			if (!str.is_string())
				return false;
			Glib::ustring xstr(str.get_string());
			Glib::ustring::size_type n(xstr.size());
			{
				Value from(val[1].as_integer());
				if (from.is_integer()) {
					int64_t i0(from.get_integer());
					int64_t i1(n);
					if (val.size() >= 3) {
						Value for_(val[2].as_integer());
						if (!for_.is_integer())
							return false;
						i1 = i0 + for_.get_integer();
					}
					i0 = std::min(std::max(i0, static_cast<int64_t>(1)), static_cast<int64_t>(n));
					i1 = std::min(std::max(i1, static_cast<int64_t>(1)), static_cast<int64_t>(n));
					Glib::ustring s;
					if (i1 > i0)
						s = xstr.substr(i0 - 1, i1 - i0);
					v = Value(static_cast<std::string>(s));
					return true;
				}
			}
			Value pattern(val[1].as_string());
			if (!pattern.is_string())
				return false;
			// FIXME escape
			if (val.size() > 2)
				return false;
			Glib::RefPtr<Glib::Regex> rx(Glib::Regex::create(pattern.get_string()));
			Glib::MatchInfo minfo;
			if (!rx->match(xstr, minfo))
				return true;
			if (minfo.get_match_count() < 1)
				return true;
			v = Value(static_cast<std::string>(minfo.fetch(0)));
		}
		return true;

	case func_t::btrim:
	case func_t::ltrim:
	case func_t::rtrim:
		if (val.size() != 2)
			return false;
		if (val[0].is_null() || val[1].is_null())
			return true;
		{
			Value str(val[0].as_string());
			Value cset(val[1].as_string());
			if (!str.is_string() || !cset.is_string())
				return false;
			Glib::ustring xstr(str.get_string());
			Glib::ustring xcset(cset.get_string());
			if (func == func_t::btrim || func == func_t::ltrim) {
				Glib::ustring::size_type i(xstr.find_first_not_of(xcset));
				xstr.erase(0, i);
			}
			if (func == func_t::btrim || func == func_t::rtrim) {
				Glib::ustring::size_type i(xstr.find_last_not_of(xcset));
				if (i == Glib::ustring::npos)
					xstr.clear();
				else
					xstr.erase(i + 1);
			}
			v = Value(static_cast<std::string>(xstr));
		}
		return true;

	case func_t::regex:
	case func_t::notregex:
	case func_t::regexnocase:
	case func_t::notregexnocase:
		if (val.size() != 2)
			return false;
		if (val[0].is_null() || val[1].is_null())
			return true;
		{
			Value str(val[0].as_string());
			Value pattern(val[1].as_string());
			if (!str.is_string() || !pattern.is_string())
				return false;
			Glib::ustring xstr(str.get_string());
			Glib::RegexCompileFlags compopt(static_cast<Glib::RegexCompileFlags>(0));
			if (func == func_t::regexnocase || func == func_t::notregexnocase)
				compopt = Glib::REGEX_CASELESS;
			Glib::RefPtr<Glib::Regex> rx(Glib::Regex::create(pattern.get_string(), compopt));
			Glib::MatchInfo minfo;
			bool val(rx->match(xstr, minfo));
			if (func == func_t::notregex || func == func_t::notregexnocase)
				val = !val;
			v = Value(val);
		}
		return true;

	case func_t::stringconcat:
	{
		Glib::ustring s;
		for (const auto& vx : val) {
			if (vx.is_null())
				return true;
			Value v(vx.as_string());
			if (!v.is_string())
				return false;
			s.append(Glib::ustring(v.get_string()));
		}
		v = Value(static_cast<std::string>(s));
		return true;
	}

	case func_t::concat:
	{
		Glib::ustring s;
		bool hasstr(false);
		for (const auto& vx : val) {
			if (vx.is_null())
				continue;
			Value v(vx.as_string());
			if (!v.is_string())
				return false;
			s.append(Glib::ustring(v.get_string()));
			hasstr = true;
		}
		if (hasstr)
			v = Value(static_cast<std::string>(s));
		return true;
	}

	case func_t::concat_ws:
		if (val.size() < 1)
			return false;
		if (val[0].is_null())
			return true;
		{
			Value vsep(val[0].as_string());
			if (!vsep.is_string())
				return false;
			Glib::ustring sep(vsep.get_string());
			Glib::ustring s;
			bool subseq(false);
			for (valuelist_t::size_type i(1), n(val.size()); i < n; ++i) {
				if (val[i].is_null())
					continue;
				Value v(val[i].as_string());
				if (!v.is_string())
					return false;
				if (subseq)
					s.append(sep);
				subseq = true;
				s.append(Glib::ustring(v.get_string()));
			}
			v = Value(static_cast<std::string>(s));
		}
		return true;

	case func_t::left:
	case func_t::right:
		if (val.size() != 2)
			return false;
		if (val[0].is_null() || val[1].is_null())
			return true;
		{
			Value str(val[0].as_string());
			Value cnt(val[1].as_integer());
			if (!str.is_string() || !cnt.is_integer())
				return false;
			int64_t xcnt(cnt.get_integer());
			if (!xcnt) {
				v = Value(std::string());
				return true;
			}
			Glib::ustring xstr(str.get_string());
			Glib::ustring::size_type n(xstr.size());
			if (xcnt < 0) {
				xcnt = -xcnt;
				xcnt = std::min(xcnt, static_cast<int64_t>(n));
				if (func == func_t::left)
					v = Value(static_cast<std::string>(xstr.substr(0, n - xcnt)));
				if (func == func_t::right)
					v = Value(static_cast<std::string>(xstr.substr(xcnt, n - xcnt)));
			} else {
				if (func == func_t::left)
					v = Value(static_cast<std::string>(xstr.substr(0, xcnt)));
				if (func == func_t::right)
					v = Value(static_cast<std::string>(xstr.substr(n - xcnt, xcnt)));
			}
		}
		return true;

	case func_t::replace:
		if (val.size() != 3)
			return false;
		if (val[0].is_null() || val[1].is_null() || val[2].is_null())
			return true;
		{
			Value str(val[0].as_string());
			Value from(val[1].as_string());
			Value to(val[2].as_string());
			if (!str.is_string() || !from.is_string() || !to.is_string())
				return false;
			Glib::ustring xstr(str.get_string());
			Glib::ustring xfrom(from.get_string());
			Glib::ustring xto(to.get_string());
			Glib::ustring::size_type n(xstr.size());
			Glib::ustring::size_type nfrom(xfrom.size());
			Glib::ustring::size_type nto(xto.size());
			for (Glib::ustring::size_type i(0); i < n; ) {
				i = xstr.find(xfrom, i);
				if (i == Glib::ustring::npos)
					break;
				xstr.replace(i, i + nfrom, xto);
				i += nto;
			}
			v = Value(static_cast<std::string>(xstr));
		}
		return true;

	case func_t::strpos:
		if (val.size() != 2)
			return false;
		if (val[0].is_null() || val[1].is_null())
			return true;
		{
			Value str(val[0].as_string());
			Value substr(val[1].as_string());
			if (!str.is_string() || !substr.is_string())
				return false;
			Glib::ustring xstr(str.get_string());
			Glib::ustring xsubstr(substr.get_string());
			Glib::ustring::size_type i(xstr.find(xsubstr));
			int64_t r((i == Glib::ustring::npos) ? 0 : i + 1);
			v = Value(r);
		}
		return true;

	case func_t::substr:
		if (val.size() < 2 || val.size() > 3)
			return false;
		for (const auto& v : val)
			if (v.is_null())
				return true;
		{
			Value str(val[0].as_string());
			Value from(val[1].as_integer());
			if (!str.is_string() || !from.is_integer())
				return false;
			Glib::ustring xstr(str.get_string());
			Glib::ustring::size_type n(xstr.size());
			int64_t i0(from.get_integer());
			int64_t i1(n);
			if (val.size() >= 3) {
				Value for_(val[2].as_integer());
				if (!for_.is_integer())
					return false;
				i1 = i0 + for_.get_integer();
			}
			i0 = std::min(std::max(i0, static_cast<int64_t>(1)), static_cast<int64_t>(n));
			i1 = std::min(std::max(i1, static_cast<int64_t>(1)), static_cast<int64_t>(n));
			Glib::ustring s;
			if (i1 > i0)
				s = xstr.substr(i0 - 1, i1 - i0);
			v = Value(static_cast<std::string>(s));
		}
		return true;

		// regex
	case func_t::like:
	case func_t::notlike:
	case func_t::ilike:
	case func_t::notilike:
		if (val.size() < 2 || val.size() > 3)
			return false;
		for (const auto& v : val)
			if (v.is_null())
				return true;
		{
			Value str(val[0].as_string());
			Value pattern(val[1].as_string());
			if (!str.is_string() || !pattern.is_string())
				return false;
			Glib::ustring xstr(str.get_string());
			Glib::ustring xpattern(pattern.get_string());
			gunichar escape('\\');
			if (func == func_t::ilike || func == func_t::notilike) {
				xstr = xstr.casefold();
				xpattern = xpattern.casefold();
			}
			if (val.size() >= 3) {
				Value escapes(val[2].as_string());
				if (!escapes.is_string())
					return false;
				Glib::ustring xescape(escapes.get_string());
				if (func == func_t::ilike || func == func_t::notilike)
					xescape = xescape.casefold();
				if (xescape.empty())
					escape = 0;
				else
					escape = xescape[0];
			}
			bool val(like(xstr.begin(), xstr.end(), xpattern.begin(), xpattern.end(), escape));
			if (func == func_t::notlike || func == func_t::notilike)
				val = !val;
			v = Value(val);
		}
		return true;

	case func_t::similar:
	case func_t::notsimilar:
		// FIXME
		return false;

		// set
	case func_t::between:
	case func_t::notbetween:
	case func_t::betweensymmetric:
	case func_t::notbetweensymmetric:
		if (val.size() != 3)
			return false;
		if (val[0].is_null() || val[1].is_null() || val[2].is_null())
			return true;
		{
			unsigned int symm(0);
			bool inv(func == func_t::notbetween || func == func_t::notbetweensymmetric);
			Value vv;
			if (func == func_t::betweensymmetric || func == func_t::notbetweensymmetric) {
				if (!calculate_comparison(vv, func_t::greaterthan, val[1], val[2]) || !vv.is_boolean())
					return false;
				symm = vv.get_boolean();
			}
			if (!calculate_comparison(vv, func_t::greaterorequal, val[0], val[1+symm]) || !vv.is_boolean())
				return false;
			if (!vv.get_boolean()) {
				v = Value(inv);
				return true;
			}
			if (!calculate_comparison(vv, func_t::lessorequal, val[0], val[2-symm]) || !vv.is_boolean())
				return false;
			if (!vv.get_boolean()) {
				v = Value(inv);
				return true;
			}
			v = Value(!inv);
		}
		return true;

	case func_t::inset:
	case func_t::notinset:
		if (val.empty())
			return false;
		if (val[0].is_null())
			return true;
		for (valuelist_t::size_type i(1), n(val.size()); i < n; ++i) {
			Value vv;
			if (!calculate_comparison(vv, func_t::equal, val[0], val[i]))
				return false;
			if (vv.is_null())
				continue;
			if (!vv.is_boolean())
				return false;
			if (!vv.get_boolean())
				continue;
			v = Value(func == func_t::inset);
			return true;
		}
		v = Value(func == func_t::notinset);
		return true;

		// conditional
	case func_t::coalesce:
		for (const Value& vv : val) {
			if (vv.is_null())
				continue;
			v = vv;
			return true;
		}
		return true;

	case func_t::nullif:
		if (val.size() != 2)
			return false;
		if (val[0].is_null())
			return true;
		{
			Value vv;
			if (!calculate(vv, func_t::equal, val))
				return false;
			if (vv.is_boolean() && vv.get_boolean())
				return true;
		}
		v = val[0];
		return true;

	case func_t::greatest:
	case func_t::least:
		if (val.empty())
			return false;
		{
			func_t func1((func == func_t::greatest) ? func_t::greaterthan : func_t::lessthan);
			Value vvv;
			for (const Value& vv : val) {
				if (vv.is_null())
					continue;
				if (vvv.is_null()) {
					vvv = vv;
					continue;
				}
				Value vvvv;
				if (!calculate_comparison(vvvv, func1, vv, vvv))
					return false;
				if (!vvvv.is_boolean() || !vvvv.get_boolean())
					continue;
				vvv = vv;
			}
			v = vvv;
		}
		return true;

		// array
	case func_t::array:
		v = Value(val);
		return true;

	case func_t::string_to_array:
		if (val.size() < 2 || val.size() > 3)
			return false;
		if (val.size() >= 3 && !(val[2].is_null() || val[2].is_string()))
			return false;
		if (val[1].is_null()) {
			v = val[0];
			return true;
		}
		if (!val[1].is_string())
			return false;
		if (val[0].is_null())
			return true;
		if (!val[0].is_string())
			return false;
		{
			Glib::ustring str(val[0].get_string());
			Glib::ustring delim(val[1].get_string());
			Glib::ustring null;
			bool hasnull(val.size() >= 3 && val[2].is_string());
			if (hasnull)
				null = val[2].get_string();
			Value::array_t va;
			if (!str.empty()) {
				for (;;) {
					Glib::ustring::size_type i(str.find(delim));
					if (i == Glib::ustring::npos) {
						if (hasnull && str == null)
							va.push_back(Value());
						else
							va.push_back(Value(static_cast<std::string>(str)));
						str.clear();
						break;
					}
					{
						Glib::ustring v(str, 0, i);
						if (hasnull && v == null)
							va.push_back(Value());
						else
							va.push_back(Value(static_cast<std::string>(v)));
					}
					str.erase(0, i + delim.size());
				}
			}
			v = Value(va);
		}
		return true;

	case func_t::array_to_string:
		if (val.size() < 2 || val.size() > 3 || !val[0].is_array())
			return false;
		if (val.size() >= 3 && !(val[2].is_null() || val[2].is_string()))
			return false;
		if (val[1].is_null())
			return true;
		{
			Glib::ustring str;
			Glib::ustring delim(val[1].get_string());
			Glib::ustring null;
			bool hasnull(val.size() >= 3 && val[2].is_string());
			if (hasnull)
				null = val[2].get_string();
			bool subseq(false);
			for (const Value& v : val[0].get_array()) {
				if (v.is_null()) {
					if (!hasnull)
						continue;
					if (subseq)
						str.append(delim);
					subseq = true;
					str.append(null);
					continue;
				}
				Value vs(v.as_string());
				if (!vs.is_string())
					return false;
				if (subseq)
					str.append(delim);
				subseq = true;
				str.append(vs.get_string());
			}
			v = Value(static_cast<std::string>(str));
		}
		return true;

	case func_t::array_length:
		if (val.size() != 2 || !val[0].is_array())
			return false;
		if (val[1].is_null())
			return true;
		if (!val[1].is_integer())
			return false;
		if (val[1].get_integer() != 1)
			return true;
		v = Value(static_cast<int64_t>(val[0].get_array().size()));
		return true;

		// GIS
	case func_t::overlap:
		if (val.size() != 2)
			return false;
		if (val[0].is_array() && val[1].is_array()) {
			Value::array_t v0(val[0].get_array());
			Value::array_t v1(val[1].get_array());
			std::sort(v0.begin(), v0.end(), Value::ValueOrdering());
			std::sort(v1.begin(), v1.end(), Value::ValueOrdering());
			for (Value::array_t::const_iterator i0(v0.begin()), e0(v0.end()), i1(v1.begin()), e1(v1.end()); i0 != e0 && i1 != e1; ) {
				int c(i0->compare_value(*i1));
				if (c < 0) {
					++i0;
				} else if (c > 0) {
					++i1;
				} else {
					v = Value(true);
					return true;
				}
			}
			v = Value(true);
			return false;
		}
		v = Value(true);
		return true;

	case func_t::st_pointonsurface:
		if (val.size() != 1)
			return false;
		switch (val[0].get_type()) {
		case Value::type_t::points:
			v = calculate_pointonsurface(val[0].get_points());
			return true;

		case Value::type_t::lines:
			v = calculate_pointonsurface(val[0].get_lines());
			return true;

		case Value::type_t::polygons:
			v = calculate_pointonsurface(val[0].get_polygons());
			return true;

		case Value::type_t::rectangle:
			v = calculate_pointonsurface(val[0].get_rectangle());
			return true;

		case Value::type_t::osmdbobject:
			switch (val[0].get_osmdbobject()->get_type()) {
			case OSMDB::Object::type_t::point:
				v = calculate_pointonsurface(val[0].get_point());
				return true;

			case OSMDB::Object::type_t::line:
			case OSMDB::Object::type_t::road:
				v = calculate_pointonsurface(val[0].get_line());
				return true;

			case OSMDB::Object::type_t::area:
				v = calculate_pointonsurface(val[0].get_polygons());
				return true;

			default:
				return false;
			}

		default:
			return false;
		}

	case func_t::st_dwithin:
	{
		if (val.size() != 3)
			return false;
		if (!val[2].is_numeric())
			return false;
		double dist_nmi(static_cast<double>(val[2]) * (1e-3 * Point::km_to_nmi_dbl));
		if (std::isnan(dist_nmi) || dist_nmi < 0)
			return true;
		switch (val[0].get_type()) {
		case Value::type_t::points:
			switch (val[1].get_type()) {
			case Value::type_t::points:
				v = val[0].get_points().dwithin(val[1].get_points(), dist_nmi);
				return true;

			case Value::type_t::lines:
				v = val[1].get_lines().dwithin(val[0].get_points(), dist_nmi);
				return true;

			case Value::type_t::polygons:
				v = val[1].get_polygons().dwithin(val[0].get_points(), dist_nmi);
				return true;

			case Value::type_t::rectangle:
				v = static_cast<PolygonSimple>(val[1].get_rectangle()).dwithin(val[0].get_points(), dist_nmi);
				return true;

			case Value::type_t::osmdbobject:
				switch (val[1].get_osmdbobject()->get_type()) {
				case OSMDB::Object::type_t::point:
					v = val[0].get_points().dwithin(val[1].get_point(), dist_nmi);
					return true;

				case OSMDB::Object::type_t::line:
				case OSMDB::Object::type_t::road:
					v = val[1].get_line().dwithin(val[0].get_points(), dist_nmi);
					return true;

				case OSMDB::Object::type_t::area:
					v = val[1].get_polygons().dwithin(val[0].get_points(), dist_nmi);
					return true;

				default:
					return false;
				}

			default:
				return false;
			}
			return false;

		case Value::type_t::lines:
			switch (val[1].get_type()) {
			case Value::type_t::points:
				v = val[0].get_lines().dwithin(val[1].get_points(), dist_nmi);
				return true;

			case Value::type_t::lines:
				v = val[0].get_lines().dwithin(val[1].get_lines(), dist_nmi);
				return true;

			case Value::type_t::polygons:
				v = val[1].get_polygons().dwithin(val[0].get_lines(), dist_nmi);
				return true;

			case Value::type_t::rectangle:
				v = static_cast<PolygonSimple>(val[1].get_rectangle()).dwithin(val[0].get_lines(), dist_nmi);
				return true;

			case Value::type_t::osmdbobject:
				switch (val[1].get_osmdbobject()->get_type()) {
				case OSMDB::Object::type_t::point:
					v = val[0].get_lines().dwithin(val[1].get_point(), dist_nmi);
					return true;

				case OSMDB::Object::type_t::line:
				case OSMDB::Object::type_t::road:
					v = val[0].get_lines().dwithin(val[1].get_line(), dist_nmi);
					return true;

				case OSMDB::Object::type_t::area:
					v = val[1].get_polygons().dwithin(val[0].get_lines(), dist_nmi);
					return true;

				default:
					return false;
				}

			default:
				return false;
			}
			return false;

		case Value::type_t::polygons:
			switch (val[1].get_type()) {
			case Value::type_t::points:
				v = val[0].get_polygons().dwithin(val[1].get_points(), dist_nmi);
				return true;

			case Value::type_t::lines:
				v = val[0].get_polygons().dwithin(val[1].get_lines(), dist_nmi);
				return true;

			case Value::type_t::polygons:
				v = val[0].get_polygons().dwithin(val[1].get_polygons(), dist_nmi);
				return true;

			case Value::type_t::rectangle:
				v = val[0].get_polygons().dwithin(static_cast<PolygonSimple>(val[1].get_rectangle()), dist_nmi);
				return true;

			case Value::type_t::osmdbobject:
				switch (val[1].get_osmdbobject()->get_type()) {
				case OSMDB::Object::type_t::point:
					v = val[0].get_polygons().dwithin(val[1].get_point(), dist_nmi);
					return true;

				case OSMDB::Object::type_t::line:
				case OSMDB::Object::type_t::road:
					v = val[0].get_polygons().dwithin(val[1].get_line(), dist_nmi);
					return true;

				case OSMDB::Object::type_t::area:
					v = val[0].get_polygons().dwithin(val[1].get_polygons(), dist_nmi);
					return true;

				default:
					return false;
				}

			default:
				return false;
			}
			return false;

		case Value::type_t::rectangle:
			switch (val[1].get_type()) {
			case Value::type_t::points:
				v = static_cast<PolygonSimple>(val[0].get_rectangle()).dwithin(val[1].get_points(), dist_nmi);
				return true;

			case Value::type_t::lines:
				v = static_cast<PolygonSimple>(val[0].get_rectangle()).dwithin(val[1].get_lines(), dist_nmi);
				return true;

			case Value::type_t::polygons:
				v = val[1].get_polygons().dwithin(static_cast<PolygonSimple>(val[0].get_rectangle()), dist_nmi);
				return true;

			case Value::type_t::rectangle:
				v = static_cast<PolygonSimple>(val[0].get_rectangle()).dwithin(static_cast<PolygonSimple>(val[1].get_rectangle()), dist_nmi);
				return true;

			case Value::type_t::osmdbobject:
				switch (val[1].get_osmdbobject()->get_type()) {
				case OSMDB::Object::type_t::point:
					v = static_cast<PolygonSimple>(val[0].get_rectangle()).dwithin(val[1].get_point(), dist_nmi);
					return true;

				case OSMDB::Object::type_t::line:
				case OSMDB::Object::type_t::road:
					v = static_cast<PolygonSimple>(val[0].get_rectangle()).dwithin(val[1].get_line(), dist_nmi);
					return true;

				case OSMDB::Object::type_t::area:
					v = val[1].get_polygons().dwithin(static_cast<PolygonSimple>(val[0].get_rectangle()), dist_nmi);
					return true;

				default:
					return false;
				}

			default:
				return false;
			}
			return false;

		case Value::type_t::osmdbobject:
			switch (val[0].get_osmdbobject()->get_type()) {
			case OSMDB::Object::type_t::point:
				switch (val[1].get_type()) {
				case Value::type_t::points:
					v = val[1].get_points().dwithin(val[0].get_point(), dist_nmi);
					return true;

				case Value::type_t::lines:
					v = val[1].get_lines().dwithin(val[0].get_point(), dist_nmi);
					return true;

				case Value::type_t::polygons:
					v = val[1].get_polygons().dwithin(val[0].get_point(), dist_nmi);
					return true;

				case Value::type_t::rectangle:
					v = static_cast<PolygonSimple>(val[1].get_rectangle()).dwithin(val[0].get_point(), dist_nmi);
					return true;

				case Value::type_t::osmdbobject:
					switch (val[1].get_osmdbobject()->get_type()) {
					case OSMDB::Object::type_t::point:
						v = val[0].get_point().dwithin(val[1].get_point(), dist_nmi);
						return true;

					case OSMDB::Object::type_t::line:
					case OSMDB::Object::type_t::road:
						v = val[1].get_line().dwithin(val[0].get_point(), dist_nmi);
						return true;

					case OSMDB::Object::type_t::area:
						v = val[1].get_polygons().dwithin(val[0].get_point(), dist_nmi);
						return true;

					default:
						return false;
					}

				default:
					return false;
				}
				return false;

			case OSMDB::Object::type_t::line:
			case OSMDB::Object::type_t::road:
				switch (val[1].get_type()) {
				case Value::type_t::points:
					v = val[0].get_line().dwithin(val[1].get_points(), dist_nmi);
					return true;

				case Value::type_t::lines:
					v = val[1].get_lines().dwithin(val[0].get_line(), dist_nmi);
					return true;

				case Value::type_t::polygons:
					v = val[1].get_polygons().dwithin(val[0].get_line(), dist_nmi);
					return true;

				case Value::type_t::rectangle:
					v = static_cast<PolygonSimple>(val[1].get_rectangle()).dwithin(val[0].get_line(), dist_nmi);
					return true;

				case Value::type_t::osmdbobject:
					switch (val[1].get_osmdbobject()->get_type()) {
					case OSMDB::Object::type_t::point:
						v = val[0].get_line().dwithin(val[1].get_point(), dist_nmi);
						return true;

					case OSMDB::Object::type_t::line:
					case OSMDB::Object::type_t::road:
						v = val[0].get_line().dwithin(val[1].get_line(), dist_nmi);
						return true;

					case OSMDB::Object::type_t::area:
						v = val[1].get_polygons().dwithin(val[0].get_line(), dist_nmi);
						return true;

					default:
						return false;
					}

				default:
					return false;
				}
				return false;

			case OSMDB::Object::type_t::area:
				switch (val[1].get_type()) {
				case Value::type_t::points:
					v = val[0].get_polygons().dwithin(val[1].get_points(), dist_nmi);
					return true;

				case Value::type_t::lines:
					v = val[0].get_polygons().dwithin(val[1].get_lines(), dist_nmi);
					return true;

				case Value::type_t::polygons:
					v = val[0].get_polygons().dwithin(val[1].get_polygons(), dist_nmi);
					return true;

				case Value::type_t::rectangle:
					v = val[0].get_polygons().dwithin(static_cast<PolygonSimple>(val[1].get_rectangle()), dist_nmi);
					return true;

				case Value::type_t::osmdbobject:
					switch (val[1].get_osmdbobject()->get_type()) {
					case OSMDB::Object::type_t::point:
						v = val[0].get_polygons().dwithin(val[1].get_point(), dist_nmi);
						return true;

					case OSMDB::Object::type_t::line:
					case OSMDB::Object::type_t::road:
						v = val[0].get_polygons().dwithin(val[1].get_line(), dist_nmi);
						return true;

					case OSMDB::Object::type_t::area:
						v = val[0].get_polygons().dwithin(val[1].get_polygons(), dist_nmi);
						return true;

					default:
						return false;
					}

				default:
					return false;
				}
				return false;

			default:
				return false;
			}

		default:
			return false;
		}
		return false;
	}

	default:
		return false;
	}
}

CartoSQL::ExprFunc::func_t CartoSQL::ExprFunc::notfunc(func_t func)
{
	switch (func) {
	case func_t::equal:
		return func_t::unequal;

	case func_t::unequal:
		return func_t::equal;

	case func_t::lessthan:
		return func_t::greaterorequal;

	case func_t::lessorequal:
		return func_t::greaterthan;

	case func_t::greaterthan:
		return func_t::lessorequal;

	case func_t::greaterorequal:
		return func_t::lessthan;

	case func_t::regex:
		return func_t::notregex;

	case func_t::notregex:
		return func_t::regex;

	case func_t::regexnocase:
		return func_t::notregexnocase;

	case func_t::notregexnocase:
		return func_t::regexnocase;

	case func_t::isnull:
		return func_t::isnotnull;

	case func_t::isnotnull:
		return func_t::isnull;

	case func_t::isfalse:
		return func_t::isnotfalse;

	case func_t::isnotfalse:
		return func_t::isfalse;

	case func_t::istrue:
		return func_t::isnottrue;

	case func_t::isnottrue:
		return func_t::istrue;

	case func_t::isunknown:
		return func_t::isnotunknown;

	case func_t::isnotunknown:
		return func_t::isunknown;

	case func_t::isdistinct:
		return func_t::isnotdistinct;

	case func_t::isnotdistinct:
		return func_t::isdistinct;

	case func_t::like:
		return func_t::notlike;

	case func_t::notlike:
		return func_t::like;

	case func_t::ilike:
		return func_t::notilike;

	case func_t::notilike:
		return func_t::ilike;

	case func_t::between:
		return func_t::notbetween;

	case func_t::notbetween:
		return func_t::between;

	case func_t::betweensymmetric:
		return func_t::notbetweensymmetric;

	case func_t::notbetweensymmetric:
		return func_t::betweensymmetric;

	case func_t::inset:
		return func_t::notinset;

	case func_t::notinset:
		return func_t::inset;

	case func_t::similar:
		return func_t::notsimilar;

	case func_t::notsimilar:
		return func_t::similar;

	default:
		return func_t::invalid;

	}
}

void CartoSQL::ExprFunc::extract_joincolumns(JoinColumns& jc) const
{
	switch (m_func) {
	case func_t::logicaland:
		for (const Expr::const_ptr_t& p : m_expr)
			if (p)
				p->extract_joincolumns(jc);
		return;

	case func_t::equal:
	{
		if (m_expr.size() != 2 || !m_expr[0] || !m_expr[1] ||
		    m_expr[0]->get_type() != type_t::tablecolumn ||
		    m_expr[1]->get_type() != type_t::tablecolumn)
			return;
		const ExprTableColumn *left(static_cast<const ExprTableColumn *>(m_expr[0].get()));
		const ExprTableColumn *right(static_cast<const ExprTableColumn *>(m_expr[1].get()));
		if (left->get_tableindex() > right->get_tableindex())
			std::swap(left, right);
		if (left->get_tableindex() != 0 && right->get_tableindex() != 1)
			return;
		jc.push_back(JoinColumn(left->get_columnindex(), right->get_columnindex()));
		return;
	}

	default:
		return;
	}
}

void CartoSQL::ExprFunc::split_and_terms(exprlist_t& el) const
{
	if (m_func != func_t::logicaland) {
		Expr::split_and_terms(el);
		return;
	}
	for (const const_ptr_t& p : m_expr)
		p->split_and_terms(el);
}

CartoSQL::Value::type_t CartoSQL::ExprFunc::calculate_type(const subtables_t& subtables) const
{
	switch (m_func) {
		// comparisons
	case func_t::equal:
	case func_t::unequal:
	case func_t::lessthan:
	case func_t::lessorequal:
	case func_t::greaterthan:
	case func_t::greaterorequal:
		return Value::type_t::boolean;

		// logical
		// unary logical
	case func_t::logicalnot:
		return Value::type_t::boolean;

		// binary logical
	case func_t::logicaland:
	case func_t::logicalor:
		return Value::type_t::boolean;

		// postfix operators
	case func_t::isnull:
	case func_t::isnotnull:
	case func_t::isfalse:
	case func_t::isnotfalse:
	case func_t::istrue:
	case func_t::isnottrue:
	case func_t::isunknown:
	case func_t::isnotunknown:
	case func_t::isdistinct:
	case func_t::isnotdistinct:
		return Value::type_t::boolean;

		// math
		// no arguments
	case func_t::pi:
		return Value::type_t::floatingpoint;

		// unary functions
	case func_t::unaryplus:
	case func_t::unaryminus:
	case func_t::abs:
	{
		if (m_expr.size() != 1 || !m_expr[0])
			return Value::type_t::null;
		Value::type_t t(m_expr[0]->calculate_type(subtables));
		if (t == Value::type_t::floatingpoint || t == Value::type_t::integer)
			return t;
		return Value::type_t::null;
	}

	case func_t::bitwisenot:
		return Value::type_t::integer;

	case func_t::cbrt:
	case func_t::ceil:
	case func_t::degrees:
	case func_t::exp:
	case func_t::floor:
	case func_t::ln:
	case func_t::log10:
	case func_t::radians:
	case func_t::round:
	case func_t::sign:
	case func_t::sqrt:
	case func_t::trunc:
	case func_t::acos:
	case func_t::asin:
	case func_t::atan:
	case func_t::cos:
	case func_t::cot:
	case func_t::sin:
	case func_t::tan:
	case func_t::acosd:
	case func_t::asind:
	case func_t::atand:
	case func_t::cosd:
	case func_t::cotd:
	case func_t::sind:
	case func_t::tand:
	case func_t::sinh:
	case func_t::cosh:
	case func_t::tanh:
	case func_t::asinh:
	case func_t::acosh:
	case func_t::atanh:
		return Value::type_t::floatingpoint;

		// binary functions
	case func_t::bitwiseand:
	case func_t::bitwiseor:
	case func_t::bitwisexor:
	case func_t::bitwiseshiftleft:
	case func_t::bitwiseshiftright:
	case func_t::binarymod:
	case func_t::div:
		return Value::type_t::integer;

	case func_t::binaryplus:
	case func_t::binaryminus:
	case func_t::binarymul:
	case func_t::binarydiv:
	case func_t::log:
	case func_t::pow:
	case func_t::atan2:
	case func_t::atan2d:
	{
		if (m_expr.size() != 2 || !m_expr[0] || !m_expr[1])
			return Value::type_t::null;
		Value::type_t t0(m_expr[0]->calculate_type(subtables));
		Value::type_t t1(m_expr[1]->calculate_type(subtables));
		if (t0 == Value::type_t::floatingpoint || t1 == Value::type_t::floatingpoint) {
			if ((t0 != Value::type_t::floatingpoint && t0 != Value::type_t::integer) ||
			    (t1 != Value::type_t::floatingpoint && t1 != Value::type_t::integer))
				return Value::type_t::null;
			return Value::type_t::floatingpoint;
		}
		if (t0 == Value::type_t::integer && t1 == Value::type_t::integer)
			return Value::type_t::integer;
		return Value::type_t::null;
	}

	case func_t::chr:
		return Value::type_t::string;

		// string
	case func_t::ascii:
	case func_t::length:
	case func_t::bit_length:
	case func_t::char_length:
	case func_t::octet_length:
	case func_t::strpos:
		return Value::type_t::integer;

	case func_t::lower:
	case func_t::md5:
	case func_t::upper:
	case func_t::overlay:
	case func_t::substring:
	case func_t::btrim:
	case func_t::ltrim:
	case func_t::rtrim:
	case func_t::stringconcat:
	case func_t::concat:
	case func_t::concat_ws:
	case func_t::left:
	case func_t::right:
	case func_t::replace:
	case func_t::substr:
		return Value::type_t::string;

	case func_t::regex:
	case func_t::notregex:
	case func_t::regexnocase:
	case func_t::notregexnocase:
	case func_t::like:
	case func_t::notlike:
	case func_t::ilike:
	case func_t::notilike:
	case func_t::between:
	case func_t::notbetween:
	case func_t::betweensymmetric:
	case func_t::notbetweensymmetric:
	case func_t::inset:
	case func_t::notinset:
		return Value::type_t::boolean;

	case func_t::similar:
	case func_t::notsimilar:
		// FIXME
		return Value::type_t::null;

		// conditional
	case func_t::coalesce:
	case func_t::greatest:
	case func_t::least:
	{
		Value::type_t t(Value::type_t::null);
		for (const const_ptr_t& p : m_expr) {
			if (!p)
				return Value::type_t::null;
			Value::type_t tx(p->calculate_type(subtables));
			if (tx == Value::type_t::null)
				return Value::type_t::null;
			if (tx == Value::type_t::integer && t == Value::type_t::floatingpoint)
				continue;
			if (tx == Value::type_t::floatingpoint && t == Value::type_t::integer) {
				t = tx;
				continue;
			}
			if (t != Value::type_t::null && t != tx)
				return Value::type_t::null;
			t = tx;
		}
		return t;
	}

	case func_t::nullif:
		if (m_expr.size() != 1 || !m_expr[0])
			return Value::type_t::null;
		return m_expr[0]->calculate_type(subtables);

		// array
	case func_t::array:
	case func_t::string_to_array:
		return Value::type_t::array;

	case func_t::array_to_string:
		return Value::type_t::string;

	case func_t::array_length:
		return Value::type_t::integer;

	case func_t::overlap:
	case func_t::st_dwithin:
		return Value::type_t::boolean;

	case func_t::st_pointonsurface:
		return Value::type_t::points;

	default:
		return Value::type_t::null;
	}
	return Value::type_t::null;
}

const std::string& to_str(CartoSQL::ExprFuncAggregate::func_t t)
{
	switch (t) {
	// aggregate
	case CartoSQL::ExprFuncAggregate::func_t::countrows:
	{
		static const std::string r("count(*)");
		return r;
	}

	case CartoSQL::ExprFuncAggregate::func_t::count:
	{
		static const std::string r("count");
		return r;
	}

	case CartoSQL::ExprFuncAggregate::func_t::sum:
	{
		static const std::string r("sum");
		return r;
	}

	case CartoSQL::ExprFuncAggregate::func_t::avg:
	{
		static const std::string r("avg");
		return r;
	}

	case CartoSQL::ExprFuncAggregate::func_t::min:
	{
		static const std::string r("min");
		return r;
	}

	case CartoSQL::ExprFuncAggregate::func_t::max:
	{
		static const std::string r("max");
		return r;
	}

	case CartoSQL::ExprFuncAggregate::func_t::bit_and:
	{
		static const std::string r("bit_and");
		return r;
	}

	case CartoSQL::ExprFuncAggregate::func_t::bit_or:
	{
		static const std::string r("bit_or");
		return r;
	}

	case CartoSQL::ExprFuncAggregate::func_t::bool_and:
	{
		static const std::string r("bool_and");
		return r;
	}

	case CartoSQL::ExprFuncAggregate::func_t::bool_or:
	{
		static const std::string r("bool_or");
		return r;
	}

	case CartoSQL::ExprFuncAggregate::func_t::array_agg:
	{
		static const std::string r("array_agg");
		return r;
	}

	case CartoSQL::ExprFuncAggregate::func_t::string_agg:
	{
		static const std::string r("string_agg");
		return r;
	}

	default:
	{
		static const std::string r("?");
		return r;
	}
	}
}


bool CartoSQL::ExprFuncAggregate::calculate(Calc& c, Value& v, func_t func, const exprlist_t& e, bool distinct, const Expr *expr)
{
	if (!c.get_group_rows()) {
		v.set_null();
		return false;
	}
	c.set_group_row(0);
	switch (func) {
	case func_t::countrows:
		if (!e.empty()) {
			if (expr)
				c.error(*expr, "no arguments expected");
			return false;
		}
		v = Value(static_cast<int64_t>(c.get_group_rows()));
		return true;

	case func_t::count:
	{
		if (e.size() != 1) {
			if (expr)
				c.error(*expr, "one argument expected");
			return false;
		}
		if (!e[0])
			return true;
		int64_t count(0);
		for (Calc::grouprow_t row(0), nrows(c.get_group_rows()); row < nrows; ++row) {
			c.set_group_row(row);
			Value val;
			if (!e[0]->calculate(c, val)) {
				if (expr)
					c.error(*expr, "argument error");
				c.set_group_row(0);
				return false;
			}
			if (val.is_null())
				continue;
			++count;
		}
		v = Value(count);
		c.set_group_row(0);
		return true;
	}

	case func_t::sum:
	case func_t::avg:
	case func_t::bit_and:
	case func_t::bit_or:
	case func_t::bool_and:
	case func_t::bool_or:
	{
		if (e.size() != 1) {
			if (expr)
				c.error(*expr, "one argument expected");
			return false;
		}
		if (!e[0])
			return true;
		ExprFunc::func_t aggfunc(ExprFunc::func_t::binaryplus);
		switch (func) {
		default:
		case func_t::sum:
		case func_t::avg:
			aggfunc = ExprFunc::func_t::binaryplus;
			break;

		case func_t::bit_and:
			aggfunc = ExprFunc::func_t::bitwiseand;
			break;

		case func_t::bit_or:
			aggfunc = ExprFunc::func_t::bitwiseor;
			break;

		case func_t::bool_and:
			aggfunc = ExprFunc::func_t::logicaland;
			break;

		case func_t::bool_or:
			aggfunc = ExprFunc::func_t::logicalor;
			break;
		}
		ExprFunc::valuelist_t vl;
		vl.resize(2);
		int64_t count(0);
		for (Calc::grouprow_t row(0), nrows(c.get_group_rows()); row < nrows; ++row) {
			c.set_group_row(row);
			if (!e[0]->calculate(c, vl[1])) {
				if (expr)
					c.error(*expr, "argument error");
				c.set_group_row(0);
				return false;
			}
			if (vl[1].is_null())
				continue;
			if (count) {
				Value acc;
				if (!ExprFunc::calculate(acc, aggfunc, vl)) {
					if (expr)
						c.error(*expr, "aggregation error");
					c.set_group_row(0);
					return false;
				}
				vl[0] = acc;
			} else {
				vl[0] = vl[1];
			}
			++count;
		}
		c.set_group_row(0);
		if (func == func_t::avg) {
			vl[1] = static_cast<double>(count);
			if (!ExprFunc::calculate(v, ExprFunc::func_t::binarydiv, vl)) {
				if (expr)
					c.error(*expr, "divide error");
				return false;
			}
		} else {
			v = vl[0];
		}
		return true;
	}

	case func_t::min:
	case func_t::max:
	{
		if (e.size() != 1) {
			if (expr)
				c.error(*expr, "one argument expected");
			return false;
		}
		if (!e[0])
			return true;
		ExprFunc::func_t cmpop((func == func_t::max) ? ExprFunc::func_t::lessthan : ExprFunc::func_t::greaterthan);
		ExprFunc::valuelist_t vl;
		vl.resize(2);
		int64_t count(0);
		for (Calc::grouprow_t row(0), nrows(c.get_group_rows()); row < nrows; ++row) {
			c.set_group_row(row);
			if (!e[0]->calculate(c, vl[1])) {
				if (expr)
					c.error(*expr, "argument error");
				c.set_group_row(0);
				return false;
			}
			if (vl[1].is_null())
				continue;
			Value cmp(true);
			if (count) {
				if (!ExprFunc::calculate(cmp, cmpop, vl)) {
					if (expr)
						c.error(*expr, "comparison error");
					c.set_group_row(0);
					return false;
				}
				if (!cmp.is_boolean()) {
					if (expr)
						c.error(*expr, "comparison returned non-boolean");
					c.set_group_row(0);
					return false;
				}
			}
			if (cmp.get_boolean())
				vl[0] = vl[1];
			++count;
		}
		c.set_group_row(0);
		v = vl[0];
		return true;
	}

	case func_t::array_agg:
	{
		if (e.size() != 1) {
			if (expr)
				c.error(*expr, "one argument expected");
			return false;
		}
		if (!e[0])
			return true;
		Value::array_t array;
		for (Calc::grouprow_t row(0), nrows(c.get_group_rows()); row < nrows; ++row) {
			c.set_group_row(row);
			array.push_back(Value());
			if (!e[0]->calculate(c, array.back())) {
				if (expr)
					c.error(*expr, "argument error");
				c.set_group_row(0);
				return false;
			}
			if (array.back().is_null())
				array.resize(array.size() - 1);
		}
		v = std::move(array);
		c.set_group_row(0);
		return true;
	}

	case func_t::string_agg:
	{
		if (e.size() < 1 || e.size() > 2) {
			if (expr)
				c.error(*expr, "one or two arguments expected");
			return false;
		}
		if (!e[0])
			return true;
		std::string delim;
		if (e.size() >= 2 && e[1]) {
			Value del;
			if (!e[1]->calculate(c, del)) {
				if (expr)
					c.error(*expr, "delimiter argument error");
				return false;
			}
			if (!del.is_null()) {
				del = del.as_string();
				if (!del.is_string()) {
					if (expr)
					c.error(*expr, "delimiter is not a string");
					return false;
				}
				delim = del.get_string();
			}
		}
		std::string str;
		bool subseq(false);
		for (Calc::grouprow_t row(0), nrows(c.get_group_rows()); row < nrows; ++row) {
			c.set_group_row(row);
			Value val;
			if (!e[0]->calculate(c, val)) {
				if (expr)
					c.error(*expr, "argument error");
				c.set_group_row(0);
				return false;
			}
			if (val.is_null())
				continue;
			val = val.as_string();
			if (!val.is_string()) {
				if (expr)
					c.error(*expr, "argument is not a string");
				c.set_group_row(0);
				return false;
			}
			if (subseq)
				str.append(delim);
			subseq = true;
			str.append(val.get_string());
		}
		v = std::move(str);
		c.set_group_row(0);
		return true;
	}

	default:
		if (expr)
			c.error(*expr, "not implemented");
		return false;
	}
}

CartoSQL::Value::type_t CartoSQL::ExprFuncAggregate::calculate_type(const subtables_t& subtables) const
{
	switch (m_func) {
	case func_t::countrows:
	case func_t::count:
		return Value::type_t::integer;

	case func_t::bool_and:
	case func_t::bool_or:
		return Value::type_t::boolean;

	case func_t::bit_and:
	case func_t::bit_or:
		return Value::type_t::integer;

	case func_t::avg:
		return Value::type_t::floatingpoint;

	case func_t::sum:
	case func_t::min:
	case func_t::max:
	{
		if (m_expr.size() != 1 || !m_expr[0])
			return Value::type_t::null;
		Value::type_t t(m_expr[0]->calculate_type(subtables));
		if (t == Value::type_t::floatingpoint || t == Value::type_t::integer)
			return t;
		return Value::type_t::null;
	}

	case func_t::array_agg:
		return Value::type_t::array;

	case func_t::string_agg:
		return Value::type_t::string;

	default:
		return Value::type_t::null;
	}
}

CartoSQL::Expr::const_ptr_t CartoSQL::ExprFuncAggregate::simplify_tail(const CartoSQL *csql, func_t func, const exprlist_t& expr, bool distinct, bool modif)
{
	{
		Calculate c(csql);
		Value v;
		if (calculate(c, v, func, expr, distinct, nullptr))
			return ptr_t(new ExprValue(v));
	}
	if (modif)
		return ptr_t(new ExprFuncAggregate(func, expr, distinct));
	return ptr_t();
}

template <typename... Args>
CartoSQL::Expr::const_ptr_t CartoSQL::ExprFuncAggregate::simplify_int(const CartoSQL *csql, Args&&... args) const
{
	bool work(false);
	exprlist_t e;
	e.reserve(m_expr.size());
	for (const const_ptr_t& p : m_expr) {
		const_ptr_t p1(p->simplify(csql, std::forward<Args>(args)...));
		if (p1)
			work = true;
		else
			p1 = p;
		e.push_back(p1);
	}
	return simplify_tail(csql, m_func, e, m_distinct, work);
}

CartoSQL::Expr::const_ptr_t CartoSQL::ExprFuncAggregate::simplify(const CartoSQL *csql, Visitor& v) const
{
	v.visit(*this);
	const_ptr_t p;
	if (v.descend(*this))
		p = simplify_int(csql, v);
	const_ptr_t p1(p ? p->visitor_call_modify(csql, v) : visitor_call_modify(csql, v));
	v.visitend(*this);
	return p1 ? p1 : p;
}

void CartoSQL::ExprFuncAggregate::visit(Visitor& v) const
{
	v.visit(*this);
	if (v.descend(*this))
		for (const const_ptr_t& p : m_expr)
			p->visit(v);
	v.visitend(*this);
}

std::ostream& CartoSQL::ExprFuncAggregate::print(std::ostream& os, unsigned int indent, const PrintContext& ctx) const
{
	os << m_func;
	if (m_func == func_t::countrows)
		return os;
	os << '(';
	if (m_distinct)
		os << "distinct ";
	bool subseq(false);
	for (const auto& e : m_expr) {
		if (subseq)
			os << ", ";
		subseq = true;
		print_expr(os, e, indent, ctx, false);
	}
	return os << ')';
}

int CartoSQL::ExprFuncAggregate::compare(const Expr& x) const
{
	int c(Expr::compare(x));
	if (c)
		return c;
	const ExprFuncAggregate& xx(static_cast<const ExprFuncAggregate&>(x));
	if (m_func < xx.m_func)
		return -1;
	if (xx.m_func < m_func)
		return 1;
	c = Expr::compare(m_expr, xx.m_expr);
	if (c)
		return c;
	if (m_distinct < xx.m_distinct)
		return -1;
	if (xx.m_distinct < m_distinct)
		return 1;
	return 0;
}

const std::string& to_str(CartoSQL::ExprFuncTable::func_t t)
{
	switch (t) {
	case CartoSQL::ExprFuncTable::func_t::scalar:
	{
		static const std::string r("scalar");
		return r;
	}

	case CartoSQL::ExprFuncTable::func_t::exists:
	{
		static const std::string r("exists");
		return r;
	}

	case CartoSQL::ExprFuncTable::func_t::notexists:
	{
		static const std::string r("not exists");
		return r;
	}

	default:
	{
		static const std::string r("?");
		return r;
	}
	}
}

bool CartoSQL::ExprFuncTable::calculate(Calc& c, Value& v, func_t func, const tblexpr_t& expr, const Expr *fexpr)
{
	v.set_null();
	if (!expr)
		return true;
	Table tbl;
	if (!expr->calculate(c, tbl)) {
		if (fexpr)
			c.error(*fexpr, "argument error");
		return false;
	}
	switch (func) {
	case func_t::scalar:
		switch (tbl.size()) {
		case 0:
			return true;

		case 1:
		{
			const TableRow& r(tbl.front());
			switch (r.size()) {
			case 0:
				return true;

			case 1:
				v = r.front();
				return true;

			default:
				v = static_cast<Value::array_t>(r);
				return true;
			}
		}

		default:
			if (fexpr)
				c.error(*fexpr, "table has multiple rows");
			return false;
		}

	case func_t::exists:
		v = Value(!tbl.empty());
		return true;

	case func_t::notexists:
		v = Value(tbl.empty());
		return true;

	default:
		if (fexpr)
			c.error(*fexpr, "not implemented");
		return false;
	}
}

CartoSQL::Value::type_t CartoSQL::ExprFuncTable::calculate_type(const subtables_t& subtables) const
{
	if (!m_expr || m_expr->get_columns().empty())
		return Value::type_t::null;
	return m_expr->get_columns().front().get_type();
}

CartoSQL::Expr::const_ptr_t CartoSQL::ExprFuncTable::simplify_tail(const CartoSQL *csql, func_t func, const tblexpr_t& expr, bool modif)
{
	{
		Calculate c(csql);
		Value v;
		if (calculate(c, v, func, expr, nullptr))
			return ptr_t(new ExprValue(v));
	}
	if (modif)
		return Expr::const_ptr_t(new ExprFuncTable(func, expr));
	return Expr::const_ptr_t();
}

template <typename... Args>
CartoSQL::Expr::const_ptr_t CartoSQL::ExprFuncTable::simplify_int(const CartoSQL *csql, Args&&... args) const
{
	bool work(false);
	tblexpr_t expr;
	if (m_expr) {
		expr = m_expr->simplify(csql, std::forward<Args>(args)...);
		if (expr)
			work = true;
		else
			expr = m_expr;
	}
	return simplify_tail(csql, m_func, expr, work);
}

CartoSQL::Expr::const_ptr_t CartoSQL::ExprFuncTable::simplify(const CartoSQL *csql, Visitor& v) const
{
	v.visit(*this);
	const_ptr_t p;
	if (v.descend(*this))
		p = simplify_int(csql, v);
	const_ptr_t p1(p ? p->visitor_call_modify(csql, v) : visitor_call_modify(csql, v));
	v.visitend(*this);
	return p1 ? p1 : p;
}

void CartoSQL::ExprFuncTable::visit(Visitor& v) const
{
	v.visit(*this);
	if (v.descend(*this))
		if (m_expr)
			m_expr->visit(v);
	v.visitend(*this);
}

std::ostream& CartoSQL::ExprFuncTable::print(std::ostream& os, unsigned int indent, const PrintContext& ctx) const
{
	if (get_func() != func_t::scalar)
		os << get_func() << ' ';
	return ExprTable::print_expr(os << '(', get_expr(), indent, ctx) << ')';
}

int CartoSQL::ExprFuncTable::compare(const Expr& x) const
{
	int c(Expr::compare(x));
	if (c)
		return c;
	const ExprFuncTable& xx(static_cast<const ExprFuncTable&>(x));
	if (get_func() < xx.get_func())
		return -1;
	if (xx.get_func() < get_func())
		return 1;
	c = ExprTable::compare(get_expr(), xx.get_expr());
	if (c)
		return c;
	return 0;
}
