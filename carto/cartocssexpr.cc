//
// C++ Implementation: cartocss
//
// Description: CartoCSS expressions
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glibmm.h>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cmath>

#include "cartocss.h"
#include "hibernate.h"

CartoCSS::Expr::ExprError::ExprError(const std::string& x)
	: std::runtime_error(x)
{
}

class CartoCSS::ExprNull : public Expr {
public:
	ExprNull(void) {}
	virtual type_t get_type(void) const { return type_t::null; }
	virtual bool is_const(void) const { return true; }
	virtual bool is_type(void) const { return true; }
	virtual std::string to_str(void) const { return "null"; }
	virtual void load(HibernateReadStream& ar) { hibernate(ar); }
	virtual void load(HibernateReadBuffer& ar) { hibernate(ar); }
	virtual void save(HibernateWriteStream& ar) const { const_cast<ExprNull *>(this)->hibernate(ar); }

private:
	template<class Archive> void hibernate(Archive& ar) {
		Expr::hibernate(ar);
	}
};

class CartoCSS::ExprKeyword : public Expr {
public:
	ExprKeyword(keyword_t v) : m_value(v) {}
	virtual type_t get_type(void) const { return type_t::keyword; }
	virtual bool is_const(void) const { return true; }
	virtual bool is_type(keyword_t& v) const { v = m_value; return true; }
	virtual std::string to_str(void) const { return ::to_str(m_value); }
	virtual void load(HibernateReadStream& ar) { hibernate(ar); }
	virtual void load(HibernateReadBuffer& ar) { hibernate(ar); }
	virtual void save(HibernateWriteStream& ar) const { const_cast<ExprKeyword *>(this)->hibernate(ar); }

protected:
	keyword_t m_value;

private:
	template<class Archive> void hibernate(Archive& ar) {
		Expr::hibernate(ar);
		ar.ioenum(m_value);
	}
};

class CartoCSS::ExprCompositeOp : public Expr {
public:
	ExprCompositeOp(compositeop_t v) : m_value(v) {}
	virtual type_t get_type(void) const { return type_t::compositeop; }
	virtual bool is_const(void) const { return true; }
	virtual bool is_type(compositeop_t& v) const { v = m_value; return true; }
	virtual std::string to_str(void) const { return ::to_str(m_value); }
	virtual void load(HibernateReadStream& ar) { hibernate(ar); }
	virtual void load(HibernateReadBuffer& ar) { hibernate(ar); }
	virtual void save(HibernateWriteStream& ar) const { const_cast<ExprCompositeOp *>(this)->hibernate(ar); }

protected:
	compositeop_t m_value;

private:
	template<class Archive> void hibernate(Archive& ar) {
		Expr::hibernate(ar);
		ar.ioenum(m_value);
	}
};

class CartoCSS::ExprLayerID : public Expr {
public:
	ExprLayerID(const LayerID& v) : m_value(v) {}
	virtual type_t get_type(void) const { return type_t::layerid; }
	virtual bool is_const(void) const { return true; }
	virtual bool is_type(LayerID& v) const { v = m_value; return true; }
	virtual const_ptr_t simplify(const LayerID& layer) const { return create(m_value == layer); }
	virtual std::string to_str(void) const { return "#" + m_value; }
	virtual void load(HibernateReadStream& ar) { hibernate(ar); }
	virtual void load(HibernateReadBuffer& ar) { hibernate(ar); }
	virtual void save(HibernateWriteStream& ar) const { const_cast<ExprLayerID *>(this)->hibernate(ar); }

protected:
	LayerID m_value;

private:
	template<class Archive> void hibernate(Archive& ar) {
		Expr::hibernate(ar);
		ar.io(static_cast<std::string&>(m_value));
	}
};

class CartoCSS::ExprClassID : public Expr {
public:
	ExprClassID(const ClassID& v) : m_value(v) {}
	virtual type_t get_type(void) const { return type_t::classid; }
	virtual bool is_const(void) const { return true; }
	virtual bool is_type(ClassID& v) const { v = m_value; return true; }
	virtual const_ptr_t simplify(const ClassID& cls) const { return create(m_value == cls); }
	virtual std::string to_str(void) const { return "." + m_value; }
	virtual void load(HibernateReadStream& ar) { hibernate(ar); }
	virtual void load(HibernateReadBuffer& ar) { hibernate(ar); }
	virtual void save(HibernateWriteStream& ar) const { const_cast<ExprClassID *>(this)->hibernate(ar); }

protected:
	ClassID m_value;

private:
	template<class Archive> void hibernate(Archive& ar) {
		Expr::hibernate(ar);
		ar.io(static_cast<std::string&>(m_value));
	}
};

class CartoCSS::ExprBool : public Expr {
public:
	ExprBool(bool v) : m_value(v) {}
	virtual type_t get_type(void) const { return type_t::boolean; }
	virtual bool is_const(void) const { return true; }
	virtual bool is_type(bool& v) const { v = m_value; return true; }
	virtual std::string to_str(void) const { return m_value ? "true" : "false"; }
	virtual void load(HibernateReadStream& ar) { hibernate(ar); }
	virtual void load(HibernateReadBuffer& ar) { hibernate(ar); }
	virtual void save(HibernateWriteStream& ar) const { const_cast<ExprBool *>(this)->hibernate(ar); }

protected:
	bool m_value;

private:
	template<class Archive> void hibernate(Archive& ar) {
		Expr::hibernate(ar);
		ar.iouint8(m_value);
	}
};

class CartoCSS::ExprInt : public Expr {
public:
	ExprInt(int64_t v) : m_value(v) {}
	virtual type_t get_type(void) const { return type_t::integer; }
	virtual bool is_const(void) const { return true; }
	virtual bool is_type(int64_t& v) const { v = m_value; return true; }
	virtual std::string to_str(void) const { std::ostringstream oss; oss << m_value; return oss.str(); }
	virtual void load(HibernateReadStream& ar) { hibernate(ar); }
	virtual void load(HibernateReadBuffer& ar) { hibernate(ar); }
	virtual void save(HibernateWriteStream& ar) const { const_cast<ExprInt *>(this)->hibernate(ar); }

protected:
	int64_t m_value;

private:
	template<class Archive> void hibernate(Archive& ar) {
		Expr::hibernate(ar);
		ar.ioleb(m_value);
	}
};

class CartoCSS::ExprDouble : public Expr {
public:
	ExprDouble(double v) : m_value(v) {}
	virtual type_t get_type(void) const { return type_t::floatingpoint; }
	virtual bool is_const(void) const { return true; }
	virtual bool is_type(double& v) const { v = m_value; return true; }
	virtual std::string to_str(void) const { std::ostringstream oss; oss << m_value; return oss.str(); }
	virtual void load(HibernateReadStream& ar) { hibernate(ar); }
	virtual void load(HibernateReadBuffer& ar) { hibernate(ar); }
	virtual void save(HibernateWriteStream& ar) const { const_cast<ExprDouble *>(this)->hibernate(ar); }

protected:
	double m_value;

private:
	template<class Archive> void hibernate(Archive& ar) {
		Expr::hibernate(ar);
		ar.io(m_value);
	}
};

class CartoCSS::ExprString : public Expr {
public:
	ExprString(std::string v) : m_value(v) {}
	virtual type_t get_type(void) const { return type_t::string; }
	virtual bool is_const(void) const { return true; }
	virtual bool is_type(std::string& v) const { v = m_value; return true; }
	virtual std::string to_str(void) const;
	virtual void load(HibernateReadStream& ar) { hibernate(ar); }
	virtual void load(HibernateReadBuffer& ar) { hibernate(ar); }
	virtual void save(HibernateWriteStream& ar) const { const_cast<ExprString *>(this)->hibernate(ar); }

protected:
	std::string m_value;

private:
	template<class Archive> void hibernate(Archive& ar) {
		Expr::hibernate(ar);
		ar.io(m_value);
	}
};

std::string CartoCSS::ExprString::to_str(void) const
{
	std::string r;
	r.push_back('"');
	for (char ch : m_value) {
		if (ch >= ' ' && ch < 0x7f) {
			r.push_back(ch);
			continue;
		}
		r.push_back('\\');
		if (ch == '\n') {
			r.push_back('n');
			continue;
		}
		if (ch == '\r') {
			r.push_back('r');
			continue;
		}
		if (ch == '\t') {
			r.push_back('t');
			continue;
		}
		std::ostringstream oss;
		oss << std::hex << std::setw(2) << std::setfill('0') << (ch & 0xff);
		r.append(oss.str());
	}
	r.push_back('"');
	return r;
}

class CartoCSS::ExprColor : public Expr {
public:
	ExprColor(const Color& v) : m_value(v) {}
	virtual type_t get_type(void) const { return type_t::color; }
	virtual bool is_const(void) const { return true; }
	virtual bool is_type(Color& v) const { v = m_value; return true; }
	virtual std::string to_str(void) const;
	virtual void load(HibernateReadStream& ar) { hibernate(ar); }
	virtual void load(HibernateReadBuffer& ar) { hibernate(ar); }
	virtual void save(HibernateWriteStream& ar) const { const_cast<ExprColor *>(this)->hibernate(ar); }

protected:
	Color m_value;

private:
	template<class Archive> void hibernate(Archive& ar) {
		Expr::hibernate(ar);
		m_value.hibernate(ar);
	}
};

std::string CartoCSS::ExprColor::to_str(void) const
{
	std::ostringstream oss;
	oss << "rgba(" << m_value.get_red() << ',' << m_value.get_green() << ',' << m_value.get_blue() << ',' << m_value.get_alpha() << ')';
	return oss.str();
}

class CartoCSS::ExprURL : public Expr {
public:
	ExprURL(std::string v) : m_value(v) {}
	virtual type_t get_type(void) const { return type_t::url; }
	virtual bool is_const(void) const { return true; }
	virtual bool is_url(std::string& v) const { v = m_value; return true; }
	virtual std::string to_str(void) const;
	virtual void load(HibernateReadStream& ar) { hibernate(ar); }
	virtual void load(HibernateReadBuffer& ar) { hibernate(ar); }
	virtual void save(HibernateWriteStream& ar) const { const_cast<ExprURL *>(this)->hibernate(ar); }

protected:
	std::string m_value;

private:
	template<class Archive> void hibernate(Archive& ar) {
		Expr::hibernate(ar);
		ar.io(m_value);
	}
};

std::string CartoCSS::ExprURL::to_str(void) const
{
	std::string r("url(\"");
	r.append(m_value);
	r.append("\")");
	return r;
}

class CartoCSS::ExprDirectory : public Expr {
public:
	ExprDirectory(std::string v) : m_value(v) {}
	virtual type_t get_type(void) const { return type_t::directory; }
	virtual bool is_const(void) const { return true; }
	virtual bool is_directory(std::string& v) const { v = m_value; return true; }
	virtual std::string to_str(void) const;
	virtual void load(HibernateReadStream& ar) { hibernate(ar); }
	virtual void load(HibernateReadBuffer& ar) { hibernate(ar); }
	virtual void save(HibernateWriteStream& ar) const { const_cast<ExprDirectory *>(this)->hibernate(ar); }

protected:
	std::string m_value;

private:
	template<class Archive> void hibernate(Archive& ar) {
		Expr::hibernate(ar);
		ar.io(m_value);
	}
};

std::string CartoCSS::ExprDirectory::to_str(void) const
{
	std::string r("dir(\"");
	r.append(m_value);
	r.append("\")");
	return r;
}

class CartoCSS::ExprVarRef : public Expr {
public:
	ExprVarRef(std::string v) : m_value(v) {}
	virtual type_t get_type(void) const { return type_t::varref; }
	virtual bool is_varref(std::string& v) const { v = m_value; return true; }
	virtual const_ptr_t simplify(const std::string& var, const const_ptr_t& expr) const;
	virtual const_ptr_t simplify(const variables_t& vars) const;
	virtual std::string to_str(void) const { return "@" + m_value; }
	virtual void load(HibernateReadStream& ar) { hibernate(ar); }
	virtual void load(HibernateReadBuffer& ar) { hibernate(ar); }
	virtual void save(HibernateWriteStream& ar) const { const_cast<ExprVarRef *>(this)->hibernate(ar); }

protected:
	std::string m_value;

private:
	template<class Archive> void hibernate(Archive& ar) {
		Expr::hibernate(ar);
		ar.io(m_value);
	}
};

CartoCSS::Expr::const_ptr_t CartoCSS::ExprVarRef::simplify(const std::string& var, const const_ptr_t& expr) const
{
	if (m_value == var)
		return expr;
	return ptr_t();
}

CartoCSS::Expr::const_ptr_t CartoCSS::ExprVarRef::simplify(const variables_t& vars) const
{
	variables_t::const_iterator i(vars.find(m_value));
	if (i == vars.end())
		return ptr_t();
	return i->second;
}

class CartoCSS::ExprField : public Expr {
public:
	ExprField(std::string v) : m_value(v) {}
	virtual type_t get_type(void) const { return type_t::field; }
	virtual bool is_field(std::string& v) const { v = m_value; return true; }
	virtual const_ptr_t simplify(const std::string& field, const Value& value) const;
	virtual const_ptr_t simplify(const Fields& fields) const;
	virtual std::string to_str(void) const { return "[" + m_value + "]"; }
	virtual void load(HibernateReadStream& ar) { hibernate(ar); }
	virtual void load(HibernateReadBuffer& ar) { hibernate(ar); }
	virtual void save(HibernateWriteStream& ar) const { const_cast<ExprField *>(this)->hibernate(ar); }

protected:
	std::string m_value;

private:
	template<class Archive> void hibernate(Archive& ar) {
		Expr::hibernate(ar);
		ar.io(m_value);
	}
};

CartoCSS::Expr::const_ptr_t CartoCSS::ExprField::simplify(const std::string& field, const Value& value) const
{
	if (m_value != field)
		return ptr_t();
	return value.get_expr();
}

CartoCSS::Expr::const_ptr_t CartoCSS::ExprField::simplify(const Fields& fields) const
{
	Fields::const_iterator i(fields.find(m_value));
	if (i == fields.end())
		return ptr_t();
	return i->second.get_expr();
}

class CartoCSS::ExprCopyLayer : public Expr {
public:
	ExprCopyLayer(std::string v) : m_value(v) {}
	virtual type_t get_type(void) const { return type_t::copylayer; }
	virtual bool is_const(void) const { return true; }
	virtual bool is_copylayer(std::string& v) const { v = m_value; return true; }
	virtual std::string to_str(void) const { return "::" + m_value; }
	virtual void load(HibernateReadStream& ar) { hibernate(ar); }
	virtual void load(HibernateReadBuffer& ar) { hibernate(ar); }
	virtual void save(HibernateWriteStream& ar) const { const_cast<ExprCopyLayer *>(this)->hibernate(ar); }

protected:
	std::string m_value;

private:
	template<class Archive> void hibernate(Archive& ar) {
		Expr::hibernate(ar);
		ar.io(m_value);
	}
};

class CartoCSS::ExprList : public Expr {
public:
	ExprList(const exprlist_t& v) : m_value(v) {}
	virtual type_t get_type(void) const { return type_t::list; }
	static bool is_const(const exprlist_t& e);
	virtual bool is_const(void) const { return is_const(m_value); }
	virtual bool is_type(exprlist_t& v) const { v = m_value; return true; }
	virtual const_ptr_t simplify(void) const { return simplify_int(); }
	virtual const_ptr_t simplify(const std::string& field, const Value& value) const { return simplify_int(field, value); }
	virtual const_ptr_t simplify(const Fields& fields) const { return simplify_int(fields); }
	virtual const_ptr_t simplify(const std::string& var, const const_ptr_t& expr) const { return simplify_int(var, expr); }
	virtual const_ptr_t simplify(const variables_t& vars) const { return simplify_int(vars); }
	virtual const_ptr_t simplify(const LayerID& layer) const { return simplify_int(layer); }
	virtual const_ptr_t simplify(const ClassID& cls) const { return simplify_int(cls); }
	virtual const_ptr_t simplify(Visitor& v) const;
	virtual void visit(Visitor& v) const;
	virtual std::string to_str(void) const;
	virtual void load(HibernateReadStream& ar) { hibernate_load(ar); }
	virtual void load(HibernateReadBuffer& ar) { hibernate_load(ar); }
	virtual void save(HibernateWriteStream& ar) const { const_cast<ExprList *>(this)->hibernate_save(ar); }

protected:
	exprlist_t m_value;
	static bool simplify_listoflist(exprlist_t& e);
	template <typename... Args> const_ptr_t simplify_int(Args&&... args) const;

private:
	template<class Archive> void hibernate_load(Archive& ar) {
		Expr::hibernate(ar);
		exprlist_t::size_type sz(m_value.size());
		ar.ioleb(sz);
		m_value.resize(sz);
		for (typename exprlist_t::iterator i(m_value.begin()), e(m_value.end()); i != e; ++i)
			boost::const_pointer_cast<Expr>(*i)->load(ar);
	}

	template<class Archive> void hibernate_save(Archive& ar) {
		Expr::hibernate(ar);
		exprlist_t::size_type sz(m_value.size());
		ar.ioleb(sz);
		for (typename exprlist_t::const_iterator i(m_value.begin()), e(m_value.end()); i != e; ++i)
			(*i)->save(ar);
	}
};

bool CartoCSS::ExprList::is_const(const exprlist_t& e)
{
	for (const auto& p : e)
		if (!p->is_const())
			return false;
	return true;
}

void CartoCSS::ExprList::visit(Visitor& v) const
{
	v.visit(*this);
	for (const auto& p : m_value)
		p->visit(v);
	v.visitend(*this);
}

CartoCSS::Expr::const_ptr_t CartoCSS::ExprList::simplify(Visitor& v) const
{
	const_ptr_t p(simplify_int(v));
	if (!p)
		return v.modify(*this);
	const_ptr_t p1(v.modify(*p));
	return p1 ? p1 : p;
}

bool CartoCSS::ExprList::simplify_listoflist(exprlist_t& e)
{
	bool work(false);
	for (exprlist_t::iterator i(e.begin()); i != e.end(); ) {
		exprlist_t e1;
		if (!(*i)->is_type(e1)) {
			++i;
			continue;
		}
		i = e.erase(i);
		i = e.insert(i, e1.begin(), e1.end());
		work = true;
	}
	return work;
}

template <typename... Args>
CartoCSS::Expr::const_ptr_t CartoCSS::ExprList::simplify_int(Args&&... args) const
{
	bool work(false);
	exprlist_t e;
	e.reserve(m_value.size());
	for (const const_ptr_t& p : m_value) {
		const_ptr_t p1(p->simplify(std::forward<Args>(args)...));
		if (p1)
			work = true;
		else
			p1 = p;
		e.push_back(p1);
	}
	if (simplify_listoflist(e) || work)
		return ptr_t(new ExprList(e));
	return ptr_t();
}

std::string CartoCSS::ExprList::to_str(void) const
{
	std::string r;
	for (const const_ptr_t& p : m_value) {
		if (!r.empty())
			r.push_back(',');
		r.append(p->to_str());
	}
	return r;
}

class CartoCSS::ExprFunction : public Expr {
public:
	ExprFunction(func_t func, const const_ptr_t& args) : m_args(args), m_func(func) {}
	virtual type_t get_type(void) const { return type_t::function; }
	virtual bool is_const(void) const { return m_args->is_const(); }
	virtual bool is_type(func_t& func, Expr::const_ptr_t& args) const { func = m_func; args = m_args; return true; }
	virtual const_ptr_t simplify(void) const { return simplify_int(); }
	virtual const_ptr_t simplify(const std::string& field, const Value& value) const { return simplify_int(field, value); }
	virtual const_ptr_t simplify(const Fields& fields) const { return simplify_int(fields); }
	virtual const_ptr_t simplify(const std::string& var, const const_ptr_t& expr) const { return simplify_int(var, expr); }
	virtual const_ptr_t simplify(const variables_t& vars) const { return simplify_int(vars); }
	virtual const_ptr_t simplify(const LayerID& layer) const { return simplify_int(layer); }
	virtual const_ptr_t simplify(const ClassID& cls) const { return simplify_int(cls); }
	virtual const_ptr_t simplify(Visitor& v) const;
	virtual void visit(Visitor& v) const;
	virtual std::string to_str(void) const;
	virtual void load(HibernateReadStream& ar) { hibernate_load(ar); }
	virtual void load(HibernateReadBuffer& ar) { hibernate_load(ar); }
	virtual void save(HibernateWriteStream& ar) const { const_cast<ExprFunction *>(this)->hibernate_save(ar); }

protected:
	const_ptr_t m_args;
	func_t m_func;

	template <typename... Args> const_ptr_t simplify_int(Args&&... args) const;
	const_ptr_t simplify_tail(const const_ptr_t& args) const;

private:
	template<class Archive> void hibernate_load(Archive& ar) {
		Expr::hibernate(ar);
		ar.ioenum(m_func);
		boost::const_pointer_cast<Expr>(m_args)->load(ar);
	}

	template<class Archive> void hibernate_save(Archive& ar) {
		Expr::hibernate(ar);
		ar.ioenum(m_func);
		m_args->save(ar);
	}
};

void CartoCSS::ExprFunction::visit(Visitor& v) const
{
	v.visit(*this);
	m_args->visit(v);
	v.visitend(*this);
}

CartoCSS::Expr::const_ptr_t CartoCSS::ExprFunction::simplify(Visitor& v) const
{
	const_ptr_t p(simplify_int(v));
	if (!p)
		return v.modify(*this);
	const_ptr_t p1(v.modify(*p));
	return p1 ? p1 : p;	
}

template <typename... Args>
CartoCSS::Expr::const_ptr_t CartoCSS::ExprFunction::simplify_int(Args&&... args) const
{
	return simplify_tail(m_args->simplify(std::forward<Args>(args)...));
}

CartoCSS::Expr::const_ptr_t CartoCSS::ExprFunction::simplify_tail(const const_ptr_t& args) const
{
	// FIXME FIXME FIXME FIXME
	exprlist_t argl;
	if (args) {
		if (!args->is_convertible(argl) || (m_func != func_t::plus && m_func != func_t::url && !ExprList::is_const(argl)))
			return ptr_t(new ExprFunction(m_func, args));
	} else {
		if (!m_args->is_convertible(argl) || (m_func != func_t::plus && m_func != func_t::url && !ExprList::is_const(argl)))
			return ptr_t();
	}
	switch (m_func) {
	// 1 function argument
	// 1 or 2 function arguments
	case func_t::url:
	{
		if (argl.size() < 1 || argl.size() > 2)
			break;
		std::string url;
		if (!argl[0]->is_convertible(url))
			break;
		if (argl.size() >= 2) {
			std::string dir;
			if (!argl[1]->is_directory(dir))
				break;
			if (!url.empty() && url.front() != '/')
				url = Glib::build_filename(dir, url);
		}
		return ptr_t(new ExprURL(url));
	}

	// 2 function arguments
	case func_t::collighten:
	{
		if (argl.size() != 2)
			break;
		Color col;
		double v;
		if (!argl[0]->is_type(col) || !argl[1]->is_convertible(v))
			break;
		double h, s, l;
		col.get_hsl(h, s, l);
		l += v;
		col.set_hsl(h, s, l);
		return ptr_t(new ExprColor(col));
	}

	case func_t::coldarken:
	{
		if (argl.size() != 2)
			break;
		Color col;
		double v;
		if (!argl[0]->is_type(col) || !argl[1]->is_convertible(v))
			break;
		double h, s, l;
		col.get_hsl(h, s, l);
		l -= v;
		col.set_hsl(h, s, l);
		return ptr_t(new ExprColor(col));
	}

	case func_t::colsaturate:
	{
		if (argl.size() != 2)
			break;
		Color col;
		double v;
		if (!argl[0]->is_type(col) || !argl[1]->is_convertible(v))
			break;
		double h, s, l;
		col.get_hsl(h, s, l);
		s += v;
		col.set_hsl(h, s, l);
		return ptr_t(new ExprColor(col));
	}

	case func_t::coldesaturate:
	{
		if (argl.size() != 2)
			break;
		Color col;
		double v;
		if (!argl[0]->is_type(col) || !argl[1]->is_convertible(v))
			break;
		double h, s, l;
		col.get_hsl(h, s, l);
		s -= v;
		col.set_hsl(h, s, l);
		return ptr_t(new ExprColor(col));
	}

	case func_t::colfadein:
       	{
		if (argl.size() != 2)
			break;
		Color col;
		double v;
		if (!argl[0]->is_type(col) || !argl[1]->is_convertible(v))
			break;
		col.set_alpha(std::min(col.get_alpha() + v, 1.0));
		return ptr_t(new ExprColor(col));
	}

	case func_t::colfadeout:
	{
		if (argl.size() != 2)
			break;
		Color col;
		double v;
		if (!argl[0]->is_type(col) || !argl[1]->is_convertible(v))
			break;
		col.set_alpha(std::max(col.get_alpha() - v, 0.0));
		return ptr_t(new ExprColor(col));
	}

	case func_t::colspin:
	{
		if (argl.size() != 2)
			break;
		Color col;
		double v;
		if (!argl[0]->is_type(col) || !argl[1]->is_convertible(v))
			break;
		double h, s, l;
		col.get_hsl(h, s, l);
		h += v;
		col.set_hsl(h, s, l);
		return ptr_t(new ExprColor(col));
	}

	case func_t::plus:
	{
		if (argl.size() != 2)
			break;
		// string addition
		{
			std::string v0, v1;
			if ((argl[0]->is_type(v0) && argl[1]->is_convertible(v1)) ||
			    (argl[1]->is_type(v1) && argl[0]->is_convertible(v0)))
				return ptr_t(new ExprString(v0 + v1));
		}
		// double addition
		{
			double v0, v1;
			if ((argl[0]->is_type(v0) && argl[1]->is_convertible(v1)) ||
			    (argl[1]->is_type(v1) && argl[0]->is_convertible(v0)))
				return ptr_t(new ExprDouble(v0 + v1));
		}
		// int addition
		{
			int64_t v0, v1;
			if ((argl[0]->is_type(v0) && argl[1]->is_convertible(v1)) ||
			    (argl[1]->is_type(v1) && argl[0]->is_convertible(v0)))
				return ptr_t(new ExprInt(v0 + v1));
		}
		break;
	}

	case func_t::minus:
	{
		if (argl.size() != 2)
			break;
		// double subtraction
		{
			double v0, v1;
			if ((argl[0]->is_type(v0) && argl[1]->is_convertible(v1)) ||
			    (argl[1]->is_type(v1) && argl[0]->is_convertible(v0)))
				return ptr_t(new ExprDouble(v0 - v1));
		}
		// int subtraction
		{
			int64_t v0, v1;
			if ((argl[0]->is_type(v0) && argl[1]->is_convertible(v1)) ||
			    (argl[1]->is_type(v1) && argl[0]->is_convertible(v0)))
				return ptr_t(new ExprInt(v0 - v1));
		}
		break;
	}

	case func_t::mul:
	{
		if (argl.size() != 2)
			break;
		// double multiplication
		{
			double v0, v1;
			if ((argl[0]->is_type(v0) && argl[1]->is_convertible(v1)) ||
			    (argl[1]->is_type(v1) && argl[0]->is_convertible(v0)))
				return ptr_t(new ExprDouble(v0 * v1));
		}
		// int multiplication
		{
			int64_t v0, v1;
			if ((argl[0]->is_type(v0) && argl[1]->is_convertible(v1)) ||
			    (argl[1]->is_type(v1) && argl[0]->is_convertible(v0)))
				return ptr_t(new ExprInt(v0 * v1));
		}
		break;
	}

	case func_t::div:
	{
		if (argl.size() != 2)
			break;
		// double division
		{
			double v0, v1;
			if ((argl[0]->is_type(v0) && argl[1]->is_convertible(v1)) ||
			    (argl[1]->is_type(v1) && argl[0]->is_convertible(v0)))
				return ptr_t(new ExprDouble(v0 / v1));
		}
		// int division
		{
			int64_t v0, v1;
			if ((argl[0]->is_type(v0) && argl[1]->is_convertible(v1)) ||
			    (argl[1]->is_type(v1) && argl[0]->is_convertible(v0)))
				return ptr_t(new ExprInt(v0 / v1));
		}
		break;
	}

	// 2 or 3 function arguments
	case func_t::colmix:
	{
		if (argl.size() < 2 || argl.size() > 3)
			break;
		Color col0, col1;
		if (!argl[0]->is_type(col0) || !argl[1]->is_type(col1))
			break;
		double m0(0.5);
		if (argl.size() >= 3 && !argl[2]->is_convertible(m0))
			break;
		double m1(1 - m0);
		col0.set_red(col0.get_red() * m0 + col1.get_red() * m1);
		col0.set_green(col0.get_green() * m0 + col1.get_green() * m1);
		col0.set_blue(col0.get_blue() * m0 + col1.get_blue() * m1);
		col0.set_alpha(col0.get_alpha() * m0 + col1.get_alpha() * m1);
		return ptr_t(new ExprColor(col0));
	}

	// 3 function arguments
	case func_t::colrgb:
	{
		double args[3];
		if (argl.size() != 3)
			break;
		bool ok(true);
		for (unsigned int i = 0; i < 3; ++i) {
			ok = argl[i]->is_convertible(args[i]);
			if (!ok)
				break;
		}
		if (ok)
			return ptr_t(new ExprColor(Color(args[0], args[1], args[2])));
		break;
	}

	case func_t::colhsl:
	{
		double args[3];
		if (argl.size() != 3)
			break;
		bool ok(true);
		for (unsigned int i = 0; i < 3; ++i) {
			ok = argl[i]->is_convertible(args[i]);
			if (!ok)
				break;
		}
		if (ok) {
			Color col(0, 0, 0, 1);
			col.set_hsl(args[0], args[1], args[2]);
			return ptr_t(new ExprColor(col));
		}
		break;
	}

	// 4 function arguments
	case func_t::colrgba:
	{
		double args[4];
		if (argl.size() != 4)
			break;
		bool ok(true);
		for (unsigned int i = 0; i < 4; ++i) {
			ok = argl[i]->is_convertible(args[i]);
			if (!ok)
				break;
		}
		if (ok)
			return ptr_t(new ExprColor(Color(args[0], args[1], args[2], args[3])));
		break;
	}

	case func_t::colhsla:
	{
		double args[4];
		if (argl.size() != 4)
			break;
		bool ok(true);
		for (unsigned int i = 0; i < 4; ++i) {
			ok = argl[i]->is_convertible(args[i]);
			if (!ok)
				break;
		}
		if (ok) {
			Color col(0, 0, 0, args[3]);
			col.set_hsl(args[0], args[1], args[2]);
			return ptr_t(new ExprColor(col));
		}
		break;
	}

	// 5 function arguments

	// 6 function arguments

	// 7 function arguments

	// 8 function arguments
	case func_t::scalehsla:
	{
		double args[8];
		if (argl.size() != 8)
			break;
		bool ok(true);
		for (unsigned int i = 0; i < 8; ++i) {
			ok = argl[i]->is_convertible(args[i]);
			if (!ok)
				break;
		}
		if (ok) {
			// FIXME
		}
		break;
	}

	default:
		break;
	}
	if (args)
		return ptr_t(new ExprFunction(m_func, args));
	return ptr_t();
}

std::string CartoCSS::ExprFunction::to_str(void) const
{
	std::string r(::to_str(m_func));
	r.push_back('(');
	r.append(m_args->to_str());
	r.push_back(')');
	return r;
}

class CartoCSS::ExprCompare : public Expr {
public:
	ExprCompare(const const_ptr_t& arg0, compop_t op, const const_ptr_t& arg1) : m_arg0(arg0), m_arg1(arg1), m_compop(op) {}
	virtual type_t get_type(void) const { return type_t::compare; }
	virtual bool is_const(void) const { return m_arg0->is_const() && m_arg1->is_const(); }
	virtual bool is_type(Expr::const_ptr_t& arg0, compop_t& op, Expr::const_ptr_t& arg1) const { arg0 = m_arg0; op = m_compop; arg1 = m_arg1; return true; }
	virtual const_ptr_t simplify(void) const { return simplify_int(); }
	virtual const_ptr_t simplify(const std::string& field, const Value& value) const { return simplify_int(field, value); }
	virtual const_ptr_t simplify(const Fields& fields) const { return simplify_int(fields); }
	virtual const_ptr_t simplify(const std::string& var, const const_ptr_t& expr) const { return simplify_int(var, expr); }
	virtual const_ptr_t simplify(const variables_t& vars) const { return simplify_int(vars); }
	virtual const_ptr_t simplify(const LayerID& layer) const { return simplify_int(layer); }
	virtual const_ptr_t simplify(const ClassID& cls) const { return simplify_int(cls); }
	virtual const_ptr_t simplify(Visitor& v) const;
	virtual void visit(Visitor& v) const;
	virtual std::string to_str(void) const;
	virtual void load(HibernateReadStream& ar) { hibernate_load(ar); }
	virtual void load(HibernateReadBuffer& ar) { hibernate_load(ar); }
	virtual void save(HibernateWriteStream& ar) const { const_cast<ExprCompare *>(this)->hibernate_save(ar); }

protected:
	const_ptr_t m_arg0;
	const_ptr_t m_arg1;
	compop_t m_compop;

	template <typename... Args> const_ptr_t simplify_int(Args&&... args) const;
	const_ptr_t simplify_tail(const const_ptr_t& arg0, const const_ptr_t& arg1) const;

private:
	template<class Archive> void hibernate_load(Archive& ar) {
		Expr::hibernate(ar);
		ar.ioenum(m_compop);
		boost::const_pointer_cast<Expr>(m_arg0)->load(ar);
		boost::const_pointer_cast<Expr>(m_arg1)->load(ar);
	}

	template<class Archive> void hibernate_save(Archive& ar) {
		Expr::hibernate(ar);
		ar.ioenum(m_compop);
		m_arg0->save(ar);
		m_arg1->save(ar);
	}
};

void CartoCSS::ExprCompare::visit(Visitor& v) const
{
	v.visit(*this);
	m_arg0->visit(v);
	m_arg1->visit(v);
	v.visitend(*this);
}

CartoCSS::Expr::const_ptr_t CartoCSS::ExprCompare::simplify(Visitor& v) const
{
	const_ptr_t p(simplify_int(v));
	if (!p)
		return v.modify(*this);
	const_ptr_t p1(v.modify(*p));
	return p1 ? p1 : p;	
}

template <typename... Args>
CartoCSS::Expr::const_ptr_t CartoCSS::ExprCompare::simplify_int(Args&&... args) const
{
	return simplify_tail(m_arg0->simplify(std::forward<Args>(args)...), m_arg1->simplify(std::forward<Args>(args)...));
}

CartoCSS::Expr::const_ptr_t CartoCSS::ExprCompare::simplify_tail(const const_ptr_t& arg0, const const_ptr_t& arg1) const
{
	const_ptr_t a0(arg0 ? arg0 : m_arg0);
	const_ptr_t a1(arg1 ? arg1 : m_arg1);
	// prevent fields etc. from optimizing away
	if (!a0->is_const() || !a1->is_const()) {
		if (arg0 || arg1)
			return ptr_t(new ExprCompare(a0, m_compop, a1));
		return ptr_t();
	}
	// null comparison: do first because null is convertible to almost everything else
	{
		bool v0(a0->is_type()), v1(a1->is_type());
		if (v0 || v1) {
			switch (m_compop) {
			case compop_t::equal:
				return ptr_t(new ExprBool(v0 == v1));

			case compop_t::unequal:
				return ptr_t(new ExprBool(v0 != v1));

			default:
				return ptr_t(new ExprBool(false));
			}
		}
	}
	// string comparison
	{
		std::string v0, v1, fld;
		if ((a0->is_type(v0) && a1->is_convertible(v1)) ||
		    (a1->is_type(v1) && a0->is_convertible(v0))) {
			switch (m_compop) {
			case compop_t::greater:
				return ptr_t(new ExprBool(v0 > v1));

			case compop_t::less:
				return ptr_t(new ExprBool(v0 < v1));

			case compop_t::greaterthan:
				return ptr_t(new ExprBool(v0 >= v1));

			case compop_t::lessthan:
				return ptr_t(new ExprBool(v0 <= v1));

			case compop_t::equal:
				return ptr_t(new ExprBool(v0 == v1));

			case compop_t::unequal:
				return ptr_t(new ExprBool(v0 != v1));

			default:
				if (arg0 || arg1)
					return ptr_t(new ExprCompare(a0, m_compop, a1));
				return ptr_t();
			}
		}
	}
	// double comparison
	{
		double v0, v1;
		if ((a0->is_type(v0) && a1->is_convertible(v1)) ||
		    (a1->is_type(v1) && a0->is_convertible(v0))) {
			switch (m_compop) {
			case compop_t::greater:
				return ptr_t(new ExprBool(v0 > v1));

			case compop_t::less:
				return ptr_t(new ExprBool(v0 < v1));

			case compop_t::greaterthan:
				return ptr_t(new ExprBool(v0 >= v1));

			case compop_t::lessthan:
				return ptr_t(new ExprBool(v0 <= v1));

			case compop_t::equal:
				return ptr_t(new ExprBool(v0 == v1));

			case compop_t::unequal:
				return ptr_t(new ExprBool(v0 != v1));

			default:
				if (arg0 || arg1)
					return ptr_t(new ExprCompare(a0, m_compop, a1));
				return ptr_t();
			}
		}
	}
	// int comparison
	{
		int64_t v0, v1;
		if ((a0->is_type(v0) && a1->is_convertible(v1)) ||
		    (a1->is_type(v1) && a0->is_convertible(v0))) {
			switch (m_compop) {
			case compop_t::greater:
				return ptr_t(new ExprBool(v0 > v1));

			case compop_t::less:
				return ptr_t(new ExprBool(v0 < v1));

			case compop_t::greaterthan:
				return ptr_t(new ExprBool(v0 >= v1));

			case compop_t::lessthan:
				return ptr_t(new ExprBool(v0 <= v1));

			case compop_t::equal:
				return ptr_t(new ExprBool(v0 == v1));

			case compop_t::unequal:
				return ptr_t(new ExprBool(v0 != v1));

			default:
				if (arg0 || arg1)
					return ptr_t(new ExprCompare(a0, m_compop, a1));
				return ptr_t();
			}
		}
	}
	if (arg0 || arg1)
		return ptr_t(new ExprCompare(a0, m_compop, a1));
	return ptr_t();
}

std::string CartoCSS::ExprCompare::to_str(void) const
{
	std::string r(m_arg0->to_str());
	r.push_back(' ');
	r.append(::to_str(m_compop));
	r.push_back(' ');
	r.append(m_arg1->to_str());
	return r;
}

const std::string& to_str(CartoCSS::Expr::keyword_t x)
{
	switch (x) {
	case CartoCSS::Expr::keyword_t::auto_:
	{
		static const std::string r("auto");
		return r;
	}

	case CartoCSS::Expr::keyword_t::bevel:
	{
		static const std::string r("bevel");
		return r;
	}

	case CartoCSS::Expr::keyword_t::bottom:
	{
		static const std::string r("bottom");
		return r;
	}

	case CartoCSS::Expr::keyword_t::butt:
	{
		static const std::string r("butt");
		return r;
	}

	case CartoCSS::Expr::keyword_t::global:
	{
		static const std::string r("global");
		return r;
	}

	case CartoCSS::Expr::keyword_t::interior:
	{
		static const std::string r("interior");
		return r;
	}

	case CartoCSS::Expr::keyword_t::left:
	{
		static const std::string r("left");
		return r;
	}

	case CartoCSS::Expr::keyword_t::left_only:
	{
		static const std::string r("left_only");
		return r;
	}

	case CartoCSS::Expr::keyword_t::line:
	{
		static const std::string r("line");
		return r;
	}

	case CartoCSS::Expr::keyword_t::middle:
	{
		static const std::string r("middle");
		return r;
	}

	case CartoCSS::Expr::keyword_t::miter:
	{
		static const std::string r("miter");
		return r;
	}

	case CartoCSS::Expr::keyword_t::point:
	{
		static const std::string r("point");
		return r;
	}

	case CartoCSS::Expr::keyword_t::right:
	{
		static const std::string r("right");
		return r;
	}

	case CartoCSS::Expr::keyword_t::right_only:
	{
		static const std::string r("right_only");
		return r;
	}

	case CartoCSS::Expr::keyword_t::round:
	{
		static const std::string r("round");
		return r;
	}

	case CartoCSS::Expr::keyword_t::simple:
	{
		static const std::string r("simple");
		return r;
	}

	case CartoCSS::Expr::keyword_t::square:
	{
		static const std::string r("square");
		return r;
	}

	case CartoCSS::Expr::keyword_t::top:
	{
		static const std::string r("top");
		return r;
	}

	case CartoCSS::Expr::keyword_t::visvalingamwhyatt:
	{
		static const std::string r("visvalingam-whyatt");
		return r;
	}

	default:
	{
		static const std::string r("?");
		return r;
	}
	}
}

const std::string& to_str(CartoCSS::Expr::func_t x)
{
	switch (x) {
	case CartoCSS::Expr::func_t::colmix:
	{
		static const std::string r("mix");
		return r;
	}

	case CartoCSS::Expr::func_t::url:
	{
		static const std::string r("url");
		return r;
	}

	case CartoCSS::Expr::func_t::collighten:
	{
		static const std::string r("lighten");
		return r;
	}

	case CartoCSS::Expr::func_t::coldarken:
	{
		static const std::string r("darken");
		return r;
	}

	case CartoCSS::Expr::func_t::colsaturate:
	{
		static const std::string r("saturate");
		return r;
	}

	case CartoCSS::Expr::func_t::coldesaturate:
	{
		static const std::string r("desaturate");
		return r;
	}

	case CartoCSS::Expr::func_t::colfadein:
	{
		static const std::string r("fadein");
		return r;
	}

	case CartoCSS::Expr::func_t::colfadeout:
	{
		static const std::string r("fadeout");
		return r;
	}

	case CartoCSS::Expr::func_t::colspin:
	{
		static const std::string r("spin");
		return r;
	}

	case CartoCSS::Expr::func_t::plus:
	{
		static const std::string r("plus");
		return r;
	}

	case CartoCSS::Expr::func_t::minus:
	{
		static const std::string r("minus");
		return r;
	}

	case CartoCSS::Expr::func_t::mul:
	{
		static const std::string r("mul");
		return r;
	}

	case CartoCSS::Expr::func_t::div:
	{
		static const std::string r("div");
		return r;
	}

	case CartoCSS::Expr::func_t::colrgb:
	{
		static const std::string r("rgb");
		return r;
	}

	case CartoCSS::Expr::func_t::colhsl:
	{
		static const std::string r("hsl");
		return r;
	}

	case CartoCSS::Expr::func_t::colrgba:
	{
		static const std::string r("rgba");
		return r;
	}

	case CartoCSS::Expr::func_t::colhsla:
	{
		static const std::string r("hsla");
		return r;
	}

	case CartoCSS::Expr::func_t::scalehsla:
	{
		static const std::string r("scale-hsla");
		return r;
	}

	default:
	{
		static const std::string r("?");
		return r;
	}
	}
}

const std::string& to_str(CartoCSS::Expr::compop_t x)
{
	switch (x) {
	case CartoCSS::Expr::compop_t::greater:
	{
		static const std::string r(">");
		return r;
	}

	case CartoCSS::Expr::compop_t::less:
	{
		static const std::string r("<");
		return r;
	}

	case CartoCSS::Expr::compop_t::greaterthan:
	{
		static const std::string r(">=");
		return r;
	}

	case CartoCSS::Expr::compop_t::lessthan:
	{
		static const std::string r("<=");
		return r;
	}

	case CartoCSS::Expr::compop_t::equal:
	{
		static const std::string r("==");
		return r;
	}

	case CartoCSS::Expr::compop_t::unequal:
	{
		static const std::string r("!=");
		return r;
	}

	case CartoCSS::Expr::compop_t::invalid:
	default:
	{
		static const std::string r("?");
		return r;
	}
	}
}

CartoCSS::Expr::Expr(void)
	: m_refcount(0)
{
}

CartoCSS::Expr::~Expr()
{
}

bool CartoCSS::Expr::is_convertible(bool& v) const
{
	if (is_type(v))
		return true;
	{
		std::string x;
		if (is_type(x)) {
			if (x == "false") {
				v = false;
				return true;
			}
			if (x == "true") {
				v = true;
				return true;
			}
			const char *cp(x.c_str());
			char *cp1(0);
			v = !!strtoll(cp, &cp1, 10);
			if (cp1 != cp && !*cp1)
				return true;
			v = !!strtod(cp, &cp1);
			if (cp1 != cp && !*cp1)
				return true;
		}
	}
	{
		int64_t x;
		if (is_type(x)) {
			v = !!x;
			return true;
		}
	}
	{
		double x;
		if (is_type(x)) {
			v = !!x;
			return true;
		}
	}
	if (is_type()) {
		v = false;
		return true;
	}
	v = false;
	return false;
}

bool CartoCSS::Expr::is_convertible(int64_t& v) const
{
	if (is_type(v))
		return true;
	{
		bool x;
		if (is_type(x)) {
			v = x;
			return true;
		}
	}
	{
		double x;
		if (is_type(x)) {
			v = x;
			return true;
		}
	}
	{
		std::string x;
		if (is_type(x)) {
			if (x == "false") {
				v = 0;
				return true;
			}
			if (x == "true") {
				v = 1;
				return true;
			}
			const char *cp(x.c_str());
			char *cp1(0);
			v = strtoll(cp, &cp1, 10);
			if (cp1 != cp && !*cp1)
				return true;
			v = strtod(cp, &cp1);
			if (cp1 != cp && !*cp1)
				return true;
		}
	}
	if (is_type()) {
		v = 0;
		return true;
	}
	v = 0;
	return false;
}

bool CartoCSS::Expr::is_convertible(double& v) const
{
	if (is_type(v))
		return true;
	{
		bool x;
		if (is_type(x)) {
			v = x;
			return true;
		}
	}
	{
		int64_t x;
		if (is_type(x)) {
			v = x;
			return true;
		}
	}
	{
		std::string x;
		if (is_type(x)) {
			if (x == "false") {
				v = 0;
				return true;
			}
			if (x == "true") {
				v = 1;
				return true;
			}
			const char *cp(x.c_str());
			char *cp1(0);
			v = strtod(cp, &cp1);
			if (cp1 != cp && !*cp1)
				return true;
		}
	}
	if (is_type()) {
		v = 0;
		return true;
	}
	v = 0;
	return false;
}

bool CartoCSS::Expr::is_convertible(std::string& v) const
{
	if (is_type(v))
		return true;
	{
		bool x;
		if (is_type(x)) {
			v = x ? "true" : "false";
			return true;
		}
	}
	{
		int64_t x;
		if (is_type(x)) {
			std::ostringstream z;
			z << x;
			v = z.str();
			return true;
		}
	}
	{
		double x;
		if (is_type(x)) {
			std::ostringstream z;
			z << x;
			v = z.str();
			return true;
		}
	}
	{
		keyword_t x;
		if (is_type(x)) {
			v = ::to_str(x);
			return true;
		}
	}
	if (is_type()) {
		v = "null";
		return true;
	}
	v.clear();
	return false;
}

bool CartoCSS::Expr::is_convertible(stringlist_t& v) const
{
	v.clear();
	exprlist_t e;
	if (!is_convertible(e))
		return false;
	v.reserve(e.size());
	for (const Expr::const_ptr_t& p : e) {
		std::string s;
		if (!p->is_convertible(s))
			return false;
		v.push_back(s);
	}
	return true;
}

bool CartoCSS::Expr::is_convertible(doublelist_t& v) const
{
	v.clear();
	exprlist_t e;
	if (!is_convertible(e))
		return false;
	v.reserve(e.size());
	for (const Expr::const_ptr_t& p : e) {
		double d;
		if (!p->is_convertible(d))
			return false;
		v.push_back(d);
	}
	return true;
}

bool CartoCSS::Expr::is_convertible(exprlist_t& v) const
{
	v.clear();
	if (is_type(v))
		return true;
	v.clear();
	v.push_back(get_ptr());
	return true;
}

bool CartoCSS::Expr::is_convertible(Value& v) const
{
	// null
	v = Value();
	if (is_type())
		return true;
	// bool
	{
		bool x;
		if (is_type(x)) {
			v = x;
			return true;
		}
	}
	// int
	{
		int64_t x;
		if (is_type(x)) {
			v = x;
			return true;
		}
	}
	// double
	{
		double x;
		if (is_type(x)) {
			v = x;
			return true;
		}
	}
	// string
	{
		std::string x;
		if (is_type(x)) {
			v = x;
			return true;
		}
	}
	// fallback
	{
		std::string x;
		if (is_convertible(x)) {
			v = x;
			return true;
		}
	}
	return false;	
}

bool CartoCSS::Expr::as_bool(void) const
{
	bool v;
	if (is_convertible(v))
		return v;
	throw ExprError("expression " + to_str() + " is not convertible to bool");
}

CartoCSS::Expr::keyword_t CartoCSS::Expr::as_keyword(void) const
{
	keyword_t v;
	if (is_type(v))
		return v;
	throw ExprError("expression " + to_str() + " is not a keyword");
}

CartoCSS::compositeop_t CartoCSS::Expr::as_compositeop(void) const
{
	compositeop_t v;
	if (is_type(v))
		return v;
	throw ExprError("expression " + to_str() + " is not a compositing operator");
}

int64_t CartoCSS::Expr::as_int(void) const
{
	int64_t v;
	if (is_convertible(v))
		return v;
	throw ExprError("expression " + to_str() + " is not convertible to int");
}

double CartoCSS::Expr::as_double(void) const
{
	double v;
	if (is_convertible(v))
		return v;
	throw ExprError("expression " + to_str() + " is not convertible to double");
}

std::string CartoCSS::Expr::as_string(void) const
{
	std::string v;
	if (is_convertible(v))
		return v;
	throw ExprError("expression " + to_str() + " is not convertible to string");
}

CartoCSS::Expr::stringlist_t CartoCSS::Expr::as_stringlist(void) const
{
	stringlist_t v;
	if (is_convertible(v))
		return v;
	throw ExprError("expression " + to_str() + " is not convertible to string list");
}

CartoCSS::Expr::doublelist_t CartoCSS::Expr::as_doublelist(void) const
{
	doublelist_t v;
	if (is_convertible(v))
		return v;
	throw ExprError("expression " + to_str() + " is not convertible to double list");
}

CartoCSS::Expr::exprlist_t CartoCSS::Expr::as_exprlist(void) const
{
	exprlist_t v;
	if (is_convertible(v))
		return v;
	throw ExprError("expression " + to_str() + " is not convertible to expression list");
}

CartoCSS::Color CartoCSS::Expr::as_color(void) const
{
	Color v;
	if (is_type(v))
		return v;
	throw ExprError("expression " + to_str() + " is not a color");
}

std::string CartoCSS::Expr::as_url(void) const
{
	std::string v;
	if (is_url(v))
		return v;
	throw ExprError("expression " + to_str() + " is not an url");
}

CartoCSS::Value CartoCSS::Expr::as_value(void) const
{
	Value v;
	if (is_convertible(v))
		return v;
	throw ExprError("expression " + to_str() + " is not convertible to Value");
}

CartoCSS::Expr::const_ptr_t CartoCSS::Expr::create(void)
{
	return ptr_t(new ExprNull());
}

CartoCSS::Expr::const_ptr_t CartoCSS::Expr::create(bool v)
{
	return ptr_t(new ExprBool(v));
}

CartoCSS::Expr::const_ptr_t CartoCSS::Expr::create(keyword_t v)
{
	return ptr_t(new ExprKeyword(v));
}

CartoCSS::Expr::const_ptr_t CartoCSS::Expr::create(compositeop_t v)
{
	return ptr_t(new ExprCompositeOp(v));
}

CartoCSS::Expr::const_ptr_t CartoCSS::Expr::create(const LayerID& v)
{
	return ptr_t(new ExprLayerID(v));
}

CartoCSS::Expr::const_ptr_t CartoCSS::Expr::create(const ClassID& v)
{
	return ptr_t(new ExprClassID(v));
}

CartoCSS::Expr::const_ptr_t CartoCSS::Expr::create(int64_t v)
{
	return ptr_t(new ExprInt(v));
}

CartoCSS::Expr::const_ptr_t CartoCSS::Expr::create(double v)
{
	return ptr_t(new ExprDouble(v));
}

CartoCSS::Expr::const_ptr_t CartoCSS::Expr::create(const std::string& v)
{
	return ptr_t(new ExprString(v));
}

CartoCSS::Expr::const_ptr_t CartoCSS::Expr::create(const Color& v)
{
	return ptr_t(new ExprColor(v));
}

CartoCSS::Expr::const_ptr_t CartoCSS::Expr::create(const Value& v)
{
	return v.get_expr();
}

CartoCSS::Expr::const_ptr_t CartoCSS::Expr::create(const exprlist_t& v)
{
	return ptr_t(new ExprList(v));
}

CartoCSS::Expr::const_ptr_t CartoCSS::Expr::create(func_t func, const Expr::const_ptr_t& args)
{
	return ptr_t(new ExprFunction(func, args));
}

CartoCSS::Expr::const_ptr_t CartoCSS::Expr::create(const Expr::const_ptr_t& v0, compop_t op, const Expr::const_ptr_t& v1)
{
	return ptr_t(new ExprCompare(v0, op, v1));
}

CartoCSS::Expr::const_ptr_t CartoCSS::Expr::create_url(const std::string& url)
{
	return ptr_t(new ExprURL(url));
}

CartoCSS::Expr::const_ptr_t CartoCSS::Expr::create_directory(const std::string& dir)
{
	return ptr_t(new ExprDirectory(dir));
}

CartoCSS::Expr::const_ptr_t CartoCSS::Expr::create_varref(const std::string& var)
{
	return ptr_t(new ExprVarRef(var));
}

CartoCSS::Expr::const_ptr_t CartoCSS::Expr::create_field(const std::string& field)
{
	return ptr_t(new ExprField(field));
}

CartoCSS::Expr::const_ptr_t CartoCSS::Expr::create_copylayer(const std::string& field)
{
	return ptr_t(new ExprCopyLayer(field));
}

CartoCSS::Expr::const_ptr_t CartoCSS::Expr::create_number(const std::string& v)
{
	// try to parse as int
	{
		std::string::const_iterator i(v.begin()), e(v.end());
		while (i != e && std::isspace(*i))
			++i;
		bool neg(i != e && *i == '-');
		if (neg)
			++i;
		bool dig(false), ovfl(false);
		int64_t v(0);
		while (i != e && std::isdigit(*i)) {
			ovfl = ovfl || (v > (std::numeric_limits<int64_t>::max() / 10));
			v *= 10;
			int64_t vadd(*i - '0');
			ovfl = ovfl || (v > (std::numeric_limits<int64_t>::max() - vadd));
			v += vadd;
			dig = true;
			++i;
		}
		while (i != e && std::isspace(*i))
			++i;
		if (i == e && dig && !ovfl) {
			if (neg)
				v = -v;
			return ptr_t(new ExprInt(v));
		}
	}
	// try to parse as double
	{
		const char *cp(v.c_str());
		char *cp1(0);
		double v1(strtod(cp, &cp1));
		if (cp != cp1) {
			while (std::isspace(*cp1))
				++cp1;
			if (!*cp1)
				return ptr_t(new ExprDouble(v1));
		}
	}
	// try to parse as bool
	if (v == "false")
		return ptr_t(new ExprBool(false));
	if (v == "true")
		return ptr_t(new ExprBool(true));
	if (v == "null")
		return ptr_t(new ExprNull());
	return ptr_t();
}

CartoCSS::Expr::ptr_t CartoCSS::Expr::create(type_t t)
{
	switch (t) {
	case type_t::null:
		return ptr_t(new ExprNull());

	case type_t::keyword:
		return ptr_t(new ExprKeyword(keyword_t::invalid));

	case type_t::compositeop:
		return ptr_t(new ExprCompositeOp(compositeop_t::invalid));

	case type_t::layerid:
		return ptr_t(new ExprLayerID(LayerID()));

	case type_t::classid:
		return ptr_t(new ExprClassID(ClassID()));

	case type_t::boolean:
		return ptr_t(new ExprBool(false));

	case type_t::integer:
		return ptr_t(new ExprInt(0));

	case type_t::floatingpoint:
		return ptr_t(new ExprDouble(std::numeric_limits<double>::quiet_NaN()));

	case type_t::string:
		return ptr_t(new ExprString(std::string()));

	case type_t::color:
		return ptr_t(new ExprColor(Color()));

	case type_t::url:
		return ptr_t(new ExprURL(std::string()));

	case type_t::directory:
		return ptr_t(new ExprDirectory(std::string()));

	case type_t::varref:
		return ptr_t(new ExprVarRef(std::string()));

	case type_t::field:
		return ptr_t(new ExprField(std::string()));

	case type_t::list:
		return ptr_t(new ExprList(exprlist_t()));

	case type_t::function:
		return ptr_t(new ExprFunction(func_t::invalid, const_ptr_t()));

	case type_t::compare:
		return ptr_t(new ExprCompare(const_ptr_t(), compop_t::invalid, const_ptr_t()));

	default:
		return ptr_t();
	}
}

CartoCSS::Expr::const_ptr_t CartoCSS::Expr::create(HibernateReadStream& ar)
{
	type_t typ;
	ar.ioenum(typ);
	ptr_t p(create(typ));
	if (p)
		p->load(ar);
	return p;
}

CartoCSS::Expr::const_ptr_t CartoCSS::Expr::create(HibernateReadBuffer& ar)
{
	type_t typ;
	ar.ioenum(typ);
	ptr_t p(create(typ));
	if (p)
		p->load(ar);
	return p;
}

CartoCSS::Expr::compop_t CartoCSS::Expr::reverse(compop_t op)
{
	switch (op) {
	case compop_t::greater:
		return compop_t::less;

	case compop_t::less:
		return compop_t::greater;

	case compop_t::greaterthan:
		return compop_t::lessthan;

	case compop_t::lessthan:
		return compop_t::greaterthan;

	default:
		return op;
	}
}

boost::intrusive_ptr<CartoCSS::Expr> CartoCSS::Value::get_expr(void) const
{
	switch (m_type) {
	case type_t::string:
		return Expr::ptr_t(new ExprString(m_string));

	case type_t::floatingpoint:
		return Expr::ptr_t(new ExprDouble(m_double));

	case type_t::integer:
		return Expr::ptr_t(new ExprInt(m_int));

	case type_t::boolean:
		return Expr::ptr_t(new ExprBool(m_bool));

	default:
		return Expr::ptr_t(new ExprNull());
	}
}
