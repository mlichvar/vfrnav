//
// C++ Implementation: cartocssparser
//
// Description: CartoCSS parser
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#undef BOOST_SPIRIT_X3_DEBUG
#include <boost/config/warning_disable.hpp>
#include <boost/spirit/home/x3.hpp>
#include <boost/fusion/include/vector.hpp>
#include <boost/fusion/include/make_deque.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/io.hpp>
#include <boost/fusion/adapted.hpp>
#include <boost/spirit/include/support_line_pos_iterator.hpp>
#include <boost/spirit/include/support_istream_iterator.hpp>

#include <boost/variant.hpp>
#include <boost/pool/object_pool.hpp>

#include <glibmm.h>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>

#include "cartocss.h"
#include "geom.h"

namespace {

	namespace x3 = boost::spirit::x3;
	namespace ascii = boost::spirit::x3::ascii;
	namespace fusion = boost::fusion;

	template<typename Iterator>
	void parse_error(const Iterator& begin, const Iterator& iter, const Iterator& end, const std::string& info = "") {
		std::ostringstream oss;
		oss << "parse error at";
		{
			std::size_t ln(boost::spirit::get_line(iter));
			if (ln != static_cast<std::size_t>(-1))
				oss << " line " << ln;
		}
		std::size_t col(boost::spirit::get_column(begin, iter, 8));
		oss << " column " << col << std::endl
		    << '\'' << boost::spirit::get_current_line(begin, iter, end) << '\'' << std::endl
		    <<std::setw(col) << ' ' << "^- here";
		if (!info.empty())
			oss << std::endl << info;
		throw std::runtime_error(oss.str());
	}

	template<typename Iterator>
	void parse_error(const Iterator& iter, const std::string& info = "") {
		std::ostringstream oss;
		oss << "parse error at";
		{
			std::size_t ln(boost::spirit::get_line(iter));
			if (ln != static_cast<std::size_t>(-1))
				oss << " line " << ln;
		}
		if (!info.empty())
			oss << std::endl << info;
		throw std::runtime_error(oss.str());
	}

	typedef CartoCSS::Color Color;
	typedef CartoCSS::Expr Expr;
	typedef CartoCSS::Statement Statement;
	typedef CartoCSS::SubBlock SubBlock;
	typedef CartoCSS::Statements Statements;
	typedef CartoCSS::VariableAssignments VariableAssignments;

	template<typename Iterator>
	struct cartocss_parser {
		typedef std::map<std::string, Expr::const_ptr_t> variables_t;

		cartocss_parser(CartoCSS& style, const std::string& dirname, std::ostream *msg = nullptr) : m_style(style), m_dirname(dirname), m_msg(msg) {
		}

		class NoncircularVars : public CartoCSS::Visitor {
		public:
			NoncircularVars(const variables_t& vars, const std::string& var) : m_vars(vars), m_var(var), m_circular(false), m_selfref(false) {}
			virtual CartoCSS::Expr::const_ptr_t modify(const CartoCSS::Expr& e) {
				std::string x;
				if (!e.is_varref(x))
					return CartoCSS::Expr::const_ptr_t();
				m_selfref = m_selfref || (!m_var.empty() && m_var == x);
				variables_t::const_iterator i(m_vars.find(x));
				if (i == m_vars.end())
					return CartoCSS::Expr::const_ptr_t();
				if (i->second)
					return i->second;
				m_circular = true;
				return CartoCSS::Expr::create();
			}
			bool is_circular(void) const { return m_circular; }
			bool is_selfref(void) const { return m_selfref; }

		protected:
		        const variables_t& m_vars;
			std::string m_var;
			bool m_circular;
			bool m_selfref;
		};

		void set_variable(const std::string& name, const Expr::const_ptr_t& val) {
			Expr::const_ptr_t nullvar(Expr::create());
			variables_t::iterator i;
			{
				bool ok;
				std::tie(i, ok) = m_variables.insert(std::make_pair(name, nullvar));
				if (!ok && m_msg)
					*m_msg << "Warning: redefining variable @" << name << std::endl;
			}
			if (val) {
				NoncircularVars vis(m_variables, name);
				Expr::const_ptr_t v(val->simplify(vis));
				i->second = val;
				if (v)
					i->second = v;
				if (m_msg) {
					if (vis.is_circular())
						*m_msg << "Warning: circular variable definition @" << name << ": " << val->to_str() << std::endl;
					else if (vis.is_selfref())
						*m_msg << "Warning: self referring variable definition @" << name << ": " << val->to_str() << std::endl;
				}
			}
			for (;;) {
				bool nowork(true);
				for (auto& var : m_variables) {
					Expr::const_ptr_t v1(var.second);
					var.second = nullvar;
					if (!v1)
						continue;
					NoncircularVars vis(m_variables, var.first);
					Expr::const_ptr_t v2(v1->simplify(vis));
					var.second = v1;
					if (v2) {
						nowork = false;
						var.second = v2;
					}
					if (!m_msg)
						continue;
					if (vis.is_circular())
						*m_msg << "Warning: circular variable definition @" << var.first << ": " << v1->to_str() << std::endl;
					else if (vis.is_selfref())
						*m_msg << "Warning: self referring variable definition @" << var.first << ": " << v1->to_str() << std::endl;
				}
				if (nowork)
					break;
			}
		}

		class UndefinedVars : public CartoCSS::Visitor {
		public:
			virtual CartoCSS::Expr::const_ptr_t modify(const CartoCSS::Expr& e) {
				std::string x;
				if (!e.is_varref(x))
					return CartoCSS::Expr::const_ptr_t();
				m_undefvars.insert(x);
				return CartoCSS::Expr::create();
			}
			const std::set<std::string>& get_undefvars(void) const { return m_undefvars; }

		protected:
			std::set<std::string> m_undefvars;
		};

		void finalize(void) {
			// resolve expressions
			Statement::const_ptr_t stmt(new Statements(m_statements));
			for (;;) {
				bool work(false);
				if (m_backgroundcol) {
					Expr::const_ptr_t p(m_backgroundcol->simplify(m_variables));
					if (p) {
						m_backgroundcol = p;
						work = true;
					}
				}
				{
					Statement::const_ptr_t p(stmt->simplify(m_variables));
					if (p) {
						stmt = p;
						work = true;
					}
				}
				if (!work)
					break;
			}
			// convert global attributes
			if (m_backgroundcol)
				m_style.get_mapparameters().set_variable(CartoCSS::variable_t::mapbackgroundcolor, m_backgroundcol);
			// convert statements
			{
				UndefinedVars udv;
				{
					Statement::const_ptr_t p(stmt->simplify(udv));
					if (p)
						stmt = p;
				}
				m_style.set_statements(stmt);
				if (!udv.get_undefvars().empty()) {
					std::cerr << "WARNING: undefined variables";
					for (const auto& v : udv.get_undefvars())
						std::cerr << ' ' << v;
					std::cerr << std::endl;
				}
			}
		}

		CartoCSS& m_style;
		std::string m_dirname;
		std::ostream *m_msg;

		variables_t m_variables;
		Statements::statements_t m_statements;
		Expr::const_ptr_t m_backgroundcol;
	};

	namespace skipper {
		using x3::rule;
		using x3::lit;
		using x3::eol;
		using x3::eoi;
		using ascii::char_;
		using ascii::space;

                const rule<class single_line_comment> single_line_comment = "single_line_comment";
                const rule<class block_comment> block_comment = "block_comment";
                const rule<class skip> skip = "skip";

                const auto single_line_comment_def = lit("//") >> *(char_ - eol) >> (eol|eoi);
                const auto block_comment_def = (lit("/*") >> *(block_comment | (char_ - lit("*/")))) > lit("*/");
                const auto skip_def = space | single_line_comment | block_comment;

                BOOST_SPIRIT_DEFINE(single_line_comment, block_comment, skip);
	};


	namespace parser {
		using x3::rule;
		using x3::get;
		using x3::no_case;
		using x3::no_skip;
		using x3::lit;
		using x3::omit;
		using x3::lexeme;
		using x3::repeat;
		using x3::eps;
		using x3::eol;
		using x3::eoi;
		using x3::attr;
		using x3::with;
		using x3::_val;
		using x3::_attr;
		using x3::_where;
		using x3::_pass;
		using x3::ulong_;
		using x3::long_;
		using x3::double_;
		using ascii::char_;
		using ascii::alnum;
		using ascii::alpha;
		using ascii::digit;
		using ascii::xdigit;
		using ascii::space;
		using fusion::at_c;

		//-----------------------------------------------------------------------------
		// Line
		//-----------------------------------------------------------------------------

		struct parser_tag {};

 		//-----------------------------------------------------------------------------
		// Operators
		//-----------------------------------------------------------------------------

		const struct compoptype_ : x3::symbols<CartoCSS::compositeop_t> {
			compoptype_() {
				add
					("clear",         CartoCSS::compositeop_t::clear)
					("src",           CartoCSS::compositeop_t::src)
					("dst",           CartoCSS::compositeop_t::dst)
					("src-over",      CartoCSS::compositeop_t::srcover)
					("dst-over",      CartoCSS::compositeop_t::dstover)
					("src-in",        CartoCSS::compositeop_t::srcin)
					("dst-in",        CartoCSS::compositeop_t::dstin)
					("src-out",       CartoCSS::compositeop_t::srcout)
					("dst-out",       CartoCSS::compositeop_t::dstout)
					("src-atop",      CartoCSS::compositeop_t::srcatop)
					("dst-atop",      CartoCSS::compositeop_t::dstatop)
					("xor",           CartoCSS::compositeop_t::xor_)
					("plus",          CartoCSS::compositeop_t::plus)
					("minus",         CartoCSS::compositeop_t::minus)
					("multiply",      CartoCSS::compositeop_t::multiply)
					("screen",        CartoCSS::compositeop_t::screen)
					("overlay",       CartoCSS::compositeop_t::overlay)
					("darken",        CartoCSS::compositeop_t::darken)
					("lighten",       CartoCSS::compositeop_t::lighten)
					("color-dodge",   CartoCSS::compositeop_t::colordodge)
					("color-burn",    CartoCSS::compositeop_t::colorburn)
					("hard-light",    CartoCSS::compositeop_t::hardlight)
					("soft-light",    CartoCSS::compositeop_t::softlight)
					("difference",    CartoCSS::compositeop_t::difference)
					("exclusion",     CartoCSS::compositeop_t::exclusion)
					("contrast",      CartoCSS::compositeop_t::contrast)
					("invert",        CartoCSS::compositeop_t::invert)
					("invert-rgb",    CartoCSS::compositeop_t::invertrgb)
					("grain-merge",   CartoCSS::compositeop_t::grainmerge)
					("grain-extract", CartoCSS::compositeop_t::grainextract)
					("hue",           CartoCSS::compositeop_t::hue)
					("saturation",    CartoCSS::compositeop_t::saturation)
					("color",         CartoCSS::compositeop_t::color)
					("value",         CartoCSS::compositeop_t::value);
			}
		} compoptype;

		const struct comparisonoptype_ : x3::symbols<Expr::compop_t> {
			comparisonoptype_() {
				add
					(">=",            Expr::compop_t::greaterthan)
					("<=",            Expr::compop_t::lessthan)
					("!=",            Expr::compop_t::unequal)
					(">",             Expr::compop_t::greater)
					("<",             Expr::compop_t::less)
					("=",             Expr::compop_t::equal);
			}
		} comparisonoptype;

		const struct exprmulop_ : x3::symbols<Expr::func_t> {
			exprmulop_() {
				add
					("*",             Expr::func_t::mul)
					("/",             Expr::func_t::div);
			}
		} exprmulop;

		const struct expraddop_ : x3::symbols<Expr::func_t> {
			expraddop_() {
				add
					("+",             Expr::func_t::plus)
					("-",             Expr::func_t::minus);
			}
		} expraddop;

		const struct variablenametype_ : x3::symbols<CartoCSS::variable_t> {
			variablenametype_() {
				add
					("comp-op",                     CartoCSS::variable_t::compop)
					("image-filters",               CartoCSS::variable_t::imagefilters)
					("line-cap",                    CartoCSS::variable_t::linecap)
					("line-clip",                   CartoCSS::variable_t::lineclip)
					("line-color",                  CartoCSS::variable_t::linecolor)
					("line-dasharray",              CartoCSS::variable_t::linedasharray)
					("line-join",                   CartoCSS::variable_t::linejoin)
					("line-offset",                 CartoCSS::variable_t::lineoffset)
					("line-opacity",                CartoCSS::variable_t::lineopacity)
					("line-pattern-file",           CartoCSS::variable_t::linepatternfile)
					("line-simplify",               CartoCSS::variable_t::linesimplify)
					("line-simplify-algorithm",     CartoCSS::variable_t::linesimplifyalgorithm)
					("line-width",                  CartoCSS::variable_t::linewidth)
					("marker-allow-overlap",        CartoCSS::variable_t::markerallowoverlap)
					("marker-clip",                 CartoCSS::variable_t::markerclip)
					("marker-file",                 CartoCSS::variable_t::markerfile)
					("marker-fill",                 CartoCSS::variable_t::markerfill)
					("marker-height",               CartoCSS::variable_t::markerheight)
					("marker-ignore-placement",     CartoCSS::variable_t::markerignoreplacement)
					("marker-line-color",           CartoCSS::variable_t::markerlinecolor)
					("marker-line-width",           CartoCSS::variable_t::markerlinewidth)
					("marker-max-error",            CartoCSS::variable_t::markermaxerror)
					("marker-opacity",              CartoCSS::variable_t::markeropacity)
					("marker-placement",            CartoCSS::variable_t::markerplacement)
					("marker-spacing",              CartoCSS::variable_t::markerspacing)
					("marker-width",                CartoCSS::variable_t::markerwidth)
					("opacity",                     CartoCSS::variable_t::opacity)
					("polygon-clip",                CartoCSS::variable_t::polygonclip)
					("polygon-fill",                CartoCSS::variable_t::polygonfill)
					("polygon-gamma",               CartoCSS::variable_t::polygongamma)
					("polygon-pattern-alignment",   CartoCSS::variable_t::polygonpatternalignment)
					("polygon-pattern-file",        CartoCSS::variable_t::polygonpatternfile)
					("polygon-pattern-gamma",       CartoCSS::variable_t::polygonpatterngamma)
					("polygon-pattern-opacity",     CartoCSS::variable_t::polygonpatternopacity)
					("shield-character-spacing",    CartoCSS::variable_t::shieldcharacterspacing)
					("shield-clip",                 CartoCSS::variable_t::shieldclip)
					("shield-face-name",            CartoCSS::variable_t::shieldfacename)
					("shield-file",                 CartoCSS::variable_t::shieldfile)
					("shield-fill",                 CartoCSS::variable_t::shieldfill)
					("shield-halo-fill",            CartoCSS::variable_t::shieldhalofill)
					("shield-halo-radius",          CartoCSS::variable_t::shieldhaloradius)
					("shield-horizontal-alignment", CartoCSS::variable_t::shieldhorizontalalignment)
					("shield-line-spacing",         CartoCSS::variable_t::shieldlinespacing)
					("shield-margin",               CartoCSS::variable_t::shieldmargin)
					("shield-name",                 CartoCSS::variable_t::shieldname)
					("shield-opacity",              CartoCSS::variable_t::shieldopacity)
					("shield-placement",            CartoCSS::variable_t::shieldplacement)
					("shield-placement-type",       CartoCSS::variable_t::shieldplacementtype)
					("shield-placements",           CartoCSS::variable_t::shieldplacements)
					("shield-repeat-distance",      CartoCSS::variable_t::shieldrepeatdistance)
					("shield-size",                 CartoCSS::variable_t::shieldsize)
					("shield-spacing",              CartoCSS::variable_t::shieldspacing)
					("shield-text-dx",              CartoCSS::variable_t::shieldtextdx)
					("shield-text-dy",              CartoCSS::variable_t::shieldtextdy)
					("shield-unlock-image",         CartoCSS::variable_t::shieldunlockimage)
					("shield-vertical-alignment",   CartoCSS::variable_t::shieldverticalalignment)
					("shield-wrap-character",       CartoCSS::variable_t::shieldwrapcharacter)
					("shield-wrap-width",           CartoCSS::variable_t::shieldwrapwidth)
					("text-character-spacing",      CartoCSS::variable_t::textcharacterspacing)
					("text-clip",                   CartoCSS::variable_t::textclip)
					("text-dx",                     CartoCSS::variable_t::textdx)
					("text-dy",                     CartoCSS::variable_t::textdy)
					("text-face-name",              CartoCSS::variable_t::textfacename)
					("text-fill",                   CartoCSS::variable_t::textfill)
					("text-halo-fill",              CartoCSS::variable_t::texthalofill)
					("text-halo-radius",            CartoCSS::variable_t::texthaloradius)
					("text-horizontal-alignment",   CartoCSS::variable_t::texthorizontalalignment)
					("text-largest-bbox-only",      CartoCSS::variable_t::textlargestbboxonly)
					("text-line-spacing",           CartoCSS::variable_t::textlinespacing)
					("text-margin",                 CartoCSS::variable_t::textmargin)
					("text-max-char-angle-delta",   CartoCSS::variable_t::textmaxcharangledelta)
					("text-min-distance",           CartoCSS::variable_t::textmindistance)
					("text-name",                   CartoCSS::variable_t::textname)
					("text-opacity",                CartoCSS::variable_t::textopacity)
					("text-placement",              CartoCSS::variable_t::textplacement)
					("text-placement-type",         CartoCSS::variable_t::textplacementtype)
					("text-placements",             CartoCSS::variable_t::textplacements)
					("text-repeat-distance",        CartoCSS::variable_t::textrepeatdistance)
					("text-size",                   CartoCSS::variable_t::textsize)
					("text-spacing",                CartoCSS::variable_t::textspacing)
					("text-upright",                CartoCSS::variable_t::textupright)
					("text-vertical-alignment",     CartoCSS::variable_t::textverticalalignment)
					("text-wrap-character",         CartoCSS::variable_t::textwrapcharacter)
					("text-wrap-width",             CartoCSS::variable_t::textwrapwidth);
			}
		} variablenametype;

		const struct keywordtype_ : x3::symbols<CartoCSS::Expr::keyword_t> {
			keywordtype_() {
				add
					("auto",                        CartoCSS::Expr::keyword_t::auto_)
					("bevel",                       CartoCSS::Expr::keyword_t::bevel)
					("bottom",                      CartoCSS::Expr::keyword_t::bottom)
					("butt",                        CartoCSS::Expr::keyword_t::butt)
					("global",                      CartoCSS::Expr::keyword_t::global)
					("interior",                    CartoCSS::Expr::keyword_t::interior)
					("left",                        CartoCSS::Expr::keyword_t::left)
					("left_only",                   CartoCSS::Expr::keyword_t::left_only)
					("line",                        CartoCSS::Expr::keyword_t::line)
					("middle",                      CartoCSS::Expr::keyword_t::middle)
					("miter",                       CartoCSS::Expr::keyword_t::miter)
					("point",                       CartoCSS::Expr::keyword_t::point)
					("right",                       CartoCSS::Expr::keyword_t::right)
					("right_only",                  CartoCSS::Expr::keyword_t::right_only)
					("round",                       CartoCSS::Expr::keyword_t::round)
					("simple",                      CartoCSS::Expr::keyword_t::simple)
					("square",                      CartoCSS::Expr::keyword_t::square)
					("top",                         CartoCSS::Expr::keyword_t::top)
					("visvalingam-whyatt",          CartoCSS::Expr::keyword_t::visvalingamwhyatt);
			}
		} keywordtype;

		const struct csscolortype_ : x3::symbols<uint32_t> {
			csscolortype_() {
				add
					// Red colors
					("lightsalmon",          0xFFA07A)
					("salmon",               0xFA8072)
					("darksalmon",           0xE9967A)
					("lightcoral",           0xF08080)
					("indianred",            0xCD5C5C)
					("crimson",              0xDC143C)
					("firebrick",            0xB22222)
					("red",                  0xFF0000)
					("darkred",              0x8B0000)
					// Orange colors
					("coral",                0xFF7F50)
					("tomato",               0xFF6347)
					("orangered",            0xFF4500)
					("gold",                 0xFFD700)
					("orange",               0xFFA500)
					("darkorange",           0xFF8C00)
					// Yellow colors
					("lightyellow",          0xFFFFE0)
					("lemonchiffon",         0xFFFACD)
					("lightgoldenrodyellow", 0xFAFAD2)
					("papayawhip",           0xFFEFD5)
					("moccasin",             0xFFE4B5)
					("peachpuff",            0xFFDAB9)
					("palegoldenrod",        0xEEE8AA)
					("khaki",                0xF0E68C)
					("darkkhaki",            0xBDB76B)
					("yellow",               0xFFFF00)
					// Green colors
					("lawngreen",            0x7CFC00)
					("chartreuse",           0x7FFF00)
					("limegreen",            0x32CD32)
					("lime",                 0x00FF00)
					("forestgreen",          0x228B22)
					("green",                0x008000)
					("darkgreen",            0x006400)
					("greenyellow",          0xADFF2F)
					("yellowgreen",          0x9ACD32)
					("springgreen",          0x00FF7F)
					("mediumspringgreen",    0x00FA9A)
					("lightgreen",           0x90EE90)
					("palegreen",            0x98FB98)
					("darkseagreen",         0x8FBC8F)
					("mediumseagreen",       0x3CB371)
					("seagreen",             0x2E8B57)
					("olive",                0x808000)
					("darkolivegreen",       0x556B2F)
					("olivedrab",            0x6B8E23)
					// Cyan colors
					("lightcyan",            0xE0FFFF)
					("cyan",                 0x00FFFF)
					("aqua",                 0x00FFFF)
					("aquamarine",           0x7FFFD4)
					("mediumaquamarine",     0x66CDAA)
					("paleturquoise",        0xAFEEEE)
					("turquoise",            0x40E0D0)
					("mediumturquoise",      0x48D1CC)
					("darkturquoise",        0x00CED1)
					("lightseagreen",        0x20B2AA)
					("cadetblue",            0x5F9EA0)
					("darkcyan",             0x008B8B)
					("teal",                 0x008080)
					// Blue colors
					("powderblue",           0xB0E0E6)
					("lightblue",            0xADD8E6)
					("lightskyblue",         0x87CEFA)
					("skyblue",              0x87CEEB)
					("deepskyblue",          0x00BFFF)
					("lightsteelblue",       0xB0C4DE)
					("dodgerblue",           0x1E90FF)
					("cornflowerblue",       0x6495ED)
					("steelblue",            0x4682B4)
					("royalblue",            0x4169E1)
					("blue",                 0x0000FF)
					("mediumblue",           0x0000CD)
					("darkblue",             0x00008B)
					("navy",                 0x000080)
					("midnightblue",         0x191970)
					("mediumslateblue",      0x7B68EE)
					("slateblue",            0x6A5ACD)
					("darkslateblue",        0x483D8B)
					// Purple colors
					("lavender",             0xE6E6FA)
					("thistle",              0xD8BFD8)
					("plum",                 0xDDA0DD)
					("violet",               0xEE82EE)
					("orchid",               0xDA70D6)
					("fuchsia",              0xFF00FF)
					("magenta",              0xFF00FF)
					("mediumorchid",         0xBA55D3)
					("mediumpurple",         0x9370DB)
					("blueviolet",           0x8A2BE2)
					("darkviolet",           0x9400D3)
					("darkorchid",           0x9932CC)
					("darkmagenta",          0x8B008B)
					("purple",               0x800080)
					("indigo",               0x4B0082)
					// Pink colors
					("pink",                 0xFFC0CB)
					("lightpink",            0xFFB6C1)
					("hotpink",              0xFF69B4)
					("deeppink",             0xFF1493)
					("palevioletred",        0xDB7093)
					("mediumvioletred",      0xC71585)
					// White colors
					("white",                0xFFFFFF)
					("snow",                 0xFFFAFA)
					("honeydew",             0xF0FFF0)
					("mintcream",            0xF5FFFA)
					("azure",                0xF0FFFF)
					("aliceblue",            0xF0F8FF)
					("ghostwhite",           0xF8F8FF)
					("whitesmoke",           0xF5F5F5)
					("seashell",             0xFFF5EE)
					("beige",                0xF5F5DC)
					("oldlace",              0xFDF5E6)
					("floralwhite",          0xFFFAF0)
					("ivory",                0xFFFFF0)
					("antiquewhite",         0xFAEBD7)
					("linen",                0xFAF0E6)
					("lavenderblush",        0xFFF0F5)
					("mistyrose",            0xFFE4E1)
					// Gray colors
					("gainsboro",            0xDCDCDC)
					("lightgray",            0xD3D3D3)
					("silver",               0xC0C0C0)
					("darkgray",             0xA9A9A9)
					("gray",                 0x808080)
					("grey",                 0x808080)
					("dimgray",              0x696969)
					("lightslategray",       0x778899)
					("slategray",            0x708090)
					("darkslategray",        0x2F4F4F)
					("black",                0x000000)
					// Brown colors
					("cornsilk",             0xFFF8DC)
					("blanchedalmond",       0xFFEBCD)
					("bisque",               0xFFE4C4)
					("navajowhite",          0xFFDEAD)
					("wheat",                0xF5DEB3)
					("burlywood",            0xDEB887)
					("tan",                  0xD2B48C)
					("rosybrown",            0xBC8F8F)
					("sandybrown",           0xF4A460)
					("goldenrod",            0xDAA520)
					("peru",                 0xCD853F)
					("chocolate",            0xD2691E)
					("saddlebrown",          0x8B4513)
					("sienna",               0xA0522D)
					("brown",                0xA52A2A)
					("maroon",               0x800000);
			}
		} csscolortype;

		//-----------------------------------------------------------------------------
                // Number Parsers
                //-----------------------------------------------------------------------------

		struct uintparse {
			unsigned int m_radix;
			uintparse(unsigned int r = 10) : m_radix(r) {}
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = strtoull(_attr(ctx).c_str(), nullptr, m_radix);
			}
		};

		struct floatparse {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = strtod(_attr(ctx).c_str(), nullptr);
			}
		};

		const rule<struct hex3, uint16_t> hex3 = "hex3";
		const rule<struct uintnumber, unsigned long long> uintnumber = "uintnumber";
		const rule<struct floatnumberstr, std::string> floatnumberstr = "floatnumberstr";
		const rule<struct floatnumber, double> floatnumber = "floatnumber";

		const auto hex3_def = lexeme[repeat(3)[xdigit]][uintparse{16}];
		const auto uintnumber_def = lexeme[+digit][uintparse{10}];

		const auto floatnumberstr_def = ((lexeme[-char_('-') >> +digit >> -(char_('.') >> *digit) >>
							 -(char_("eE") >> -char_("+-") >> *digit)]) |
						 (lexeme[-char_('-') >> *digit >> char_('.') >> +digit >>
							 -(char_("eE") >> -char_("+-") >> *digit)]));

		const auto floatnumber_def = floatnumberstr[floatparse{}];

		BOOST_SPIRIT_DEFINE(hex3, uintnumber, floatnumberstr, floatnumber);

		//-----------------------------------------------------------------------------
                // Identifiers and Strings
                //-----------------------------------------------------------------------------

		struct layeridparse {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = static_cast<CartoCSS::LayerID>(_attr(ctx));
			}
		};

		struct classidparse {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = static_cast<CartoCSS::ClassID>(_attr(ctx));
			}
		};

		struct stringescapeparse {
			template<typename Context>
			void operator()(Context& ctx) {
				char ch(_attr(ctx));
        			switch (ch) {
				case 'n':
				case 'N':
					_val(ctx) = '\n';
					break;

				case 'r':
				case 'R':
					_val(ctx) = '\r';
					break;

				case 't':
				case 'T':
				        _val(ctx) = '\t';
					break;

				default:
					_val(ctx) = 0;
					break;
				}
			}
		};

		struct hexcharparse {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = strtoul(_attr(ctx).c_str(), nullptr, 16);
			}
		};

		struct appendchar {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx).push_back(_attr(ctx));
			}
		};

		const rule<struct identifier, std::string> identifier = "identifier";
		const rule<struct layeridtype, CartoCSS::LayerID> layeridtype = "layeridtype";
		const rule<struct classidtype, CartoCSS::ClassID> classidtype = "classidtype";
		const rule<struct stringescape, char> stringescape = "stringescape";
		const rule<struct stringhexchar, char> stringhexchar = "stringhexchar";
		const rule<struct stringcontent1, std::string> stringcontent1 = "stringcontent1";
		const rule<struct stringcontent2, std::string> stringcontent2 = "stringcontent2";
		const rule<struct quotedstring, std::string> quotedstring = "quotedstring";
		const rule<struct fieldstring, std::string> fieldstring = "fieldstring";
		const rule<struct identorqstring, std::string> identorqstring = "identorqstring";

		const auto identifier_def = lexeme[alpha >> *(alnum | char_('_') | char_('-'))];
		const auto layeridtype_def = identifier[layeridparse{}];
		const auto classidtype_def = identifier[classidparse{}];

		const auto stringescape_def = no_skip[char_("nNrRtT")][stringescapeparse{}];
		const auto stringhexchar_def = no_skip[repeat(1,2)[xdigit]][hexcharparse{}];
		const auto stringcontent1_def = no_skip[*((!(char_("\\\"") | eol) >> char_[appendchar{}]) |
							  (char_('\\') >> (stringescape[appendchar{}] |
									   stringhexchar[appendchar{}] |
									   eol)))];
		const auto stringcontent2_def = no_skip[*((!(char_("\\'") | eol) >> char_[appendchar{}]) |
							  (char_('\\') >> (stringescape[appendchar{}] |
									   stringhexchar[appendchar{}] |
									   eol)))];
		const auto quotedstring_def = lexeme[(lit('"') >> stringcontent1 >> lit('"')) |
						     (lit('\'') >> stringcontent2 >> lit('\''))];

		const auto fieldstring_def = lexeme[lit('[') >> +(alnum | char_('_') | char_('-')) >> lit(']')];

		const auto identorqstring_def = identifier | quotedstring;

		BOOST_SPIRIT_DEFINE(identifier, layeridtype, classidtype, stringescape, stringhexchar,
				    stringcontent1, stringcontent2, quotedstring, fieldstring, identorqstring);

		//-----------------------------------------------------------------------------
		// Expressions
		//-----------------------------------------------------------------------------

		struct exprcopy {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = _attr(ctx);
			}
		};

		struct exprfloatpercent {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = Expr::create(0.01 * _val(ctx)->as_double());
			}
		};

		struct exprnumber {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = Expr::create_number(_attr(ctx));
			}
		};

		struct exprbool {
			bool m_val;
			exprbool(bool b) : m_val(b) {}
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = Expr::create(m_val);
			}
		};

		struct exprstring {
			template<typename Context>
			void operator()(Context& ctx) {
				std::string s(_attr(ctx));
				Expr::exprlist_t e;
				while (!s.empty()) {
					std::string::size_type n(s.find('['));
					if (n) {
						e.push_back(Expr::create(s.substr(0, n)));
						s.erase(0, n);
						continue;
					}
					n = s.find(']');
					if (n == std::string::npos)
						boost::throw_exception(x3::expectation_failure<decltype(_where(ctx).begin())>(_where(ctx).begin(), "invalid string field expression (missing ])"));
					if (n == 1)
						boost::throw_exception(x3::expectation_failure<decltype(_where(ctx).begin())>(_where(ctx).begin(), "invalid string field expression (empty)"));
					e.push_back(Expr::create_field(s.substr(1, n-1)));
					s.erase(0, n+1);
				}
				if (e.empty())
					e.push_back(Expr::create(std::string()));
				while (e.size() >= 2) {
					Expr::exprlist_t e1;
					e1.reserve(2);
					e1.push_back(e[e.size()-2]);
					e1.push_back(e[e.size()-1]);
					e.resize(e.size()-1);
					e.back() = Expr::create(Expr::func_t::plus, Expr::create(e1));
				}
				_val(ctx) = e.front();
			}
		};

		struct exprfieldstring {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = Expr::create_field(_attr(ctx));
			}
		};

		struct colorcss {
			template<typename Context>
			void operator()(Context& ctx) {
				uint32_t col(_attr(ctx));
				Color c;
				c.set_rgba_uint8((col >> 16) & 255, (col >> 8) & 255, col & 255, 255);
				_val(ctx) = Expr::create(c);
			}
		};

		struct colorset {
			Color m_col;
			colorset(double r = 0, double g = 0, double b = 0, double a = 1) : m_col(r, g, b, a) {}
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = Expr::create(m_col);
			}
		};

		struct colorhex {
			template<typename Context>
			void operator()(Context& ctx) {
		        	if (at_c<1>(_attr(ctx))) {
					uint32_t col(((0xfff & at_c<0>(_attr(ctx))) << 12) | (0xfff & *at_c<1>(_attr(ctx))));
					Color c;
					c.set_rgba_uint8((col >> 16) & 255, (col >> 8) & 255, col & 255, 255);
					_val(ctx) = Expr::create(c);
				} else {
					uint16_t col(0xfff & at_c<0>(_attr(ctx)));
					Color c;
					c.set_rgba_uint4((col >> 8) & 15, (col >> 4) & 15, col & 15, 15);
					_val(ctx) = Expr::create(c);
				}
			}
		};

		struct exprfunc {
			Expr::func_t m_func;
			exprfunc(Expr::func_t func) : m_func(func) {}
			template<typename Context>
			void operator()(Context& ctx) {
				auto& g(get<parser_tag>(ctx).get());
				Expr::const_ptr_t expr(_attr(ctx));
				if (m_func == Expr::func_t::url) {
					Expr::exprlist_t e;
					if (!expr->is_convertible(e))
						throw std::runtime_error("exprfunc: cannot convert to expr list");
					e.push_back(Expr::create_directory(g.m_dirname));
					expr = Expr::create(e);
				}
				_val(ctx) = Expr::create(m_func, expr);
			}
		};

		struct exprvariable {
			template<typename Context>
			void operator()(Context& ctx) {
	        		_val(ctx) = Expr::create_varref(_attr(ctx));
			}
		};

		struct exprcompop {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = Expr::create(_attr(ctx));
			}
		};

		struct exprkeyword {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = Expr::create(_attr(ctx));
			}
		};

		struct exprnull {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = Expr::create();
			}
		};

		struct exprbinop {
			template<typename Context>
			void operator()(Context& ctx) {
				Expr::exprlist_t v;
				v.push_back(_val(ctx));
				v.push_back(at_c<1>(_attr(ctx)));
				_val(ctx) = Expr::create(at_c<0>(_attr(ctx)), Expr::create(v));
			}
		};

		struct exprlist {
			template<typename Context>
			void operator()(Context& ctx) {
				if (_attr(ctx).size() == 1)
					_val(ctx) = _attr(ctx).front();
				else
					_val(ctx) = Expr::create(_attr(ctx));
			}
		};

		const rule<struct floatnumberpercent, Expr::const_ptr_t> floatnumberpercent = "floatnumberpercent";
		const rule<struct exprterminal, Expr::const_ptr_t> exprterminal = "exprterminal";
		const rule<struct exprmul, Expr::const_ptr_t> exprmul = "exprmul";
		const rule<struct expradd, Expr::const_ptr_t> expradd = "expradd";
		const rule<struct expr, Expr::const_ptr_t> expr = "expr";

		const auto floatnumberpercent_def = floatnumberstr[exprnumber{}] >> -(lit('%')[exprfloatpercent{}]);

		const auto exprterminal_def = ((lit('(') > expr > lit(')'))[exprcopy{}] |
					       // numbers
					       floatnumberpercent[exprcopy{}] |
					       // literals
					       lit("true")[exprbool{true}] |
					       lit("false")[exprbool{false}] |
					       // string
					       quotedstring[exprstring{}] |
					       fieldstring[exprfieldstring{}] |
					       // CSS colors
					       csscolortype[colorcss{}] |
					       // Other color functions
					       lexeme[lit('#') >> hex3 >> -hex3][colorhex{}] |
					       (lit("rgb") >> lit('(') > expr > lit(')'))[exprfunc{Expr::func_t::colrgb}] |
					       (lit("rgba") >> lit('(') > expr > lit(')'))[exprfunc{Expr::func_t::colrgba}] |
					       (lit("hsl") >> lit('(') > expr > lit(')'))[exprfunc{Expr::func_t::colhsl}] |
					       (lit("hsla") >> lit('(') > expr > lit(')'))[exprfunc{Expr::func_t::colhsla}] |
					       (lit("lighten") >> lit('(') > expr > lit(')'))[exprfunc{Expr::func_t::collighten}] |
					       (lit("darken") >> lit('(') > expr > lit(')'))[exprfunc{Expr::func_t::coldarken}] |
					       (lit("saturate") >> lit('(') > expr > lit(')'))[exprfunc{Expr::func_t::colsaturate}] |
					       (lit("desaturate") >> lit('(') > expr > lit(')'))[exprfunc{Expr::func_t::coldesaturate}] |
					       (lit("fadein") >> lit('(') > expr > lit(')'))[exprfunc{Expr::func_t::colfadein}] |
					       (lit("fadeout") >> lit('(') > expr > lit(')'))[exprfunc{Expr::func_t::colfadeout}] |
					       (lit("spin") >> lit('(') > expr > lit(')'))[exprfunc{Expr::func_t::colspin}] |
					       (lit("mix") >> lit('(') > expr > lit(')'))[exprfunc{Expr::func_t::colmix}] |
					       // variables
					       (lit('@') > identifier)[exprvariable{}] |
					       // special
					       (lit("scale-hsla") >> lit('(') > expr > lit(')'))[exprfunc{Expr::func_t::scalehsla}] |
					       (lit("url") >> lit('(') > expr > lit(')'))[exprfunc{Expr::func_t::url}] |
					       compoptype[exprcompop{}] |
					       keywordtype[exprkeyword{}] |
					       lit("null")[exprnull{}]);

		const auto exprmul_def = exprterminal[exprcopy{}] >> *((exprmulop > exprterminal)[exprbinop{}]);

		const auto expradd_def = exprmul[exprcopy{}] >> *((expraddop > exprmul)[exprbinop{}]);

		const auto expr_def = (expradd % lit(','))[exprlist{}];

		BOOST_SPIRIT_DEFINE(floatnumberpercent, exprterminal, exprmul, expradd, expr);

		//-----------------------------------------------------------------------------
		// Layers
		//-----------------------------------------------------------------------------

		struct varprefixassign {
			template<typename Context>
			void operator()(Context& ctx) {
				if (_attr(ctx))
					_val(ctx) = *_attr(ctx);
				else
					_val(ctx) = std::string();
			}
		};

		struct varassign {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = VariableAssignments::Assignment(at_c<1>(_attr(ctx)), at_c<2>(_attr(ctx)), at_c<0>(_attr(ctx)));
			}
		};

		struct condcondition1 {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = Expr::create(_attr(ctx));
			}
		};

		struct condcondition2 {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = Expr::create_copylayer(_attr(ctx));
			}
		};

		struct condcondition3 {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = Expr::create(Expr::create_field(at_c<0>(_attr(ctx))), at_c<1>(_attr(ctx)), at_c<2>(_attr(ctx)));
			}
		};

		struct stmtvar {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = Statement::ptr_t(new VariableAssignments(_attr(ctx)));
			}
		};

		struct addstmtlistvar {
			template<typename Context>
			void operator()(Context& ctx) {
				VariableAssignments::assignments_t a;
				a.push_back(_attr(ctx));
				_val(ctx).push_back(Statement::ptr_t(new VariableAssignments(a)));
			}
		};

		struct addstmtlist {
			template<typename Context>
			void operator()(Context& ctx) {
				if (_attr(ctx))
					_val(ctx).push_back(_attr(ctx));
			}
		};

		struct subblockalloc {
			template<typename Context>
			void operator()(Context& ctx) {
				_val(ctx) = Statement::ptr_t(new SubBlock(at_c<0>(_attr(ctx)), at_c<1>(_attr(ctx))));
			}
		};

		const rule<struct layercond, Expr::const_ptr_t> layercond = "layercond";
		const rule<struct layerconds, SubBlock::ConditionChain> layerconds = "layerconds";
		const rule<struct layercondlist, SubBlock::Conditions> layercondlist = "layercondlist";

		const rule<struct varprefix, std::string> varprefix = "varprefix";
		const rule<struct paramassignment, VariableAssignments::Assignment> paramassignment = "paramassignment";

		const rule<struct varassignments, VariableAssignments::assignments_t> varassignments = "varassignments";
		const rule<struct statement, Statement::const_ptr_t> statement = "statement";
		const rule<struct statementlist, Statements::statements_t> statementlist = "statementlist";
		const rule<struct subblock, Statement::const_ptr_t> subblock = "subblock";

		const auto layercond_def = ((lit('#') > layeridtype)[condcondition1{}] |
					    (lit('.') > classidtype)[condcondition1{}] |
					    (lit('[') > identorqstring > comparisonoptype > expr > lit(']'))[condcondition3{}] |
					    (lit("::") > identifier)[condcondition2{}]);

		const auto layerconds_def = +layercond;

		const auto layercondlist_def = (layerconds % lit(',')) >> -lit(',');

		const auto varprefix_def = (-(identifier >> lit('/')))[varprefixassign{}];
		const auto paramassignment_def = (varprefix >> variablenametype >> lit(':') >> expr)[varassign{}];

		const auto varassignments_def = +(paramassignment >> lit(';'));
		const auto statement_def = varassignments[stmtvar{}] | subblock[exprcopy{}];
		const auto statementlist_def = *(statement[addstmtlist{}]) >> -(paramassignment[addstmtlistvar{}]);
		const auto subblock_def = (layercondlist > lit('{') > statementlist > lit('}'))[subblockalloc{}];

		BOOST_SPIRIT_DEFINE(layercond, layerconds, layercondlist, varprefix, paramassignment,
				    statementlist, statement, varassignments, subblock);

		//-----------------------------------------------------------------------------
		// Statements
		//-----------------------------------------------------------------------------

		struct endofinput {
			template<typename Context>
			void operator()(Context& ctx) {
				//auto& g(get<parser_tag>(ctx).get());
				//g.finalize();
			}
		};

		struct dovarassign {
			template<typename Context>
			void operator()(Context& ctx) {
				auto& g(get<parser_tag>(ctx).get());
		        	g.set_variable(at_c<0>(_attr(ctx)), at_c<1>(_attr(ctx)));
			}
		};

		struct setbackgroundcol {
			template<typename Context>
			void operator()(Context& ctx) {
				auto& g(get<parser_tag>(ctx).get());
				g.m_backgroundcol = _attr(ctx);
			}
		};

		struct addattributes {
			template<typename Context>
			void operator()(Context& ctx) {
				auto& g(get<parser_tag>(ctx).get());
				g.m_attributes.push_back(_attr(ctx));
			}
		};

		struct addstatement {
			template<typename Context>
			void operator()(Context& ctx) {
				auto& g(get<parser_tag>(ctx).get());
				g.m_statements.push_back(_attr(ctx));
			}
		};

		const rule<struct varassign> varassign = "varassign";
		const rule<struct mapsection> mapsection = "mapsection";
		const rule<struct mapstatement> mapstatement = "mapstatement";
		const rule<struct file> file = "file";

		const auto varassign_def = (lit('@') > identifier > lit(':') > expr > lit(';'))[dovarassign{}];

		const auto mapsection_def = lit("Map") > lit('{') > *mapstatement > lit('}');
		const auto mapstatement_def = ((lit("background-color") > lit(':') > expr > lit(';'))[setbackgroundcol{}]);

		const auto file_def = *(statement[addstatement{}] |
					mapsection |
					varassign) >> eoi[endofinput{}];

		BOOST_SPIRIT_DEFINE(varassign, mapsection, mapstatement, file);
	};
};

void CartoCSS::parse_stylefiles(const std::vector<std::string>& fns, const std::string& dirname, std::ostream *msg)
{
	namespace x3 = boost::spirit::x3;
	using Iterator = boost::spirit::istream_iterator;
	using PosIterator = boost::spirit::line_pos_iterator<Iterator>;
	cartocss_parser<PosIterator> g(*this, dirname, msg);
	std::vector<PosIterator> iters;
	for (const std::string& fn : fns) {
		std::ifstream is(fn.c_str());
		if (!is.is_open())
			throw std::runtime_error("Cannot open file " + fn);
		Iterator f(is >> std::noskipws), l;
		PosIterator pf(f), pi(pf), pl;
		iters.push_back(pf);
		try {
			if (!x3::phrase_parse(pi, pl, x3::with<parser::parser_tag>(std::ref(g))[parser::file], skipper::skip))
				parse_error(pf, pi, pl);
		} catch (const x3::expectation_failure<PosIterator>& x) {
			parse_error(pf, x.where(), pl, x.which());
		}
	}
	if (false)
		std::cerr << "finalizing..." << std::endl;
	try {
		g.finalize();
	} catch (const x3::expectation_failure<PosIterator>& x) {
		for (const PosIterator& pf : iters)
			for (PosIterator pi(pf), pl; pi != pl; ++pi)
				if (pi == x.where())
					parse_error(pf, x.where(), pl, x.which());
		parse_error(x.where(), x.which());
	}
}
