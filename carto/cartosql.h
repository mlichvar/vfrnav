//
// C++ Interface: cartosql
//
// Description: CartoSQL sql expression parser
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef CARTOSQL_HH
#define CARTOSQL_HH

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <boost/smart_ptr/intrusive_ptr.hpp>
#include <atomic>
#include <limits>
#include <vector>
#include <set>
#include <map>
#include <glibmm.h>

#include "geom.h"
#include "osmdb.h"

class HibernateWriteStream;
class HibernateReadStream;
class HibernateReadBuffer;

class CartoSQL {
public:
	class Value;

	typedef std::set<unsigned int> columnset_t;

	class TableRowValues : public std::vector<Value> {
	public:
		int compare_value(const TableRowValues& x) const;

		int compare(const TableRowValues& x) const;
		bool operator==(const TableRowValues& x) const { return !compare(x); }
		bool operator!=(const TableRowValues& x) const { return !operator==(x); }
		bool operator<(const TableRowValues& x) const { return compare(x) < 0; }
		bool operator<=(const TableRowValues& x) const { return compare(x) <= 0; }
		bool operator>(const TableRowValues& x) const { return compare(x) > 0; }
		bool operator>=(const TableRowValues& x) const { return compare(x) >= 0; }

		template<class Archive> void hibernate(Archive& ar) {
			size_type sz(size());
			ar.ioleb(sz);
			this->resize(sz);
			for (iterator i(begin()), e(end()); i != e; ++i)
				i->hibernate(ar);
		}
	};

	class TableRow : public TableRowValues {
	public:
		typedef OSMDB::Object::id_t osmid_t;
		typedef std::set<osmid_t> osmidset_t;

		void swap(TableRow& x);
		const osmidset_t& get_osmidset(void) const { return m_osmidset; }
		osmidset_t& get_osmidset(void) { return m_osmidset; }
		void set_osmidset(const osmidset_t& s) { m_osmidset = s; }
		void set_osmidset(osmidset_t&& s) { m_osmidset.swap(s); }
		void add_osmidset(osmid_t id) { m_osmidset.insert(id); }
		void add_osmidset(const osmidset_t& s) { m_osmidset.insert(s.begin(), s.end()); }
		static std::string get_osmidset_string(const osmidset_t& s);
		std::string get_osmidset_string(void) const { return get_osmidset_string(get_osmidset()); }
		TableRow get_columnset(const columnset_t& colset) const;

		template<class Archive> void hibernate(Archive& ar) {
			TableRowValues::hibernate(ar);
			osmidset_t::size_type n(m_osmidset.size());
			ar.ioleb(n);
			if (ar.is_load()) {
				m_osmidset.clear();
				for (; n; --n) {
					osmid_t id(0);
					ar.ioleb(id);
					m_osmidset.insert(id);
				}
			} else {
				for (osmidset_t::iterator i(begin()), e(end()); i != e; ++i) {
					osmid_t id(*i);
					ar.ioleb(id);
				}
			}
		}

	protected:
		osmidset_t m_osmidset;
	};

	class Table : public std::vector<TableRow> {
	public:
		int compare_value(const Table& x) const;

		int compare(const Table& x) const;
		bool operator==(const Table& x) const { return !compare(x); }
		bool operator!=(const Table& x) const { return !operator==(x); }
		bool operator<(const Table& x) const { return compare(x) < 0; }
		bool operator<=(const Table& x) const { return compare(x) <= 0; }
		bool operator>(const Table& x) const { return compare(x) > 0; }
		bool operator>=(const Table& x) const { return compare(x) >= 0; }

		template<class Archive> void hibernate(Archive& ar) {
			size_type sz(size());
			ar.ioleb(sz);
			this->resize(sz);
			for (iterator i(begin()), e(end()); i != e; ++i)
				i->hibernate(ar);
		}

		void eliminate_duplicates(void);
		Table& append(const Table& tbl);
		Table& append(Table&& tbl);
		Table get_columnset(const columnset_t& colset) const;

		struct ValueSorter {
			bool operator()(const TableRow& a, const TableRow& b) const {
				return a.compare_value(b) < 0;
			}
		};

		class ValueColumnSorter {
		public:
			typedef std::vector<unsigned int> colindices_t;

			ValueColumnSorter(const colindices_t& cols = colindices_t()) : m_cols(cols) {}

			int compare(const TableRow& a, const TableRow& b) const;
			bool operator()(const TableRow& a, const TableRow& b) const {
				return compare(a, b) < 0;
			}

		protected:
			colindices_t m_cols;
		};
	};

	class IndexTableRow : public TableRowValues {
	public:
		typedef Table::size_type index_t;

		void swap(IndexTableRow& x);
		index_t get_index(void) const { return m_index; }
		void set_index(index_t idx) { m_index = idx; }

		template<class Archive> void hibernate(Archive& ar) {
			TableRowValues::hibernate(ar);
			ar.ioleb(m_index);
		}

	protected:
		index_t m_index;
	};

	class IndexTable : public std::vector<IndexTableRow> {
	public:
		int compare_value(const IndexTable& x) const;

		int compare(const IndexTable& x) const;
		bool operator==(const IndexTable& x) const { return !compare(x); }
		bool operator!=(const IndexTable& x) const { return !operator==(x); }
		bool operator<(const IndexTable& x) const { return compare(x) < 0; }
		bool operator<=(const IndexTable& x) const { return compare(x) <= 0; }
		bool operator>(const IndexTable& x) const { return compare(x) > 0; }
		bool operator>=(const IndexTable& x) const { return compare(x) >= 0; }

		template<class Archive> void hibernate(Archive& ar) {
			size_type sz(size());
			ar.ioleb(sz);
			this->resize(sz);
			for (iterator i(begin()), e(end()); i != e; ++i)
				i->hibernate(ar);
		}

		template <typename T>
		void permute(T& x) const;

		typedef std::tuple<IndexTableRow::index_t,IndexTableRow::index_t> indexrange_t;
		typedef std::vector<indexrange_t> indexranges_t;
		indexranges_t get_equalranges(void) const;

		struct ValueSorter {
			bool operator()(const IndexTableRow& a, const IndexTableRow& b) const {
				return a.compare_value(b) < 0;
			}
		};
	};

	class Value {
	public:
		class ValueError : public std::runtime_error {
		public:
			explicit ValueError(const std::string& x);
		};

		enum class type_t : uint8_t {
			null = 0,
			string,
			floatingpoint,
			integer,
			boolean,
			array,
			tags,
			points,
			lines,
			polygons,
			rectangle,
			osmdbobject
		};

		typedef std::vector<Value> array_t;
		typedef std::map<std::string,Value> tags_t;

		Value(void);
		Value(const std::string& val);
		Value(const char *val);
		Value(double val);
		Value(int64_t val);
		Value(bool val);
		Value(const array_t& v);
		Value(array_t&& v);
		Value(const tags_t& v);
		Value(tags_t&& v);
		Value(const MultiPoint& v);
		Value(MultiPoint&& v);
		Value(const MultiLineString& v);
		Value(MultiLineString&& v);
		Value(const MultiPolygonHole& v);
		Value(MultiPolygonHole&& v);
		Value(const Rect& v);
		Value(const OSMDB::Object::const_ptr_t& v);
		Value(OSMDB::Object::const_ptr_t&& v);
		Value(const Value& v);
		Value(Value&& v);
		~Value();

		void set_null(void);
		Value& operator=(const std::string& v);
		Value& operator=(const char *v);
		Value& operator=(double v);
		Value& operator=(int64_t v);
		Value& operator=(bool v);
		Value& operator=(const array_t& v);
		Value& operator=(array_t&& v);
		Value& operator=(const tags_t& v);
		Value& operator=(tags_t&& v);
		Value& operator=(const Table& v);
		Value& operator=(Table&& v);
		Value& operator=(const MultiPoint& v);
		Value& operator=(MultiPoint&& v);
		Value& operator=(const MultiLineString& v);
		Value& operator=(MultiLineString&& v);
		Value& operator=(const MultiPolygonHole& v);
		Value& operator=(MultiPolygonHole&& v);
		Value& operator=(const Rect& v);
		Value& operator=(const OSMDB::Object::const_ptr_t& v);
		Value& operator=(OSMDB::Object::const_ptr_t&& v);
		Value& operator=(const Value& v);
		Value& operator=(Value&& v);

		type_t get_type(void) const { return m_type; }
		bool is_null(void) const { return get_type() == type_t::null; }
		bool is_boolean(void) const { return get_type() == type_t::boolean; }
		bool is_integer(void) const { return get_type() == type_t::integer; }
		bool is_double(void) const { return get_type() == type_t::floatingpoint; }
		bool is_string(void) const { return get_type() == type_t::string; }
		bool is_array(void) const { return get_type() == type_t::array; }
		bool is_tags(void) const { return get_type() == type_t::tags; }
		bool is_points(void) const { return get_type() == type_t::points; }
		bool is_lines(void) const { return get_type() == type_t::lines; }
		bool is_polygons(void) const { return get_type() == type_t::polygons; }
		bool is_rectangle(void) const { return get_type() == type_t::rectangle; }
		bool is_osmdbobject(void) const { return get_type() == type_t::osmdbobject; }
		bool is_integral(void) const;
		bool is_numeric(void) const;
		bool is_geometry(void) const;
		bool is_geom_point(void) const;
		bool is_geom_points(void) const;
		bool is_geom_line(void) const;
		bool is_geom_lines(void) const;
		bool is_geom_polygons(void) const;

		operator bool(void) const;
		operator int64_t(void) const;
		operator double(void) const;
		operator std::string(void) const;

		bool get_boolean(void) const;
		int64_t get_integer(void) const;
		double get_double(void) const;
		const std::string& get_string(void) const;
		const array_t& get_array(void) const;
		const tags_t& get_tags(void) const;
		const Point& get_point(void) const;
		const MultiPoint& get_points(void) const;
		const LineString& get_line(void) const;
		const MultiLineString& get_lines(void) const;
		const MultiPolygonHole& get_polygons(void) const;
		const Rect& get_rectangle(void) const;
		const OSMDB::Object::const_ptr_t& get_osmdbobject(void) const;
		const OSMDB::Object::tags_t& get_osmdbobject_tags(void) const;

		Value as_boolean(void) const;
		Value as_integer(void) const;
		Value as_double(void) const;
		Value as_string(void) const;
		Value as_type(type_t typ) const;

		int compare_value(const Value& x) const;

		int compare(const Value& x) const;
		bool operator==(const Value& x) const { return !compare(x); }
		bool operator!=(const Value& x) const { return !operator==(x); }
		bool operator<(const Value& x) const { return compare(x) < 0; }
		bool operator<=(const Value& x) const { return compare(x) <= 0; }
		bool operator>(const Value& x) const { return compare(x) > 0; }
		bool operator>=(const Value& x) const { return compare(x) >= 0; }

		static const std::string& get_sqltypename(type_t typ);
		const std::string& get_sqltypename(void) const { return get_sqltypename(get_type()); }

		enum class sqlstringflags_t : uint8_t {
			none     = 0,
			array    = 1 << 0,
			tags     = 1 << 1,
			geometry = 1 << 2,
			all      = array | tags | geometry
		};

		std::string get_sqlstring(sqlstringflags_t flags = sqlstringflags_t::all) const;

		template<class Archive> void hibernate(Archive& ar) {
			if (ar.is_load())
				set_null();
			ar.ioenum(m_type);
			switch (m_type) {
			case type_t::string:
				if (ar.is_load()) {
					std::string x;
					ar.io(x);
					::new(&m_string) string_t(x);
					break;
				}
				ar.io(m_string);
				break;

			case type_t::floatingpoint:
				ar.io(m_double);
				break;

			case type_t::integer:
				ar.ioleb(m_int);
				break;

			case type_t::boolean:
				ar.iouint8(m_bool);
				break;

			case type_t::array:
			{
				if (ar.is_load())
					::new(&m_array) array_t();
				array_t::size_type sz(m_array.size());
				ar.ioleb(sz);
				m_array.resize(sz);
				for (array_t::iterator i(m_array.begin()), e(m_array.end()); i != e; ++i)
					i->hibernate(ar);
				break;
			}

			case type_t::tags:
			{
				if (ar.is_load())
					::new(&m_tags) tags_t();
				array_t::size_type sz(m_tags.size());
				ar.ioleb(sz);
				if (ar.is_load()) {
					m_tags.clear();
					for (; sz; --sz) {
						std::string tag;
						ar.io(tag);
						m_tags[tag].hibernate(ar);
					}
				} else {
					for (tags_t::iterator i(m_tags.begin()), e(m_tags.end()); i != e; ++i) {
						std::string tag(i->first);
						ar.io(tag);
						i->second.hibernate(ar);
					}
				}
				break;
			}

			case type_t::points:
				if (ar.is_load())
					::new(&m_points) MultiPoint();
				m_points.hibernate_binary(ar);
				break;

			case type_t::lines:
				if (ar.is_load())
					::new(&m_lines) MultiLineString();
				m_lines.hibernate_binary(ar);
				break;

			case type_t::polygons:
				if (ar.is_load())
					::new(&m_polygons) MultiPolygonHole();
				m_polygons.hibernate_binary(ar);
				break;

			case type_t::rectangle:
				if (ar.is_load())
					::new(&m_rectangle) Rect();
				m_rectangle.hibernate_binary(ar);
				break;

			case type_t::osmdbobject:
				if (ar.is_load()) {
					::new(&m_osmdbobject) osmdbobjptr_t();
					m_osmdbobject = OSMDB::Object::create(ar);
					break;
				}
				m_osmdbobject->store(ar);
				break;

			default:
				break;
			}
		}

		struct ValueOrdering {
			bool operator()(const Value& a, const Value& b) const { return a.compare_value(b) < 0; }
		};

	protected:
		typedef std::string string_t;
		typedef OSMDB::Object::const_ptr_t osmdbobjptr_t;
		union {
			string_t m_string;
			double m_double;
			int64_t m_int;
			bool m_bool;
			array_t m_array;
			tags_t m_tags;
			Table m_table;
			MultiPoint m_points;
			MultiLineString m_lines;
			MultiPolygonHole m_polygons;
			Rect m_rectangle;
			osmdbobjptr_t m_osmdbobject;
		};
		type_t m_type;
	};

	class Expr;
	class ExprValue;
	class ExprVariable;
	class ExprFunc;
	class ExprFuncAggregate;
	class ExprFuncTable;
	class ExprCase;
	class ExprCast;
	class ExprTagMember;
	class ExprColumn;
	class ExprTableColumn;
	class ExprOSMSpecial;
	class ExprOSMTag;
	class ExprTable;
	class ExprOSMTable;
	class ExprLiteralTable;
	class ExprTableFunc;
	class ExprSelect;
	class ExprOSMSelect;
	class ExprCompound;
	class ExprJoin;
	class ExprTableReference;
	class ExprCommonTables;

	class Visitor {
	public:
		virtual void visit(const Expr& e) {}
		virtual void visit(const ExprValue& e) { visit(static_cast<const Expr&>(e)); }
		virtual void visit(const ExprVariable& e) { visit(static_cast<const Expr&>(e)); }
		virtual void visit(const ExprFunc& e) { visit(static_cast<const Expr&>(e)); }
		virtual void visit(const ExprFuncAggregate& e) { visit(static_cast<const Expr&>(e)); }
		virtual void visit(const ExprFuncTable& e) { visit(static_cast<const Expr&>(e)); }
		virtual void visit(const ExprCase& e) { visit(static_cast<const Expr&>(e)); }
		virtual void visit(const ExprCast& e) { visit(static_cast<const Expr&>(e)); }
		virtual void visit(const ExprTagMember& e) { visit(static_cast<const Expr&>(e)); }
		virtual void visit(const ExprColumn& e) { visit(static_cast<const Expr&>(e)); }
		virtual void visit(const ExprTableColumn& e) { visit(static_cast<const Expr&>(e)); }
		virtual void visit(const ExprOSMSpecial& e) { visit(static_cast<const Expr&>(e)); }
		virtual void visit(const ExprOSMTag& e) { visit(static_cast<const Expr&>(e)); }
		virtual void visit(const ExprTable& e) {}
		virtual void visit(const ExprOSMTable& e) { visit(static_cast<const ExprTable&>(e)); }
		virtual void visit(const ExprLiteralTable& e) { visit(static_cast<const ExprTable&>(e)); }
		virtual void visit(const ExprTableFunc& e) { visit(static_cast<const ExprTable&>(e)); }
		virtual void visit(const ExprSelect& e) { visit(static_cast<const ExprTable&>(e)); }
		virtual void visit(const ExprOSMSelect& e) { visit(static_cast<const ExprTable&>(e)); }
		virtual void visit(const ExprCompound& e) { visit(static_cast<const ExprTable&>(e)); }
		virtual void visit(const ExprJoin& e) { visit(static_cast<const ExprTable&>(e)); }
		virtual void visit(const ExprTableReference& e) { visit(static_cast<const ExprTable&>(e)); }
		virtual void visit(const ExprCommonTables& e) { visit(static_cast<const ExprTable&>(e)); }
		virtual void visitend(const Expr& e) {}
		virtual void visitend(const ExprVariable& e) { visitend(static_cast<const Expr&>(e)); }
		virtual void visitend(const ExprFunc& e) { visitend(static_cast<const Expr&>(e)); }
		virtual void visitend(const ExprFuncAggregate& e) { visitend(static_cast<const Expr&>(e)); }
		virtual void visitend(const ExprFuncTable& e) { visitend(static_cast<const Expr&>(e)); }
		virtual void visitend(const ExprCase& e) { visitend(static_cast<const Expr&>(e)); }
		virtual void visitend(const ExprCast& e) { visitend(static_cast<const Expr&>(e)); }
		virtual void visitend(const ExprTagMember& e) { visitend(static_cast<const Expr&>(e)); }
		virtual void visitend(const ExprColumn& e) { visitend(static_cast<const Expr&>(e)); }
		virtual void visitend(const ExprTableColumn& e) { visitend(static_cast<const Expr&>(e)); }
		virtual void visitend(const ExprOSMSpecial& e) { visitend(static_cast<const Expr&>(e)); }
		virtual void visitend(const ExprOSMTag& e) { visitend(static_cast<const Expr&>(e)); }
		virtual void visitend(const ExprTable& e) {}
		virtual void visitend(const ExprOSMTable& e) { visitend(static_cast<const ExprTable&>(e)); }
		virtual void visitend(const ExprLiteralTable& e) { visitend(static_cast<const ExprTable&>(e)); }
		virtual void visitend(const ExprTableFunc& e) { visitend(static_cast<const ExprTable&>(e)); }
		virtual void visitend(const ExprSelect& e) { visitend(static_cast<const ExprTable&>(e)); }
		virtual void visitend(const ExprOSMSelect& e) { visitend(static_cast<const ExprTable&>(e)); }
		virtual void visitend(const ExprCompound& e) { visitend(static_cast<const ExprTable&>(e)); }
		virtual void visitend(const ExprJoin& e) { visitend(static_cast<const ExprTable&>(e)); }
		virtual void visitend(const ExprTableReference& e) { visitend(static_cast<const ExprTable&>(e)); }
		virtual void visitend(const ExprCommonTables& e) { visitend(static_cast<const ExprTable&>(e)); }
		virtual bool descend(const Expr& e) { return true; }
		virtual bool descend(const ExprFunc& e) { return descend(static_cast<const Expr&>(e)); }
		virtual bool descend(const ExprFuncAggregate& e) { return descend(static_cast<const Expr&>(e)); }
		virtual bool descend(const ExprFuncTable& e) { return descend(static_cast<const Expr&>(e)); }
		virtual bool descend(const ExprCase& e) { return descend(static_cast<const Expr&>(e)); }
		virtual bool descend(const ExprCast& e) { return descend(static_cast<const Expr&>(e)); }
		virtual bool descend(const ExprTagMember& e) { return descend(static_cast<const Expr&>(e)); }
		virtual bool descend(const ExprTable& e) { return true; }
		virtual bool descend(const ExprTableFunc& e) { return descend(static_cast<const ExprTable&>(e)); }
		virtual bool descend(const ExprSelect& e) { return descend(static_cast<const ExprTable&>(e)); }
		virtual bool descend(const ExprOSMSelect& e) { return descend(static_cast<const ExprTable&>(e)); }
		virtual bool descend(const ExprCompound& e) { return descend(static_cast<const ExprTable&>(e)); }
		virtual bool descend(const ExprJoin& e) { return descend(static_cast<const ExprTable&>(e)); }
		virtual bool descend(const ExprCommonTables& e) { return descend(static_cast<const ExprTable&>(e)); }
		virtual void select_result_table_available(const CartoSQL *csql, const ExprTable& e) {}
		virtual boost::intrusive_ptr<const Expr> modify(const CartoSQL *csql, const Expr& e) { return boost::intrusive_ptr<const Expr>(); }
		virtual boost::intrusive_ptr<const Expr> modify(const CartoSQL *csql, const ExprValue& e) { return modify(csql, static_cast<const Expr&>(e)); }
		virtual boost::intrusive_ptr<const Expr> modify(const CartoSQL *csql, const ExprVariable& e) { return modify(csql, static_cast<const Expr&>(e)); }
		virtual boost::intrusive_ptr<const Expr> modify(const CartoSQL *csql, const ExprFunc& e) { return modify(csql, static_cast<const Expr&>(e)); }
		virtual boost::intrusive_ptr<const Expr> modify(const CartoSQL *csql, const ExprFuncAggregate& e) { return modify(csql, static_cast<const Expr&>(e)); }
		virtual boost::intrusive_ptr<const Expr> modify(const CartoSQL *csql, const ExprFuncTable& e) { return modify(csql, static_cast<const Expr&>(e)); }
		virtual boost::intrusive_ptr<const Expr> modify(const CartoSQL *csql, const ExprCase& e) { return modify(csql, static_cast<const Expr&>(e)); }
		virtual boost::intrusive_ptr<const Expr> modify(const CartoSQL *csql, const ExprCast& e) { return modify(csql, static_cast<const Expr&>(e)); }
		virtual boost::intrusive_ptr<const Expr> modify(const CartoSQL *csql, const ExprTagMember& e) { return modify(csql, static_cast<const Expr&>(e)); }
		virtual boost::intrusive_ptr<const Expr> modify(const CartoSQL *csql, const ExprColumn& e) { return modify(csql, static_cast<const Expr&>(e)); }
		virtual boost::intrusive_ptr<const Expr> modify(const CartoSQL *csql, const ExprTableColumn& e) { return modify(csql, static_cast<const Expr&>(e)); }
		virtual boost::intrusive_ptr<const Expr> modify(const CartoSQL *csql, const ExprOSMSpecial& e) { return modify(csql, static_cast<const Expr&>(e)); }
		virtual boost::intrusive_ptr<const Expr> modify(const CartoSQL *csql, const ExprOSMTag& e) { return modify(csql, static_cast<const Expr&>(e)); }
		virtual boost::intrusive_ptr<const ExprTable> modify(const CartoSQL *csql, const ExprTable& e) { return boost::intrusive_ptr<const ExprTable>(); }
		virtual boost::intrusive_ptr<const ExprTable> modify(const CartoSQL *csql, const ExprOSMTable& e) { return modify(csql, static_cast<const ExprTable&>(e)); }
		virtual boost::intrusive_ptr<const ExprTable> modify(const CartoSQL *csql, const ExprLiteralTable& e) { return modify(csql, static_cast<const ExprTable&>(e)); }
		virtual boost::intrusive_ptr<const ExprTable> modify(const CartoSQL *csql, const ExprTableFunc& e) { return modify(csql, static_cast<const ExprTable&>(e)); }
		virtual boost::intrusive_ptr<const ExprTable> modify(const CartoSQL *csql, const ExprOSMSelect& e) { return modify(csql, static_cast<const ExprTable&>(e)); }
		virtual boost::intrusive_ptr<const ExprTable> modify(const CartoSQL *csql, const ExprCompound& e) { return modify(csql, static_cast<const ExprTable&>(e)); }
		virtual boost::intrusive_ptr<const ExprTable> modify(const CartoSQL *csql, const ExprJoin& e) { return modify(csql, static_cast<const ExprTable&>(e)); }
		virtual boost::intrusive_ptr<const ExprTable> modify(const CartoSQL *csql, const ExprTableReference& e) { return modify(csql, static_cast<const ExprTable&>(e)); }
		virtual boost::intrusive_ptr<const ExprTable> modify(const CartoSQL *csql, const ExprCommonTables& e) { return modify(csql, static_cast<const ExprTable&>(e)); }
	};

	class Calc {
	public:
		virtual const CartoSQL *get_csql(void) const = 0;

		virtual void error(const Expr& e, const char *msg) {}
		virtual void error(const ExprTable& e, const char *msg) {}
		virtual void error(const Expr& e, const Value& v, const char *msg) { error(e, msg); }
		virtual void error(const ExprTable& e, const Value& v, const char *msg) { error(e, msg); }

		virtual bool get_variable(Value& v, const std::string& var) { v = Value(); return false; }

		virtual const Table *get_referenced_table(const std::string& name) { return nullptr; }

		virtual const TableRow *get_table_row(unsigned int tableindex) { return nullptr; }
		virtual const OSMDB::Object *get_osmobject(void) { return nullptr; }
		typedef IndexTableRow::index_t grouprow_t;
		virtual grouprow_t get_group_rows(void) { return 0; }
		virtual void set_group_row(grouprow_t x) {}
	};

	class Calculate : public Calc {
	public:
		Calculate(const CartoSQL *csql = nullptr) : m_csql(csql) {}
		virtual const CartoSQL *get_csql(void) const { return m_csql; }

		virtual void error(const Expr& e, const char *msg) {}
		virtual void error(const ExprTable& e, const char *msg) {}

		virtual bool get_variable(Value& v, const std::string& var) { v = Value(); return false; }

		virtual const Table *get_referenced_table(const std::string& name) { return nullptr; }

		virtual const TableRow *get_table_row(unsigned int tableindex) { return nullptr; }
		virtual const OSMDB::Object *get_osmobject(void) { return nullptr; }
		typedef IndexTableRow::index_t grouprow_t;
		virtual grouprow_t get_group_rows(void) { return 0; }
		virtual void set_group_row(grouprow_t x) {}

	protected:
		const CartoSQL *m_csql;
	};

	class CalcOverride : public Calc {
	public:
		CalcOverride(Calc& c) : m_calc(c) {}
		virtual const CartoSQL *get_csql(void) const { return m_calc.get_csql(); }

		virtual void error(const Expr& e, const char *msg) { m_calc.error(e, msg); }
		virtual void error(const ExprTable& e, const char *msg) { m_calc.error(e, msg); }

		virtual bool get_variable(Value& v, const std::string& var) { return m_calc.get_variable(v, var); }

		virtual const Table *get_referenced_table(const std::string& name) { return m_calc.get_referenced_table(name); }

		virtual const TableRow *get_table_row(unsigned int tableindex) { return m_calc.get_table_row(tableindex); }
		virtual const OSMDB::Object *get_osmobject(void) { return m_calc.get_osmobject(); }
		virtual grouprow_t get_group_rows(void) { return m_calc.get_group_rows(); }
		virtual void set_group_row(grouprow_t x) { m_calc.set_group_row(x); }

	protected:
		Calc& m_calc;
	};

	class CalcOSMVariables : public Calculate {
	public:
		CalcOSMVariables(const CartoSQL *csql = nullptr)
			: Calculate(csql), m_bbox(Rect::invalid), m_scale_denominator(std::numeric_limits<double>::quiet_NaN()),
			  m_pixel_width(std::numeric_limits<double>::quiet_NaN()), m_pixel_height(std::numeric_limits<double>::quiet_NaN()) {}

		virtual bool get_variable(Value& v, const std::string& var) {
			if (var == "bbox") {
				v = m_bbox;
				return true;
			}
			if (var == "scale_denominator") {
				v = m_scale_denominator;
				return true;
			}
			if (var == "pixel_width") {
				v = m_pixel_width;
				return true;
			}
			if (var == "pixel_height") {
				v = m_pixel_height;
				return true;
			}
			return Calc::get_variable(v, var);
		}

		const Rect& get_bbox(void) const { return m_bbox; }
		void set_bbox(const Rect& x) { m_bbox = x; }
		double get_scale_denominator(void) const { return m_scale_denominator; }
		void set_scale_denominator(double x) { m_scale_denominator = x; }
		double get_pixel_width(void) const { return m_pixel_width; }
		void set_pixel_width(double x) { m_pixel_width = x; }
		double get_pixel_height(void) const { return m_pixel_height; }
		void set_pixel_height(double x) { m_pixel_height = x; }

	protected:
		Rect m_bbox;
		double m_scale_denominator;
		double m_pixel_width;
		double m_pixel_height;
	};

	class CalcOSMVariablesOverride : public CalcOverride {
	public:
		CalcOSMVariablesOverride(Calc& c)
			: CalcOverride(c), m_bbox(Rect::invalid), m_scale_denominator(std::numeric_limits<double>::quiet_NaN()),
			  m_pixel_width(std::numeric_limits<double>::quiet_NaN()), m_pixel_height(std::numeric_limits<double>::quiet_NaN()) {}

		virtual bool get_variable(Value& v, const std::string& var) {
			if (var == "bbox") {
				v = m_bbox;
				return true;
			}
			if (var == "scale_denominator") {
				v = m_scale_denominator;
				return true;
			}
			if (var == "pixel_width") {
				v = m_pixel_width;
				return true;
			}
			if (var == "pixel_height") {
				v = m_pixel_height;
				return true;
			}
			return CalcOverride::get_variable(v, var);
		}

		const Rect& get_bbox(void) const { return m_bbox; }
		void set_bbox(const Rect& x) { m_bbox = x; }
		double get_scale_denominator(void) const { return m_scale_denominator; }
		void set_scale_denominator(double x) { m_scale_denominator = x; }
		double get_pixel_width(void) const { return m_pixel_width; }
		void set_pixel_width(double x) { m_pixel_width = x; }
		double get_pixel_height(void) const { return m_pixel_height; }
		void set_pixel_height(double x) { m_pixel_height = x; }

	protected:
		Rect m_bbox;
		double m_scale_denominator;
		double m_pixel_width;
		double m_pixel_height;
	};

	class PrintContext {
	public:
		virtual const CartoSQL *get_csql(void) const = 0;
		virtual const char *get_osmkey(OSMDB::Object::tagkey_t key) const = 0;
		virtual const char *get_osmvalue(OSMDB::Object::tagname_t name) const = 0;
		virtual std::string get_tablecolumn(unsigned int tblnr, unsigned int colnr) const = 0;
	};

	class PrintContextSQL : public PrintContext {
	public:
		PrintContextSQL(const CartoSQL *csql = nullptr) : m_csql(csql) {}
		virtual const CartoSQL *get_csql(void) const { return m_csql; }
		virtual const char *get_osmkey(OSMDB::Object::tagkey_t key) const;
		virtual const char *get_osmvalue(OSMDB::Object::tagname_t name) const;
		virtual std::string get_tablecolumn(unsigned int tblnr, unsigned int colnr) const;

	protected:
		const CartoSQL *m_csql;
		const ExprTable * const *m_subtables;
	};

	class PrintContextOverride : public PrintContext {
	public:
		PrintContextOverride(const PrintContext& ctx) : m_ctx(ctx) {}
		virtual const CartoSQL *get_csql(void) const { return m_ctx.get_csql(); }
		virtual const char *get_osmkey(OSMDB::Object::tagkey_t key) const { return m_ctx.get_osmkey(key); }
		virtual const char *get_osmvalue(OSMDB::Object::tagname_t name) const { return m_ctx.get_osmvalue(name); }
		virtual std::string get_tablecolumn(unsigned int tblnr, unsigned int colnr) const { return m_ctx.get_tablecolumn(tblnr, colnr); }

	protected:
		const PrintContext& m_ctx;
	};

	class PrintContextSubtables : public PrintContextOverride {
	public:
		PrintContextSubtables(const PrintContext& ctx, const ExprTable * const *subtbl = nullptr)
			: PrintContextOverride(ctx), m_subtables(subtbl) {}
		virtual std::string get_tablecolumn(unsigned int tblnr, unsigned int colnr) const;

	protected:
		const ExprTable * const *m_subtables;
	};

	class PrintContextStackSubtables : public PrintContextOverride {
	public:
		PrintContextStackSubtables(const PrintContext& ctx, const ExprTable * const *subtbl = nullptr)
			: PrintContextOverride(ctx), m_subtables(subtbl) {}
		virtual std::string get_tablecolumn(unsigned int tblnr, unsigned int colnr) const;

	protected:
		const ExprTable * const *m_subtables;
	};

	class PrintContextSubtableVector : public PrintContextOverride {
	public:
		typedef std::vector<boost::intrusive_ptr<const ExprTable> > subtables_t;
		PrintContextSubtableVector(const PrintContext& ctx, const subtables_t& subtables = subtables_t())
			: PrintContextOverride(ctx), m_subtables(subtables) {}
		virtual std::string get_tablecolumn(unsigned int tblnr, unsigned int colnr) const;

	protected:
		subtables_t m_subtables;
	};

	class PrintContextHideFirstTable : public PrintContextOverride {
	public:
		PrintContextHideFirstTable(const PrintContext& ctx) : PrintContextOverride(ctx) {}
		virtual std::string get_tablecolumn(unsigned int tblnr, unsigned int colnr) const;
	};

	class JoinColumns;

	class Expr {
	public:
		typedef boost::intrusive_ptr<Expr> ptr_t;
		typedef boost::intrusive_ptr<const Expr> const_ptr_t;

		enum class type_t : uint8_t {
			value,
			variable,
			function,
			funcaggregate,
			functable,
			case_,
			cast,
			tagmember,
			column,
			tablecolumn,
			osmspecial,
			osmtag,
			invalid = static_cast<std::underlying_type<type_t>::type>(~0)
		};

		typedef std::vector<const_ptr_t> exprlist_t;

		Expr(void);
		virtual ~Expr();

		unsigned int breference(void) const { return ++m_refcount; }
		unsigned int bunreference(void) const { return --m_refcount; }
		unsigned int get_refcount(void) const { return m_refcount; }
		ptr_t get_ptr(void) { return ptr_t(this); }
		const_ptr_t get_ptr(void) const { return const_ptr_t(this); }
		friend inline void intrusive_ptr_add_ref(const Expr *expr) { expr->breference(); }
		friend inline void intrusive_ptr_release(const Expr *expr) { if (!expr->bunreference()) delete expr; }

		virtual type_t get_type(void) const = 0;

		virtual bool calculate(Calc& c, Value& v) const;
		virtual const_ptr_t simplify(const CartoSQL *csql) const;
		virtual const_ptr_t simplify(const CartoSQL *csql, Visitor& v) const;
		virtual void visit(Visitor& v) const { v.visit(*this); v.visitend(*this); }
		virtual const_ptr_t visitor_call_modify(const CartoSQL *csql, Visitor& v) const = 0;
		virtual void visitor_call_visit(Visitor& v) const { v.visit(*this); }
		virtual void visitor_call_visitend(Visitor& v) const { v.visitend(*this); }

		virtual bool is_type(Value& v) const { v = Value(); return false; }
		bool is_false(void) const;
		bool is_true(void) const;
		typedef std::vector<boost::intrusive_ptr<const ExprTable> > subtables_t;
		virtual Value::type_t calculate_type(const subtables_t& subtables = subtables_t()) const { return Value::type_t::null; }

		Value as_value(void) const;

		virtual std::ostream& print(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const = 0;
		virtual bool is_paren(void) const { return true; }

		virtual int compare(const Expr& x) const;
		bool operator==(const Expr& x) const { return !compare(x); }
		bool operator!=(const Expr& x) const { return !operator==(x); }
		bool operator<(const Expr& x) const { return compare(x) < 0; }
		bool operator<=(const Expr& x) const { return compare(x) <= 0; }
		bool operator>(const Expr& x) const { return compare(x) > 0; }
		bool operator>=(const Expr& x) const { return compare(x) >= 0; }

		virtual void extract_joincolumns(JoinColumns& jc) const { }
		bool is_aggregate(void) const;
		const_ptr_t replace_variable(Calc& c) const;
		virtual void split_and_terms(exprlist_t& el) const;
		static Expr::const_ptr_t combine_and_terms(const exprlist_t& el);

		static int compare(const const_ptr_t& p0, const const_ptr_t& p1);
		static int compare(const exprlist_t& e0, const exprlist_t& e1);

		static std::ostream& print_expr(std::ostream& os, const const_ptr_t& e, unsigned int indent, const PrintContext& ctx = PrintContextSQL(), bool paren = true);
		static std::ostream& print_expr(std::ostream& os, const exprlist_t& e, unsigned int indent, const PrintContext& ctx = PrintContextSQL());

	protected:
		class AggregateVisitor;
		mutable std::atomic<unsigned int> m_refcount;
	};

	class ExprValue : public Expr {
	public:
		ExprValue(const Value& v) : m_value(v) {}

		virtual type_t get_type(void) const { return type_t::value; }
		virtual bool calculate(Calc& c, Value& v) const { v = m_value; return true; }
		virtual bool is_type(Value& v) const { v = m_value; return true; }
		virtual Value::type_t calculate_type(const subtables_t& subtables = subtables_t()) const { return m_value.get_type(); }
		using Expr::simplify;
		virtual const_ptr_t simplify(const CartoSQL *csql, Visitor& v) const;
		virtual void visit(Visitor& v) const { v.visit(*this); v.visitend(*this); }
		virtual const_ptr_t visitor_call_modify(const CartoSQL *csql, Visitor& v) const { return v.modify(csql, *this); }
		virtual void visitor_call_visit(Visitor& v) const { v.visit(*this); }
		virtual void visitor_call_visitend(Visitor& v) const { v.visitend(*this); }
		virtual std::ostream& print(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const;
		virtual bool is_paren(void) const { return false; }
		virtual int compare(const Expr& x) const;

	protected:
		Value m_value;
	};

	class ExprVariable : public Expr {
	public:
		ExprVariable(const std::string& v) : m_variable(v) {}

		virtual type_t get_type(void) const { return type_t::variable; }
		virtual bool calculate(Calc& c, Value& v) const;
		virtual Value::type_t calculate_type(const subtables_t& subtables = subtables_t()) const { return Value::type_t::null; }
		using Expr::simplify;
		virtual const_ptr_t simplify(const CartoSQL *csql, Visitor& v) const;
		virtual void visit(Visitor& v) const { v.visit(*this); v.visitend(*this); }
		virtual const_ptr_t visitor_call_modify(const CartoSQL *csql, Visitor& v) const { return v.modify(csql, *this); }
		virtual void visitor_call_visit(Visitor& v) const { v.visit(*this); }
		virtual void visitor_call_visitend(Visitor& v) const { v.visitend(*this); }
		virtual std::ostream& print(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const;
		virtual bool is_paren(void) const { return false; }
		virtual int compare(const Expr& x) const;

		const std::string& get_variable(void) const { return m_variable; }
		void set_variable(const std::string& v) { m_variable = v; }

	protected:
		std::string m_variable;
	};

	class ExprFunc : public Expr {
	public:
		enum class func_t : uint8_t {
			first,
			// unary operators
			operatorbegin,
			unaryoperatorbegin = operatorbegin,
			unaryplus = unaryoperatorbegin,
			unaryminus,
			bitwisenot,
			logicalnot,
			factorial,
			unaryoperatorend = factorial,
			// binary operators
			binaryoperatorbegin,
			comparisonbegin = binaryoperatorbegin,
			equal = comparisonbegin,
			unequal,
			lessthan,
			lessorequal,
			greaterthan,
			greaterorequal,
			comparisonend = greaterorequal,
			binaryplus,
			binaryminus,
			binarymul,
			binarydiv,
			binarymod,
			bitwiseand,
			bitwiseor,
			bitwisexor,
			bitwiseshiftleft,
			bitwiseshiftright,
			logicaland,
			logicalor,
			stringconcat,
			regex,
			notregex,
			regexnocase,
			notregexnocase,
			overlap,
			binaryoperatorend = overlap,
			operatorend = binaryoperatorend,
			// postfix operators
			postfixoperatorbegin,
			isnull = postfixoperatorbegin,
			isnotnull,
			isfalse,
			isnotfalse,
			istrue,
			isnottrue,
			isunknown,
			isnotunknown,
			isdistinct,
			isnotdistinct,
			postfixoperatorend = isnotdistinct,
			// math
			abs,
			cbrt,
			ceil,
			degrees,
			exp,
			floor,
			ln,
			log,
			log10,
			radians,
			round,
			sign,
			sqrt,
			trunc,
			acos,
			asin,
			atan,
			cos,
			cot,
			sin,
			tan,
			acosd,
			asind,
			atand,
			cosd,
			cotd,
			sind,
			tand,
			sinh,
			cosh,
			tanh,
			asinh,
			acosh,
			atanh,
			div,
			pi,
			pow,
			atan2,
			atan2d,
			// string
			ascii,
			bit_length,
			char_length,
			chr,
			length,
			lower,
			md5,
			octet_length,
			upper,
			overlay,
			substring,
			btrim,
			concat,
			concat_ws,
			left,
			ltrim,
			replace,
			right,
			rtrim,
			strpos,
			substr,
			// regex
			like,
			notlike,
			ilike,
			notilike,
			similar,
			notsimilar,
			// set
			between,
			notbetween,
			betweensymmetric,
			notbetweensymmetric,
			inset,
			notinset,
			// conditional
			coalesce,
			nullif,
			greatest,
			least,
			// array
			array,
			array_to_string,
			string_to_array,
			array_length,
			// GIS
			st_pointonsurface,
			st_dwithin,
			invalid = static_cast<std::underlying_type<func_t>::type>(~0)
		};

		typedef std::vector<Value> valuelist_t;

		ExprFunc(func_t func, const exprlist_t& expr) : m_expr(expr), m_func(func) {}

		virtual type_t get_type(void) const { return type_t::function; }
		virtual bool calculate(Calc& c, Value& v) const { return calculate(c, v, m_func, m_expr, this); }
		virtual Value::type_t calculate_type(const subtables_t& subtables = subtables_t()) const;
		virtual const_ptr_t simplify(const CartoSQL *csql) const { return simplify_int(csql);  }
		virtual const_ptr_t simplify(const CartoSQL *csql, Visitor& v) const;
		virtual void visit(Visitor& v) const;
		virtual const_ptr_t visitor_call_modify(const CartoSQL *csql, Visitor& v) const { return v.modify(csql, *this); }
		virtual void visitor_call_visit(Visitor& v) const { v.visit(*this); }
		virtual void visitor_call_visitend(Visitor& v) const { v.visitend(*this); }
		virtual std::ostream& print(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const;
		virtual bool is_paren(void) const;
		virtual int compare(const Expr& x) const;

		static bool calculate(Calc& c, Value& v, func_t func, const exprlist_t& e, const Expr *expr);
		static bool calculate(Value& v, func_t func, const valuelist_t& val);
		static bool calculate_comparison(Value& v, func_t func, const Value& v0, const Value& v1);
		static Value calculate_pointonsurface(const Point& g);
		static Value calculate_pointonsurface(const MultiPoint& g);
		static Value calculate_pointonsurface(const LineString& g);
		static Value calculate_pointonsurface(const MultiLineString& g);
		static Value calculate_pointonsurface(const MultiPolygonHole& g);
		static Value calculate_pointonsurface(const Rect& g);

		virtual void extract_joincolumns(JoinColumns& jc) const;
		virtual void split_and_terms(exprlist_t& el) const;

		static func_t notfunc(func_t func);
		func_t get_func(void) const { return m_func; }
		void set_func(func_t f) { m_func = f; }
		const exprlist_t& get_expr(void) const { return m_expr; }
		void set_expr(const exprlist_t& e) { m_expr = e; }

	protected:
		exprlist_t m_expr;
		func_t m_func;

		static const_ptr_t simplify_tail(const CartoSQL *csql, func_t func, const exprlist_t& expr, bool modif);
		template <typename... Args> const_ptr_t simplify_int(const CartoSQL *csql, Args&&... args) const;
		template <typename T> static bool calc_comparison(Value& v, func_t func, T arg0, T arg1);
		template <typename T> static bool calc_math_unary(Value& v, func_t func, T arg);
		template <typename T> static bool calc_math_binary(Value& v, func_t func, T arg0, T arg1);
		static bool like(Glib::ustring::const_iterator ti, Glib::ustring::const_iterator te,
				 Glib::ustring::const_iterator pi, Glib::ustring::const_iterator pe, gunichar escape);
	};

	class ExprFuncAggregate : public Expr {
	public:
		enum class func_t : uint8_t {
			first,
			countrows = first,
			count,
			sum,
			avg,
			min,
			max,
			bit_and,
			bit_or,
			bool_and,
			bool_or,
			array_agg,
			string_agg,
			last = string_agg
		};

		ExprFuncAggregate(func_t func = func_t::countrows, const exprlist_t& expr = exprlist_t(), bool distinct = false)
			: m_expr(expr), m_func(func), m_distinct(distinct) {}

		virtual type_t get_type(void) const { return type_t::funcaggregate; }
		virtual bool calculate(Calc& c, Value& v) const { return calculate(c, v, m_func, m_expr, m_distinct, this); }
		virtual Value::type_t calculate_type(const subtables_t& subtables = subtables_t()) const;
		virtual const_ptr_t simplify(const CartoSQL *csql) const { return simplify_int(csql);  }
		virtual const_ptr_t simplify(const CartoSQL *csql, Visitor& v) const;
		virtual void visit(Visitor& v) const;
		virtual const_ptr_t visitor_call_modify(const CartoSQL *csql, Visitor& v) const { return v.modify(csql, *this); }
		virtual void visitor_call_visit(Visitor& v) const { v.visit(*this); }
		virtual void visitor_call_visitend(Visitor& v) const { v.visitend(*this); }
		virtual std::ostream& print(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const;
		virtual bool is_paren(void) const { return false; }
		virtual int compare(const Expr& x) const;

		const exprlist_t& get_expr(void) const { return m_expr; }
		void set_expr(const exprlist_t& e) { m_expr = e; }
		func_t get_func(void) const { return m_func; }
		void set_func(func_t f) { m_func = f; }
		bool is_distinct(void) const { return m_distinct; }
		void set_distinct(bool d) { m_distinct = d; }

		static bool calculate(Calc& c, Value& v, func_t func, const exprlist_t& e, bool distinct, const Expr *expr);

	protected:
		exprlist_t m_expr;
		func_t m_func;
		bool m_distinct;

		static const_ptr_t simplify_tail(const CartoSQL *csql, func_t func, const exprlist_t& expr, bool distinct, bool modif);
		template <typename... Args> const_ptr_t simplify_int(const CartoSQL *csql, Args&&... args) const;
	};

	class ExprFuncTable : public Expr {
	public:
		enum class func_t : uint8_t {
			first,
			scalar = first,
			exists,
			notexists,
			last = notexists
		};

		typedef boost::intrusive_ptr<const ExprTable> tblexpr_t;

		ExprFuncTable(func_t func = func_t::scalar, const tblexpr_t& expr = tblexpr_t()) : m_expr(expr), m_func(func) {}

		virtual type_t get_type(void) const { return type_t::functable; }
		virtual bool calculate(Calc& c, Value& v) const { return calculate(c, v, m_func, m_expr, this); }
		virtual Value::type_t calculate_type(const subtables_t& subtables = subtables_t()) const;
		virtual const_ptr_t simplify(const CartoSQL *csql) const { return simplify_int(csql);  }
		virtual const_ptr_t simplify(const CartoSQL *csql, Visitor& v) const;
		virtual void visit(Visitor& v) const;
		virtual const_ptr_t visitor_call_modify(const CartoSQL *csql, Visitor& v) const { return v.modify(csql, *this); }
		virtual void visitor_call_visit(Visitor& v) const { v.visit(*this); }
		virtual void visitor_call_visitend(Visitor& v) const { v.visitend(*this); }
		virtual std::ostream& print(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const;
		virtual int compare(const Expr& x) const;

		const tblexpr_t& get_expr(void) const { return m_expr; }
		void set_expr(const tblexpr_t& expr) { m_expr = expr; }
		func_t get_func(void) const { return m_func; }
		void set_func(func_t f) { m_func = f; }

		static bool calculate(Calc& c, Value& v, func_t func, const tblexpr_t& expr, const Expr *fexpr);

	protected:
		tblexpr_t m_expr;
		func_t m_func;

		static const_ptr_t simplify_tail(const CartoSQL *csql, func_t func, const tblexpr_t& expr, bool modif);
		template <typename... Args> const_ptr_t simplify_int(const CartoSQL *csql, Args&&... args) const;
	};

	class ExprCase : public Expr {
	public:
		class When {
		public:
			When(const const_ptr_t& expr = const_ptr_t(), const const_ptr_t& value = const_ptr_t())
				: m_expr(expr), m_value(value) {}
			const const_ptr_t& get_expr(void) const { return m_expr; }
			const const_ptr_t& get_value(void) const { return m_value; }
			void set_expr(const const_ptr_t& expr) { m_expr = expr; }
			void set_value(const const_ptr_t& value) { m_value = value; }
			int compare(const When& x) const;
			Value::type_t calculate_type(const subtables_t& subtables = subtables_t()) const;

		protected:
			const_ptr_t m_expr;
			const_ptr_t m_value;
		};

		typedef std::vector<When> when_t;

		ExprCase(const const_ptr_t& expr = const_ptr_t(), const when_t& when = when_t(), const const_ptr_t& else_ = const_ptr_t())
			: m_when(when), m_expr(expr), m_else(else_) {}

		virtual type_t get_type(void) const { return type_t::case_; }
		virtual bool calculate(Calc& c, Value& v) const;
		virtual Value::type_t calculate_type(const subtables_t& subtables = subtables_t()) const;
		virtual const_ptr_t simplify(const CartoSQL *csql) const { return simplify_int(csql);  }
		virtual const_ptr_t simplify(const CartoSQL *csql, Visitor& v) const;
		virtual void visit(Visitor& v) const;
		virtual const_ptr_t visitor_call_modify(const CartoSQL *csql, Visitor& v) const { return v.modify(csql, *this); }
		virtual void visitor_call_visit(Visitor& v) const { v.visit(*this); }
		virtual void visitor_call_visitend(Visitor& v) const { v.visitend(*this); }
		virtual std::ostream& print(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const;
		virtual int compare(const Expr& x) const;

		const const_ptr_t& get_expr(void) const { return m_expr; }
		const when_t& get_when(void) const { return m_when; }
		when_t& get_when(void) { return m_when; }
		const const_ptr_t& get_else(void) const { return m_else; }
		void set_expr(const const_ptr_t& expr) { m_expr = expr; }
		void set_when(const when_t& when) { m_when = when; }
		void append_when(const When& when) { m_when.push_back(when); }
		void set_else(const const_ptr_t& else_) { m_else = else_; }

	protected:
		when_t m_when;
		const_ptr_t m_expr;
		const_ptr_t m_else;

		template <typename... Args> const_ptr_t simplify_int(const CartoSQL *csql, Args&&... args) const;
	};

	class ExprOneChild : public Expr {
	public:
		ExprOneChild(const const_ptr_t& expr = const_ptr_t()) : m_expr(expr) {}
		virtual void visit(Visitor& v) const;
		virtual std::ostream& print(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const;
		virtual int compare(const Expr& x) const;

		const const_ptr_t& get_expr(void) const { return m_expr; }
		void set_expr(const const_ptr_t& expr) { m_expr = expr; }

	protected:
		const_ptr_t m_expr;
	};

	class ExprCast : public ExprOneChild {
	public:
		ExprCast(const const_ptr_t& expr = const_ptr_t(), Value::type_t typ = Value::type_t::null)
			: ExprOneChild(expr), m_type(typ) {}

		virtual type_t get_type(void) const { return type_t::cast; }
		virtual bool calculate(Calc& c, Value& v) const { return calculate(c, v, m_expr, m_type, this); }
		virtual Value::type_t calculate_type(const subtables_t& subtables = subtables_t()) const { return m_type; }
		virtual const_ptr_t simplify(const CartoSQL *csql) const { return simplify_int(csql);  }
		virtual const_ptr_t simplify(const CartoSQL *csql, Visitor& v) const;
		virtual void visit(Visitor& v) const;
		virtual const_ptr_t visitor_call_modify(const CartoSQL *csql, Visitor& v) const { return v.modify(csql, *this); }
		virtual void visitor_call_visit(Visitor& v) const { v.visit(*this); }
		virtual void visitor_call_visitend(Visitor& v) const { v.visitend(*this); }
		virtual std::ostream& print(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const;
		virtual bool is_paren(void) const { return false; }
		virtual int compare(const Expr& x) const;

		Value::type_t get_datatype(void) const { return m_type; }

		static bool calculate(Calc& c, Value& v, const const_ptr_t& expr, Value::type_t typ, const Expr *expr1);

	protected:
		Value::type_t m_type;

		static const_ptr_t simplify_tail(const CartoSQL *csql, const const_ptr_t& expr, Value::type_t typ, bool modif);
		template <typename... Args> const_ptr_t simplify_int(const CartoSQL *csql, Args&&... args) const;
	};

	class ExprTagMember : public ExprOneChild {
	public:
		enum class tag_t : uint8_t {
			value,
			exists,
			isequal
		};

		ExprTagMember(const const_ptr_t& expr = const_ptr_t(), tag_t tag = tag_t::value, const std::string& key = "",
			      const std::string& value = "", OSMDB::Object::tagkey_t tagkey = OSMDB::Object::tagkey_t::invalid,
			      OSMDB::Object::tagname_t tagname = OSMDB::Object::tagname_t::invalid)
			: ExprOneChild(expr), m_key(key), m_value(value), m_tagname(tagname), m_tagkey(tagkey), m_tag(tag) {}

		virtual type_t get_type(void) const { return type_t::tagmember; }
		virtual bool calculate(Calc& c, Value& v) const { return calculate(c, v, m_expr, m_key, m_value, m_tagkey, m_tagname, m_tag, this); }
		virtual Value::type_t calculate_type(const subtables_t& subtables = subtables_t()) const;
		virtual const_ptr_t simplify(const CartoSQL *csql) const { return simplify_int(csql);  }
		virtual const_ptr_t simplify(const CartoSQL *csql, Visitor& v) const;
		virtual void visit(Visitor& v) const;
		virtual const_ptr_t visitor_call_modify(const CartoSQL *csql, Visitor& v) const { return v.modify(csql, *this); }
		virtual void visitor_call_visit(Visitor& v) const { v.visit(*this); }
		virtual void visitor_call_visitend(Visitor& v) const { v.visitend(*this); }
		virtual std::ostream& print(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const;
		virtual bool is_paren(void) const { return true; }
		virtual int compare(const Expr& x) const;

		const std::string& get_key(void) const { return m_key; }
		void set_key(const std::string& x) { m_key = x; }
		const std::string& get_value(void) const { return m_value; }
		void set_value(const std::string& x) { m_value = x; }
		OSMDB::Object::tagname_t get_tagname(void) const { return m_tagname; }
		void set_tagname(OSMDB::Object::tagname_t n) { m_tagname = n; }
		OSMDB::Object::tagkey_t get_tagkey(void) const { return m_tagkey; }
		void set_tagkey(OSMDB::Object::tagkey_t k) { m_tagkey = k; }
		tag_t get_tag(void) const { return m_tag; }
		void set_tag(tag_t x) { m_tag = x; }

		static bool calculate(Calc& c, Value& v, const const_ptr_t& expr, const std::string& key, const std::string& value,
				      OSMDB::Object::tagkey_t tagkey, OSMDB::Object::tagname_t tagname, tag_t tag, const Expr *expr1);

	protected:
		std::string m_key;
		std::string m_value;
		OSMDB::Object::tagname_t m_tagname;
		OSMDB::Object::tagkey_t m_tagkey;
		tag_t m_tag;

		static const_ptr_t simplify_tail(const CartoSQL *csql, const const_ptr_t& expr, const std::string& key, const std::string& value,
						 OSMDB::Object::tagkey_t tagkey, OSMDB::Object::tagname_t tagname, tag_t tag, bool modif);
		template <typename... Args> const_ptr_t simplify_int(const CartoSQL *csql, Args&&... args) const;
	};

	class ExprColumn : public Expr {
	public:
		enum class record_t : uint8_t {
			none,
			member,
			exists,
			equalsvalue,
			invalid
		};

		ExprColumn(const std::string& schema = "", const std::string& table = "",
			   const std::string& column = "", record_t rec = record_t::none,
			   const std::string& member = "", const std::string& value = "")
			: m_schema(schema), m_table(table), m_column(column), m_member(member),
			  m_value(value), m_record(rec) {}

		virtual type_t get_type(void) const { return type_t::column; }
		virtual bool calculate(Calc& c, Value& v) const;
		virtual Value::type_t calculate_type(const subtables_t& subtables = subtables_t()) const { return Value::type_t::null; }
		using Expr::simplify;
		virtual const_ptr_t simplify(const CartoSQL *csql, Visitor& v) const;
		virtual void visit(Visitor& v) const { v.visit(*this); v.visitend(*this); }
		virtual const_ptr_t visitor_call_modify(const CartoSQL *csql, Visitor& v) const { return v.modify(csql, *this); }
		virtual void visitor_call_visit(Visitor& v) const { v.visit(*this); }
		virtual void visitor_call_visitend(Visitor& v) const { v.visitend(*this); }
		virtual std::ostream& print(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const;
		virtual bool is_paren(void) const { return false; }
		virtual int compare(const Expr& x) const;

		const std::string& get_schema(void) const { return m_schema; }
		const std::string& get_table(void) const { return m_table; }
		const std::string& get_column(void) const { return m_column; }
		const std::string& get_member(void) const { return m_member; }
		const std::string& get_value(void) const { return m_value; }
		const record_t get_record(void) const { return m_record; }

		void set_schema(const std::string& x) { m_schema = x; }
		void set_table(const std::string& x) { m_table = x; }
		void set_column(const std::string& x) { m_column = x; }
		void set_member(const std::string& x) { m_member = x; }
		void set_value(const std::string& x) { m_value = x; }
		void set_record(record_t x) { m_record = x; }

	protected:
		std::string m_schema;
		std::string m_table;
		std::string m_column;
		std::string m_member;
		std::string m_value;
		record_t m_record;
	};

	class ExprTableColumn : public Expr {
	public:
		ExprTableColumn(unsigned int tblidx = ~0U, unsigned int colidx = ~0U)
			: m_tableindex(tblidx), m_columnindex(colidx) {}

		virtual type_t get_type(void) const { return type_t::tablecolumn; }
		virtual bool calculate(Calc& c, Value& v) const;
		virtual Value::type_t calculate_type(const subtables_t& subtables = subtables_t()) const;
		using Expr::simplify;
		virtual const_ptr_t simplify(const CartoSQL *csql, Visitor& v) const;
		virtual void visit(Visitor& v) const { v.visit(*this); v.visitend(*this); }
		virtual const_ptr_t visitor_call_modify(const CartoSQL *csql, Visitor& v) const { return v.modify(csql, *this); }
		virtual void visitor_call_visit(Visitor& v) const { v.visit(*this); }
		virtual void visitor_call_visitend(Visitor& v) const { v.visitend(*this); }
		virtual std::ostream& print(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const;
		virtual bool is_paren(void) const { return false; }
		virtual int compare(const Expr& x) const;

		unsigned int get_tableindex(void) const { return m_tableindex; }
		void set_tableindex(unsigned int x) { m_tableindex = x; }
		unsigned int get_columnindex(void) const { return m_columnindex; }
		void set_columnindex(unsigned int x) { m_columnindex = x; }

	protected:
		unsigned int m_tableindex;
		unsigned int m_columnindex;
	};

	class ExprOSMSpecial : public Expr {
	public:
		enum class field_t : uint8_t {
			osmid,
			first = osmid,
			zorder,
			tags,
			wayarea,
			waytruearea,
			wayangulararea,
			way,
			last = way
		};

		ExprOSMSpecial(field_t fld = field_t::osmid)
			: m_field(fld) {}

		virtual type_t get_type(void) const { return type_t::osmspecial; }
		virtual bool calculate(Calc& c, Value& v) const;
		static Value::type_t get_valuetype(field_t fld);
		Value::type_t get_valuetype(void) const { return get_valuetype(get_field()); }
		static Value::type_t get_valuetype(field_t fld, OSMDB::Object::type_t osmt);
		Value::type_t get_valuetype(OSMDB::Object::type_t osmt) const { return get_valuetype(get_field(), osmt); }
		virtual Value::type_t calculate_type(const subtables_t& subtables = subtables_t()) const { return get_valuetype(); }
		using Expr::simplify;
		virtual const_ptr_t simplify(const CartoSQL *csql, Visitor& v) const;
		virtual void visit(Visitor& v) const { v.visit(*this); v.visitend(*this); }
		virtual const_ptr_t visitor_call_modify(const CartoSQL *csql, Visitor& v) const { return v.modify(csql, *this); }
		virtual void visitor_call_visit(Visitor& v) const { v.visit(*this); }
		virtual void visitor_call_visitend(Visitor& v) const { v.visitend(*this); }
		virtual std::ostream& print(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const;
		virtual bool is_paren(void) const { return false; }
		virtual int compare(const Expr& x) const;

		field_t get_field(void) const { return m_field; }
		void set_field(field_t x) { m_field = x; }
		static bool parse_field(field_t& fld, const std::string& str);

	protected:
		field_t m_field;
	};

	class ExprOSMTag : public Expr {
	public:
		typedef ExprTagMember::tag_t tag_t;
		typedef OSMDB::Object::tagkey_t tagkey_t;
		typedef OSMDB::Object::tagname_t tagname_t;

		ExprOSMTag(tag_t tag = tag_t::value, tagkey_t key = tagkey_t::invalid, tagname_t value = tagname_t::invalid)
			: m_value(value), m_key(key), m_tag(tag) {}

		virtual type_t get_type(void) const { return type_t::osmtag; }
		virtual bool calculate(Calc& c, Value& v) const;
		virtual Value::type_t calculate_type(const subtables_t& subtables = subtables_t()) const;
		using Expr::simplify;
		virtual const_ptr_t simplify(const CartoSQL *csql, Visitor& v) const;
		virtual void visit(Visitor& v) const { v.visit(*this); v.visitend(*this); }
		virtual const_ptr_t visitor_call_modify(const CartoSQL *csql, Visitor& v) const { return v.modify(csql, *this); }
		virtual void visitor_call_visit(Visitor& v) const { v.visit(*this); }
		virtual void visitor_call_visitend(Visitor& v) const { v.visitend(*this); }
		virtual std::ostream& print(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const;
		virtual bool is_paren(void) const { return true; }
		virtual int compare(const Expr& x) const;

		tag_t get_tag(void) const { return m_tag; }
		void set_tag(tag_t x) { m_tag = x; }
		tagkey_t get_key(void) const { return m_key; }
		void set_key(tagkey_t x) { m_key = x; }
		tagname_t get_value(void) const { return m_value; }
		void set_value(tagname_t x) { m_value = x; }

	protected:
		tagname_t m_value;
		tagkey_t m_key;
		tag_t m_tag;
	};

	class ExprTable {
	public:
		class ColumnName {
		public:
			ColumnName(const std::string& table = "", const std::string& name = "") : m_table(table), m_name(name) {}
			const std::string& get_name(void) const { return m_name; }
			const std::string& get_table(void) const { return m_table; }
			void set_table(const std::string& x) { m_table = x; }
			void set_name(const std::string& x) { m_name = x; }
			bool is_wildcard(void) const { return m_name.size() == 1 && m_name[0] == '*'; }
			bool has_table(void) const { return !m_table.empty(); }
			bool is_match(const ColumnName& x) const;
			int compare(const ColumnName& x) const;
			std::string to_str(void) const;

		protected:
			std::string m_table;
			std::string m_name;
		};

		class Column : public ColumnName {
		public:
			Column(const std::string& table = "", const std::string& name = "", const Expr::const_ptr_t& expr = Expr::const_ptr_t(), Value::type_t typ = Value::type_t::null)
				: ColumnName(table, name), m_expr(expr), m_type(typ) {}

			const Expr::const_ptr_t& get_expr(void) const { return m_expr; }
			void set_expr(const Expr::const_ptr_t& e) { m_expr = e; }
			Value::type_t get_type(void) const { return m_type; }
			void set_type(Value::type_t t) { m_type = t; }

			bool simplify(const CartoSQL *csql, const Expr::subtables_t& subtables);
			bool simplify(const CartoSQL *csql, const Expr::subtables_t& subtables, Visitor& v);
			void visit(Visitor& v) const;
			std::ostream& print(std::ostream& os, unsigned int indent, const std::string& postfix, const PrintContext& ctx = PrintContextSQL()) const;
			int compare(const Column& x) const;

		protected:
			Expr::const_ptr_t m_expr;
			std::string m_name;
			Value::type_t m_type;

			template <typename... Args> bool simplify_int(const CartoSQL *csql, const Expr::subtables_t& subtables, Args&&... args);
		};

		class Columns : public std::vector<Column> {
		public:
			bool simplify(const CartoSQL *csql, const Expr::subtables_t& subtables);
			bool simplify(const CartoSQL *csql, const Expr::subtables_t& subtables, Visitor& v);
			void visit(Visitor& v) const;
			std::ostream& print(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const;
			int compare(const Columns& x) const;

			bool is_wildcard(void) const;
			size_type find(const ColumnName& x) const;
			void set_table(const std::string& tbl);

			bool has_aggregate(void) const;
			void remove_unused_columns(const columnset_t& colsused);

		protected:
			template <typename... Args> bool simplify_int(const CartoSQL *csql, const Expr::subtables_t& subtables, Args&&... args);
		};

		typedef boost::intrusive_ptr<ExprTable> ptr_t;
		typedef boost::intrusive_ptr<const ExprTable> const_ptr_t;

		class ResolveColRefSubtable : public Visitor {
		public:
			ResolveColRefSubtable(const Expr::subtables_t& subtables = Expr::subtables_t());
			virtual Expr::const_ptr_t modify(const CartoSQL *csql, const ExprColumn& e);
			virtual void visit(const ExprSelect& e);
			virtual void visit(const ExprOSMSelect& e);
			virtual void visit(const ExprCompound& e);
			virtual void visit(const ExprJoin& e);
			virtual void visitend(const ExprSelect& e);
			virtual void visitend(const ExprOSMSelect& e);
			virtual void visitend(const ExprCompound& e);
			virtual void visitend(const ExprJoin& e);
			virtual void select_result_table_available(const CartoSQL *csql, const ExprTable& e);

		protected:
			Expr::subtables_t m_subtables;
			typedef std::vector<bool> enables_t;
			enables_t m_enables;
			unsigned int m_offset;
		};

		class UnresolvedColRef : public Visitor {
		public:
			typedef std::map<Expr::const_ptr_t,ExprTable::const_ptr_t> exprs_t;
			UnresolvedColRef(void) : m_colref(false) {}
			bool is_colref(void) const { return m_colref; }
			const exprs_t& get_expressions(void) const { return m_exprs; }
			std::string get_expressionstring(void) const;
			virtual void visit(const ExprColumn& e);
			virtual void visit(const ExprTable& e);
			virtual void visitend(const ExprTable& e);

		protected:
			typedef std::stack<ExprTable::const_ptr_t> tablestack_t;
			tablestack_t m_tablestack;
			exprs_t m_exprs;
			bool m_colref;
		};

		class UnresolvedSubtableColRef : public Visitor {
		public:
			typedef std::map<Expr::const_ptr_t,ExprTable::const_ptr_t> exprs_t;
			UnresolvedSubtableColRef(void) : m_colref(false) {}
			bool is_colref(void) const { return m_colref; }
			const exprs_t& get_expressions(void) const { return m_exprs; }
			std::string get_expressionstring(void) const;
			virtual void visit(const ExprTableColumn& e);
			virtual void visit(const ExprTable& e);
			virtual void visitend(const ExprTable& e);

		protected:
			typedef std::stack<ExprTable::const_ptr_t> tablestack_t;
			tablestack_t m_tablestack;
			exprs_t m_exprs;
			bool m_colref;
		};

		class PushConditionUpResolve : public Visitor {
		public:
			PushConditionUpResolve(const ExprTable::const_ptr_t& tbl)
				: m_table(tbl), m_error(false) {}

			virtual void visit(const ExprTable& e) { m_error = true; }
			virtual bool descend(const ExprTable& e) { return false; }
			virtual boost::intrusive_ptr<const Expr> modify(const CartoSQL *csql, const ExprTableColumn& e);
			bool is_error(void) const { return m_error; }

		protected:
			ExprTable::const_ptr_t m_table;
			bool m_error;
		};

		class PushConditionUpRename : public Visitor {
		public:
			PushConditionUpRename(unsigned int tblidx)
				: m_tableindex(tblidx), m_error(false) {}

			virtual void visit(const ExprTable& e) { m_error = true; }
			virtual bool descend(const ExprTable& e) { return false; }
			virtual boost::intrusive_ptr<const Expr> modify(const CartoSQL *csql, const ExprTableColumn& e);
			bool is_error(void) const { return m_error; }

		protected:
			unsigned int m_tableindex;
			bool m_error;
		};

		class TableColumnSets : public Visitor {
		public:
			typedef std::map<unsigned int,columnset_t> columnsets_t;

			TableColumnSets(void) : m_tablehierarchy(0) {}

			virtual void visit(const ExprTableColumn& e) { m_columnsets[e.get_tableindex()].insert(e.get_columnindex()); }
			virtual void visit(const ExprTable& e) { ++m_tablehierarchy; }
			virtual void visitend(const ExprTable& e) { --m_tablehierarchy; }
			virtual bool descend(const ExprTable& e) { return m_tablehierarchy <= 1; }
			const columnsets_t& get_columnsets(void) const { return m_columnsets; }
			const columnset_t& get_columnset(unsigned int tblidx) const;

		protected:
			columnsets_t m_columnsets;
			unsigned int m_tablehierarchy;
		};

		class RenumberTableColumns : public Visitor {
		public:
			RenumberTableColumns(unsigned int tblidx, const columnset_t& colsused) : m_colsused(colsused), m_tableindex(tblidx), m_tablehierarchy(0) {}

			virtual void visit(const ExprTable& e) { ++m_tablehierarchy; }
			virtual void visitend(const ExprTable& e) { --m_tablehierarchy; }
			virtual bool descend(const ExprTable& e) { return m_tablehierarchy <= 1; }
			virtual boost::intrusive_ptr<const Expr> modify(const CartoSQL *csql, const ExprTableColumn& e);

		protected:
			columnset_t m_colsused;
			unsigned int m_tableindex;
			unsigned int m_tablehierarchy;
		};

		class CaseInsensitiveCompare;

		enum class type_t : uint8_t {
			tablevalue,
			osmtable,
			literaltable,
			tablefunc,
			select,
			osmselect,
			compound,
			join,
			tablereference,
			commontables,
			invalid = static_cast<std::underlying_type<type_t>::type>(~0)
		};

		typedef Expr::exprlist_t exprlist_t;

		ExprTable(void);
		virtual ~ExprTable();

		unsigned int breference(void) const { return ++m_refcount; }
		unsigned int bunreference(void) const { return --m_refcount; }
		unsigned int get_refcount(void) const { return m_refcount; }
		ptr_t get_ptr(void) { return ptr_t(this); }
		const_ptr_t get_ptr(void) const { return const_ptr_t(this); }
		friend inline void intrusive_ptr_add_ref(const ExprTable *expr) { expr->breference(); }
		friend inline void intrusive_ptr_release(const ExprTable *expr) { if (!expr->bunreference()) delete expr; }

		virtual type_t get_type(void) const = 0;
		void copy(ExprTable& x) const;
		virtual ptr_t clone(void) const = 0;
		virtual bool calculate(Calc& c, Table& t) const;
		virtual const_ptr_t simplify(const CartoSQL *csql) const = 0;
		virtual const_ptr_t simplify(const CartoSQL *csql, Visitor& v) const = 0;
		virtual void visit(Visitor& v) const { v.visit(*this); v.visitend(*this); }
		virtual const_ptr_t visitor_call_modify(const CartoSQL *csql, Visitor& v) const = 0;
		virtual void visitor_call_visit(Visitor& v) const { v.visit(*this); }
		virtual void visitor_call_visitend(Visitor& v) const { v.visitend(*this); }
		virtual Expr::subtables_t get_subtables(void) const { return Expr::subtables_t(); }

		virtual std::ostream& print(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const = 0;

		virtual int compare(const ExprTable& x) const;
		bool operator==(const ExprTable& x) const { return !compare(x); }
		bool operator!=(const ExprTable& x) const { return !operator==(x); }
		bool operator<(const ExprTable& x) const { return compare(x) < 0; }
		bool operator<=(const ExprTable& x) const { return compare(x) <= 0; }
		bool operator>(const ExprTable& x) const { return compare(x) > 0; }
		bool operator>=(const ExprTable& x) const { return compare(x) >= 0; }

		const_ptr_t replace_variable(Calc& c) const;
		virtual const_ptr_t push_filter_up(const CartoSQL *csql, const Expr::const_ptr_t& expr) const { return const_ptr_t(); }
		boost::intrusive_ptr<ExprSelect> select_from(void) const;
		const_ptr_t filter(const CartoSQL *csql, const Expr::const_ptr_t& expr) const;
		bool is_all_columns(const columnset_t& colsused) const;
		virtual const_ptr_t remove_unused_columns(const CartoSQL *csql, columnset_t& colsused) const;
		const_ptr_t remove_unused_columns(const CartoSQL *csql) const;
		virtual const_ptr_t add_sort_columns(const std::vector<unsigned int>& sortcols) const;
		const_ptr_t add_all_sort_columns_except(unsigned int colidx) const;
		const_ptr_t add_all_sort_columns_except(const std::string& colname) const;

		const Columns& get_columns(void) const { return m_columns; }
		Columns& get_columns(void) { return m_columns; }
		void set_columns(const Columns& c) { m_columns = c; }
		void set_columns(Columns&& c) { m_columns.swap(c); }

		static int compare(const const_ptr_t& p0, const const_ptr_t& p1);
		static std::ostream& print_expr(std::ostream& os, const const_ptr_t& e, unsigned int indent, const PrintContext& ctx = PrintContextSQL());

	protected:
		Columns m_columns;
		mutable std::atomic<unsigned int> m_refcount;

		static const_ptr_t simplify_tail(const CartoSQL *csql, const boost::intrusive_ptr<const ExprTable>& p, bool modif);
		template <typename... Args> static bool simplify_int(const CartoSQL *csql, const Expr::subtables_t& subtables, boost::intrusive_ptr<ExprTable> p, Args&&... args);

		class CalcStaticRows : public CalcOverride {
		public:
			CalcStaticRows(Calc& c) : CalcOverride(c) {}
			const CartoSQL *get_csql(void) const { return m_calc.get_csql(); }

			virtual void error(const Expr& e, const char *msg) { m_calc.error(e, msg); }
			virtual void error(const ExprTable& e, const char *msg) { m_calc.error(e, msg); }

			virtual bool get_variable(Value& v, const std::string& var) { return m_calc.get_variable(v, var); }

			virtual const TableRow *get_table_row(unsigned int tableindex) {
				if (tableindex >= m_tablerows.size())
					return CalcOverride::get_table_row(tableindex - m_tablerows.size());
				return m_tablerows[tableindex];
			}
			virtual const OSMDB::Object *get_osmobject(void) { return nullptr; }
			virtual grouprow_t get_group_rows(void) { return 1; }
			virtual void set_group_row(grouprow_t x) { }

			void set_table_row(unsigned int tableindex, const TableRow *tr = nullptr) {
				if (m_tablerows.size() <= tableindex)
					m_tablerows.resize(tableindex + 1, nullptr);
				m_tablerows[tableindex] = tr;
			}

		protected:
			std::vector<const TableRow *> m_tablerows;
		};
	};

	class ExprTableValue : public ExprTable {
	public:
		ExprTableValue(const Table& tbl = Table()) : m_table(tbl) {}

		virtual type_t get_type(void) const { return type_t::tablevalue; }
		void copy(ExprTableValue& x) const;
		virtual ptr_t clone(void) const { boost::intrusive_ptr<ExprTableValue> p(new ExprTableValue()); copy(*p); return p; }
		std::pair<bool,bool> is_renamed(void) const;
		virtual bool calculate(Calc& c, Table& t) const;
		virtual const_ptr_t simplify(const CartoSQL *csql) const;
		virtual const_ptr_t simplify(const CartoSQL *csql, Visitor& v) const;
		virtual void visit(Visitor& v) const { v.visit(*this); v.visitend(*this); }
		virtual const_ptr_t visitor_call_modify(const CartoSQL *csql, Visitor& v) const { return v.modify(csql, *this); }
		virtual void visitor_call_visit(Visitor& v) const { v.visit(*this); }
		virtual void visitor_call_visitend(Visitor& v) const { v.visitend(*this); }
		virtual std::ostream& print(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const;
		virtual int compare(const ExprTable& x) const;
		virtual const_ptr_t push_filter_up(const CartoSQL *csql, const Expr::const_ptr_t& expr) const;
		virtual const_ptr_t remove_unused_columns(const CartoSQL *csql, columnset_t& colsused) const;

		const Table& get_table(void) const { return m_table; }
		Table& get_table(void) { return m_table; }
		void set_table(const Table& d) { m_table = d; }
		void set_table(Table&& d) { m_table.swap(d); }

	protected:
		Table m_table;
	};

	class ExprTableSort : public ExprTable {
	public:
		class Term {
		public:
			enum class direction_t : uint8_t {
				ascending,
				descending
			};

			enum class nulls_t : uint8_t {
				default_,
				first,
				last
			};

			Term(const Expr::const_ptr_t& expr = Expr::const_ptr_t(), direction_t dir = direction_t::ascending,
			     nulls_t nulls = nulls_t::default_) : m_expr(expr), m_direction(dir), m_nulls(nulls) {}

			const Expr::const_ptr_t& get_expr(void) const { return m_expr; }
			direction_t get_direction(void) const { return m_direction; }
			nulls_t get_nulls(void) const { return m_nulls; }

			void set_expr(const Expr::const_ptr_t& x) { m_expr = x; }
			void set_direction(direction_t x) { m_direction = x; }
			void set_nulls(nulls_t x) { m_nulls = x; }

			void resolve_nulls(void);
			bool simplify(const CartoSQL *csql);
			bool simplify(const CartoSQL *csql, Visitor& v);
			void visit(Visitor& v) const;
			std::ostream& print(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const;
		        int compare(const Term& x) const;

		protected:
			Expr::const_ptr_t m_expr;
			direction_t m_direction;
			nulls_t m_nulls;

			template <typename... Args> bool simplify_int(const CartoSQL *csql, Args&&... args);
		};

		class Terms : public std::vector<Term> {
		public:
			bool simplify(const CartoSQL *csql);
			bool simplify(const CartoSQL *csql, Visitor& v);
			void visit(Visitor& v) const;
			std::ostream& print(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const;
		        int compare(const Terms& x) const;

		protected:
			template <typename... Args> bool simplify_int(const CartoSQL *csql, Args&&... args);
		};

		void copy(ExprTableSort& x) const;
		virtual void visit(Visitor& v) const;
		virtual std::ostream& print(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const;
		virtual int compare(const ExprTable& x) const;
		virtual const_ptr_t add_sort_columns(const std::vector<unsigned int>& sortcols) const;

		const Terms& get_terms(void) const { return m_terms; }
		Terms& get_terms(void) { return m_terms; }
		const Expr::const_ptr_t& get_limit(void) const { return m_limit; }
		const Expr::const_ptr_t& get_offset(void) const { return m_offset; }

		void set_terms(const Terms& x) { m_terms = x; }
		void set_terms(Terms&& x) { m_terms.swap(x); }
		void set_limit(const Expr::const_ptr_t& x) { m_limit = x; }
		void set_offset(const Expr::const_ptr_t& x) { m_offset = x; }

	protected:
		ExprTable::const_ptr_t m_subtable;
		Terms m_terms;
		Expr::const_ptr_t m_limit;
		Expr::const_ptr_t m_offset;

		template <typename... Args> static bool simplify_order(const CartoSQL *csql, const Expr::subtables_t& subtables, boost::intrusive_ptr<ExprTableSort> p, Args&&... args);
		template <typename... Args> static bool simplify_int(const CartoSQL *csql, const Expr::subtables_t& subtables, boost::intrusive_ptr<ExprTableSort> p, Args&&... args);

		class TermsSorter {
		public:
			TermsSorter(const Terms& trm) : m_terms(trm) {}
			int compare(const TableRowValues& a, const TableRowValues& b) const;
			bool operator()(const TableRowValues& a, const TableRowValues& b) const { return compare(a, b) < 0; }

		protected:
			const Terms& m_terms;
		};

		bool apply_offset_limit(Calc& c, Table& t) const;
	};

	class ExprOSMTable : public ExprTable {
	public:
		typedef OSMDB::Object::type_t table_t;

		ExprOSMTable(table_t tab = table_t::point) : m_table(tab) {}

		virtual type_t get_type(void) const { return type_t::osmtable; }
		void copy(ExprOSMTable& x) const;
		virtual ptr_t clone(void) const { boost::intrusive_ptr<ExprOSMTable> p(new ExprOSMTable()); copy(*p); return p; }
		void compute_columns(const CartoSQL& csql);
		std::pair<bool,bool> is_renamed(const CartoSQL *csql) const;
		virtual bool calculate(Calc& c, Table& t) const;
		virtual const_ptr_t simplify(const CartoSQL *csql) const;
		virtual const_ptr_t simplify(const CartoSQL *csql, Visitor& v) const;
		virtual void visit(Visitor& v) const { v.visit(*this); v.visitend(*this); }
		virtual const_ptr_t visitor_call_modify(const CartoSQL *csql, Visitor& v) const { return v.modify(csql, *this); }
		virtual void visitor_call_visit(Visitor& v) const { v.visit(*this); }
		virtual void visitor_call_visitend(Visitor& v) const { v.visitend(*this); }
		virtual std::ostream& print(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const;
		virtual bool is_paren(void) const { return false; }
		virtual int compare(const ExprTable& x) const;
		virtual const_ptr_t push_filter_up(const CartoSQL *csql, const Expr::const_ptr_t& expr) const;
		virtual const_ptr_t remove_unused_columns(const CartoSQL *csql, columnset_t& colsused) const;

		table_t get_table(void) const { return m_table; }
		void set_table(table_t x) { m_table = x; }

		static bool parse_tablename(table_t& t, const std::string& str);
		static const std::string& get_tablename(table_t t);
		const std::string& get_tablename(void) const { return get_tablename(get_table()); }

		static bool get_dbobjs(OSMDB::objects_t& objs, Calc& c, table_t tbl, const ExprTable *expr);
		bool get_dbobjs(OSMDB::objects_t& objs, Calc& c) const { return get_dbobjs(objs, c, get_table(), this); }

	protected:
		table_t m_table;

		void default_columns(Columns& cols, const CartoSQL& csql, table_t tbl) const;

		class OSMCalc;
	};

	class ExprLiteralTable : public ExprTableSort {
	public:
		typedef std::vector<exprlist_t> literaltable_t;

		ExprLiteralTable(const literaltable_t& data = literaltable_t()) : m_data(data) {}

		virtual type_t get_type(void) const { return type_t::literaltable; }
		void copy(ExprLiteralTable& x) const;
		virtual ptr_t clone(void) const { boost::intrusive_ptr<ExprLiteralTable> p(new ExprLiteralTable()); copy(*p); return p; }
		void compute_columns(void);
		std::pair<bool,bool> is_renamed(void) const;
		virtual bool calculate(Calc& c, Table& t) const;
		virtual const_ptr_t simplify(const CartoSQL *csql) const;
		virtual const_ptr_t simplify(const CartoSQL *csql, Visitor& v) const;
		virtual void visit(Visitor& v) const;
		virtual const_ptr_t visitor_call_modify(const CartoSQL *csql, Visitor& v) const { return v.modify(csql, *this); }
		virtual void visitor_call_visit(Visitor& v) const { v.visit(*this); }
		virtual void visitor_call_visitend(Visitor& v) const { v.visitend(*this); }
		virtual std::ostream& print(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const;
		virtual int compare(const ExprTable& x) const;
		virtual const_ptr_t push_filter_up(const CartoSQL *csql, const Expr::const_ptr_t& expr) const;
		virtual const_ptr_t remove_unused_columns(const CartoSQL *csql, columnset_t& colsused) const;

		const literaltable_t& get_data(void) const { return m_data; }
		literaltable_t& get_data(void) { return m_data; }
		void set_data(const literaltable_t& d) { m_data = d; }
		void set_data(literaltable_t&& d) { m_data.swap(d); }

	protected:
		literaltable_t m_data;

		template <typename... Args> static bool simplify_int(const CartoSQL *csql, const Expr::subtables_t& subtables, boost::intrusive_ptr<ExprLiteralTable> p, Args&&... args);
	};

	class ExprTableFunc : public ExprTable {
	public:
		enum class func_t : uint8_t {
			first,
			unnest = first,
			generate_series,
			last = generate_series
		};

		ExprTableFunc(func_t func = func_t::unnest, const exprlist_t& expr = exprlist_t()) : m_expr(expr), m_func(func) {}

		virtual type_t get_type(void) const { return type_t::tablefunc; }
		void copy(ExprTableFunc& x) const;
		virtual ptr_t clone(void) const { boost::intrusive_ptr<ExprTableFunc> p(new ExprTableFunc()); copy(*p); return p; }
		void compute_columns(void);
		std::pair<bool,bool> is_renamed(void) const;
		virtual bool calculate(Calc& c, Table& t) const;
		virtual const_ptr_t simplify(const CartoSQL *csql) const;
		virtual const_ptr_t simplify(const CartoSQL *csql, Visitor& v) const;
		virtual void visit(Visitor& v) const;
		virtual const_ptr_t visitor_call_modify(const CartoSQL *csql, Visitor& v) const { return v.modify(csql, *this); }
		virtual void visitor_call_visit(Visitor& v) const { v.visit(*this); }
		virtual void visitor_call_visitend(Visitor& v) const { v.visitend(*this); }
		virtual std::ostream& print(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const;
		virtual int compare(const ExprTable& x) const;

		func_t get_func(void) const { return m_func; }
		void set_func(func_t f) { m_func = f; }
		const exprlist_t& get_expr(void) const { return m_expr; }
		exprlist_t& get_expr(void) { return m_expr; }
		void set_expr(const exprlist_t& e) { m_expr = e; }
		void set_expr(exprlist_t&& e) { m_expr.swap(e); }

	protected:
		exprlist_t m_expr;
		func_t m_func;

		template <typename... Args> static bool simplify_int(const CartoSQL *csql, const Expr::subtables_t& subtables, boost::intrusive_ptr<ExprTableFunc> p, Args&&... args);
	};

	class ExprSelectBase : public ExprTableSort {
	public:
		enum class distinct_t : uint8_t {
			all,
			distinctrows,
			distinctexpr
		};

		ExprSelectBase(void);
		void copy(ExprSelectBase& x) const;
		bool is_renamed(void) const { return !m_columns.empty() && m_columns.front().get_table() != default_tablename; }
		virtual void visit(Visitor& v) const;
		virtual int compare(const ExprTable& x) const;

		const exprlist_t& get_distinctexpr(void) const { return m_distinctexpr; }
		exprlist_t& get_distinctexpr(void) { return m_distinctexpr; }
		void set_distinctexpr(const exprlist_t& de) { m_distinctexpr = de; }
		void set_distinctexpr(exprlist_t&& de) { m_distinctexpr.swap(de); }
		distinct_t get_distinct(void) const { return m_distinct; }
		void set_distinct(distinct_t d) { m_distinct = d; }
		const Expr::const_ptr_t& get_whereexpr(void) const { return m_whereexpr; }
		void set_whereexpr(const Expr::const_ptr_t& e) { m_whereexpr = e; }
		const exprlist_t& get_groupexpr(void) const { return m_groupexpr; }
		exprlist_t& get_groupexpr(void) { return m_groupexpr; }
		void set_groupexpr(const exprlist_t& e) { m_groupexpr = e; }
		const Expr::const_ptr_t& get_havingexpr(void) const { return m_havingexpr; }
		void set_havingexpr(const Expr::const_ptr_t& e) { m_havingexpr = e; }

	protected:
		exprlist_t m_distinctexpr;
		Expr::const_ptr_t m_whereexpr;
		exprlist_t m_groupexpr;
		Expr::const_ptr_t m_havingexpr;
		distinct_t m_distinct;

		static constexpr char default_tablename[] = "?select?";
		static std::string default_column_name(const Expr::const_ptr_t& expr, const ExprTable::const_ptr_t& fromexpr);
		void compute_columns(const ExprTable::const_ptr_t& fromexpr);
		std::ostream& print1(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const;
		std::ostream& print2(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const;
		template <typename... Args> static bool simplify_preresult(const CartoSQL *csql, const Expr::subtables_t& subtables, boost::intrusive_ptr<ExprSelectBase> p, Args&&... args);
		template <typename... Args> static bool simplify_postresult(const CartoSQL *csql, const Expr::subtables_t& subtables, boost::intrusive_ptr<ExprSelectBase> p, Args&&... args);
		template <typename... Args> static bool simplify_int(const CartoSQL *csql, const Expr::subtables_t& subtables, boost::intrusive_ptr<ExprSelectBase> p, Args&&... args);
	};

	class ExprSelect : public ExprSelectBase {
	public:
		ExprSelect(void);

		virtual type_t get_type(void) const { return type_t::select; }
		void copy(ExprSelect& x) const;
		virtual ptr_t clone(void) const { boost::intrusive_ptr<ExprSelect> p(new ExprSelect()); copy(*p); return p; }
		void compute_columns(void);
		boost::intrusive_ptr<ExprOSMSelect> convert_osmselect(const CartoSQL *csql) const;
		virtual bool calculate(Calc& c, Table& t) const;
		virtual const_ptr_t simplify(const CartoSQL *csql) const;
		virtual const_ptr_t simplify(const CartoSQL *csql, Visitor& v) const;
		virtual void visit(Visitor& v) const;
		virtual const_ptr_t visitor_call_modify(const CartoSQL *csql, Visitor& v) const { return v.modify(csql, *this); }
		virtual void visitor_call_visit(Visitor& v) const { v.visit(*this); }
		virtual void visitor_call_visitend(Visitor& v) const { v.visitend(*this); }
		virtual Expr::subtables_t get_subtables(void) const;
		virtual std::ostream& print(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const;
		virtual int compare(const ExprTable& x) const;
		virtual const_ptr_t push_filter_up(const CartoSQL *csql, const Expr::const_ptr_t& expr) const;
		virtual const_ptr_t remove_unused_columns(const CartoSQL *csql, columnset_t& colsused) const;

		const ExprTable::const_ptr_t& get_fromexpr(void) const { return m_fromexpr; }
		void set_fromexpr(const ExprTable::const_ptr_t& e) { m_fromexpr = e; }

	protected:
		ExprTable::const_ptr_t m_fromexpr;

		template <typename... Args> static bool simplify_preresult(const CartoSQL *csql, const Expr::subtables_t& subtables, boost::intrusive_ptr<ExprSelect> p, Args&&... args);
		template <typename... Args> static bool simplify_int(const CartoSQL *csql, const Expr::subtables_t& subtables, boost::intrusive_ptr<ExprSelect> p, Args&&... args);

		class ConvertOSM;
		class SelectCalc;
	};

	class ExprOSMSelect : public ExprSelectBase {
	public:
		typedef OSMDB::Object::type_t table_t;

		ExprOSMSelect(void);

		virtual type_t get_type(void) const { return type_t::osmselect; }
		void copy(ExprOSMSelect& x) const;
		virtual ptr_t clone(void) const { boost::intrusive_ptr<ExprOSMSelect> p(new ExprOSMSelect()); copy(*p); return p; }
		virtual bool calculate(Calc& c, Table& t) const;
		virtual const_ptr_t simplify(const CartoSQL *csql) const;
		virtual const_ptr_t simplify(const CartoSQL *csql, Visitor& v) const;
		virtual void visit(Visitor& v) const;
		virtual const_ptr_t visitor_call_modify(const CartoSQL *csql, Visitor& v) const { return v.modify(csql, *this); }
		virtual void visitor_call_visit(Visitor& v) const { v.visit(*this); }
		virtual void visitor_call_visitend(Visitor& v) const { v.visitend(*this); }
		virtual Expr::subtables_t get_subtables(void) const;
		virtual std::ostream& print(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const;
		virtual int compare(const ExprTable& x) const;
		virtual const_ptr_t push_filter_up(const CartoSQL *csql, const Expr::const_ptr_t& expr) const;
		virtual const_ptr_t remove_unused_columns(const CartoSQL *csql, columnset_t& colsused) const;

		table_t get_from(void) const { return m_from; }
		void set_from(table_t t) { m_from = t; }

	protected:
	        table_t m_from;

		class OSMSelectCalc;
	};

	class ExprCompound : public ExprTableSort {
	public:
		enum class operator_t : uint8_t {
			union_,
			intersect,
			except
		};

		enum class distinct_t : uint8_t {
			distinct,
			all
		};

		ExprCompound(const ExprTable::const_ptr_t& se0 = ExprTable::const_ptr_t(),
			     const ExprTable::const_ptr_t& se1 = ExprTable::const_ptr_t(),
			     operator_t op = operator_t::union_, distinct_t dis = distinct_t::distinct)
			: m_operator(op), m_distinct(dis) { m_subtable[0] = se0; m_subtable[1] = se1; }

		virtual type_t get_type(void) const { return type_t::compound; }
		void copy(ExprCompound& x) const;
		virtual ptr_t clone(void) const { boost::intrusive_ptr<ExprCompound> p(new ExprCompound()); copy(*p); return p; }
		void compute_columns(void);
		std::pair<bool,bool> is_renamed(void) const;
		virtual bool calculate(Calc& c, Table& t) const;
		virtual const_ptr_t simplify(const CartoSQL *csql) const;
		virtual const_ptr_t simplify(const CartoSQL *csql, Visitor& v) const;
		virtual void visit(Visitor& v) const;
		virtual const_ptr_t visitor_call_modify(const CartoSQL *csql, Visitor& v) const { return v.modify(csql, *this); }
		virtual void visitor_call_visit(Visitor& v) const { v.visit(*this); }
		virtual void visitor_call_visitend(Visitor& v) const { v.visitend(*this); }
		virtual Expr::subtables_t get_subtables(void) const;
		virtual std::ostream& print(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const;
		virtual int compare(const ExprTable& x) const;
		virtual const_ptr_t push_filter_up(const CartoSQL *csql, const Expr::const_ptr_t& expr) const;
		virtual const_ptr_t remove_unused_columns(const CartoSQL *csql, columnset_t& colsused) const;

		const ExprTable::const_ptr_t& get_subtable(unsigned int index) const { return m_subtable[!!index]; }
		operator_t get_operator(void) const { return m_operator; }
		distinct_t get_distinct(void) const { return m_distinct; }

		void set_subtable(unsigned int index, const ExprTable::const_ptr_t& x) { m_subtable[!!index] = x; }
		void set_operator(operator_t x) { m_operator = x; }
		void set_distinct(distinct_t x) { m_distinct = x; }

	protected:
		ExprTable::const_ptr_t m_subtable[2];
		operator_t m_operator;
		distinct_t m_distinct;

		static bool types_compound_compatible(Value::type_t t0, Value::type_t t1);
		void default_columns(Columns& cols) const;
		template <typename... Args> static bool simplify_int(const CartoSQL *csql, const Expr::subtables_t& subtables, boost::intrusive_ptr<ExprCompound> p, Args&&... args);

		class CompoundCalc;
	};

	class JoinColumn {
	public:
		typedef ExprTable::Columns::size_type size_type;
		JoinColumn(size_type left = std::numeric_limits<size_type>::max(), size_type right = std::numeric_limits<size_type>::max()) { m_idx[0] = left; m_idx[1] = right; }
		size_type get_left(void) const { return m_idx[0]; }
		size_type get_right(void) const { return m_idx[1]; }
		size_type operator[](unsigned int idx) const { return m_idx[!!idx]; }
		int compare(const JoinColumn& x) const;
		void get_columns_used(columnset_t colsets[2]) const;
		void renumber_columns(const columnset_t colsused[2]);

	protected:
		size_type m_idx[2];
	};

	class JoinColumns : public std::vector<JoinColumn> {
	public:
		typedef std::set<JoinColumn::size_type> joincols_t;
		joincols_t get_joincols(unsigned int idx) const;
		int compare(const JoinColumns& x) const;
		void get_columns_used(columnset_t colsets[2]) const;
		void renumber_columns(const columnset_t colsused[2]);
	};

	class Error : public std::runtime_error {
	public:
		explicit Error(const std::string& x);
	};

	class ExprJoin : public ExprTable {
	public:
		enum class jointype_t : uint8_t {
			inner,
			leftouter,
			rightouter,
			fullouter
		};

		enum class constraint_t : uint8_t {
			none,
			column,
			expr
		};

		ExprJoin(const ExprTable::const_ptr_t& se0 = ExprTable::const_ptr_t(),
			 const ExprTable::const_ptr_t& se1 = ExprTable::const_ptr_t(),
			 jointype_t jointype = jointype_t::inner);
		~ExprJoin();

		virtual type_t get_type(void) const { return type_t::join; }
		void copy(ExprJoin& x) const;
		virtual ptr_t clone(void) const { boost::intrusive_ptr<ExprJoin> p(new ExprJoin()); copy(*p); return p; }
		void compute_columns(void);
		void compute_joincolumns(const std::vector<std::string>& joincols);
		void compute_joincolumns_natural(void);
		std::pair<bool,bool> is_renamed(void) const;
		virtual bool calculate(Calc& c, Table& t) const;
		virtual const_ptr_t simplify(const CartoSQL *csql) const;
		virtual const_ptr_t simplify(const CartoSQL *csql, Visitor& v) const;
		virtual void visit(Visitor& v) const;
		virtual const_ptr_t visitor_call_modify(const CartoSQL *csql, Visitor& v) const { return v.modify(csql, *this); }
		virtual void visitor_call_visit(Visitor& v) const { v.visit(*this); }
		virtual void visitor_call_visitend(Visitor& v) const { v.visitend(*this); }
		virtual Expr::subtables_t get_subtables(void) const;
		virtual std::ostream& print(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const;
		virtual int compare(const ExprTable& x) const;
		virtual const_ptr_t push_filter_up(const CartoSQL *csql, const Expr::const_ptr_t& expr) const;
		virtual const_ptr_t remove_unused_columns(const CartoSQL *csql, columnset_t& colsused) const;

		const ExprTable::const_ptr_t& get_subtable(unsigned int index) const { return m_subtable[!!index]; }
		const Expr::const_ptr_t& get_joinexpr(void) const;
		const JoinColumns& get_joincolumns(void) const;
		std::vector<std::string> get_joincolumns_string(void) const;
		jointype_t get_jointype(void) const { return m_jointype; }
		constraint_t get_constraint(void) const { return m_constraint; }

		void set_subtable(unsigned int index, const ExprTable::const_ptr_t& x) { m_subtable[!!index] = x; }
		void set_joinexpr(const Expr::const_ptr_t& x);
		void set_joincolumns(const JoinColumns& x);
		void set_joincolumns(JoinColumns&& x);
		void set_jointype(jointype_t x) { m_jointype = x; }
		void set_constraint_none(void);

	protected:
		ExprTable::const_ptr_t m_subtable[2];
		typedef Expr::const_ptr_t joinexpr_t;
		union {
			joinexpr_t m_joinexpr;
			JoinColumns m_joincolumns;
		};
		jointype_t m_jointype;
		constraint_t m_constraint;

		void default_columns(Columns& cols) const;
		template <typename... Args> static bool simplify_int(const CartoSQL *csql, const Expr::subtables_t& subtables, boost::intrusive_ptr<ExprJoin> p, Args&&... args);

		class JoinCalc;
		class JoinColSorter;
	};

	class ExprTableReference : public ExprTable {
	public:
		ExprTableReference(const std::string& name = "") : m_name(name) {}

		virtual type_t get_type(void) const { return type_t::tablereference; }
		void copy(ExprTableReference& x) const;
		virtual ptr_t clone(void) const { boost::intrusive_ptr<ExprTableReference> p(new ExprTableReference()); copy(*p); return p; }
		virtual bool calculate(Calc& c, Table& t) const;
		virtual const_ptr_t simplify(const CartoSQL *csql) const;
		virtual const_ptr_t simplify(const CartoSQL *csql, Visitor& v) const;
		virtual void visit(Visitor& v) const { v.visit(*this); v.visitend(*this); }
		virtual const_ptr_t visitor_call_modify(const CartoSQL *csql, Visitor& v) const { return v.modify(csql, *this); }
		virtual void visitor_call_visit(Visitor& v) const { v.visit(*this); }
		virtual void visitor_call_visitend(Visitor& v) const { v.visitend(*this); }
		virtual std::ostream& print(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const;
		virtual int compare(const ExprTable& x) const;

		const std::string& get_name(void) const { return m_name; }
		std::string& get_name(void) { return m_name; }
		void set_name(const std::string& n) { m_name = n; }
		void set_name(std::string&& n) { m_name.swap(n); }

	protected:
		std::string m_name;
	};

	class ExprCommonTables : public ExprTable {
	public:
		typedef std::vector<const_ptr_t> namedtables_t;

		ExprCommonTables(const const_ptr_t& subtbl = const_ptr_t(), const namedtables_t& tbls = namedtables_t());

		virtual type_t get_type(void) const { return type_t::commontables; }
		void copy(ExprCommonTables& x) const;
		virtual ptr_t clone(void) const { boost::intrusive_ptr<ExprCommonTables> p(new ExprCommonTables()); copy(*p); return p; }
		virtual bool calculate(Calc& c, Table& t) const;
		virtual const_ptr_t simplify(const CartoSQL *csql) const;
		virtual const_ptr_t simplify(const CartoSQL *csql, Visitor& v) const;
		virtual void visit(Visitor& v) const;
		virtual const_ptr_t visitor_call_modify(const CartoSQL *csql, Visitor& v) const { return v.modify(csql, *this); }
		virtual void visitor_call_visit(Visitor& v) const { v.visit(*this); }
		virtual void visitor_call_visitend(Visitor& v) const { v.visitend(*this); }
		virtual Expr::subtables_t get_subtables(void) const;
		virtual std::ostream& print(std::ostream& os, unsigned int indent, const PrintContext& ctx = PrintContextSQL()) const;
		virtual int compare(const ExprTable& x) const;
		virtual const_ptr_t push_filter_up(const CartoSQL *csql, const Expr::const_ptr_t& expr) const;

		const const_ptr_t& get_subtable(void) const { return m_subtable; }
		void set_subtable(const const_ptr_t& t);
		void set_subtable(const_ptr_t&& t);
		const namedtables_t& get_tables(void) const { return m_tables; }
		void set_tables(const namedtables_t& t);
		void set_tables(namedtables_t&& t);
		const const_ptr_t& find_table(const std::string& n) const;

	protected:
		const_ptr_t m_subtable;
		namedtables_t m_tables;

		template <typename... Args> static bool simplify_int(const CartoSQL *csql, const Expr::subtables_t& subtables, boost::intrusive_ptr<ExprCommonTables> p, Args&&... args);
		void sort_tables(void);
		class CommonTablesCalc;
		class NameSorter;
	};

	class SQLError : public Error {
	public:
		static constexpr std::size_t invalid = static_cast<std::size_t>(-1);

		SQLError(const std::string& summary = "", const std::string& info = "",
			 const std::string& line = "", std::size_t linenum = invalid,
			 std::size_t colnum = invalid)
			: Error(summary), m_info(info), m_line(line),
			  m_linenum(linenum), m_colnum(colnum) {}

		const std::string& get_info(void) const { return m_info; }
		const std::string& get_line(void) const { return m_line; }
		std::size_t get_linenum(void) const { return m_linenum; }
		std::size_t get_colnum(void) const { return m_colnum; }

	protected:
		std::string m_info;
		std::string m_line;
		std::size_t m_linenum;
		std::size_t m_colnum;
	};

	CartoSQL(void) : m_osmdb(nullptr) {}

	typedef std::map<std::string,CartoSQL::Value::type_t> osmschema_t;
	const osmschema_t& get_osmschema(void) const { return m_osmschema; }

	const OSMDB *get_osmdb(void) const { return m_osmdb; }
	void set_osmdb(const OSMDB *odb = nullptr) { m_osmdb = odb; }

	ExprTable::const_ptr_t parse_sql(std::istream& is, std::ostream *msg);
	Expr::const_ptr_t parse_sql_value(std::istream& is, std::ostream *msg);
	void parse_style(std::istream& is, std::ostream *msg);
	void parse_style(const std::string& fn, std::ostream *msg);

	std::ostream& print_expr(std::ostream& os, const Expr::const_ptr_t& expr, unsigned int indent = 0) const;
	std::ostream& print_expr(std::ostream& os, const ExprTable::const_ptr_t& expr, unsigned int indent = 0) const;
	static std::ostream& print_table(std::ostream& os, const Table& tbl, const ExprTable::const_ptr_t& expr = ExprTable::const_ptr_t(),
					 unsigned int indent = 0, const std::string& delim = "  ",
					 Value::sqlstringflags_t flags = Value::sqlstringflags_t::all);
	static std::ostream& print_table(std::ostream& os, const IndexTable& tbl, unsigned int indent = 0, const std::string& delim = "  ",
					 Value::sqlstringflags_t flags = Value::sqlstringflags_t::all);

protected:
	class ReplaceVariable : public Visitor {
	public:
		ReplaceVariable(Calc& c) : m_calc(c) {}
		virtual Expr::const_ptr_t modify(const CartoSQL *csql, const ExprVariable& e) {
			Value v;
			if (!m_calc.get_variable(v, e.get_variable()))
				return Expr::const_ptr_t();
			return Expr::const_ptr_t(new ExprValue(v));
		}

	protected:
		Calc& m_calc;
	};

	osmschema_t m_osmschema;
	const OSMDB *m_osmdb;
};

const std::string& to_str(CartoSQL::Value::type_t x);
inline std::ostream& operator<<(std::ostream& os, CartoSQL::Value::type_t x) { return os << to_str(x); }
const std::string& to_str(CartoSQL::Expr::type_t x);
inline std::ostream& operator<<(std::ostream& os, CartoSQL::Expr::type_t x) { return os << to_str(x); }
const std::string& to_str(CartoSQL::ExprFunc::func_t x);
inline std::ostream& operator<<(std::ostream& os, CartoSQL::ExprFunc::func_t x) { return os << to_str(x); }
const std::string& to_str(CartoSQL::ExprFuncAggregate::func_t t);
inline std::ostream& operator<<(std::ostream& os, CartoSQL::ExprFuncAggregate::func_t x) { return os << to_str(x); }
const std::string& to_str(CartoSQL::ExprFuncTable::func_t t);
inline std::ostream& operator<<(std::ostream& os, CartoSQL::ExprFuncTable::func_t x) { return os << to_str(x); }
const std::string& to_str(CartoSQL::ExprTagMember::tag_t x);
inline std::ostream& operator<<(std::ostream& os, CartoSQL::ExprTagMember::tag_t x) { return os << to_str(x); }
const std::string& to_str(CartoSQL::ExprColumn::record_t x);
inline std::ostream& operator<<(std::ostream& os, CartoSQL::ExprColumn::record_t x) { return os << to_str(x); }
const std::string& to_str(CartoSQL::ExprOSMSpecial::field_t x);
inline std::ostream& operator<<(std::ostream& os, CartoSQL::ExprOSMSpecial::field_t x) { return os << to_str(x); }
const std::string& to_str(CartoSQL::ExprTable::type_t x);
inline std::ostream& operator<<(std::ostream& os, CartoSQL::ExprTable::type_t x) { return os << to_str(x); }
const std::string& to_str(CartoSQL::ExprTableSort::Term::direction_t x);
inline std::ostream& operator<<(std::ostream& os, CartoSQL::ExprTableSort::Term::direction_t x) { return os << to_str(x); }
const std::string& to_str(CartoSQL::ExprTableSort::Term::nulls_t x);
inline std::ostream& operator<<(std::ostream& os, CartoSQL::ExprTableSort::Term::nulls_t x) { return os << to_str(x); }
const std::string& to_str(CartoSQL::ExprTableFunc::func_t t);
inline std::ostream& operator<<(std::ostream& os, CartoSQL::ExprTableFunc::func_t x) { return os << to_str(x); }
const std::string& to_str(CartoSQL::ExprSelectBase::distinct_t x);
inline std::ostream& operator<<(std::ostream& os, CartoSQL::ExprSelectBase::distinct_t x) { return os << to_str(x); }
const std::string& to_str(CartoSQL::ExprJoin::jointype_t x);
inline std::ostream& operator<<(std::ostream& os, CartoSQL::ExprJoin::jointype_t x) { return os << to_str(x); }
const std::string& to_str(CartoSQL::ExprJoin::constraint_t x);
inline std::ostream& operator<<(std::ostream& os, CartoSQL::ExprJoin::constraint_t x) { return os << to_str(x); }
const std::string& to_str(CartoSQL::ExprCompound::operator_t x);
inline std::ostream& operator<<(std::ostream& os, CartoSQL::ExprCompound::operator_t x) { return os << to_str(x); }
const std::string& to_str(CartoSQL::ExprCompound::distinct_t x);
inline std::ostream& operator<<(std::ostream& os, CartoSQL::ExprCompound::distinct_t x) { return os << to_str(x); }

inline CartoSQL::Value::sqlstringflags_t operator|(CartoSQL::Value::sqlstringflags_t x, CartoSQL::Value::sqlstringflags_t y) { return (CartoSQL::Value::sqlstringflags_t)((unsigned int)x | (unsigned int)y); }
inline CartoSQL::Value::sqlstringflags_t operator&(CartoSQL::Value::sqlstringflags_t x, CartoSQL::Value::sqlstringflags_t y) { return (CartoSQL::Value::sqlstringflags_t)((unsigned int)x & (unsigned int)y); }
inline CartoSQL::Value::sqlstringflags_t operator^(CartoSQL::Value::sqlstringflags_t x, CartoSQL::Value::sqlstringflags_t y) { return (CartoSQL::Value::sqlstringflags_t)((unsigned int)x ^ (unsigned int)y); }
inline CartoSQL::Value::sqlstringflags_t operator~(CartoSQL::Value::sqlstringflags_t x){ return (CartoSQL::Value::sqlstringflags_t)~(unsigned int)x; }
inline CartoSQL::Value::sqlstringflags_t& operator|=(CartoSQL::Value::sqlstringflags_t& x, CartoSQL::Value::sqlstringflags_t y) { x = x | y; return x; }
inline CartoSQL::Value::sqlstringflags_t& operator&=(CartoSQL::Value::sqlstringflags_t& x, CartoSQL::Value::sqlstringflags_t y) { x = x & y; return x; }
inline CartoSQL::Value::sqlstringflags_t& operator^=(CartoSQL::Value::sqlstringflags_t& x, CartoSQL::Value::sqlstringflags_t y) { x = x ^ y; return x; }

#endif /* CARTOSQL_HH */
