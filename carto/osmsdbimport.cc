/*****************************************************************************/

/*
 *      osmsdbimport.cc  --  OpenStreetMap proprietary static database importer.
 *
 *      Copyright (C) 2020  Thomas Sailer (t.sailer@alumni.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/*****************************************************************************/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <sys/stat.h>
#include <fcntl.h>

#include <iomanip>
#include <fstream>
#include <zlib.h>

#include "osmsdb.h"
#include "hibernate.h"
#include "mapfile.h"

const char * const OSMStaticDB::ImportSourceFiles::default_filenames[] = {
	"ne_110m_admin_0_boundary_lines_land/ne_110m_admin_0_boundary_lines_land.shp",
	"antarctica-icesheet-polygons-3857/icesheet_polygons.shp",
	"antarctica-icesheet-outlines-3857/icesheet_outlines.shp",
	"water-polygons-split-3857/water_polygons.shp",
	"simplified-water-polygons-split-3857/simplified_water_polygons.shp"
};

OSMStaticDB::ImportSourceFiles::ImportSourceFiles(void)
{
	set_default();
}

void OSMStaticDB::ImportSourceFiles::set_default(void)
{
	for (unsigned int l(static_cast<unsigned int>(layer_t::first)); l <= static_cast<unsigned int>(layer_t::last); ++l)
		m_file[l] = default_filenames[l];
}

void OSMStaticDB::ImportSourceFiles::set_default(const std::string& dir)
{
	for (unsigned int l(static_cast<unsigned int>(layer_t::first)); l <= static_cast<unsigned int>(layer_t::last); ++l)
		m_file[l] = Glib::build_filename(dir, default_filenames[l]);
}

const std::string& OSMStaticDB::ImportSourceFiles::get_file(layer_t layer) const
{
	static const std::string empty;
	if (layer > layer_t::last)
		return empty;
	return m_file[static_cast<unsigned int>(layer)];
}

void OSMStaticDB::ImportSourceFiles::set_file(layer_t layer, const std::string& file)
{
	if (layer <= layer_t::last)
		m_file[static_cast<unsigned int>(layer)] = file;
}

#if defined(HAVE_GDAL)

#if defined(HAVE_GDAL2)
#include <gdal.h>
#include <ogr_geometry.h>
#include <ogr_feature.h>
#include <ogrsf_frmts.h>
#else
#include <ogrsf_frmts.h>
#endif

class OSMStaticDB::Import {
public:
	Import(const std::string& tmpdir, importflags_t flags = importflags_t::none);
	~Import();

	void osmimport(int outfd, const ImportSourceFiles& infnames);
	void osmimport(const std::string& outfname, const ImportSourceFiles& infnames);

	class ShapeFile;

protected:
	struct FileIDSorter {
		bool operator()(const Object::id_t& a, const Object::id_t& b) const { return a < b; }
	};

	template<typename T> class TagRemap {
	public:
		typedef T key_t;
		typedef typename std::underlying_type<T>::type underlying_t;
		typedef std::map<std::string, key_t> ms2k_t;

		TagRemap(void);
		key_t find(const std::string& x) const;
		key_t find(const std::string& x);
		key_t highest(void) const;
		const std::string& find(key_t k) const;
		void compute_remap(std::vector<key_t>& table) const;
		void clear(void);
		const ms2k_t& alltags(void) const { return m_ms2k; }

	protected:
		ms2k_t m_ms2k;
		typedef std::map<key_t, std::string> mk2s_t;
		mk2s_t m_mk2s;
	};

	void clear(void);
	void clear_nofiles(void);
	template<typename EntryType> void save_object_data(EntryType& ent, const std::string& data);
	template<typename ObjType> void save_object(const ObjType& obj, layer_t layer);
	uint32_t copy(Object::type_t objtyp, int fd, uint64_t addr, uint64_t source, uint32_t len) const;
	template<layer_t layer> inline void sort_directory(uint64_t& addr, int outfd, bool dolog);
	static std::size_t entry_size(layer_t layer);

	BinFileHeader m_hdr;
	static constexpr bool compressionstatistics = false;
	TagRemap<Object::tagkey_t> m_tagkeys;
	TagRemap<Object::tagname_t> m_tagnames;
	std::vector<Object::tagkey_t> m_tagkeyremap;
	std::vector<Object::tagname_t> m_tagnameremap;
	std::string m_tmpdir;
	std::size_t m_comprsave;
	int m_datafd;
	int m_objfd[static_cast<unsigned int>(OSMStaticDB::layer_t::last) + 1];
	importflags_t m_flags;
};

template<typename ObjType>
void OSMStaticDB::Import::save_object(const ObjType& obj, layer_t layer)
{
	BinFileLineAreaEntry ent;
	ent.set_id(obj.get_id());
	ent.set_bbox(obj.get_bbox());
	std::ostringstream blob;
	{
		HibernateWriteStream ar(blob);
		const_cast<ObjType&>(obj).hibernate_noid(ar);
	}
	save_object_data(ent, blob.str());
	if (write(m_objfd[static_cast<unsigned int>(layer)], &ent, sizeof(ent)) != sizeof(ent))
		MapFile::throw_strerror(to_str(layer) + " table write error");
	m_hdr.set_objdirentries(layer, m_hdr.get_objdirentries(layer) + 1);	
}

template<>
void OSMStaticDB::Import::save_object<OSMStaticDB::ObjPoint>(const ObjPoint& obj, layer_t layer)
{
	typedef OSMStaticDB::ObjPoint ObjType;
	BinFilePointEntry ent;
	ent.set_id(obj.get_id());
	ent.set_location(obj.get_location());
	std::ostringstream blob;
	{
		HibernateWriteStream ar(blob);
		const_cast<ObjType&>(obj).Object::hibernate_noid(ar);
	}
	save_object_data(ent, blob.str());
	if (write(m_objfd[static_cast<unsigned int>(layer)], &ent, sizeof(ent)) != sizeof(ent))
		MapFile::throw_strerror(to_str(layer) + " table write error");
	m_hdr.set_objdirentries(layer, m_hdr.get_objdirentries(layer) + 1);	
}

class OSMStaticDB::Import::ShapeFile {
public:
	typedef boost::intrusive_ptr<ShapeFile> ptr_t;
	typedef boost::intrusive_ptr<const ShapeFile> const_ptr_t;

	static ptr_t create(const std::string& fname);
	~ShapeFile();
	void save(Import& imp, Object::type_t objtype, layer_t layer, OGRCoordinateTransformation *trans = nullptr);
	std::ostream& info(std::ostream& os, unsigned int maxfeatures = std::numeric_limits<unsigned int>::max());
	const std::string& get_name(void) const { return m_filename; }

	unsigned int breference(void) const { return ++m_refcount; }
	unsigned int bunreference(void) const { return --m_refcount; }
	unsigned int get_refcount(void) const { return m_refcount; }
	ptr_t get_ptr(void) { return ptr_t(this); }
	const_ptr_t get_ptr(void) const { return const_ptr_t(this); }
	friend inline void intrusive_ptr_add_ref(const ShapeFile* expr) { expr->breference(); }
	friend inline void intrusive_ptr_release(const ShapeFile* expr) { if (!expr->bunreference()) delete expr; }

protected:
	static PolygonSimple get_linestring(const OGRLineString *geom, OGRCoordinateTransformation *trans);
	static PolygonHole get_polygon(const OGRPolygon *geom, OGRCoordinateTransformation *trans);
	static Point get_point(const OGRPoint *geom, OGRCoordinateTransformation *trans);

private:
#if defined(HAVE_GDAL2)
	ShapeFile(const std::string& fname, GDALDataset *ds);
#else
	ShapeFile(const std::string& fname, OGRDataSource *ds);
#endif
	void open(void);
	void close(void);

	std::string m_filename;
#if defined(HAVE_GDAL2)
	GDALDataset *m_ds;
#else
	OGRDataSource *m_ds;
#endif
	mutable std::atomic<unsigned int> m_refcount;
};

#if defined(HAVE_GDAL2)
OSMStaticDB::Import::ShapeFile::ShapeFile(const std::string& fname, GDALDataset *ds)
#else
OSMStaticDB::Import::ShapeFile::ShapeFile(const std::string& fname, OGRDataSource *ds)
#endif
	: m_filename(fname), m_ds(ds), m_refcount(0)
{
	// save some memory for now
	close();
}

OSMStaticDB::Import::ShapeFile::~ShapeFile()
{
	close();
}

PolygonSimple OSMStaticDB::Import::ShapeFile::get_linestring(const OGRLineString *geom, OGRCoordinateTransformation *trans)
{
	PolygonSimple p;
	if (!geom)
		return p;
	int pt(geom->getNumPoints());
	if (pt <= 0)
		return p;
	p.reserve(pt);
	for (int i = 0; i < pt; i++) {
		OGRPoint opt;
		geom->getPoint(i, &opt);
		double lon(opt.getX());
		double lat(opt.getY());
		if (trans)
			trans->Transform(1, &lon, &lat);		
		Point ipt;
		ipt.set_lon_deg_dbl(lon);
		ipt.set_lat_deg_dbl(lat);
		p.push_back(ipt);
	}
	return p;
}

PolygonHole OSMStaticDB::Import::ShapeFile::get_polygon(const OGRPolygon *geom, OGRCoordinateTransformation *trans)
{
	if (!geom)
		return PolygonHole();
	PolygonHole p(get_linestring(geom->getExteriorRing(), trans));
	for (int i = 0; i < geom->getNumInteriorRings(); i++)
		p.add_interior(get_linestring(geom->getInteriorRing(i), trans));
	p.remove_redundant_polypoints();
	p.normalize();
	return p;
}

Point OSMStaticDB::Import::ShapeFile::get_point(const OGRPoint *geom, OGRCoordinateTransformation *trans)
{
	if (!geom)
		return Point();
	double lon(geom->getX());
	double lat(geom->getY());
	if (trans)
		trans->Transform(1, &lon, &lat);		
	Point pt;
	pt.set_lon_deg_dbl(lon);
	pt.set_lat_deg_dbl(lat);
	return pt;
}

void OSMStaticDB::Import::ShapeFile::open(void)
{
	if (m_ds)
		return;
#if defined(HAVE_GDAL2)
	m_ds = (GDALDataset *)GDALOpenEx(m_filename.c_str(), GDAL_OF_READONLY, 0, 0, 0);
#else
	OGRSFDriver *drv(nullptr);
	m_ds = OGRSFDriverRegistrar::Open(m_filename.c_str(), false, &drv);
#endif
	if (m_ds == nullptr)
		throw std::runtime_error("Cannot open " + m_filename);
}

void OSMStaticDB::Import::ShapeFile::close(void)
{
#if defined(HAVE_GDAL2)
	GDALClose(m_ds);
#else
	OGRDataSource::DestroyDataSource(m_ds);
#endif
	m_ds = nullptr;
}

OSMStaticDB::Import::ShapeFile::ptr_t OSMStaticDB::Import::ShapeFile::create(const std::string& fname)
{
	if (fname.empty())
		return ptr_t();
#if defined(HAVE_GDAL2)
	GDALDataset *ds((GDALDataset *)GDALOpenEx(fname.c_str(), GDAL_OF_READONLY, 0, 0, 0));
#else
	OGRSFDriver *drv(nullptr);
	OGRDataSource *ds = OGRSFDriverRegistrar::Open(fname.c_str(), false, &drv);
#endif
	if (ds == nullptr)
		return ptr_t();
	ptr_t t(new ShapeFile(fname, ds));
	return t;
}

std::ostream& OSMStaticDB::Import::ShapeFile::info(std::ostream& os, unsigned int maxfeatures)
{
	open();
#if !defined(HAVE_GDAL2)
	os << "Tile Name: " << m_ds->GetName() << std::endl;
#endif
	for (int layernr = 0; layernr < m_ds->GetLayerCount(); layernr++) {
		OGRLayer *layer(m_ds->GetLayer(layernr));
		OGRFeatureDefn *layerdef(layer->GetLayerDefn());
		os << "  Layer Name: " << layerdef->GetName() << std::endl;
		for (int fieldnr = 0; fieldnr < layerdef->GetFieldCount(); fieldnr++) {
			OGRFieldDefn *fielddef(layerdef->GetFieldDefn(fieldnr));
			os << "    Field Name: " << fielddef->GetNameRef()
			   << " type " << OGRFieldDefn::GetFieldTypeName(fielddef->GetType())  << std::endl;
		}
		layer->ResetReading();
     		unsigned int featurecount(0);
		while (OGRFeature *feature = layer->GetNextFeature()) {
			if (++featurecount > maxfeatures)
				break;
			os << "  Feature: " << feature->GetFID() << std::endl;
			for (int fieldnr = 0; fieldnr < layerdef->GetFieldCount(); fieldnr++) {
				OGRFieldDefn *fielddef(layerdef->GetFieldDefn(fieldnr));
				os << "    Field Name: " << fielddef->GetNameRef() << " Value ";
				switch (fielddef->GetType()) {
					case OFTInteger:
						os << feature->GetFieldAsInteger(fieldnr);
						break;

					case OFTReal:
						os << feature->GetFieldAsDouble(fieldnr);
						break;

					case OFTString:
					default:
						os << feature->GetFieldAsString(fieldnr);
						break;
				}
				os << std::endl;
			}
			OGRGeometry *geom(feature->GetGeometryRef());
			char *wkt;
			geom->exportToWkt(&wkt);
			os << "  Geom: " << wkt << std::endl;
			delete wkt;
		}
	}
	close();
	return os;
}

void OSMStaticDB::Import::ShapeFile::save(Import& imp, Object::type_t objtype, layer_t lay, OGRCoordinateTransformation *trans)
{
	open();
#if defined(HAVE_GDAL2)
	std::cerr << "Processing file " << m_filename << std::endl;
#else
	std::cerr << "Processing " << m_ds->GetName() << " (file " << m_filename << ')' << std::endl;
#endif
	for (int layernr = 0; layernr < m_ds->GetLayerCount(); layernr++) {
		OGRLayer *layer(m_ds->GetLayer(layernr));
		OGRFeatureDefn *layerdef(layer->GetLayerDefn());
		layer->ResetReading();
		while (OGRFeature *feature = layer->GetNextFeature()) {
			OGRGeometry *geom(feature->GetGeometryRef());
			if (!geom)
				continue;
			Point geompt;
			MultiLineString geomls;
			MultiPolygonHole geomph;
			switch (objtype) {
			case Object::type_t::point:
			{
				OGRPoint *geomp(dynamic_cast<OGRPoint *>(geom));
				if (!geomp) {
					std::cerr << "Geometry not of type POINT, but " << geom->getGeometryName() << std::endl;
					continue;
				}
				geompt = get_point(geomp, trans);
				if (geompt.is_invalid())
					continue;
				break;
			}

			case Object::type_t::line:
			{
				OGRLineString *geoml(dynamic_cast<OGRLineString *>(geom));
				if (geoml) {
					geomls.push_back(get_linestring(geoml, trans));
				} else {
					OGRMultiLineString *geomml(dynamic_cast<OGRMultiLineString *>(geom));
					if (geomml) {
						for (const auto& x : *geomml)
							geomls.push_back(get_linestring(x, trans));
					} else {					
						std::cerr << "Geometry not of type LINESTRING or MULTILINESTRING, but " << geom->getGeometryName() << std::endl;
						continue;
					}
				}
				if (geomls.empty())
					continue;
				break;
			}

			case Object::type_t::area:
			{
				OGRPolygon *geomp(dynamic_cast<OGRPolygon *>(geom));
				if (geomp) {
					geomph.push_back(get_polygon(geomp, trans));
				} else {
					OGRMultiPolygon *geommp(dynamic_cast<OGRMultiPolygon *>(geom));
					if (geommp) {
						for (const auto& x : *geommp)
							geomph.push_back(get_polygon(x, trans));
					} else {
						std::cerr << "Geometry not of type POLYGON or MULTIPOLYGON, but " << geom->getGeometryName() << std::endl;
						continue;
					}
				}
				if (geomph.empty())
					continue;
				break;
			}

			default:
				continue;
			}
			Object::tags_t tags;
			for (int fieldnr = 0; fieldnr < layerdef->GetFieldCount(); fieldnr++) {
				OGRFieldDefn *fielddef(layerdef->GetFieldDefn(fieldnr));
				switch (fielddef->GetType()) {
					case OFTInteger:
					{
						std::ostringstream oss;
						oss << feature->GetFieldAsInteger(fieldnr);
						tags.push_back(std::make_tuple(imp.m_tagkeys.find(fielddef->GetNameRef()), imp.m_tagnames.find(oss.str())));
						break;
					}

					case OFTReal:
					{
						std::ostringstream oss;
						oss << feature->GetFieldAsDouble(fieldnr);
						tags.push_back(std::make_tuple(imp.m_tagkeys.find(fielddef->GetNameRef()), imp.m_tagnames.find(oss.str())));
						break;
					}

					case OFTString:
					default:
						tags.push_back(std::make_tuple(imp.m_tagkeys.find(fielddef->GetNameRef()), imp.m_tagnames.find(feature->GetFieldAsString(fieldnr))));
						break;
				}
			}
			switch (objtype) {
			case Object::type_t::point:
				imp.save_object(OSMStaticDB::ObjPoint(feature->GetFID(), tags, geompt), lay);
				break;

			case Object::type_t::line:
				imp.save_object(OSMStaticDB::ObjLine(feature->GetFID(), tags, geomls), lay);
				break;

			case Object::type_t::area:
				imp.save_object(OSMStaticDB::ObjArea(feature->GetFID(), tags, geomph), lay);
				break;

			default:
				break;
			}
		}
	}
	close();
}

template <typename T>
OSMStaticDB::Import::TagRemap<T>::TagRemap(void)
{
}

template <typename T>
typename OSMStaticDB::Import::TagRemap<T>::key_t OSMStaticDB::Import::TagRemap<T>::find(const std::string& x) const
{
	typename ms2k_t::const_iterator i(m_ms2k.find(x));
	if (i != m_ms2k.end())
		return i->second;
	return key_t::invalid;
}

template <typename T>
typename OSMStaticDB::Import::TagRemap<T>::key_t OSMStaticDB::Import::TagRemap<T>::find(const std::string& x)
{
	{
		typename ms2k_t::const_iterator i(m_ms2k.find(x));
		if (i != m_ms2k.end())
			return i->second;
	}
	key_t k(static_cast<key_t>(0));
	if (!m_mk2s.empty())
		k = static_cast<key_t>(static_cast<underlying_t>(highest()) + 1);
	m_ms2k[x] = k;
	m_mk2s[k] = x;
	return k;
}

template <typename T>
typename OSMStaticDB::Import::TagRemap<T>::key_t OSMStaticDB::Import::TagRemap<T>::highest(void) const
{
	return m_mk2s.rbegin()->first;
}

template <typename T>
const std::string& OSMStaticDB::Import::TagRemap<T>::find(key_t k) const
{
	typename mk2s_t::const_iterator i(m_mk2s.find(k));
	if (i != m_mk2s.end())
		return i->second;
	static const std::string empty;
	return empty;
}

template <typename T>
void OSMStaticDB::Import::TagRemap<T>::compute_remap(std::vector<key_t>& table) const
{
	table.clear();
	if (m_ms2k.empty())
		return;
	table.resize(static_cast<underlying_t>(highest()) + 1, key_t::invalid);
	underlying_t count(0);
	for (const auto& k : m_ms2k)
		table[static_cast<underlying_t>(k.second)] = static_cast<key_t>(count++);
}

template <typename T>
void OSMStaticDB::Import::TagRemap<T>::clear(void)
{
	m_ms2k.clear();
	m_mk2s.clear();
}

constexpr bool OSMStaticDB::Import::compressionstatistics;

OSMStaticDB::Import::Import(const std::string& tmpdir, importflags_t flags)
	: m_tmpdir(tmpdir), m_comprsave(0), m_datafd(-1), m_flags(flags)
{
	for (unsigned int i = 0; i <= static_cast<unsigned int>(OSMStaticDB::layer_t::last); ++i)
		m_objfd[i] = -1;
	// create files
	for (unsigned int i = 0; i <= static_cast<unsigned int>(OSMStaticDB::layer_t::last); ++i) {
		std::ostringstream fn;
		fn << "dir" << i;
		std::string ffn(Glib::build_filename(m_tmpdir, fn.str()));
		m_objfd[i] = ::open(ffn.c_str(), O_CREAT | O_TRUNC | O_EXCL | O_CLOEXEC | O_NOATIME | O_RDWR, S_IRUSR | S_IWUSR);
		if (m_objfd[i] == -1)
			MapFile::throw_strerror(("Cannot create file " + ffn).c_str());
		if ((m_flags & importflags_t::keep) == importflags_t::none)
			unlink(ffn.c_str());
	}
	static const char * const fntable[1] = {
		"data"
	};
	for (unsigned int i = 0; i < 1; ++i) {
		std::string ffn(Glib::build_filename(m_tmpdir, fntable[i]));
		int fd = ::open(ffn.c_str(), O_CREAT | O_TRUNC | O_EXCL | O_CLOEXEC | O_NOATIME | O_RDWR, S_IRUSR | S_IWUSR);
		if (fd == -1)
			MapFile::throw_strerror(("Cannot create file " + ffn).c_str());
		switch (i) {
		case 0:
			m_datafd = fd;
			break;

		default:
			break;
		}
		if ((m_flags & importflags_t::keep) == importflags_t::none)
			unlink(ffn.c_str());
	}
}

OSMStaticDB::Import::~Import()
{
	clear();
	for (unsigned int i = 0; i <= static_cast<unsigned int>(OSMStaticDB::layer_t::last); ++i)
		if (m_objfd[i] != -1)
			::close(m_objfd[i]);
	if (m_datafd != -1)
		::close(m_datafd);
}

void OSMStaticDB::Import::clear_nofiles(void)
{
	m_hdr.set_signature();
	for (layer_t l(layer_t::first); l <= layer_t::last; l = static_cast<layer_t>(static_cast<unsigned int>(l) + 1)) {
		m_hdr.set_objdiroffs(l, 0);
		m_hdr.set_objdirentries(l, 0);
	}
	m_tagkeys.clear();
	m_tagnames.clear();
	m_tagkeyremap.clear();
	m_tagnameremap.clear();
	// statistics
	m_comprsave = 0;
}

void OSMStaticDB::Import::clear(void)
{
	clear_nofiles();
	// truncate files
	for (unsigned int i = 0; i <= static_cast<unsigned int>(OSMStaticDB::layer_t::last); ++i) {
		if (m_objfd[i] != -1) {
			lseek(m_objfd[i], 0, SEEK_SET);
			ftruncate(m_objfd[i], 0);
		}
	}
	if (m_datafd != -1) {
		lseek(m_datafd, 0, SEEK_SET);
		ftruncate(m_datafd, 0);
	}
}

template<typename EntryType>
void OSMStaticDB::Import::save_object_data(EntryType& ent, const std::string& data)
{
	ent.set_dataoffs(lseek(m_datafd, 0, SEEK_CUR));
	ent.set_datasize(data.size());
	if (ent.get_datasize() != data.size()) {
		std::ostringstream oss;
		oss << "Data size truncated, blob size " << data.size();
		throw std::runtime_error(oss.str());
	}
	if (write(m_datafd, data.c_str(), ent.get_datasize()) != ent.get_datasize())
		MapFile::throw_strerror("Data Write error");
	if (compressionstatistics && data.size() > 16) {
		z_stream c_stream;
		memset(&c_stream, 0, sizeof(c_stream));
		c_stream.zalloc = (alloc_func)0;
		c_stream.zfree = (free_func)0;
		c_stream.opaque = (voidpf)0;
		int err(deflateInit(&c_stream, Z_BEST_COMPRESSION));
		if (!err) {
			uint8_t dout[data.size() + 64];
			c_stream.next_out = (Bytef *)dout;
                        c_stream.avail_out = sizeof(dout);
                        c_stream.next_in = (Bytef *)data.c_str();
                        c_stream.avail_in = data.size();
                        err = deflate(&c_stream, Z_FINISH);
                        if (err == Z_STREAM_END &&
			    c_stream.avail_in == 0 &&
			    c_stream.total_in == data.size() &&
			    c_stream.total_out < data.size())
				m_comprsave += data.size() - c_stream.total_out;
		}
	}
}

uint32_t OSMStaticDB::Import::copy(Object::type_t objtyp, int fd, uint64_t addr, uint64_t source, uint32_t len) const
{
	Object::ptr_t p(Object::create(objtyp));
	if (!p)
		return 0;
	{
		uint8_t buf[len];
		if (pread(m_datafd, buf, len, source) != len)
			MapFile::throw_strerror(to_str(objtyp) + " object data read error");
		HibernateReadBuffer ar(buf, buf + len);
		p->loaddb(ar);
	}
	{
		Object::tags_t& tags(p->get_tags());
		for (Object::tags_t::iterator i(tags.begin()), e(tags.end()); i != e; ) {
			auto tk(static_cast<std::underlying_type<Object::tagkey_t>::type>(std::get<Object::tagkey_t>(*i)));
			if (tk >= m_tagkeyremap.size()) {
				std::cerr << "tag key " << tk << " outside of remap table " << m_tagkeyremap.size() << std::endl;
				i = tags.erase(i);
				e = tags.end();
				continue;
			}
			auto tn(static_cast<std::underlying_type<Object::tagname_t>::type>(std::get<Object::tagname_t>(*i)));
			if (tn >= m_tagnameremap.size()) {
				std::cerr << "tag name " << tn << " outside of remap table " << m_tagnameremap.size() << std::endl;
				i = tags.erase(i);
				e = tags.end();
				continue;
			}
			Object::tagkey_t tkm(m_tagkeyremap[tk]);
			if (tkm == Object::tagkey_t::invalid) {
				std::cerr << "tag key " << tk << " maps to invalid" << std::endl;
				i = tags.erase(i);
				e = tags.end();
				continue;
			}
			Object::tagname_t tnm(m_tagnameremap[tn]);
			if (tnm == Object::tagname_t::invalid) {
				std::cerr << "tag name " << tn << " maps to invalid" << std::endl;
				i = tags.erase(i);
				e = tags.end();
				continue;
			}
			std::get<Object::tagkey_t>(*i) = tkm;
			std::get<Object::tagname_t>(*i) = tnm;
			++i;
		}
		// sort
		Object::sort_tags(tags);
		// check for duplicate
		Object::tagkey_t lastkey(Object::tagkey_t::invalid);
		for (Object::tags_t::iterator i(tags.begin()), e(tags.end()); i != e; ) {
			Object::tagkey_t key(std::get<Object::tagkey_t>(*i));
			if (key != lastkey) {
				lastkey = key;
				++i;
				continue;
			}
			std::cerr << "duplicate key " << static_cast<std::underlying_type<Object::tagkey_t>::type>(key) << std::endl;
			i = tags.erase(i);
			e = tags.end();
		}
		if (false)
			std::cerr << "Object has " << tags.size() << " tags" << std::endl;
	}
	std::ostringstream blob;
	{
		HibernateWriteStream ar(blob);
		p->savedb(ar);
	}
	p.reset();
	if (pwrite(fd, blob.str().c_str(), blob.str().size(), addr) != static_cast<ssize_t>(blob.str().size()))
		MapFile::throw_strerror(to_str(objtyp) + " object data write error");
	return blob.str().size();
}

template<OSMStaticDB::layer_t layer> inline void OSMStaticDB::Import::sort_directory(uint64_t& addr, int outfd, bool dolog)
{
	static constexpr uint64_t copychunksize = 1ULL << 30;
	static const Object::type_t objtype = layer_data_type<layer>::objtype;
	typedef typename layer_types<layer>::BinFileEntry BinFileEntry;
	MapFile mmap(m_objfd[static_cast<unsigned int>(layer)], true, "point directory");
	BinFileEntry *dirb(mmap.begin_as<BinFileEntry>());
	BinFileEntry *dire(mmap.end_as<BinFileEntry>());
	// sorting
	if (dolog)
		std::cerr << "Sorting " << layer << "..." << std::endl;
	std::sort(dirb, dire, BinFileIDSorter());
	// copy object data and fix up addresses
	if (dolog)
		std::cerr << "Copying " << layer << " objects..." << std::endl;
	for (BinFileEntry *x(dirb); x != dire; ++x) {
		uint32_t sz(copy(objtype, outfd, addr, x->get_dataoffs(), x->get_datasize()));
		x->set_dataoffs(addr - m_hdr.get_objdiroffs(layer) - (x - dirb) * sizeof(BinFileEntry));
		x->set_datasize(sz);
		addr += sz;
	}
	// copy object directories
	{
		uint64_t sz(m_hdr.get_objdirentries(layer) * sizeof(BinFileEntry));
		uint64_t addr(m_hdr.get_objdiroffs(layer));
		const uint8_t *sptr(mmap.begin());
		std::cerr << "Writing " << layer << " table";
		while (sz > 0) {
			ssize_t sz1(std::min(sz, copychunksize));
			ssize_t ret(pwrite(outfd, sptr, sz1, addr));
			if (ret != sz1)
				MapFile::throw_strerror(to_str(layer) + " table write error");
			addr += sz1;
			sptr += sz1;
			sz -= sz1;
			std::cerr << '.';
		}
		std::cerr << std::endl;
	}
	// save memory
	mmap.close();
	if ((m_flags & importflags_t::keep) == importflags_t::none)
		ftruncate(m_objfd[static_cast<unsigned int>(layer)], 0);
}

std::size_t OSMStaticDB::Import::entry_size(layer_t layer)
{
	switch (layer) {
	case layer_t::boundarylinesland:
		return sizeof(layer_types<layer_t::boundarylinesland>::BinFileEntry);

	case layer_t::icesheetpolygons:
		return sizeof(layer_types<layer_t::icesheetpolygons>::BinFileEntry);

	case layer_t::icesheetoutlines:
		return sizeof(layer_types<layer_t::icesheetoutlines>::BinFileEntry);

	case layer_t::waterpolygons:
		return sizeof(layer_types<layer_t::waterpolygons>::BinFileEntry);

	case layer_t::simplifiedwaterpolygons:
		return sizeof(layer_types<layer_t::simplifiedwaterpolygons>::BinFileEntry);

	default:
		return 0;
	}
}

void OSMStaticDB::Import::osmimport(int outfd, const ImportSourceFiles& infnames)
{
	static constexpr bool dolog = true;
	clear();
	if (lseek(outfd, 0, SEEK_SET) == static_cast<off_t>(-1))
		MapFile::throw_strerror("seeking output file");
	if (ftruncate(outfd, 0))
		MapFile::throw_strerror("truncating output file");
	// read objects
	{
		OGRSpatialReference srefmercator;
		OGRSpatialReference srefwgs84;
		if (srefmercator.importFromEPSG(3857) != OGRERR_NONE)
			throw std::runtime_error("Cannot set projection EPSG::3857 (Mercator)");
		if (srefwgs84.importFromEPSG(4326) != OGRERR_NONE)
			throw std::runtime_error("Cannot set projection EPSG::4326 (WGS84)");
		OGRCoordinateTransformation *transmercator(OGRCreateCoordinateTransformation(&srefmercator, &srefwgs84));
		for (layer_t l(layer_t::first); l <= layer_t::last; l = static_cast<layer_t>(static_cast<unsigned int>(l) + 1)) {
			ShapeFile::ptr_t shp(ShapeFile::create(infnames.get_file(l)));
			if (!shp) {
				std::cerr << "Cannot open file \"" << infnames.get_file(l) << "\" for layer " << l << std::endl;
				continue;
			}
			switch (l) {
			default:
			case layer_t::boundarylinesland:
				shp->save(*this, get_objtype(l), l, nullptr);
				break;

			case layer_t::icesheetpolygons:
			case layer_t::icesheetoutlines:
			case layer_t::waterpolygons:
			case layer_t::simplifiedwaterpolygons:
				shp->save(*this, get_objtype(l), l, transmercator);
				break;
			}
		}
		OGRFree(transmercator);
	}
	// put together the output file
	uint64_t addr(sizeof(m_hdr));
	if (dolog)
		std::cerr << "Writing tag keys..." << std::endl;
	m_hdr.set_tagkeyentries(m_tagkeys.alltags().size());
	m_hdr.set_tagkeyoffs(addr);
	{
		uint64_t taddr(addr);
		addr += m_hdr.get_tagkeyentries() * sizeof(BinFileTagKeyEntry);
		for (const auto& n : m_tagkeys.alltags()) {
			BinFileTagKeyEntry te;
			te.set_stroffs(addr - taddr);
			ssize_t sz(n.first.size() + 1);
			if (pwrite(outfd, n.first.c_str(), sz, addr) != sz)
				MapFile::throw_strerror("Tag Key Write error");
			addr += sz;
			if (pwrite(outfd, &te, sizeof(te), taddr) != sizeof(te))
				MapFile::throw_strerror("Tag Key Entry Write error");
			taddr += sizeof(te);
		}
		addr += aligninc;
		addr &= alignmask;
		if (dolog)
			std::cerr << "Tag key table size " << (addr - m_hdr.get_tagkeyoffs())
				  << " bytes" << std::endl;
	}
	if (dolog)
		std::cerr << "Writing tag names..." << std::endl;
	m_hdr.set_tagnameentries(m_tagnames.alltags().size());
	m_hdr.set_tagnameoffs(addr);
	{
		uint64_t taddr(addr);
		addr += m_hdr.get_tagnameentries() * sizeof(BinFileTagNameEntry);
		for (const auto& n : m_tagnames.alltags()) {
			BinFileTagNameEntry te;
			te.set_stroffs(addr - taddr);
			ssize_t sz(n.first.size() + 1);
			if (pwrite(outfd, n.first.c_str(), sz, addr) != sz)
				MapFile::throw_strerror("Tag Name Write error");
			addr += sz;
			if (pwrite(outfd, &te, sizeof(te), taddr) != sizeof(te))
				MapFile::throw_strerror("Tag Name Entry Write error");
			taddr += sizeof(te);
		}
		addr += aligninc;
		addr &= alignmask;
		if (dolog)
			std::cerr << "Tag name table size " << (addr - m_hdr.get_tagnameoffs())
				  << " bytes" << std::endl;
	}
	// save more memory
	m_tagkeys.compute_remap(m_tagkeyremap);
	m_tagnames.compute_remap(m_tagnameremap);
	m_tagkeys.clear();
	m_tagnames.clear();
	// update header
	for (layer_t l(layer_t::first); l <= layer_t::last; l = static_cast<layer_t>(static_cast<unsigned int>(l) + 1)) {
		std::size_t entsz(entry_size(l));
		m_hdr.set_objdiroffs(l, addr);
		addr += m_hdr.get_objdirentries(l) * entsz;
		addr += aligninc;
		addr &= alignmask;
		m_hdr.set_rtreeoffs(l, 0);
	}
	// write header
	if (pwrite(outfd, &m_hdr, sizeof(m_hdr), 0) != sizeof(m_hdr))
		MapFile::throw_strerror("Header write error");
	// sort directories and copy object data
	sort_directory<layer_t::boundarylinesland>(addr, outfd, dolog);
	sort_directory<layer_t::icesheetpolygons>(addr, outfd, dolog);
	sort_directory<layer_t::icesheetoutlines>(addr, outfd, dolog);
	sort_directory<layer_t::waterpolygons>(addr, outfd, dolog);
	sort_directory<layer_t::simplifiedwaterpolygons>(addr, outfd, dolog);
	// delete temporaries
	if ((m_flags & importflags_t::keep) == importflags_t::none)
		clear();
	else
		clear_nofiles();
	if ((m_flags & importflags_t::noindex) == importflags_t::none)
		reindex(m_tmpdir, outfd);
	// compression statistics
	if (compressionstatistics)
		std::cerr << "Compression Statistics: " << m_comprsave
			  << " bytes could be saved with deflate(best)" << std::endl;
}

void OSMStaticDB::Import::osmimport(const std::string& outfname, const ImportSourceFiles& infnames)
{
	int outfd(::open(outfname.c_str(), O_CREAT | O_TRUNC | O_CLOEXEC | O_NOATIME | O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP));
	if (outfd == -1)
		MapFile::throw_strerror(("Cannot create file " + outfname).c_str());
	try {
		osmimport(outfd, infnames);
	} catch (...) {
		::close(outfd);
		throw;
	}
	::close(outfd);
}

void OSMStaticDB::import(const std::string& tmpdir, const std::string& outfname, const ImportSourceFiles& infnames, importflags_t flags)
{
	Import imp(tmpdir, flags);
	imp.osmimport(outfname, infnames);
}

void OSMStaticDB::import(const std::string& tmpdir, int outfd, const ImportSourceFiles& infnames, importflags_t flags)
{
	Import imp(tmpdir, flags);
	imp.osmimport(outfd, infnames);
}

std::ostream& OSMStaticDB::shapeinfo(std::ostream& os, const std::string& name, unsigned int maxfeatures)
{
	Import::ShapeFile::ptr_t p(Import::ShapeFile::create(name));
	if (!p)
		return os << "Cannot open file \"" << name << '"' << std::endl;
	return p->info(os, maxfeatures);
}

#else /* HAVE_GDAL */

void OSMStaticDB::import(const OsmStyle& style, const std::string& tmpdir, const std::string& outfname, const ImportSourceFiles& infnames, importflags_t flags)
{
	throw std::runtime_error("OSM import must be compiled with osmium");
}

void OSMStaticDB::import(const OsmStyle& style, const std::string& tmpdir, int outfd, const ImportSourceFiles& infnames, importflags_t flags)
{
	throw std::runtime_error("OSM import must be compiled with osmium");
}

std::ostream& OSMStaticDB::shapeinfo(std::ostream& os, const std::string& name, unsigned int maxfeatures)
{
	return os << "Not compiled with GDAL" << std::endl;
}

#endif /* HAVE_GDAL */
