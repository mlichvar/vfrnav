/*****************************************************************************/

/*
 *      osmdb.cc  --  OpenStreetMap proprietary database.
 *
 *      Copyright (C) 2020  Thomas Sailer (t.sailer@alumni.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/*****************************************************************************/

#include <iomanip>
#include <fstream>

#include "osmdb.h"
#include "hibernate.h"

template<int sz> const unsigned int OSMDB::BinFileEntry<sz>::size;

template<int sz> OSMDB::BinFileEntry<sz>::BinFileEntry(void)
{
	memset(m_data, 0, sz);
}

template<int sz> uint8_t OSMDB::BinFileEntry<sz>::readu8(unsigned int idx) const
{
	if (idx >= size)
		return 0;
	return m_data[idx];
}

template<int sz> uint16_t OSMDB::BinFileEntry<sz>::readu16(unsigned int idx) const
{
	if (idx + 1U >= size)
		return 0;
	return m_data[idx] | (m_data[idx + 1U] << 8);
}

template<int sz> uint32_t OSMDB::BinFileEntry<sz>::readu24(unsigned int idx) const
{
	if (idx + 2U >= size)
		return 0;
	return m_data[idx] | (m_data[idx + 1U] << 8) | (m_data[idx + 2U] << 16);
}

template<int sz> uint32_t OSMDB::BinFileEntry<sz>::readu32(unsigned int idx) const
{
	if (idx + 3U >= size)
		return 0;
	return m_data[idx] | (m_data[idx + 1U] << 8) | (m_data[idx + 2U] << 16) | (m_data[idx + 3U] << 24);
}

template<int sz> uint64_t OSMDB::BinFileEntry<sz>::readu40(unsigned int idx) const
{
	if (idx + 4U >= size)
		return 0;
	uint32_t r0(m_data[idx] | (m_data[idx + 1U] << 8) | (m_data[idx + 2U] << 16) | (m_data[idx + 3U] << 24));
	uint32_t r1(m_data[idx + 4U]);
	uint64_t r(r1);
	r <<= 32;
	r |= r0;
	return r;
}

template<int sz> uint64_t OSMDB::BinFileEntry<sz>::readu48(unsigned int idx) const
{
	if (idx + 5U >= size)
		return 0;
	uint32_t r0(m_data[idx] | (m_data[idx + 1U] << 8) | (m_data[idx + 2U] << 16) | (m_data[idx + 3U] << 24));
	uint32_t r1(m_data[idx + 4U] | (m_data[idx + 5U] << 8));
	uint64_t r(r1);
	r <<= 32;
	r |= r0;
	return r;
}

template<int sz> uint64_t OSMDB::BinFileEntry<sz>::readu64(unsigned int idx) const
{
	if (idx + 7U >= size)
		return 0;
	uint32_t r0(m_data[idx] | (m_data[idx + 1U] << 8) | (m_data[idx + 2U] << 16) | (m_data[idx + 3U] << 24));
	uint32_t r1(m_data[idx + 4U] | (m_data[idx + 5U] << 8) | (m_data[idx + 6U] << 16) | (m_data[idx + 7U] << 24));
	uint64_t r(r1);
	r <<= 32;
	r |= r0;
	return r;
}

template<int sz> void OSMDB::BinFileEntry<sz>::writeu8(unsigned int idx, uint8_t v)
{
	if (idx >= size)
		return;
	m_data[idx] = v;
}

template<int sz> void OSMDB::BinFileEntry<sz>::writeu16(unsigned int idx, uint16_t v)
{
	if (idx + 1U >= size)
		return;
	m_data[idx] = v;
	m_data[idx + 1U] = v >> 8;
}

template<int sz> void OSMDB::BinFileEntry<sz>::writeu24(unsigned int idx, uint32_t v)
{
	if (idx + 2U >= size)
		return;
	m_data[idx] = v;
	m_data[idx + 1U] = v >> 8;
	m_data[idx + 2U] = v >> 16;
}

template<int sz> void OSMDB::BinFileEntry<sz>::writeu32(unsigned int idx, uint32_t v)
{
	if (idx + 3U >= size)
		return;
	m_data[idx] = v;
	m_data[idx + 1U] = v >> 8;
	m_data[idx + 2U] = v >> 16;
	m_data[idx + 3U] = v >> 24;
}

template<int sz> void OSMDB::BinFileEntry<sz>::writeu40(unsigned int idx, uint64_t v)
{
	if (idx + 4U >= size)
		return;
	m_data[idx] = v;
	m_data[idx + 1U] = v >> 8;
	m_data[idx + 2U] = v >> 16;
	m_data[idx + 3U] = v >> 24;
	m_data[idx + 4U] = v >> 32;
}

template<int sz> void OSMDB::BinFileEntry<sz>::writeu48(unsigned int idx, uint64_t v)
{
	if (idx + 5U >= size)
		return;
	m_data[idx] = v;
	m_data[idx + 1U] = v >> 8;
	m_data[idx + 2U] = v >> 16;
	m_data[idx + 3U] = v >> 24;
	m_data[idx + 4U] = v >> 32;
	m_data[idx + 5U] = v >> 40;
}

template<int sz> void OSMDB::BinFileEntry<sz>::writeu64(unsigned int idx, uint64_t v)
{
	if (idx + 7U >= size)
		return;
	m_data[idx] = v;
	m_data[idx + 1U] = v >> 8;
	m_data[idx + 2U] = v >> 16;
	m_data[idx + 3U] = v >> 24;
	m_data[idx + 4U] = v >> 32;
	m_data[idx + 5U] = v >> 40;
	m_data[idx + 6U] = v >> 48;
	m_data[idx + 7U] = v >> 56;
}

const char OSMDB::BinFileHeader::signature[] = "vfrnav OSM Database V1\n";

bool OSMDB::BinFileHeader::check_signature(void) const
{
	return !memcmp(m_data, signature, sizeof(signature));
}

void OSMDB::BinFileHeader::set_signature(void)
{
	memset(m_data, 0, 32);
	memset(m_data + 104, 0, 24);
	memcpy(m_data, signature, sizeof(signature));
}

uint64_t OSMDB::BinFileHeader::get_objdiroffs(Object::type_t typ) const
{
	if (typ > Object::type_t::area)
		return 0;
	return readu64(32 + 8 * static_cast<unsigned int>(typ));
}

void OSMDB::BinFileHeader::set_objdiroffs(Object::type_t typ, uint64_t offs)
{
	if (typ > Object::type_t::area)
		return;
	writeu64(32 + 8 * static_cast<unsigned int>(typ), offs);
}

uint32_t OSMDB::BinFileHeader::get_objdirentries(Object::type_t typ) const
{
	if (typ > Object::type_t::area)
		return 0;
	return readu32(96 + 4 * static_cast<unsigned int>(typ));
}

void OSMDB::BinFileHeader::set_objdirentries(Object::type_t typ, uint32_t n)
{
	if (typ > Object::type_t::area)
		return;
	writeu32(96 + 4 * static_cast<unsigned int>(typ), n);
}

uint64_t OSMDB::BinFileHeader::get_rtreeoffs(Object::type_t typ) const
{
	if (typ > Object::type_t::area)
		return 0;
	return readu64(64 + 8 * static_cast<unsigned int>(typ));
}

void OSMDB::BinFileHeader::set_rtreeoffs(Object::type_t typ, uint64_t offs)
{
	if (typ > Object::type_t::area)
		return;
	writeu64(64 + 8 * static_cast<unsigned int>(typ), offs);
}

uint64_t OSMDB::BinFileHeader::get_tagkeyoffs(void) const
{
	return readu64(112);
}

void OSMDB::BinFileHeader::set_tagkeyoffs(uint64_t offs)
{
	writeu64(112, offs);
}

uint32_t OSMDB::BinFileHeader::get_tagkeyentries(void) const
{
	return readu32(128);
}

void OSMDB::BinFileHeader::set_tagkeyentries(uint32_t n)
{
	writeu32(128, n);
}

uint64_t OSMDB::BinFileHeader::get_tagnameoffs(void) const
{
	return readu64(120);
}

void OSMDB::BinFileHeader::set_tagnameoffs(uint64_t offs)
{
	writeu64(120, offs);
}

uint32_t OSMDB::BinFileHeader::get_tagnameentries(void) const
{
	return readu32(132);
}

void OSMDB::BinFileHeader::set_tagnameentries(uint32_t n)
{
	writeu32(132, n);
}

template<OSMDB::Object::type_t typ>
typename OSMDB::obj_types<typ>::IndexRangeType OSMDB::BinFileHeader::get_objdir_range(void) const
{
	typedef typename obj_types<typ>::IndexType IndexType;
	uint64_t offs(get_objdiroffs(typ));
	if (!offs)
		return std::make_pair(nullptr, nullptr);
	const IndexType *p(reinterpret_cast<const IndexType *>(reinterpret_cast<const uint8_t *>(this) + offs));
	return std::make_pair(p, p + get_objdirentries(typ));
}

const OSMDB::BinFileRtreeInternalNode *OSMDB::BinFileHeader::get_rtree_root(Object::type_t typ) const
{
	uint64_t offs(get_rtreeoffs(typ));
	if (!offs)
		return nullptr;
	return reinterpret_cast<const BinFileRtreeInternalNode *>(reinterpret_cast<const uint8_t *>(this) + offs);
}

const OSMDB::BinFileTagKeyEntry *OSMDB::BinFileHeader::get_tagkey_begin(void) const
{
	uint64_t offs(get_tagkeyoffs());
	if (!offs)
		return nullptr;
	return reinterpret_cast<const BinFileTagKeyEntry *>(reinterpret_cast<const uint8_t *>(this) + offs);
}

const OSMDB::BinFileTagKeyEntry *OSMDB::BinFileHeader::get_tagkey_end(void) const
{
	const BinFileTagKeyEntry *p(get_tagkey_begin());
	if (p)
		p += get_tagkeyentries();
	return p;
	
}

const OSMDB::BinFileTagNameEntry *OSMDB::BinFileHeader::get_tagname_begin(void) const
{
	uint64_t offs(get_tagnameoffs());
	if (!offs)
		return nullptr;
	return reinterpret_cast<const BinFileTagNameEntry *>(reinterpret_cast<const uint8_t *>(this) + offs);
}

const OSMDB::BinFileTagNameEntry *OSMDB::BinFileHeader::get_tagname_end(void) const
{
	const BinFileTagNameEntry *p(get_tagname_begin());
	if (p)
		p += get_tagnameentries();
	return p;
	
}

OSMDB::Object::id_t OSMDB::BinFilePointEntry::get_id(void) const
{
	return reads64(0);
}

void OSMDB::BinFilePointEntry::set_id(Object::id_t id)
{
	writes64(0, id);
}

Point OSMDB::BinFilePointEntry::get_location(void) const
{
	return Point(reads32(8), reads32(12));
}

void OSMDB::BinFilePointEntry::set_location(const Point& location)
{
	writes32(8, location.get_lon());
	writes32(12, location.get_lat());
}

uint64_t OSMDB::BinFilePointEntry::get_dataoffs(void) const
{
	return readu40(16);
}

void OSMDB::BinFilePointEntry::set_dataoffs(uint64_t offs)
{
	writeu40(16, offs);
}

const uint8_t *OSMDB::BinFilePointEntry::get_dataptr(void) const
{
	return reinterpret_cast<const uint8_t *>(this) + get_dataoffs();
}

uint32_t OSMDB::BinFilePointEntry::get_datasize(void) const
{
	return readu24(21);
}

void OSMDB::BinFilePointEntry::set_datasize(uint32_t sz)
{
	writeu24(21, sz);
}

OSMDB::Object::id_t OSMDB::BinFileLineAreaEntry::get_id(void) const
{
	return reads64(0);
}

void OSMDB::BinFileLineAreaEntry::set_id(Object::id_t id)
{
	writes64(0, id);
}

Rect OSMDB::BinFileLineAreaEntry::get_bbox(void) const
{
	return Rect(Point(reads32(8), reads32(12)), Point(reads32(16), reads32(20)));
}

void OSMDB::BinFileLineAreaEntry::set_bbox(const Rect& bbox)
{
	writes32(8, bbox.get_southwest().get_lon());
	writes32(12, bbox.get_southwest().get_lat());
	writes32(16, bbox.get_northeast().get_lon());
	writes32(20, bbox.get_northeast().get_lat());
}

uint64_t OSMDB::BinFileLineAreaEntry::get_dataoffs(void) const
{
	return readu40(24);
}

void OSMDB::BinFileLineAreaEntry::set_dataoffs(uint64_t offs)
{
	writeu40(24, offs);
}

const uint8_t *OSMDB::BinFileLineAreaEntry::get_dataptr(void) const
{
	return reinterpret_cast<const uint8_t *>(this) + get_dataoffs();
}

uint32_t OSMDB::BinFileLineAreaEntry::get_datasize(void) const
{
	return readu24(29);
}

void OSMDB::BinFileLineAreaEntry::set_datasize(uint32_t sz)
{
	writeu24(29, sz);
}

uint64_t OSMDB::BinFileTagKeyEntry::get_stroffs(void) const
{
	return readu32(0);
}

void OSMDB::BinFileTagKeyEntry::set_stroffs(uint64_t offs)
{
	writeu32(0, offs);
}

const char *OSMDB::BinFileTagKeyEntry::get_strptr(void) const
{
	return reinterpret_cast<const char *>(this) + get_stroffs();
}

bool OSMDB::BinFileTagKeySorter::operator()(const char *a, const char *b) const
{
	if (!a)
		return !!b;
	if (!b)
		return false;
	return strcmp(a, b) < 0;
}

uint64_t OSMDB::BinFileTagNameEntry::get_stroffs(void) const
{
	return readu32(0);
}

void OSMDB::BinFileTagNameEntry::set_stroffs(uint64_t offs)
{
	writeu32(0, offs);
}

const char *OSMDB::BinFileTagNameEntry::get_strptr(void) const
{
	return reinterpret_cast<const char *>(this) + get_stroffs();
}

bool OSMDB::BinFileTagNameSorter::operator()(const char *a, const char *b) const
{
	if (!a)
		return !!b;
	if (!b)
		return false;
	return strcmp(a, b) < 0;
}

uint16_t OSMDB::BinFileRtreeLeafNode::get_nodes(void) const
{
	return readu16(0);
}

void OSMDB::BinFileRtreeLeafNode::set_nodes(uint16_t n)
{
	writeu16(0, std::min(n, static_cast<uint16_t>(rtreemaxelperpage)));
}

std::size_t OSMDB::BinFileRtreeLeafNode::get_size(void) const
{
	return get_nodes() * 4 + 2;
}

OSMDB::BinFileRtreeLeafNode::index_t OSMDB::BinFileRtreeLeafNode::get_index(uint16_t n) const
{
	if (n >= static_cast<uint16_t>(rtreemaxelperpage))
		return 0;
	return readu32(n * 4 + 2);
}

void OSMDB::BinFileRtreeLeafNode::set_index(uint16_t n, index_t index)
{
	if (n >= static_cast<uint16_t>(rtreemaxelperpage))
		return;
	writeu32(n * 4 + 2, index);
}

uint16_t OSMDB::BinFileRtreeInternalNode::get_nodes(void) const
{
	return readu16(0) & 0x7fff;
}

void OSMDB::BinFileRtreeInternalNode::set_nodes(uint16_t n)
{
	uint16_t x(readu16(0));
	x ^= (x ^ std::min(n, static_cast<uint16_t>(rtreemaxelperpage))) & 0x7fff;
	writeu16(0, x);
}

bool OSMDB::BinFileRtreeInternalNode::is_pointstoleaf(void) const
{
	return !!(readu16(0) & 0x8000);
}

void OSMDB::BinFileRtreeInternalNode::set_pointstoleaf(bool l)
{
	uint16_t x(readu16(0));
	x ^= (x ^ -!!l) & 0x8000;
	writeu16(0, x);
}

std::size_t OSMDB::BinFileRtreeInternalNode::get_size(void) const
{
	return get_nodes() * 24 + 2;
}

int64_t OSMDB::BinFileRtreeInternalNode::get_addroffs(uint16_t n) const
{
	if (n >= static_cast<uint16_t>(rtreemaxelperpage))
		return 0;
	return reads64(n * 24 + 2);
}

void OSMDB::BinFileRtreeInternalNode::set_addroffs(uint16_t n, int64_t addr)
{
	if (n >= static_cast<uint16_t>(rtreemaxelperpage))
		return;
	writes64(n * 24 + 2, addr);
}

const OSMDB::BinFileRtreeLeafNode *OSMDB::BinFileRtreeInternalNode::get_leafnode(uint16_t n) const
{
	return reinterpret_cast<const BinFileRtreeLeafNode *>(reinterpret_cast<const uint8_t *>(this) + get_addroffs(n));
}

const OSMDB::BinFileRtreeInternalNode *OSMDB::BinFileRtreeInternalNode::get_internalnode(uint16_t n) const
{
	return reinterpret_cast<const BinFileRtreeInternalNode *>(reinterpret_cast<const uint8_t *>(this) + get_addroffs(n));
}

Rect OSMDB::BinFileRtreeInternalNode::get_bbox(uint16_t n) const
{
	if (n >= static_cast<uint16_t>(rtreemaxelperpage))
		return Rect::invalid;
	return Rect(Point(reads32(n * 24 + 10), reads32(n * 24 + 14)), Point(reads32(n * 24 + 18), reads32(n * 24 + 22)));
}

void OSMDB::BinFileRtreeInternalNode::set_bbox(uint16_t n, const Rect& bbox)
{
	if (n >= static_cast<uint16_t>(rtreemaxelperpage))
		return;
	writes32(n * 24 + 10, bbox.get_southwest().get_lon());
	writes32(n * 24 + 14, bbox.get_southwest().get_lat());
	writes32(n * 24 + 18, bbox.get_northeast().get_lon());
	writes32(n * 24 + 22, bbox.get_northeast().get_lat());
}

const std::string& to_str(OSMDB::Object::type_t t)
{
	switch (t) {
	case OSMDB::Object::type_t::point:
		return OSMDB::ObjPoint::object_name;

	case OSMDB::Object::type_t::line:
		return OSMDB::ObjLine::object_name;

	case OSMDB::Object::type_t::road:
		return OSMDB::ObjRoad::object_name;

	case OSMDB::Object::type_t::area:
		return OSMDB::ObjArea::object_name;

	default:
	{
		static const std::string r("?");
		return r;
	}
	}
}

OSMDB::Object::Object(id_t id, const tags_t& tags, int32_t zorder)
	: m_tags(tags), m_id(id), m_zorder(zorder), m_refcount(0)
{
}

OSMDB::Object::~Object()
{
}

OSMDB::Object::tagname_t OSMDB::Object::find_tag(tagkey_t key) const
{
	if (key == tagkey_t::invalid)
		return tagname_t::invalid;
	tags_t::const_iterator i(std::lower_bound(m_tags.begin(), m_tags.end(), key, TagSorter()));
	if (i == m_tags.end() || std::get<tagkey_t>(*i) != key)
		return tagname_t::invalid;
	return std::get<tagname_t>(*i);
}

int OSMDB::Object::compare_tags(const tags_t& a, const tags_t& b)
{
	tags_t::size_type i(b.size()), n(a.size());
	if (n < i)
		return -1;
	if (i < n)
		return 1;
	for (i = 0; i < n; ++i) {
		const tag_t& aa(a[i]), bb(b[i]);
		if (std::get<tagkey_t>(aa) < std::get<tagkey_t>(bb))
			return -1;
		if (std::get<tagkey_t>(bb) < std::get<tagkey_t>(aa))
			return 1;
		if (std::get<tagname_t>(aa) < std::get<tagname_t>(bb))
			return -1;
		if (std::get<tagname_t>(bb) < std::get<tagname_t>(aa))
			return 1;
	}
	return 0;
}

int OSMDB::Object::compare(const Object& x) const
{
	if (get_type() < x.get_type())
		return -1;
	if (x.get_type() < get_type())
		return 1;
	if (get_id() < x.get_id())
		return -1;
	if (x.get_id() < get_id())
		return 1;
	if (get_zorder() < x.get_zorder())
		return -1;
	if (x.get_zorder() < get_zorder())
		return 1;
	return compare_tags(get_tags(), x.get_tags());
}

OSMDB::Object::ptr_t OSMDB::Object::create(type_t t)
{
	switch (t) {
	case type_t::point:
		return ptr_t(new ObjPoint());

	case type_t::line:
		return ptr_t(new ObjLine());

	case type_t::road:
		return ptr_t(new ObjRoad());

	case type_t::area:
		return ptr_t(new ObjArea());

	default:
		return ptr_t();
	}
}

OSMDB::Object::ptr_t OSMDB::Object::create(HibernateReadStream& ar)
{
	type_t typ;
	ar.ioenum(typ);
	ptr_t p(create(typ));
	if (p)
		p->load(ar);
	return p;
}

OSMDB::Object::ptr_t OSMDB::Object::create(HibernateReadBuffer& ar)
{
	type_t typ;
	ar.ioenum(typ);
	ptr_t p(create(typ));
	if (p)
		p->load(ar);
	return p;
}

void OSMDB::Object::store(HibernateWriteStream& ar)
{
	type_t typ(get_type());
	ar.ioenum(typ);
	save(ar);
}

std::ostream& OSMDB::Object::print(std::ostream& os, const OSMDB& db, printflags_t flags) const
{
	static const char * const pfx[4] = { "Point", "Line", "Road", "Area" };
	os << pfx[static_cast<unsigned int>(get_type())] << " ID " << get_id();
	{
		Rect bbox(get_bbox());
		if (!bbox.is_invalid())
			os << ' ' << bbox.get_southwest().get_lat_str2()
			   << ' ' << bbox.get_southwest().get_lon_str2()
			   << ' ' << bbox.get_northeast().get_lat_str2()
			   << ' ' << bbox.get_northeast().get_lon_str2();
	}
	os << " Z " << get_zorder() << std::endl;
	if ((flags & printflags_t::tags) != printflags_t::none) {
		bool subseq(false);
		for (const auto& t : get_tags()) {
			os << (subseq ? ", " : "  Tags: ");
			subseq = true;
			const char *tk(db.find_tagkey(std::get<OSMDB::Object::tagkey_t>(t)));
			if (tk)
				os << '"' << tk << '"';
			else
				os << static_cast<std::underlying_type<tagkey_t>::type>(std::get<OSMDB::Object::tagkey_t>(t));
			os << " = ";
			const char *tn(db.find_tagname(std::get<OSMDB::Object::tagname_t>(t)));
			if (tn)
				os << '"' << tn << '"';
			else
				os << static_cast<std::underlying_type<tagname_t>::type>(std::get<OSMDB::Object::tagname_t>(t));
		}
		if (subseq)
			os << std::endl;
	}
	return os;
}

constexpr OSMDB::ObjPoint::type_t OSMDB::ObjPoint::object_type;
const std::string OSMDB::ObjPoint::object_name("point");

OSMDB::ObjPoint::ObjPoint(id_t id, const tags_t& tags, int32_t zorder, const Point& loc)
	: Object(id, tags, zorder), m_location(loc)
{
}

std::ostream& OSMDB::ObjPoint::print(std::ostream& os, const OSMDB& db, printflags_t flags) const
{
	Object::print(os, db, flags);
	if ((flags & printflags_t::coords) != printflags_t::none)
		get_location().print(os << "  POINT: ") << std::endl;
	return os;
}

int OSMDB::ObjPoint::compare(const Object& x) const
{
	int c(Object::compare(x));
	if (c)
		return c;
	const ObjPoint& xx(static_cast<const ObjPoint&>(x));
	return m_location.compare(xx.m_location);
}

void OSMDB::ObjPoint::load(HibernateReadStream& ar)
{
	hibernate(ar);
}

void OSMDB::ObjPoint::load(HibernateReadBuffer& ar)
{
	hibernate(ar);
}

void OSMDB::ObjPoint::save(HibernateWriteStream& ar) const
{
	(const_cast<ObjPoint *>(this))->hibernate(ar);
}

void OSMDB::ObjPoint::loaddb(HibernateReadStream& ar)
{
	Object::hibernate_noid(ar);
}

void OSMDB::ObjPoint::loaddb(HibernateReadBuffer& ar)
{
	Object::hibernate_noid(ar);
}

void OSMDB::ObjPoint::savedb(HibernateWriteStream& ar) const
{
	(const_cast<ObjPoint *>(this))->Object::hibernate_noid(ar);
}

constexpr OSMDB::ObjLine::type_t OSMDB::ObjLine::object_type;
const std::string OSMDB::ObjLine::object_name("line");

OSMDB::ObjLine::ObjLine(id_t id, const tags_t& tags, int32_t zorder, const LineString& ln)
	: Object(id, tags, zorder), m_line(ln)
{
}

std::ostream& OSMDB::ObjLine::print(std::ostream& os, const OSMDB& db, printflags_t flags) const
{
	Object::print(os, db, flags);
	if ((flags & printflags_t::coords) != printflags_t::none)
		get_line().print(os << "  LINE: ") << std::endl;
	if ((flags & printflags_t::skyvector) != printflags_t::none)
		os << "  " << get_line().to_skyvector() << std::endl;
	return os;
}

int OSMDB::ObjLine::compare(const Object& x) const
{
	int c(Object::compare(x));
	if (c)
		return c;
	const ObjLine& xx(static_cast<const ObjLine&>(x));
	return m_line.compare(xx.m_line);
}

void OSMDB::ObjLine::load(HibernateReadStream& ar)
{
	hibernate(ar);
}

void OSMDB::ObjLine::load(HibernateReadBuffer& ar)
{
	hibernate(ar);
}

void OSMDB::ObjLine::save(HibernateWriteStream& ar) const
{
	(const_cast<ObjLine *>(this))->hibernate(ar);
}

void OSMDB::ObjLine::loaddb(HibernateReadStream& ar)
{
	hibernate_noid(ar);
}

void OSMDB::ObjLine::loaddb(HibernateReadBuffer& ar)
{
	hibernate_noid(ar);
}

void OSMDB::ObjLine::savedb(HibernateWriteStream& ar) const
{
	(const_cast<ObjLine *>(this))->hibernate_noid(ar);
}

constexpr OSMDB::ObjRoad::type_t OSMDB::ObjRoad::object_type;
const std::string OSMDB::ObjRoad::object_name("road");

OSMDB::ObjRoad::ObjRoad(id_t id, const tags_t& tags, int32_t zorder, const LineString& ln)
	: ObjLine(id, tags, zorder, ln)
{
}

constexpr OSMDB::ObjArea::type_t OSMDB::ObjArea::object_type;
const std::string OSMDB::ObjArea::object_name("area");

OSMDB::ObjArea::ObjArea(id_t id, const tags_t& tags, int32_t zorder, const MultiPolygonHole& a)
	: Object(id, tags, zorder), m_area(a)
{
}

std::ostream& OSMDB::ObjArea::print(std::ostream& os, const OSMDB& db, printflags_t flags) const
{
	Object::print(os, db, flags);
	if ((flags & printflags_t::coords) != printflags_t::none)
		get_area().print(os << "  AREA: ") << std::endl;
	if ((flags & printflags_t::skyvector) != printflags_t::none)
		for (const auto& ph : get_area()) {
			os << "  OUTER: " << ph.get_exterior().to_skyvector() << std::endl;
			for (unsigned int i(0), n(ph.get_nrinterior()); i < n; ++i)
				os << "    INNER: " << ph[i].to_skyvector() << std::endl;
		}
	return os;
}

int OSMDB::ObjArea::compare(const Object& x) const
{
	int c(Object::compare(x));
	if (c)
		return c;
	const ObjArea& xx(static_cast<const ObjArea&>(x));
	return m_area.compare(xx.m_area);
}

void OSMDB::ObjArea::load(HibernateReadStream& ar)
{
	hibernate(ar);
}

void OSMDB::ObjArea::load(HibernateReadBuffer& ar)
{
	hibernate(ar);
}

void OSMDB::ObjArea::save(HibernateWriteStream& ar) const
{
	(const_cast<ObjArea *>(this))->hibernate(ar);
}

void OSMDB::ObjArea::loaddb(HibernateReadStream& ar)
{
	hibernate_noid(ar);
}

void OSMDB::ObjArea::loaddb(HibernateReadBuffer& ar)
{
	hibernate_noid(ar);
}

void OSMDB::ObjArea::savedb(HibernateWriteStream& ar) const
{
	(const_cast<ObjArea *>(this))->hibernate_noid(ar);
}

OSMDB::Statistics::RTree::RTree(void)
	: m_bytes(0), m_objects(0), m_pages(0), m_depth(0)
{
}

OSMDB::Statistics::RTree::RTree(const BinFileRtreeInternalNode *node)
	: RTree()
{
	depth_first_visit(node, 1);
}

void OSMDB::Statistics::RTree::depth_first_visit(const BinFileRtreeInternalNode *node, unsigned int depth)
{
	if (!node)
		return;
	m_depth = std::max(m_depth, depth);
	++m_pages;
	m_bytes += node->get_size();
	uint16_t n(node->get_nodes());
	++depth;
	if (node->is_pointstoleaf()) {
		for (uint16_t i(0); i < n; ++i)
			depth_first_visit(node->get_leafnode(i), depth);
	} else {
		for (uint16_t i(0); i < n; ++i)
			depth_first_visit(node->get_internalnode(i), depth);
	}
}

void OSMDB::Statistics::RTree::depth_first_visit(const BinFileRtreeLeafNode *node, unsigned int depth)
{
	if (!node)
		return;
	m_depth = std::max(m_depth, depth);
	++m_pages;
	m_bytes += node->get_size();
	m_objects += node->get_nodes();
}

bool OSMDB::Statistics::RTree::is_valid(void) const
{
	return !(!get_objects() && !get_pages() && !get_bytes() && !get_depth());
}

std::string OSMDB::Statistics::RTree::to_str(void) const
{
	std::ostringstream oss;
	oss << get_objects() << " objects, " << get_pages() << " pages " << get_bytes() << " bytes " << get_depth() << " depth";
	return oss.str();
}

OSMDB::Statistics::Statistics(void)
	: m_tagkeys(0), m_tagnames(0)
{
	for (Object::type_t t(Object::type_t::point); t <= Object::type_t::area; t = static_cast<Object::type_t>(static_cast<unsigned int>(t) + 1))
		m_objects[static_cast<unsigned int>(t)] = 0;
}

OSMDB::Statistics::Statistics(const BinFileHeader& hdr, bool dortree)
{
	m_tagkeys = hdr.get_tagkeyentries();
	m_tagnames = hdr.get_tagnameentries();
	for (Object::type_t t(Object::type_t::point); t <= Object::type_t::area; t = static_cast<Object::type_t>(static_cast<unsigned int>(t) + 1)) {
		m_objects[static_cast<unsigned int>(t)] = hdr.get_objdirentries(t);
		if (dortree)
			m_rtrees[static_cast<unsigned int>(t)] = RTree(hdr.get_rtree_root(t));
	}
}

unsigned int OSMDB::Statistics::get_objects(Object::type_t typ) const
{
	if (typ > Object::type_t::area)
		return 0;
	return m_objects[static_cast<unsigned int>(typ)];
}

const OSMDB::Statistics::RTree& OSMDB::Statistics::get_rtree(Object::type_t typ) const
{
	static const RTree empty;
	if (typ > Object::type_t::area)
		return empty;
	return m_rtrees[static_cast<unsigned int>(typ)];
}

std::string OSMDB::Statistics::to_str(void) const
{
	std::ostringstream oss;
	for (Object::type_t t(Object::type_t::point); t <= Object::type_t::area; t = static_cast<Object::type_t>(static_cast<unsigned int>(t) + 1))
		oss << get_objects(t) << ' ' << t << "s, ";
	oss << "tags " << get_tagkeys() << " keys " << get_tagnames() << " names";
	for (Object::type_t t(Object::type_t::point); t <= Object::type_t::area; t = static_cast<Object::type_t>(static_cast<unsigned int>(t) + 1)) {
		const RTree& rt(get_rtree(t));
		if (!rt.is_valid())
			continue;
		oss << ", " << t << " rtree " << rt.to_str();
	}
	return oss.str();
}

constexpr unsigned int OSMDB::rtreemaxelperpage;
constexpr unsigned int OSMDB::rtreedefaultmaxelperpage;
constexpr uint64_t OSMDB::aligninc;
constexpr uint64_t OSMDB::alignmask;

OSMDB::OSMDB(const std::string& path)
	: m_path(path)
{
}

OSMDB::~OSMDB()
{
	close();
}

void OSMDB::set_path(const std::string& path)
{
	if (path == m_path)
		return;
	close();
	m_path = path;
}

void OSMDB::open(void)
{
	static constexpr bool errmsg = true;
	if (m_bin.is_open())
		return;
	try {
		m_bin.open(m_path.c_str(), false, "osm database");
	} catch (const std::exception& e) {
		if (errmsg)
			std::cerr << "Cannot open file " << m_path << ": " << e.what() << std::endl;
		return;
	}
	const BinFileHeader& hdr(*m_bin.begin_as<BinFileHeader>());
	if (!hdr.check_signature()) {
		if (errmsg)
			std::cerr << "Invalid signature on file " << m_path << std::endl;
		close();
		return;
	}
	if (errmsg) {
		std::cerr << "Using file " << m_path << ": ";
		for (Object::type_t t(Object::type_t::point); t <= Object::type_t::area; t = static_cast<Object::type_t>(static_cast<unsigned int>(t) + 1)) {
			std::cerr << hdr.get_objdirentries(t) << ' ' << t << " objects";
			if (hdr.get_rtreeoffs(t))
				std::cerr << " (rtree)";
			std::cerr << ", ";
		}
		std::cerr << hdr.get_tagkeyentries() << " tags" << std::endl;
	}
}

void OSMDB::close(void)
{
	m_bin.close();
}

template<OSMDB::Object::type_t typ>
typename OSMDB::obj_types<typ>::ObjType::const_ptr_t OSMDB::load_object(const typename obj_types<typ>::IndexType *pe) const {
	typedef typename obj_types<typ>::ObjType ObjType;
	if (!pe)
		return typename ObjType::const_ptr_t();
	unsigned int sz(pe->get_datasize());
	if (!sz)
		return typename ObjType::const_ptr_t();
	const uint8_t *data(pe->get_dataptr());
	HibernateReadBuffer ar(data, data + sz);
	typename ObjType::ptr_t p(new ObjType());
	p->hibernate_noid(ar);
	p->set_id(pe->get_id());
	return p;
}

template<>
typename OSMDB::obj_types<OSMDB::Object::type_t::point>::ObjType::const_ptr_t OSMDB::load_object<OSMDB::Object::type_t::point>(const typename obj_types<Object::type_t::point>::IndexType *pe) const {
	typedef typename obj_types<Object::type_t::point>::ObjType ObjType;
	if (!pe)
		return typename ObjType::const_ptr_t();
	unsigned int sz(pe->get_datasize());
	if (!sz)
		return typename ObjType::const_ptr_t();
	const uint8_t *data(pe->get_dataptr());
	HibernateReadBuffer ar(data, data + sz);
	typename ObjType::ptr_t p(new ObjType());
	p->Object::hibernate_noid(ar);
	p->set_id(pe->get_id());
	p->set_location(pe->get_location());
	return p;
}

template<OSMDB::Object::type_t typ>
void OSMDB::find_object(objects_t& obj) const
{
	typedef typename obj_types<typ>::ObjType ObjType;
	typedef typename obj_types<typ>::IndexType IndexType;
	typedef typename obj_types<typ>::IndexRangeType IndexRangeType;
	if (!m_bin.is_open())
		return;
	const BinFileHeader& hdr(*m_bin.begin_as<BinFileHeader>());
	IndexRangeType index(hdr.get_objdir_range<typ>());
	for (const IndexType *oi(index.first); oi != index.second; ++oi) {
		typename ObjType::const_ptr_t p(load_object<typ>(oi));
		if (!p)
			continue;
		obj.push_back(p);
	}
}

OSMDB::objects_t OSMDB::find(typemask_t mask) const
{
	objects_t obj;
	if ((mask & typemask_t::point) != typemask_t::none)
		find_object<Object::type_t::point>(obj);
	if ((mask & typemask_t::line) != typemask_t::none)
		find_object<Object::type_t::line>(obj);
	if ((mask & typemask_t::road) != typemask_t::none)
		find_object<Object::type_t::road>(obj);
	if ((mask & typemask_t::area) != typemask_t::none)
		find_object<Object::type_t::area>(obj);
	return obj;
}

template<OSMDB::Object::type_t typ>
void OSMDB::find_object(objects_t& obj, Object::id_t id) const
{
	typedef typename obj_types<typ>::ObjType ObjType;
	typedef typename obj_types<typ>::IndexType IndexType;
	typedef typename obj_types<typ>::IndexRangeType IndexRangeType;
	if (!m_bin.is_open())
		return;
	const BinFileHeader& hdr(*m_bin.begin_as<BinFileHeader>());
	IndexRangeType index(hdr.get_objdir_range<typ>());
	for (const IndexType *oi(std::lower_bound(index.first, index.second, id, BinFileIDSorter())); oi != index.second && oi->get_id() == id; ++oi) {
		typename ObjType::const_ptr_t p(load_object<typ>(oi));
		if (!p)
			continue;
		obj.push_back(p);
	}
}

OSMDB::objects_t OSMDB::find(Object::id_t id, Object::type_t typ) const
{
	objects_t obj;
	switch (typ) {
	case Object::type_t::point:
		find_object<Object::type_t::point>(obj, id);
		break;

	case Object::type_t::line:
		find_object<Object::type_t::line>(obj, id);
		break;

	case Object::type_t::road:
		find_object<Object::type_t::road>(obj, id);
		break;

	case Object::type_t::area:
		find_object<Object::type_t::area>(obj, id);
		break;

	default:
		break;
	}
	return obj;
}

OSMDB::objects_t OSMDB::find(Object::id_t id, typemask_t mask) const
{
	objects_t obj;
	if ((mask & typemask_t::point) != typemask_t::none)
		find_object<Object::type_t::point>(obj, id);
	if ((mask & typemask_t::line) != typemask_t::none)
		find_object<Object::type_t::line>(obj, id);
	if ((mask & typemask_t::road) != typemask_t::none)
		find_object<Object::type_t::road>(obj, id);
	if ((mask & typemask_t::area) != typemask_t::none)
		find_object<Object::type_t::area>(obj, id);
	return obj;
}

void OSMDB::rtree_visitor(rtree_ids_t& ids, const Rect& bbox, const BinFileRtreeInternalNode *node)
{
	if (!node)
		return;
	uint16_t n(node->get_nodes());
	if (node->is_pointstoleaf()) {
		for (uint16_t i(0); i < n; ++i)
			if (bbox.is_intersect(node->get_bbox(i)))
				rtree_visitor(ids, node->get_leafnode(i));
	} else {
		for (uint16_t i(0); i < n; ++i)
			if (bbox.is_intersect(node->get_bbox(i)))
				rtree_visitor(ids, bbox, node->get_internalnode(i));
	}
}

void OSMDB::rtree_visitor(rtree_ids_t& ids, const BinFileRtreeLeafNode *node)
{
	if (!node)
		return;
	uint16_t n(node->get_nodes());
	for (uint16_t i(0); i < n; ++i)
		ids.push_back(node->get_index(i));
}

template<OSMDB::Object::type_t typ>
void OSMDB::find_objects(objects_t& obj, const Rect& bbox) const
{
	typedef typename obj_types<typ>::ObjType ObjType;
	typedef typename obj_types<typ>::IndexType IndexType;
	typedef typename obj_types<typ>::IndexRangeType IndexRangeType;
	if (!m_bin.is_open())
		return;
	const BinFileHeader& hdr(*m_bin.begin_as<BinFileHeader>());
	IndexRangeType index(hdr.get_objdir_range<typ>());
	const BinFileRtreeInternalNode *rtree(hdr.get_rtree_root(typ));
	if (!rtree) {
		// if the database has no spatial index, go through all objects
		for (const IndexType *oi(index.first); oi != index.second; ++oi) {
			if (!(oi->is_match(bbox)))
				continue;
			typename ObjType::const_ptr_t p(load_object<typ>(oi));
			if (!p)
				continue;
			obj.push_back(p);
		}
		return;
	}
	rtree_ids_t ids;
	rtree_visitor(ids, bbox, rtree);
	if (ids.empty())
		return;
	std::sort(ids.begin(), ids.end());
	BinFileRtreeLeafNode::index_t lastid(*ids.begin() - 1);
	for (const auto id : ids) {
		if (id == lastid)
			continue;
		lastid = id;
		const IndexType *oi(index.first + id);
		if (oi >= index.second || !(oi->is_match(bbox)))
			continue;
		typename ObjType::const_ptr_t p(load_object<typ>(oi));
		if (!p)
			continue;
		obj.push_back(p);
	}
}

OSMDB::objects_t OSMDB::find(const Rect& bbox, typemask_t mask) const
{
	objects_t obj;
	if ((mask & typemask_t::point) != typemask_t::none)
		find_objects<Object::type_t::point>(obj, bbox);
	if ((mask & typemask_t::line) != typemask_t::none)
		find_objects<Object::type_t::line>(obj, bbox);
	if ((mask & typemask_t::road) != typemask_t::none)
		find_objects<Object::type_t::road>(obj, bbox);
	if ((mask & typemask_t::area) != typemask_t::none)
		find_objects<Object::type_t::area>(obj, bbox);
	return obj;
}

OSMDB::Object::const_ptr_t OSMDB::get(uint32_t index, Object::type_t typ) const
{
	if (!m_bin.is_open())
		return Object::const_ptr_t();
	const BinFileHeader& hdr(*m_bin.begin_as<BinFileHeader>());
	switch (typ) {
	case Object::type_t::point:
	{
		obj_types<Object::type_t::point>::IndexRangeType idx(hdr.get_objdir_range<Object::type_t::point>());
		const obj_types<Object::type_t::point>::IndexType *oi(idx.first + index);
		if (oi >= idx.second)
			return Object::const_ptr_t();
		return load_object<Object::type_t::point>(oi);
	}

	case Object::type_t::line:
	{
		obj_types<Object::type_t::line>::IndexRangeType idx(hdr.get_objdir_range<Object::type_t::line>());
		const obj_types<Object::type_t::line>::IndexType *oi(idx.first + index);
		if (oi >= idx.second)
			return Object::const_ptr_t();
		return load_object<Object::type_t::line>(oi);
	}

	case Object::type_t::road:
	{
		obj_types<Object::type_t::road>::IndexRangeType idx(hdr.get_objdir_range<Object::type_t::road>());
		const obj_types<Object::type_t::road>::IndexType *oi(idx.first + index);
		if (oi >= idx.second)
			return Object::const_ptr_t();
		return load_object<Object::type_t::road>(oi);
	}

	case Object::type_t::area:
	{
		obj_types<Object::type_t::area>::IndexRangeType idx(hdr.get_objdir_range<Object::type_t::area>());
		const obj_types<Object::type_t::area>::IndexType *oi(idx.first + index);
		if (oi >= idx.second)
			return Object::const_ptr_t();
		return load_object<Object::type_t::area>(oi);
	}

	default:
		return Object::const_ptr_t();
	}
}

const char *OSMDB::find_tagkey(Object::tagkey_t key) const
{
	if (!m_bin.is_open())
		return nullptr;
	const BinFileHeader& hdr(*m_bin.begin_as<BinFileHeader>());
	const BinFileTagKeyEntry *oi(hdr.get_tagkey_begin());
	if (!oi)
		return nullptr;
	const BinFileTagKeyEntry *oe(hdr.get_tagkey_end());
	const BinFileTagKeyEntry *ox(oi + static_cast<std::underlying_type<Object::tagkey_t>::type>(key));
	if (ox >= oe)
		return nullptr;
	return ox->get_strptr();
}

OSMDB::Object::tagkey_t OSMDB::find_tagkey(const std::string& tag) const
{
	if (!m_bin.is_open())
		return Object::tagkey_t::invalid;
	const BinFileHeader& hdr(*m_bin.begin_as<BinFileHeader>());
	const BinFileTagKeyEntry *oi(hdr.get_tagkey_begin());
	const BinFileTagKeyEntry *oe(hdr.get_tagkey_end());
	const BinFileTagKeyEntry *ox(std::lower_bound(oi, oe, tag.c_str(), BinFileTagKeySorter()));
	if (ox == oe || BinFileTagKeySorter()(tag.c_str(), *ox))
		return Object::tagkey_t::invalid;
	return static_cast<Object::tagkey_t>(ox - oi);
}

bool OSMDB::set_tagkey(std::string& v, Object::tagkey_t key) const
{
	const char *cp(find_tagkey(key));
	if (cp) {
		v = cp;
		return true;
	}
	v.clear();
	return false;
}

const char *OSMDB::find_tagname(Object::tagname_t key) const
{
	if (!m_bin.is_open())
		return nullptr;
	const BinFileHeader& hdr(*m_bin.begin_as<BinFileHeader>());
	const BinFileTagNameEntry *oi(hdr.get_tagname_begin());
	if (!oi)
		return nullptr;
	const BinFileTagNameEntry *oe(hdr.get_tagname_end());
	const BinFileTagNameEntry *ox(oi + static_cast<std::underlying_type<Object::tagname_t>::type>(key));
	if (ox >= oe)
		return nullptr;
	return ox->get_strptr();
}

OSMDB::Object::tagname_t OSMDB::find_tagname(const std::string& tag) const
{
	if (!m_bin.is_open())
		return Object::tagname_t::invalid;
	const BinFileHeader& hdr(*m_bin.begin_as<BinFileHeader>());
	const BinFileTagNameEntry *oi(hdr.get_tagname_begin());
	const BinFileTagNameEntry *oe(hdr.get_tagname_end());
	const BinFileTagNameEntry *ox(std::lower_bound(oi, oe, tag.c_str(), BinFileTagNameSorter()));
	if (ox == oe || BinFileTagNameSorter()(tag.c_str(), *ox))
		return Object::tagname_t::invalid;
	return static_cast<Object::tagname_t>(ox - oi);
}

bool OSMDB::set_tagname(std::string& v, Object::tagname_t key) const
{
	const char *cp(find_tagname(key));
	if (cp) {
		v = cp;
		return true;
	}
	v.clear();
	return false;
}

OSMDB::TagNameSet OSMDB::create_tagnameset(const std::vector<std::string>& x) const
{
	TagNameSet r;
	for (const std::string& s : x) {
		Object::tagname_t t(find_tagname(s));
		if (t != Object::tagname_t::invalid)
			r.insert(t);
	}
	return r;
}

OSMDB::TagNameSet OSMDB::create_tagnameset(const char * const *x) const
{
	if (!x)
		return TagNameSet();
	TagNameSet r;
	for (; *x; ++x) {
		Object::tagname_t t(find_tagname(*x));
		if (t != Object::tagname_t::invalid)
			r.insert(t);
	}
	return r;
}

OSMDB::TagNameSet OSMDB::create_tagnameset(std::initializer_list<const char *> x) const
{
	TagNameSet r;
	for (const auto v : x) {
		Object::tagname_t t(find_tagname(v));
		if (t != Object::tagname_t::invalid)
			r.insert(t);
	}
	return r;
}

OSMDB::Statistics OSMDB::get_statistics(bool dortree) const
{
	if (!m_bin.is_open())
		return Statistics();
	const BinFileHeader& hdr(*m_bin.begin_as<BinFileHeader>());
	return Statistics(hdr, dortree);
}

template class OSMDB::BinFileEntry<4>;
template class OSMDB::BinFileEntry<16>;
template class OSMDB::BinFileEntry<20>;
template class OSMDB::BinFileEntry<24>;
template class OSMDB::BinFileEntry<32>;
template class OSMDB::BinFileEntry<256>;
template class OSMDB::BinFileEntry<2+OSMDB::rtreemaxelperpage*4>;
template class OSMDB::BinFileEntry<2+OSMDB::rtreemaxelperpage*24>;
template typename OSMDB::obj_types<OSMDB::Object::type_t::point>::IndexRangeType OSMDB::BinFileHeader::get_objdir_range<OSMDB::Object::type_t::point>(void) const;
template typename OSMDB::obj_types<OSMDB::Object::type_t::line>::IndexRangeType OSMDB::BinFileHeader::get_objdir_range<OSMDB::Object::type_t::line>(void) const;
template typename OSMDB::obj_types<OSMDB::Object::type_t::road>::IndexRangeType OSMDB::BinFileHeader::get_objdir_range<OSMDB::Object::type_t::road>(void) const;
template typename OSMDB::obj_types<OSMDB::Object::type_t::area>::IndexRangeType OSMDB::BinFileHeader::get_objdir_range<OSMDB::Object::type_t::area>(void) const;
