/*****************************************************************************/

/*
 *      acftclbdesc.cc  --  Aircraft Model Climb/Descent.
 *
 *      Copyright (C) 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020
 *        Thomas Sailer (t.sailer@alumni.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/*****************************************************************************/

#include "aircraft.h"

#include <fstream>
#include <iostream>
#include <iomanip>

#ifdef HAVE_EIGEN3
#include <Eigen/Dense>
#include <Eigen/Cholesky>
#include <unsupported/Eigen/Polynomials>
#endif

#ifdef HAVE_JSONCPP
#include <json/json.h>
#endif

#ifdef HAVE_EIGEN3

namespace {

#ifdef __WIN32__
__attribute__((force_align_arg_pointer))
#endif

Eigen::VectorXd least_squares_solution(const Eigen::MatrixXd& A, const Eigen::VectorXd& b)
{
	if (false) {
		// traditional
		return (A.transpose() * A).inverse() * A.transpose() * b;
	} else if (true) {
		// cholesky
		return (A.transpose() * A).ldlt().solve(A.transpose() * b);
	} else if (false) {
		// householder QR
		return A.colPivHouseholderQr().solve(b);
	} else if (false) {
		// jacobi SVD
		return A.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(b);
	} else {
		typedef Eigen::JacobiSVD<Eigen::MatrixXd, Eigen::FullPivHouseholderQRPreconditioner> jacobi_t;
		jacobi_t jacobi(A, Eigen::ComputeFullU | Eigen::ComputeFullV);
		return jacobi.solve(b);
	}
}

};

#endif

Aircraft::ClimbDescent::Point::Point(double pa, double rate, double fuelflow, double cas)
	: m_pa(pa), m_rate(rate), m_fuelflow(fuelflow), m_cas(cas)
{
}

void Aircraft::ClimbDescent::Point::load_xml(const xmlpp::Element *el, mode_t& mode, double altfactor,
					     double ratefactor, double fuelflowfactor, double casfactor,
					     double timefactor, double fuelfactor, double distfactor)
{
	if (!el)
		return;
	m_pa = m_rate = m_fuelflow = std::numeric_limits<double>::quiet_NaN();
	xmlpp::Attribute *attr;
	if (((attr = el->get_attribute("pa")) || (attr = el->get_attribute("da"))) && attr->get_value() != "")
		m_pa = Glib::Ascii::strtod(attr->get_value()) * altfactor;
	if (mode == mode_rateofclimb || mode == mode_invalid) {
		if ((attr = el->get_attribute("rate")) && attr->get_value() != "") {
			m_rate = Glib::Ascii::strtod(attr->get_value()) * ratefactor;
			mode = mode_rateofclimb;
		}
		if ((attr = el->get_attribute("fuelflow")) && attr->get_value() != "") {
			m_fuelflow = Glib::Ascii::strtod(attr->get_value()) * fuelflowfactor;
			mode = mode_rateofclimb;
		}
		if ((attr = el->get_attribute("cas")) && attr->get_value() != "") {
			m_cas = Glib::Ascii::strtod(attr->get_value()) * casfactor;
			mode = mode_rateofclimb;
		}
	}
	if (mode == mode_timetoaltitude || mode == mode_invalid) {
		if ((attr = el->get_attribute("time")) && attr->get_value() != "") {
			m_rate = Glib::Ascii::strtod(attr->get_value()) * timefactor;
			mode = mode_timetoaltitude;
		}
		if ((attr = el->get_attribute("fuel")) && attr->get_value() != "") {
			m_fuelflow = Glib::Ascii::strtod(attr->get_value()) * fuelfactor;
			mode = mode_timetoaltitude;
		}
		if ((attr = el->get_attribute("dist")) && attr->get_value() != "") {
			m_cas = Glib::Ascii::strtod(attr->get_value()) * distfactor;
			mode = mode_timetoaltitude;
		} else if ((attr = el->get_attribute("distance")) && attr->get_value() != "") {
			m_cas = Glib::Ascii::strtod(attr->get_value()) * distfactor;
			mode = mode_timetoaltitude;
		}
	}
}

void Aircraft::ClimbDescent::Point::save_xml(xmlpp::Element *el, mode_t mode, double altfactor,
					     double ratefactor, double fuelflowfactor, double casfactor,
					     double timefactor, double fuelfactor, double distfactor) const
{
	if (!el)
		return;
	if (std::isnan(m_pa) || altfactor == 0) {
		el->remove_attribute("pa");
	} else {
		std::ostringstream oss;
		oss << (m_pa / altfactor);
		el->set_attribute("pa", oss.str());
	}
	switch (mode) {
	case mode_rateofclimb:
		if (std::isnan(m_rate) || std::isnan(ratefactor) || ratefactor == 0) {
			el->remove_attribute("rate");
		} else {
			std::ostringstream oss;
			oss << (m_rate / ratefactor);
			el->set_attribute("rate", oss.str());
		}
		if (std::isnan(m_fuelflow) || std::isnan(fuelflowfactor) || fuelflowfactor == 0) {
			el->remove_attribute("fuelflow");
		} else {
			std::ostringstream oss;
			oss << (m_fuelflow / fuelflowfactor);
			el->set_attribute("fuelflow", oss.str());
		}
		if (std::isnan(m_cas) || std::isnan(casfactor) || casfactor == 0) {
			el->remove_attribute("cas");
		} else {
			std::ostringstream oss;
			oss << (m_cas / casfactor);
			el->set_attribute("cas", oss.str());
		}
		break;

	case mode_timetoaltitude:
		if (std::isnan(m_rate) || std::isnan(timefactor) || timefactor == 0) {
			el->remove_attribute("time");
		} else {
			std::ostringstream oss;
			oss << (m_rate / timefactor);
			el->set_attribute("time", oss.str());
		}
		if (std::isnan(m_fuelflow) || std::isnan(fuelfactor) || fuelfactor == 0) {
			el->remove_attribute("fuel");
		} else {
			std::ostringstream oss;
			oss << (m_fuelflow / fuelfactor);
			el->set_attribute("fuel", oss.str());
		}
		if (std::isnan(m_cas) || std::isnan(distfactor) || distfactor == 0) {
			el->remove_attribute("dist");
		} else {
			std::ostringstream oss;
			oss << (m_cas / distfactor);
			el->set_attribute("dist", oss.str());
		}
		break;

	default:
		break;
	}
}

std::ostream& Aircraft::ClimbDescent::Point::print(std::ostream& os, mode_t mode) const
{
	switch (mode) {
	case mode_rateofclimb:
		return os << "rate " << get_rate() << " ff " << get_fuelflow() << " cas " << get_cas();

	case mode_timetoaltitude:
		return os << "time " << get_time() << " fuel " << get_fuel() << " dist " << get_dist();

	default:
		return os << "?";
	}
}

const std::string& to_str(Aircraft::ClimbDescent::mode_t m)
{
	switch (m) {
	case Aircraft::ClimbDescent::mode_rateofclimb:
	{
		static const std::string r("rateofclimb");
		return r;
	}

	case Aircraft::ClimbDescent::mode_timetoaltitude:
	{
		static const std::string r("timetoaltitude");
		return r;
	}

	default:
	case Aircraft::ClimbDescent::mode_invalid:
	{
		static const std::string r("");
		return r;
	}
	}
}

constexpr double Aircraft::ClimbDescent::maxtime;
constexpr double Aircraft::ClimbDescent::minroc;

Aircraft::ClimbDescent::ClimbDescent(const std::string& name, double mass, double isaoffs)
	: m_name(name), m_mass(mass), m_isaoffset(isaoffs),
	  m_ceiling(18000), m_climbtime(3600), m_mode(mode_rateofclimb)
{
	// PA28R-200
	m_points.push_back(Point(0, 910, 14, 95 * 0.86897624));
	m_points.push_back(Point(18000, 0, 8, 95 * 0.86897624));
	recalculatepoly(false);
}

void Aircraft::ClimbDescent::calculate(double& rate, double& fuelflow, double& cas, double da) const
{
	rate = m_ratepoly.eval(da);
	fuelflow = m_fuelflowpoly.eval(da);
	cas = m_caspoly.eval(da);
}

double Aircraft::ClimbDescent::time_to_altitude(double t) const
{
	return m_climbaltpoly.eval(t);
}

double Aircraft::ClimbDescent::time_to_distance(double t) const
{
	return m_climbdistpoly.eval(t);
}

double Aircraft::ClimbDescent::time_to_fuel(double t) const
{
	return m_climbfuelpoly.eval(t);
}

double Aircraft::ClimbDescent::time_to_climbrate(double t) const
{
	return m_climbaltpoly.evalderiv(t) * 60.0;
}

double Aircraft::ClimbDescent::time_to_tas(double t) const
{
	return m_climbdistpoly.evalderiv(t) * 3600.0;
}

double Aircraft::ClimbDescent::time_to_fuelflow(double t) const
{
	return m_climbfuelpoly.evalderiv(t) * 3600.0;
}

double Aircraft::ClimbDescent::altitude_to_time(double a) const
{
	return m_climbaltpoly.boundedinveval(a, 0, get_climbtime());
}

double Aircraft::ClimbDescent::distance_to_time(double d) const
{
	return m_climbdistpoly.boundedinveval(d, 0, get_climbtime());
}

#ifdef __WIN32__
__attribute__((force_align_arg_pointer))
#endif
bool Aircraft::ClimbDescent::recalculateratepoly(bool force)
{
	static constexpr bool debuglog = false;
	if (std::isnan(m_ceiling) || m_ceiling <= 0) {
		m_ceiling = 0;
		for (points_t::const_iterator pi(m_points.begin()), pe(m_points.end()); pi != pe; ++pi) {
			double pa(pi->get_pressurealt());
			if (std::isnan(pa))
				continue;
			m_ceiling = std::max(m_ceiling, pa);
		}
	}
	m_ceiling = std::min(m_ceiling, 50000.);
#ifdef HAVE_EIGEN3
	if (!force && !m_ratepoly.empty() && !m_fuelflowpoly.empty() && !m_caspoly.empty())
		return false;
	if (m_points.empty()) {
		m_ratepoly.clear();
		m_fuelflowpoly.clear();
		m_caspoly.clear();
		return false;
	}
	unsigned int polyorder(4);
	unsigned int ptrate(0);
	unsigned int ptfuel(0);
	unsigned int ptcas(0);
	Eigen::MatrixXd mr(m_points.size(), polyorder);
	Eigen::MatrixXd mf(m_points.size(), polyorder);
	Eigen::MatrixXd mc(m_points.size(), polyorder);
	Eigen::VectorXd vr(m_points.size());
	Eigen::VectorXd vf(m_points.size());
	Eigen::VectorXd vc(m_points.size());
	{
		typedef std::map<double,double> dmap_t;
		dmap_t rmap, fmap, cmap;
		for (points_t::const_iterator pi(m_points.begin()), pe(m_points.end()); pi != pe; ++pi) {
			if (debuglog)
				std::cerr << "climbpoint: pa " << pi->get_pressurealt() << " rate " << pi->get_rate()
					  << " ff " << pi->get_fuelflow() << " cas " << pi->get_cas() << std::endl;
			double pa(pi->get_pressurealt());
			if (std::isnan(pa))
				continue;
			double d(pi->get_rate());
			if (!std::isnan(d)) {
				std::pair<dmap_t::iterator,bool> ins(rmap.insert(dmap_t::value_type(pa, d)));
				if (!ins.second) {
					std::cerr << "Duplicate entry for PA " << pa << " new rate value " << d
						  << " previous " << ins.first->second << std::endl;
					ins.first->second = (ins.first->second + d) * 0.5;
				}
			}
			d = pi->get_fuelflow();
			if (!std::isnan(d)) {
				std::pair<dmap_t::iterator,bool> ins(fmap.insert(dmap_t::value_type(pa, d)));
				if (!ins.second) {
					std::cerr << "Duplicate entry for PA " << pa << " new ff value " << d
						  << " previous " << ins.first->second << std::endl;
					ins.first->second = (ins.first->second + d) * 0.5;
				}
			}
			d = pi->get_cas();
			if (!std::isnan(d)) {
				std::pair<dmap_t::iterator,bool> ins(cmap.insert(dmap_t::value_type(pa, d)));
				if (!ins.second) {
					std::cerr << "Duplicate entry for PA " << pa << " new cas value " << d
						  << " previous " << ins.first->second << std::endl;
					ins.first->second = (ins.first->second + d) * 0.5;
				}
			}
		}
		for (dmap_t::const_iterator i(rmap.begin()), e(rmap.end()); i != e; ++i) {
			double pa1(1), pa(i->first);
			for (unsigned int i = 0; i < polyorder; ++i, pa1 *= pa)
				mr(ptrate, i) = pa1;
			vr(ptrate) = i->second;
			++ptrate;
		}
		for (dmap_t::const_iterator i(fmap.begin()), e(fmap.end()); i != e; ++i) {
			double pa1(1), pa(i->first);
			for (unsigned int i = 0; i < polyorder; ++i, pa1 *= pa)
				mf(ptfuel, i) = pa1;
			vf(ptfuel) = i->second;
			++ptfuel;
		}
		for (dmap_t::const_iterator i(cmap.begin()), e(cmap.end()); i != e; ++i) {
			double pa1(1), pa(i->first);
			for (unsigned int i = 0; i < polyorder; ++i, pa1 *= pa)
				mc(ptcas, i) = pa1;
			vc(ptcas) = i->second;
			++ptcas;
		}
	}
	bool ret(false);
	if (force || m_ratepoly.empty()) {
		m_ratepoly.clear();
		if (ptrate) {
			ret = true;
			//mr.conservativeResize(ptrate, std::min(ptrate, polyorder));
			//vr.conservativeResize(ptrate);
			mr = mr.topLeftCorner(ptrate, std::min(ptrate, polyorder)).eval();
			vr = vr.head(ptrate).eval();
			Eigen::VectorXd x(least_squares_solution(mr, vr));
			if (debuglog)
				std::cerr << "mr [ " << mr << " ]; vr [ " << vr << " ]" << std::endl;
			poly_t p(x.data(), x.data() + x.size());
			m_ratepoly.swap(p);
			m_ratepoly = m_ratepoly.simplify(0.1, 0.001, 1./50000);
		}
	}
	if (force || m_fuelflowpoly.empty()) {
		m_fuelflowpoly.clear();
		if (ptfuel) {
			ret = true;
			//mf.conservativeResize(ptfuel, std::min(ptfuel, polyorder));
			//vf.conservativeResize(ptfuel);
			mf = mf.topLeftCorner(ptfuel, std::min(ptfuel, polyorder)).eval();
			vf = vf.head(ptfuel).eval();
			Eigen::VectorXd x(least_squares_solution(mf, vf));
			if (debuglog)
				std::cerr << "mf [ " << mr << " ]; vf [ " << vr << " ]" << std::endl;
			poly_t p(x.data(), x.data() + x.size());
			m_fuelflowpoly.swap(p);
			m_fuelflowpoly = m_fuelflowpoly.simplify(0.01, 0.001, 1./50000);
		}
	}
	if (force || m_caspoly.empty()) {
		m_caspoly.clear();
		if (ptcas) {
			ret = true;
			//mc.conservativeResize(ptcas, std::min(ptcas, polyorder));
			//vc.conservativeResize(ptcas);
			mc = mc.topLeftCorner(ptcas, std::min(ptcas, polyorder)).eval();
			vc = vc.head(ptcas).eval();
			Eigen::VectorXd x(least_squares_solution(mc, vc));
			if (debuglog)
				std::cerr << "mc [ " << mc << " ]; vc [ " << vc << " ]" << std::endl;
			poly_t p(x.data(), x.data() + x.size());
			m_caspoly.swap(p);
			m_caspoly = m_caspoly.simplify(0.1, 0.001, 1./50000);
		}
	}
	if (debuglog)
		m_fuelflowpoly.print(m_caspoly.print(m_ratepoly.print(std::cerr << "recalculateratepoly: ratepoly ")
						     << std::endl << "recalculateratepoly: caspoly ") << std::endl
				     << "recalculateratepoly: fuelflowpoly ") << std::endl;
	return ret;
#else
	return false;
#endif
}

#ifdef __WIN32__
__attribute__((force_align_arg_pointer))
#endif
void Aircraft::ClimbDescent::recalculateclimbpoly(double pastart)
{
	static constexpr bool debuglog = false;
	// recompute climb profile polynomials
#ifdef HAVE_EIGEN3
	Poly1D<double> altv, distv, fuelv;
	double dist(0), fuel(0);
	AirData<float> ad;
	ad.set_qnh_temp(); // standard atmosphere
	ad.set_pressure_altitude(pastart);
	static constexpr unsigned int tinc = 1;
	if (debuglog)
		m_fuelflowpoly.print(m_caspoly.print(m_ratepoly.print(std::cerr << "recalculateclimbpoly: ratepoly ")
						     << std::endl << "recalculateclimbpoly: caspoly ") << std::endl
				     << "recalculateclimbpoly: fuelflowpoly ") << std::endl;
	{
		bool lastalt(false);
		for (unsigned int i = 0; ; i += tinc) {
			double cas(m_caspoly.eval(ad.get_pressure_altitude()));
			if (std::isnan(cas) || cas < 1)
				break;
			ad.set_cas(cas);
			altv.push_back(ad.get_pressure_altitude());
			distv.push_back(dist);
			fuelv.push_back(fuel);
			if (debuglog)
				std::cerr << "recalculateclimbpoly: t " << i << " PA " << ad.get_pressure_altitude()
					  << " dist " << dist << " fuel " << fuel << " CAS " << ad.get_cas() << " TAS " << ad.get_tas()
					  << " rate " << m_ratepoly.eval(ad.get_pressure_altitude()) << std::endl;
			// limit simulated climb to one hour
			if (lastalt || i >= static_cast<unsigned int>(maxtime))
				break;
			fuel += m_fuelflowpoly.eval(ad.get_pressure_altitude()) * (1.0 / 3600.0);
			dist += ad.get_tas() * (1.0 / 3600.0);
			{
				double altr(m_ratepoly.eval(ad.get_pressure_altitude()));
				if (altr <= 0 || std::isnan(altr))
					break;
				double altn(ad.get_pressure_altitude() + altr * (1.0 / 60.0));
				if (std::isnan(altn) || altn < pastart)
					break;
				if (altn > m_ceiling) {
					altn = m_ceiling;
					lastalt = true;
				}
				ad.set_pressure_altitude(altn);
			}
		}
	}
	if (!altv.empty()) {
		m_ceiling = altv.back();
		m_climbtime = altv.size() * tinc;
	}
	if (altv.size() > 10) {
		const bool force_origin = !pastart;
		{
			const unsigned int polyorder = 5 + !force_origin;
			Eigen::MatrixXd m(altv.size(), polyorder);
			Eigen::VectorXd v(altv.size());
			for (std::vector<double>::size_type i(0); i < altv.size(); ++i) {
				v(i) = altv[i];
				double t(1);
				if (force_origin)
					t *= i * tinc;
				for (unsigned int j = 0; j < polyorder; ++j, t *= i * tinc)
					m(i, j) = t;
			}
			Eigen::VectorXd x(least_squares_solution(m, v));
			poly_t p(x.data(), x.data() + x.size());
			if (force_origin)
				p.insert(p.begin(), 0);
			m_climbaltpoly.swap(p);
			m_climbaltpoly = m_climbaltpoly.simplify(0.1, 0.001, 1./3600);
		}
		{
			const unsigned int polyorder = 5 + !force_origin;
			Eigen::MatrixXd m(distv.size(), polyorder);
			Eigen::VectorXd v(distv.size());
			for (std::vector<double>::size_type i(0); i < distv.size(); ++i) {
				v(i) = distv[i];
				double t(1);
				if (force_origin)
					t *= i * tinc;
				for (unsigned int j = 0; j < polyorder; ++j, t *= i * tinc)
					m(i, j) = t;
			}
			Eigen::VectorXd x(least_squares_solution(m, v));
			poly_t p(x.data(), x.data() + x.size());
			if (force_origin)
				p.insert(p.begin(), 0);
			m_climbdistpoly.swap(p);
			m_climbdistpoly = m_climbdistpoly.simplify(0.01, 0.001, 1./3600);
		}
		{
			const unsigned int polyorder = 5 + !force_origin;
			Eigen::MatrixXd m(fuelv.size(), polyorder);
			Eigen::VectorXd v(fuelv.size());
			for (std::vector<double>::size_type i(0); i < fuelv.size(); ++i) {
				v(i) = fuelv[i];
				double t(1);
				if (force_origin)
					t *= i * tinc;
				for (unsigned int j = 0; j < polyorder; ++j, t *= i * tinc)
					m(i, j) = t;
			}
			Eigen::VectorXd x(least_squares_solution(m, v));
			poly_t p(x.data(), x.data() + x.size());
			if (force_origin)
				p.insert(p.begin(), 0);
			m_climbfuelpoly.swap(p);
			m_climbfuelpoly = m_climbfuelpoly.simplify(0.01, 0.001, 1./3600);
		}
		if (debuglog) {
			char filename[] = "/tmp/climbdescentXXXXXX";
			int fd(mkstemp(filename));
			if (fd != -1) {
				close(fd);
				std::ofstream os(filename);
				altv.save_octave(os, "altv");
				distv.save_octave(os, "distv");
				fuelv.save_octave(os, "fuelv");
				m_ratepoly.save_octave(os, "ratepoly");
				m_fuelflowpoly.save_octave(os, "fuelflowpoly");
				m_caspoly.save_octave(os, "caspoly");
				m_climbaltpoly.save_octave(os, "climbaltpoly");
				m_climbdistpoly.save_octave(os, "climbdistpoly");
				m_climbfuelpoly.save_octave(os, "climbfuelpoly");
				{
					Aircraft::Poly1D<double> p;
					p.push_back(m_climbtime);
					p.save_octave(os, "climbtime");
				}
				{
					Aircraft::Poly1D<double> p;
					p.push_back(m_ceiling);
					p.save_octave(os, "ceiling");
				}
				std::cerr << "recalculateclimbpoly: data saved to " << filename << std::endl;
			}
		}
	}
	// do not extrapolate climb
	{
		double maxpa(max_point_pa());
		if (!std::isnan(maxpa) && maxpa >= 1000 && maxpa < m_ceiling)
			m_ceiling = maxpa;
	}
	{
		double t(altitude_to_time(m_ceiling));
		if (!std::isnan(t) && t > 0 && t < m_climbtime)
			m_climbtime = t;
	}
	if (debuglog)
		m_climbfuelpoly.print(m_climbdistpoly.print(m_climbaltpoly.print(std::cerr << "recalculateclimbpoly: climbaltpoly ") << std::endl
							    << "recalculateclimbpoly: climbdistpoly ") << std::endl
				      << "recalculateclimbpoly: climbfuelpoly ") << std::endl;
#endif
}

namespace {
	class PointSorterTime {
	public:
		bool operator()(const Aircraft::ClimbDescent::Point& a, const Aircraft::ClimbDescent::Point& b) const {
			if (std::isnan(b.get_time()))
				return false;
			if (std::isnan(a.get_time()))
				return true;
			return a.get_time() < b.get_time();
		}
	};
};

#ifdef __WIN32__
__attribute__((force_align_arg_pointer))
#endif
bool Aircraft::ClimbDescent::recalculatetoclimbpoly(bool force)
{
	static constexpr bool debuglog = false;
	// recalculate climb profile polynomials
#ifdef HAVE_EIGEN3
	static const double enforce_points_altdist = 5000;
	if (!force && !m_climbaltpoly.empty() && !m_climbfuelpoly.empty() && !m_climbdistpoly.empty())
		return false;
	if (m_points.empty()) {
		m_climbaltpoly.clear();
		m_climbfuelpoly.clear();
		m_climbdistpoly.clear();
		return false;
	}
	points_t points(m_points);
	std::sort(points.begin(), points.end(), PointSorterTime());
	for (points_t::size_type i(0), n(points.size()); i < n; ) {
		Point& b(points[i]);
		if (std::isnan(b.get_time()) || std::isnan(b.get_pressurealt()) ||
		    std::isnan(b.get_fuel()) || std::isnan(b.get_dist()) ||
		    b.get_time() < 0) {
			points.erase(points.begin() + i);
			n = points.size();
			continue;
		}
		++i;
		static const Point nullpoint(0, 0, 0, 0);
		const Point& a((i < 2) ? nullpoint : points[i-2]);
		if (b.get_pressurealt() < a.get_pressurealt() + enforce_points_altdist)
			continue;
		unsigned int m(std::ceil((b.get_pressurealt() - a.get_pressurealt()) * (1.0 / enforce_points_altdist)));
		if (m < 2)
			continue;
		double t0(a.get_time()), td((b.get_time() - a.get_time()) / m);
		double a0(a.get_pressurealt()), ad((b.get_pressurealt() - a.get_pressurealt()) / m);
		double f0(a.get_fuel()), fd((b.get_fuel() - a.get_fuel()) / m);
		double d0(a.get_dist()), dd((b.get_dist() - a.get_dist()) / m);
		for (unsigned int j = 1; j < m; ++j, ++i)
			points.insert(points.begin() + (i - 1), Point(a0 + ad * j, t0 + td * j, f0 + fd * j, d0 + dd * j));
		n = points.size();
	}
	m_ceiling = 0;
	m_climbtime = 0;
	unsigned int polyorder(5);
	unsigned int ptalt(0);
	unsigned int ptfuel(0);
	unsigned int ptdist(0);
	Eigen::MatrixXd ma(points.size() + 1, polyorder);
	Eigen::MatrixXd mf(points.size() + 1, polyorder);
	Eigen::MatrixXd md(points.size() + 1, polyorder);
	Eigen::VectorXd va(points.size() + 1);
	Eigen::VectorXd vf(points.size() + 1);
	Eigen::VectorXd vd(points.size() + 1);
	{
		typedef std::map<double,double> dmap_t;
		dmap_t pmap, fmap, dmap;
		for (points_t::const_iterator pi(points.begin()), pe(points.end()); pi != pe; ++pi) {
			if (debuglog)
				std::cerr << "climbpoint: time " << pi->get_time() << " pa " << pi->get_pressurealt()
					  << " fuel " << pi->get_fuel() << " dist " << pi->get_dist() << std::endl;
			double tm(pi->get_time());
			if (std::isnan(tm))
				continue;
			tm = std::max(tm, 0.);
			m_climbtime = std::max(m_climbtime, tm);
			double d(pi->get_pressurealt());
			if (!std::isnan(d)) {
				std::pair<dmap_t::iterator,bool> ins(pmap.insert(dmap_t::value_type(tm, d)));
				if (!ins.second) {
					std::cerr << "Duplicate entry for time " << tm << " new PA value " << d
						  << " previous " << ins.first->second << std::endl;
					ins.first->second = (ins.first->second + d) * 0.5;
				}
			}
			d = pi->get_fuel();
			if (!std::isnan(d)) {
				std::pair<dmap_t::iterator,bool> ins(fmap.insert(dmap_t::value_type(tm, d)));
				if (!ins.second) {
					std::cerr << "Duplicate entry for time " << tm << " new fuel value " << d
						  << " previous " << ins.first->second << std::endl;
					ins.first->second = (ins.first->second + d) * 0.5;
				}
			}
			d = pi->get_dist();
			if (!std::isnan(d)) {
				std::pair<dmap_t::iterator,bool> ins(dmap.insert(dmap_t::value_type(tm, d)));
				if (!ins.second) {
					std::cerr << "Duplicate entry for time " << tm << " new dist value " << d
						  << " previous " << ins.first->second << std::endl;
					ins.first->second = (ins.first->second + d) * 0.5;
				}
			}
		}
		if (pmap.empty() || fabs(pmap.begin()->first) >= 1)
			pmap.insert(dmap_t::value_type(0, 0));
		if (fmap.empty() || fabs(fmap.begin()->first) >= 1)
			fmap.insert(dmap_t::value_type(0, 0));
		if (dmap.empty() || fabs(dmap.begin()->first) >= 1)
			dmap.insert(dmap_t::value_type(0, 0));
		for (dmap_t::const_iterator i(pmap.begin()), e(pmap.end()); i != e; ++i) {
			double tm1(1), tm(i->first);
			m_ceiling = std::max(m_ceiling, i->second);
			for (unsigned int i = 0; i < polyorder; ++i, tm1 *= tm)
				ma(ptalt, i) = tm1;
			va(ptalt) = i->second;
			++ptalt;
		}
		for (dmap_t::const_iterator i(fmap.begin()), e(fmap.end()); i != e; ++i) {
			double tm1(1), tm(i->first);
			for (unsigned int i = 0; i < polyorder; ++i, tm1 *= tm)
				mf(ptfuel, i) = tm1;
			vf(ptfuel) = i->second;
			++ptfuel;
		}
		for (dmap_t::const_iterator i(dmap.begin()), e(dmap.end()); i != e; ++i) {
			double tm1(1), tm(i->first);
			for (unsigned int i = 0; i < polyorder; ++i, tm1 *= tm)
				md(ptdist, i) = tm1;
			vd(ptdist) = i->second;
			++ptdist;
		}
	}
	bool ret(false);
	if (force || m_climbaltpoly.empty()) {
		m_climbaltpoly.clear();
		if (ptalt) {
			ret = true;
			//ma.conservativeResize(ptalt, std::min(ptalt, polyorder));
			//va.conservativeResize(ptalt);
			ma = ma.topLeftCorner(ptalt, std::min(ptalt, polyorder)).eval();
			va = va.head(ptalt).eval();
			Eigen::VectorXd x(least_squares_solution(ma, va));
			if (debuglog)
				std::cerr << "ma [ " << ma << " ]; va [ " << va << " ]" << std::endl;
			poly_t p(x.data(), x.data() + x.size());
			m_climbaltpoly.swap(p);
			m_climbaltpoly = m_climbaltpoly.simplify(0.1, 0.001, 1./3600);
		}
	}
	if (force || m_climbfuelpoly.empty()) {
		m_climbfuelpoly.clear();
		if (ptfuel) {
			ret = true;
			//mf.conservativeResize(ptfuel, std::min(ptfuel, polyorder));
			//vf.conservativeResize(ptfuel);
			mf = mf.topLeftCorner(ptfuel, std::min(ptfuel, polyorder)).eval();
			vf = vf.head(ptfuel).eval();
			Eigen::VectorXd x(least_squares_solution(mf, vf));
			if (debuglog)
				std::cerr << "mf [ " << mf << " ]; vf [ " << vf << " ]" << std::endl;
			poly_t p(x.data(), x.data() + x.size());
			m_climbfuelpoly.swap(p);
			m_climbfuelpoly = m_climbfuelpoly.simplify(0.01, 0.001, 1./3600);
		}
	}
	if (force || m_climbdistpoly.empty()) {
		m_climbdistpoly.clear();
		if (ptdist) {
			ret = true;
			//md.conservativeResize(ptdist, std::min(ptdist, polyorder));
			//vd.conservativeResize(ptdist);
			md = md.topLeftCorner(ptdist, std::min(ptdist, polyorder)).eval();
			vd = vd.head(ptdist).eval();
			Eigen::VectorXd x(least_squares_solution(md, vd));
			if (debuglog)
				std::cerr << "md [ " << md << " ]; vd [ " << vd << " ]" << std::endl;
			poly_t p(x.data(), x.data() + x.size());
			m_climbdistpoly.swap(p);
			m_climbdistpoly = m_climbdistpoly.simplify(0.01, 0.001, 1./3600);
		}
	}
	if (debuglog)
		m_climbfuelpoly.print(m_climbdistpoly.print(m_climbaltpoly.print(std::cerr << "recalculatetoclimbpoly: climbaltpoly ") << std::endl
							    << "recalculatetoclimbpoly: climbdistpoly ") << std::endl
				      << "recalculatetoclimbpoly: climbfuelpoly ") << std::endl;
	if (debuglog) {
		char filename[] = "/tmp/toclimbdescentXXXXXX";
		int fd(mkstemp(filename));
		if (fd != -1) {
			close(fd);
			std::ofstream os(filename);
			m_ratepoly.save_octave(os, "ratepoly");
			m_fuelflowpoly.save_octave(os, "fuelflowpoly");
			m_caspoly.save_octave(os, "caspoly");
			m_climbaltpoly.save_octave(os, "climbaltpoly");
			m_climbdistpoly.save_octave(os, "climbdistpoly");
			m_climbfuelpoly.save_octave(os, "climbfuelpoly");
			{
				Aircraft::Poly1D<double> p;
				p.push_back(m_climbtime);
				p.save_octave(os, "climbtime");
			}
			{
				Aircraft::Poly1D<double> p;
				p.push_back(m_ceiling);
				p.save_octave(os, "ceiling");
			}
			std::cerr << "recalculatetoclimbpoly: data saved to " << filename << std::endl;
		}
	}
	return ret;
#else
	return false;
#endif
}

#ifdef __WIN32__
__attribute__((force_align_arg_pointer))
#endif
void Aircraft::ClimbDescent::recalculatetoratepoly(void)
{
	// recompute rate polynomials
#ifdef HAVE_EIGEN3
	m_ratepoly.clear();
	m_fuelflowpoly.clear();
	m_caspoly.clear();
	unsigned int polyorder(4);
	unsigned int pt(0);
	const unsigned int nrpt(m_ceiling * 0.01 + 1);
	Eigen::MatrixXd m(nrpt, polyorder);
	Eigen::VectorXd vr(nrpt);
	Eigen::VectorXd vf(nrpt);
	Eigen::VectorXd vc(nrpt);
	AirData<float> ad;
	ad.set_qnh_temp(); // standard atmosphere
	ad.set_pressure_altitude(0);
	for (; pt < nrpt; ++pt) {
		double pa(pt * 100);
		ad.set_pressure_altitude(pa);
		if (pa > m_ceiling + 50)
			break;
		{
			double pa1(1);
			for (unsigned int i = 0; i < polyorder; ++i, pa1 *= pa)
				m(pt, i) = pa1;
		}
		double t(altitude_to_time(pa));
		vr(pt) = time_to_climbrate(t);
		vf(pt) = time_to_fuelflow(t);
		double tas(time_to_tas(t));
		if (tas > 1) {
			ad.set_tas(tas);
			vc(pt) = ad.get_cas();
		} else {
			vc(pt) = 1;
		}
		if (false)
			std::cerr << "recalculatetoratepoly: pa " << m(pt, 1) << " rate " << vr(pt) << " ff " << vf(pt)
				  << " cas " << vc(pt) << " tas " << tas << std::endl;
	}
	if (pt < 1)
		return;
	//m.conservativeResize(pt, std::min(pt, polyorder));
	m = m.topLeftCorner(pt, std::min(pt, polyorder)).eval();
	//vr.conservativeResize(pt);
	vr = vr.head(pt).eval();
	//vf.conservativeResize(pt);
	vf = vf.head(pt).eval();
	//vc.conservativeResize(pt);
	vc = vc.head(pt).eval();
	{
		Eigen::VectorXd x(least_squares_solution(m, vr));
		if (false)
			std::cerr << "m [ " << m << " ]; vr [ " << vr << " ]" << std::endl;
		poly_t p(x.data(), x.data() + x.size());
		m_ratepoly.swap(p);
		m_ratepoly = m_ratepoly.simplify(0.1, 0.001, 1./50000);
	}
	{
		Eigen::VectorXd x(least_squares_solution(m, vf));
		if (false)
			std::cerr << "m [ " << m << " ]; vf [ " << vr << " ]" << std::endl;
		poly_t p(x.data(), x.data() + x.size());
		m_fuelflowpoly.swap(p);
		m_fuelflowpoly = m_fuelflowpoly.simplify(0.01, 0.001, 1./50000);
	}
	{
		Eigen::VectorXd x(least_squares_solution(m, vc));
		if (false)
			std::cerr << "m [ " << m << " ]; vc [ " << vc << " ]" << std::endl;
		poly_t p(x.data(), x.data() + x.size());
		m_caspoly.swap(p);
		m_caspoly = m_caspoly.simplify(0.1, 0.001, 1./50000);
	}
	if (false)
		m_fuelflowpoly.print(m_caspoly.print(m_ratepoly.print(std::cerr << "recalculatetoratepoly: ratepoly ")
						     << std::endl << "recalculatetoratepoly: caspoly ") << std::endl
				     << "recalculatetoratepoly: fuelflowpoly ") << std::endl;
#endif
}

bool Aircraft::ClimbDescent::recalculatepoly(bool force)
{
	switch (get_mode()) {
	case mode_timetoaltitude:
		if (!recalculatetoclimbpoly(force))
			return false;
		recalculatetoratepoly();
		break;

	default:
		if (!recalculateratepoly(force))
			return false;
		recalculateclimbpoly(0);
		break;
	}
	return true;
}

void Aircraft::ClimbDescent::recalculateformass(double newmass)
{
	static constexpr bool debuglog = false;
	if (fabs(get_mass() - newmass) < 0.01)
		return;
	m_ratepoly *= get_mass() / newmass;
	m_ratepoly = m_ratepoly.simplify(0.1, 0.001, 1./50000);
	double oldceil(m_ceiling), oldmass(m_mass);
	m_mass = newmass;
	recalculateclimbpoly(time_to_altitude(0));
	if (debuglog)
		std::cerr << "recalculateformass: mass " << oldmass << " -> " << m_mass
			  << " ceil " << oldceil << " -> " << m_ceiling << std::endl;
	clear_points();
	//recalc_points();
	if (debuglog)
		print(std::cerr << "ClimbDescent::recalculateformass:" << std::endl, "  ");
}

#ifdef __WIN32__
__attribute__((force_align_arg_pointer))
#endif
void Aircraft::ClimbDescent::recalculateforatmo(double isaoffs, double qnh)
{
	static constexpr bool debuglog = false;
	// recompute climb alt & time polynomials
	if (fabs(qnh - IcaoAtmosphere<double>::std_sealevel_pressure) < 0.1 &&
	    fabs(isaoffs - m_isaoffset) < 0.1)
		return;
#ifdef HAVE_EIGEN3
	static constexpr int timestart = -60;
	static constexpr int timestep = 10;
	Poly1D<double> altv;
	Poly1D<int> timev;
	{
		IcaoAtmosphere<double> atmoo(IcaoAtmosphere<double>::std_sealevel_pressure, IcaoAtmosphere<double>::std_sealevel_temperature + m_isaoffset);
		IcaoAtmosphere<double> atmon(qnh, IcaoAtmosphere<double>::std_sealevel_temperature + isaoffs);
		for (int i = timestart; i <= m_climbtime; i += timestep) {
			double alt(time_to_altitude(i));
			if (alt > m_ceiling)
				break;
			double density(atmoo.altitude_to_density(alt * ::Point::ft_to_m_dbl));
			double altn(atmon.density_to_altitude(density) * ::Point::m_to_ft_dbl);
			altv.push_back(altn);
			timev.push_back(i);
			if (debuglog)
				std::cerr << "recalculateforatmo: time " << i << " alt old " << alt << " new " << altn << " density " << density << std::endl;
		}
		if (altv.size() < 10)
			return;
		double newceil(atmon.density_to_altitude(atmoo.altitude_to_density(m_ceiling)));
		if (debuglog)
			std::cerr << "recalculateforatmo: isaoffs " << (atmoo.get_temp() - IcaoAtmosphere<double>::std_sealevel_temperature)
				  << " -> " << (atmon.get_temp() - IcaoAtmosphere<double>::std_sealevel_temperature) << " qnh "
				  << atmoo.get_qnh() << " -> " << atmon.get_qnh() << " ceil " << m_ceiling << " -> " << newceil << std::endl;
		m_ceiling = newceil;
	}
	static constexpr bool force_origin = false;
	{
		static constexpr unsigned int polyorder = 5;
		Eigen::MatrixXd m(altv.size(), polyorder);
		Eigen::VectorXd v(altv.size());
		for (std::vector<double>::size_type i(0); i < altv.size(); ++i) {
			v(i) = altv[i];
			const double tinc(timev[i]);
			double t(1);
			if (force_origin)
				t *= tinc;
			for (unsigned int j = 0; j < polyorder; ++j, t *= tinc)
				m(i, j) = t;
		}
		Eigen::VectorXd x(least_squares_solution(m, v));
		poly_t p(x.data(), x.data() + x.size());
		if (force_origin)
			p.insert(p.begin(), 0);
		m_climbaltpoly.swap(p);
		if (debuglog)
			m_climbaltpoly.print(std::cerr << "recalculateforatmo: climbaltpoly (1): ") << std::endl;
		m_climbaltpoly = m_climbaltpoly.simplify(0.1, 0.001, 1./3600);
		if (debuglog)
			m_climbaltpoly.print(std::cerr << "recalculateforatmo: climbaltpoly: ") << std::endl;
	}
	validateceiling();
	m_isaoffset = isaoffs;
#endif
	clear_points();
	//recalc_points();
	recalculatetoratepoly();
	if (false)
		print(std::cerr << "ClimbDescent::recalculateforatmo:" << std::endl, "  ");
}

void Aircraft::ClimbDescent::validateceiling(void)
{
	static constexpr bool debuglog(false);
	static constexpr double mintas(1 / 3600.0);
	double oldclimbtime(m_climbtime);
	double oldceiling(m_ceiling);
	// check altitude poly (roc)
	ClimbDescent::poly_t climbaltpolyderiv(m_climbaltpoly.differentiate());
	if (climbaltpolyderiv.empty()) {
		m_climbtime = 0;
		print(std::cerr << "validateceiling: altitude derivative empty" << std::endl, "  ");
	} else {
		// require 100ft/min minimum
		climbaltpolyderiv[0] -= minroc;
		if (climbaltpolyderiv.eval(0) <= 0) {
			m_climbtime = 0;
			print(std::cerr << "validateceiling: climb performance at time 0 <= " << (minroc * 60) << "ft/min" << std::endl, "  ");
		} else if (climbaltpolyderiv.size() == 1) {
			m_climbtime = m_ceiling / climbaltpolyderiv[0];
			if (m_climbtime > maxtime)
				m_climbtime = maxtime;
			if (debuglog)
				climbaltpolyderiv.print(std::cerr << "validateceiling: climbaltpolyderiv: ") << std::endl;
		} else {
			double ct(2 * m_climbtime);
			if (std::isnan(ct) || ct < 1 || ct > maxtime)
				ct = maxtime;
			{
				std::vector<double> r(climbaltpolyderiv.real_roots());
				for (std::vector<double>::const_iterator i(r.begin()), e(r.end()); i != e; ++i) {
					if (std::isnan(*i) || *i <= 0)
						continue;
					if (debuglog)
						std::cerr << "validateceiling: altpoly derivative root " << *i << std::endl;
					if (*i < ct)
						ct = *i;
				}
			}
			m_climbtime = ct;
		}
	}
	// check distance poly (tas)
	ClimbDescent::poly_t climbdistpolyderiv(m_climbdistpoly.differentiate());
	if (climbdistpolyderiv.empty()) {
		m_climbtime = 0;
		print(std::cerr << "validateceiling: distance derivative empty" << std::endl, "  ");
	} else {
		// require 100ft/min minimum
		climbdistpolyderiv[0] -= mintas;
		if (climbdistpolyderiv.eval(0) <= 0) {
			m_climbtime = 0;
			print(std::cerr << "validateceiling: climb speed at time 0 <= " << (mintas * 3600.0) << "kts" << std::endl, "  ");
		} else if (climbdistpolyderiv.size() == 1) {
			double ct(m_ceiling / climbdistpolyderiv[0]);
			if (ct < m_climbtime)
				m_climbtime = ct;
			if (debuglog)
				climbdistpolyderiv.print(std::cerr << "validateceiling: climbdistpolyderiv: ") << std::endl;
		} else {
			double ct(m_climbtime);
			{
				std::vector<double> r(climbdistpolyderiv.real_roots());
				for (std::vector<double>::const_iterator i(r.begin()), e(r.end()); i != e; ++i) {
					if (std::isnan(*i) || *i <= 0)
						continue;
					if (debuglog)
						std::cerr << "validateceiling: distpoly derivative root " << *i << std::endl;
					if (*i < ct)
						ct = *i;
				}
			}
			m_climbtime = ct;
		}
	}
	{
		double newceil(m_climbaltpoly.eval(m_climbtime));
		if (newceil < m_ceiling)
			m_ceiling = newceil;
	}
	{
		double ct(m_climbaltpoly.boundedinveval(m_ceiling, 0, m_climbtime));
		if (!std::isnan(ct) && ct >= 0 && ct < m_climbtime)
			m_climbtime = ct;
	}
	if (debuglog)
		climbdistpolyderiv.print(climbaltpolyderiv.print(std::cerr << "validateceiling: climbaltpolyderiv: ")
					 << std::endl << "validateceiling: climbdistpolyderiv: ") << std::endl
					 << "validateceiling: ceil " << oldceiling << '/' << m_ceiling
					 << " climbtime " << oldclimbtime << '/' << m_climbtime << std::endl;
}

Aircraft::ClimbDescent Aircraft::ClimbDescent::interpolate(double t, const ClimbDescent& cd) const
{
	ClimbDescent r(*this);
	if (std::isnan(t))
		return r;
	t = std::max(std::min(t, 1.), 0.);
	r.m_ratepoly *= t;
	r.m_fuelflowpoly *= t;
	r.m_caspoly *= t;
	r.m_climbaltpoly *= t;
	r.m_climbdistpoly *= t;
	r.m_climbfuelpoly *= t;
	r.m_mass *= t;
	r.m_isaoffset *= t;
	r.m_ceiling *= t;
	r.m_climbtime *= t;
	t = 1 - t;
	r.m_ratepoly += cd.m_ratepoly * t;
	r.m_fuelflowpoly += cd.m_fuelflowpoly * t;
	r.m_caspoly += cd.m_caspoly * t;
	r.m_climbaltpoly += cd.m_climbaltpoly * t;
	r.m_climbdistpoly += cd.m_climbdistpoly * t;
	r.m_climbfuelpoly += cd.m_climbfuelpoly * t;
	r.m_mass += cd.m_mass * t;
	r.m_isaoffset += cd.m_isaoffset * t;
	r.m_ceiling += cd.m_ceiling * t;
	r.m_climbtime += cd.m_climbtime * t;
	r.clear_points();
	r.validateceiling();
	//r.recalc_points();
	return r;
}

void Aircraft::ClimbDescent::load_xml(const xmlpp::Element *el, double altfactor, double ratefactor, double fuelflowfactor, double casfactor,
				      double timefactor, double fuelfactor, double distfactor)
{
	if (!el)
		return;
	m_name.clear();
	m_mass = std::numeric_limits<double>::quiet_NaN();
	m_isaoffset = 0;
	m_ceiling = 10000;
	m_climbtime = maxtime;
	m_mode = mode_invalid;
	m_ratepoly.clear();
	m_fuelflowpoly.clear();
	m_caspoly.clear();
	m_climbaltpoly.clear();
	m_climbdistpoly.clear();
	m_climbfuelpoly.clear();
	m_points.clear();
	xmlpp::Attribute *attr;
	if ((attr = el->get_attribute("name")))
		m_name = attr->get_value();
	if ((attr = el->get_attribute("mass")) && attr->get_value() != "")
		m_mass = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("isaoffset")) && attr->get_value() != "")
		m_isaoffset = Glib::Ascii::strtod(attr->get_value());
	else if ((attr = el->get_attribute("isaoffs")) && attr->get_value() != "")
		m_isaoffset = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("ceiling")) && attr->get_value() != "")
		m_ceiling = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("climbtime")) && attr->get_value() != "")
		m_climbtime = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("mode"))) {
		if (attr->get_value() == "rateofdescent") {
			m_mode = mode_rateofclimb;
		} else if (attr->get_value() == "timefromaltitude") {
			m_mode = mode_timetoaltitude;
		} else {
			for (mode_t m(mode_rateofclimb); m < mode_invalid; m = (mode_t)(m + 1)) {
				if (to_str(m) != attr->get_value())
					continue;
				m_mode = m;
				break;
			}
		}
	}
	{
		xmlpp::Node::NodeList nl(el->get_children("point"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni) {
			m_points.push_back(Point());
			m_points.back().load_xml(static_cast<xmlpp::Element *>(*ni), m_mode, altfactor, ratefactor, fuelflowfactor, casfactor,
						 timefactor, fuelfactor, distfactor);
		}
	}
	{
		xmlpp::Node::NodeList nl(el->get_children("ratepoly"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni)
			m_ratepoly.load_xml(static_cast<xmlpp::Element *>(*ni));
 	}
	{
		xmlpp::Node::NodeList nl(el->get_children("fuelflowpoly"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni)
			m_fuelflowpoly.load_xml(static_cast<xmlpp::Element *>(*ni));
	}
	{
		xmlpp::Node::NodeList nl(el->get_children("caspoly"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni)
			m_caspoly.load_xml(static_cast<xmlpp::Element *>(*ni));
	}
	{
		xmlpp::Node::NodeList nl(el->get_children("climbaltpoly"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni)
			m_climbaltpoly.load_xml(static_cast<xmlpp::Element *>(*ni));
	}
	{
		xmlpp::Node::NodeList nl(el->get_children("climbdistpoly"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni)
			m_climbdistpoly.load_xml(static_cast<xmlpp::Element *>(*ni));
	}
	{
		xmlpp::Node::NodeList nl(el->get_children("climbfuelpoly"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni)
			m_climbfuelpoly.load_xml(static_cast<xmlpp::Element *>(*ni));
	}
}

void Aircraft::ClimbDescent::save_xml(xmlpp::Element *el, double altfactor, double ratefactor, double fuelflowfactor, double casfactor,
				      double timefactor, double fuelfactor, double distfactor) const
{
	if (!el)
		return;
	if (m_name.empty())
		el->remove_attribute("name");
        else
		el->set_attribute("name", m_name);
	if (std::isnan(m_mass)) {
		el->remove_attribute("mass");
	} else {
		std::ostringstream oss;
		oss << m_mass;
		el->set_attribute("mass", oss.str());
	}
	if (m_isaoffset == 0) {
		el->remove_attribute("isaoffset");
	} else {
		std::ostringstream oss;
		oss << m_isaoffset;
		el->set_attribute("isaoffset", oss.str());
	}
	if (m_ceiling == 1) {
		el->remove_attribute("ceiling");
	} else {
		std::ostringstream oss;
		oss << m_ceiling;
		el->set_attribute("ceiling", oss.str());
	}
	if (m_climbtime == 1) {
		el->remove_attribute("climbtime");
	} else {
		std::ostringstream oss;
		oss << m_climbtime;
		el->set_attribute("climbtime", oss.str());
	}
	if (m_mode == mode_invalid)
		el->remove_attribute("mode");
	else
		el->set_attribute("mode", to_str(m_mode));
	for (points_t::const_iterator di(m_points.begin()), de(m_points.end()); di != de; ++di)
		di->save_xml(el->add_child("point"), m_mode, altfactor, ratefactor, fuelflowfactor, casfactor,
			     timefactor, fuelfactor, distfactor);
	{
		xmlpp::Node::NodeList nl(el->get_children("ratepoly"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni)
			el->remove_child(*ni);
	}
	if (!m_ratepoly.empty())
		m_ratepoly.save_xml(el->add_child("ratepoly"));
	{
		xmlpp::Node::NodeList nl(el->get_children("fuelflowpoly"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni)
			el->remove_child(*ni);
	}
	if (!m_fuelflowpoly.empty())
		m_fuelflowpoly.save_xml(el->add_child("fuelflowpoly"));
	{
		xmlpp::Node::NodeList nl(el->get_children("caspoly"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni)
			el->remove_child(*ni);
	}
	if (!m_caspoly.empty())
		m_caspoly.save_xml(el->add_child("caspoly"));
	{
		xmlpp::Node::NodeList nl(el->get_children("climbtimepoly"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni)
			el->remove_child(*ni);
	}
	{
		xmlpp::Node::NodeList nl(el->get_children("climbaltpoly"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni)
			el->remove_child(*ni);
	}
	if (!m_climbaltpoly.empty())
		m_climbaltpoly.save_xml(el->add_child("climbaltpoly"));
 	{
		xmlpp::Node::NodeList nl(el->get_children("climbdistpoly"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni)
			el->remove_child(*ni);
	}
	if (!m_climbdistpoly.empty())
		m_climbdistpoly.save_xml(el->add_child("climbdistpoly"));
	{
		xmlpp::Node::NodeList nl(el->get_children("climbfuelpoly"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni)
			el->remove_child(*ni);
	}
	if (!m_climbfuelpoly.empty())
		m_climbfuelpoly.save_xml(el->add_child("climbfuelpoly"));
}

#ifdef HAVE_JSONCPP

bool Aircraft::ClimbDescent::load_garminpilot(const Json::Value& root, const Aircraft& acft, unsigned int version)
{
	if (root.isMember("fuelMeasureType")) {
		const Json::Value& fmt(root["fuelMeasureType"]);
		if (fmt.isString()) {
			if (fmt.asString() == "volume") {
				// ??
			} else if (fmt.asString() == "weight") {
				// ??
			}
		}
	}
	if (root.isMember("samples")) {
		const Json::Value& samp(root["samples"]);
		if (samp.isArray()) {
			for (Json::ArrayIndex i = 0; i < samp.size(); ++i) {
				const Json::Value& samp1(samp[i]);
				if (!samp1.isObject())
					continue;
				double pa(std::numeric_limits<double>::quiet_NaN());
				double rate(std::numeric_limits<double>::quiet_NaN());
				double fuelflow(std::numeric_limits<double>::quiet_NaN());
				double cas(std::numeric_limits<double>::quiet_NaN());
				if (samp1.isMember("iasInput")) {
					const Json::Value& x(root["iasInput"]);
					if (x.isNumeric())
						cas = x.asDouble();
				}
				if (samp1.isMember("outputRate")) {
					const Json::Value& x(root["outputRate"]);
					if (x.isNumeric())
						rate = x.asDouble();
				}
				if (samp1.isMember("fuelFlow")) {
					const Json::Value& x(root["fuelFlow"]);
					if (x.isNumeric())
						fuelflow = acft.convert_fuel(unit_usgal, acft.get_fuelunit(), x.asDouble());
				}
				if (samp1.isMember("altitude")) {
					const Json::Value& x(root["altitude"]);
					if (x.isNumeric())
						pa = x.asDouble();
				}
				if (!std::isnan(pa) && !std::isnan(rate))
					m_points.push_back(Point(pa, rate, fuelflow, cas));
			}
		}
	}
	m_mode = mode_rateofclimb;
	return !m_points.empty();
}

void Aircraft::ClimbDescent::save_garminpilot(Json::Value& root, const Aircraft& acft, unsigned int version)
{
	{
		Json::Value& pdu(root["parameterDisplayUnits"]);
		pdu["temperature"] = "Degrees Celsius";
	}
	root["tempType"] = "isa";
	{
		Json::Value& pd(root["parameterDefaults"]);
		pd = Json::Value(Json::objectValue);
	}
	{
		Json::Value& par(root["inputParameters"]);
		par.append(Json::Value("altitude"));
		par.append(Json::Value("iasInput"));
	}
	root["powerSettingType"] = "percentPower";
	Json::Value& samp(root["samples"]);
	samp = Json::Value(Json::arrayValue);
	double ceil(get_ceiling());
	if (ceil < 0)
		return;
	for (double pa = 0;; ) {
		double rate(std::numeric_limits<double>::quiet_NaN());
		double fuelflow(std::numeric_limits<double>::quiet_NaN());
		double cas(std::numeric_limits<double>::quiet_NaN());
		calculate(rate, fuelflow, cas, pa);
		Json::Value x;
		x["iasInput"] = cas;
		x["outputRate"] = rate;
		x["fuelFlow"] = acft.convert_fuel(acft.get_fuelunit(), unit_usgal, fuelflow);
		x["altitude"] = pa;
		samp.append(x);
		if (pa >= ceil)
			break;
		pa += 2000;
		if (pa > ceil)
			pa = ceil;
	}
}

#endif

void Aircraft::ClimbDescent::add_point(const Point& pt)
{
	m_points.push_back(pt);
}

void Aircraft::ClimbDescent::clear_points(void)
{
	m_points.clear();
}

void Aircraft::ClimbDescent::recalc_points(mode_t m)
{
	clear_points();
	switch (m) {
	case mode_timetoaltitude:
		m_mode = mode_timetoaltitude;
		if (std::isnan(get_ceiling()) || get_ceiling() <= 0 || get_ceiling() >= 100000)
			break;
		{
			for (double alt = 0;; alt += 500) {
				bool last(alt >= get_ceiling());
				if (last)
					alt = get_ceiling();
				double t(altitude_to_time(alt));
				if (t >= 0 && t <= get_climbtime())
					m_points.push_back(Point(alt, t, time_to_fuel(t), time_to_distance(t)));
				if (last)
					break;
			}
		}
		break;

	default:
		m_mode = mode_rateofclimb;
		if (std::isnan(get_ceiling()) || get_ceiling() <= 0 || get_ceiling() >= 100000)
			break;
		{
			for (double alt = 0;; alt += 500) {
				bool last(alt >= get_ceiling());
				if (last)
					alt = get_ceiling();
				double rate, fuelflow, cas;
				calculate(rate, fuelflow, cas, alt);
				m_points.push_back(Point(alt, rate, fuelflow, cas));
				if (last)
					break;
			}
		}
		break;
	}
}

void Aircraft::ClimbDescent::limit_ceiling(double lim)
{
	if (std::isnan(lim))
		return;
	lim = std::max(0., lim);
	m_ceiling = std::min(m_ceiling, lim);
}

void Aircraft::ClimbDescent::set_ceiling(double ceil)
{
	if (std::isnan(ceil) || ceil < 0)
		return;
	m_ceiling = ceil;
}

double Aircraft::ClimbDescent::max_point_pa(void) const
{
	double r(std::numeric_limits<double>::min());
	for (points_t::const_iterator pi(m_points.begin()), pe(m_points.end()); pi != pe; ++pi) {
		double pa(pi->get_pressurealt());
		if (std::isnan(pa))
			continue;
		r = std::max(r, pa);
	}
	return r;
}

void Aircraft::ClimbDescent::set_point_vy(double vy)
{
	if (std::isnan(vy) || vy <= 0 || m_mode != mode_rateofclimb)
		return;
	for (points_t::iterator pi(m_points.begin()), pe(m_points.end()); pi != pe; ++pi) {
		if (!std::isnan(pi->get_cas()) && pi->get_cas() > 0)
			continue;
		pi->set_cas(vy);
	}
}

void Aircraft::ClimbDescent::set_mass(double mass)
{
	if (std::isnan(mass) || mass <= 0)
		return;
	m_mass = mass;
}

void Aircraft::ClimbDescent::set_descent_rate(double rate)
{
	m_ratepoly.clear();
	m_fuelflowpoly.clear();
	m_caspoly.clear();
	m_climbaltpoly.clear();
	m_climbdistpoly.clear();
	m_climbfuelpoly.clear();
	m_points.clear();
	m_name.clear();
	m_mass = std::numeric_limits<double>::quiet_NaN();
	m_isaoffset = 0;
	m_ceiling = 18000;
	m_climbtime = maxtime;
	m_mode = mode_rateofclimb;
	m_ratepoly.push_back(rate);
	m_fuelflowpoly.push_back(0);
	m_caspoly.push_back(0);
}

double Aircraft::ClimbDescent::get_constant_cas(void) const
{
	if (get_caspoly().size() != 1)
		return std::numeric_limits<double>::quiet_NaN();
	return get_caspoly().front();
}

double Aircraft::ClimbDescent::get_constant_rate(void) const
{
	if (get_ratepoly().size() != 1)
		return std::numeric_limits<double>::quiet_NaN();
	return get_ratepoly().front();
}

double Aircraft::ClimbDescent::get_constant_slope(void) const
{
	if (std::isnan(get_ceiling()) || get_ceiling() <= 0 || get_ceiling() > 1000000)
		return std::numeric_limits<double>::quiet_NaN();
	const double knots_to_ftpm = 1.e3 / ::Point::km_to_nmi_dbl * ::Point::m_to_ft_dbl / 60.0;
	const double tolerance = 0.05;
	std::vector<double> ratios;
	for (double alt = 0;; alt += 1000) {
		bool last(alt >= get_ceiling());
		if (last)
			alt = get_ceiling();
		AirData<double> ad;
		ad.set_qnh_tempoffs(IcaoAtmosphere<double>::std_sealevel_pressure, get_isaoffset());
		ad.set_pressure_altitude(alt);
		ad.set_cas(get_caspoly().eval(alt));
		double ratio(ad.get_tas() / get_ratepoly().eval(alt));
		if (std::isnan(ratio) || ratio <= 0.0)
			return std::numeric_limits<double>::quiet_NaN();
		ratios.push_back(ratio);
		if (last)
			break;
	}
	if (ratios.size() < 1)
		return std::numeric_limits<double>::quiet_NaN();
	std::sort(ratios.begin(), ratios.end());
	double s(ratios[ratios.size() / 2]);
	if (!(ratios.size() & 1)) {
		s += ratios[ratios.size() / 2 - 1];
		s *= 0.5;
	}
	if (ratios[0] < s * (1 - tolerance) || ratios[ratios.size() - 1] > s * (1 + tolerance))
		return std::numeric_limits<double>::quiet_NaN();
	return s * knots_to_ftpm;
}

Aircraft::CheckErrors Aircraft::ClimbDescent::check(double minmass, double maxmass, double vstall, bool descent) const
{
	CheckErrors r;
	if (get_mass() < minmass * 0.9)
		r.add(*this, descent, CheckError::severity_warning) << "mass less than empty mass - 10%";
	if (get_mass() > maxmass)
		r.add(*this, descent, CheckError::severity_warning) << "mass greater than maximum takeoff mass";
	if (std::isnan(get_ceiling()) || get_ceiling() <= 0) {
		r.add(*this, descent, CheckError::severity_error) << "invalid ceiling";
		return r;
	}
	if (std::isnan(get_climbtime()) || get_climbtime() <= 0) {
		r.add(*this, descent, CheckError::severity_error) << "invalid climb time";
		return r;
	}
	if (get_ceiling() < 7000)
		r.add(*this, descent, CheckError::severity_error) << "unrealistically low ceiling";
	if (get_climbtime() < 90)
		r.add(*this, descent, CheckError::severity_error) << "unrealistically low climb time";
	int tratemin(std::numeric_limits<int>::max()), tratemax(std::numeric_limits<int>::min());
	int taltmin(tratemin), taltmax(tratemax), tdistmin(tratemin), tdistmax(tratemax), tfuelmin(tratemin), tfuelmax(tratemax);
	int ttamin(tratemin), ttamax(tratemax), ttdmin(tratemin), ttdmax(tratemax);
	int ttasmin(tratemin), ttasmax(tratemax), ttasvsmin(tratemin), ttasvsmax(tratemax);
	double tadiff(0), tddiff(0), aadiff(0), addiff(0);
	double tas0(get_climbdistpoly().evalderiv(0) * 3600.0), tasmin(tas0), tasmax(tas0);
	if (std::isnan(vstall) || vstall < 10.0)
		vstall = 10.0;
	AirData<double> ad;
	ad.set_qnh_tempoffs(IcaoAtmosphere<double>::std_sealevel_pressure, get_isaoffset());
	ad.set_pressure_altitude(0);
	ad.set_cas(vstall);
	if (false)
		std::cerr << "Sealevel TAS " << tas0 << " VS " << ad.get_tas() << " IAS " << ad.get_cas() << std::endl;
	if (std::isnan(tas0) || tas0 < ad.get_tas())
		r.add(*this, descent, CheckError::severity_error) << "sea level TAS excessively low (<" << std::fixed
								  << std::setprecision(0) << ad.get_tas()
								  << "kts vstall): " << std::setprecision(1) << tas0;
	for (int t = 0; t <= get_climbtime(); t += 10) {
		if (get_ratepoly().eval(t) < 0) {
			tratemin = std::min(tratemin, t);
			tratemax = std::max(tratemax, t);
		}
		if (get_climbaltpoly().evalderiv(t) < 0) {
			taltmin = std::min(taltmin, t);
			taltmax = std::max(taltmax, t);
		}
		ad.set_pressure_altitude(get_climbaltpoly().eval(t));
		ad.set_cas(vstall);
		{
			double tas(get_climbdistpoly().evalderiv(t) * 3600.0);
			if (tas < 0) {
				tdistmin = std::min(tdistmin, t);
				tdistmax = std::max(tdistmax, t);
			}
			if (tas < tas0 * 0.25) {
				ttasmin = std::min(ttasmin, t);
				ttasmax = std::max(ttasmax, t);
			}
			if (tas < ad.get_tas()) {
				ttasvsmin = std::min(ttasvsmin, t);
				ttasvsmax = std::max(ttasvsmax, t);
			}
			tasmin = std::min(tasmin, tas);
			tasmax = std::max(tasmax, tas);
			
		}
		if (get_climbfuelpoly().evalderiv(t) < 0) {
			tfuelmin = std::min(tfuelmin, t);
			tfuelmax = std::max(tfuelmax, t);
		}
                double t0(altitude_to_time(time_to_altitude(t)) - t);
		if (fabs(t0) > 30) {
			double a0(time_to_altitude(t0 + t) - time_to_altitude(t));
			if (fabs(a0) > 100) {
				ttamin = std::min(ttamin, t);
				ttamax = std::max(ttamax, t);
				if (fabs(t0) > fabs(tadiff))
					tadiff = t0;
				if (fabs(a0) > fabs(aadiff))
					aadiff = a0;
			}
		}
		t0 = distance_to_time(time_to_distance(t)) - t;
		if (fabs(t0) > 30) {
			double a0(time_to_altitude(t0 + t) - time_to_altitude(t));
			if (fabs(a0) > 100) {
				ttdmin = std::min(ttdmin, t);
				ttdmax = std::max(ttdmax, t);
				if (fabs(t0) > fabs(tddiff))
					tddiff = t0;
				if (fabs(a0) > fabs(addiff))
					addiff = a0;
			}
		}
	}
	static const char * const climbdescentstr[2] = { "climb", "descent" };
	if (tratemin <= tratemax)
		r.add(*this, descent, CheckError::severity_error, tratemin, tratemax) << "rate of "
										      << climbdescentstr[descent] << " less than zero";
	if (taltmin <= taltmax)
		r.add(*this, descent, CheckError::severity_error, taltmin, taltmax) << "altitude derivative (rate of "
										    << climbdescentstr[descent] << ") less than zero";
	if (tdistmin <= tdistmax)
		r.add(*this, descent, CheckError::severity_error, tdistmin, tdistmax) << "distance derivative (TAS) less than zero (distance goes backwards)";
	if (ttasmin <= ttasmax)
		r.add(*this, descent, CheckError::severity_error, ttasmin, ttasmax) << "excessive TAS variance (TAS less than one forth of maximum): " << std::fixed
										    << std::setprecision(1) << tasmin << "..."
										    << std::setprecision(1) << tas0 << "..."
										    << std::setprecision(1) << tasmax << "kts";
	if (ttasvsmin <= ttasvsmax) {
		ad.set_pressure_altitude(get_climbaltpoly().eval(ttasvsmin));
		ad.set_cas(vstall);
		double tas0(get_climbdistpoly().evalderiv(ttasvsmin) * 3600.0), vs0(ad.get_tas());
		ad.set_pressure_altitude(get_climbaltpoly().eval(ttasvsmax));
		ad.set_cas(vstall);
		double tas1(get_climbdistpoly().evalderiv(ttasvsmax) * 3600.0), vs1(ad.get_tas());
		r.add(*this, descent, CheckError::severity_error, ttasvsmin, ttasvsmax) << "TAS lower than stall speed: " << std::fixed
											<< std::setprecision(1) << tas0 << '/'
											<< std::setprecision(1) << tas1 << " < "
											<< std::setprecision(1) << vs0 << '/'
											<< std::setprecision(1) << vs1 << "kts";
	}
	if (tfuelmin <= tfuelmax)
		r.add(*this, descent, CheckError::severity_error, tfuelmin, tfuelmax) << "fuel derivative less than zero (negative fuel flow)";
	if (ttamin <= ttamax)
		r.add(*this, descent, CheckError::severity_error, ttamin, ttamax) << "excessive time/altitude forward/backward error: time error "
										  << tadiff << " altitude error " << aadiff;
	if (ttdmin <= ttdmax)
		r.add(*this, descent, CheckError::severity_error, ttdmin, ttdmax) << "excessive time/distance forward/backward error: time error "
										  << tddiff << " altitude error " << addiff;
	if (false && ttamin <= ttamax)
		m_climbaltpoly.print(std::cerr << "ClimbDescent name " << get_name() << " mass " << get_mass()
				     << " isaoffs " << get_isaoffset() << std::endl << "  climbaltpoly ") << std::endl;
	return r;
}

std::ostream& Aircraft::ClimbDescent::print(std::ostream& os, const std::string& indent) const
{
	os << indent << "name = \"" << get_name() << "\"" << std::endl
	   << indent << "mode = \"" << to_str(get_mode()) << "\"" << std::endl
	   << indent << "mass = " << get_mass() << std::endl
	   << indent << "isaoffset = " << get_isaoffset() << std::endl
	   << indent << "ceiling = " << get_ceiling() << std::endl
	   << indent << "climbtime = " << get_climbtime() << std::endl;
	get_ratepoly().print(os << indent << "ratepoly = [") << "]" << std::endl;
	get_fuelflowpoly().print(os << indent << "fuelflowpoly = [") << "]" << std::endl;
	get_caspoly().print(os << indent << "caspoly = [") << "]" << std::endl;
	get_climbaltpoly().print(os << indent << "climbaltpoly = [") << "]" << std::endl;
	get_climbdistpoly().print(os << indent << "climbdistpoly = [") << "]" << std::endl;
	get_climbfuelpoly().print(os << indent << "climbfuelpoly = [") << "]" << std::endl;
	for (points_t::const_iterator i(m_points.begin()), e(m_points.end()); i != e; ++i)
		i->print(os << indent << "  ", get_mode()) << std::endl;
	return os;
}

Aircraft::Climb::Climb(double altfactor, double ratefactor, double fuelflowfactor, double casfactor,
		       double timefactor, double fuelfactor, double distfactor)
	: m_altfactor(altfactor), m_ratefactor(ratefactor), m_fuelflowfactor(fuelflowfactor), m_casfactor(casfactor),
	  m_timefactor(timefactor), m_fuelfactor(fuelfactor), m_distfactor(distfactor)
{
	add(ClimbDescent("", 2600));
}

Aircraft::ClimbDescent Aircraft::Climb::calculate(const std::string& name, double mass, double isaoffs, double qnh) const
{
	curves_t::const_iterator ci(m_curves.find(name));
	if (ci == m_curves.end()) {
		ci = m_curves.begin();
		if (ci == m_curves.end())
			return ClimbDescent("", 2600);
	}
	return calculate(ci->second, mass, isaoffs, qnh);
}

Aircraft::ClimbDescent Aircraft::Climb::calculate(std::string& name, const std::string& dfltname, double mass, double isaoffs, double qnh) const
{
	curves_t::const_iterator ci(m_curves.find(name));
	if (ci == m_curves.end()) {
		ci = m_curves.find(dfltname);
		if (ci == m_curves.end()) {
			ci = m_curves.begin();
			if (ci == m_curves.end())
				return ClimbDescent("", 2600);
		} else {
			name = dfltname;
		}
	}
	return calculate(ci->second, mass, isaoffs, qnh);
}

Aircraft::ClimbDescent Aircraft::Climb::calculate(const massmap_t& ci, double mass, double isaoffs, double qnh) const
{
	massmap_t::const_iterator miu(ci.lower_bound(mass));
	if (miu == ci.end()) {
		if (miu == ci.begin())
			return ClimbDescent("", 2600);
		--miu;
		if (miu != ci.begin())
			return calculate(miu->second, isaoffs, qnh);
		ClimbDescent r(calculate(miu->second, isaoffs, qnh));
		r.recalculateformass(mass);
		return r;
	}
	massmap_t::const_iterator mil(miu);
	if (miu == ci.begin()) {
		++miu;
		if (miu == ci.end()) {
			ClimbDescent r(calculate(mil->second, isaoffs, qnh));
			r.recalculateformass(mass);
			return r;
		}
	} else {
		--mil;
	}
	double t((mass - mil->first) / (miu->first - mil->first));
	if (t <= 0)
		return calculate(mil->second, isaoffs, qnh);
	if (t >= 1)
		return calculate(miu->second, isaoffs, qnh);
	return calculate(miu->second, isaoffs, qnh).interpolate(t, calculate(mil->second, isaoffs, qnh));
}

Aircraft::ClimbDescent Aircraft::Climb::calculate(const isamap_t& mi, double isaoffs, double qnh) const
{
	isamap_t::const_iterator iiu(mi.lower_bound(isaoffs));
	if (iiu == mi.end()) {
		if (iiu == mi.begin())
			return ClimbDescent("", 2600);
		--iiu;
		if (iiu != mi.begin())
			return iiu->second;
		ClimbDescent r(iiu->second);
		r.recalculateforatmo(isaoffs, qnh);
		return r;
	}
	isamap_t::const_iterator iil(iiu);
	if (iiu == mi.begin()) {
		++iiu;
		if (iiu == mi.end()) {
			ClimbDescent r(iil->second);
			r.recalculateforatmo(isaoffs, qnh);
			return r;
		}
	} else {
		--iil;
	}
	double t((isaoffs - iil->first) / (iiu->first - iil->first));
	if (t <= 0)
		return iil->second;
	if (t >= 1)
		return iiu->second;
	return iiu->second.interpolate(t, iil->second);
}

unsigned int Aircraft::Climb::count_curves(void) const
{
	unsigned int ret(0);
	for (curves_t::const_iterator ci(m_curves.begin()), ce(m_curves.end()); ci != ce; ++ci)
		for (massmap_t::const_iterator mi(ci->second.begin()), me(ci->second.end()); mi != me; ++mi)
			ret += mi->second.size();
	return ret;
}

std::set<std::string> Aircraft::Climb::get_curves(void) const
{
	std::set<std::string> r;
	for (curves_t::const_iterator i(m_curves.begin()), e(m_curves.end()); i != e; ++i)
		r.insert(i->first);
	return r;
}

double Aircraft::Climb::get_ceiling(void) const
{
	double c(0);
	for (curves_t::const_iterator ci(m_curves.begin()), ce(m_curves.end()); ci != ce; ++ci)
		for (massmap_t::const_iterator mi(ci->second.begin()), me(ci->second.end()); mi != me; ++mi)
			for (isamap_t::const_iterator ii(mi->second.begin()), ie(mi->second.end()); ii != ie; ++ii)
				c = std::max(c, ii->second.get_ceiling());
	return c;
}

void Aircraft::Climb::load_xml(const xmlpp::Element *el)
{
	if (!el)
		return;
	m_curves.clear();
	m_remark.clear();
	m_altfactor = 1;
	m_ratefactor = 1;
	m_fuelflowfactor = 1;
	m_casfactor = 1;
	m_timefactor = 1;
	m_fuelfactor = 1;
	m_distfactor = 1;
	xmlpp::Attribute *attr;
	if ((attr = el->get_attribute("altfactor")) && attr->get_value() != "")
		m_altfactor = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("ratefactor")) && attr->get_value() != "")
		m_ratefactor = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("fuelfactor")) && attr->get_value() != "")
		m_fuelflowfactor = m_fuelfactor = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("fuelflowfactor")) && attr->get_value() != "")
		m_fuelflowfactor = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("casfactor")) && attr->get_value() != "")
		m_casfactor = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("timefactor")) && attr->get_value() != "")
		m_timefactor = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("distfactor")) && attr->get_value() != "")
		m_distfactor = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("remark")))
		m_remark = attr->get_value();
	{
		xmlpp::Node::NodeList nl(el->get_children("curve"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni) {
			ClimbDescent cd;
			cd.load_xml(static_cast<xmlpp::Element *>(*ni), m_altfactor, m_ratefactor, m_fuelflowfactor, m_casfactor,
				    m_timefactor, m_fuelfactor, m_distfactor);
			add(cd);
		}
		if (nl.empty()) {
			ClimbDescent cd;
			cd.load_xml(el, m_altfactor, m_ratefactor, m_fuelflowfactor, m_casfactor,
				    m_timefactor, m_fuelfactor, m_distfactor);
			if (cd.has_points()) {
				if (cd.get_name().empty())
					cd.set_name("Normal");
				add(cd);
			}
		}
	}
}

void Aircraft::Climb::save_xml(xmlpp::Element *el) const
{
	if (m_altfactor == 1) {
		el->remove_attribute("altfactor");
	} else {
		std::ostringstream oss;
		oss << m_altfactor;
		el->set_attribute("altfactor", oss.str());
	}
	if (m_ratefactor == 1) {
		el->remove_attribute("ratefactor");
	} else {
		std::ostringstream oss;
		oss << m_ratefactor;
		el->set_attribute("ratefactor", oss.str());
	}
	if (m_fuelflowfactor == 1) {
		el->remove_attribute("fuelflowfactor");
	} else {
		std::ostringstream oss;
		oss << m_fuelflowfactor;
		el->set_attribute("fuelflowfactor", oss.str());
	}
	if (m_casfactor == 1) {
		el->remove_attribute("casfactor");
	} else {
		std::ostringstream oss;
		oss << m_casfactor;
		el->set_attribute("casfactor", oss.str());
	}
	if (m_timefactor == 1) {
		el->remove_attribute("timefactor");
	} else {
		std::ostringstream oss;
		oss << m_timefactor;
		el->set_attribute("timefactor", oss.str());
	}
	if (m_fuelfactor == 1) {
		el->remove_attribute("fuelfactor");
	} else {
		std::ostringstream oss;
		oss << m_fuelfactor;
		el->set_attribute("fuelfactor", oss.str());
	}
	if (m_distfactor == 1) {
		el->remove_attribute("distfactor");
	} else {
		std::ostringstream oss;
		oss << m_distfactor;
		el->set_attribute("distfactor", oss.str());
	}
	if (m_remark.empty())
		el->remove_attribute("remark");
	else
		el->set_attribute("remark", m_remark);
	for (curves_t::const_iterator ci(m_curves.begin()), ce(m_curves.end()); ci != ce; ++ci)
		for (massmap_t::const_iterator mi(ci->second.begin()), me(ci->second.end()); mi != me; ++mi)
			for (isamap_t::const_iterator ii(mi->second.begin()), ie(mi->second.end()); ii != ie; ++ii)
				ii->second.save_xml(el->add_child("curve"), m_altfactor, m_ratefactor, m_fuelflowfactor, m_casfactor,
						    m_timefactor, m_fuelfactor, m_distfactor);
}

void Aircraft::Climb::clear(void)
{
	m_curves.clear();
}

void Aircraft::Climb::add(const ClimbDescent& cd)
{
	double m(cd.get_mass());
	if (std::isnan(m) || m <= 0)
		m = 0;
	m_curves[cd.get_name()][m][cd.get_isaoffset()] = cd;
}

bool Aircraft::Climb::recalculatepoly(bool force)
{
	bool work(false);
	for (curves_t::iterator ci(m_curves.begin()), ce(m_curves.end()); ci != ce; ++ci)
		for (massmap_t::iterator mi(ci->second.begin()), me(ci->second.end()); mi != me; ++mi)
			for (isamap_t::iterator ii(mi->second.begin()), ie(mi->second.end()); ii != ie; ++ii)
				work = const_cast<ClimbDescent&>(ii->second).recalculatepoly(force) || work;
	return work;
}

void Aircraft::Climb::set_point_vy(double vy)
{
	for (curves_t::iterator ci(m_curves.begin()), ce(m_curves.end()); ci != ce; ++ci)
		for (massmap_t::iterator mi(ci->second.begin()), me(ci->second.end()); mi != me; ++mi)
			for (isamap_t::iterator ii(mi->second.begin()), ie(mi->second.end()); ii != ie; ++ii)
				const_cast<ClimbDescent&>(ii->second).set_point_vy(vy);
}

void Aircraft::Climb::set_mass(double mass)
{
	if (std::isnan(mass) || mass <= 0)
		return;
	for (curves_t::iterator ci(m_curves.begin()), ce(m_curves.end()); ci != ce; ++ci) {
		massmap_t::iterator mi(ci->second.find(0));
		if (mi == ci->second.end())
			continue;
		for (isamap_t::const_iterator ii(mi->second.begin()), ie(mi->second.end()); ii != ie; ++ii) {
			ClimbDescent cd(ii->second);
			cd.set_mass(mass);
			add(cd);
		}
		ci->second.erase(mi);
	}
}

const Aircraft::ClimbDescent *Aircraft::Climb::find_single_curve(void) const
{
	if (m_curves.size() != 1)
		return 0;
	const massmap_t& m(m_curves.begin()->second);
	if (m.size() != 1)
		return 0;
	const isamap_t& i(m.begin()->second);
	if (i.size() != 1)
		return 0;
	return &i.begin()->second;
}

Aircraft::CheckErrors Aircraft::Climb::check(double minmass, double maxmass, double vstall, bool descent) const
{
	CheckErrors r;
	typedef std::map<double,double> cmassmap_t;
	typedef std::map<double,cmassmap_t> cisamap_t;
	cisamap_t ceilings;
	for (curves_t::const_iterator ci(m_curves.begin()), ce(m_curves.end()); ci != ce; ++ci) {
		for (massmap_t::const_iterator mi(ci->second.begin()), me(ci->second.end()); mi != me; ++mi)
			for (isamap_t::const_iterator ii(mi->second.begin()), ie(mi->second.end()); ii != ie; ++ii) {
				r.add(ii->second.check(minmass, maxmass, vstall, descent));
				std::pair<cmassmap_t::iterator, bool> ins(ceilings[ii->second.get_isaoffset()].
									 insert(cmassmap_t::value_type(ii->second.get_mass(), ii->second.get_ceiling())));
				if (!ins.second)
					ins.first->second = std::max(ins.first->second, ii->second.get_ceiling());
			}
		for (cisamap_t::const_iterator ii(ceilings.begin()), ie(ceilings.end()); ii != ie; ++ii) {
			for (cmassmap_t::const_iterator mi(ii->second.begin()), me(ii->second.end()); mi != me; ) {
				cmassmap_t::const_iterator mi2(mi);
				++mi;
				if (mi == me)
					break;
				if (mi2->second >= mi->second)
					continue;
				r.add(descent ? CheckError::type_descent : CheckError::type_climb,
				      CheckError::severity_warning, ci->first, mi2->first, ii->first)
					<< "curve has lower ceiling than higher mass" << mi->first;
			}
		}
	}
	return r;
}

std::ostream& Aircraft::Climb::print(std::ostream& os, const std::string& indent) const
{
	for (curves_t::const_iterator ci(m_curves.begin()), ce(m_curves.end()); ci != ce; ++ci)
		for (massmap_t::const_iterator mi(ci->second.begin()), me(ci->second.end()); mi != me; ++mi)
			for (isamap_t::const_iterator ii(mi->second.begin()), ie(mi->second.end()); ii != ie; ++ii)
				ii->second.print(os << indent << "Profile Name \"" << ci->first << "\" mass " << mi->first
						 << " isaoffs " << ii->first << std::endl, indent + "  ");
	return os;
}

Aircraft::Descent::Descent(double altfactor, double ratefactor, double fuelflowfactor, double casfactor,
			   double timefactor, double fuelfactor, double distfactor)
	: Climb(altfactor, ratefactor, fuelflowfactor, casfactor, timefactor, fuelfactor, distfactor)
{
	m_curves.clear();
	{
		ClimbDescent cd;
		// descent rate in ft/min
		cd.set_descent_rate(1000);
		add(cd);
	}
}

void Aircraft::Descent::load_xml(const xmlpp::Element *el)
{
	if (!el)
		return;
	Climb::load_xml(el);
	xmlpp::Attribute *attr;
	if ((attr = el->get_attribute("rate")) && attr->get_value() != "") {
		double rate(Glib::Ascii::strtod(attr->get_value()));
		std::set<std::string> cn(get_curves());
		if (cn.empty() && !std::isnan(rate) && (rate >= 100)) {
			ClimbDescent cd;
			cd.set_descent_rate(rate);
			add(cd);
		}
	}
}

void Aircraft::Descent::save_xml(xmlpp::Element *el) const
{
	if (!el)
		return;
	Climb::save_xml(el);
	{
		double rate(get_rate());
		if (std::isnan(rate)) {
			el->remove_attribute("rate");
		} else {
			std::ostringstream oss;
			oss << rate;
			el->set_attribute("rate", oss.str());
		}
	}
}

double Aircraft::Descent::get_rate(void) const
{
	double rate(std::numeric_limits<double>::quiet_NaN());
	for (curves_t::const_iterator ci(m_curves.begin()), ce(m_curves.end()); ci != ce; ++ci)
		for (massmap_t::const_iterator mi(ci->second.begin()), me(ci->second.end()); mi != me; ++mi)
			for (isamap_t::const_iterator ii(mi->second.begin()), ie(mi->second.end()); ii != ie; ++ii) {
				if (ii->second.get_ratepoly().size() != 1)
					return std::numeric_limits<double>::quiet_NaN();
				double r(ii->second.get_ratepoly().front());
				if (std::isnan(r))
					return std::numeric_limits<double>::quiet_NaN();
				if (!std::isnan(rate) && rate != r)
					return std::numeric_limits<double>::quiet_NaN();
				rate = r;
			}
	return rate;
}

void Aircraft::Descent::set_default(propulsion_t prop, const Cruise& cruise, const Climb& climb)
{
	double ceiling(climb.get_ceiling());
	if (std::isnan(ceiling) || ceiling < 1000)
		return;
	double rate(std::numeric_limits<double>::quiet_NaN()), mass(std::numeric_limits<double>::quiet_NaN()), isaoffs(0);
	{
		const ClimbDescent *cd(find_single_curve());
		if (!cd)
			return;
		if (cd->has_points())
			return;
		double ff, cas;
		cd->calculate(rate, ff, cas, 0);
		if (std::isnan(rate))
			return;
		mass = cd->get_mass();
		isaoffs = cd->get_isaoffset();
	}
	std::set<std::string> cn(cruise.get_curves());
	if (cn.empty())
		return;
	AirData<float> ad;
	ad.set_qnh_temp();
	bool first(true);
	for (std::set<std::string>::const_iterator ci(cn.begin()), ce(cn.end()); ci != ce; ++ci) {
		if (cruise.get_curve_flags(*ci) & Cruise::Curve::flags_hold)
			continue;
		ClimbDescent cd(*ci, mass, isaoffs);
		cd.clear_points();
		cd.set_ceiling(ceiling);
		typedef std::set<double> alts_t;
		alts_t alts;
		for (double pa = 0;; pa += 1000) {
			bool last(pa >= ceiling);
			if (last)
				pa = ceiling;
			double tas(0), fuelflow(0), pa1(pa), mass1(mass), isaoffs1(isaoffs);
			Cruise::CruiseEngineParams ep(*ci);
			cruise.calculate(prop, tas, fuelflow, pa1, mass1, isaoffs1, ep);
			if (false)
				std::cerr << "Descent: " << *ci << " tas " << tas << " ff " << fuelflow << " pa " << pa1 << " rate " << rate << std::endl;
			if (!std::isnan(tas) && !std::isnan(fuelflow) && !std::isnan(pa1) && tas >= 1 && pa1 >= 0) {
				if (alts.insert(pa1).second) {
					ad.set_pressure_altitude(pa1);
					ad.set_tas(tas);
					cd.add_point(ClimbDescent::Point(pa1, rate, fuelflow, ad.get_cas()));
				}
			}
			if (last)
				break;
		}
		cd.recalculatepoly(true);
		if (first) {
			m_curves.clear();
			first = false;
		}
		add(cd);
	}
}

bool Aircraft::Descent::limit_static_descent_by_opsperf(propulsion_t prop, const Cruise& cruise, const Climb& climb, double mtom,
							const OperationsPerformance::Aircraft& opsperfacft)
{
	double ceiling(climb.get_ceiling());
	if (std::isnan(ceiling) || ceiling < 1000)
		return false;
	double rate(get_rate());
	if (std::isnan(rate) || rate <= 0)
		return false;
	if (!opsperfacft.is_valid())
		return false;
	Descent desc(*this);
	desc.clear();
	// modified version of Aircraft::Descent::set_default
	const double mass(mtom);
	const double isaoffs(0);
	std::set<std::string> cn(cruise.get_curves());
	if (cn.empty())
		return false;
	AirData<float> ad;
	ad.set_qnh_temp();
	bool first(true);
	for (std::set<std::string>::const_iterator ci(cn.begin()), ce(cn.end()); ci != ce; ++ci) {
		if (cruise.get_curve_flags(*ci) & Cruise::Curve::flags_hold)
			continue;
		ClimbDescent cd(*ci, mass, isaoffs);
		cd.clear_points();
		cd.set_ceiling(ceiling);
		typedef std::set<double> alts_t;
		alts_t alts;
		for (double pa = 0;; pa += 1000) {
			bool last(pa >= ceiling);
			if (last)
				pa = ceiling;
			double tas(0), fuelflow(0), pa1(pa), mass1(mass), isaoffs1(isaoffs), rate1(rate);
			Cruise::CruiseEngineParams ep(*ci);
			cruise.calculate(prop, tas, fuelflow, pa1, mass1, isaoffs1, ep);
			if (pa >= 4000) {
				OperationsPerformance::Aircraft::ComputeResult res(opsperfacft.get_massref());
				res.set_qnh_temp(); // ISA
				res.set_pressure_altitude(pa);
				if (opsperfacft.compute(res, OperationsPerformance::Aircraft::compute_descent)) {
					if (res.get_rocd() < -300)
						rate1 = std::min(rate1, -res.get_rocd());
					if (false)
						std::cerr << "Descent: OpsPerf: rate " << -res.get_rocd() << std::endl;
				}
			}
			if (false)
				std::cerr << "Descent: " << *ci << " tas " << tas << " ff " << fuelflow << " pa " << pa1 << " rate " << rate1 << std::endl;
			if (!std::isnan(tas) && !std::isnan(fuelflow) && !std::isnan(pa1) && tas >= 1 && pa1 >= 0) {
				if (alts.insert(pa1).second) {
					ad.set_pressure_altitude(pa1);
					ad.set_tas(tas);
					cd.add_point(ClimbDescent::Point(pa1, rate1, fuelflow, ad.get_cas()));
				}
			}
			if (last)
				break;
		}
		cd.recalculatepoly(true);
		first = false;
		desc.add(cd);
	}
	if (!first)
		*this = desc;
	return !first;

}

Aircraft::ClimbDescent Aircraft::Descent::calculate(const std::string& name, double mass, double isaoffs, double qnh) const
{
	curves_t::const_iterator ci(m_curves.find(name));
	if (ci == m_curves.end()) {
		ci = m_curves.begin();
		if (ci == m_curves.end())
			return ClimbDescent("", 2600);
	}
	return calculate(ci->second, mass, isaoffs, qnh);
}

Aircraft::ClimbDescent Aircraft::Descent::calculate(std::string& name, const std::string& dfltname, double mass, double isaoffs, double qnh) const
{
	curves_t::const_iterator ci(m_curves.find(name));
	if (ci == m_curves.end()) {
		ci = m_curves.find(dfltname);
		if (ci == m_curves.end()) {
			ci = m_curves.begin();
			if (ci == m_curves.end())
				return ClimbDescent("", 2600);
		} else {
			name = dfltname;
		}
	}
	return calculate(ci->second, mass, isaoffs, qnh);
}

Aircraft::ClimbDescent Aircraft::Descent::calculate(const massmap_t& ci, double mass, double isaoffs, double qnh) const
{
	massmap_t::const_iterator miu(ci.lower_bound(mass));
	if (miu == ci.end()) {
		if (miu == ci.begin())
			return ClimbDescent("", 2600);
		--miu;
		if (miu != ci.begin())
			return calculate(miu->second, isaoffs, qnh);
		ClimbDescent r(calculate(miu->second, isaoffs, qnh));
		//r.recalculateformass(mass);
		return r;
	}
	massmap_t::const_iterator mil(miu);
	if (miu == ci.begin()) {
		++miu;
		if (miu == ci.end()) {
			ClimbDescent r(calculate(mil->second, isaoffs, qnh));
			//r.recalculateformass(mass);
			return r;
		}
	} else {
		--mil;
	}
	double t((mass - mil->first) / (miu->first - mil->first));
	if (t <= 0)
		return calculate(mil->second, isaoffs, qnh);
	if (t >= 1)
		return calculate(miu->second, isaoffs, qnh);
	return calculate(miu->second, isaoffs, qnh).interpolate(t, calculate(mil->second, isaoffs, qnh));
}
