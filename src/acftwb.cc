/*****************************************************************************/

/*
 *      acftwb.cc  --  Aircraft Model Mass (Weight) & Balance.
 *
 *      Copyright (C) 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020
 *        Thomas Sailer (t.sailer@alumni.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/*****************************************************************************/

#include "aircraft.h"

#ifdef HAVE_JSONCPP
#include <json/json.h>
#endif

Aircraft::WeightBalance::Element::Unit::Unit(const Glib::ustring& name, double factor, double offset)
	: m_name(name), m_factor(factor), m_offset(offset)
{
}

void Aircraft::WeightBalance::Element::Unit::load_xml(const xmlpp::Element *el)
{
	if (!el)
		return;
	m_name.clear();
	m_factor = 0;
	m_offset = 0;
	xmlpp::Attribute *attr;
 	if ((attr = el->get_attribute("name")))
		m_name = attr->get_value();
	if ((attr = el->get_attribute("factor")) && attr->get_value() != "")
		m_factor = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("offset")) && attr->get_value() != "")
		m_offset = Glib::Ascii::strtod(attr->get_value());
}

void Aircraft::WeightBalance::Element::Unit::save_xml(xmlpp::Element *el) const
{
	if (!el)
		return;
	if (m_name.empty())
		el->remove_attribute("name");
	else
		el->set_attribute("name", m_name);
	if (m_factor == 0) {
		el->remove_attribute("factor");
	} else {
		std::ostringstream oss;
		oss << m_factor;
		el->set_attribute("factor", oss.str());
	}
	if (m_offset == 0) {
		el->remove_attribute("offset");
	} else {
		std::ostringstream oss;
		oss << m_offset;
		el->set_attribute("offset", oss.str());
	}
}

const Aircraft::WeightBalance::Element::FlagName Aircraft::WeightBalance::Element::flagnames[] =
{
	{ "fixed", flag_fixed },
	{ "binary", flag_binary },
	{ "avgas", flag_avgas },
	{ "jeta", flag_jeta },
	{ "diesel", flag_diesel },
	{ "oil", flag_oil },
	{ "gear", flag_gear },
	{ "consumable", flag_consumable },
	{ "seating", flag_seating },
	{ 0, flag_none }
};

Aircraft::WeightBalance::Element::Element(const Glib::ustring& name, double massmin, double massmax, double arm, unsigned int seats, flags_t flags)
	: m_name(name), m_massmin(massmin), m_massmax(massmax), m_seats(seats), m_flags(flags)
{
	if (!std::isnan(arm))
		m_arms[0.0] = arm;
}

void Aircraft::WeightBalance::Element::load_xml(const xmlpp::Element *el)
{
	if (!el)
		return;
	m_units.clear();
	m_name.clear();
	m_massmin = 0;
	m_massmax = 0;
	m_arms.clear();
	m_seats = 0;
	m_flags = flag_none;
	xmlpp::Attribute *attr;
 	if ((attr = el->get_attribute("name")))
		m_name = attr->get_value();
	if ((attr = el->get_attribute("massmin")) && attr->get_value() != "")
		m_massmin = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("massmax")) && attr->get_value() != "")
		m_massmax = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("arm")) && attr->get_value() != "")
		m_arms[0.0] = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("seats")) && attr->get_value() != "") {
		std::istringstream iss(attr->get_value());
		iss >> m_seats;
	}
	if ((attr = el->get_attribute("flags"))) {
		Glib::ustring val(attr->get_value());
		Glib::ustring::size_type vn(0);
		while (vn < val.size()) {
			Glib::ustring::size_type vn2(val.find('|', vn));
			Glib::ustring v1(val.substr(vn, vn2 - vn));
			vn = vn2;
			if (vn < val.size())
				++vn;
			if (v1.empty())
				continue;
			if (*std::min_element(v1.begin(), v1.end()) >= (Glib::ustring::value_type)'0' &&
			    *std::max_element(v1.begin(), v1.end()) <= (Glib::ustring::value_type)'9') {
				m_flags |= (flags_t)strtol(v1.c_str(), 0, 10);
				continue;
			}
			const FlagName *fn = flagnames;
			for (; fn->m_name; ++fn)
				if (fn->m_name == v1)
					break;
			if (!fn->m_name) {
				std::cerr << "WeightBalance::Element::load_xml: invalid flag " << v1 << std::endl;
				continue;
			}
			m_flags |= fn->m_flag;
		}
	}
	{
		xmlpp::Node::NodeList nl(el->get_children("arms"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni) {
			const xmlpp::Element *el2(static_cast<xmlpp::Element *>(*ni));
			double m(std::numeric_limits<double>::quiet_NaN()), a(std::numeric_limits<double>::quiet_NaN());
			if ((attr = el2->get_attribute("mass")) && attr->get_value() != "")
				m = Glib::Ascii::strtod(attr->get_value());
			if ((attr = el2->get_attribute("arm")) && attr->get_value() != "")
				a = Glib::Ascii::strtod(attr->get_value());
			if (!std::isnan(m) && !std::isnan(a))
				m_arms[m] = a;
		}
	}
	{
		xmlpp::Node::NodeList nl(el->get_children("unit"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni) {
			m_units.push_back(Unit());
			m_units.back().load_xml(static_cast<xmlpp::Element *>(*ni));
		}
	}
}

void Aircraft::WeightBalance::Element::save_xml(xmlpp::Element *el) const
{
	if (!el)
		return;
	{
		xmlpp::Node::NodeList nl(el->get_children());
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni)
			el->remove_child(*ni);
	}
	if (m_name.empty())
		el->remove_attribute("name");
	else
		el->set_attribute("name", m_name);
	if (m_massmin == 0) {
		el->remove_attribute("massmin");
	} else {
		std::ostringstream oss;
		oss << m_massmin;
		el->set_attribute("massmin", oss.str());
	}
	if (m_massmax == 0) {
		el->remove_attribute("massmax");
	} else {
		std::ostringstream oss;
		oss << m_massmax;
		el->set_attribute("massmax", oss.str());
	}
	{
		arms_t::const_iterator i(m_arms.begin());
		if (i != m_arms.end() && i->first == 0.0)
			++i;
		if (i == m_arms.end() && i != m_arms.begin()) {
			std::ostringstream oss;
			oss << m_arms.begin()->second;
			el->set_attribute("arm", oss.str());
		} else {
			el->remove_attribute("arm");
			for (arms_t::const_iterator ai(m_arms.begin()), ae(m_arms.end()); ai != ae; ++ai) {
				xmlpp::Element *el2(el->add_child("arms"));
				{
					std::ostringstream oss;
					oss << ai->first;
					el2->set_attribute("mass", oss.str());
				}
				{
					std::ostringstream oss;
					oss << ai->second;
					el2->set_attribute("arm", oss.str());
				}
			}
		}
	}
	if (m_seats) {
		std::ostringstream oss;
		oss << m_seats;
		el->set_attribute("seats", oss.str());
	} else {
		el->remove_attribute("seats");
	}
	{
		Glib::ustring flg;
		flags_t flags(m_flags);
		for (const FlagName *fn = flagnames; fn->m_name; ++fn) {
			if (fn->m_flag & ~flags)
				continue;
			flags &= ~fn->m_flag;
			flg += "|";
			flg += fn->m_name;
		}
		if (flags) {
			std::ostringstream oss;
			oss << '|' << (unsigned int)flags;
			flg += oss.str();
		}
		if (flg.empty())
			el->remove_attribute("flags");
		else
			el->set_attribute("flags", flg.substr(1));
	}
	for (units_t::const_iterator ui(m_units.begin()), ue(m_units.end()); ui != ue; ++ui)
		ui->save_xml(el->add_child("unit"));
}

double Aircraft::WeightBalance::Element::get_arm(double mass) const
{
	if (std::isnan(mass) || m_arms.empty())
		return std::numeric_limits<double>::quiet_NaN();
	arms_t::const_iterator iu(m_arms.lower_bound(mass));
	if (iu == m_arms.begin())
		return iu->second;
	arms_t::const_iterator il(iu);
	--il;
	if (iu == m_arms.end())
		return il->second;
	double t(mass - il->first);
	t /= (iu->first - il->first);
	return t * iu->second + (1 - t) * il->second;
}

std::pair<double,double> Aircraft::WeightBalance::Element::get_piecelimits(double mass, bool rounddown) const
{
	std::pair<double,double> r(std::numeric_limits<double>::quiet_NaN(), std::numeric_limits<double>::quiet_NaN());
	if (std::isnan(mass) || m_arms.empty())
		return r;
	arms_t::const_iterator iu(m_arms.lower_bound(mass));
	if (!rounddown && iu != m_arms.end() && mass == iu->first)
		++iu;
	if (iu == m_arms.begin()) {
		r.first = 0;
		r.second = iu->first;
		return r;
	}
	arms_t::const_iterator il(iu);
	--il;
	if (iu == m_arms.end()) {
		r.first = il->first;
		r.second = std::numeric_limits<double>::max();
		return r;
	}
	r.first = il->first;
	r.second = iu->first;
	return r;
}

void Aircraft::WeightBalance::Element::clear_units(void)
{
	m_units.clear();
}

void Aircraft::WeightBalance::Element::add_unit(const Glib::ustring& name, double factor, double offset)
{
	m_units.push_back(Unit(name, factor, offset));
	if (m_units.back().is_fixed())
		m_flags |= flag_fixed;
}

void Aircraft::WeightBalance::Element::add_auto_units(unit_t massunit, unit_t fuelunit, unit_t oilunit)
{
	if (!((1 << massunit) & unitmask_mass))
		return;
	flags_t flags(get_flags() & (flag_fuel | flag_oil));
	unit_t un(unit_invalid);
	unitmask_t um(unitmask_mass);
	switch (flags) {
	case flag_avgas:
	case flag_jeta:
	case flag_diesel:
		un = fuelunit;
		um |= unitmask_volume;
		break;

	case flag_oil:
		un = oilunit;
		um |= unitmask_volume;
		break;

	default:
		break;
	}
	add_unit(to_str(massunit, true), 1.0, 0);
	if ((1 << un) & unitmask_volume)
		add_unit(to_str(un, true), convert(un, massunit, 1.0, flags), 0);
	else
		un = unit_invalid;
	for (unit_t u(unit_kg); u < unit_invalid; u = (unit_t)(u + 1))
		if (u != un && u != massunit && ((1 << u) & um))
			add_unit(to_str(u, true), convert(u, massunit, 1.0, flags), 0);
}

Aircraft::WeightBalance::Envelope::Point::Point(double arm, double mass)
	: m_arm(arm), m_mass(mass)
{
}

/* twice the signed area of the triangle p0, p1, p2 */
double Aircraft::WeightBalance::Envelope::Point::area2(const Point& p0, const Point& p1, const Point& p2)
{
        double p2x = p2.get_arm();
        double p2y = p2.get_mass();
        double p0x = p0.get_arm() - p2x;
        double p0y = p0.get_mass() - p2y;
        double p1x = p1.get_arm() - p2x;
        double p1y = p1.get_mass() - p2y;
        return p0x * (p1y - p0y) - p0y * (p1x - p0x);
}

double Aircraft::WeightBalance::Envelope::Point::length2(const Point& p0, const Point& p1)
{
	Point p(p1 - p0);
	return p.get_arm() * p.get_arm() + p.get_mass() * p.get_mass();
}

double Aircraft::WeightBalance::Envelope::Point::interpolate_arm(const Point& p0, const Point& p1, double mass)
{
	return p0.get_arm() + (p1.get_arm() - p0.get_arm()) * (mass - p0.get_mass()) / (p1.get_mass() - p0.get_mass());
}

void Aircraft::WeightBalance::Envelope::Point::load_xml(const xmlpp::Element *el)
{
	if (!el)
		return;
	m_arm = 0;
	m_mass = 0;
	xmlpp::Attribute *attr;
 	if ((attr = el->get_attribute("arm")) && attr->get_value() != "")
		m_arm = Glib::Ascii::strtod(attr->get_value());
 	if ((attr = el->get_attribute("mass")) && attr->get_value() != "")
		m_mass = Glib::Ascii::strtod(attr->get_value());
}

void Aircraft::WeightBalance::Envelope::Point::save_xml(xmlpp::Element *el) const
{
	if (!el)
		return;
	if (m_arm == 0) {
		el->remove_attribute("arm");
	} else {
		std::ostringstream oss;
		oss << m_arm;
		el->set_attribute("arm", oss.str());
	}
	if (m_mass == 0) {
		el->remove_attribute("mass");
	} else {
		std::ostringstream oss;
		oss << m_mass;
		el->set_attribute("mass", oss.str());
	}
}

Aircraft::WeightBalance::Envelope::Envelope(const Glib::ustring& name)
	: m_name(name)
{
}

int Aircraft::WeightBalance::Envelope::windingnumber(const Point& pt) const
{
	int wn = 0; /* the winding number counter */

	/* loop through all edges of the polygon */
	unsigned int j = size() - 1;
	for (unsigned int i = 0; i < size(); j = i, i++) {
		const Point& pj(operator[](j));
		const Point& pi(operator[](i));
		double lon = pt.area2(pj, pi);
		/* edge from V[j] to V[i] */
		if (pj.get_mass() <= pt.get_mass()) {
			if (pi.get_mass() > pt.get_mass())
				/* an upward crossing */
				if (lon > 0)
					/* P left of edge: have a valid up intersect */
					wn++;
		} else {
			if (pi.get_mass() <= pt.get_mass())
				/* a downward crossing */
				if (lon < 0)
					/* P right of edge: have a valid down intersect */
					wn--;
		}
	}
	return wn;
}

void Aircraft::WeightBalance::Envelope::get_bounds(double& minarm, double& maxarm, double& minmass, double& maxmass) const
{
	double mina(std::numeric_limits<double>::max());
	double maxa(std::numeric_limits<double>::min());
	double minm(mina);
	double maxm(maxa);
	for (env_t::const_iterator ei(m_env.begin()), ee(m_env.end()); ei != ee; ++ei) {
		const Point& el(*ei);
		mina = std::min(mina, el.get_arm());
		maxa = std::max(maxa, el.get_arm());
		minm = std::min(minm, el.get_mass());
		maxm = std::max(maxm, el.get_mass());
	}
	minarm = mina;
	maxarm = maxa;
	minmass = minm;
	maxmass = maxm;
}

void Aircraft::WeightBalance::Envelope::load_xml(const xmlpp::Element *el)
{
	if (!el)
		return;
	m_env.clear();
	m_name.clear();
	xmlpp::Attribute *attr;
 	if ((attr = el->get_attribute("name")))
		m_name = attr->get_value();
	{
		xmlpp::Node::NodeList nl(el->get_children("point"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni) {
			m_env.push_back(Point());
			m_env.back().load_xml(static_cast<xmlpp::Element *>(*ni));
		}
	}
}

void Aircraft::WeightBalance::Envelope::save_xml(xmlpp::Element *el) const
{
	if (!el)
		return;
	if (m_name.empty())
		el->remove_attribute("name");
	else
		el->set_attribute("name", m_name);
	for (env_t::const_iterator ei(m_env.begin()), ee(m_env.end()); ei != ee; ++ei)
		ei->save_xml(el->add_child("point"));
}

void Aircraft::WeightBalance::Envelope::add_point(const Point& pt)
{
	m_env.push_back(pt);
}

void Aircraft::WeightBalance::Envelope::add_point(double arm, double mass)
{
	add_point(Point(arm, mass));
}

Aircraft::WeightBalance::Envelope::masses_t Aircraft::WeightBalance::Envelope::get_masses(void) const
{
	masses_t m;
	for (env_t::const_iterator ei(m_env.begin()), ee(m_env.end()); ei != ee; ++ei)
		m.insert(ei->get_mass());
	return m;
}

Aircraft::WeightBalance::Envelope::armintervals_t Aircraft::WeightBalance::Envelope::get_armintervals(double mass) const
{
	armintervals_t intv;
	typedef std::map<double, int> wn_t;
	wn_t wn;
/* loop through all edges of the polygon */
	unsigned int j = size() - 1;
	for (unsigned int i = 0; i < size(); j = i, i++) {
		const Point& pj(operator[](j));
		const Point& pi(operator[](i));
		/* edge from V[j] to V[i] */
		if (pj.get_mass() <= mass) {
			if (pi.get_mass() > mass) {
				/* an upward crossing */
				double arm(pj.get_arm() + (pi.get_arm() - pj.get_arm()) * (mass - pj.get_mass()) / (pi.get_mass() - pj.get_mass()));
				wn_t::iterator it(wn.insert(wn_t::value_type(arm, 0)).first);
				++it->second;
			} else if (pj.get_mass() == mass && pi.get_mass() == mass) {
				/* a horizontal crossing on line */
				double arm0(pj.get_arm()), arm1(pi.get_arm());
				if (arm0 > arm1)
					std::swap(arm0, arm1);
				intv |= armintervals_t::Intvl(arm0, arm1);
			}
		} else {
			if (pi.get_mass() <= mass) {
				/* a downward crossing */
				double arm(pj.get_arm() + (pi.get_arm() - pj.get_arm()) * (mass - pj.get_mass()) / (pi.get_mass() - pj.get_mass()));
				wn_t::iterator it(wn.insert(wn_t::value_type(arm, 0)).first);
				--it->second;
			}
		}
	}
	double arm0(std::numeric_limits<double>::min());
	int wn0(0);
	for (wn_t::const_iterator wi(wn.begin()), we(wn.end()); wi != we; ++wi) {
		int wn1(wn0 + wi->second);
		if (!wn0 && wn1) {
			arm0 = wi->first;
		} else if (wn0 && !wn1) {
			intv |= armintervals_t::Intvl(arm0, wi->first);
			arm0 = wi->first;
		}
		wn0 = wn1;
	}
	if (wn0)
		intv |= armintervals_t::Intvl(arm0, std::numeric_limits<double>::max());
	return intv;
}

Aircraft::WeightBalance::WeightBalance(void)
	: m_armunit(unit_in), m_massunit(unit_lb)
{
	// Default: P28R-200
	m_elements.push_back(Element("Pilot and Front Pax", 0, 600, 85.5, 2, Element::flag_seating));
	m_elements.back().add_auto_units(m_massunit, unit_usgal, unit_quart);
	m_elements.push_back(Element("Rear Pax", 0, 600, 118.1, 2, Element::flag_seating));
	m_elements.back().add_auto_units(m_massunit, unit_usgal, unit_quart);
	m_elements.push_back(Element("Cargo", 0, 200, 142.8, 0, Element::flag_none));
	m_elements.back().add_auto_units(m_massunit, unit_usgal, unit_quart);
	m_elements.push_back(Element("Fuel", 0, 50 * usgal_to_liter * avgas_density * kg_to_lb, 95, 0, Element::flag_avgas));
	m_elements.back().add_auto_units(m_massunit, unit_usgal, unit_quart);
	m_elements.push_back(Element("Oil", 0, 15, 29.5, 0, Element::flag_oil));
	m_elements.back().add_auto_units(m_massunit, unit_usgal, unit_quart);
	m_elements.push_back(Element("Gear", 0, 0.1, 8190, 0, Element::flag_gear));
	m_elements.back().add_unit("Extended", 0, 0);
	m_elements.back().add_unit("Retracted", 0, 0.1);
	m_elements.push_back(Element("Basic Empty Mass", 1653.0, 1653.0, 84.69, 0, Element::flag_fixed));
	m_envelopes.push_back(Envelope("Normal"));
	m_envelopes.back().add_point(81.0, 1600);
	m_envelopes.back().add_point(95.9, 1600);
	m_envelopes.back().add_point(95.9, 2600);
	m_envelopes.back().add_point(90.0, 2600);
	m_envelopes.back().add_point(81.0, 1925);
	if (false) {
		static const double mtom_utility(1950);
		m_envelopes.push_back(Envelope("Utility"));
		m_envelopes.back().add_point(81.0, 1600);
		m_envelopes.back().add_point(95.9, 1600);
		m_envelopes.back().add_point(95.9, mtom_utility);
		m_envelopes.back().add_point(81.0 + (90.0 - 81.0) * (mtom_utility - 1925) / (2600 - 1925), mtom_utility);
		m_envelopes.back().add_point(81.0, 1925);
	}
}

void Aircraft::WeightBalance::load_xml(const xmlpp::Element *el)
{
	if (!el)
		return;
	m_elements.clear();
	m_envelopes.clear();
	m_remark.clear();
	m_armunit = unit_invalid;
	m_massunit = unit_invalid;
	xmlpp::Attribute *attr;
 	if ((attr = el->get_attribute("armunitname")))
		m_armunit = parse_unit(attr->get_value());
 	if ((attr = el->get_attribute("massunitname")))
		m_massunit = parse_unit(attr->get_value());
	if (!((1 << m_armunit) & unitmask_length))
		m_armunit = unit_in;
	if (!((1 << m_massunit) & unitmask_mass))
		m_massunit = unit_lb;
	if ((attr = el->get_attribute("remark")))
		m_remark = attr->get_value();
	{
		xmlpp::Node::NodeList nl(el->get_children("element"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni) {
			m_elements.push_back(Element());
			m_elements.back().load_xml(static_cast<xmlpp::Element *>(*ni));
		}
	}
	{
		xmlpp::Node::NodeList nl(el->get_children("envelope"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni) {
			m_envelopes.push_back(Envelope());
			m_envelopes.back().load_xml(static_cast<xmlpp::Element *>(*ni));
		}
	}
}

void Aircraft::WeightBalance::save_xml(xmlpp::Element *el) const
{
	if (!el)
		return;
	if (m_armunit == unit_invalid)
		el->remove_attribute("armunitname");
	else
		el->set_attribute("armunitname", to_str(m_armunit));
	if (m_massunit == unit_invalid)
		el->remove_attribute("massunitname");
	else
		el->set_attribute("massunitname", to_str(m_massunit));
	if (m_remark.empty())
		el->remove_attribute("remark");
	else
		el->set_attribute("remark", m_remark);
	for (elements_t::const_iterator ei(m_elements.begin()), ee(m_elements.end()); ei != ee; ++ei)
		ei->save_xml(el->add_child("element"));
	for (envelopes_t::const_iterator ei(m_envelopes.begin()), ee(m_envelopes.end()); ei != ee; ++ei)
		ei->save_xml(el->add_child("envelope"));
}

#ifdef HAVE_JSONCPP

namespace {
	class EnvelopePointMassSorter {
	public:
		bool operator()(const Aircraft::WeightBalance::Envelope::Point& a,
				const Aircraft::WeightBalance::Envelope::Point& b) const {
			return a.get_mass() < b.get_mass();
		}
	};
};

bool Aircraft::WeightBalance::load_garminpilot(const Json::Value& root, const Aircraft& acft, unsigned int version)
{
	WeightBalance::Element::flags_t fueltype(WeightBalance::Element::flag_jeta);
	double fullfuel(std::numeric_limits<double>::quiet_NaN());
	if (root.isMember("fuelTankFullAmount") && root["fuelTankFullAmount"].isNumeric())
		fullfuel = root["fuelTankFullAmount"].asDouble();
	if (std::isnan(fullfuel) && root.isMember("fuel") && root["fuel"].isNumeric())
		fullfuel = root["fuel"].asDouble();
	if (root.isMember("fuelType")) {
		const Json::Value& ft(root["fuelType"]);
		if (ft.isString()) {
			if (ft.asString() == "100LL")
				fueltype = WeightBalance::Element::flag_avgas;
		}
	}
	m_armunit = unit_in;
	m_massunit = unit_lb;
	if (root.isMember("unitSystem")) {
		const Json::Value& sys(root["unitSystem"]);
		if (sys.isIntegral()) {
			switch (sys.asInt()) {
			default:
			case 0:
				m_armunit = unit_in;
				m_massunit = unit_lb;
				break;

			case 1:
				m_armunit = unit_cm;
				m_massunit = unit_kg;
				break;
			}
		}
	}
	if (root.isMember("cargoStations")) {
		const Json::Value& cstn(root["cargoStations"]);
		if (cstn.isArray()) {
			for (Json::ArrayIndex i = 0; i < cstn.size(); ++i) {
				const Json::Value& cstn1(cstn[i]);
				if (!cstn1.isObject())
					continue;
				Element::flags_t flags(Element::flag_none);
				Glib::ustring name;
				double massmax(std::numeric_limits<double>::quiet_NaN());
				double arm(std::numeric_limits<double>::quiet_NaN());
				unsigned int seats(0);
				if (cstn1.isMember("name")) {
					const Json::Value& x(cstn1["name"]);
					if (x.isString())
						name = x.asString();
				}
				if (cstn1.isMember("defaultArm")) {
					const Json::Value& x(cstn1["defaultArm"]);
					if (x.isNumeric())
						arm = convert(unit_lb, m_armunit, x.asDouble());
				}
				if (cstn1.isMember("type")) {
					const Json::Value& typ(cstn1["type"]);
					if (typ.isIntegral()) {
						switch (typ.asInt()) {
						case 0:
							if (cstn1.isMember("numberOfSeats")) {
								const Json::Value& x(cstn1["numberOfSeats"]);
								if (x.isIntegral()) {
									seats = x.asInt();
									massmax = convert(unit_lb, m_massunit, 200.0 * seats);
									flags |= Element::flag_seating;
								}
							}
							break;

						case 1:
							if (cstn1.isMember("maximumWeight")) {
								const Json::Value& x(cstn1["maximumWeight"]);
								if (x.isNumeric())
									massmax = convert(unit_lb, m_massunit, x.asDouble());
							}
							break;

						default:
							break;
						}
					}
				}
				if (std::isnan(massmax) || std::isnan(arm) || massmax < 0.0)
					continue;
				add_element(Element(name, 0.0, massmax, arm, seats, flags));
			}
		}
	}
	if (root.isMember("fuelStations")) {
		const Json::Value& fstn(root["fuelStations"]);
		if (fstn.isArray()) {
			for (Json::ArrayIndex i = 0; i < fstn.size(); ++i) {
				const Json::Value& fstn1(fstn[i]);
				if (!fstn1.isObject())
					continue;
				Glib::ustring name;
				double massmax(std::numeric_limits<double>::quiet_NaN());
				double arm(std::numeric_limits<double>::quiet_NaN());
				if (fstn1.isMember("name")) {
					const Json::Value& x(fstn1["name"]);
					if (x.isString())
						name = x.asString();
				}
				if (fstn1.isMember("fixedArm")) {
					const Json::Value& x(fstn1["fixedArm"]);
					if (x.isNumeric())
						arm = convert(unit_lb, m_armunit, x.asDouble());
				}
				if (fstn1.isMember("fixedMaximumWeight")) {
					const Json::Value& x(fstn1["fixedMaximumWeight"]);
					if (x.isNumeric())
						massmax = convert(unit_lb, m_massunit, x.asDouble());
				}
				if (std::isnan(massmax) || std::isnan(arm) || massmax < 0.0)
					continue;
				add_element(Element(name, 0.0, massmax, arm, 0, fueltype));
			}
		}
	}
	if (root.isMember("optionalEquipment")) {
		const Json::Value& oeqpt(root["optionalEquipment"]);
		if (oeqpt.isArray()) {
			for (Json::ArrayIndex i = 0; i < oeqpt.size(); ++i) {
				const Json::Value& oeqpt1(oeqpt[i]);
				if (!oeqpt1.isObject())
					continue;
				Element::flags_t flags(Element::flag_none);
				Glib::ustring name;
				double massmax(std::numeric_limits<double>::quiet_NaN());
				double arm(std::numeric_limits<double>::quiet_NaN());
				if (oeqpt1.isMember("name")) {
					const Json::Value& x(oeqpt1["name"]);
					if (x.isString())
						name = x.asString();
				}
				if (oeqpt1.isMember("fixedArm")) {
					const Json::Value& x(oeqpt1["fixedArm"]);
					if (x.isNumeric())
						arm = convert(unit_lb, m_armunit, x.asDouble());
				}
				if (oeqpt1.isMember("fixedMaximumWeight")) {
					const Json::Value& x(oeqpt1["fixedMaximumWeight"]);
					if (x.isNumeric())
						massmax = convert(unit_lb, m_massunit, x.asDouble());
				}
				if (std::isnan(massmax) || std::isnan(arm) || massmax < 0.0)
					continue;
				add_element(Element(name, 0.0, massmax, arm, 0, flags));
			}
		}
	}
	if (root.isMember("variableWeightEquipment")) {
		const Json::Value& veqpt(root["variableWeightEquipment"]);
		if (veqpt.isArray()) {
			for (Json::ArrayIndex i = 0; i < veqpt.size(); ++i) {
				const Json::Value& veqpt1(veqpt[i]);
				if (!veqpt1.isObject())
					continue;
				Element::flags_t flags(Element::flag_none);
				Glib::ustring name;
				double massmax(std::numeric_limits<double>::quiet_NaN());
				double arm(std::numeric_limits<double>::quiet_NaN());
				if (veqpt1.isMember("name")) {
					const Json::Value& x(veqpt1["name"]);
					if (x.isString())
						name = x.asString();
				}
				if (veqpt1.isMember("fixedArm")) {
					const Json::Value& x(veqpt1["fixedArm"]);
					if (x.isNumeric())
						arm = convert(unit_lb, m_armunit, x.asDouble());
				}
				if (veqpt1.isMember("fixedMaximumWeight")) {
					const Json::Value& x(veqpt1["fixedMaximumWeight"]);
					if (x.isNumeric())
						massmax = convert(unit_lb, m_massunit, x.asDouble());
				}
				if (std::isnan(massmax) || std::isnan(arm) || massmax < 0.0)
					continue;
				add_element(Element(name, 0.0, massmax, arm, 0, flags));
			}
		}
	}
	if (root.isMember("emptyWeight") && root.isMember("emptyCG")) {
		const Json::Value& ew(root["emptyWeight"]);
		const Json::Value& ea(root["emptyCG"]);
		double mass(std::numeric_limits<double>::quiet_NaN());
		double arm(std::numeric_limits<double>::quiet_NaN());
		if (ew.isNumeric())
			mass = convert(unit_lb, m_massunit, ew.asDouble());
  		if (ea.isNumeric())
			arm = convert(unit_lb, m_armunit, ea.asDouble());
		if (!std::isnan(mass) && !std::isnan(arm) && mass > 0.0)
			add_element(Element("Basic Empty Mass", mass, mass, arm, 0, Element::flag_fixed));
	}
	if (root.isMember("momentChangeDueToGearRetraction")) {
		const Json::Value& mom(root["momentChangeDueToGearRetraction"]);
		double moment(std::numeric_limits<double>::quiet_NaN());
		if (mom.isNumeric())
			moment = convert(unit_lb, m_massunit, convert(unit_lb, m_armunit, mom.asDouble()));
		if (!std::isnan(moment) && moment != 0.0) {
			static const double arm(0.1);
			Element el("Gear", 0.0, moment * (1.0 / arm), arm, 0, Element::flag_fixed | Element::flag_gear);
			el.add_unit("Extended", 0.0, 0.0);
			el.add_unit("Retracted", 0.0, arm);
			add_element(el);
		}
	}
	static const char * const envelopes[] = {
		"normalCategoryEnvelope",
		"utilityCategoryEnvelope",
		"acrobaticCategoryEnvelope"
	};
	static const char * const envelopenames[] = {
		"Normal Category",
		"Utility Category",
		"Acrobatic Category"
	};
	for (int envidx = 0; envidx < sizeof(envelopes)/sizeof(envelopes[0]); ++envidx) {
		if (!root.isMember(envelopes[envidx]))
			continue;
		const Json::Value& envelope(root[envelopes[envidx]]);
		if (!envelope.isObject())
			continue;
		static const char * const limitnames[] = {
			"forwardLimits",
			"aftLimits"
		};
		typedef std::vector<Envelope::Point> env_t;
		env_t limits[sizeof(limitnames)/sizeof(limitnames[0])];
		for (int limidx = 0; limidx < sizeof(limitnames)/sizeof(limitnames[0]); ++limidx) {
			if (!envelope.isMember(limitnames[limidx]))
				continue;
			const Json::Value& limita(envelope[limitnames[limidx]]);
			if (!limita.isArray())
				continue;
			for (Json::ArrayIndex i = 0; i < limita.size(); ++i) {
				const Json::Value& limitp(limita[i]);
				if (!limitp.isObject() || !limitp.isMember("weight") || !limitp.isMember("limit"))
					continue;
				const Json::Value& ew(root["weight"]);
				const Json::Value& ea(root["limit"]);
				double mass(std::numeric_limits<double>::quiet_NaN());
				double arm(std::numeric_limits<double>::quiet_NaN());
				if (ew.isNumeric())
					mass = convert(unit_lb, m_massunit, ew.asDouble());
				if (ea.isNumeric())
					arm = convert(unit_lb, m_armunit, ea.asDouble());
				if (!std::isnan(mass) && !std::isnan(arm))
					limits[limidx].push_back(Envelope::Point(arm, mass));
			}
			std::sort(limits[limidx].begin(), limits[limidx].end(), EnvelopePointMassSorter());
		}
		if (limits[0].empty() || limits[1].empty())
			continue;
		Envelope ev(envelopenames[envidx]);
		for (env_t::const_iterator i(limits[0].begin()), e(limits[0].end()); i != e; ++i)
			ev.add_point(*i);
		for (env_t::const_reverse_iterator i(limits[0].rbegin()), e(limits[0].rend()); i != e; ++i)
			ev.add_point(*i);
		add_envelope(ev);
	}
	return true;
}

void Aircraft::WeightBalance::save_garminpilot(Json::Value& root, const Aircraft& acft, unsigned int version)
{
	root["showEquipmentOnChart"] = false;
	root["envelopeLimitType"] = 0;
	root["fuelBurnStrategy"] = 0;
	root["showMaximumWeightLinesOnChart"] = false;
	root["divideMoment"] = true;
	root["sourceTemplateUUID"] = Json::Value(Json::nullValue);
	unit_t armunit, massunit;
	if (false && m_massunit == unit_kg) {
		root["unitSystem"] = 1;
		armunit = unit_cm;
		massunit = unit_kg;
	} else {
		root["unitSystem"] = 0;
		armunit = unit_in;
		massunit = unit_lb;
	}
	Json::Value *stns[4] = { &root["cargoStations"], &root["fuelStations"], &root["optionalEquipment"], &root["variableWeightEquipment"] };
	for (unsigned int i = 0; i < sizeof(stns)/sizeof(stns[0]); ++i)
		*stns[i] = Json::Value(Json::arrayValue);
	double bemass(0), bemoment(0), gearmom(0);
 	for (elements_t::const_iterator ei(m_elements.begin()), ee(m_elements.end()); ei != ee; ++ei) {
		const Element& el(*ei);
		if (el.get_flags() & Element::flag_fixed) {
			if (el.get_flags() & Element::flag_gear) {
				gearmom += el.get_moment(el.get_massmax());
			} else {
				bemass += el.get_massmax();
				bemoment += el.get_moment(el.get_massmax());
			}
			continue;
		}
		typedef std::set<double> armpoints_t;
		armpoints_t armpoints;
		{
			double massmin(el.get_massmin());
                        double mass1(el.get_massmax());
                        for (;;) {
                                std::pair<double,double> lim(el.get_piecelimits(mass1, true));
                                if (std::isnan(lim.first) || lim.first <= massmin)
                                        break;
				if (lim.first >= mass1)
                                        break;
                                mass1 = lim.first;
				armpoints.insert(mass1);
                        }
		}
		bool fixedarm(armpoints.empty());
		armpoints.insert(el.get_massmin());
		armpoints.insert(el.get_massmax());
		if (el.get_flags() & Element::flag_fuel) {
			Json::Value x;
			x["name"] = (std::string)el.get_name();
			if (fixedarm) {
				x["variableArmPoints"] = Json::Value(Json::nullValue);
				x["fixedArm"] = convert(m_armunit, armunit, el.get_arm(el.get_massmax()));
			}
			x["fixedMaximumWeight"] = convert(m_massunit, massunit, el.get_massmax());
			stns[1]->append(x);
			continue;
		}
		if (el.get_flags() & Element::flag_oil) {
			Json::Value x;
			x["name"] = (std::string)el.get_name();
			if (fixedarm) {
				x["variableArmPoints"] = Json::Value(Json::nullValue);
				x["fixedArm"] = convert(m_armunit, armunit, el.get_arm(el.get_massmax()));
			}
			x["fixedMaximumWeight"] = convert(m_massunit, massunit, el.get_massmax());
			stns[3]->append(x);
			continue;
		}
		if (true) {
			Json::Value x;
			x["name"] = (std::string)el.get_name();
			if (fixedarm) {
				x["defaultArm"] = convert(m_armunit, armunit, el.get_arm(el.get_massmax()));
				x["minimumArm"] = Json::Value(Json::nullValue);
				x["maximumArm"] = Json::Value(Json::nullValue);
			}
			if (el.get_flags() & Element::flag_seating)
				x["numberOfSeats"] = el.get_seats();
			else
				x["numberOfSeats"] = Json::Value(Json::nullValue);
			x["maximumWeight"] = convert(m_massunit, massunit, el.get_massmax());
			x["type"] = 1;
			stns[0]->append(x);
		}
	}
	if (bemass > 0.0) {
		root["emptyWeight"] = convert(m_massunit, massunit, bemass);
		root["emptyCG"] = convert(m_armunit, armunit, bemoment / bemass);
	} else {
		root["emptyWeight"] = Json::Value(Json::nullValue);
		root["emptyCG"] = Json::Value(Json::nullValue);
	}
	if (gearmom != 0.0)
		root["momentChangeDueToGearRetraction"] = convert(m_massunit, massunit, convert(m_armunit, armunit, gearmom));
 	else
		root["momentChangeDueToGearRetraction"] = Json::Value(Json::nullValue);
	static const char * const envelope_names[] = { "normalCategoryEnvelope", "utilityCategoryEnvelope", "acrobaticCategoryEnvelope" };
	unsigned int envidx(0);
	for (envelopes_t::const_iterator ei(m_envelopes.begin()), ee(m_envelopes.end()); ei != ee; ++ei, ++envidx) {
		if (envidx >= sizeof(envelope_names)/sizeof(envelope_names[0]))
			break;
		Json::Value& env(root[envelope_names[envidx]]);
		env = Json::Value(Json::objectValue);
		typedef std::vector<Envelope::Point> limits_t;
		limits_t lim[2];
		{
			Envelope::masses_t masses(ei->get_masses());
			for (Envelope::masses_t::const_iterator mi(masses.begin()), me(masses.end()); mi != me; ++mi) {
				Envelope::armintervals_t intv(ei->get_armintervals(*mi));
				if (intv.is_empty())
					continue;
				lim[0].push_back(Envelope::Point(intv.begin()->get_lower(), *mi));
				lim[1].push_back(Envelope::Point(intv.rbegin()->get_upper(), *mi));
			}
		}
		static const char * const limnames[2] = { "forwardLimits", "aftLimits" };
		for (unsigned int li = 0; li < sizeof(lim)/sizeof(lim[0]); ++li) {
			Json::Value& limv(env[limnames[li]]);
			limv = Json::Value(Json::arrayValue);
			// remove congruent points
			for (limits_t::size_type i(2); i < lim[li].size(); ) {
				const Envelope::Point& p0(lim[li][i-2]);
				const Envelope::Point& p1(lim[li][i-1]);
				const Envelope::Point& p2(lim[li][i]);
				if (p2.get_mass() == p0.get_mass()) {
					++i;
					continue;
				}
				double arm1(Envelope::Point::interpolate_arm(p0, p2, p1.get_mass()));
				if (fabs(arm1 - p1.get_arm()) > 1e-6) {
					++i;
					continue;
				}
				lim[li].erase(lim[li].begin() + (i - 1));
			}
			for (limits_t::const_iterator i(lim[li].begin()), e(lim[li].end()); i != e; ++i) {
				Json::Value x;
				x["weight"] = convert(m_massunit, massunit, i->get_mass());
				x["limit"] = convert(m_armunit, armunit, i->get_arm());
				limv.append(x);
			}
		}
	}
	for (; envidx < sizeof(envelope_names)/sizeof(envelope_names[0]); ++envidx)
		root[envelope_names[envidx]] = Json::Value(Json::nullValue);
}

#endif

void Aircraft::WeightBalance::clear_elements(void)
{
	m_elements.clear();
}

void Aircraft::WeightBalance::add_element(const Element& el)
{
	m_elements.push_back(el);
}

void Aircraft::WeightBalance::clear_envelopes(void)
{
	m_envelopes.clear();
}

void Aircraft::WeightBalance::add_envelope(const Envelope& el)
{
	m_envelopes.push_back(el);
}

const std::string& Aircraft::WeightBalance::get_armunitname(void) const
{
	return to_str(get_armunit());
}

const std::string& Aircraft::WeightBalance::get_massunitname(void) const
{
	return to_str(get_massunit());
}

double Aircraft::WeightBalance::get_useable_fuelmass(void) const
{
	double m(0);
	for (elements_t::const_iterator i(m_elements.begin()), e(m_elements.end()); i != e; ++i) {
		const Aircraft::WeightBalance::Element& el(*i);
		if (!(el.get_flags() & Aircraft::WeightBalance::Element::flag_fuel))
			continue;
		m += el.get_massmax() - el.get_massmin();
	}
	return m;
}

double Aircraft::WeightBalance::get_total_fuelmass(void) const
{
	double m(0);
	for (elements_t::const_iterator i(m_elements.begin()), e(m_elements.end()); i != e; ++i) {
		const Aircraft::WeightBalance::Element& el(*i);
		if (!(el.get_flags() & Aircraft::WeightBalance::Element::flag_fuel))
			continue;
		m += el.get_massmax();
	}
	return m;
}

double Aircraft::WeightBalance::get_useable_oilmass(void) const
{
	double m(0);
	for (elements_t::const_iterator i(m_elements.begin()), e(m_elements.end()); i != e; ++i) {
		const Aircraft::WeightBalance::Element& el(*i);
		if (!(el.get_flags() & Aircraft::WeightBalance::Element::flag_oil))
			continue;
		m += el.get_massmax() - el.get_massmin();
	}
	return m;
}

double Aircraft::WeightBalance::get_total_oilmass(void) const
{
	double m(0);
	for (elements_t::const_iterator i(m_elements.begin()), e(m_elements.end()); i != e; ++i) {
		const Aircraft::WeightBalance::Element& el(*i);
		if (!(el.get_flags() & Aircraft::WeightBalance::Element::flag_oil))
			continue;
		m += el.get_massmax();
	}
	return m;
}

double Aircraft::WeightBalance::get_min_mass(void) const
{
	double m(0);
	for (elements_t::const_iterator i(m_elements.begin()), e(m_elements.end()); i != e; ++i) {
		const Aircraft::WeightBalance::Element& el(*i);
		m += el.get_massmin();
	}
	return m;
}

Aircraft::WeightBalance::Element::flags_t Aircraft::WeightBalance::get_element_flags(void) const
{
	Element::flags_t flg(Element::flag_none);
	for (elements_t::const_iterator i(m_elements.begin()), e(m_elements.end()); i != e; ++i) {
		const Aircraft::WeightBalance::Element& el(*i);
		flg |= el.get_flags();
	}
	return flg;
}

bool Aircraft::WeightBalance::has_seats(void) const
{
	for (elements_t::const_iterator i(m_elements.begin()), e(m_elements.end()); i != e; ++i) {
		const Aircraft::WeightBalance::Element& el(*i);
		if (el.get_flags() & Element::flag_seating)
			return true;
	}
	return false;
}

unsigned int Aircraft::WeightBalance::get_seats(void) const
{
	unsigned int seats(0);
	for (elements_t::const_iterator i(m_elements.begin()), e(m_elements.end()); i != e; ++i) {
		const Aircraft::WeightBalance::Element& el(*i);
		if (el.get_flags() & Element::flag_seating)
			seats += el.get_seats();
	}
	return seats;
}

bool Aircraft::WeightBalance::add_auto_units(unit_t fuelunit, unit_t oilunit)
{
	bool work(false);
	for (elements_t::iterator i(m_elements.begin()), e(m_elements.end()); i != e; ++i) {
		if (i->get_nrunits())
			continue;
		if (i->get_flags() & Element::flag_fixed) {
			i->add_unit(to_str(m_massunit), 1, 0);
			work = true;
			continue;
		}
		i->add_auto_units(m_massunit, fuelunit, oilunit);
		work = true;
	}
	return work;
}

void Aircraft::WeightBalance::set_units(unit_t armunit, unit_t massunit)
{
	bool armok(!!((1 << armunit) & unitmask_length));
	bool massok(!!((1 << massunit) & unitmask_mass));
	if (armok)
		m_armunit = armunit;
	if (massok)
		m_massunit = massunit;
	if (armok && massok)
		return;
	std::ostringstream oss;
	oss << "Aircraft::WeightBalance::set_units: invalid units for ";
	if (!armok)
		oss << "arm";
	if (!(armok || massok))
		oss << ", ";
	if (!massok)
		oss << "mass";
	throw std::runtime_error(oss.str());
}

void Aircraft::WeightBalance::limit(elementvalues_t& ev) const
{
	for (elements_t::size_type i(0), n(std::min(m_elements.size(), ev.size())); i < n; ++i) {
		const Element& el(m_elements[i]);
		ElementValue& evs(ev[i]);
		if (evs.get_unit() >= el.get_nrunits()) {
			evs.set_unit(0);
			if (!el.get_nrunits())
				continue;
		}
		const Element::Unit& unit(el.get_unit(evs.get_unit()));
		if (std::isnan(unit.get_factor()) || std::isnan(unit.get_offset()) || unit.get_factor() == 0)
			continue;
		double mass(evs.get_value() * unit.get_factor() + unit.get_offset());
		if (mass < el.get_massmin())
			mass = el.get_massmin();
		if (mass > el.get_massmax())
			mass = el.get_massmax();
		evs.set_value((mass - unit.get_offset()) / unit.get_factor());
	}
}
