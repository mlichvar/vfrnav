/*****************************************************************************/

/*
 *      grib2.cc  --  Gridded Binary weather files.
 *
 *      Copyright (C) 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019  Thomas Sailer (t.sailer@alumni.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/*****************************************************************************/

#include "grib2.h"
#include "baro.h"
#include "fplan.h"
#include "SunriseSunset.h"
#include "metgraph.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <cmath>
#include <stdexcept>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <set>
#include <giomm.h>
#ifdef HAVE_OPENJPEG
#include <openjpeg.h>
#endif
#ifdef HAVE_EIGEN3
#include <Eigen/Dense>
#endif
#ifdef HAVE_UNISTD
#include <unistd.h>
#endif
#ifdef HAVE_SYS_MMAN_H
#include <sys/mman.h>
#endif

constexpr uint8_t GRIB2::surface_ground_or_water;
constexpr uint8_t GRIB2::surface_cloud_base;
constexpr uint8_t GRIB2::surface_cloud_top;
constexpr uint8_t GRIB2::surface_0degc_isotherm;
constexpr uint8_t GRIB2::surface_lifted_adiabatic_condensation;
constexpr uint8_t GRIB2::surface_maximum_wind;
constexpr uint8_t GRIB2::surface_tropopause;
constexpr uint8_t GRIB2::surface_nominal_atmosphere_top;
constexpr uint8_t GRIB2::surface_sea_bottom;
constexpr uint8_t GRIB2::surface_entire_atmosphere;
constexpr uint8_t GRIB2::surface_cumulonimbus_base;
constexpr uint8_t GRIB2::surface_cumulonimbus_top;
constexpr uint8_t GRIB2::surface_isothermal_level;
constexpr uint8_t GRIB2::surface_isobaric_surface;
constexpr uint8_t GRIB2::surface_mean_sea_level;
constexpr uint8_t GRIB2::surface_specific_altitude_amsl;
constexpr uint8_t GRIB2::surface_specific_height_gnd;
constexpr uint8_t GRIB2::surface_sigma_level;
constexpr uint8_t GRIB2::surface_hybrid_level;
constexpr uint8_t GRIB2::surface_depth_below_land_surface;
constexpr uint8_t GRIB2::surface_isentropic_level;
constexpr uint8_t GRIB2::surface_level_specific_pressure_difference;
constexpr uint8_t GRIB2::surface_potential_vorticity_surface;
constexpr uint8_t GRIB2::surface_eta_level;
constexpr uint8_t GRIB2::surface_logarithmic_hybrid_coordinate;
constexpr uint8_t GRIB2::surface_mixed_layer_depth;
constexpr uint8_t GRIB2::surface_hybrid_height_level;
constexpr uint8_t GRIB2::surface_hybrid_pressure_level;
constexpr uint8_t GRIB2::surface_pressure_thickness;
constexpr uint8_t GRIB2::surface_generalized_vertical_height_coordinate;
constexpr uint8_t GRIB2::surface_depth_below_sealevel;
constexpr uint8_t GRIB2::surface_depth_below_water_surface;
constexpr uint8_t GRIB2::surface_lake_bottom;
constexpr uint8_t GRIB2::surface_sediment_bottom;
constexpr uint8_t GRIB2::surface_thermally_active_sediment_bottom;
constexpr uint8_t GRIB2::surface_thermal_wave_penetrated_sediment_bottom;
constexpr uint8_t GRIB2::surface_maxing_layer;
constexpr uint8_t GRIB2::surface_ionospheric_d_level;
constexpr uint8_t GRIB2::surface_ionospheric_e_level;
constexpr uint8_t GRIB2::surface_ionospheric_f1_level;
constexpr uint8_t GRIB2::surface_ionospheric_f2_level;
constexpr uint8_t GRIB2::surface_entire_atmosphere_as_single_layer;
constexpr uint8_t GRIB2::surface_entire_ocean_as_single_layer;
constexpr uint8_t GRIB2::surface_highest_tropospheric_freezing_level;
constexpr uint8_t GRIB2::surface_grid_scale_cloud_bottom;
constexpr uint8_t GRIB2::surface_grid_scale_cloud_top;
constexpr uint8_t GRIB2::surface_boundary_layer_cloud_bottom;
constexpr uint8_t GRIB2::surface_boundary_layer_cloud_top;
constexpr uint8_t GRIB2::surface_boundary_layer_cloud;
constexpr uint8_t GRIB2::surface_low_cloud_bottom;
constexpr uint8_t GRIB2::surface_low_cloud_top;
constexpr uint8_t GRIB2::surface_low_cloud;
constexpr uint8_t GRIB2::surface_cloud_ceiling;
constexpr uint8_t GRIB2::surface_planetary_boundary_layer;
constexpr uint8_t GRIB2::surface_layer_between_hybrid_levels;
constexpr uint8_t GRIB2::surface_middle_cloud_bottom;
constexpr uint8_t GRIB2::surface_middle_cloud_top;
constexpr uint8_t GRIB2::surface_middle_cloud;
constexpr uint8_t GRIB2::surface_top_cloud_bottom;
constexpr uint8_t GRIB2::surface_top_cloud_top;
constexpr uint8_t GRIB2::surface_top_cloud;
constexpr uint8_t GRIB2::surface_ocean_isotherm_level;
constexpr uint8_t GRIB2::surface_layer_between_depths_below_ocean_surface;
constexpr uint8_t GRIB2::surface_ocean_mixing_layer_bottom;
constexpr uint8_t GRIB2::surface_ocean_isothermal_layer_bottom;
constexpr uint8_t GRIB2::surface_ocean_surface_26c_isothermal;
constexpr uint8_t GRIB2::surface_ocean_mixing_layer;
constexpr uint8_t GRIB2::surface_ordered_sequence_of_data;
constexpr uint8_t GRIB2::surface_convective_cloud_bottom;
constexpr uint8_t GRIB2::surface_convective_cloud_top;
constexpr uint8_t GRIB2::surface_convective_cloud;
constexpr uint8_t GRIB2::surface_lowest_wet_bulb_zero;
constexpr uint8_t GRIB2::surface_maximum_equivalent_potential_temperature;
constexpr uint8_t GRIB2::surface_equilibrium_level;
constexpr uint8_t GRIB2::surface_shallow_convective_cloud_bottom;
constexpr uint8_t GRIB2::surface_shallow_convective_cloud_top;
constexpr uint8_t GRIB2::surface_shallow_convective_cloud;
constexpr uint8_t GRIB2::surface_deep_convective_cloud_top;
constexpr uint8_t GRIB2::surface_supercooled_liquid_water_bottom;
constexpr uint8_t GRIB2::surface_supercooled_liquid_water_top;
constexpr uint8_t GRIB2::surface_missing;

constexpr uint8_t GRIB2::discipline_meteorology;
constexpr uint8_t GRIB2::discipline_hydrology;
constexpr uint8_t GRIB2::discipline_landsurface;
constexpr uint8_t GRIB2::discipline_space;
constexpr uint8_t GRIB2::discipline_spaceweather;
constexpr uint8_t GRIB2::discipline_oceanography;

constexpr uint16_t GRIB2::paramcat_meteorology_temperature;
constexpr uint16_t GRIB2::paramcat_meteorology_moisture;
constexpr uint16_t GRIB2::paramcat_meteorology_momentum;
constexpr uint16_t GRIB2::paramcat_meteorology_mass;
constexpr uint16_t GRIB2::paramcat_meteorology_shortwave_radiation;
constexpr uint16_t GRIB2::paramcat_meteorology_longwave_radiation;
constexpr uint16_t GRIB2::paramcat_meteorology_cloud;
constexpr uint16_t GRIB2::paramcat_meteorology_thermodynamic_stability;
constexpr uint16_t GRIB2::paramcat_meteorology_kinematic_stability;
constexpr uint16_t GRIB2::paramcat_meteorology_temperature_prob;
constexpr uint16_t GRIB2::paramcat_meteorology_moisture_prob;
constexpr uint16_t GRIB2::paramcat_meteorology_momentum_prob;
constexpr uint16_t GRIB2::paramcat_meteorology_mass_probabilities;
constexpr uint16_t GRIB2::paramcat_meteorology_aerosols;
constexpr uint16_t GRIB2::paramcat_meteorology_trace_gases;
constexpr uint16_t GRIB2::paramcat_meteorology_radar;
constexpr uint16_t GRIB2::paramcat_meteorology_forecast_radar;
constexpr uint16_t GRIB2::paramcat_meteorology_electrodynamics;
constexpr uint16_t GRIB2::paramcat_meteorology_nuclear;
constexpr uint16_t GRIB2::paramcat_meteorology_atmosphere_physics;
constexpr uint16_t GRIB2::paramcat_meteorology_atmosphere_chemistry;
constexpr uint16_t GRIB2::paramcat_meteorology_string;
constexpr uint16_t GRIB2::paramcat_meteorology_miscellaneous;
constexpr uint16_t GRIB2::paramcat_meteorology_covariance;
constexpr uint16_t GRIB2::paramcat_meteorology_missing;
constexpr uint16_t GRIB2::paramcat_hydrology_basic;
constexpr uint16_t GRIB2::paramcat_hydrology_prob;
constexpr uint16_t GRIB2::paramcat_hydrology_inlandwater_sediment;
constexpr uint16_t GRIB2::paramcat_hydrology_missing;
constexpr uint16_t GRIB2::paramcat_landsurface_vegetation;
constexpr uint16_t GRIB2::paramcat_landsurface_agriaquaculture;
constexpr uint16_t GRIB2::paramcat_landsurface_transportation;
constexpr uint16_t GRIB2::paramcat_landsurface_soil;
constexpr uint16_t GRIB2::paramcat_landsurface_fire;
constexpr uint16_t GRIB2::paramcat_landsurface_missing;
constexpr uint16_t GRIB2::paramcat_space_image;
constexpr uint16_t GRIB2::paramcat_space_quantitative;
constexpr uint16_t GRIB2::paramcat_space_forecast;
constexpr uint16_t GRIB2::paramcat_space_missing;
constexpr uint16_t GRIB2::paramcat_spaceweather_temperature;
constexpr uint16_t GRIB2::paramcat_spaceweather_momentum;
constexpr uint16_t GRIB2::paramcat_spaceweather_charged_particles;
constexpr uint16_t GRIB2::paramcat_spaceweather_fields;
constexpr uint16_t GRIB2::paramcat_spaceweather_energetic_particles;
constexpr uint16_t GRIB2::paramcat_spaceweather_waves;
constexpr uint16_t GRIB2::paramcat_spaceweather_solar;
constexpr uint16_t GRIB2::paramcat_spaceweather_terrestrial;
constexpr uint16_t GRIB2::paramcat_spaceweather_imagery;
constexpr uint16_t GRIB2::paramcat_spaceweather_ionneutral_coupling;
constexpr uint16_t GRIB2::paramcat_spaceweather_missing;
constexpr uint16_t GRIB2::paramcat_oceanography_waves;
constexpr uint16_t GRIB2::paramcat_oceanography_currents;
constexpr uint16_t GRIB2::paramcat_oceanography_ice;
constexpr uint16_t GRIB2::paramcat_oceanography_surface;
constexpr uint16_t GRIB2::paramcat_oceanography_subsurface;
constexpr uint16_t GRIB2::paramcat_oceanography_miscellaneous;
constexpr uint16_t GRIB2::paramcat_oceanography_missing;

constexpr uint32_t GRIB2::param_meteorology_temperature_tmp;
constexpr uint32_t GRIB2::param_meteorology_temperature_vtmp;
constexpr uint32_t GRIB2::param_meteorology_temperature_pot;
constexpr uint32_t GRIB2::param_meteorology_temperature_epot;
constexpr uint32_t GRIB2::param_meteorology_temperature_tmax;
constexpr uint32_t GRIB2::param_meteorology_temperature_tmin;
constexpr uint32_t GRIB2::param_meteorology_temperature_dpt;
constexpr uint32_t GRIB2::param_meteorology_temperature_depr;
constexpr uint32_t GRIB2::param_meteorology_temperature_lapr;
constexpr uint32_t GRIB2::param_meteorology_temperature_tmpa;
constexpr uint32_t GRIB2::param_meteorology_temperature_lhtfl;
constexpr uint32_t GRIB2::param_meteorology_temperature_shtfl;
constexpr uint32_t GRIB2::param_meteorology_temperature_heatx;
constexpr uint32_t GRIB2::param_meteorology_temperature_wcf;
constexpr uint32_t GRIB2::param_meteorology_temperature_mindpd;
constexpr uint32_t GRIB2::param_meteorology_temperature_vptmp;
constexpr uint32_t GRIB2::param_meteorology_temperature_snohf_2;
constexpr uint32_t GRIB2::param_meteorology_temperature_skint;
constexpr uint32_t GRIB2::param_meteorology_temperature_snot;
constexpr uint32_t GRIB2::param_meteorology_temperature_ttcht;
constexpr uint32_t GRIB2::param_meteorology_temperature_tdcht;
constexpr uint32_t GRIB2::param_meteorology_temperature_snohf;
constexpr uint32_t GRIB2::param_meteorology_temperature_ttrad;
constexpr uint32_t GRIB2::param_meteorology_temperature_rev;
constexpr uint32_t GRIB2::param_meteorology_temperature_lrghr;
constexpr uint32_t GRIB2::param_meteorology_temperature_cnvhr;
constexpr uint32_t GRIB2::param_meteorology_temperature_thflx;
constexpr uint32_t GRIB2::param_meteorology_temperature_ttdia;
constexpr uint32_t GRIB2::param_meteorology_temperature_ttphy;
constexpr uint32_t GRIB2::param_meteorology_temperature_tsd1d;
constexpr uint32_t GRIB2::param_meteorology_temperature_shahr;
constexpr uint32_t GRIB2::param_meteorology_temperature_vdfhr;
constexpr uint32_t GRIB2::param_meteorology_temperature_thz0;
constexpr uint32_t GRIB2::param_meteorology_temperature_tchp;
constexpr uint32_t GRIB2::param_meteorology_moisture_spfh;
constexpr uint32_t GRIB2::param_meteorology_moisture_rh;
constexpr uint32_t GRIB2::param_meteorology_moisture_mixr;
constexpr uint32_t GRIB2::param_meteorology_moisture_pwat;
constexpr uint32_t GRIB2::param_meteorology_moisture_vapp;
constexpr uint32_t GRIB2::param_meteorology_moisture_satd;
constexpr uint32_t GRIB2::param_meteorology_moisture_evp;
constexpr uint32_t GRIB2::param_meteorology_moisture_prate;
constexpr uint32_t GRIB2::param_meteorology_moisture_apcp;
constexpr uint32_t GRIB2::param_meteorology_moisture_ncpcp;
constexpr uint32_t GRIB2::param_meteorology_moisture_acpcp;
constexpr uint32_t GRIB2::param_meteorology_moisture_snod;
constexpr uint32_t GRIB2::param_meteorology_moisture_srweq;
constexpr uint32_t GRIB2::param_meteorology_moisture_weasd;
constexpr uint32_t GRIB2::param_meteorology_moisture_snoc;
constexpr uint32_t GRIB2::param_meteorology_moisture_snol;
constexpr uint32_t GRIB2::param_meteorology_moisture_snom;
constexpr uint32_t GRIB2::param_meteorology_moisture_snoag;
constexpr uint32_t GRIB2::param_meteorology_moisture_absh;
constexpr uint32_t GRIB2::param_meteorology_moisture_ptype;
constexpr uint32_t GRIB2::param_meteorology_moisture_iliqw;
constexpr uint32_t GRIB2::param_meteorology_moisture_tcond;
constexpr uint32_t GRIB2::param_meteorology_moisture_clwmr;
constexpr uint32_t GRIB2::param_meteorology_moisture_icmr;
constexpr uint32_t GRIB2::param_meteorology_moisture_rwmr;
constexpr uint32_t GRIB2::param_meteorology_moisture_snmr;
constexpr uint32_t GRIB2::param_meteorology_moisture_mconv;
constexpr uint32_t GRIB2::param_meteorology_moisture_maxrh;
constexpr uint32_t GRIB2::param_meteorology_moisture_maxah;
constexpr uint32_t GRIB2::param_meteorology_moisture_asnow;
constexpr uint32_t GRIB2::param_meteorology_moisture_pwcat;
constexpr uint32_t GRIB2::param_meteorology_moisture_hail;
constexpr uint32_t GRIB2::param_meteorology_moisture_grle;
constexpr uint32_t GRIB2::param_meteorology_moisture_crain;
constexpr uint32_t GRIB2::param_meteorology_moisture_cfrzr;
constexpr uint32_t GRIB2::param_meteorology_moisture_cicep;
constexpr uint32_t GRIB2::param_meteorology_moisture_csnow;
constexpr uint32_t GRIB2::param_meteorology_moisture_cprat;
constexpr uint32_t GRIB2::param_meteorology_moisture_mdiv;
constexpr uint32_t GRIB2::param_meteorology_moisture_cpofp;
constexpr uint32_t GRIB2::param_meteorology_moisture_pevap;
constexpr uint32_t GRIB2::param_meteorology_moisture_pevpr;
constexpr uint32_t GRIB2::param_meteorology_moisture_snowc;
constexpr uint32_t GRIB2::param_meteorology_moisture_frain;
constexpr uint32_t GRIB2::param_meteorology_moisture_rime;
constexpr uint32_t GRIB2::param_meteorology_moisture_tcolr;
constexpr uint32_t GRIB2::param_meteorology_moisture_tcols;
constexpr uint32_t GRIB2::param_meteorology_moisture_lswp;
constexpr uint32_t GRIB2::param_meteorology_moisture_cwp;
constexpr uint32_t GRIB2::param_meteorology_moisture_twatp;
constexpr uint32_t GRIB2::param_meteorology_moisture_tsnowp;
constexpr uint32_t GRIB2::param_meteorology_moisture_tcwat;
constexpr uint32_t GRIB2::param_meteorology_moisture_tprate;
constexpr uint32_t GRIB2::param_meteorology_moisture_tsrwe;
constexpr uint32_t GRIB2::param_meteorology_moisture_lsprate;
constexpr uint32_t GRIB2::param_meteorology_moisture_csrwe;
constexpr uint32_t GRIB2::param_meteorology_moisture_lssrwe;
constexpr uint32_t GRIB2::param_meteorology_moisture_tsrate;
constexpr uint32_t GRIB2::param_meteorology_moisture_csrate;
constexpr uint32_t GRIB2::param_meteorology_moisture_lssrate;
constexpr uint32_t GRIB2::param_meteorology_moisture_sdwe;
constexpr uint32_t GRIB2::param_meteorology_moisture_sden;
constexpr uint32_t GRIB2::param_meteorology_moisture_sevap;
constexpr uint32_t GRIB2::param_meteorology_moisture_tciwv;
constexpr uint32_t GRIB2::param_meteorology_moisture_rprate;
constexpr uint32_t GRIB2::param_meteorology_moisture_sprate;
constexpr uint32_t GRIB2::param_meteorology_moisture_fprate;
constexpr uint32_t GRIB2::param_meteorology_moisture_iprate;
constexpr uint32_t GRIB2::param_meteorology_moisture_tcolw;
constexpr uint32_t GRIB2::param_meteorology_moisture_tcoli;
constexpr uint32_t GRIB2::param_meteorology_moisture_hailmxr;
constexpr uint32_t GRIB2::param_meteorology_moisture_tcolh;
constexpr uint32_t GRIB2::param_meteorology_moisture_hailpr;
constexpr uint32_t GRIB2::param_meteorology_moisture_tcolg;
constexpr uint32_t GRIB2::param_meteorology_moisture_gprate;
constexpr uint32_t GRIB2::param_meteorology_moisture_crrate;
constexpr uint32_t GRIB2::param_meteorology_moisture_lsrrate;
constexpr uint32_t GRIB2::param_meteorology_moisture_tcolwa;
constexpr uint32_t GRIB2::param_meteorology_moisture_evarate;
constexpr uint32_t GRIB2::param_meteorology_moisture_totcon;
constexpr uint32_t GRIB2::param_meteorology_moisture_tcicon;
constexpr uint32_t GRIB2::param_meteorology_moisture_cimixr;
constexpr uint32_t GRIB2::param_meteorology_moisture_scllwc;
constexpr uint32_t GRIB2::param_meteorology_moisture_scliwc;
constexpr uint32_t GRIB2::param_meteorology_moisture_srainw;
constexpr uint32_t GRIB2::param_meteorology_moisture_ssnoww;
constexpr uint32_t GRIB2::param_meteorology_moisture_tkmflx;
constexpr uint32_t GRIB2::param_meteorology_moisture_ukmflx;
constexpr uint32_t GRIB2::param_meteorology_moisture_vkmflx;
constexpr uint32_t GRIB2::param_meteorology_moisture_crain_2;
constexpr uint32_t GRIB2::param_meteorology_moisture_cfrzr_2;
constexpr uint32_t GRIB2::param_meteorology_moisture_cicep_2;
constexpr uint32_t GRIB2::param_meteorology_moisture_csnow_2;
constexpr uint32_t GRIB2::param_meteorology_moisture_cprat_2;
constexpr uint32_t GRIB2::param_meteorology_moisture_mdiv_2;
constexpr uint32_t GRIB2::param_meteorology_moisture_minrh;
constexpr uint32_t GRIB2::param_meteorology_moisture_pevap_2;
constexpr uint32_t GRIB2::param_meteorology_moisture_pevpr_2;
constexpr uint32_t GRIB2::param_meteorology_moisture_snowc_2;
constexpr uint32_t GRIB2::param_meteorology_moisture_frain_2;
constexpr uint32_t GRIB2::param_meteorology_moisture_rime_2;
constexpr uint32_t GRIB2::param_meteorology_moisture_tcolr_2;
constexpr uint32_t GRIB2::param_meteorology_moisture_tcols_2;
constexpr uint32_t GRIB2::param_meteorology_moisture_tipd;
constexpr uint32_t GRIB2::param_meteorology_moisture_ncip;
constexpr uint32_t GRIB2::param_meteorology_moisture_snot;
constexpr uint32_t GRIB2::param_meteorology_moisture_tclsw;
constexpr uint32_t GRIB2::param_meteorology_moisture_tcolm;
constexpr uint32_t GRIB2::param_meteorology_moisture_emnp;
constexpr uint32_t GRIB2::param_meteorology_moisture_sbsno;
constexpr uint32_t GRIB2::param_meteorology_moisture_cnvmr;
constexpr uint32_t GRIB2::param_meteorology_moisture_shamr;
constexpr uint32_t GRIB2::param_meteorology_moisture_vdfmr;
constexpr uint32_t GRIB2::param_meteorology_moisture_condp;
constexpr uint32_t GRIB2::param_meteorology_moisture_lrgmr;
constexpr uint32_t GRIB2::param_meteorology_moisture_qz0;
constexpr uint32_t GRIB2::param_meteorology_moisture_qmax;
constexpr uint32_t GRIB2::param_meteorology_moisture_qmin;
constexpr uint32_t GRIB2::param_meteorology_moisture_arain;
constexpr uint32_t GRIB2::param_meteorology_moisture_snowt;
constexpr uint32_t GRIB2::param_meteorology_moisture_apcpn;
constexpr uint32_t GRIB2::param_meteorology_moisture_acpcpn;
constexpr uint32_t GRIB2::param_meteorology_moisture_frzr;
constexpr uint32_t GRIB2::param_meteorology_moisture_pwther;
constexpr uint32_t GRIB2::param_meteorology_moisture_frozr;
constexpr uint32_t GRIB2::param_meteorology_moisture_tsnow;
constexpr uint32_t GRIB2::param_meteorology_moisture_rhpw;
constexpr uint32_t GRIB2::param_meteorology_momentum_wdir;
constexpr uint32_t GRIB2::param_meteorology_momentum_wind;
constexpr uint32_t GRIB2::param_meteorology_momentum_ugrd;
constexpr uint32_t GRIB2::param_meteorology_momentum_vgrd;
constexpr uint32_t GRIB2::param_meteorology_momentum_strm;
constexpr uint32_t GRIB2::param_meteorology_momentum_vpot;
constexpr uint32_t GRIB2::param_meteorology_momentum_mntsf;
constexpr uint32_t GRIB2::param_meteorology_momentum_sgcvv;
constexpr uint32_t GRIB2::param_meteorology_momentum_vvel;
constexpr uint32_t GRIB2::param_meteorology_momentum_dzdt;
constexpr uint32_t GRIB2::param_meteorology_momentum_absv;
constexpr uint32_t GRIB2::param_meteorology_momentum_absd;
constexpr uint32_t GRIB2::param_meteorology_momentum_relv;
constexpr uint32_t GRIB2::param_meteorology_momentum_reld;
constexpr uint32_t GRIB2::param_meteorology_momentum_pvort;
constexpr uint32_t GRIB2::param_meteorology_momentum_vucsh;
constexpr uint32_t GRIB2::param_meteorology_momentum_vvcsh;
constexpr uint32_t GRIB2::param_meteorology_momentum_uflx;
constexpr uint32_t GRIB2::param_meteorology_momentum_vflx;
constexpr uint32_t GRIB2::param_meteorology_momentum_wmixe;
constexpr uint32_t GRIB2::param_meteorology_momentum_blydp;
constexpr uint32_t GRIB2::param_meteorology_momentum_maxgust;
constexpr uint32_t GRIB2::param_meteorology_momentum_gust;
constexpr uint32_t GRIB2::param_meteorology_momentum_ugust;
constexpr uint32_t GRIB2::param_meteorology_momentum_vgust;
constexpr uint32_t GRIB2::param_meteorology_momentum_vwsh;
constexpr uint32_t GRIB2::param_meteorology_momentum_mflx;
constexpr uint32_t GRIB2::param_meteorology_momentum_ustm;
constexpr uint32_t GRIB2::param_meteorology_momentum_vstm;
constexpr uint32_t GRIB2::param_meteorology_momentum_cd;
constexpr uint32_t GRIB2::param_meteorology_momentum_fricv;
constexpr uint32_t GRIB2::param_meteorology_momentum_tdcmom;
constexpr uint32_t GRIB2::param_meteorology_momentum_etacvv;
constexpr uint32_t GRIB2::param_meteorology_momentum_windf;
constexpr uint32_t GRIB2::param_meteorology_momentum_vwsh_2;
constexpr uint32_t GRIB2::param_meteorology_momentum_mflx_2;
constexpr uint32_t GRIB2::param_meteorology_momentum_ustm_2;
constexpr uint32_t GRIB2::param_meteorology_momentum_vstm_2;
constexpr uint32_t GRIB2::param_meteorology_momentum_cd_2;
constexpr uint32_t GRIB2::param_meteorology_momentum_fricv_2;
constexpr uint32_t GRIB2::param_meteorology_momentum_lauv;
constexpr uint32_t GRIB2::param_meteorology_momentum_louv;
constexpr uint32_t GRIB2::param_meteorology_momentum_lavv;
constexpr uint32_t GRIB2::param_meteorology_momentum_lovv;
constexpr uint32_t GRIB2::param_meteorology_momentum_lapp;
constexpr uint32_t GRIB2::param_meteorology_momentum_lopp;
constexpr uint32_t GRIB2::param_meteorology_momentum_vedh;
constexpr uint32_t GRIB2::param_meteorology_momentum_covmz;
constexpr uint32_t GRIB2::param_meteorology_momentum_covtz;
constexpr uint32_t GRIB2::param_meteorology_momentum_covtm;
constexpr uint32_t GRIB2::param_meteorology_momentum_vdfua;
constexpr uint32_t GRIB2::param_meteorology_momentum_vdfva;
constexpr uint32_t GRIB2::param_meteorology_momentum_gwdu;
constexpr uint32_t GRIB2::param_meteorology_momentum_gwdv;
constexpr uint32_t GRIB2::param_meteorology_momentum_cnvu;
constexpr uint32_t GRIB2::param_meteorology_momentum_cnvv;
constexpr uint32_t GRIB2::param_meteorology_momentum_wtend;
constexpr uint32_t GRIB2::param_meteorology_momentum_omgalf;
constexpr uint32_t GRIB2::param_meteorology_momentum_cngwdu;
constexpr uint32_t GRIB2::param_meteorology_momentum_cngwdv;
constexpr uint32_t GRIB2::param_meteorology_momentum_lmv;
constexpr uint32_t GRIB2::param_meteorology_momentum_pvmww;
constexpr uint32_t GRIB2::param_meteorology_momentum_maxuvv;
constexpr uint32_t GRIB2::param_meteorology_momentum_maxdvv;
constexpr uint32_t GRIB2::param_meteorology_momentum_maxuw;
constexpr uint32_t GRIB2::param_meteorology_momentum_maxvw;
constexpr uint32_t GRIB2::param_meteorology_momentum_vrate;
constexpr uint32_t GRIB2::param_meteorology_mass_pres;
constexpr uint32_t GRIB2::param_meteorology_mass_prmsl;
constexpr uint32_t GRIB2::param_meteorology_mass_ptend;
constexpr uint32_t GRIB2::param_meteorology_mass_icaht;
constexpr uint32_t GRIB2::param_meteorology_mass_gp;
constexpr uint32_t GRIB2::param_meteorology_mass_hgt;
constexpr uint32_t GRIB2::param_meteorology_mass_dist;
constexpr uint32_t GRIB2::param_meteorology_mass_hstdv;
constexpr uint32_t GRIB2::param_meteorology_mass_presa;
constexpr uint32_t GRIB2::param_meteorology_mass_gpa;
constexpr uint32_t GRIB2::param_meteorology_mass_den;
constexpr uint32_t GRIB2::param_meteorology_mass_alts;
constexpr uint32_t GRIB2::param_meteorology_mass_thick;
constexpr uint32_t GRIB2::param_meteorology_mass_presalt;
constexpr uint32_t GRIB2::param_meteorology_mass_denalt;
constexpr uint32_t GRIB2::param_meteorology_mass_5wavh;
constexpr uint32_t GRIB2::param_meteorology_mass_hpbl;
constexpr uint32_t GRIB2::param_meteorology_mass_5wava;
constexpr uint32_t GRIB2::param_meteorology_mass_sdsgso;
constexpr uint32_t GRIB2::param_meteorology_mass_aosgso;
constexpr uint32_t GRIB2::param_meteorology_mass_ssgso;
constexpr uint32_t GRIB2::param_meteorology_mass_gwd;
constexpr uint32_t GRIB2::param_meteorology_mass_asgso;
constexpr uint32_t GRIB2::param_meteorology_mass_nlpres;
constexpr uint32_t GRIB2::param_meteorology_mass_mslet;
constexpr uint32_t GRIB2::param_meteorology_mass_5wavh_2;
constexpr uint32_t GRIB2::param_meteorology_mass_hpbl_2;
constexpr uint32_t GRIB2::param_meteorology_mass_5wava_2;
constexpr uint32_t GRIB2::param_meteorology_mass_mslma;
constexpr uint32_t GRIB2::param_meteorology_mass_tslsa;
constexpr uint32_t GRIB2::param_meteorology_mass_plpl;
constexpr uint32_t GRIB2::param_meteorology_mass_lpsx;
constexpr uint32_t GRIB2::param_meteorology_mass_lpsy;
constexpr uint32_t GRIB2::param_meteorology_mass_hgtx;
constexpr uint32_t GRIB2::param_meteorology_mass_hgty;
constexpr uint32_t GRIB2::param_meteorology_mass_layth;
constexpr uint32_t GRIB2::param_meteorology_mass_nlgsp;
constexpr uint32_t GRIB2::param_meteorology_mass_cnvumf;
constexpr uint32_t GRIB2::param_meteorology_mass_cnvdmf;
constexpr uint32_t GRIB2::param_meteorology_mass_cnvdemf;
constexpr uint32_t GRIB2::param_meteorology_mass_lmh;
constexpr uint32_t GRIB2::param_meteorology_mass_hgtn;
constexpr uint32_t GRIB2::param_meteorology_mass_presn;
constexpr uint32_t GRIB2::param_meteorology_shortwave_radiation_nswrs;
constexpr uint32_t GRIB2::param_meteorology_shortwave_radiation_nswrt;
constexpr uint32_t GRIB2::param_meteorology_shortwave_radiation_swavr;
constexpr uint32_t GRIB2::param_meteorology_shortwave_radiation_grad;
constexpr uint32_t GRIB2::param_meteorology_shortwave_radiation_brtmp;
constexpr uint32_t GRIB2::param_meteorology_shortwave_radiation_lwrad;
constexpr uint32_t GRIB2::param_meteorology_shortwave_radiation_swrad;
constexpr uint32_t GRIB2::param_meteorology_shortwave_radiation_dswrf;
constexpr uint32_t GRIB2::param_meteorology_shortwave_radiation_uswrf;
constexpr uint32_t GRIB2::param_meteorology_shortwave_radiation_nswrf;
constexpr uint32_t GRIB2::param_meteorology_shortwave_radiation_photar;
constexpr uint32_t GRIB2::param_meteorology_shortwave_radiation_nswrfcs;
constexpr uint32_t GRIB2::param_meteorology_shortwave_radiation_dwuvr;
constexpr uint32_t GRIB2::param_meteorology_shortwave_radiation_uviucs;
constexpr uint32_t GRIB2::param_meteorology_shortwave_radiation_uvi;
constexpr uint32_t GRIB2::param_meteorology_shortwave_radiation_dswrf_2;
constexpr uint32_t GRIB2::param_meteorology_shortwave_radiation_uswrf_2;
constexpr uint32_t GRIB2::param_meteorology_shortwave_radiation_duvb;
constexpr uint32_t GRIB2::param_meteorology_shortwave_radiation_cduvb;
constexpr uint32_t GRIB2::param_meteorology_shortwave_radiation_csdsf;
constexpr uint32_t GRIB2::param_meteorology_shortwave_radiation_swhr;
constexpr uint32_t GRIB2::param_meteorology_shortwave_radiation_csusf;
constexpr uint32_t GRIB2::param_meteorology_shortwave_radiation_cfnsf;
constexpr uint32_t GRIB2::param_meteorology_shortwave_radiation_vbdsf;
constexpr uint32_t GRIB2::param_meteorology_shortwave_radiation_vddsf;
constexpr uint32_t GRIB2::param_meteorology_shortwave_radiation_nbdsf;
constexpr uint32_t GRIB2::param_meteorology_shortwave_radiation_nddsf;
constexpr uint32_t GRIB2::param_meteorology_shortwave_radiation_dtrf;
constexpr uint32_t GRIB2::param_meteorology_shortwave_radiation_utrf;
constexpr uint32_t GRIB2::param_meteorology_longwave_radiation_nlwrs;
constexpr uint32_t GRIB2::param_meteorology_longwave_radiation_nlwrt;
constexpr uint32_t GRIB2::param_meteorology_longwave_radiation_lwavr;
constexpr uint32_t GRIB2::param_meteorology_longwave_radiation_dlwrf;
constexpr uint32_t GRIB2::param_meteorology_longwave_radiation_ulwrf;
constexpr uint32_t GRIB2::param_meteorology_longwave_radiation_nlwrf;
constexpr uint32_t GRIB2::param_meteorology_longwave_radiation_nlwrcs;
constexpr uint32_t GRIB2::param_meteorology_longwave_radiation_dlwrf_2;
constexpr uint32_t GRIB2::param_meteorology_longwave_radiation_ulwrf_2;
constexpr uint32_t GRIB2::param_meteorology_longwave_radiation_lwhr;
constexpr uint32_t GRIB2::param_meteorology_longwave_radiation_csulf;
constexpr uint32_t GRIB2::param_meteorology_longwave_radiation_csdlf;
constexpr uint32_t GRIB2::param_meteorology_longwave_radiation_cfnlf;
constexpr uint32_t GRIB2::param_meteorology_cloud_cice;
constexpr uint32_t GRIB2::param_meteorology_cloud_tcdc;
constexpr uint32_t GRIB2::param_meteorology_cloud_cdcon;
constexpr uint32_t GRIB2::param_meteorology_cloud_lcdc;
constexpr uint32_t GRIB2::param_meteorology_cloud_mcdc;
constexpr uint32_t GRIB2::param_meteorology_cloud_hcdc;
constexpr uint32_t GRIB2::param_meteorology_cloud_cwat;
constexpr uint32_t GRIB2::param_meteorology_cloud_cdca;
constexpr uint32_t GRIB2::param_meteorology_cloud_cdcty;
constexpr uint32_t GRIB2::param_meteorology_cloud_tmaxt;
constexpr uint32_t GRIB2::param_meteorology_cloud_thunc;
constexpr uint32_t GRIB2::param_meteorology_cloud_cdcb;
constexpr uint32_t GRIB2::param_meteorology_cloud_cdct;
constexpr uint32_t GRIB2::param_meteorology_cloud_ceil;
constexpr uint32_t GRIB2::param_meteorology_cloud_cdlyr;
constexpr uint32_t GRIB2::param_meteorology_cloud_cwork;
constexpr uint32_t GRIB2::param_meteorology_cloud_cuefi;
constexpr uint32_t GRIB2::param_meteorology_cloud_tcond;
constexpr uint32_t GRIB2::param_meteorology_cloud_tcolw;
constexpr uint32_t GRIB2::param_meteorology_cloud_tcoli;
constexpr uint32_t GRIB2::param_meteorology_cloud_tcolc;
constexpr uint32_t GRIB2::param_meteorology_cloud_fice;
constexpr uint32_t GRIB2::param_meteorology_cloud_cdcc;
constexpr uint32_t GRIB2::param_meteorology_cloud_cdcimr;
constexpr uint32_t GRIB2::param_meteorology_cloud_suns;
constexpr uint32_t GRIB2::param_meteorology_cloud_cbhe;
constexpr uint32_t GRIB2::param_meteorology_cloud_hconcb;
constexpr uint32_t GRIB2::param_meteorology_cloud_hconct;
constexpr uint32_t GRIB2::param_meteorology_cloud_nconcd;
constexpr uint32_t GRIB2::param_meteorology_cloud_nccice;
constexpr uint32_t GRIB2::param_meteorology_cloud_ndencd;
constexpr uint32_t GRIB2::param_meteorology_cloud_ndcice;
constexpr uint32_t GRIB2::param_meteorology_cloud_fraccc;
constexpr uint32_t GRIB2::param_meteorology_cloud_sunsd;
constexpr uint32_t GRIB2::param_meteorology_cloud_cdlyr_2;
constexpr uint32_t GRIB2::param_meteorology_cloud_cwork_2;
constexpr uint32_t GRIB2::param_meteorology_cloud_cuefi_2;
constexpr uint32_t GRIB2::param_meteorology_cloud_tcond_2;
constexpr uint32_t GRIB2::param_meteorology_cloud_tcolw_2;
constexpr uint32_t GRIB2::param_meteorology_cloud_tcoli_2;
constexpr uint32_t GRIB2::param_meteorology_cloud_tcolc_2;
constexpr uint32_t GRIB2::param_meteorology_cloud_fice_2;
constexpr uint32_t GRIB2::param_meteorology_cloud_mflux;
constexpr uint32_t GRIB2::param_meteorology_cloud_sunsd_2;
constexpr uint32_t GRIB2::param_meteorology_thermodynamic_stability_pli;
constexpr uint32_t GRIB2::param_meteorology_thermodynamic_stability_bli;
constexpr uint32_t GRIB2::param_meteorology_thermodynamic_stability_kx;
constexpr uint32_t GRIB2::param_meteorology_thermodynamic_stability_kox;
constexpr uint32_t GRIB2::param_meteorology_thermodynamic_stability_totalx;
constexpr uint32_t GRIB2::param_meteorology_thermodynamic_stability_sx;
constexpr uint32_t GRIB2::param_meteorology_thermodynamic_stability_cape;
constexpr uint32_t GRIB2::param_meteorology_thermodynamic_stability_cin;
constexpr uint32_t GRIB2::param_meteorology_thermodynamic_stability_hlcy;
constexpr uint32_t GRIB2::param_meteorology_thermodynamic_stability_ehlx;
constexpr uint32_t GRIB2::param_meteorology_thermodynamic_stability_lftx;
constexpr uint32_t GRIB2::param_meteorology_thermodynamic_stability_4lftx;
constexpr uint32_t GRIB2::param_meteorology_thermodynamic_stability_ri;
constexpr uint32_t GRIB2::param_meteorology_thermodynamic_stability_shwinx;
constexpr uint32_t GRIB2::param_meteorology_thermodynamic_stability_uphl;
constexpr uint32_t GRIB2::param_meteorology_thermodynamic_stability_lftx_2;
constexpr uint32_t GRIB2::param_meteorology_thermodynamic_stability_4lftx_2;
constexpr uint32_t GRIB2::param_meteorology_thermodynamic_stability_ri_2;
constexpr uint32_t GRIB2::param_meteorology_thermodynamic_stability_cwdi;
constexpr uint32_t GRIB2::param_meteorology_thermodynamic_stability_uvi;
constexpr uint32_t GRIB2::param_meteorology_thermodynamic_stability_uphl_2;
constexpr uint32_t GRIB2::param_meteorology_thermodynamic_stability_lai;
constexpr uint32_t GRIB2::param_meteorology_thermodynamic_stability_mxuphl;
constexpr uint32_t GRIB2::param_meteorology_aerosols_aerot;
constexpr uint32_t GRIB2::param_meteorology_aerosols_pmtc;
constexpr uint32_t GRIB2::param_meteorology_aerosols_pmtf;
constexpr uint32_t GRIB2::param_meteorology_aerosols_lpmtf;
constexpr uint32_t GRIB2::param_meteorology_aerosols_lipmf;
constexpr uint32_t GRIB2::param_meteorology_trace_gases_tozne;
constexpr uint32_t GRIB2::param_meteorology_trace_gases_o3mr;
constexpr uint32_t GRIB2::param_meteorology_trace_gases_tcioz;
constexpr uint32_t GRIB2::param_meteorology_trace_gases_o3mr_2;
constexpr uint32_t GRIB2::param_meteorology_trace_gases_ozcon;
constexpr uint32_t GRIB2::param_meteorology_trace_gases_ozcat;
constexpr uint32_t GRIB2::param_meteorology_trace_gases_vdfoz;
constexpr uint32_t GRIB2::param_meteorology_trace_gases_poz;
constexpr uint32_t GRIB2::param_meteorology_trace_gases_toz;
constexpr uint32_t GRIB2::param_meteorology_trace_gases_pozt;
constexpr uint32_t GRIB2::param_meteorology_trace_gases_pozo;
constexpr uint32_t GRIB2::param_meteorology_trace_gases_ozmax1;
constexpr uint32_t GRIB2::param_meteorology_trace_gases_ozmax8;
constexpr uint32_t GRIB2::param_meteorology_trace_gases_pdmax1;
constexpr uint32_t GRIB2::param_meteorology_trace_gases_pdmax24;
constexpr uint32_t GRIB2::param_meteorology_radar_bswid;
constexpr uint32_t GRIB2::param_meteorology_radar_bref;
constexpr uint32_t GRIB2::param_meteorology_radar_brvel;
constexpr uint32_t GRIB2::param_meteorology_radar_vil;
constexpr uint32_t GRIB2::param_meteorology_radar_lmaxbr;
constexpr uint32_t GRIB2::param_meteorology_radar_prec;
constexpr uint32_t GRIB2::param_meteorology_radar_rdsp1;
constexpr uint32_t GRIB2::param_meteorology_radar_rdsp2;
constexpr uint32_t GRIB2::param_meteorology_radar_rdsp3;
constexpr uint32_t GRIB2::param_meteorology_radar_rfcd;
constexpr uint32_t GRIB2::param_meteorology_radar_rfci;
constexpr uint32_t GRIB2::param_meteorology_radar_rfsnow;
constexpr uint32_t GRIB2::param_meteorology_radar_rfrain;
constexpr uint32_t GRIB2::param_meteorology_radar_rfgrpl;
constexpr uint32_t GRIB2::param_meteorology_radar_rfhail;
constexpr uint32_t GRIB2::param_meteorology_forecast_radar_refzr;
constexpr uint32_t GRIB2::param_meteorology_forecast_radar_refzi;
constexpr uint32_t GRIB2::param_meteorology_forecast_radar_refzc;
constexpr uint32_t GRIB2::param_meteorology_forecast_radar_retop;
constexpr uint32_t GRIB2::param_meteorology_forecast_radar_refd;
constexpr uint32_t GRIB2::param_meteorology_forecast_radar_refc;
constexpr uint32_t GRIB2::param_meteorology_forecast_radar_refzr_2;
constexpr uint32_t GRIB2::param_meteorology_forecast_radar_refzi_2;
constexpr uint32_t GRIB2::param_meteorology_forecast_radar_refzc_2;
constexpr uint32_t GRIB2::param_meteorology_forecast_radar_refd_2;
constexpr uint32_t GRIB2::param_meteorology_forecast_radar_refc_2;
constexpr uint32_t GRIB2::param_meteorology_forecast_radar_retop_2;
constexpr uint32_t GRIB2::param_meteorology_forecast_radar_maxref;
constexpr uint32_t GRIB2::param_meteorology_electrodynamics_ltng;
constexpr uint32_t GRIB2::param_meteorology_nuclear_acces;
constexpr uint32_t GRIB2::param_meteorology_nuclear_aciod;
constexpr uint32_t GRIB2::param_meteorology_nuclear_acradp;
constexpr uint32_t GRIB2::param_meteorology_nuclear_gdces;
constexpr uint32_t GRIB2::param_meteorology_nuclear_gdiod;
constexpr uint32_t GRIB2::param_meteorology_nuclear_gdradp;
constexpr uint32_t GRIB2::param_meteorology_nuclear_tiaccp;
constexpr uint32_t GRIB2::param_meteorology_nuclear_tiacip;
constexpr uint32_t GRIB2::param_meteorology_nuclear_tiacrp;
constexpr uint32_t GRIB2::param_meteorology_nuclear_aircon;
constexpr uint32_t GRIB2::param_meteorology_nuclear_wetdep;
constexpr uint32_t GRIB2::param_meteorology_nuclear_drydep;
constexpr uint32_t GRIB2::param_meteorology_nuclear_totlwd;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_vis;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_albdo;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_tstm;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_mixht;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_volash;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_icit;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_icib;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_ici;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_turbt;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_turbb;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_turb;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_tke;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_pblreg;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_conti;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_contet;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_contt;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_contb;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_mxsalb;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_snfalb;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_salbd;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_icip;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_ctp;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_cat;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_sldp;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_contke;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_wiww;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_convo;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_mxsalb_2;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_snfalb_2;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_srcono;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_mrcono;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_hrcono;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_torprob;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_hailprob;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_windprob;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_storprob;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_shailpro;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_swindpro;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_tstmc;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_mixly;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_flght;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_cicel;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_civis;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_ciflt;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_lavni;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_havni;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_sbsalb;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_swsalb;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_nbsalb;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_nwsalb;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_prsvr;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_prsigsvr;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_sipd;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_epsr;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_tpfi;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_svrts;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_procon;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_convp;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_vaftd;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_icprb;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_icsev;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_massden;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_colmd;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_massmr;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_aemflx;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_anpmflx;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_anpemflx;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_sddmflx;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_swdmflx;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_aremflx;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_wlsmflx;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_wdcpmflx;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_sedmflx;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_ddmflx;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_tranhh;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_trsds;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_aia;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_conair;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_vmxr;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_cgprc;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_cgdrc;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_sflux;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_coaia;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_tyaba;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_tyaal;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_ancon;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_saden;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_atmtk;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_aotk;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_ssalbk;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_asysfk;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_aecoef;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_aacoef;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_albsat;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_albgrd;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_alesat;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_chemistry_alegrd;
constexpr uint32_t GRIB2::param_meteorology_string_atext;
constexpr uint32_t GRIB2::param_meteorology_miscellaneous_tsec;
constexpr uint32_t GRIB2::param_meteorology_miscellaneous_geolat;
constexpr uint32_t GRIB2::param_meteorology_miscellaneous_geolon;
constexpr uint32_t GRIB2::param_meteorology_miscellaneous_nlat;
constexpr uint32_t GRIB2::param_meteorology_miscellaneous_elon;
constexpr uint32_t GRIB2::param_meteorology_miscellaneous_tsec_2;
constexpr uint32_t GRIB2::param_meteorology_miscellaneous_mlyno;
constexpr uint32_t GRIB2::param_meteorology_miscellaneous_nlatn;
constexpr uint32_t GRIB2::param_meteorology_miscellaneous_elonn;
constexpr uint32_t GRIB2::param_meteorology_covariance_covmz;
constexpr uint32_t GRIB2::param_meteorology_covariance_covtz;
constexpr uint32_t GRIB2::param_meteorology_covariance_covtm;
constexpr uint32_t GRIB2::param_meteorology_covariance_covtw;
constexpr uint32_t GRIB2::param_meteorology_covariance_covzz;
constexpr uint32_t GRIB2::param_meteorology_covariance_covmm;
constexpr uint32_t GRIB2::param_meteorology_covariance_covqz;
constexpr uint32_t GRIB2::param_meteorology_covariance_covqm;
constexpr uint32_t GRIB2::param_meteorology_covariance_covtvv;
constexpr uint32_t GRIB2::param_meteorology_covariance_covqvv;
constexpr uint32_t GRIB2::param_meteorology_covariance_covpsps;
constexpr uint32_t GRIB2::param_meteorology_covariance_covqq;
constexpr uint32_t GRIB2::param_meteorology_covariance_covvvvv;
constexpr uint32_t GRIB2::param_meteorology_covariance_covtt;
constexpr uint32_t GRIB2::param_hydrology_basic_ffldg;
constexpr uint32_t GRIB2::param_hydrology_basic_ffldro;
constexpr uint32_t GRIB2::param_hydrology_basic_rssc;
constexpr uint32_t GRIB2::param_hydrology_basic_esct;
constexpr uint32_t GRIB2::param_hydrology_basic_swepon;
constexpr uint32_t GRIB2::param_hydrology_basic_bgrun;
constexpr uint32_t GRIB2::param_hydrology_basic_ssrun;
constexpr uint32_t GRIB2::param_hydrology_basic_bgrun_2;
constexpr uint32_t GRIB2::param_hydrology_basic_ssrun_2;
constexpr uint32_t GRIB2::param_hydrology_prob_cppop;
constexpr uint32_t GRIB2::param_hydrology_prob_pposp;
constexpr uint32_t GRIB2::param_hydrology_prob_pop;
constexpr uint32_t GRIB2::param_hydrology_prob_cpozp;
constexpr uint32_t GRIB2::param_hydrology_prob_cpofp;
constexpr uint32_t GRIB2::param_hydrology_prob_ppffg;
constexpr uint32_t GRIB2::param_hydrology_prob_cwr;
constexpr uint32_t GRIB2::param_hydrology_inlandwater_sediment_wdpthil;
constexpr uint32_t GRIB2::param_hydrology_inlandwater_sediment_wtmpil;
constexpr uint32_t GRIB2::param_hydrology_inlandwater_sediment_wfract;
constexpr uint32_t GRIB2::param_hydrology_inlandwater_sediment_sedtk;
constexpr uint32_t GRIB2::param_hydrology_inlandwater_sediment_sedtmp;
constexpr uint32_t GRIB2::param_hydrology_inlandwater_sediment_ictkil;
constexpr uint32_t GRIB2::param_hydrology_inlandwater_sediment_icetil;
constexpr uint32_t GRIB2::param_hydrology_inlandwater_sediment_icecil;
constexpr uint32_t GRIB2::param_hydrology_inlandwater_sediment_landil;
constexpr uint32_t GRIB2::param_hydrology_inlandwater_sediment_sfsal;
constexpr uint32_t GRIB2::param_hydrology_inlandwater_sediment_sftmp;
constexpr uint32_t GRIB2::param_hydrology_inlandwater_sediment_acwsr;
constexpr uint32_t GRIB2::param_hydrology_inlandwater_sediment_saltil;
constexpr uint32_t GRIB2::param_landsurface_vegetation_land;
constexpr uint32_t GRIB2::param_landsurface_vegetation_sfcr;
constexpr uint32_t GRIB2::param_landsurface_vegetation_tsoil;
constexpr uint32_t GRIB2::param_landsurface_vegetation_soilmc;
constexpr uint32_t GRIB2::param_landsurface_vegetation_veg;
constexpr uint32_t GRIB2::param_landsurface_vegetation_watr;
constexpr uint32_t GRIB2::param_landsurface_vegetation_evapt;
constexpr uint32_t GRIB2::param_landsurface_vegetation_mterh;
constexpr uint32_t GRIB2::param_landsurface_vegetation_landu;
constexpr uint32_t GRIB2::param_landsurface_vegetation_soilw;
constexpr uint32_t GRIB2::param_landsurface_vegetation_gflux;
constexpr uint32_t GRIB2::param_landsurface_vegetation_mstav;
constexpr uint32_t GRIB2::param_landsurface_vegetation_sfexc;
constexpr uint32_t GRIB2::param_landsurface_vegetation_cnwat;
constexpr uint32_t GRIB2::param_landsurface_vegetation_bmixl;
constexpr uint32_t GRIB2::param_landsurface_vegetation_ccond;
constexpr uint32_t GRIB2::param_landsurface_vegetation_rsmin;
constexpr uint32_t GRIB2::param_landsurface_vegetation_wilt;
constexpr uint32_t GRIB2::param_landsurface_vegetation_rcs;
constexpr uint32_t GRIB2::param_landsurface_vegetation_rct;
constexpr uint32_t GRIB2::param_landsurface_vegetation_rcq;
constexpr uint32_t GRIB2::param_landsurface_vegetation_rcsol;
constexpr uint32_t GRIB2::param_landsurface_vegetation_soilm;
constexpr uint32_t GRIB2::param_landsurface_vegetation_cisoilw;
constexpr uint32_t GRIB2::param_landsurface_vegetation_hflux;
constexpr uint32_t GRIB2::param_landsurface_vegetation_vsoilm;
constexpr uint32_t GRIB2::param_landsurface_vegetation_wilt_3;
constexpr uint32_t GRIB2::param_landsurface_vegetation_vwiltp;
constexpr uint32_t GRIB2::param_landsurface_vegetation_leainx;
constexpr uint32_t GRIB2::param_landsurface_vegetation_everf;
constexpr uint32_t GRIB2::param_landsurface_vegetation_decf;
constexpr uint32_t GRIB2::param_landsurface_vegetation_ndvinx;
constexpr uint32_t GRIB2::param_landsurface_vegetation_rdveg;
constexpr uint32_t GRIB2::param_landsurface_vegetation_soilw_2;
constexpr uint32_t GRIB2::param_landsurface_vegetation_gflux_2;
constexpr uint32_t GRIB2::param_landsurface_vegetation_mstav_2;
constexpr uint32_t GRIB2::param_landsurface_vegetation_sfexc_2;
constexpr uint32_t GRIB2::param_landsurface_vegetation_cnwat_2;
constexpr uint32_t GRIB2::param_landsurface_vegetation_bmixl_2;
constexpr uint32_t GRIB2::param_landsurface_vegetation_vgtyp;
constexpr uint32_t GRIB2::param_landsurface_vegetation_ccond_2;
constexpr uint32_t GRIB2::param_landsurface_vegetation_rsmin_2;
constexpr uint32_t GRIB2::param_landsurface_vegetation_wilt_2;
constexpr uint32_t GRIB2::param_landsurface_vegetation_rcs_2;
constexpr uint32_t GRIB2::param_landsurface_vegetation_rct_2;
constexpr uint32_t GRIB2::param_landsurface_vegetation_rcq_2;
constexpr uint32_t GRIB2::param_landsurface_vegetation_rcsol_2;
constexpr uint32_t GRIB2::param_landsurface_vegetation_rdrip;
constexpr uint32_t GRIB2::param_landsurface_vegetation_icwat;
constexpr uint32_t GRIB2::param_landsurface_vegetation_akhs;
constexpr uint32_t GRIB2::param_landsurface_vegetation_akms;
constexpr uint32_t GRIB2::param_landsurface_vegetation_vegt;
constexpr uint32_t GRIB2::param_landsurface_vegetation_sstor;
constexpr uint32_t GRIB2::param_landsurface_vegetation_lsoil;
constexpr uint32_t GRIB2::param_landsurface_vegetation_ewatr;
constexpr uint32_t GRIB2::param_landsurface_vegetation_gwrec;
constexpr uint32_t GRIB2::param_landsurface_vegetation_qrec;
constexpr uint32_t GRIB2::param_landsurface_vegetation_sfcrh;
constexpr uint32_t GRIB2::param_landsurface_vegetation_ndvi;
constexpr uint32_t GRIB2::param_landsurface_vegetation_landn;
constexpr uint32_t GRIB2::param_landsurface_vegetation_amixl;
constexpr uint32_t GRIB2::param_landsurface_vegetation_wvinc;
constexpr uint32_t GRIB2::param_landsurface_vegetation_wcinc;
constexpr uint32_t GRIB2::param_landsurface_vegetation_wvconv;
constexpr uint32_t GRIB2::param_landsurface_vegetation_wcconv;
constexpr uint32_t GRIB2::param_landsurface_vegetation_wvuflx;
constexpr uint32_t GRIB2::param_landsurface_vegetation_wvvflx;
constexpr uint32_t GRIB2::param_landsurface_vegetation_wcuflx;
constexpr uint32_t GRIB2::param_landsurface_vegetation_wcvflx;
constexpr uint32_t GRIB2::param_landsurface_vegetation_acond;
constexpr uint32_t GRIB2::param_landsurface_vegetation_evcw;
constexpr uint32_t GRIB2::param_landsurface_vegetation_trans;
constexpr uint32_t GRIB2::param_landsurface_agriaquaculture_canl;
constexpr uint32_t GRIB2::param_landsurface_soil_sotyp;
constexpr uint32_t GRIB2::param_landsurface_soil_uplst;
constexpr uint32_t GRIB2::param_landsurface_soil_uplsm;
constexpr uint32_t GRIB2::param_landsurface_soil_lowlsm;
constexpr uint32_t GRIB2::param_landsurface_soil_botlst;
constexpr uint32_t GRIB2::param_landsurface_soil_soill;
constexpr uint32_t GRIB2::param_landsurface_soil_rlyrs;
constexpr uint32_t GRIB2::param_landsurface_soil_smref;
constexpr uint32_t GRIB2::param_landsurface_soil_smdry;
constexpr uint32_t GRIB2::param_landsurface_soil_poros;
constexpr uint32_t GRIB2::param_landsurface_soil_liqvsm;
constexpr uint32_t GRIB2::param_landsurface_soil_voltso;
constexpr uint32_t GRIB2::param_landsurface_soil_transo;
constexpr uint32_t GRIB2::param_landsurface_soil_voldec;
constexpr uint32_t GRIB2::param_landsurface_soil_direc;
constexpr uint32_t GRIB2::param_landsurface_soil_soilp;
constexpr uint32_t GRIB2::param_landsurface_soil_vsosm;
constexpr uint32_t GRIB2::param_landsurface_soil_satosm;
constexpr uint32_t GRIB2::param_landsurface_soil_soiltmp;
constexpr uint32_t GRIB2::param_landsurface_soil_soilmoi;
constexpr uint32_t GRIB2::param_landsurface_soil_cisoilm;
constexpr uint32_t GRIB2::param_landsurface_soil_soilice;
constexpr uint32_t GRIB2::param_landsurface_soil_cisice;
constexpr uint32_t GRIB2::param_landsurface_soil_soill_2;
constexpr uint32_t GRIB2::param_landsurface_soil_rlyrs_2;
constexpr uint32_t GRIB2::param_landsurface_soil_sltyp;
constexpr uint32_t GRIB2::param_landsurface_soil_smref_2;
constexpr uint32_t GRIB2::param_landsurface_soil_smdry_2;
constexpr uint32_t GRIB2::param_landsurface_soil_poros_2;
constexpr uint32_t GRIB2::param_landsurface_soil_evbs;
constexpr uint32_t GRIB2::param_landsurface_soil_lspa;
constexpr uint32_t GRIB2::param_landsurface_soil_baret;
constexpr uint32_t GRIB2::param_landsurface_soil_avsft;
constexpr uint32_t GRIB2::param_landsurface_soil_radt;
constexpr uint32_t GRIB2::param_landsurface_soil_fldcp;
constexpr uint32_t GRIB2::param_landsurface_fire_fireolk;
constexpr uint32_t GRIB2::param_landsurface_fire_fireodt;
constexpr uint32_t GRIB2::param_landsurface_fire_hindex;
constexpr uint32_t GRIB2::param_space_image_srad;
constexpr uint32_t GRIB2::param_space_image_salbedo;
constexpr uint32_t GRIB2::param_space_image_sbtmp;
constexpr uint32_t GRIB2::param_space_image_spwat;
constexpr uint32_t GRIB2::param_space_image_slfti;
constexpr uint32_t GRIB2::param_space_image_sctpres;
constexpr uint32_t GRIB2::param_space_image_sstmp;
constexpr uint32_t GRIB2::param_space_image_cloudm;
constexpr uint32_t GRIB2::param_space_image_pixst;
constexpr uint32_t GRIB2::param_space_image_firedi;
constexpr uint32_t GRIB2::param_space_quantitative_estp;
constexpr uint32_t GRIB2::param_space_quantitative_irrate;
constexpr uint32_t GRIB2::param_space_quantitative_ctoph;
constexpr uint32_t GRIB2::param_space_quantitative_ctophqi;
constexpr uint32_t GRIB2::param_space_quantitative_estugrd;
constexpr uint32_t GRIB2::param_space_quantitative_estvgrd;
constexpr uint32_t GRIB2::param_space_quantitative_npixu;
constexpr uint32_t GRIB2::param_space_quantitative_solza;
constexpr uint32_t GRIB2::param_space_quantitative_raza;
constexpr uint32_t GRIB2::param_space_quantitative_rfl06;
constexpr uint32_t GRIB2::param_space_quantitative_rfl08;
constexpr uint32_t GRIB2::param_space_quantitative_rfl16;
constexpr uint32_t GRIB2::param_space_quantitative_rfl39;
constexpr uint32_t GRIB2::param_space_quantitative_atmdiv;
constexpr uint32_t GRIB2::param_space_quantitative_cbtmp;
constexpr uint32_t GRIB2::param_space_quantitative_csbtmp;
constexpr uint32_t GRIB2::param_space_quantitative_cldrad;
constexpr uint32_t GRIB2::param_space_quantitative_cskyrad;
constexpr uint32_t GRIB2::param_space_quantitative_winds;
constexpr uint32_t GRIB2::param_space_quantitative_aot06;
constexpr uint32_t GRIB2::param_space_quantitative_aot08;
constexpr uint32_t GRIB2::param_space_quantitative_aot16;
constexpr uint32_t GRIB2::param_space_quantitative_angcoe;
constexpr uint32_t GRIB2::param_space_quantitative_usct;
constexpr uint32_t GRIB2::param_space_quantitative_vsct;
constexpr uint32_t GRIB2::param_space_forecast_sbt122;
constexpr uint32_t GRIB2::param_space_forecast_sbt123;
constexpr uint32_t GRIB2::param_space_forecast_sbt124;
constexpr uint32_t GRIB2::param_space_forecast_sbt126;
constexpr uint32_t GRIB2::param_space_forecast_sbc123;
constexpr uint32_t GRIB2::param_space_forecast_sbc124;
constexpr uint32_t GRIB2::param_space_forecast_sbt112;
constexpr uint32_t GRIB2::param_space_forecast_sbt113;
constexpr uint32_t GRIB2::param_space_forecast_sbt114;
constexpr uint32_t GRIB2::param_space_forecast_sbt115;
constexpr uint32_t GRIB2::param_space_forecast_amsre9;
constexpr uint32_t GRIB2::param_space_forecast_amsre10;
constexpr uint32_t GRIB2::param_space_forecast_amsre11;
constexpr uint32_t GRIB2::param_space_forecast_amsre12;
constexpr uint32_t GRIB2::param_spaceweather_temperature_tmpswp;
constexpr uint32_t GRIB2::param_spaceweather_temperature_electmp;
constexpr uint32_t GRIB2::param_spaceweather_temperature_prottmp;
constexpr uint32_t GRIB2::param_spaceweather_temperature_iontmp;
constexpr uint32_t GRIB2::param_spaceweather_temperature_pratmp;
constexpr uint32_t GRIB2::param_spaceweather_temperature_prptmp;
constexpr uint32_t GRIB2::param_spaceweather_momentum_speed;
constexpr uint32_t GRIB2::param_spaceweather_momentum_vel1;
constexpr uint32_t GRIB2::param_spaceweather_momentum_vel2;
constexpr uint32_t GRIB2::param_spaceweather_momentum_vel3;
constexpr uint32_t GRIB2::param_spaceweather_charged_particles_plsmden;
constexpr uint32_t GRIB2::param_spaceweather_charged_particles_elcden;
constexpr uint32_t GRIB2::param_spaceweather_charged_particles_protden;
constexpr uint32_t GRIB2::param_spaceweather_charged_particles_ionden;
constexpr uint32_t GRIB2::param_spaceweather_charged_particles_vtec;
constexpr uint32_t GRIB2::param_spaceweather_charged_particles_absfrq;
constexpr uint32_t GRIB2::param_spaceweather_charged_particles_absrb;
constexpr uint32_t GRIB2::param_spaceweather_charged_particles_sprdf;
constexpr uint32_t GRIB2::param_spaceweather_charged_particles_hprimf;
constexpr uint32_t GRIB2::param_spaceweather_charged_particles_crtfrq;
constexpr uint32_t GRIB2::param_spaceweather_charged_particles_scint;
constexpr uint32_t GRIB2::param_spaceweather_fields_btot;
constexpr uint32_t GRIB2::param_spaceweather_fields_bvec1;
constexpr uint32_t GRIB2::param_spaceweather_fields_bvec2;
constexpr uint32_t GRIB2::param_spaceweather_fields_bvec3;
constexpr uint32_t GRIB2::param_spaceweather_fields_etot;
constexpr uint32_t GRIB2::param_spaceweather_fields_evec1;
constexpr uint32_t GRIB2::param_spaceweather_fields_evec2;
constexpr uint32_t GRIB2::param_spaceweather_fields_evec3;
constexpr uint32_t GRIB2::param_spaceweather_energetic_particles_difpflux;
constexpr uint32_t GRIB2::param_spaceweather_energetic_particles_intpflux;
constexpr uint32_t GRIB2::param_spaceweather_energetic_particles_difeflux;
constexpr uint32_t GRIB2::param_spaceweather_energetic_particles_inteflux;
constexpr uint32_t GRIB2::param_spaceweather_energetic_particles_dififlux;
constexpr uint32_t GRIB2::param_spaceweather_energetic_particles_intiflux;
constexpr uint32_t GRIB2::param_spaceweather_energetic_particles_ntrnflux;
constexpr uint32_t GRIB2::param_spaceweather_solar_tsi;
constexpr uint32_t GRIB2::param_spaceweather_solar_xlong;
constexpr uint32_t GRIB2::param_spaceweather_solar_xshrt;
constexpr uint32_t GRIB2::param_spaceweather_solar_euvirr;
constexpr uint32_t GRIB2::param_spaceweather_solar_specirr;
constexpr uint32_t GRIB2::param_spaceweather_solar_f107;
constexpr uint32_t GRIB2::param_spaceweather_solar_solrf;
constexpr uint32_t GRIB2::param_spaceweather_terrestrial_lmbint;
constexpr uint32_t GRIB2::param_spaceweather_terrestrial_dskint;
constexpr uint32_t GRIB2::param_spaceweather_terrestrial_dskday;
constexpr uint32_t GRIB2::param_spaceweather_terrestrial_dskngt;
constexpr uint32_t GRIB2::param_spaceweather_imagery_xrayrad;
constexpr uint32_t GRIB2::param_spaceweather_imagery_euvrad;
constexpr uint32_t GRIB2::param_spaceweather_imagery_harad;
constexpr uint32_t GRIB2::param_spaceweather_imagery_whtrad;
constexpr uint32_t GRIB2::param_spaceweather_imagery_caiirad;
constexpr uint32_t GRIB2::param_spaceweather_imagery_whtcor;
constexpr uint32_t GRIB2::param_spaceweather_imagery_helcor;
constexpr uint32_t GRIB2::param_spaceweather_imagery_mask;
constexpr uint32_t GRIB2::param_spaceweather_ionneutral_coupling_sigped;
constexpr uint32_t GRIB2::param_spaceweather_ionneutral_coupling_sighal;
constexpr uint32_t GRIB2::param_spaceweather_ionneutral_coupling_sigpar;
constexpr uint32_t GRIB2::param_oceanography_waves_wvsp1;
constexpr uint32_t GRIB2::param_oceanography_waves_wvsp2;
constexpr uint32_t GRIB2::param_oceanography_waves_wvsp3;
constexpr uint32_t GRIB2::param_oceanography_waves_htsgw;
constexpr uint32_t GRIB2::param_oceanography_waves_wvdir;
constexpr uint32_t GRIB2::param_oceanography_waves_wvhgt;
constexpr uint32_t GRIB2::param_oceanography_waves_wvper;
constexpr uint32_t GRIB2::param_oceanography_waves_swdir;
constexpr uint32_t GRIB2::param_oceanography_waves_swell;
constexpr uint32_t GRIB2::param_oceanography_waves_swper;
constexpr uint32_t GRIB2::param_oceanography_waves_dirpw;
constexpr uint32_t GRIB2::param_oceanography_waves_perpw;
constexpr uint32_t GRIB2::param_oceanography_waves_dirsw;
constexpr uint32_t GRIB2::param_oceanography_waves_persw;
constexpr uint32_t GRIB2::param_oceanography_waves_wwsdir;
constexpr uint32_t GRIB2::param_oceanography_waves_mwsper;
constexpr uint32_t GRIB2::param_oceanography_waves_cdww;
constexpr uint32_t GRIB2::param_oceanography_waves_fricv;
constexpr uint32_t GRIB2::param_oceanography_waves_wstr;
constexpr uint32_t GRIB2::param_oceanography_waves_nwstr;
constexpr uint32_t GRIB2::param_oceanography_waves_mssw;
constexpr uint32_t GRIB2::param_oceanography_waves_ussd;
constexpr uint32_t GRIB2::param_oceanography_waves_vssd;
constexpr uint32_t GRIB2::param_oceanography_waves_pmaxwh;
constexpr uint32_t GRIB2::param_oceanography_waves_maxwh;
constexpr uint32_t GRIB2::param_oceanography_waves_imwf;
constexpr uint32_t GRIB2::param_oceanography_waves_imfww;
constexpr uint32_t GRIB2::param_oceanography_waves_imftsw;
constexpr uint32_t GRIB2::param_oceanography_waves_mzwper;
constexpr uint32_t GRIB2::param_oceanography_waves_mzpww;
constexpr uint32_t GRIB2::param_oceanography_waves_mzptsw;
constexpr uint32_t GRIB2::param_oceanography_waves_wdirw;
constexpr uint32_t GRIB2::param_oceanography_waves_dirwww;
constexpr uint32_t GRIB2::param_oceanography_waves_dirwts;
constexpr uint32_t GRIB2::param_oceanography_waves_pwper;
constexpr uint32_t GRIB2::param_oceanography_waves_pperww;
constexpr uint32_t GRIB2::param_oceanography_waves_pperts;
constexpr uint32_t GRIB2::param_oceanography_waves_altwh;
constexpr uint32_t GRIB2::param_oceanography_waves_alcwh;
constexpr uint32_t GRIB2::param_oceanography_waves_alrrc;
constexpr uint32_t GRIB2::param_oceanography_waves_mnwsow;
constexpr uint32_t GRIB2::param_oceanography_waves_mwdirw;
constexpr uint32_t GRIB2::param_oceanography_waves_wesp;
constexpr uint32_t GRIB2::param_oceanography_waves_kssedw;
constexpr uint32_t GRIB2::param_oceanography_waves_beninx;
constexpr uint32_t GRIB2::param_oceanography_waves_spftr;
constexpr uint32_t GRIB2::param_oceanography_waves_2dsed;
constexpr uint32_t GRIB2::param_oceanography_waves_fseed;
constexpr uint32_t GRIB2::param_oceanography_waves_dirsed;
constexpr uint32_t GRIB2::param_oceanography_waves_hsign;
constexpr uint32_t GRIB2::param_oceanography_waves_pkdir;
constexpr uint32_t GRIB2::param_oceanography_waves_mnstp;
constexpr uint32_t GRIB2::param_oceanography_waves_dmspr;
constexpr uint32_t GRIB2::param_oceanography_waves_wffrac;
constexpr uint32_t GRIB2::param_oceanography_waves_temm1;
constexpr uint32_t GRIB2::param_oceanography_waves_dir11;
constexpr uint32_t GRIB2::param_oceanography_waves_dir22;
constexpr uint32_t GRIB2::param_oceanography_waves_dspr11;
constexpr uint32_t GRIB2::param_oceanography_waves_dspr22;
constexpr uint32_t GRIB2::param_oceanography_waves_wlen;
constexpr uint32_t GRIB2::param_oceanography_waves_rdsxx;
constexpr uint32_t GRIB2::param_oceanography_waves_rdsyy;
constexpr uint32_t GRIB2::param_oceanography_waves_rdsxy;
constexpr uint32_t GRIB2::param_oceanography_waves_wstp;
constexpr uint32_t GRIB2::param_oceanography_currents_dirc;
constexpr uint32_t GRIB2::param_oceanography_currents_spc;
constexpr uint32_t GRIB2::param_oceanography_currents_uogrd;
constexpr uint32_t GRIB2::param_oceanography_currents_vogrd;
constexpr uint32_t GRIB2::param_oceanography_currents_omlu;
constexpr uint32_t GRIB2::param_oceanography_currents_omlv;
constexpr uint32_t GRIB2::param_oceanography_currents_ubaro;
constexpr uint32_t GRIB2::param_oceanography_currents_vbaro;
constexpr uint32_t GRIB2::param_oceanography_ice_icec;
constexpr uint32_t GRIB2::param_oceanography_ice_icetk;
constexpr uint32_t GRIB2::param_oceanography_ice_diced;
constexpr uint32_t GRIB2::param_oceanography_ice_siced;
constexpr uint32_t GRIB2::param_oceanography_ice_uice;
constexpr uint32_t GRIB2::param_oceanography_ice_vice;
constexpr uint32_t GRIB2::param_oceanography_ice_iceg;
constexpr uint32_t GRIB2::param_oceanography_ice_iced;
constexpr uint32_t GRIB2::param_oceanography_ice_icetmp;
constexpr uint32_t GRIB2::param_oceanography_ice_iceprs;
constexpr uint32_t GRIB2::param_oceanography_surface_wtmp;
constexpr uint32_t GRIB2::param_oceanography_surface_dslm;
constexpr uint32_t GRIB2::param_oceanography_surface_surge;
constexpr uint32_t GRIB2::param_oceanography_surface_etsrg;
constexpr uint32_t GRIB2::param_oceanography_surface_elev;
constexpr uint32_t GRIB2::param_oceanography_surface_sshg;
constexpr uint32_t GRIB2::param_oceanography_surface_p2omlt;
constexpr uint32_t GRIB2::param_oceanography_surface_aohflx;
constexpr uint32_t GRIB2::param_oceanography_surface_ashfl;
constexpr uint32_t GRIB2::param_oceanography_surface_sstt;
constexpr uint32_t GRIB2::param_oceanography_surface_ssst;
constexpr uint32_t GRIB2::param_oceanography_surface_keng;
constexpr uint32_t GRIB2::param_oceanography_surface_sltfl;
constexpr uint32_t GRIB2::param_oceanography_surface_tcsrg20;
constexpr uint32_t GRIB2::param_oceanography_surface_tcsrg30;
constexpr uint32_t GRIB2::param_oceanography_surface_tcsrg40;
constexpr uint32_t GRIB2::param_oceanography_surface_tcsrg50;
constexpr uint32_t GRIB2::param_oceanography_surface_tcsrg60;
constexpr uint32_t GRIB2::param_oceanography_surface_tcsrg70;
constexpr uint32_t GRIB2::param_oceanography_surface_tcsrg80;
constexpr uint32_t GRIB2::param_oceanography_surface_tcsrg90;
constexpr uint32_t GRIB2::param_oceanography_surface_etcwl;
constexpr uint32_t GRIB2::param_oceanography_subsurface_mthd;
constexpr uint32_t GRIB2::param_oceanography_subsurface_mtha;
constexpr uint32_t GRIB2::param_oceanography_subsurface_tthdp;
constexpr uint32_t GRIB2::param_oceanography_subsurface_salty;
constexpr uint32_t GRIB2::param_oceanography_subsurface_ovhd;
constexpr uint32_t GRIB2::param_oceanography_subsurface_ovsd;
constexpr uint32_t GRIB2::param_oceanography_subsurface_ovmd;
constexpr uint32_t GRIB2::param_oceanography_subsurface_bathy;
constexpr uint32_t GRIB2::param_oceanography_subsurface_sfsalp;
constexpr uint32_t GRIB2::param_oceanography_subsurface_sftmpp;
constexpr uint32_t GRIB2::param_oceanography_subsurface_acwsrd;
constexpr uint32_t GRIB2::param_oceanography_subsurface_wdepth;
constexpr uint32_t GRIB2::param_oceanography_subsurface_wtmpss;
constexpr uint32_t GRIB2::param_oceanography_subsurface_wtmpc;
constexpr uint32_t GRIB2::param_oceanography_subsurface_salin;
constexpr uint32_t GRIB2::param_oceanography_subsurface_bkeng;
constexpr uint32_t GRIB2::param_oceanography_subsurface_dbss;
constexpr uint32_t GRIB2::param_oceanography_subsurface_intfd;
constexpr uint32_t GRIB2::param_oceanography_subsurface_ohc;
constexpr uint32_t GRIB2::param_oceanography_miscellaneous_tsec;
constexpr uint32_t GRIB2::param_oceanography_miscellaneous_mosf;

constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_dwdwawfor_icingbase;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_dwdwawfor_icingmaxbase;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_dwdwawfor_icingmaxtop;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_dwdwawfor_icingtop;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_dwdwawfor_icingverticalcode;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_dwdwawfor_icingmaxcode;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_dwdwawfor_icingsignificantcode;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_dwdwawfor_icingdegree;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_dwdwawfor_eddydissrate;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_dwdwawfor_eddydissrate_ufir;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_dwdwawfor_eddydissrate_luir;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_dwdwawfor_eddydissrate_uuir;
constexpr uint32_t GRIB2::param_meteorology_atmosphere_physics_dwdwawfor_eddydissrate_lfir;

constexpr uint16_t GRIB2::center_melbourne_wmc_1;
constexpr uint16_t GRIB2::center_melbourne_wmc_2;
constexpr uint16_t GRIB2::center_melbourne_wmc_3;
constexpr uint16_t GRIB2::center_moscow_wmc_1;
constexpr uint16_t GRIB2::center_moscow_wmc_2;
constexpr uint16_t GRIB2::center_moscow_wmc_3;
constexpr uint16_t GRIB2::center_usnationalweatherservice_ncep_wmc;
constexpr uint16_t GRIB2::center_usnationalweatherservice_nwstg_wmc;
constexpr uint16_t GRIB2::center_usnationalweatherservice_other_wmc;
constexpr uint16_t GRIB2::center_cairo_rsmc_rafc_1;
constexpr uint16_t GRIB2::center_cairo_rsmc_rafc_2;
constexpr uint16_t GRIB2::center_dakar_rsmc_rafc_1;
constexpr uint16_t GRIB2::center_dakar_rsmc_rafc_2;
constexpr uint16_t GRIB2::center_nairobi_rsmc_rafc_1;
constexpr uint16_t GRIB2::center_nairobi_rsmc_rafc_2;
constexpr uint16_t GRIB2::center_casablanca_rsmc;
constexpr uint16_t GRIB2::center_tunis_rsmc;
constexpr uint16_t GRIB2::center_tunis_casablanca_rsmc_1;
constexpr uint16_t GRIB2::center_tunis_casablanca_rsmc_2;
constexpr uint16_t GRIB2::center_laspalmas_rafc;
constexpr uint16_t GRIB2::center_algiers_rsmc;
constexpr uint16_t GRIB2::center_acmad;
constexpr uint16_t GRIB2::center_mozambique_nmc;
constexpr uint16_t GRIB2::center_pretoria_rsmc;
constexpr uint16_t GRIB2::center_lareunion_rsmc;
constexpr uint16_t GRIB2::center_khabarovsk_rsmc_1;
constexpr uint16_t GRIB2::center_khabarovsk_rsmc_2;
constexpr uint16_t GRIB2::center_newdelhi_rsmc_rafc_1;
constexpr uint16_t GRIB2::center_newdelhi_rsmc_rafc_2;
constexpr uint16_t GRIB2::center_novosibirsk_rsmc_1;
constexpr uint16_t GRIB2::center_novosibirsk_rsmc_2;
constexpr uint16_t GRIB2::center_tashkent_rsmc;
constexpr uint16_t GRIB2::center_jeddah_rsmc;
constexpr uint16_t GRIB2::center_tokyo_rsmc_1;
constexpr uint16_t GRIB2::center_tokyo_rsmc_2;
constexpr uint16_t GRIB2::center_bankok;
constexpr uint16_t GRIB2::center_ulanbator;
constexpr uint16_t GRIB2::center_beijing_rsmc_1;
constexpr uint16_t GRIB2::center_beijing_rsmc_2;
constexpr uint16_t GRIB2::center_seoul;
constexpr uint16_t GRIB2::center_buenosaires_rsmc_rafc_1;
constexpr uint16_t GRIB2::center_buenosaires_rsmc_rafc_2;
constexpr uint16_t GRIB2::center_brasilia_rsmc_rafc_1;
constexpr uint16_t GRIB2::center_brasilia_rsmc_rafc_2;
constexpr uint16_t GRIB2::center_santiago;
constexpr uint16_t GRIB2::center_brazilianspaceagency_inpe;
constexpr uint16_t GRIB2::center_columbia_nmc;
constexpr uint16_t GRIB2::center_ecuador_nmc;
constexpr uint16_t GRIB2::center_peru_nmc;
constexpr uint16_t GRIB2::center_venezuela_nmc;
constexpr uint16_t GRIB2::center_miami_rsmc_rafc;
constexpr uint16_t GRIB2::center_miami_nationalhurricanecenter_rsmc;
constexpr uint16_t GRIB2::center_montreal_rsmc_1;
constexpr uint16_t GRIB2::center_montreal_rsmc_2;
constexpr uint16_t GRIB2::center_sanfrancisco;
constexpr uint16_t GRIB2::center_arinc;
constexpr uint16_t GRIB2::center_usairforceglobalweathercenter;
constexpr uint16_t GRIB2::center_fleetnumericalmeteorologyandoceanographycenter_monterey;
constexpr uint16_t GRIB2::center_noaaforecastsystemslab_boulder;
constexpr uint16_t GRIB2::center_nationalcenterforatmosphericresearch_boulder;
constexpr uint16_t GRIB2::center_argos_landover;
constexpr uint16_t GRIB2::center_usnavaloceanographicoffice;
constexpr uint16_t GRIB2::center_internationalresearchinstitudeforclimateandsociety;
constexpr uint16_t GRIB2::center_honolulu;
constexpr uint16_t GRIB2::center_darwin_rsmc_1;
constexpr uint16_t GRIB2::center_darwin_rsmc_2;
constexpr uint16_t GRIB2::center_melbourne_rsmc;
constexpr uint16_t GRIB2::center_wellington_rsmc_rafc_1;
constexpr uint16_t GRIB2::center_wellington_rsmc_rafc_2;
constexpr uint16_t GRIB2::center_nadi_rsmc;
constexpr uint16_t GRIB2::center_singapore;
constexpr uint16_t GRIB2::center_malaysia_nmc;
constexpr uint16_t GRIB2::center_ukmetoffice_exeter_rsmc_1;
constexpr uint16_t GRIB2::center_ukmetoffice_exeter_rsmc_2;
constexpr uint16_t GRIB2::center_moscow_rsmc_rafc;
constexpr uint16_t GRIB2::center_offenbach_rsmc_1;
constexpr uint16_t GRIB2::center_offenbach_rsmc_2;
constexpr uint16_t GRIB2::center_rome_rsmc_1;
constexpr uint16_t GRIB2::center_rome_rsmc_2;
constexpr uint16_t GRIB2::center_norrkoping_1;
constexpr uint16_t GRIB2::center_norrkoping_2;
constexpr uint16_t GRIB2::center_toulouse_1;
constexpr uint16_t GRIB2::center_toulouse_2;
constexpr uint16_t GRIB2::center_helsinki;
constexpr uint16_t GRIB2::center_belgrade;
constexpr uint16_t GRIB2::center_oslo;
constexpr uint16_t GRIB2::center_prague;
constexpr uint16_t GRIB2::center_episkopi;
constexpr uint16_t GRIB2::center_ankara;
constexpr uint16_t GRIB2::center_frankfurtmain_rafc;
constexpr uint16_t GRIB2::center_london_wafc;
constexpr uint16_t GRIB2::center_copenhagen;
constexpr uint16_t GRIB2::center_rota;
constexpr uint16_t GRIB2::center_athens;
constexpr uint16_t GRIB2::center_esa;
constexpr uint16_t GRIB2::center_europeancenterformediumrangeweatherforecasts_rsmc;
constexpr uint16_t GRIB2::center_debilt_netherlands;
constexpr uint16_t GRIB2::center_brazzaville;
constexpr uint16_t GRIB2::center_abidjan;
constexpr uint16_t GRIB2::center_libyanarabjamahiriya_nmc;
constexpr uint16_t GRIB2::center_madagascar_nmc;
constexpr uint16_t GRIB2::center_mauritius_nmc;
constexpr uint16_t GRIB2::center_niger_nmc;
constexpr uint16_t GRIB2::center_seychelles_nmc;
constexpr uint16_t GRIB2::center_uganda_nmc;
constexpr uint16_t GRIB2::center_unitedrepublicoftanzania_nmc;
constexpr uint16_t GRIB2::center_zimbabwe_nmc;
constexpr uint16_t GRIB2::center_hong_kong;
constexpr uint16_t GRIB2::center_afghanistan_nmc;
constexpr uint16_t GRIB2::center_bahrain_nmc;
constexpr uint16_t GRIB2::center_bangladesh_nmc;
constexpr uint16_t GRIB2::center_bhutan_nmc;
constexpr uint16_t GRIB2::center_cambodia_nmc;
constexpr uint16_t GRIB2::center_democraticpeoplesrepublicofkorea_nmc;
constexpr uint16_t GRIB2::center_islamicrepublicofiran_nmc;
constexpr uint16_t GRIB2::center_iraq_nmc;
constexpr uint16_t GRIB2::center_kazakhstan_nmc;
constexpr uint16_t GRIB2::center_kuwait_nmc;
constexpr uint16_t GRIB2::center_kyrgyzrepublic_nmc;
constexpr uint16_t GRIB2::center_laopeoplesdemocraticrepublic_nmc;
constexpr uint16_t GRIB2::center_macao_china;
constexpr uint16_t GRIB2::center_maldives_nmc;
constexpr uint16_t GRIB2::center_myanmar_nmc;
constexpr uint16_t GRIB2::center_nepal_nmc;
constexpr uint16_t GRIB2::center_oman_nmc;
constexpr uint16_t GRIB2::center_pakistan_nmc;
constexpr uint16_t GRIB2::center_qatar_nmc;
constexpr uint16_t GRIB2::center_yemen_nmc;
constexpr uint16_t GRIB2::center_srilanka_nmc;
constexpr uint16_t GRIB2::center_tajikistan_nmc;
constexpr uint16_t GRIB2::center_turkmenistan_nmc;
constexpr uint16_t GRIB2::center_unitedarabemirates_nmc;
constexpr uint16_t GRIB2::center_uzbekistan_nmc;
constexpr uint16_t GRIB2::center_vietnam_nmc;
constexpr uint16_t GRIB2::center_bolivia_nmc;
constexpr uint16_t GRIB2::center_guyana_nmc;
constexpr uint16_t GRIB2::center_paraguay_nmc;
constexpr uint16_t GRIB2::center_suriname_nmc;
constexpr uint16_t GRIB2::center_uruguay_nmc;
constexpr uint16_t GRIB2::center_frenchguyana;
constexpr uint16_t GRIB2::center_braziliannavyhydrographiccenter;
constexpr uint16_t GRIB2::center_nationalcommissiononspaceactivities_argentina;
constexpr uint16_t GRIB2::center_braziliandepartmentofairspacecontrol_decea;
constexpr uint16_t GRIB2::center_antiguaandbarbuda_nmc;
constexpr uint16_t GRIB2::center_bahamas_nmc;
constexpr uint16_t GRIB2::center_barbados_nmc;
constexpr uint16_t GRIB2::center_belize_nmc;
constexpr uint16_t GRIB2::center_britishcaribbeanterritoriescenter;
constexpr uint16_t GRIB2::center_sanjose;
constexpr uint16_t GRIB2::center_cuba_nmc;
constexpr uint16_t GRIB2::center_dominica_nmc;
constexpr uint16_t GRIB2::center_dominicanrepublic_nmc;
constexpr uint16_t GRIB2::center_elsalvador_nmc;
constexpr uint16_t GRIB2::center_usnoaa_nesdis;
constexpr uint16_t GRIB2::center_usnoaaofficeofoceanicandatmosphericresearch;
constexpr uint16_t GRIB2::center_guatemala_nmc;
constexpr uint16_t GRIB2::center_haiti_nmc;
constexpr uint16_t GRIB2::center_honduras_nmc;
constexpr uint16_t GRIB2::center_jamaica_nmc;
constexpr uint16_t GRIB2::center_mexicocity;
constexpr uint16_t GRIB2::center_netherlandsantillesandaruba_nmc;
constexpr uint16_t GRIB2::center_nicaragua_nmc;
constexpr uint16_t GRIB2::center_panama_nmc;
constexpr uint16_t GRIB2::center_saintlucia_nmc;
constexpr uint16_t GRIB2::center_trinidadandtobago_nmc;
constexpr uint16_t GRIB2::center_frenchdepartments_ra_iv;
constexpr uint16_t GRIB2::center_nasa;
constexpr uint16_t GRIB2::center_isdm_meds_canada;
constexpr uint16_t GRIB2::center_uscooperativeinstitudeformeteorologicalsatellitestudies;
constexpr uint16_t GRIB2::center_cookislands_nmc;
constexpr uint16_t GRIB2::center_frenchpolynesia_nmc;
constexpr uint16_t GRIB2::center_tonga_nmc;
constexpr uint16_t GRIB2::center_vanuatu_nmc;
constexpr uint16_t GRIB2::center_brunei_nmc;
constexpr uint16_t GRIB2::center_indonesia_nmc;
constexpr uint16_t GRIB2::center_kiribati_nmc;
constexpr uint16_t GRIB2::center_federatedstatesofmicronesia_nmc;
constexpr uint16_t GRIB2::center_newcaledonia_nmc;
constexpr uint16_t GRIB2::center_niue;
constexpr uint16_t GRIB2::center_papuanewguinea_nmc;
constexpr uint16_t GRIB2::center_philippines_nmc;
constexpr uint16_t GRIB2::center_samoa_nmc;
constexpr uint16_t GRIB2::center_solomonislands_nmc;
constexpr uint16_t GRIB2::center_narionalinstitudeofwaterandatmosphericresearch_newzealand;
constexpr uint16_t GRIB2::center_frascati_esa_esrin;
constexpr uint16_t GRIB2::center_lanion;
constexpr uint16_t GRIB2::center_lisbon;
constexpr uint16_t GRIB2::center_reykjavik;
constexpr uint16_t GRIB2::center_madrid;
constexpr uint16_t GRIB2::center_zurich;
constexpr uint16_t GRIB2::center_argos_toulouse;
constexpr uint16_t GRIB2::center_bratislava;
constexpr uint16_t GRIB2::center_budapest;
constexpr uint16_t GRIB2::center_ljubljana;
constexpr uint16_t GRIB2::center_warsaw;
constexpr uint16_t GRIB2::center_zagreb;
constexpr uint16_t GRIB2::center_albania_nmc;
constexpr uint16_t GRIB2::center_armenia_nmc;
constexpr uint16_t GRIB2::center_austria_nmc;
constexpr uint16_t GRIB2::center_azerbaijan_nmc;
constexpr uint16_t GRIB2::center_belarus_nmc;
constexpr uint16_t GRIB2::center_belgium_nmc;
constexpr uint16_t GRIB2::center_bosniaandherzegovina_nmc;
constexpr uint16_t GRIB2::center_bulgaria_nmc;
constexpr uint16_t GRIB2::center_cyprus_nmc;
constexpr uint16_t GRIB2::center_estonia_nmc;
constexpr uint16_t GRIB2::center_georgia_nmc;
constexpr uint16_t GRIB2::center_dublin;
constexpr uint16_t GRIB2::center_israel_nmc;
constexpr uint16_t GRIB2::center_jordan_nmc;
constexpr uint16_t GRIB2::center_latvia_nmc;
constexpr uint16_t GRIB2::center_lebanon_nmc;
constexpr uint16_t GRIB2::center_lithuania_nmc;
constexpr uint16_t GRIB2::center_luxembourg;
constexpr uint16_t GRIB2::center_malta_nmc;
constexpr uint16_t GRIB2::center_monaco;
constexpr uint16_t GRIB2::center_romania_nmc;
constexpr uint16_t GRIB2::center_syrianarabrepublic_nmc;
constexpr uint16_t GRIB2::center_macedonia_nmc;
constexpr uint16_t GRIB2::center_ukraine_nmc;
constexpr uint16_t GRIB2::center_republicofmoldova_nmc;
constexpr uint16_t GRIB2::center_opera_eumetnet;
constexpr uint16_t GRIB2::center_cosmo;
constexpr uint16_t GRIB2::center_eumetsat;
constexpr uint16_t GRIB2::center_missing;

class GRIB2::LayerAllocator {
public:
	static inline std::size_t pagesize(void);
	static inline void readonly(const void *ptr);
	static inline void readwrite(const void *ptr);
};

const GRIB2::LayerAllocator GRIB2::LayerAlloc;

inline std::size_t GRIB2::LayerAllocator::pagesize(void)
{
#if defined(HAVE_SYSCONF)
	return sysconf(_SC_PAGE_SIZE);
#else
	return 4096;
#endif
}

#undef DEBUG_LAYERALLOC

#if !defined(HAVE_MMAP)
#undef DEBUG_LAYERALLOC
#endif

inline void *operator new(std::size_t sz, const GRIB2::LayerAllocator& a)
{
#if defined(DEBUG_LAYERALLOC)
	const std::size_t pgsz(a.pagesize());
	const std::size_t objsz((sz + 15 + pgsz) & ~pgsz);
	void *ptr(mmap(0, objsz, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0));
	if (ptr == MAP_FAILED)
		throw std::bad_alloc();
	*(std::size_t *)ptr = objsz;
	return ptr + 16;
#else
	return ::operator new(sz);
#endif
}

inline void *operator new[](std::size_t sz, const GRIB2::LayerAllocator& a)
{
#if defined(DEBUG_LAYERALLOC)
	return ::operator new(sz, a);
#else
	return ::operator new[](sz);
#endif
}

inline void operator delete(void *ptr, const GRIB2::LayerAllocator& a) throw ()
{
#if defined(DEBUG_LAYERALLOC)
	//const std::size_t pgsz(a.pagesize());
	void *ptr1(ptr - 16);
	std::size_t objsz(*(std::size_t *)ptr1);

	if (((uint8_t *)ptr1)[8]) {
		std::cerr << "Double delete" << std::endl;
		*(int *)0 = 0;
	}
	((uint8_t *)ptr1)[8] = 1;
	mprotect(ptr1, objsz, PROT_READ);

	//munmap(ptr1, objsz);
#else
	::operator delete(ptr);
#endif
}

inline void operator delete[](void *ptr, const GRIB2::LayerAllocator& a) throw ()
{
#if defined(DEBUG_LAYERALLOC)
	::operator delete(ptr, a);
#else
	::operator delete[](ptr);
#endif
}

inline void GRIB2::LayerAllocator::readonly(const void *ptr)
{
#if defined(DEBUG_LAYERALLOC)
	//const std::size_t pgsz(a.pagesize());
	void *ptr1(const_cast<void *>(ptr) - 16);
	std::size_t objsz(*(std::size_t *)ptr1);
	mprotect(ptr1, objsz, PROT_READ);
#endif
}

inline void GRIB2::LayerAllocator::readwrite(const void *ptr)
{
#if defined(DEBUG_LAYERALLOC)
	//const std::size_t pgsz(a.pagesize());
	void *ptr1(const_cast<void *>(ptr) - 16);
	std::size_t objsz(*(std::size_t *)ptr1);
	mprotect(ptr1, objsz, PROT_READ | PROT_WRITE);
#endif
}

GRIB2::Grid::Grid()
	: m_refcount(0)
{
}

GRIB2::Grid::~Grid()
{
}

std::pair<float,float> GRIB2::Grid::axes_to_dirmag(float u, float v)
{
	std::pair<float,float> r(atan2f(u, v) * (180.f / M_PI), sqrtf(u * u + v * v));
	if (r.first < 0)
		r.first += 360;
	else if (r.first > 360)
		r.first -= 360;
	return r;
}

std::pair<float,float> GRIB2::Grid::axes_to_dirmag(std::pair<float,float> uv)
{
	return axes_to_dirmag(uv.first, uv.second);
}

std::pair<float,float> GRIB2::Grid::axes_to_dirmag_kts(float u, float v)
{
	return axes_to_dirmag(u * (-1e-3f * Point::km_to_nmi * 3600), v * (-1e-3f * Point::km_to_nmi * 3600));
}

std::pair<float,float> GRIB2::Grid::axes_to_dirmag_kts(std::pair<float,float> uv)
{
	return axes_to_dirmag_kts(uv.first, uv.second);
}

GRIB2::GridLatLon::GridLatLon(const Point& origin, const Point& ptsz, unsigned int usz, unsigned int vsz, int scu, int scv, int offs, bool geoaxes)
	: m_origin(origin), m_pointsize(ptsz), m_usize(usz), m_vsize(vsz), m_scaleu(scu), m_scalev(scv), m_offset(offs), m_geoaxes(geoaxes)
{
}

unsigned int GRIB2::GridLatLon::operator()(int u, int v) const
{
	if (u < 0)
		u = 0;
	else if ((unsigned int)u >= m_usize)
		u = m_usize - 1;
	if (v < 0)
		v = 0;
	else if ((unsigned int)v >= m_vsize)
		v = m_vsize - 1;
	return m_offset + u * m_scaleu + v * m_scalev;
}

Point GRIB2::GridLatLon::get_center(int u, int v) const
{
	if (u < 0)
		u = 0;
	else if ((unsigned int)u >= m_usize)
		u = m_usize - 1;
	if (v < 0)
		v = 0;
	else if ((unsigned int)v >= m_vsize)
		v = m_vsize - 1;
	return Point(m_origin.get_lon() + u * m_pointsize.get_lon(),
		     m_origin.get_lat() + v * m_pointsize.get_lat());
}

bool GRIB2::GridLatLon::operator==(const Grid& x) const
{
	const GridLatLon *xx(dynamic_cast<const GridLatLon *>(&x));
	if (!xx)
		return false;
	if (m_origin != xx->m_origin)
		return false;
	if (m_pointsize != xx->m_pointsize)
		return false;
	if (m_usize != xx->m_usize)
		return false;
	if (m_vsize != xx->m_vsize)
		return false;
	if (m_scaleu != xx->m_scaleu)
		return false;
	if (m_scalev != xx->m_scalev)
		return false;
	if (m_offset != xx->m_offset)
		return false;
	if (m_geoaxes != xx->m_geoaxes)
		return false;
	return true;
}

std::pair<float,float> GRIB2::GridLatLon::transform_axes(float u, float v) const
{
	std::pair<float,float> r(0, 0);
	if (m_geoaxes || m_scaleu > 0)
		r.first = u;
	else if (m_scaleu < 0)
		r.first = -u;
	if (m_geoaxes || m_scalev > 0)
		r.second = v;
	else if (m_scalev < 0)
		r.second = -v;
	return r;
}

#ifdef HAVE_LIBCRYPTO
GRIB2::Cache::Cache(const std::vector<uint8_t>& filedata)
{
	if (!filedata.size()) {
		memset(m_hash, 0, sizeof(m_hash));
		return;
	}
	MD4(&filedata[0], filedata.size(), m_hash);
}

GRIB2::Cache::Cache(const uint8_t *ptr, unsigned int len)
{
	if (!ptr || !len) {
		memset(m_hash, 0, sizeof(m_hash));
		return;
	}
	MD4(ptr, len, m_hash);
}

std::string GRIB2::Cache::get_filename(const std::string& cachedir) const
{
	if (cachedir.empty())
		return cachedir;
	std::ostringstream oss;
	oss << "jpeg2000." << std::hex;
	bool nz(false);
	for (unsigned int i(0); i < sizeof(m_hash); ++i) {
		nz = nz || m_hash[i];
		oss << std::setw(2) << std::setfill('0') << (unsigned int)m_hash[i];
	}
	if (!nz)
		return "";
	return Glib::build_filename(cachedir, oss.str());
}

bool GRIB2::Cache::load(const std::string& cachedir, std::vector<uint8_t>& data) const
{
	std::string filename(get_filename(cachedir));
	if (filename.empty())
		return false;
	int fd(open(filename.c_str(), O_RDONLY));
	if (fd == -1)
		return false;
	struct stat st;
	if (fstat(fd, &st)) {
		close(fd);
		return false;
	}
	if (!st.st_size || st.st_size > 1024*1024*128)
		return false;
	data.resize(st.st_size, 0);
	ssize_t r(read(fd, &data[0], data.size()));
	close(fd);
	return r == data.size();
}

void GRIB2::Cache::save(const std::string& cachedir, const uint8_t *ptr, unsigned int len)
{
	if (!ptr || !len)
		return;
	std::string filename(get_filename(cachedir));
	if (filename.empty())
		return;
	int fd(open(filename.c_str(), O_CREAT | O_RDWR | O_TRUNC | O_EXCL, 0755));
	if (fd == -1)
		return;
	ssize_t r(write(fd, ptr, len));
	close(fd);
	if (r != len)
		unlink(filename.c_str());
}

template <typename T> bool GRIB2::Cache::load(const std::string& cachedir, std::vector<T>& data, unsigned int typesz) const
{
	std::string filename(get_filename(cachedir));
	if (filename.empty())
		return false;
	int fd(open(filename.c_str(), O_RDONLY));
	if (fd == -1)
		return false;
	struct stat st;
	if (fstat(fd, &st)) {
		close(fd);
		return false;
	}
	if (!st.st_size || st.st_size > 1024*1024*128)
		return false;
	unsigned int sz(st.st_size / typesz);
	uint8_t d[st.st_size];
	ssize_t r(read(fd, d, st.st_size));
	close(fd);
	if (r != st.st_size)
		return false;
	data.resize(sz, 0);
	const uint8_t *dp = d;
	for (unsigned int i = 0; i < sz; ++i) {
		T d(0);
		for (unsigned int sh(0); sh < CHAR_BIT*typesz; sh += CHAR_BIT)
			d |= ((T)*dp++) << sh;
		if (typesz < sizeof(T) && (T)-1 < (T)0)
			d |= -(d & (((T)1) << (typesz * CHAR_BIT - 1)));
		data[i] = d;
	}
	return true;
}

template <typename T> void GRIB2::Cache::save(const std::string& cachedir, const T *ptr, unsigned int len, unsigned int typesz)
{
	if (!ptr || !len)
		return;
	std::string filename(get_filename(cachedir));
	if (filename.empty())
		return;
	int fd(open(filename.c_str(), O_CREAT | O_RDWR | O_TRUNC | O_EXCL, 0755));
	if (fd == -1)
		return;
	unsigned int blen(len * typesz);
	uint8_t d[blen];
	uint8_t *dp = d;
	for (unsigned int i = 0; i < len; ++i) {
		T d(ptr[i]);
		for (unsigned int j(0); j < typesz; ++j) {
			*dp++ = d;
			d >>= CHAR_BIT;
		}
	}
	ssize_t r(write(fd, d, blen));
	close(fd);
	if (r != blen)
		unlink(filename.c_str());
}

unsigned int GRIB2::Cache::expire(const std::string& cachedir, unsigned int maxdays, off_t maxbytes)
{
	if (cachedir.empty())
		return 0;
	time_t texp;
	time(&texp);
	texp -= maxdays*24*60*60;
	Glib::Dir d(cachedir);
	unsigned int ret(0);
	off_t bytes(0);
	typedef std::pair<std::string,off_t> file_t;
	typedef std::multimap<time_t,file_t> files_t;
	files_t files;
        for (;;) {
		std::string filename(d.read_name());
		if (filename.empty())
			break;
		filename = Glib::build_filename(cachedir, filename);
		struct stat st;
		if (stat(filename.c_str(), &st))
			continue;
		if (st.st_atime >= texp) {
			bytes += st.st_size;
			files.insert(files_t::value_type(st.st_atime, file_t(filename, st.st_size)));
			continue;
		}
		unlink(filename.c_str());
		++ret;
	}
	while (bytes > maxbytes && !files.empty()) {
		bytes -= std::min(bytes, files.begin()->second.second);
		unlink(files.begin()->second.first.c_str());
		files.erase(files.begin());
	}
	return ret;
}
#endif

const std::string& to_str(GRIB2::Layer::model_t m)
{
	switch (m) {
	case GRIB2::Layer::model_t::gfs:
	{
		static const std::string r("GFS");
		return r;
	}

	case GRIB2::Layer::model_t::dwdiconglobal:
	{
		static const std::string r("DWD Icon Global");
		return r;
	}

	case GRIB2::Layer::model_t::dwdiconeu:
	{
		static const std::string r("DWD Icon EU");
		return r;
	}

	default:
	{
		static const std::string r("Unknown");
		return r;
	}
	}
}

GRIB2::Layer::Layer(const Parameter *param, const boost::intrusive_ptr<Grid const>& grid,
		    gint64 reftime, gint64 efftime, uint16_t centerid, uint16_t subcenterid,
		    uint8_t productionstatus, uint8_t datatype, uint8_t genprocess, uint8_t genprocesstype,
		    uint8_t surface1type, double surface1value, uint8_t surface2type, double surface2value,
		    statproc_t statproc)
	: m_refcount(0), m_grid(grid), m_reftime(reftime), m_efftime(efftime), m_cachetime(0),
	  m_parameter(param), m_surface1value(surface1value), m_surface2value(surface2value),
	  m_centerid(centerid), m_subcenterid(subcenterid), m_productionstatus(productionstatus), m_datatype(datatype),
	  m_genprocess(genprocess), m_genprocesstype(genprocesstype), m_surface1type(surface1type), m_surface2type(surface2type),
	  m_statproc(statproc)
{
}

GRIB2::Layer::~Layer()
{
	m_expire.disconnect();
}

GRIB2::Layer::model_t GRIB2::Layer::get_model(void) const
{
	switch (get_centerid()) {
	case center_usnationalweatherservice_ncep_wmc:
		return model_t::gfs;

	case center_offenbach_rsmc_1:
		switch (get_genprocesstype()) {
		case 1:
			return model_t::dwdiconglobal;

		case 2:
			return model_t::dwdiconeu;

		default:
			return model_t::unknown;
		}

	default:
		return model_t::unknown;
	}
}

bool GRIB2::Layer::is_interpolatable(const Parameter *par)
{
	if (!par)
		return true;
	switch (par->get_fullid()) {
	case param_meteorology_moisture_crain:
	case param_meteorology_moisture_cfrzr:
	case param_meteorology_moisture_cicep:
	case param_meteorology_moisture_csnow:
		// FIXME: should probably be false as well
		return true;

	case param_meteorology_atmosphere_physics_wiww:
		return false;

	default:
		return true;
	}
}

void GRIB2::Layer::readonly(void) const
{
	LayerAllocator().readonly(this);
}

void GRIB2::Layer::readwrite(void) const
{
	LayerAllocator().readwrite(this);
}

void GRIB2::Layer::expire_time(long curtime)
{
	readwrite();
	Glib::Mutex::Lock lock(m_mutex);
	if (curtime < m_cachetime) {
		readonly();
		return;
	}
	m_cachetime = 0;
	m_data.clear();
	m_expire.disconnect();
	readonly();
}

void GRIB2::Layer::expire_now(void)
{
	Glib::TimeVal tv;
	tv.assign_current_time();
	expire_time(tv.tv_sec);
}

boost::intrusive_ptr<GRIB2::LayerResult> GRIB2::Layer::get_results(const Rect& bbox)
{
	static constexpr bool trace(false);
	if (!m_grid) {
		if (true)
			std::cerr << "GRIB2: layer has no grid" << std::endl;
		return boost::intrusive_ptr<LayerResult>();
	}
	readwrite();
	{
		Glib::Mutex::Lock lock(m_mutex);
		m_expire.disconnect();
	}
	if (m_data.empty()) {
		load();
		if (m_data.empty()) {
			if (true)
				std::cerr << "GRIB2: cannot load layer" << std::endl;
			readonly();
			return boost::intrusive_ptr<LayerResult>();
		}
	}
	{
		Glib::TimeVal tv;
		tv.assign_current_time();
		tv.add_seconds(60);
		m_cachetime = tv.tv_sec;
	}
#if !defined(DEBUG_LAYERALLOC)
	{
		Glib::Mutex::Lock lock(m_mutex);
		m_expire = Glib::signal_timeout().connect_seconds(sigc::bind_return(sigc::mem_fun(*this, &Layer::expire_now), false), 65);
	}
#endif
	static const int64_t twopower32(((int64_t)1) << 32);
	Point ptsz(m_grid->get_pointsize());
	Point pthalfsz(ptsz.get_lon() / 2, ptsz.get_lat() / 2);
	Point ptorigin(m_grid->get_center(0, 0));
	unsigned int usize(m_grid->get_usize());
	unsigned int vsize(m_grid->get_vsize());
	if (trace)
		std::cerr << "GRIB2: Grid: (" << usize << 'x' << vsize << ") pointsz " << ptsz.get_lat_str2()
			  << ' ' << ptsz.get_lon_str2() << " origin " << ptorigin.get_lat_str2()
			  << ' ' << ptorigin.get_lon_str2() << std::endl;
	int umin, umax, vmin, vmax;
	{
		int64_t by1((Point::coord_t)(bbox.get_south() - (uint32_t)(ptorigin.get_lat() - (uint32_t)pthalfsz.get_lat())));
		int64_t by2(bbox.get_north() - (int64_t)bbox.get_south());
		by2 += by1;
		vmin = by1 / ptsz.get_lat();
		vmax = (by2 + ptsz.get_lat() - 1) / ptsz.get_lat();
		if (vmin < 0)
			vmin = 0;
		else if (vmin >= (int)vsize)
			vmin = vsize - 1;
		if (vmax >= (int)vsize)
			vmax = vsize - 1;
		else if (vmax < vmin)
			vmax = vmin;
	}
	{
		int64_t bx1((Point::coord_t)(bbox.get_west() - (uint32_t)(ptorigin.get_lon() - (uint32_t)pthalfsz.get_lon())));
		if (bx1 < 0)
			bx1 += twopower32;
		int64_t bx2(bx1);
		bx2 += (uint32_t)(bbox.get_east() - (uint32_t)bbox.get_west());
		int64_t xw(ptsz.get_lon());
		xw *= usize;
		if (xw >= twopower32) {
			umin = bx1 / ptsz.get_lon();
			umax = (bx2 - bx1 + ptsz.get_lon() - 1) / ptsz.get_lon();
		} else {
			int64_t cx1(bx1);
			int64_t cx2(std::min(xw, bx2));
			int64_t dx1(std::max(twopower32, bx1));
			int64_t dx2(std::min(twopower32 + xw, bx2));
			if ((dx2 - dx1) > (cx2 - cx1)) {
				cx1 = dx1;
				cx2 = dx2;
			}
			umin = cx1 / ptsz.get_lon();
			umax = (cx2 - cx1 + ptsz.get_lon() - 1) / ptsz.get_lon();
		}
		if (umin < 0)
			umin = 0;
		else if (umin >= (int)usize)
			umin = usize - 1;
		if (umax < 0)
			umax = 0;
		else if (umax >= (int)usize)
			umax = usize - 1;
	}
	Rect laybbox(m_grid->get_center(umin, vmin) - pthalfsz, Point());
	laybbox.set_north(laybbox.get_south() + (vmax + 1 - vmin) * ptsz.get_lat());
	{
		int64_t x(ptsz.get_lon());
		x *= (umax + 1);
		if (x >= twopower32)
			x = twopower32 - 1;
		laybbox.set_east(laybbox.get_west() + x);
	}
	boost::intrusive_ptr<LayerResult> lr(new (LayerAlloc) LayerResult(get_ptr(), laybbox, umax + 1, vmax + 1 - vmin, get_efftime(), get_reftime(), get_reftime(), get_surface1value()));
	umax += umin;
	if (umax >= (int)usize)
		umax -= usize;
	if (trace)
		std::cerr << "GRIB2: query " << bbox << " result (" << umin << ".." << umax << ',' << vmin << ".." << vmax
			  << ") " << laybbox << std::endl;
	unsigned int vv(0);
	for (int v = vmin; v <= vmax; ++vv, ++v) {
		unsigned int uu(0);
		for (int u = umin; ; ++uu) {
			lr->operator()(uu, vv) = m_data[m_grid->operator()(u, v)];
			if (u == umax)
				break;
			++u;
			if (u >= (int)usize)
				u = 0;
		}
	}
	readonly();
	lr->readonly();
	return lr;
}

unsigned int GRIB2::Layer::extract(const std::vector<uint8_t>& filedata, unsigned int offs, unsigned int width)
{
	unsigned int r(0);
	while (width > 0) {
		unsigned int bi(offs >> 3);
		if (bi >= filedata.size())
			return 0;
		unsigned int bo(offs & 7);
		unsigned int w(std::min(width, 8 - bo));
		unsigned int m((1U << w) - 1U);
		r <<= w;
		r |= ((filedata[bi] << bo) >> (8 - w)) & m;
		offs += w;
		width -= w;
	}
	return r;
}

std::ostream& GRIB2::Layer::print_param(std::ostream& os)
{
	if (!m_parameter)
		return os;
	os << (unsigned int)m_parameter->get_id() << ':'
	   << (unsigned int)m_parameter->get_category_id() << ':'
	   << (unsigned int)m_parameter->get_discipline_id();
	if (m_parameter->get_str())
		os << ' ' << m_parameter->get_str();
	if (m_parameter->get_abbrev())
		os << " (" << m_parameter->get_abbrev() << ')';
	if (m_parameter->get_unit())
		os << " [" << m_parameter->get_unit() << ']';
	return os;
}

GRIB2::LayerJ2KParam::LayerJ2KParam(void)
	: m_datascale(std::numeric_limits<double>::quiet_NaN()), m_dataoffset(std::numeric_limits<double>::quiet_NaN())
{
}

GRIB2::LayerSimplePackingParam::LayerSimplePackingParam(void)
	: m_nbitsgroupref(0), m_fieldvaluetype(255)
{
}

GRIB2::LayerComplexPackingParam::LayerComplexPackingParam(void)
	: m_ngroups(0), m_refgroupwidth(0), m_nbitsgroupwidth(0),
	  m_refgrouplength(0), m_incrgrouplength(0), m_lastgrouplength(0), m_nbitsgrouplength(0),
	  m_primarymissingvalue(0), m_secondarymissingvalue(0), m_groupsplitmethod(255), m_missingvaluemgmt(255)
{
}

bool GRIB2::LayerComplexPackingParam::is_primarymissingvalue(void) const
{
	switch (get_missingvaluemgmt()) {
	case 1:
	case 2:
		return true;

	default:
		return false;
	}
}

bool GRIB2::LayerComplexPackingParam::is_secondarymissingvalue(void) const
{
	switch (get_missingvaluemgmt()) {
	case 2:
		return true;

	default:
		return false;
	}
}

unsigned int GRIB2::LayerComplexPackingParam::get_primarymissingvalue_raw(void) const
{
	if (is_primarymissingvalue())
		return (1U << get_nbitsgroupref()) - 1U;
	return 0;
}

unsigned int GRIB2::LayerComplexPackingParam::get_secondarymissingvalue_raw(void) const
{
	if (is_secondarymissingvalue())
		return (1U << get_nbitsgroupref()) - 2U;
	return 0;
}

double GRIB2::LayerComplexPackingParam::get_primarymissingvalue_float(void) const
{
	if (!is_fieldvalue_float())
		return get_primarymissingvalue();
	union {
		uint32_t u;
		float f;
	} u;
	u.u = get_primarymissingvalue();
	return u.f;
}

double GRIB2::LayerComplexPackingParam::get_secondarymissingvalue_float(void) const
{
	if (!is_fieldvalue_float())
		return get_secondarymissingvalue();
	union {
		uint32_t u;
		float f;
	} u;
	u.u = get_secondarymissingvalue();
	return u.f;
}

GRIB2::LayerComplexPackingSpatialDiffParam::LayerComplexPackingSpatialDiffParam(void)
	: m_spatialdifforder(0), m_extradescroctets(0)
{
}

#if defined(HAVE_OPENJPEG) && !defined(HAVE_OPENJPEG1)

namespace {

	class OpjMemStream {
	public:
		OpjMemStream(const std::vector<uint8_t>& filedata);
		~OpjMemStream();
		opj_stream_t *get_stream(void) { return m_stream; }

	protected:
		const std::vector<uint8_t>& m_filedata;
		opj_stream_t *m_stream;
		OPJ_OFF_T m_offs;

		OPJ_SIZE_T opj_mem_stream_read(void *p_buffer, OPJ_SIZE_T p_nb_bytes);
		OPJ_SIZE_T opj_mem_stream_write(void *p_buffer, OPJ_SIZE_T p_nb_bytes);
		OPJ_OFF_T opj_mem_stream_skip(OPJ_OFF_T p_nb_bytes);
		OPJ_BOOL opj_mem_stream_seek(OPJ_OFF_T p_nb_bytes);
		void opj_mem_stream_free_user_data(void);

		static OPJ_SIZE_T opj_mem_stream_read_1(void *p_buffer, OPJ_SIZE_T p_nb_bytes, void *p_user_data) {
			return ((OpjMemStream *)p_user_data)->opj_mem_stream_read(p_buffer, p_nb_bytes);
		}

		static OPJ_SIZE_T opj_mem_stream_write_1(void *p_buffer, OPJ_SIZE_T p_nb_bytes, void *p_user_data) {
			return ((OpjMemStream *)p_user_data)->opj_mem_stream_write(p_buffer, p_nb_bytes);
		}

		static OPJ_OFF_T opj_mem_stream_skip_1(OPJ_OFF_T p_nb_bytes, void *p_user_data) {
			return ((OpjMemStream *)p_user_data)->opj_mem_stream_skip(p_nb_bytes);
		}

		static OPJ_BOOL opj_mem_stream_seek_1(OPJ_OFF_T p_nb_bytes, void *p_user_data) {
			return ((OpjMemStream *)p_user_data)->opj_mem_stream_seek(p_nb_bytes);
		}

		static void opj_mem_stream_free_user_data_1(void *p_user_data) {
			return ((OpjMemStream *)p_user_data)->opj_mem_stream_free_user_data();
		}
	};

	OpjMemStream::OpjMemStream(const std::vector<uint8_t>& filedata)
		: m_filedata(filedata), m_stream(0), m_offs(0)
	{
		m_stream = opj_stream_create(m_filedata.size(), 1);
		if (!m_stream)
			return;
		opj_stream_set_user_data(m_stream, this, opj_mem_stream_free_user_data_1);
		opj_stream_set_user_data_length(m_stream, m_filedata.size());
		opj_stream_set_read_function(m_stream, opj_mem_stream_read_1);
		opj_stream_set_write_function(m_stream, opj_mem_stream_write_1);
		opj_stream_set_skip_function(m_stream, opj_mem_stream_skip_1);
		opj_stream_set_seek_function(m_stream, opj_mem_stream_seek_1);
	}

	OpjMemStream::~OpjMemStream()
	{
		if (m_stream)
			opj_stream_destroy(m_stream);
	}

	OPJ_SIZE_T OpjMemStream::opj_mem_stream_read(void *p_buffer, OPJ_SIZE_T p_nb_bytes)
	{
		OPJ_SIZE_T r = p_nb_bytes;
		if (m_offs + r > m_filedata.size())
			r = m_filedata.size() - m_offs;
		memcpy(p_buffer, &m_filedata[m_offs], r);
		m_offs += r;
		return r;
	}

	OPJ_SIZE_T OpjMemStream::opj_mem_stream_write(void *p_buffer, OPJ_SIZE_T p_nb_bytes)
	{
		return 0;
	}

	OPJ_OFF_T OpjMemStream::opj_mem_stream_skip(OPJ_OFF_T p_nb_bytes)
	{
		OPJ_SIZE_T r = p_nb_bytes;
		if (m_offs + r > m_filedata.size())
			r = m_filedata.size() - m_offs;
		m_offs += r;
		return r;
	}

	OPJ_BOOL OpjMemStream::opj_mem_stream_seek(OPJ_OFF_T p_nb_bytes)
	{
		m_offs = p_nb_bytes;
		if (m_offs <= m_filedata.size())
			return OPJ_TRUE;
		m_offs = m_filedata.size();
		return OPJ_FALSE;
	}

	void OpjMemStream::opj_mem_stream_free_user_data(void)
	{
	}

};

#endif

GRIB2::LayerJ2K::LayerJ2K(const Parameter *param, const boost::intrusive_ptr<Grid const>& grid,
			  gint64 reftime, gint64 efftime, uint16_t centerid, uint16_t subcenterid,
			  uint8_t productionstatus, uint8_t datatype, uint8_t genprocess, uint8_t genprocesstype,
			  uint8_t surface1type, double surface1value, uint8_t surface2type, double surface2value,
			  statproc_t statproc, const LayerJ2KParam& layerparam, goffset bitmapoffs, bool bitmap,
			  goffset fileoffset, gsize filesize, const std::string& filename,
			  const std::string& cachedir)
	: Layer(param, grid, reftime, efftime, centerid, subcenterid,
		productionstatus, datatype, genprocess, genprocesstype, surface1type, surface1value,
		surface2type, surface2value, statproc),
	  m_filename(filename), m_cachedir(cachedir), m_param(layerparam), m_bitmapoffset(bitmapoffs),
	  m_fileoffset(fileoffset), m_filesize(filesize), m_bitmap(bitmap)
{
}

void GRIB2::LayerJ2K::load(void)
{
	Glib::TimeVal tv;
	tv.assign_current_time();
	Glib::Mutex::Lock lock(m_mutex);
	if (m_cachetime > tv.tv_sec) {
		if (true)
			std::cerr << "GRIB2: do not load layer due to negative cache" << std::endl;
		return;
	}
	if (false)
		std::cerr << "Layer " << get_parameter()->get_abbrev_nonnull() << " load" << std::endl;
	m_data.clear();
	m_cachetime = std::numeric_limits<long>::max();
	if (!m_grid) {
		if (true)
			std::cerr << "GRIB2: layer has no grid" << std::endl;
		return;
	}
	if (!m_filesize)
		return;
	unsigned int usize(m_grid->get_usize());
        unsigned int vsize(m_grid->get_vsize());
	unsigned int uvsize(usize * vsize);
	if (!uvsize)
		return;
	std::vector<uint8_t> bitmap;
	std::vector<uint8_t> filedata;
	Glib::RefPtr<Gio::File> file(Gio::File::create_for_path(m_filename));
	Glib::RefPtr<Gio::FileInputStream> instream(file->read());
	if (m_bitmap) {
		bitmap.resize((uvsize + 7) >> 3, 0);
		if (!instream->seek(m_bitmapoffset, Glib::SEEK_TYPE_SET)) {
			if (true)
				std::cerr << "GRIB2: cannot seek to bitmap" << std::endl;
			return;
		}
		int r(instream->read(&bitmap[0], bitmap.size()));
		if (r != (int)bitmap.size()) {
			if (true)
				std::cerr << "GRIB2: cannot read bitmap" << std::endl;
			return;
		}
	}
	{
		filedata.resize(m_filesize, 0);
		if (!instream->seek(m_fileoffset, Glib::SEEK_TYPE_SET)) {
			if (true)
				std::cerr << "GRIB2: cannot seek to data" << std::endl;
			return;
		}
		int r(instream->read(&filedata[0], filedata.size()));
		if (r != (int)filedata.size()) {
			if (true)
				std::cerr << "GRIB2: cannot read data" << std::endl;
			return;
		}
	}
	Cache cache(filedata);
	{
		std::vector<int> data;
		if (cache.load(m_cachedir, data)) {
			m_data.resize(uvsize, std::numeric_limits<float>::quiet_NaN());
			if (m_bitmap) {
				unsigned int imgsz(data.size());
				unsigned int imgptr(0);
				for (unsigned int i = 0; i < uvsize; ++i) {
					if (!((bitmap[i >> 3] << (i & 7)) & 0x80))
						continue;
					if (imgptr >= imgsz)
						break;
					m_data[i] = m_param.scale(data[imgptr++]);
				}
			} else {
				for (unsigned int i = 0, n = std::min(uvsize, (unsigned int)data.size()); i < n; ++i)
					m_data[i] = m_param.scale(data[i]);
			}
			if (false)
				std::cerr << "LayerJ2K: load (cached): dataoffset " << m_param.get_dataoffset()
					  << " datascale " << m_param.get_datascale() << " data " << m_param.scale(data[0])
					  << '(' << data[0] << ')' << std::endl;
			tv.add_seconds(60);
			m_cachetime = tv.tv_sec;
			return;
		}
	}
#ifdef HAVE_OPENJPEG
#ifdef HAVE_OPENJPEG1
	opj_common_struct_t cinfo;
	opj_codestream_info_t cstr_info;
	memset(&cinfo, 0, sizeof(cinfo));
	memset(&cstr_info, 0, sizeof(cstr_info));
	opj_cio_t *cio(opj_cio_open(&cinfo, &filedata[0], filedata.size()));
	opj_dinfo_t *dinfo(opj_create_decompress(CODEC_J2K));
	opj_dparameters_t param;
	opj_set_default_decoder_parameters(&param);
	opj_setup_decoder(dinfo, &param);
	opj_image_t *img(opj_decode_with_info(dinfo, cio, &cstr_info));
	opj_destroy_decompress(dinfo);
	opj_cio_close(cio);
	if (img->numcomps == 1 && (m_bitmap || (img->comps[0].w == cstr_info.image_w && img->comps[0].h == cstr_info.image_h))) {
		cache.save(m_cachedir, img->comps[0].data, cstr_info.image_w * cstr_info.image_h);
		m_data.resize(uvsize, std::numeric_limits<float>::quiet_NaN());
		if (m_bitmap) {
			unsigned int imgsz(cstr_info.image_w * cstr_info.image_h);
			unsigned int imgptr(0);
			for (unsigned int i = 0; i < uvsize; ++i) {
				if (!((bitmap[i >> 3] << (i & 7)) & 0x80))
					continue;
				if (imgptr >= imgsz)
					break;
				m_data[i] = m_param.scale(img->comps[0].data[imgptr++]);
			}
		} else {
			for (unsigned int i = 0; i < uvsize; ++i)
				m_data[i] = m_param.scale(img->comps[0].data[i]);
		}
		if (false)
			std::cerr << "LayerJ2K: load: dataoffset " << m_param.get_dataoffset()
				  << " datascale " << m_param.get_datascale() << " data " << m_param.scale(img->comps[0].data[0])
				  << '(' << img->comps[0].data[0] << ')' << std::endl;
	}
	opj_image_destroy(img);
	opj_destroy_cstr_info(&cstr_info);
#else
	opj_image_t *img(0);
	{
		OpjMemStream streamdata(filedata);
		opj_codec_t *codec(opj_create_decompress(OPJ_CODEC_J2K));
		if (codec) {
			opj_dparameters_t param;
			opj_set_default_decoder_parameters(&param);
			if (opj_setup_decoder(codec, &param)) {
				if (opj_read_header(streamdata.get_stream(), codec, &img)) {
					if (!opj_decode(codec, streamdata.get_stream(), img)) {
						opj_image_destroy(img);
						img = 0;
					}
				}
			}
			opj_destroy_codec(codec);
		}
	}
	if (img) {
		if (img->numcomps == 1) {
			cache.save(m_cachedir, img->comps[0].data, img->comps[0].w * img->comps[0].h);
			m_data.resize(uvsize, std::numeric_limits<float>::quiet_NaN());
			if (m_bitmap) {
				unsigned int imgsz(img->comps[0].w * img->comps[0].h);
				unsigned int imgptr(0);
				for (unsigned int i = 0; i < uvsize; ++i) {
					if (!((bitmap[i >> 3] << (i & 7)) & 0x80))
						continue;
					if (imgptr >= imgsz)
						break;
					m_data[i] = m_param.scale(img->comps[0].data[imgptr++]);
				}
			} else {
				for (unsigned int i = 0; i < uvsize; ++i)
					m_data[i] = m_param.scale(img->comps[0].data[i]);
			}
			if (false)
				std::cerr << "LayerJ2K: load: dataoffset " << m_param.get_dataoffset()
					  << " datascale " << m_param.get_datascale() << " data " << m_param.scale(img->comps[0].data[0])
					  << '(' << img->comps[0].data[0] << ')' << std::endl;
		}
		opj_image_destroy(img);
	}
#endif
#endif
	if (m_data.empty()) {
		if (true)
			std::cerr << "GRIB2: JPEG2000 data format error" << std::endl;
		return;
	}
	tv.add_seconds(60);
	m_cachetime = tv.tv_sec;
}

bool GRIB2::LayerJ2K::check_load(void)
{
	if (!m_filesize)
		return false;
	return Glib::file_test(m_filename, Glib::FILE_TEST_EXISTS) && !Glib::file_test(m_filename, Glib::FILE_TEST_IS_DIR);
}


GRIB2::LayerSimplePacking::LayerSimplePacking(const Parameter *param, const boost::intrusive_ptr<Grid const>& grid,
					      gint64 reftime, gint64 efftime, uint16_t centerid, uint16_t subcenterid,
					      uint8_t productionstatus, uint8_t datatype, uint8_t genprocess, uint8_t genprocesstype,
					      uint8_t surface1type, double surface1value, uint8_t surface2type, double surface2value,
					      statproc_t statproc, const LayerSimplePackingParam& layerparam,
					      goffset bitmapoffs, bool bitmap, goffset fileoffset, gsize filesize, const std::string& filename)
	: Layer(param, grid, reftime, efftime, centerid, subcenterid,
		productionstatus, datatype, genprocess, genprocesstype,
		surface1type, surface1value, surface2type, surface2value, statproc),
	  m_filename(filename), m_param(layerparam), m_bitmapoffset(bitmapoffs),
	  m_fileoffset(fileoffset), m_filesize(filesize), m_bitmap(bitmap)
{
}

void GRIB2::LayerSimplePacking::load(void)
{
	Glib::TimeVal tv;
	tv.assign_current_time();
	Glib::Mutex::Lock lock(m_mutex);
	if (m_cachetime > tv.tv_sec) {
		if (true)
			std::cerr << "GRIB2: do not load layer due to negative cache" << std::endl;
		return;
	}
	if (false)
		std::cerr << "Layer " << get_parameter()->get_abbrev_nonnull() << " load" << std::endl;
	m_data.clear();
	m_cachetime = std::numeric_limits<long>::max();
	if (!m_grid) {
		if (true)
			std::cerr << "GRIB2: layer has no grid" << std::endl;
		return;
	}
	if (!m_filesize)
		return;
	unsigned int usize(m_grid->get_usize());
        unsigned int vsize(m_grid->get_vsize());
	unsigned int uvsize(usize * vsize);
	if (!uvsize)
		return;
	std::vector<uint8_t> bitmap;
	std::vector<uint8_t> filedata;
	Glib::RefPtr<Gio::File> file(Gio::File::create_for_path(m_filename));
	Glib::RefPtr<Gio::FileInputStream> instream(file->read());
	if (m_bitmap) {
		bitmap.resize((uvsize + 7) >> 3, 0);
		if (!instream->seek(m_bitmapoffset, Glib::SEEK_TYPE_SET)) {
			if (true)
				std::cerr << "GRIB2: cannot seek to bitmap" << std::endl;
			return;
		}
		int r(instream->read(&bitmap[0], bitmap.size()));
		if (r != (int)bitmap.size()) {
			if (true)
				std::cerr << "GRIB2: cannot read bitmap" << std::endl;
			return;
		}
	}
	{
		filedata.resize(m_filesize, 0);
		if (!instream->seek(m_fileoffset, Glib::SEEK_TYPE_SET)) {
			if (true)
				std::cerr << "GRIB2: cannot seek to data" << std::endl;
			return;
		}
		int r(instream->read(&filedata[0], filedata.size()));
		if (r != (int)filedata.size()) {
			if (true)
				std::cerr << "GRIB2: cannot read data" << std::endl;
			return;
		}
	}
	m_data.resize(uvsize, std::numeric_limits<float>::quiet_NaN());
	{
		unsigned int ptr(0), width(m_param.get_nbitsgroupref());
		for (unsigned int i = 0; i < uvsize; ++i) {
			if (m_bitmap && !((bitmap[i >> 3] << (i & 7)) & 0x80))
				continue;
			m_data[i] = m_param.scale(extract(filedata, ptr, width));
			ptr += width;
		}
	}
	if (false) {
		std::cerr << "LayerSimplePacking::load:" << get_parameter()->get_category()->get_discipline()->get_str("?")
			  << ':' << get_parameter()->get_category()->get_str("?") << ':' << get_parameter()->get_str("?")
			  << ' ' << find_surfacetype_str(get_surface1type(), "?") << ',' << get_surface1value()
			  << ' ' << find_surfacetype_str(get_surface2type(), "?") << ',' << get_surface2value() << " data:";
		for (unsigned int i = 0; i < uvsize; ++i)
			std::cerr << ' ' << m_data[i];
		std::cerr << std::endl;
	}
}

bool GRIB2::LayerSimplePacking::check_load(void)
{
	if (!m_filesize)
		return false;
	if (!m_param.get_nbitsgroupref())
		return false;
	return Glib::file_test(m_filename, Glib::FILE_TEST_EXISTS) && !Glib::file_test(m_filename, Glib::FILE_TEST_IS_DIR);
}

GRIB2::LayerComplexPacking::LayerComplexPacking(const Parameter *param, const boost::intrusive_ptr<Grid const>& grid,
						gint64 reftime, gint64 efftime, uint16_t centerid, uint16_t subcenterid,
						uint8_t productionstatus, uint8_t datatype, uint8_t genprocess, uint8_t genprocesstype,
						uint8_t surface1type, double surface1value, uint8_t surface2type, double surface2value,
						statproc_t statproc, const LayerComplexPackingParam& layerparam,
						goffset bitmapoffs, bool bitmap, goffset fileoffset, gsize filesize, const std::string& filename)
	: Layer(param, grid, reftime, efftime, centerid, subcenterid,
		productionstatus, datatype, genprocess, genprocesstype,
		surface1type, surface1value, surface2type, surface2value, statproc),
	  m_filename(filename), m_param(layerparam), m_bitmapoffset(bitmapoffs),
	  m_fileoffset(fileoffset), m_filesize(filesize), m_bitmap(bitmap)
{
}

void GRIB2::LayerComplexPacking::load(void)
{
	Glib::TimeVal tv;
	tv.assign_current_time();
	Glib::Mutex::Lock lock(m_mutex);
	if (m_cachetime > tv.tv_sec) {
		if (true)
			std::cerr << "GRIB2: do not load layer due to negative cache" << std::endl;
		return;
	}
	if (false)
		std::cerr << "Layer " << get_parameter()->get_abbrev_nonnull() << " load" << std::endl;
	m_data.clear();
	m_cachetime = std::numeric_limits<long>::max();
	if (!m_grid) {
		if (true)
			std::cerr << "GRIB2: layer has no grid" << std::endl;
		return;
	}
	if (!m_filesize)
		return;
	unsigned int usize(m_grid->get_usize());
        unsigned int vsize(m_grid->get_vsize());
	unsigned int uvsize(usize * vsize);
	if (!uvsize)
		return;
	std::vector<uint8_t> bitmap;
	std::vector<uint8_t> filedata;
	Glib::RefPtr<Gio::File> file(Gio::File::create_for_path(m_filename));
	Glib::RefPtr<Gio::FileInputStream> instream(file->read());
	if (m_bitmap) {
		bitmap.resize((uvsize + 7) >> 3, 0);
		if (!instream->seek(m_bitmapoffset, Glib::SEEK_TYPE_SET)) {
			if (true)
				std::cerr << "GRIB2: cannot seek to bitmap" << std::endl;
			return;
		}
		int r(instream->read(&bitmap[0], bitmap.size()));
		if (r != (int)bitmap.size()) {
			if (true)
				std::cerr << "GRIB2: cannot read bitmap" << std::endl;
			return;
		}
	}
	{
		filedata.resize(m_filesize, 0);
		if (!instream->seek(m_fileoffset, Glib::SEEK_TYPE_SET)) {
			if (true)
				std::cerr << "GRIB2: cannot seek to data" << std::endl;
			return;
		}
		int r(instream->read(&filedata[0], filedata.size()));
		if (r != (int)filedata.size()) {
			if (true)
				std::cerr << "GRIB2: cannot read data" << std::endl;
			return;
		}
	}
	if (!m_param.is_gengroupsplit() || !m_param.get_ngroups())
		return;
	std::vector<unsigned int> grpref(m_param.get_ngroups(), 0), grpwidth(m_param.get_ngroups(), 0), grplength(m_param.get_ngroups(), 0);
	unsigned int ptr(0);
	for (unsigned int i(0), ng(m_param.get_ngroups()), grw(m_param.get_nbitsgroupref()); i < ng; ++i, ptr += grw)
		grpref[i] = extract(filedata, ptr, grw);
	ptr = (ptr + 7U) & ~7U;
	for (unsigned int i(0), ng(m_param.get_ngroups()), gww(m_param.get_nbitsgroupwidth()),
		     gwr(m_param.get_refgroupwidth()); i < ng; ++i, ptr += gww)
		grpwidth[i] = extract(filedata, ptr, gww) + gwr;
	ptr = (ptr + 7U) & ~7U;
	for (unsigned int i(0), ng(m_param.get_ngroups()), glw(m_param.get_nbitsgrouplength()),
		     glr(m_param.get_refgrouplength()), gli(m_param.get_incrgrouplength()); i < ng; ++i, ptr += glw)
		grplength[i] = extract(filedata, ptr, glw) * gli + glr;
	if (!grplength.empty())
		grplength.back() = m_param.get_lastgrouplength();
	ptr = (ptr + 7U) & ~7U;
	if (false) {
		unsigned int totlen(0), bmsz(0);
		for (std::vector<unsigned int>::const_iterator gi(grplength.begin()), ge(grplength.end()); gi != ge; ++gi)
			totlen += *gi;
		if (m_bitmap)
			for (unsigned int i = 0; i < uvsize; ++i)
				if ((bitmap[i >> 3] << (i & 7)) & 0x80)
					++bmsz;
		print_param(std::cerr << "GRIB2: LayerComplexPacking::load: param ")
			<< " surface " << (unsigned int)get_surface1type() << ' ' << get_surface1value()
			<< " / " << (unsigned int)get_surface2type() << ' ' << get_surface2value()
			<< " group length sum " << totlen << " bitmap " << bmsz << " uv " << uvsize << std::endl;
	}
	m_data.resize(uvsize, std::numeric_limits<float>::quiet_NaN());
	{
		const unsigned int primiss(m_param.get_primarymissingvalue_raw());
		const unsigned int secmiss(m_param.get_secondarymissingvalue_raw());
		const double primissfloat(m_param.get_primarymissingvalue_float());
		const double secmissfloat(m_param.get_secondarymissingvalue_float());
		unsigned int grpidx(0), grpcnt(1U), grpr(0U), grpw(1U), grpprimiss(~0U), grpsecmiss(~0U);
		if (grpidx < grplength.size()) {
			grpcnt = grplength[grpidx];
			grpr = grpref[grpidx];
			grpw = grpwidth[grpidx];
			if (grpw > 0 && m_param.is_primarymissingvalue())
				grpprimiss = (1U << grpw) - 1U;
			if (grpw > 1 && m_param.is_secondarymissingvalue())
				grpsecmiss = (1U << grpw) - 2U;
		}
		for (unsigned int i = 0; i < uvsize; ++i) {
			if (m_bitmap && !((bitmap[i >> 3] << (i & 7)) & 0x80))
				continue;
			unsigned int v(extract(filedata, ptr, grpw));
			if (v == grpprimiss) {
				m_data[i] = primissfloat;
				goto next;
			}
			if (v == grpsecmiss) {
				m_data[i] = secmissfloat;
				goto next;
			}
			v += grpr;
			if (v) {
				if (v == primiss) {
					m_data[i] = primissfloat;
					goto next;
				}
				if (v == secmiss) {
					m_data[i] = secmissfloat;
					goto next;
				}
			}
			m_data[i] = m_param.scale(v);
		  next:
			ptr += grpw;
			--grpcnt;
			if (grpcnt)
				continue;
			++grpidx;
			grpcnt = 1U;
			grpr = 0U;
			grpw = 1U;
			if (grpidx < grplength.size()) {
				grpcnt = grplength[grpidx];
				grpr = grpref[grpidx];
				grpw = grpwidth[grpidx];
			}
		}
	}
}

bool GRIB2::LayerComplexPacking::check_load(void)
{
	if (!m_filesize)
		return false;
	if (!m_param.is_gengroupsplit() || !m_param.get_ngroups())
		return false;
	return Glib::file_test(m_filename, Glib::FILE_TEST_EXISTS) && !Glib::file_test(m_filename, Glib::FILE_TEST_IS_DIR);
}

GRIB2::LayerComplexPackingSpatialDiff::LayerComplexPackingSpatialDiff(const Parameter *param, const boost::intrusive_ptr<Grid const>& grid,
								      gint64 reftime, gint64 efftime, uint16_t centerid, uint16_t subcenterid,
								      uint8_t productionstatus, uint8_t datatype, uint8_t genprocess, uint8_t genprocesstype,
								      uint8_t surface1type, double surface1value, uint8_t surface2type, double surface2value,
								      statproc_t statproc, const LayerComplexPackingSpatialDiffParam& layerparam,
								      goffset bitmapoffs, bool bitmap, goffset fileoffset, gsize filesize, const std::string& filename)
	: Layer(param, grid, reftime, efftime, centerid, subcenterid,
		productionstatus, datatype, genprocess, genprocesstype,
		surface1type, surface1value, surface2type, surface2value, statproc),
	  m_filename(filename), m_param(layerparam), m_bitmapoffset(bitmapoffs),
	  m_fileoffset(fileoffset), m_filesize(filesize), m_bitmap(bitmap)
{
}

void GRIB2::LayerComplexPackingSpatialDiff::load(void)
{
	Glib::TimeVal tv;
	tv.assign_current_time();
	Glib::Mutex::Lock lock(m_mutex);
	if (m_cachetime > tv.tv_sec) {
		if (true)
			std::cerr << "GRIB2: do not load layer due to negative cache" << std::endl;
		return;
	}
	if (false)
		std::cerr << "Layer " << get_parameter()->get_abbrev_nonnull() << " load" << std::endl;
	m_data.clear();
	m_cachetime = std::numeric_limits<long>::max();
	if (!m_grid) {
		if (true)
			std::cerr << "GRIB2: layer has no grid" << std::endl;
		return;
	}
	if (!m_filesize)
		return;
	unsigned int usize(m_grid->get_usize());
        unsigned int vsize(m_grid->get_vsize());
	unsigned int uvsize(usize * vsize);
	if (uvsize < m_param.get_spatialdifforder())
		return;
	std::vector<uint8_t> bitmap;
	std::vector<uint8_t> filedata;
	Glib::RefPtr<Gio::File> file(Gio::File::create_for_path(m_filename));
	Glib::RefPtr<Gio::FileInputStream> instream(file->read());
	if (m_bitmap) {
		bitmap.resize((uvsize + 7) >> 3, 0);
		if (!instream->seek(m_bitmapoffset, Glib::SEEK_TYPE_SET)) {
			if (true)
				std::cerr << "GRIB2: cannot seek to bitmap" << std::endl;
			return;
		}
		int r(instream->read(&bitmap[0], bitmap.size()));
		if (r != (int)bitmap.size()) {
			if (true)
				std::cerr << "GRIB2: cannot read bitmap" << std::endl;
			return;
		}
	}
	{
		filedata.resize(m_filesize, 0);
		if (!instream->seek(m_fileoffset, Glib::SEEK_TYPE_SET)) {
			if (true)
				std::cerr << "GRIB2: cannot seek to data" << std::endl;
			return;
		}
		int r(instream->read(&filedata[0], filedata.size()));
		if (r != (int)filedata.size()) {
			if (true)
				std::cerr << "GRIB2: cannot read data" << std::endl;
			return;
		}
	}
	if (!m_param.is_gengroupsplit() || !m_param.get_ngroups() || m_param.get_spatialdifforder() > 2U)
		return;
	std::vector<unsigned int> grpref(m_param.get_ngroups(), 0), grpwidth(m_param.get_ngroups(), 0), grplength(m_param.get_ngroups(), 0);
	unsigned int ptr(0);
	std::vector<int> ddata;
	unsigned int diffinit[2] = { 0, 0 };
	{
		unsigned int w(m_param.get_extradescroctets() << 3);
		for (unsigned int i(0), n(m_param.get_spatialdifforder()); i < n; ++i) {
			diffinit[i] = extract(filedata, ptr, w);
			ptr += w;
		}
		unsigned int sgn(extract(filedata, ptr, 1));
		int ghmin(extract(filedata, ptr + 1, w - 1));
		if (sgn)
			ghmin = -ghmin;
		ptr += w;
		ddata.resize(uvsize, ghmin);
	}
	for (unsigned int i(0), ng(m_param.get_ngroups()), grw(m_param.get_nbitsgroupref()); i < ng; ++i, ptr += grw)
		grpref[i] = extract(filedata, ptr, grw);
	ptr = (ptr + 7U) & ~7U;
	for (unsigned int i(0), ng(m_param.get_ngroups()), gww(m_param.get_nbitsgroupwidth()),
		     gwr(m_param.get_refgroupwidth()); i < ng; ++i, ptr += gww)
		grpwidth[i] = extract(filedata, ptr, gww) + gwr;
	ptr = (ptr + 7U) & ~7U;
	for (unsigned int i(0), ng(m_param.get_ngroups()), glw(m_param.get_nbitsgrouplength()),
		     glr(m_param.get_refgrouplength()), gli(m_param.get_incrgrouplength()); i < ng; ++i, ptr += glw)
		grplength[i] = extract(filedata, ptr, glw) * gli + glr;
	if (!grplength.empty())
		grplength.back() = m_param.get_lastgrouplength();
	ptr = (ptr + 7U) & ~7U;
	if (false) {
		unsigned int totlen(0), bmsz(0);
		for (std::vector<unsigned int>::const_iterator gi(grplength.begin()), ge(grplength.end()); gi != ge; ++gi)
			totlen += *gi;
		if (m_bitmap)
			for (unsigned int i = 0; i < uvsize; ++i)
				if ((bitmap[i >> 3] << (i & 7)) & 0x80)
					++bmsz;
		print_param(std::cerr << "GRIB2: LayerComplexPackingSpatialDiff::load: param ")
			<< " surface " << (unsigned int)get_surface1type() << ' ' << get_surface1value()
			<< " / " << (unsigned int)get_surface2type() << ' ' << get_surface2value()
			<< " group length sum " << totlen << " bitmap " << bmsz << " uv " << uvsize << std::endl;
	}
	static const int ddataprimiss(std::numeric_limits<int>::min());
	static const int ddatasecmiss(std::numeric_limits<int>::min() + 1);
	static const int ddatamiss(std::numeric_limits<int>::min() + 2);
	{
		const unsigned int primiss(m_param.get_primarymissingvalue_raw());
		const unsigned int secmiss(m_param.get_secondarymissingvalue_raw());
		unsigned int grpidx(0), grpcnt(1U), grpr(0U), grpw(1U), grpprimiss(~0U), grpsecmiss(~0U);
		if (grpidx < grplength.size()) {
			grpcnt = grplength[grpidx];
			grpr = grpref[grpidx];
			grpw = grpwidth[grpidx];
			if (grpw > 0 && m_param.is_primarymissingvalue())
				grpprimiss = (1U << grpw) - 1U;
			if (grpw > 1 && m_param.is_secondarymissingvalue())
				grpsecmiss = (1U << grpw) - 2U;
		}
		for (unsigned int i = 0; i < uvsize; ++i) {
			if (m_bitmap && !((bitmap[i >> 3] << (i & 7)) & 0x80)) {
				ddata[i] = ddatamiss;
				continue;
			}
			unsigned int v(extract(filedata, ptr, grpw));
			if (v == grpprimiss) {
				ddata[i] = ddataprimiss;
				goto next;
			}
			if (v == grpsecmiss) {
				ddata[i] = ddatasecmiss;
				goto next;
			}
			v += grpr;
			if (v) {
				if (v == primiss) {
					ddata[i] = ddataprimiss;
					goto next;
				}
				if (v == secmiss) {
					ddata[i] = ddatasecmiss;
					goto next;
				}
			}
			ddata[i] += v;
		  next:
			ptr += grpw;
			--grpcnt;
			if (grpcnt)
				continue;
			++grpidx;
			grpcnt = 1U;
			grpr = 0U;
			grpw = 1U;
			grpprimiss = grpsecmiss = ~0U;
			if (grpidx < grplength.size()) {
				grpcnt = grplength[grpidx];
				grpr = grpref[grpidx];
				grpw = grpwidth[grpidx];
				if (grpw > 0 && m_param.is_primarymissingvalue())
					grpprimiss = (1U << grpw) - 1U;
				if (grpw > 1 && m_param.is_secondarymissingvalue())
					grpsecmiss = (1U << grpw) - 2U;
			}
		}
	}
	switch (m_param.get_spatialdifforder()) {
	case 1:
		if (ddata[0] > ddatamiss)
			ddata[0] = diffinit[0];
		for (unsigned int i = 1; i < uvsize; ++i) {
			int v(ddata[i]);
			if (v <= ddatamiss)
				continue;
			v += diffinit[0];
			diffinit[0] = v;
			ddata[i] = v;
		}
		break;

	case 2:
		if (ddata[0] > ddatamiss)
			ddata[0] = diffinit[0];
		if (ddata[1] > ddatamiss)
			ddata[1] = diffinit[1];
		for (unsigned int i = 2; i < uvsize; ++i) {
			int v(ddata[i]);
			if (v <= ddatamiss)
				continue;
			v += 2 * diffinit[1] - diffinit[0];
			diffinit[0] = diffinit[1];
			diffinit[1] = v;
			ddata[i] = v;
		}
		break;

	default:
		break;
	}
	m_data.resize(uvsize, std::numeric_limits<float>::quiet_NaN());
	const double primissfloat(m_param.get_primarymissingvalue_float());
	const double secmissfloat(m_param.get_secondarymissingvalue_float());
	for (unsigned int i = 0; i < uvsize; ++i) {
		switch (ddata[i]) {
		case ddataprimiss:
			m_data[i] = primissfloat;
			break;

		case ddatasecmiss:
			m_data[i] = secmissfloat;
			break;

		case ddatamiss:
			break;

		default:
			m_data[i] = m_param.scale(ddata[i]);
			break;
		}
	}
}

bool GRIB2::LayerComplexPackingSpatialDiff::check_load(void)
{
	if (!m_filesize)
		return false;
	if (!m_param.is_gengroupsplit() || !m_param.get_ngroups() || m_param.get_spatialdifforder() > 2U)
		return false;
	return Glib::file_test(m_filename, Glib::FILE_TEST_EXISTS) && !Glib::file_test(m_filename, Glib::FILE_TEST_IS_DIR);
}

const float GRIB2::LayerResult::nan(std::numeric_limits<float>::quiet_NaN());

GRIB2::LayerResult::LayerResult(const boost::intrusive_ptr<Layer const>& layer, const Rect& bbox, unsigned int w, unsigned int h, gint64 efftime, gint64 minreftime, gint64 maxreftime, double sfc1value)
	: m_refcount(0), m_layer(layer), m_bbox(bbox), m_width(w), m_height(h), m_efftime(efftime), m_minreftime(minreftime), m_maxreftime(maxreftime), m_surface1value(sfc1value)
{
	unsigned int sz(m_width * m_height);
	m_data.resize(sz, nan);
}

GRIB2::LayerResult::~LayerResult()
{
}

void GRIB2::LayerResult::readonly(void) const
{
	LayerAllocator().readonly(this);
}

void GRIB2::LayerResult::readwrite(void) const
{
	LayerAllocator().readwrite(this);
}

const float& GRIB2::LayerResult::operator()(unsigned int x, unsigned int y) const
{
	if (x < m_width && y < m_height)
		return m_data[x + y * m_width];
	return nan;
}

float& GRIB2::LayerResult::operator()(unsigned int x, unsigned int y)
{
	if (x < m_width && y < m_height)
		return m_data[x + y * m_width];
	return const_cast<float&>(nan);
}

const float& GRIB2::LayerResult::operator[](unsigned int x) const
{
	if (x < m_data.size())
		return m_data[x];
	return nan;
}

float& GRIB2::LayerResult::operator[](unsigned int x)
{
	if (x < m_data.size())
		return m_data[x];
	return const_cast<float&>(nan);
}

float GRIB2::LayerResult::operator()(const Point& pt) const
{
	Point ptsz(get_pixelsize());
	uint32_t latd(pt.get_lat() - m_bbox.get_south());
	uint32_t lond(pt.get_lon() - m_bbox.get_west());
	unsigned int x(lond / ptsz.get_lon());
	unsigned int y(latd / ptsz.get_lat());
	lond -= x * ptsz.get_lon();
	latd -= y * ptsz.get_lat();
	float v[2][2];
	for (unsigned int xx = 0; xx < 2; ++xx) {
		uint32_t xxx(x + xx);
		if (xxx >= m_width) {
			for (unsigned int yy = 0; yy < 2; ++yy)
				v[xx][yy] = nan;
			continue;
		}
		for (unsigned int yy = 0; yy < 2; ++yy) {
			uint32_t yyy(y + yy);
			if (yyy >= m_height) {
				v[xx][yy] = nan;
				continue;
			}
			v[xx][yy] = operator()(xxx, yyy);
		}
	}
	double mx[2], my[2];
	mx[1] = lond / (float)ptsz.get_lon();
	mx[0] = 1 - mx[1];
	my[1] = latd / (float)ptsz.get_lat();
	my[0] = 1 - my[1];
	for (unsigned int xx = 0; xx < 2; ++xx) {
		if (mx[xx] < 0.5)
			continue;
		for (unsigned int yy = 0; yy < 2; ++yy)
			if (std::isnan(v[1 - xx][yy]))
				v[1 - xx][yy] = v[xx][yy];
	}
	for (unsigned int yy = 0; yy < 2; ++yy) {
		if (my[yy] < 0.5)
			continue;
		for (unsigned int xx = 0; xx < 2; ++xx)
			if (std::isnan(v[xx][1 - yy]))
				v[xx][1 - yy] = v[xx][yy];
	}
	float z(0);
	for (unsigned int xx = 0; xx < 2; ++xx)
		for (unsigned int yy = 0; yy < 2; ++yy) {
			if (std::isnan(v[xx][yy]))
				return nan;
			z += v[xx][yy] * mx[xx] * my[yy];
		}
	return z;
}

Point GRIB2::LayerResult::get_center(unsigned int x, unsigned int y) const
{
	Point ptsz(get_pixelsize());
	Point pt(ptsz.get_lon() / 2 + x * ptsz.get_lon(), ptsz.get_lat() / 2 + y * ptsz.get_lat());
	pt += m_bbox.get_southwest();
	return pt;
}

Point GRIB2::LayerResult::get_pixelsize(void) const
{
	if (!m_width || !m_height)
		return Point(0, 0);
	Point pt(m_bbox.get_northeast() - m_bbox.get_southwest());
	pt.set_lat(((uint32_t)pt.get_lat()) / m_height);
	pt.set_lon(((uint32_t)pt.get_lon()) / m_width);
	return pt;
}

GRIB2::Layer::model_t GRIB2::LayerResult::get_model(void) const
{
	if (m_layer)
		return m_layer->get_model();
	return Layer::model_t::unknown;
}

GRIB2::LayerInterpolateResult::LinInterp::LinInterp(float p0, float p1, float p2, float p3)
{
	m_p[0] = p0;
	m_p[1] = p1;
	m_p[2] = p2;
	m_p[3] = p3;
}

float GRIB2::LayerInterpolateResult::LinInterp::operator()(const InterpIndex& idx) const
{
	return m_p[0] + m_p[1] * idx.get_idxtime() + m_p[2] * idx.get_idxsfc1value() + m_p[3] * idx.get_idxtime() * idx.get_idxsfc1value();
}

float GRIB2::LayerInterpolateResult::LinInterp::operator[](unsigned int idx) const
{
	if (idx >= 4)
		return 0;
	return m_p[idx];
}

GRIB2::LayerInterpolateResult::LinInterp& GRIB2::LayerInterpolateResult::LinInterp::operator+=(const LinInterp& x)
{
	for (unsigned int i = 0; i < 4; ++i)
		m_p[i] += x.m_p[i];
	return *this;
}

GRIB2::LayerInterpolateResult::LinInterp& GRIB2::LayerInterpolateResult::LinInterp::operator*=(float m)
{
	for (unsigned int i = 0; i < 4; ++i)
		m_p[i] *= m;
	return *this;
}

GRIB2::LayerInterpolateResult::LinInterp GRIB2::LayerInterpolateResult::LinInterp::operator+(const LinInterp& x) const
{
	LinInterp z(*this);
	z += x;
	return z;
}

GRIB2::LayerInterpolateResult::LinInterp GRIB2::LayerInterpolateResult::LinInterp::operator*(float m) const
{
	LinInterp z(*this);
	z *= m;
	return z;
}

bool GRIB2::LayerInterpolateResult::LinInterp::is_nan(void) const
{
	for (unsigned int i = 0; i < 4; ++i)
		if (std::isnan(m_p[i]))
			return true;
	return false;
}

float GRIB2::LayerInterpolateResult::LinInterp::get_category(float idx) const
{
	idx = std::max(0.f, std::min(3.f, idx));
	unsigned int idx1(ldexpf(idx, 16));
	idx1 += 0x8000;
	idx1 >>= 16;
	return m_p[idx1];
}

const GRIB2::LayerInterpolateResult::LinInterp GRIB2::LayerInterpolateResult::nan(std::numeric_limits<float>::quiet_NaN(),
										  std::numeric_limits<float>::quiet_NaN(),
										  std::numeric_limits<float>::quiet_NaN(),
										  std::numeric_limits<float>::quiet_NaN());

GRIB2::LayerInterpolateResult::LayerInterpolateResult(const boost::intrusive_ptr<Layer const>& layer, const Rect& bbox, unsigned int w, unsigned int h,
						      gint64 minefftime, gint64 maxefftime, gint64 minreftime, gint64 maxreftime, double minsfc1value, double maxsfc1value,
						      const LinInterp& catinterp, bool categorical)
	: m_refcount(0), m_layer(layer), m_bbox(bbox), m_catinterp(catinterp), m_width(w), m_height(h), m_minefftime(minefftime), m_maxefftime(maxefftime),
	  m_minreftime(minreftime), m_maxreftime(maxreftime), m_efftimemul(0), m_minsurface1value(minsfc1value), m_maxsurface1value(maxsfc1value), m_surface1valuemul(0),
	  m_iscategorical(categorical)
{
	unsigned int sz(m_width * m_height);
	m_data.resize(sz, nan);
	if (m_maxefftime > m_minefftime)
		m_efftimemul = 1.0 / (double)(m_maxefftime - m_minefftime);
	{
		double x(m_maxsurface1value - m_minsurface1value);
		if (x > 0 && !std::isnan(x))
			m_surface1valuemul = 1.0 / x;
	}
}

GRIB2::LayerInterpolateResult::~LayerInterpolateResult()
{
}

void GRIB2::LayerInterpolateResult::readonly(void) const
{
	LayerAllocator().readonly(this);
}

void GRIB2::LayerInterpolateResult::readwrite(void) const
{
	LayerAllocator().readwrite(this);
}

GRIB2::LayerInterpolateResult::InterpIndex GRIB2::LayerInterpolateResult::get_index(gint64 efftime, double sfc1value) const
{
	efftime = std::min(std::max(efftime, m_minefftime), m_maxefftime) - m_minefftime;
	sfc1value = std::min(std::max(sfc1value, m_minsurface1value), m_maxsurface1value) - m_minsurface1value;
	if (std::isnan(sfc1value))
		sfc1value = 0;
	InterpIndex idx(efftime * m_efftimemul, sfc1value * m_surface1valuemul);
	if (false)
		std::cerr << "get_index: efftime " << efftime << " sfc1value " << sfc1value
			  << " idxtime " << idx.get_idxtime() << " idxsfc1value " << idx.get_idxsfc1value() << std::endl;
	return idx;
}

GRIB2::LayerInterpolateResult::InterpIndex GRIB2::LayerInterpolateResult::get_index_efftime(gint64 efftime) const
{
	efftime = std::min(std::max(efftime, m_minefftime), m_maxefftime) - m_minefftime;
	InterpIndex idx(efftime * m_efftimemul, 0);
	if (false)
		std::cerr << "get_index: efftime " << efftime
			  << " idxtime " << idx.get_idxtime() << " idxsfc1value " << idx.get_idxsfc1value() << std::endl;
	return idx;
}

GRIB2::LayerInterpolateResult::InterpIndex GRIB2::LayerInterpolateResult::get_index_surface1value(double sfc1value) const
{
	sfc1value = std::min(std::max(sfc1value, m_minsurface1value), m_maxsurface1value) - m_minsurface1value;
	if (std::isnan(sfc1value))
		sfc1value = 0;
	InterpIndex idx(0, sfc1value * m_surface1valuemul);
	if (false)
		std::cerr << "get_index: sfc1value " << sfc1value
			  << " idxtime " << idx.get_idxtime() << " idxsfc1value " << idx.get_idxsfc1value() << std::endl;
	return idx;
}

float GRIB2::LayerInterpolateResult::operator()(unsigned int x, unsigned int y, gint64 efftime, double sfc1value) const
{
	return operator()(x, y, get_index(efftime, sfc1value));
}

float GRIB2::LayerInterpolateResult::operator()(unsigned int x, unsigned int y, const InterpIndex& idx) const
{
	if (false) {
		std::cerr << "(): x " << x << " y " << y
			  << " idxtime " << idx.get_idxtime() << " idxsfc1value " << idx.get_idxsfc1value();
		if (x < m_width && y < m_height) {
			std::cerr << " poly";
			for (unsigned int i = 0; i < 4; ++i)
				std::cerr << ' ' << m_data[x + y * m_width][i];
		}
		std::cerr << std::endl;
	}
	if (x < m_width && y < m_height) {
		if (m_iscategorical)
			return m_data[x + y * m_width].get_category(m_catinterp(idx));
		return m_data[x + y * m_width](idx);
	}
       	return std::numeric_limits<float>::quiet_NaN();
}

const GRIB2::LayerInterpolateResult::LinInterp& GRIB2::LayerInterpolateResult::operator()(unsigned int x, unsigned int y) const
{
	if (x < m_width && y < m_height)
		return m_data[x + y * m_width];
	return nan;
}

GRIB2::LayerInterpolateResult::LinInterp& GRIB2::LayerInterpolateResult::operator()(unsigned int x, unsigned int y)
{
	if (x < m_width && y < m_height)
		return m_data[x + y * m_width];
	return const_cast<LinInterp&>(nan);
}

const GRIB2::LayerInterpolateResult::LinInterp& GRIB2::LayerInterpolateResult::operator[](unsigned int x) const
{
	if (x < m_data.size())
		return m_data[x];
	return nan;
}

GRIB2::LayerInterpolateResult::LinInterp& GRIB2::LayerInterpolateResult::operator[](unsigned int x)
{
	if (x < m_data.size())
		return m_data[x];
	return const_cast<LinInterp&>(nan);
}

GRIB2::LayerInterpolateResult::LinInterp GRIB2::LayerInterpolateResult::operator()(const Point& pt) const
{
	Point ptsz(get_pixelsize());
	uint32_t latd(pt.get_lat() - m_bbox.get_south());
	uint32_t lond(pt.get_lon() - m_bbox.get_west());
	unsigned int x(lond / ptsz.get_lon());
	unsigned int y(latd / ptsz.get_lat());
	lond -= x * ptsz.get_lon();
	latd -= y * ptsz.get_lat();
	LinInterp v[2][2];
	for (unsigned int xx = 0; xx < 2; ++xx) {
		uint32_t xxx(x + xx);
		if (xxx >= m_width) {
			for (unsigned int yy = 0; yy < 2; ++yy)
				v[xx][yy] = nan;
			continue;
		}
		for (unsigned int yy = 0; yy < 2; ++yy) {
			uint32_t yyy(y + yy);
			if (yyy >= m_height) {
				v[xx][yy] = nan;
				continue;
			}
			v[xx][yy] = operator()(xxx, yyy);
		}
	}
	double mx[2], my[2];
	mx[1] = lond / (float)ptsz.get_lon();
	mx[0] = 1 - mx[1];
	my[1] = latd / (float)ptsz.get_lat();
	my[0] = 1 - my[1];
	for (unsigned int xx = 0; xx < 2; ++xx) {
		if (mx[xx] < 0.5)
			continue;
		for (unsigned int yy = 0; yy < 2; ++yy)
			if (v[1 - xx][yy].is_nan())
				v[1 - xx][yy] = v[xx][yy];
	}
	for (unsigned int yy = 0; yy < 2; ++yy) {
		if (my[yy] < 0.5)
			continue;
		for (unsigned int xx = 0; xx < 2; ++xx)
			if (v[xx][1 - yy].is_nan())
				v[xx][1 - yy] = v[xx][yy];
	}
	if (m_iscategorical)
		return v[mx[1] >= 0.5][my[1] >= 0.5];
	LinInterp z(0);
	for (unsigned int xx = 0; xx < 2; ++xx)
		for (unsigned int yy = 0; yy < 2; ++yy) {
			if (v[xx][yy].is_nan())
				return nan;
			z += v[xx][yy] * (mx[xx] * my[yy]);
		}
	if (false && z.is_nan()) {
		std::cerr << "LayerInterpolateResult: " << z[0] << ' ' << z[1] << ' ' << z[2] << ' ' << z[3]
			  << " x " << x << " y " << y << " mx " << mx[1] << " my " << my[1] << std::endl;
	        for (unsigned int xx = 0; xx < 2; ++xx)
			for (unsigned int yy = 0; yy < 2; ++yy)
				std::cerr << "  " << xx << ',' << yy << ": " << v[xx][yy][0] << ' ' << v[xx][yy][1]
					  << ' ' << v[xx][yy][2] << ' ' << v[xx][yy][3] << std::endl;
	}
	return z;
}

float GRIB2::LayerInterpolateResult::operator()(const Point& pt, gint64 efftime, double sfc1value) const
{
	return operator()(pt, get_index(efftime, sfc1value));
}

float GRIB2::LayerInterpolateResult::operator()(const Point& pt, const InterpIndex& idx) const
{
	LinInterp z(this->operator()(pt));
	if (false) {
		std::cerr << "(): pt " << pt.get_lat_str2() << ' ' << pt.get_lon_str2()
			  << " idxtime " << idx.get_idxtime() << " idxsfc1value " << idx.get_idxsfc1value() << " poly";
		for (unsigned int i = 0; i < 4; ++i)
			std::cerr << ' ' << z[i];
		std::cerr << std::endl;
	}
	if (m_iscategorical) {
		if (false) {
			LinInterp y(m_catinterp(idx));
			std::cerr << "(): pt " << pt.get_lat_str2() << ' ' << pt.get_lon_str2()
				  << " idxtime " << idx.get_idxtime() << " idxsfc1value " << idx.get_idxsfc1value() << " polyc";
			for (unsigned int i = 0; i < 4; ++i)
				std::cerr << ' ' << y[i];
			std::cerr << " poly";
			for (unsigned int i = 0; i < 4; ++i)
				std::cerr << ' ' << z[i];
			std::cerr << std::endl;
		}
		return z.get_category(m_catinterp(idx));
	}
	return z(idx);
}

Point GRIB2::LayerInterpolateResult::get_center(unsigned int x, unsigned int y) const
{
	Point ptsz(get_pixelsize());
	Point pt(ptsz.get_lon() / 2 + x * ptsz.get_lon(), ptsz.get_lat() / 2 + y * ptsz.get_lat());
	pt += m_bbox.get_southwest();
	return pt;
}

Point GRIB2::LayerInterpolateResult::get_pixelsize(void) const
{
	if (!m_width || !m_height)
		return Point(0, 0);
	Point pt(m_bbox.get_northeast() - m_bbox.get_southwest());
	pt.set_lat(((uint32_t)pt.get_lat()) / m_height);
	pt.set_lon(((uint32_t)pt.get_lon()) / m_width);
	return pt;
}

boost::intrusive_ptr<GRIB2::LayerResult> GRIB2::LayerInterpolateResult::get_results(gint64 efftime, double sfc1value)
{
	efftime = std::min(std::max(efftime, m_minefftime), m_maxefftime);
	sfc1value = std::min(std::max(sfc1value, m_minsurface1value), m_maxsurface1value);
	InterpIndex idx(get_index(efftime, sfc1value));
	boost::intrusive_ptr<LayerResult> r(new (LayerAlloc) LayerResult(get_layer(), get_bbox(), get_width(), get_height(), efftime, get_minreftime(), get_maxreftime(), sfc1value));
	for (data_t::size_type i(0), sz(m_data.size()); i < sz; ++i)
		r->operator[](i) = m_data[i](idx);
	return r;
}

GRIB2::Layer::model_t GRIB2::LayerInterpolateResult::get_model(void) const
{
	if (m_layer)
		return m_layer->get_model();
	return Layer::model_t::unknown;
}

int GRIB2::LayerPtr::compare(const LayerPtr& x) const
{
	if (m_layer) {
		if (!x.m_layer)
			return -1;
	} else {
		if (x.m_layer)
			return 1;
		return 0;
	}
	{
		const Parameter *a(m_layer->get_parameter());
		const Parameter *b(x.m_layer->get_parameter());
		if (a) {
			if (!b)
				return -1;
			int c(a->compareid(*b));
			if (c)
				return c;
		} else {
			if (b)
				return 1;
		}
	}
	{
		Layer::statproc_t a(m_layer->get_statproc());
		Layer::statproc_t b(x.m_layer->get_statproc());
		if (a < b)
			return -1;
		if (b < a)
			return 1;
	}
	{
		uint8_t a(m_layer->get_surface1type());
		uint8_t b(x.m_layer->get_surface1type());
		if (a < b)
			return -1;
		if (b < a)
			return 1;
	}
	{
		double a(m_layer->get_surface1value());
		double b(x.m_layer->get_surface1value());
		if (a < b)
			return -1;
		if (b < a)
			return 1;
	}
	{
		uint8_t a(m_layer->get_surface2type());
		uint8_t b(x.m_layer->get_surface2type());
		if (a < b)
			return -1;
		if (b < a)
			return 1;
	}
	{
		double a(m_layer->get_surface2value());
		double b(x.m_layer->get_surface2value());
		if (a < b)
			return -1;
		if (b < a)
			return 1;
	}
	{
		gint64 a(m_layer->get_efftime());
		gint64 b(x.m_layer->get_efftime());
		if (a < b)
			return -1;
		if (b < a)
			return 1;
	}
	{
		gint64 a(m_layer->get_reftime());
		gint64 b(x.m_layer->get_reftime());
		if (a < b)
			return -1;
		if (b < a)
			return 1;
	}
	return 0;
}

int GRIB2::LayerPtrSurface::compare(const LayerPtrSurface& x) const
{
	if (m_layer) {
		if (!x.m_layer)
			return -1;
	} else {
		if (x.m_layer)
			return 1;
		return 0;
	}
	{
		const Parameter *a(m_layer->get_parameter());
		const Parameter *b(x.m_layer->get_parameter());
		if (a) {
			if (!b)
				return -1;
			int c(a->compareid(*b));
			if (c)
				return c;
		} else {
			if (b)
				return 1;
		}
	}
	{
		Layer::statproc_t a(m_layer->get_statproc());
		Layer::statproc_t b(x.m_layer->get_statproc());
		if (a < b)
			return -1;
		if (b < a)
			return 1;
	}
	{
		uint8_t a(m_layer->get_surface1type());
		uint8_t b(x.m_layer->get_surface1type());
		if (a < b)
			return -1;
		if (b < a)
			return 1;
	}
	{
		double a(m_layer->get_surface1value());
		double b(x.m_layer->get_surface1value());
		if (a < b)
			return -1;
		if (b < a)
			return 1;
	}
	return 0;
}

GRIB2::Parser::Parser(GRIB2& gr2)
	: m_grib2(gr2),
	  m_surface1value(std::numeric_limits<double>::quiet_NaN()),
	  m_surface2value(std::numeric_limits<double>::quiet_NaN()),
	  m_bitmapoffs(0), m_bitmapsize(0), m_nrdatapoints(0),
	  m_centerid(0xffff), m_subcenterid(0xffff), m_datarepresentation(0xffff), m_productionstatus(0xff),
	  m_datatype(0xff), m_productdiscipline(0xff), m_paramcategory(0xff), m_paramnumber(0xff),
	  m_genprocess(0xff), m_genprocesstype(0xff), m_surface1type(0xff), m_surface2type(0xff),
	  m_statproc(Layer::statproc_t::none), m_bitmap(false)
{
}

int GRIB2::Parser::parse_file(const std::string& filename)
{
       	Glib::RefPtr<Gio::File> file(Gio::File::create_for_path(filename));
	Glib::RefPtr<Gio::FileInputStream> instream(file->read());
	uint8_t buf[4096];
	unsigned int bufsz(0);
	int layers(0);
	int err(0);
	do {
		{
			gssize r(instream->read(&buf[bufsz], sizeof(buf) - bufsz));
			if (r > 0)
				bufsz += r;
			if (r < 0) {
				err = -1;
				break;
			}
		}
		if (!bufsz)
			break;
		// check section0 size
		if (bufsz < 16) {
			err = -1;
			break;
		}
		// check signature
		if (memcmp(&buf[0], &grib2_header[0], sizeof(grib2_header))) {
			err = -2;
			break;
		}
		// check GRIB version
		if (buf[7] != 0x02) {
			err = -3;
			break;
		}
		uint64_t file_len(buf[15] | (((uint32_t)buf[14]) << 8) |
				  (((uint32_t)buf[13]) << 16) | (((uint32_t)buf[12]) << 24) |
				  (((uint64_t)buf[11]) << 32) | (((uint64_t)buf[10]) << 40) |
				  (((uint64_t)buf[9]) << 48) | (((uint64_t)buf[8]) << 56));
		m_productdiscipline = getu8(buf, 6);
		bufsz -= 16;
		if (bufsz)
			memmove(&buf[0], &buf[16], bufsz);
		m_grid.reset();
		m_reftime = 0;
		m_efftime = 0;
		m_surface1value = std::numeric_limits<double>::quiet_NaN();
		m_surface2value = std::numeric_limits<double>::quiet_NaN();
		m_layerparam = LayerComplexPackingSpatialDiffParam();
		m_centerid = 0xffff;
		m_subcenterid = 0xffff;
		m_datarepresentation = 0xffff;
		m_bitmapoffs = 0;
		m_bitmapsize = 0;
		m_bitmap = false;
		m_nrdatapoints = 0;
		m_productionstatus = 0xff;
		m_datatype = 0xff;
		m_paramcategory = 0xff;
		m_paramnumber = 0xff;
		m_genprocess = 0xff;
		m_genprocesstype = 0xff;
		m_surface1type = 0xff;
		m_surface2type = 0xff;
		do {
			{
				gssize r(instream->read(&buf[bufsz], sizeof(buf) - bufsz));
				if (r > 0)
					bufsz += r;
				if (r < 0) {
					err = -4;
					break;
				}
			}
			if (bufsz < 4) {
				err = -4;
				break;
			}
			uint32_t len(getu32(buf, 0));
			if (len == grib2_terminator) {
				bufsz -= 4;
				if (bufsz)
					memmove(&buf[0], &buf[4], bufsz);
				break;
			}
			if (len < 5 || bufsz < 5) {
				err = -4;
				break;
			}
			switch (getu8(buf, 4)) {
			case 1:
			{
				err = section1(buf + 5, len - 5);
				if (err >= 0) {
					layers += err;
					err = 0;
				}
				break;
			}

			case 3:
		        {
				err = section3(buf + 5, len - 5);
				if (err >= 0) {
					layers += err;
					err = 0;
				}
				break;
			}

			case 4:
			{
				err = section4(buf + 5, len - 5);
				if (err >= 0) {
					layers += err;
					err = 0;
				}
				break;
			}

			case 5:
			{
				err = section5(buf + 5, len - 5);
				if (err >= 0) {
					layers += err;
					err = 0;
				}
				break;
			}

			case 6:
			{
				err = section6(buf + 5, len - 5, instream->tell() + 5 - bufsz);
				if (err >= 0) {
					layers += err;
					err = 0;
				}
				break;
			}

			case 7:
			{
				err = section7(buf + 5, len - 5, instream->tell() + 5 - bufsz, filename);
				if (err >= 0) {
					layers += err;
					err = 0;
				}
				break;
			}

			default:
				break;
			}
			if (true && err) {
				unsigned int l1(len);
				l1 = std::min(l1, bufsz);
				l1 = std::min(l1, 64U);
				std::ostringstream oss;
				oss << std::hex;
				for (unsigned int i = 0; i < l1; ++i) {
					if (!(i & 15))
						oss << std::endl << std::setfill('0') << std::setw(4) << i << ':';
					oss << ' ' << std::setfill('0') << std::setw(2) << (unsigned int)buf[i];
				}
				std::cerr << "GRIB2: " << filename << ':' << (instream->tell() + 5 - bufsz)
					  << ": error " << err << " in section " << (unsigned int)buf[4]
					  << "(section length " << len << ")" << oss.str() << std::endl;
			}
			if (len > bufsz) {
				len -= bufsz;
				bufsz = 0;
				instream->skip(len);
			} else {
				bufsz -= len;
				if (bufsz)
					memmove(&buf[0], &buf[len], bufsz);
			}
		} while (!err);
	} while (!err);
	if (layers > 0)
		return layers;
	return err;
}

uint8_t GRIB2::Parser::getu8(const uint8_t *sec, unsigned int offs)
{
	return sec[offs];
}

int8_t GRIB2::Parser::gets8(const uint8_t *sec, unsigned int offs)
{
	uint8_t x(getu8(sec, offs));
	if (!(x & 0x80))
		return x;
	return -(x & 0x7f);
}

uint16_t GRIB2::Parser::getu16(const uint8_t *sec, unsigned int offs)
{
	return sec[offs + 1] | (((uint16_t)sec[offs]) << 8);
}

int16_t GRIB2::Parser::gets16(const uint8_t *sec, unsigned int offs)
{
	uint16_t x(getu16(sec, offs));
	if (!(x & 0x8000))
		return x;
	return -(x & 0x7fff);
}

uint32_t GRIB2::Parser::getu32(const uint8_t *sec, unsigned int offs)
{
	return sec[offs + 3] | (((uint32_t)sec[offs + 2]) << 8) | (((uint32_t)sec[offs + 1]) << 16) | (((uint32_t)sec[offs]) << 24);
}

int32_t GRIB2::Parser::gets32(const uint8_t *sec, unsigned int offs)
{
	uint32_t x(getu32(sec, offs));
	if (!(x & 0x80000000))
		return x;
	return -(x & 0x7fffffff);
}

bool GRIB2::Parser::isvalid8(const uint8_t *sec, unsigned int offs)
{
	return sec[offs] != 0xff;
}

bool GRIB2::Parser::isvalid16(const uint8_t *sec, unsigned int offs)
{
	return sec[offs] != 0xff || sec[offs + 1] != 0xff;
}

bool GRIB2::Parser::isvalid32(const uint8_t *sec, unsigned int offs)
{
	return sec[offs] != 0xff || sec[offs + 1] != 0xff || sec[offs + 2] != 0xff || sec[offs + 3] != 0xff;
}

int GRIB2::Parser::section1(const uint8_t *buf, uint32_t len)
{
	m_reftime = 0;
	m_centerid = 0xffff;
	m_subcenterid = 0xffff;
	m_productionstatus = 0xff;
	m_datatype = 0xff;
	if (len < 16)
		return -101;
	m_centerid = getu16(buf, 0);
	m_subcenterid = getu16(buf, 2);
	m_productionstatus = getu8(buf, 14);
	m_datatype = getu8(buf, 15);
	m_reftime = Glib::DateTime::create_utc(getu16(buf, 7), getu8(buf, 9), getu8(buf, 10), getu8(buf, 11), getu8(buf, 12), getu8(buf, 13)).to_unix();
	return 0;
}

int GRIB2::Parser::section3(const uint8_t *buf, uint32_t len)
{
	m_grid.reset();
	if (len < 10)
		return -301;
	if (getu8(buf, 0)) {
		std::cerr << "Invalid Grid Source" << std::endl;
		return 0;
	}
	if (getu8(buf, 5) || getu8(buf, 6)) {
		std::cerr << "Quasi-Regular Grids not supported" << std::endl;
		return 0;
	}
	uint32_t ndata(getu32(buf, 1));
	uint16_t gridtempl(getu16(buf, 7));
	switch (gridtempl) {
	default:
		std::cerr << "Unsupported Grid " << gridtempl << " - only Lat/Lon Grids supported" << std::endl;
		return 0;

	case 0:
	{
		if (len < 67)
			return -301;
		uint32_t radiusearth(getu32(buf, 11));
		uint32_t majoraxis(getu32(buf, 16));
		uint32_t minoraxis(getu32(buf, 21));
		uint32_t Ni(getu32(buf, 25));
		uint32_t Nj(getu32(buf, 29));
		uint32_t basicangle(getu32(buf, 33));
		uint32_t subdivbasicangle(getu32(buf, 37));
		int32_t lat1(gets32(buf, 41));
		uint32_t lon1(getu32(buf, 45));
		int32_t lat2(gets32(buf, 50));
		uint32_t lon2(getu32(buf, 54));
		uint32_t Di(getu32(buf, 58));
		uint32_t Dj(getu32(buf, 62));
		uint8_t rescompflg(getu8(buf, 49));
		uint8_t scanmode(getu8(buf, 66));
		// we do not (yet) care about the exact earth definition
		// FIXME: subdivbasicangle should not be zero, old GFS data
		if (basicangle || (subdivbasicangle != 0 && subdivbasicangle != (uint32_t)~0UL)) {
			std::cerr << "Grid: basic angle/subdivisions not supported" << std::endl;
			return 0;
		}
		if (Ni * Nj != ndata) {
			std::cerr << "Grid: Ni * Nj does not match number of data points: " << Ni << '*' << Nj << "!=" << ndata << std::endl;
			return 0;
		}
		if (Ni < 2 || Nj < 2) {
			std::cerr << "Grid: Ni / Nj must be at least 2" << std::endl;
			return 0;
		}
		if (scanmode & 16) {
			std::cerr << "Grid: opposite row scanning not supported" << std::endl;
			return 0;
		}
		if (rescompflg & 8) {
			std::cerr << "Grid: u/v vector components not aligned to easterly/northerly direction" << std::endl;
			return 0;
		}
		Point origin;
		Point ptsz;
		int scu((scanmode & 32) ? Nj : 1);
		int scv((scanmode & 32) ? 1 : Ni);
		int offs(0);
		origin.set_lat_deg_dbl(((scanmode & 64) ? lat1 : lat2) * 1e-6);
		origin.set_lon_deg_dbl(((scanmode & 128) ? lon2 : lon1) * 1e-6);
		if (!(rescompflg & 32)) {
			int32_t ld(lon2 - lon1);
			if (ld < 0)
				ld += 360000000;
			if (ld >= 360000000)
				ld -= 360000000;
			Di = (ld + (Ni - 1)/2) / (Ni - 1);
		}
		if (!(rescompflg & 16))
			Dj = ((scanmode & 64) ? lat2 - lat1 : lat1 - lat2) / (Nj - 1);
		ptsz.set_lat(static_cast<Point::coord_t>(ceil(Dj * (1e-6 * Point::from_deg_dbl))));
		ptsz.set_lon(static_cast<Point::coord_t>(ceil(Di * (1e-6 * Point::from_deg_dbl))));
		if (scanmode & 128) {
			offs += scu * (Ni - 1);
			scu = -scu;
		}
		if (!(scanmode & 64)) {
			offs += scv * (Nj - 1);
			scv = -scv;
		}
		if (false)
			std::cerr << "GridLatLon: origin " << origin.get_lat_str2() << ' ' << origin.get_lon_str2()
				  << " ptsz " << ptsz.get_lat_str2() << ' ' << ptsz.get_lon_str2() << " N " << Ni << ',' << Nj
				  << " D " << Di << ',' << Dj << " sc " << scu << ',' << scv << " offs " << offs
				  << " lat " << lat1 << ',' << lat2 << " lon " << lon1 << ',' << lon2
				  << " scanmode " << (unsigned int)scanmode << " rescomp " << (unsigned int)rescompflg << std::endl;
		m_grid = boost::intrusive_ptr<GridLatLon>(new GridLatLon(origin, ptsz, Ni, Nj, scu, scv, offs, !(rescompflg & 8)));
		if (m_lastgrid && *(m_lastgrid.operator->()) == *(m_grid.operator->()))
			m_grid = m_lastgrid;
		m_lastgrid = m_grid;
		break;
	}
	}
	return 0;
}

int GRIB2::Parser::section4(const uint8_t *buf, uint32_t len)
{
	m_efftime = 0;
	m_paramcategory = 0xff;
	m_paramnumber = 0xff;
	m_genprocess = 0xff;
	m_genprocesstype = 0xff;
	m_surface1type = 0xff;
	m_surface2type = 0xff;
	m_surface1value = std::numeric_limits<double>::quiet_NaN();
	m_surface2value = std::numeric_limits<double>::quiet_NaN();
	m_statproc = Layer::statproc_t::none;
	if (len < 4)
		return -401;
	uint16_t nrcoordval(getu16(buf, 0));
	if (nrcoordval) {
		std::cerr << "Product Definition: Coordinate Values not supported" << std::endl;
		return 0;
	}
	uint16_t proddeftempl(getu16(buf, 2));
	switch (proddeftempl) {
	default:
		std::cerr << "Unsupported Product definition template " << proddeftempl << std::endl;
		return 0;

	case 0:
	case 8:
	case 15:
	{
		if (len < 29)
			return -401;
		uint16_t hrcutoff(getu16(buf, 9));
		int32_t fcsttime(gets32(buf, 13));
		int scsfc1(gets8(buf, 18));
		int32_t fixsfc1(gets32(buf, 19));
		int scsfc2(gets8(buf, 24));
		int32_t fixsfc2(gets32(buf, 25));
		switch (getu8(buf, 12)) {
		case 0: // Minute
			m_efftime = m_reftime + 60 * fcsttime;
			break;

		case 1: // Hour
			m_efftime = m_reftime + (60 * 60) * fcsttime;
			break;

		case 2: // Day
			m_efftime = m_reftime + (24 * 60 * 60) * fcsttime;
			break;

		case 3: // Month
			m_efftime = Glib::DateTime::create_now_utc(m_reftime).add_months(fcsttime).to_unix();
			break;

		case 4: // Year
			m_efftime = Glib::DateTime::create_now_utc(m_reftime).add_years(fcsttime).to_unix();
			break;

		case 5: // Decade
			m_efftime = Glib::DateTime::create_now_utc(m_reftime).add_years(10 * fcsttime).to_unix();
			break;

		case 6: // Normal (30 Years)
			m_efftime = Glib::DateTime::create_now_utc(m_reftime).add_years(30 * fcsttime).to_unix();
			break;

		case 7: // Century
			m_efftime = Glib::DateTime::create_now_utc(m_reftime).add_years(100 * fcsttime).to_unix();
			break;

		case 10: // 3 Hours
			m_efftime = m_reftime + (3 * 60 * 60) * fcsttime;
			break;

		case 11: // 6 Hours
			m_efftime = m_reftime + (6 * 60 * 60) * fcsttime;
			break;

		case 12: // 12 Hours
			m_efftime = m_reftime + (12 * 60 * 60) * fcsttime;
			break;

		case 13: // Second
			m_efftime = m_reftime + fcsttime;
			break;

		default:
			std::cerr << "Unsupported Forecast Time Unit Indicator " << (unsigned int)getu8(buf, 12) << std::endl;
			return 0;
		}
		m_paramcategory = getu8(buf, 4);
		m_paramnumber = getu8(buf, 5);
		m_genprocess = getu8(buf, 6);
		m_genprocesstype = getu8(buf, 8);
		m_surface1type = getu8(buf, 17);
		m_surface2type = getu8(buf, 23);
		m_surface1value = fixsfc1 * exp((-M_LN10) * scsfc1);
		m_surface2value = fixsfc2 * exp((-M_LN10) * scsfc2);
		if (m_surface1type == surface_missing)
			m_surface1value = 0;
		if (m_surface2type == surface_missing)
			m_surface2value = 0;
		if (!proddeftempl)
			break;
		if (proddeftempl == 15) {
                        if (len < 32)
                                return -2;
			m_statproc = static_cast<Layer::statproc_t>(gets8(buf, 29) + 1);
			break;
		}
		if (len < 41)
			return -401;
		Glib::DateTime intvlend(Glib::DateTime::create_utc(getu16(buf, 29), getu8(buf, 31), getu8(buf, 32), getu8(buf, 33), getu8(buf, 34), getu8(buf, 35)));
		uint32_t valmissing(getu32(buf, 37));
		unsigned int tmlen(getu8(buf, 36));
		if (len < 41U + 12U * tmlen)
			return -401;
		for (unsigned int j = 0; j < tmlen; ++j) {
			uint32_t timerange(getu32(buf, 44 + j*12));
			uint32_t timeincr(getu32(buf, 49 + j*12));
		}
		break;
	}
	}
	return 0;
}

int GRIB2::Parser::section5(const uint8_t *buf, uint32_t len)
{
	m_nrdatapoints = 0;
	m_layerparam = LayerComplexPackingSpatialDiffParam();
	m_datarepresentation = 0xffff;
	if (len < 6)
		return -501;
        m_nrdatapoints = getu32(buf, 0);
        m_datarepresentation = getu16(buf, 4);
	switch (m_datarepresentation) {
	case 0:
	{
		if (len < 16)
			return -501;
		union {
			float f;
			uint32_t w;
			uint8_t b[4];
		} r;
		r.w = getu32(buf, 6);
		int16_t e(gets16(buf, 10));
		int16_t d(gets16(buf,12));
		double sc(exp((-M_LN10) * d));
		m_layerparam.set_datascale(std::ldexp(sc, e));
		m_layerparam.set_dataoffset(r.f * sc);
		m_layerparam.set_nbitsgroupref(getu8(buf, 14));
		m_layerparam.set_fieldvaluetype(getu8(buf, 15));
		break;
	}

	case 2:
	{
		if (len < 42)
			return -501;
		union {
			float f;
			uint32_t w;
			uint8_t b[4];
		} r;
		r.w = getu32(buf, 6);
		int16_t e(gets16(buf, 10));
		int16_t d(gets16(buf,12));
		double sc(exp((-M_LN10) * d));
		m_layerparam.set_datascale(std::ldexp(sc, e));
		m_layerparam.set_dataoffset(r.f * sc);
		m_layerparam.set_nbitsgroupref(getu8(buf, 14));
		m_layerparam.set_fieldvaluetype(getu8(buf, 15));
		m_layerparam.set_groupsplitmethod(getu8(buf, 16));
		m_layerparam.set_missingvaluemgmt(getu8(buf, 17));
	        m_layerparam.set_primarymissingvalue(getu32(buf, 18));
		m_layerparam.set_secondarymissingvalue(getu32(buf, 22));
		m_layerparam.set_ngroups(getu32(buf, 26));
		m_layerparam.set_refgroupwidth(getu8(buf, 30));
		m_layerparam.set_nbitsgroupwidth(getu8(buf, 31));
		m_layerparam.set_refgrouplength(getu32(buf, 32));
		m_layerparam.set_incrgrouplength(getu8(buf, 36));
		m_layerparam.set_lastgrouplength(getu32(buf, 37));
		m_layerparam.set_nbitsgrouplength(getu8(buf, 41));
		break;
	}

	case 3:
	{
		if (len < 44)
			return -501;
		union {
			float f;
			uint32_t w;
			uint8_t b[4];
		} r;
		r.w = getu32(buf, 6);
		int16_t e(gets16(buf, 10));
		int16_t d(gets16(buf,12));
		double sc(exp((-M_LN10) * d));
		m_layerparam.set_datascale(std::ldexp(sc, e));
		m_layerparam.set_dataoffset(r.f * sc);
		m_layerparam.set_nbitsgroupref(getu8(buf, 14));
		m_layerparam.set_fieldvaluetype(getu8(buf, 15));
		m_layerparam.set_groupsplitmethod(getu8(buf, 16));
		m_layerparam.set_missingvaluemgmt(getu8(buf, 17));
	        m_layerparam.set_primarymissingvalue(getu32(buf, 18));
		m_layerparam.set_secondarymissingvalue(getu32(buf, 22));
		m_layerparam.set_ngroups(getu32(buf, 26));
		m_layerparam.set_refgroupwidth(getu8(buf, 30));
		m_layerparam.set_nbitsgroupwidth(getu8(buf, 31));
		m_layerparam.set_refgrouplength(getu32(buf, 32));
		m_layerparam.set_incrgrouplength(getu8(buf, 36));
		m_layerparam.set_lastgrouplength(getu32(buf, 37));
		m_layerparam.set_nbitsgrouplength(getu8(buf, 41));
		m_layerparam.set_spatialdifforder(getu8(buf, 42));
		m_layerparam.set_extradescroctets(getu8(buf, 43));
		break;
	}

	case 40:
	{
		if (len < 18)
			return -501;
		union {
			float f;
			uint32_t w;
			uint8_t b[4];
		} r;
		r.w = getu32(buf, 6);
		int16_t e(gets16(buf, 10));
		int16_t d(gets16(buf,12));
		double sc(exp((-M_LN10) * d));
		m_layerparam.set_datascale(std::ldexp(sc, e));
		m_layerparam.set_dataoffset(r.f * sc);
		break;
	}

	default:
	{
		const Parameter *param(find_parameter(m_productdiscipline, m_paramcategory, m_paramnumber));
		const char *sfc(find_surfacetype_str(m_surface1type, "?"));
		std::cerr << "Unsupported Data Representation " << m_datarepresentation
			  << " (param " << param->get_str_nonnull() << ' ' << param->get_unit_nonnull()
			  << ' ' << param->get_abbrev_nonnull() << " surface " << sfc
			  << " reftime " << Glib::TimeVal(m_reftime, 0).as_iso8601()
			  << " efftime " << Glib::TimeVal(m_efftime, 0).as_iso8601() << ')' << std::endl;
		return 0;
	}
	}
	return 0;
}

int GRIB2::Parser::section6(const uint8_t *buf, uint32_t len, goffset offs)
{
	m_bitmap = false;
	if (len < 1)
		return -601;
	switch (getu8(buf, 0)) {
	case 255:
		break;

	case 254:
		m_bitmap = true;
		break;

	case 0:
		m_bitmapoffs = offs + 1;
		m_bitmapsize = len - 1;
		m_bitmap = true;
		break;

	default:
		std::cerr << "Bitmap Section: unsupported bitmap type " << (unsigned int)getu8(buf, 0) << std::endl;
		return 0;
	}
	if (!m_bitmap)
		return 0;

	return 0;
}

int GRIB2::Parser::section7(const uint8_t *buf, uint32_t len, goffset offs, const std::string& filename)
{
	static constexpr bool trace_layeradd(false);
	if (false)
		std::cout << "section7: grid " << (!m_grid ? "no" : "yes")
			  << " reftime " << (m_reftime > 0 ? "no" : "yes") << ' '
			  << Glib::DateTime::create_now_utc(m_reftime).format("%F %H:%M:%S")
			  << " efftime " << (m_efftime > 0 ? "no" : "yes") << ' '
			  << Glib::DateTime::create_now_utc(m_efftime).format("%F %H:%M:%S")
			  << " discipline " << (unsigned int)m_productdiscipline << " category " << (unsigned int)m_paramcategory
			  << " parameter " << (unsigned int)m_paramnumber << std::endl;
	if (!m_grid || !m_reftime || !m_efftime ||
	    m_productdiscipline == 0xff || m_paramcategory == 0xff || m_paramnumber == 0xff)
		return 0;
	const Parameter *param(find_parameter(m_productdiscipline, m_paramcategory, m_paramnumber));
	if (!param) {
		std::cerr << "Layer Parameter " << (unsigned int)m_productdiscipline << ':'
			  << (unsigned int)m_paramcategory << ':'
			  << (unsigned int)m_paramnumber << " not found" << std::endl;
		return 0;
	}
	if (m_grid) {
		uint32_t griddp(m_grid->get_usize() * m_grid->get_vsize());
		if (m_bitmap) {
			if (m_bitmapsize * 8 < griddp) {
				std::cerr << "Data Representation: not enough bitmap bits for grid: " << (m_bitmapsize * 8) << '<' << griddp << std::endl;
				return 0;
			}
		} else {
			if (m_nrdatapoints < griddp) {
				std::cerr << "Data Representation: not enough datapoints for grid: " << m_nrdatapoints << '<' << griddp << std::endl;
				return 0;
			}
		}
	}
	boost::intrusive_ptr<Layer> layer;
	switch (m_datarepresentation) {
	case 0:
		if (!len || filename.empty()) {
			if (trace_layeradd)
				std::cerr << "Invalid layer " << param->get_category()->get_discipline()->get_str("?")
					  << ':' << param->get_category()->get_str("?") << ':' << param->get_str("?")
					  << ' ' << find_surfacetype_str(m_surface1type, "?") << ',' << m_surface1value
					  << ' ' << find_surfacetype_str(m_surface2type, "?") << ',' << m_surface2value
					  << " filename " << filename << " pos " << offs << " len " << len << std::endl;
			break;
		}
		if (trace_layeradd)
			std::cout << "Adding layer " << param->get_category()->get_discipline()->get_str("?")
				  << ':' << param->get_category()->get_str("?") << ':' << param->get_str("?")
				  << ' ' << find_surfacetype_str(m_surface1type, "?") << ',' << m_surface1value
				  << ' ' << find_surfacetype_str(m_surface2type, "?") << ',' << m_surface2value << std::endl;
		layer = boost::intrusive_ptr<LayerSimplePacking>(new (LayerAlloc) LayerSimplePacking(param, m_grid, m_reftime, m_efftime, m_centerid, m_subcenterid,
												     m_productionstatus, m_datatype, m_genprocess, m_genprocesstype,
												     m_surface1type, m_surface1value, m_surface2type, m_surface2value,
												     m_statproc, m_layerparam, m_bitmapoffs, m_bitmap, offs, len, filename));
		break;

	case 2:
		if (!len || filename.empty()) {
			if (trace_layeradd)
				std::cerr << "Invalid layer " << param->get_category()->get_discipline()->get_str("?")
					  << ':' << param->get_category()->get_str("?") << ':' << param->get_str("?")
					  << ' ' << find_surfacetype_str(m_surface1type, "?") << ',' << m_surface1value
					  << ' ' << find_surfacetype_str(m_surface2type, "?") << ',' << m_surface2value
					  << " filename " << filename << " pos " << offs << " len " << len << std::endl;
			break;
		}
		if (trace_layeradd)
			std::cout << "Adding layer " << param->get_category()->get_discipline()->get_str("?")
				  << ':' << param->get_category()->get_str("?") << ':' << param->get_str("?")
				  << ' ' << find_surfacetype_str(m_surface1type, "?") << ',' << m_surface1value
				  << ' ' << find_surfacetype_str(m_surface2type, "?") << ',' << m_surface2value << std::endl;
		layer = boost::intrusive_ptr<LayerComplexPacking>(new (LayerAlloc) LayerComplexPacking(param, m_grid, m_reftime, m_efftime, m_centerid, m_subcenterid,
												       m_productionstatus, m_datatype, m_genprocess, m_genprocesstype,
												       m_surface1type, m_surface1value, m_surface2type, m_surface2value,
												       m_statproc, m_layerparam, m_bitmapoffs, m_bitmap, offs, len, filename));
		break;

	case 3:
		if (!len || filename.empty()) {
			if (trace_layeradd)
				std::cerr << "Invalid layer " << param->get_category()->get_discipline()->get_str("?")
					  << ':' << param->get_category()->get_str("?") << ':' << param->get_str("?")
					  << ' ' << find_surfacetype_str(m_surface1type, "?") << ',' << m_surface1value
					  << ' ' << find_surfacetype_str(m_surface2type, "?") << ',' << m_surface2value
					  << " filename " << filename << " pos " << offs << " len " << len << std::endl;
			break;
		}
		if (trace_layeradd)
			std::cout << "Adding layer " << param->get_category()->get_discipline()->get_str("?")
				  << ':' << param->get_category()->get_str("?") << ':' << param->get_str("?")
				  << ' ' << find_surfacetype_str(m_surface1type, "?") << ',' << m_surface1value
				  << ' ' << find_surfacetype_str(m_surface2type, "?") << ',' << m_surface2value << std::endl;
		layer = boost::intrusive_ptr<LayerComplexPackingSpatialDiff>(new (LayerAlloc) LayerComplexPackingSpatialDiff(param, m_grid, m_reftime, m_efftime, m_centerid, m_subcenterid,
															     m_productionstatus, m_datatype, m_genprocess, m_genprocesstype,
															     m_surface1type, m_surface1value, m_surface2type, m_surface2value,
															     m_statproc, m_layerparam, m_bitmapoffs, m_bitmap, offs, len, filename));
		break;

	case 40:
		if (!len || filename.empty()) {
			if (trace_layeradd)
				std::cerr << "Invalid layer " << param->get_category()->get_discipline()->get_str("?")
					  << ':' << param->get_category()->get_str("?") << ':' << param->get_str("?")
					  << ' ' << find_surfacetype_str(m_surface1type, "?") << ',' << m_surface1value
					  << ' ' << find_surfacetype_str(m_surface2type, "?") << ',' << m_surface2value
					  << " filename " << filename << " pos " << offs << " len " << len << std::endl;
			break;
		}
		if (trace_layeradd)
			std::cout << "Adding layer " << param->get_category()->get_discipline()->get_str("?")
				  << ':' << param->get_category()->get_str("?") << ':' << param->get_str("?")
				  << ' ' << find_surfacetype_str(m_surface1type, "?") << ',' << m_surface1value
				  << ' ' << find_surfacetype_str(m_surface2type, "?") << ',' << m_surface2value << std::endl;
		layer = boost::intrusive_ptr<LayerJ2K>(new (LayerAlloc) LayerJ2K(param, m_grid, m_reftime, m_efftime, m_centerid, m_subcenterid,
										 m_productionstatus, m_datatype, m_genprocess, m_genprocesstype,
										 m_surface1type, m_surface1value, m_surface2type, m_surface2value,
										 m_statproc, m_layerparam, m_bitmapoffs, m_bitmap, offs, len, filename,
										 m_grib2.get_cachedir()));
		break;

	default:
		break;
	}
	if (!layer)
		return 0;
	m_grib2.add_layer(layer);
	return 1;
}

int GRIB2::Parser::parse_directory(const std::string& dir)
{
	int err(0), lay(0);
	Glib::Dir d(dir);
	for (;;) {
		std::string f(d.read_name());
		if (f.empty())
			break;
		int r(parse(Glib::build_filename(dir, f)));
		if (r < 0)
			err = r;
		else
			lay += r;
	}
	if (lay)
		return lay;
	return err;
}

int GRIB2::Parser::parse(const std::string& p)
{
	if (Glib::file_test(p, Glib::FILE_TEST_IS_DIR))
		return parse_directory(p);
	if (Glib::file_test(p, Glib::FILE_TEST_IS_REGULAR))
		return parse_file(p);
	return -1;
}

int GRIB2::ParamDiscipline::compareid(const ParamDiscipline& x) const
{
	if (get_id() < x.get_id())
		return -1;
	if (x.get_id() < get_id())
		return 1;
	return 0;
}

int GRIB2::ParamCategory::compareid(const ParamCategory& x) const
{
	if (get_discipline_id() < x.get_discipline_id())
		return -1;
	if (x.get_discipline_id() < get_discipline_id())
		return 1;
	if (get_id() < x.get_id())
		return -1;
	if (x.get_id() < get_id())
		return 1;
	return 0;
}

int GRIB2::Parameter::compareid(const Parameter& x) const
{
	if (get_discipline_id() < x.get_discipline_id())
		return -1;
	if (x.get_discipline_id() < get_discipline_id())
		return 1;
	if (get_category_id() < x.get_category_id())
		return -1;
	if (x.get_category_id() < get_category_id())
		return 1;
	if (get_id() < x.get_id())
		return -1;
	if (x.get_id() < get_id())
		return 1;
	return 0;
}

int GRIB2::ID8String::compareid(const ID8String& x) const
{
	if (get_id() < x.get_id())
		return -1;
	if (x.get_id() < get_id())
		return 1;
	return 0;
}

int GRIB2::ID16String::compareid(const ID16String& x) const
{
	if (get_id() < x.get_id())
		return -1;
	if (x.get_id() < get_id())
		return 1;
	return 0;
}

int GRIB2::CenterTable::compareid(const CenterTable& x) const
{
	if (get_id() < x.get_id())
		return -1;
	if (x.get_id() < get_id())
		return 1;
	return 0;
}

int GRIB2::SurfaceTable::compareid(const SurfaceTable& x) const
{
	if (get_id() < x.get_id())
		return -1;
	if (x.get_id() < get_id())
		return 1;
	return 0;
}

GRIB2::WeatherProfilePoint::SoundingSurfaceTemp::SoundingSurfaceTemp(float press, float temp, float dewpt)
	: m_press(press), m_temp(temp), m_dewpt(dewpt)
{
}

int GRIB2::WeatherProfilePoint::SoundingSurfaceTemp::compare(const SoundingSurfaceTemp& x) const
{
	if (std::isnan(get_press()))
		return std::isnan(x.get_press()) ? 0 : -1;
	if (std::isnan(x.get_press()))
		return 1;
	if (get_press() < x.get_press())
		return 1;
	if (x.get_press() < get_press())
		return -1;
	return 0;
}

GRIB2::WeatherProfilePoint::SoundingSurface::SoundingSurface(float press, float temp, float dewpt, float winddir, float windspd)
	: SoundingSurfaceTemp(press, temp, dewpt), m_winddir(winddir), m_windspeed(windspd)
{
}

constexpr float GRIB2::WeatherProfilePoint::Surface::magnus_b;
constexpr float GRIB2::WeatherProfilePoint::Surface::magnus_c;
constexpr double GRIB2::WeatherProfilePoint::Surface::turbulence_thresholds[3][4];

GRIB2::WeatherProfilePoint::Surface::Surface(float uwind, float vwind, float temp, float rh, float hwsh, float vwsh, float ccover, float icing, float edp)
	: m_uwind(uwind), m_vwind(vwind), m_temp(temp), m_rh(rh), m_hwsh(hwsh), m_vwsh(vwsh), m_cloudcover(ccover), m_icing(icing), m_edp(edp)
{
}

float GRIB2::WeatherProfilePoint::Surface::get_turbulenceindex(void) const
{
	float x(get_vwsh() * 1e3);
	x *= x;
	x += 42 + 5e5 * get_hwsh();
	return x * 0.25;
}

float GRIB2::WeatherProfilePoint::Surface::get_dewpoint(void) const
{
	if (std::isnan(get_rh()) || std::isnan(get_temp()))
		return std::numeric_limits<float>::quiet_NaN();
	if (get_rh() <= 0)
		return 0;
	float gamma(magnus_gamma());
	float dewpt(magnus_c * gamma / (magnus_b - gamma) + IcaoAtmosphere<float>::degc_to_kelvin);
	if (false)
		std::cerr << "get_dewpoint: rh " << get_rh() << " temp " << get_temp()
			  << " gamma " << gamma << " dewpt " << dewpt << std::endl;
	return dewpt;
}

float GRIB2::WeatherProfilePoint::Surface::magnus_gamma(float temp, float rh)
{
	temp -= IcaoAtmosphere<float>::degc_to_kelvin;
	return logf(rh * 0.01) + magnus_b * temp / (magnus_c + temp);
}

GRIB2::WeatherProfilePoint::Surface::operator SoundingSurface(void) const
{
	SoundingSurface s;
	{
		float wu(get_uwind()), wv(get_vwind());
		wu *= (-1e-3f * Point::km_to_nmi * 3600);
		wv *= (-1e-3f * Point::km_to_nmi * 3600);
		s.set_winddir(atan2f(wu, wv) * (180.0 / M_PI));
		s.set_windspeed(sqrtf(wu * wu + wv * wv));
	}
	s.set_temp(get_temp());
	s.set_dewpt(get_dewpoint());
	return s;
}

const double *GRIB2::WeatherProfilePoint::Surface::get_turbulence_thresholds(double mtom)
{
	if (std::isnan(mtom) || mtom < 7030)
		return turbulence_thresholds[0];
	if (mtom < 136077)
		return turbulence_thresholds[1];
	return turbulence_thresholds[2];
}	

GRIB2::WeatherProfilePoint::Stability::Stability(const soundingsurfaces_t& surf)
	: m_lcl_press(std::numeric_limits<float>::quiet_NaN()),
	  m_lcl_temp(std::numeric_limits<float>::quiet_NaN()),
	  m_lfc_press(std::numeric_limits<float>::quiet_NaN()),
	  m_lfc_temp(std::numeric_limits<float>::quiet_NaN()),
	  m_el_press(std::numeric_limits<float>::quiet_NaN()),
	  m_el_temp(std::numeric_limits<float>::quiet_NaN()),
	  m_liftedindex(std::numeric_limits<float>::quiet_NaN()),
	  m_cape(std::numeric_limits<float>::quiet_NaN()),
	  m_cin(std::numeric_limits<float>::quiet_NaN())
{
	if (surf.empty())
		return;
	for (soundingsurfaces_t::const_iterator si(surf.begin()), se(surf.end()); si != se; ++si) {
		if (std::isnan(si->get_press()) || std::isnan(si->get_temp()) || si->get_press() <= 0)
			continue;
		m_tempcurve.insert(SoundingSurfaceTemp(si->get_press(), si->get_temp()));
	}
	const SoundingSurfaceTemp& gnd(*surf.begin());
	calculate(gnd.get_press(), gnd.get_temp(), gnd.get_dewpt());
}

GRIB2::WeatherProfilePoint::Stability::Stability(const WeatherProfilePoint& wxpp)
	: m_lcl_press(std::numeric_limits<float>::quiet_NaN()),
	  m_lcl_temp(std::numeric_limits<float>::quiet_NaN()),
	  m_lfc_press(std::numeric_limits<float>::quiet_NaN()),
	  m_lfc_temp(std::numeric_limits<float>::quiet_NaN()),
	  m_el_press(std::numeric_limits<float>::quiet_NaN()),
	  m_el_temp(std::numeric_limits<float>::quiet_NaN()),
	  m_liftedindex(std::numeric_limits<float>::quiet_NaN()),
	  m_cape(std::numeric_limits<float>::quiet_NaN()),
	  m_cin(std::numeric_limits<float>::quiet_NaN())
{
	static constexpr unsigned int nrsfc(sizeof(WeatherProfilePoint::isobaric_levels)/sizeof(WeatherProfilePoint::isobaric_levels[0]));
	for (unsigned int i = 0; i < nrsfc; ++i) {
		int16_t press(isobaric_levels[i]);
		const Surface& surf(wxpp[i]);
		if (std::isnan(surf.get_temp()) || press <= 0)
			continue;
		m_tempcurve.insert(SoundingSurfaceTemp(press, surf.get_temp()));
	}
	const Surface& gnd(wxpp[0]);
	calculate(wxpp.get_qff(), gnd.get_temp(), gnd.get_dewpoint());
}

void GRIB2::WeatherProfilePoint::Stability::calculate(double press, double gndtemp, double gnddewpoint)
{
	m_lcl_press = m_lcl_temp = m_lfc_press = m_lfc_temp = m_el_press = m_el_temp = m_liftedindex =
		m_cape = m_cin = std::numeric_limits<float>::quiet_NaN();
	if (m_tempcurve.empty())
		return;
	if (std::isnan(press) || std::isnan(gndtemp) || std::isnan(gnddewpoint) ||
	    press <= 0 || gndtemp < gnddewpoint) {
		m_tempcurve.clear();
		return;
	}
	// find LCL
	{
		double mixr(1);
		for (int i = 0; i < 12; ++i) {
			double t(SkewTChart::mixing_ratio(mixr, press));
			double td(SkewTChart::mixing_ratio(mixr + 0.01, press));
			if (!false)
				std::cerr << "Stability: " << press << "hPa, dewpt " << gnddewpoint
					  << " t " << t << " td " << td << " mixr " << mixr << std::endl;
			mixr += (gnddewpoint - t) / (td - t) * 0.01;
		}
		double tdry(gndtemp / SkewTChart::dry_adiabat(1., press));
		if (!false)
			std::cerr << "Stability: mixr " << mixr << " tdry " << tdry << std::endl;
		SoundingSurfaceTemp x[2];
		x[0].set_press(1023);
		x[1].set_press(10);
		for (unsigned int j = 0; j < 2; ++j) {
			x[j].set_temp(SkewTChart::dry_adiabat(tdry, x[j].get_press()));
			x[j].set_dewpt(SkewTChart::mixing_ratio(mixr, x[j].get_press()));
		}
		if (x[0].get_dewpt() > x[0].get_temp() || x[1].get_dewpt() <= x[1].get_temp()) {
			m_tempcurve.clear();
			return;
		}
		for (int i = 0; i < 12; ++i) {
			SoundingSurfaceTemp y;
			y.set_press(0.5 * (x[0].get_press() + x[1].get_press()));
			y.set_temp(SkewTChart::dry_adiabat(tdry, y.get_press()));
			y.set_dewpt(SkewTChart::mixing_ratio(mixr, y.get_press()));
			x[(y.get_dewpt() > y.get_temp())] = y;
		}
		double t(x[1].get_temp() - x[0].get_temp() - x[1].get_dewpt() + x[0].get_dewpt());
		if (fabs(t) < 1e-10) {
			t = 0;
		} else {
			t = (x[0].get_dewpt() - x[0].get_temp()) / t;
			t = std::min(std::max(t, 0.), 1.);
		}
		m_lcl_press = x[0].get_press() + t * (x[1].get_press() - x[0].get_press());
		m_lcl_temp = x[0].get_temp() + t * (x[1].get_temp() - x[0].get_temp());
		if (!false)
			std::cerr << "Stability: t " << t << " p " << m_lcl_press << " t " << m_lcl_temp
				  << ' ' << x[0].get_dewpt() + t * (x[1].get_dewpt() - x[0].get_dewpt()) << std::endl;
		double tsat(m_lcl_temp);
		for (int i = 0; i < 12; ++i) {
			double t(SkewTChart::sat_adiabat(tsat, m_lcl_press));
			double td(SkewTChart::sat_adiabat(tsat + 1, m_lcl_press));
			if (!false)
				std::cerr << "Stability: " << m_lcl_press << "hPa, temp " << m_lcl_temp
					  << " t " << t << " td " << td << " tsat " << tsat << std::endl;
			tsat += (m_lcl_temp - t) / (td - t);
		}
		for (tempcurve_t::iterator i(m_tempcurve.begin()), e(m_tempcurve.end()); i != e; ++i) {
			SoundingSurfaceTemp& sfc(const_cast<SoundingSurfaceTemp&>(*i));
			if (sfc.get_press() >= m_lcl_press)
				sfc.set_dewpt(SkewTChart::dry_adiabat(tdry, sfc.get_press()));
			else
				sfc.set_dewpt(SkewTChart::sat_adiabat(tsat, sfc.get_press()));
		}
		// insert LCL
		{
			tempcurve_t::const_iterator i1(m_tempcurve.lower_bound(SoundingSurfaceTemp(m_lcl_press)));
			if (i1 != m_tempcurve.end() && i1 != m_tempcurve.begin() && m_lcl_press < i1->get_press()) {
				tempcurve_t::const_iterator i2(i1);
				--i1;
				double t((m_lcl_press - i1->get_press()) / (i2->get_press() - i1->get_press()));
				t *= i2->get_temp() - i1->get_temp();
				t += i1->get_temp();
				m_tempcurve.insert(SoundingSurfaceTemp(m_lcl_press, t, m_lcl_temp));
			}
		}
	}
	// insert crossover points
	for (tempcurve_t::iterator i1(m_tempcurve.begin()), ie(m_tempcurve.end()); i1 != ie; ) {
		tempcurve_t::iterator i2(i1);
		++i2;
		if (i2 == ie)
			break;
		if (!((i1->get_dewpt() < i1->get_temp() && i2->get_dewpt() > i2->get_temp()) ||
		      (i1->get_dewpt() > i1->get_temp() && i2->get_dewpt() < i2->get_temp()))) {
			i1 = i2;
			continue;
		}
		double t(i2->get_temp() - i1->get_temp() - i2->get_dewpt() + i1->get_dewpt());
		if (fabs(t) < 1e-10) {
			t = 0;
		} else {
			t = (i1->get_dewpt() - i1->get_temp()) / t;
			t = std::min(std::max(t, 0.), 1.);
		}
		float pr(i1->get_press() + t * (i2->get_press() - i1->get_press()));
		float te(i1->get_temp() + t * (i2->get_temp() - i1->get_temp()));
		std::pair<tempcurve_t::iterator,bool> ins(m_tempcurve.insert(SoundingSurfaceTemp(pr, te, te)));
		i1 = ins.first;
		if (!ins.second) {
			SoundingSurfaceTemp& x(const_cast<SoundingSurfaceTemp&>(*i1));
			x.set_temp(te);
			x.set_dewpt(te);
		}
	}
	// lifted index
	{
		static const float li_press = 500.0;
		tempcurve_t::const_iterator i1(m_tempcurve.lower_bound(SoundingSurfaceTemp(li_press)));
		if (i1 != m_tempcurve.end()) {
			if (i1->get_press() == li_press || i1 == m_tempcurve.begin()) {
				m_liftedindex = i1->get_temp() - i1->get_dewpt();
			} else {
				tempcurve_t::const_iterator i2(i1);
				--i1;
				double t((m_lcl_press - i1->get_press()) / (i2->get_press() - i1->get_press()));
				t *= i2->get_temp() - i1->get_temp() - i2->get_dewpt() + i1->get_dewpt();
				t += i1->get_temp() - i1->get_dewpt();
				m_liftedindex = t;
			}
		}
	}
	// CIN and CAPE
	// FIXME: CAPE/CIN should use virtual temperatures
	// http://journals.ametsoc.org/doi/pdf/10.1175/1520-0434%281994%29009%3C0625%3ATEONTV%3E2.0.CO%3B2
	if (!m_tempcurve.empty()) {
		tempcurve_t::const_iterator ie(m_tempcurve.end()), i1(m_tempcurve.lower_bound(SoundingSurfaceTemp(m_lcl_press)));
		--ie;
		if (i1 != m_tempcurve.begin() && i1->get_press() > m_lcl_press)
			--i1;
		for (; i1 != ie; ) {
			tempcurve_t::const_iterator i2(i1);
			++i2;
			while (i2 != ie && i2->get_temp() != i2->get_dewpt())
				++i2;
			float pot(0);
			{
				tempcurve_t::const_iterator i0(i1);
				while (i0 != i2) {
					tempcurve_t::const_iterator i3(i0);
					++i3;
					float alt0(0), alt1(0);
					IcaoAtmosphere<float>::std_pressure_to_altitude(&alt0, 0, i0->get_press());
					IcaoAtmosphere<float>::std_pressure_to_altitude(&alt1, 0, i3->get_press());
					float t0((i0->get_dewpt() - i0->get_temp()) / i0->get_temp());
					float t1((i3->get_dewpt() - i3->get_temp()) / i3->get_temp());
					pot += 0.5 * (t0 + t1) * (alt1 - alt0);
					i0 = i3;
				}
			}
			pot *= IcaoAtmosphere<float>::const_g;
			if (!std::isnan(m_cape))
				break;
			if (pot < 0 && std::isnan(m_cin))
				m_cin = -pot;
			if (pot > 0) {
				m_cape = pot;
				m_lfc_press = i1->get_press();
				m_lfc_temp = i1->get_temp();
				m_el_press = i2->get_press();
				m_el_temp = i2->get_temp();
				break;
			}
			i1 = i2;
		}
	}
}

const int32_t GRIB2::WeatherProfilePoint::invalidalt = std::numeric_limits<int32_t>::min();
const float GRIB2::WeatherProfilePoint::invalidcover = std::numeric_limits<float>::quiet_NaN();
const int16_t GRIB2::WeatherProfilePoint::isobaric_levels[33] = {
	-1,
	1000,
	975,
	950,
	925,
	900,
	850,
	800,
	750,
	700,
	650,
	600,
	550,
	500,
	475,
	450,
	425,
	400,
	350,
	300,
	275,
	250,
	225,
	200,
	175,
	150,
	125,
	100,
	70,
	50,
	30,
	20,
	10
};

constexpr uint16_t GRIB2::WeatherProfilePoint::flags_daymask;
constexpr uint16_t GRIB2::WeatherProfilePoint::flags_day;
constexpr uint16_t GRIB2::WeatherProfilePoint::flags_dusk;
constexpr uint16_t GRIB2::WeatherProfilePoint::flags_night;
constexpr uint16_t GRIB2::WeatherProfilePoint::flags_dawn;
constexpr uint16_t GRIB2::WeatherProfilePoint::flags_rain;
constexpr uint16_t GRIB2::WeatherProfilePoint::flags_freezingrain;
constexpr uint16_t GRIB2::WeatherProfilePoint::flags_icepellets;
constexpr uint16_t GRIB2::WeatherProfilePoint::flags_snow;
constexpr uint16_t GRIB2::WeatherProfilePoint::flags_sand;
constexpr uint16_t GRIB2::WeatherProfilePoint::flags_tornado;
constexpr uint16_t GRIB2::WeatherProfilePoint::flags_thunderstorm;

namespace {

	inline int32_t grib2_std_pressure_to_altitude(float press)
	{
		float alt;
		IcaoAtmosphere<float>::std_pressure_to_altitude(&alt, 0, press);
		return Point::round<int32_t,float>(alt * Point::m_to_ft);
	};

};

const int32_t GRIB2::WeatherProfilePoint::altitudes[sizeof(isobaric_levels)/sizeof(isobaric_levels[0])] = {
	0,
	grib2_std_pressure_to_altitude(1000),
	grib2_std_pressure_to_altitude(975),
	grib2_std_pressure_to_altitude(950),
	grib2_std_pressure_to_altitude(925),
	grib2_std_pressure_to_altitude(900),
	grib2_std_pressure_to_altitude(850),
	grib2_std_pressure_to_altitude(800),
	grib2_std_pressure_to_altitude(750),
	grib2_std_pressure_to_altitude(700),
	grib2_std_pressure_to_altitude(650),
	grib2_std_pressure_to_altitude(600),
	grib2_std_pressure_to_altitude(550),
	grib2_std_pressure_to_altitude(500),
	grib2_std_pressure_to_altitude(475),
	grib2_std_pressure_to_altitude(450),
	grib2_std_pressure_to_altitude(425),
	grib2_std_pressure_to_altitude(400),
	grib2_std_pressure_to_altitude(350),
	grib2_std_pressure_to_altitude(300),
	grib2_std_pressure_to_altitude(275),
	grib2_std_pressure_to_altitude(250),
	grib2_std_pressure_to_altitude(225),
	grib2_std_pressure_to_altitude(200),
	grib2_std_pressure_to_altitude(175),
	grib2_std_pressure_to_altitude(150),
	grib2_std_pressure_to_altitude(125),
	grib2_std_pressure_to_altitude(100),
	grib2_std_pressure_to_altitude(70),
	grib2_std_pressure_to_altitude(50),
	grib2_std_pressure_to_altitude(30),
	grib2_std_pressure_to_altitude(20),
	grib2_std_pressure_to_altitude(10)
};

const GRIB2::WeatherProfilePoint::Surface GRIB2::WeatherProfilePoint::invalid_surface(std::numeric_limits<float>::quiet_NaN(),
										      std::numeric_limits<float>::quiet_NaN(),
										      std::numeric_limits<float>::quiet_NaN(),
										      std::numeric_limits<float>::quiet_NaN(),
										      std::numeric_limits<float>::quiet_NaN(),
										      std::numeric_limits<float>::quiet_NaN());

GRIB2::WeatherProfilePoint::WeatherProfilePoint(double dist, double routedist, unsigned int routeindex,
						const Point& pt, gint64 efftime, int32_t alt,
						int32_t zerodegisotherm, int32_t tropopause,
						float cldbdrycover, int32_t bdrylayerheight,
						float cldlowcover, int32_t cldlowbase, int32_t cldlowtop,
						float cldmidcover, int32_t cldmidbase, int32_t cldmidtop,
						float cldhighcover, int32_t cldhighbase, int32_t cldhightop,
						float cldconvcover, int32_t cldconvbase, int32_t cldconvtop,
						float precip, float preciprate, float convprecip, float convpreciprate,
						float liftedindex, float cape, float cin, float qff, uint16_t flags)
	: m_pt(pt), m_efftime(efftime), m_dist(dist), m_routedist(routedist), m_routeindex(routeindex), m_alt(alt),
	  m_zerodegisotherm(zerodegisotherm), m_tropopause(tropopause),
	  m_cldbdrycover(cldbdrycover), m_bdrylayerheight(bdrylayerheight),
	  m_cldlowcover(cldlowcover), m_cldlowbase(cldlowbase), m_cldlowtop(cldlowtop),
	  m_cldmidcover(cldmidcover), m_cldmidbase(cldmidbase), m_cldmidtop(cldmidtop),
	  m_cldhighcover(cldhighcover), m_cldhighbase(cldhighbase), m_cldhightop(cldhightop),
	  m_cldconvcover(cldconvcover), m_cldconvbase(cldconvbase), m_cldconvtop(cldconvtop),
	  m_precip(precip), m_preciprate(preciprate), m_convprecip(convprecip), m_convpreciprate(convpreciprate),
	  m_liftedindex(liftedindex), m_cape(cape), m_cin(cin), m_qff(qff), m_flags(flags)
{
}

int32_t GRIB2::WeatherProfilePoint::get_bdrylayerbase(void) const
{
	// https://en.wikipedia.org/wiki/Lapse_rate#Dry_adiabatic_lapse_rate
	// 9.8°C/km
	static const float dryadiabaticlapserate(9.8f / 1000.f * Point::ft_to_m);
	float dewpt(operator[](0).get_dewpoint()), temp(operator[](0).get_temp());
	if (std::isnan(dewpt) || std::isnan(temp) || dewpt > temp)
		return invalidalt;
	return (temp - dewpt) * (1.f / dryadiabaticlapserate);
}

GRIB2::WeatherProfilePoint::Surface& GRIB2::WeatherProfilePoint::operator[](unsigned int i)
{
	if (i >= sizeof(m_surfaces)/sizeof(m_surfaces[0]))
		return const_cast<Surface&>(invalid_surface);
	return m_surfaces[i];
}

const GRIB2::WeatherProfilePoint::Surface& GRIB2::WeatherProfilePoint::operator[](unsigned int i) const
{
	if (i >= sizeof(m_surfaces)/sizeof(m_surfaces[0]))
		return invalid_surface;
	return m_surfaces[i];
}

GRIB2::WeatherProfilePoint::operator soundingsurfaces_t(void) const
{
	soundingsurfaces_t s;
	for (unsigned int i(0); i <= sizeof(m_surfaces)/sizeof(m_surfaces[0]); ++i) {
		SoundingSurface x(m_surfaces[i]);
		if (!x.is_temp_valid())
			continue;
		x.set_press(isobaric_levels[i]);
		s.insert(x);
	}
	return s;
}

bool GRIB2::WeatherProfilePoint::is_pressure_valid(float press_pa)
{
	return !std::isnan(press_pa) && press_pa >= 0.38f && press_pa <= 200000.0f;
}

GRIB2::WeatherProfile::WeatherProfile(void)
	: m_minefftime(std::numeric_limits<gint64>::max()), m_maxefftime(std::numeric_limits<gint64>::min()),
	  m_minreftime(std::numeric_limits<gint64>::max()), m_maxreftime(std::numeric_limits<gint64>::min()),
	  m_icingmode(icingmode_t::autorouter)
{
}

void GRIB2::WeatherProfile::add_reftime(gint64 t)
{
	m_minreftime = std::min(t, m_minreftime);
	m_maxreftime = std::max(t, m_maxreftime);
}

void GRIB2::WeatherProfile::add_efftime(gint64 t)
{
	m_minefftime = std::min(t, m_minefftime);
	m_maxefftime = std::max(t, m_maxefftime);
}

double GRIB2::WeatherProfile::get_dist(void) const
{
        if (this->empty())
                return 0;
        return this->operator[](this->size() - 1).get_routedist();
}

std::ostream& GRIB2::WeatherProfile::print(std::ostream& os) const
{
	static constexpr unsigned int nrsfc(sizeof(WeatherProfilePoint::isobaric_levels)/sizeof(WeatherProfilePoint::isobaric_levels[0]));
	os << "Weather Profile";
	for (WeatherProfile::models_t::const_iterator i(get_models().begin()), e(get_models().end()); i != e; ++i)
		os << ' ' << *i;
	os << std::endl;
	for (WeatherProfile::const_iterator pi(begin()), pe(end()); pi != pe; ++pi) {
		os << "D " << pi->get_routedist() << " W" << pi->get_routeindex() << " CLD B ";
		if (std::isnan(pi->get_cldbdrycover()) ||
		    pi->get_bdrylayerheight() == GRIB2::WeatherProfilePoint::invalidalt)
			os << "--";
		else
			os << pi->get_cldbdrycover() << ' ' << pi->get_bdrylayerheight();
		os << " L ";
		if (std::isnan(pi->get_cldlowcover()) ||
		    pi->get_cldlowbase() == GRIB2::WeatherProfilePoint::invalidalt ||
		    pi->get_cldlowtop() == GRIB2::WeatherProfilePoint::invalidalt)
			os << "--";
		else
			os << pi->get_cldlowcover() << ' ' << pi->get_cldlowbase() << ' ' << pi->get_cldlowtop();
		os << " M ";
		if (std::isnan(pi->get_cldmidcover()) ||
		    pi->get_cldmidbase() == GRIB2::WeatherProfilePoint::invalidalt ||
		    pi->get_cldmidtop() == GRIB2::WeatherProfilePoint::invalidalt)
			os << "--";
		else
			os << pi->get_cldmidcover() << ' ' << pi->get_cldmidbase() << ' ' << pi->get_cldmidtop();
		os << " H ";
		if (std::isnan(pi->get_cldhighcover()) ||
		    pi->get_cldhighbase() == GRIB2::WeatherProfilePoint::invalidalt ||
		    pi->get_cldhightop() == GRIB2::WeatherProfilePoint::invalidalt)
			os << "--";
		else
			os << pi->get_cldhighcover() << ' ' << pi->get_cldhighbase() << ' ' << pi->get_cldhightop();
		os << " C ";
		if (std::isnan(pi->get_cldconvcover()) ||
		    pi->get_cldconvbase() == GRIB2::WeatherProfilePoint::invalidalt ||
		    pi->get_cldconvtop() == GRIB2::WeatherProfilePoint::invalidalt)
			os << "--";
		else
			os << pi->get_cldconvcover() << ' ' << pi->get_cldconvbase() << ' ' << pi->get_cldconvtop();
		os << " precip " << (pi->get_preciprate() * 86400.0)
		   << "mm/day conv " << (pi->get_convpreciprate() * 86400.0) << "mm/day"
		   << " Q" << pi->get_qff() << " CAPE " << pi->get_cape() << " CIN " << pi->get_cin() << " LI " << pi->get_liftedindex();
		switch (pi->get_flags() & WeatherProfilePoint::flags_daymask) {
		default:
		case WeatherProfilePoint::flags_day:
			os << " day";
			break;

		case WeatherProfilePoint::flags_dusk:
			os << " dusk";
			break;

		case WeatherProfilePoint::flags_night:
			os << " night";
			break;

		case WeatherProfilePoint::flags_dawn:
			os << " dawn";
			break;
		}
		if (pi->get_flags() & WeatherProfilePoint::flags_rain)
			os << " rain";
		if (pi->get_flags() & WeatherProfilePoint::flags_freezingrain)
			os << " freezingrain";
		if (pi->get_flags() & WeatherProfilePoint::flags_icepellets)
			os << " icepellets";
		if (pi->get_flags() & WeatherProfilePoint::flags_snow)
			os << " snow";
		os << std::endl;
		if (true) {
			for (unsigned int i = 0; i < nrsfc; ++i) {
				const WeatherProfilePoint::Surface& sfc(pi->operator[](i));
				os << "  Isobar " << WeatherProfilePoint::isobaric_levels[i]
				   << " w " << sfc.get_uwind() << '/' << sfc.get_vwind()
				   << " t " << sfc.get_temp() << '/' << sfc.get_dewpoint()
				   << " rh " << sfc.get_rh() << " hwsh " << sfc.get_hwsh()
				   << " vwsh " << sfc.get_vwsh() << " turb " << sfc.get_turbulenceindex()
				   << " cld " << sfc.get_cloudcover() << " ice " << sfc.get_icing()
				   << " edp " << sfc.get_eddydissipationrate() << std::endl;
			}
		}
	}
	return os;
}

GRIB2::LayerStatistics::LayerStatistics(gint64 minref, gint64 maxref, gint64 mineff, gint64 maxeff)
	: m_minref(minref), m_maxref(maxref), m_mineff(mineff), m_maxeff(maxeff)
{
}

void GRIB2::LayerStatistics::add_reftime(gint64 t)
{
	m_minref = std::min(m_minref, t);
	m_maxref = std::max(m_maxref, t);
}

void GRIB2::LayerStatistics::add_efftime(gint64 t)
{
	m_mineff = std::min(m_mineff, t);
	m_maxeff = std::max(m_maxeff, t);
}

void GRIB2::LayerStatistics::add(const boost::intrusive_ptr<const Layer>& p)
{
	if (!p)
		return;
	add_reftime(p->get_reftime());
	add_efftime(p->get_efftime());
}

GRIB2::GRIB2(void)
{
	set_default_cachedir();
}

GRIB2::GRIB2(const std::string& cachedir)
	: m_cachedir(cachedir)
{
}

GRIB2::~GRIB2(void)
{
}

void GRIB2::deallocate(const Layer *expr)
{
	expr->~Layer();
	operator delete(const_cast<Layer *>(expr), LayerAlloc);
}

void GRIB2::deallocate(const LayerResult *expr)
{
	expr->~LayerResult();
	operator delete(const_cast<GRIB2::LayerResult *>(expr), GRIB2::LayerAlloc);
}

void GRIB2::deallocate(const LayerInterpolateResult *expr)
{
	expr->~LayerInterpolateResult();
	operator delete(const_cast<GRIB2::LayerInterpolateResult *>(expr), GRIB2::LayerAlloc);
}

void GRIB2::set_default_cachedir(void)
{
	m_cachedir.clear();
	std::string dir(FPlan::get_userdbpath());
	if (Glib::file_test(dir, Glib::FILE_TEST_EXISTS) && Glib::file_test(dir, Glib::FILE_TEST_IS_DIR)) {
		dir = Glib::build_filename(dir, "gfscache");
		if (!Glib::file_test(dir, Glib::FILE_TEST_EXISTS))
			g_mkdir_with_parents(dir.c_str(), 0755);
		if (Glib::file_test(dir, Glib::FILE_TEST_EXISTS) && Glib::file_test(dir, Glib::FILE_TEST_IS_DIR))
			m_cachedir = dir;
	}
}

void GRIB2::add_layer(const boost::intrusive_ptr<Layer>& layer)
{
	if (!layer)
		return;
	std::pair<layers_t::iterator,bool> ins;
	{
		Glib::Mutex::Lock lock(m_mutex);
		ins = m_layers.insert(LayerPtr(layer));
	}
	if (false && !ins.second)
		std::cerr << "duplicate layer " << layer->get_parameter()->get_abbrev_nonnull()
			  << " sfc1 " << find_surfacetype_str(layer->get_surface1type())
			  << ' ' << layer->get_surface1value()
			  << " sfc2 " << find_surfacetype_str(layer->get_surface2type())
			  << ' ' << layer->get_surface2value()
			  << " eff " << layer->get_efftime()
			  << " ref " << layer->get_reftime()
			  << " not inserted" << std::endl;
}

unsigned int GRIB2::remove_missing_layers(void)
{
	unsigned int ret(0);
	{
		Glib::Mutex::Lock lock(m_mutex);
		for (layers_t::iterator li(m_layers.begin()), le(m_layers.end()); li != le;) {
			layers_t::iterator li1(li);
			++li;
			if (li1->get_layer()->check_load())
				continue;
			m_layers.erase(li1);
			++ret;
		}
	}
	return ret;
}

unsigned int GRIB2::remove_obsolete_layers(void)
{
	unsigned int ret(0);
	{
		Glib::Mutex::Lock lock(m_mutex);
		for (layers_t::iterator li(m_layers.begin()), le(m_layers.end()); li != le;) {
			layers_t::iterator li1(li);
			++li;
			if (li == le)
				break;
			const boost::intrusive_ptr<Layer>& lay1(li1->get_layer());
			const boost::intrusive_ptr<Layer>& lay2(li->get_layer());
			if (lay1->get_parameter() != lay2->get_parameter())
				continue;
			if (lay1->get_surface1type() != lay2->get_surface1type())
				continue;
			if (lay1->get_surface1value() != lay2->get_surface1value())
				continue;
			if (lay1->get_surface2type() != lay2->get_surface2type())
				continue;
			if (lay1->get_surface2value() != lay2->get_surface2value())
				continue;
			if (lay1->get_efftime() != lay2->get_efftime())
				continue;
			m_layers.erase(li1);
			++ret;
		}
	}
	return ret;
}

unsigned int GRIB2::expire_cache(unsigned int maxdays, off_t maxbytes)
{
	return Cache::expire(get_cachedir(), maxdays, maxbytes);
}

struct GRIB2::parameter_cmp
{
	bool operator()(const GRIB2::ParamDiscipline& a, uint8_t b) { return a.get_id() < b; }
	bool operator()(const GRIB2::ParamCategory& a, uint8_t b) { return a.get_id() < b; }
	bool operator()(const GRIB2::Parameter& a, uint8_t b) { return a.get_id() < b; }
	bool operator()(const GRIB2::ID8String& a, uint8_t b) { return a.get_id() < b; }
	bool operator()(const GRIB2::ID16String& a, uint8_t b) { return a.get_id() < b; }
	bool operator()(const GRIB2::CenterTable& a, uint8_t b) { return a.get_id() < b; }
	bool operator()(const GRIB2::SurfaceTable& a, uint8_t b) { return a.get_id() < b; }
	bool operator()(const char *a, const char *b) {
		if (a) {
			if (!b)
				return false;
			return strcmp(a, b) < 0;
		}
		return !!b;
	}
	bool operator()(const GRIB2::ParamDiscipline& a, const char *b) { return (*this)(a.get_str(), b); }
	bool operator()(const GRIB2::ParamCategory& a, const char *b) { return (*this)(a.get_str(), b); }
	bool operator()(const GRIB2::Parameter& a, const char *b) { return (*this)(a.get_str(), b); }
	bool operator()(const GRIB2::ParamDiscipline *a, const char *b) { if (!a) return false; return (*this)(*a, b); }
	bool operator()(const GRIB2::ParamCategory *a, const char *b) { if (!a) return false; return (*this)(*a, b); }
	bool operator()(const GRIB2::Parameter *a, const char *b) { if (!a) return false; return (*this)(*a, b); }
};

struct GRIB2::parameter_abbrev_cmp
{
	bool operator()(const GRIB2::Parameter& a, const char *b) { return parameter_cmp()(a.get_abbrev(), b); }
	bool operator()(const GRIB2::Parameter *a, const char *b) { if (!a) return false; return (*this)(*a, b); }
};

const GRIB2::ParamDiscipline *GRIB2::find_discipline(uint8_t id)
{
	const ParamDiscipline *b(&paramdiscipline[0]);
	const ParamDiscipline *e(&paramdiscipline[nr_paramdiscipline]);
	const ParamDiscipline *i(std::lower_bound(b, e, id, parameter_cmp()));
	if (i == e)
		return 0;
	return i;
}

const GRIB2::ParamCategory *GRIB2::find_paramcategory(const ParamDiscipline *disc, uint8_t id)
{
	if (!disc)
		return 0;
	const ParamCategory *b(disc[0].get_category());
	const ParamCategory *e(disc[1].get_category());
	const ParamCategory *i(std::lower_bound(b, e, id, parameter_cmp()));
	if (i == e)
		return 0;
	return i;
}

const GRIB2::Parameter *GRIB2::find_parameter(const ParamCategory *cat, uint8_t id)
{
	if (!cat)
		return 0;
	const Parameter *b(cat[0].get_parameter());
	const Parameter *e(cat[1].get_parameter());
	const Parameter *i(std::lower_bound(b, e, id, parameter_cmp()));
	if (i == e)
		return 0;
	return i;
}

const GRIB2::ParamDiscipline * const *GRIB2::find_discipline(const char *str)
{
	if (!str)
		return 0;
	const ParamDiscipline * const *b(&paramdisciplineindex[0]);
	const ParamDiscipline * const *e(&paramdisciplineindex[nr_paramdisciplineindex]);
	const ParamDiscipline * const *i(std::lower_bound(b, e, str, parameter_cmp()));
	if (i == e)
		return 0;
	if (!*i)
		return 0;
	if (!(*i)->get_str())
		return 0;
	if (strcmp((*i)->get_str(), str))
		return 0;
	return i;
}

const GRIB2::ParamCategory * const *GRIB2::find_paramcategory(const char *str)
{
	if (!str)
		return 0;
	const ParamCategory * const *b(&paramcategoryindex[0]);
	const ParamCategory * const *e(&paramcategoryindex[nr_paramcategoryindex]);
	const ParamCategory * const *i(std::lower_bound(b, e, str, parameter_cmp()));
	if (i == e)
		return 0;
	if (!*i)
		return 0;
	if (!(*i)->get_str())
		return 0;
	if (strcmp((*i)->get_str(), str))
		return 0;
	return i;
}

const GRIB2::Parameter * const *GRIB2::find_parameter(const char *str)
{
	if (!str)
		return 0;
	const Parameter * const *b(&parameternameindex[0]);
	const Parameter * const *e(&parameternameindex[nr_parameternameindex]);
	const Parameter * const *i(std::lower_bound(b, e, str, parameter_cmp()));
	if (i == e)
		return 0;
	if (!*i)
		return 0;
	if (!(*i)->get_str())
		return 0;
	if (strcmp((*i)->get_str(), str))
		return 0;
	return i;
}

const GRIB2::Parameter * const *GRIB2::find_parameter_by_abbrev(const char *str)
{
	if (!str)
		return 0;
	const Parameter * const *b(&parameterabbrevindex[0]);
	const Parameter * const *e(&parameterabbrevindex[nr_parameterabbrevindex]);
	const Parameter * const *i(std::lower_bound(b, e, str, parameter_abbrev_cmp()));
	if (i == e)
		return 0;
	if (!*i)
		return 0;
	if (strcmp((*i)->get_abbrev(), str))
		return 0;
	return i;
}

const GRIB2::CenterTable *GRIB2::find_centerid_table(uint16_t cid)
{
	const CenterTable *b(&centertable[0]);
	const CenterTable *e(&centertable[nr_centertable]);
	const CenterTable *i(std::lower_bound(b, e, cid, parameter_cmp()));
	if (i == e)
		return 0;
	return i;
}

const char *GRIB2::find_centerid_str(uint16_t cid, const char *dflt)
{
	const CenterTable *ct(find_centerid_table(cid));
	if (!ct)
		return dflt;
	return ct->get_str(dflt);
}

const char *GRIB2::find_subcenterid_str(uint16_t cid, uint16_t sid, const char *dflt)
{
	const CenterTable *ct(find_centerid_table(cid));
	if (!ct)
		return dflt;
	const ID16String *b(ct->subcenter_begin());
	const ID16String *e(ct->subcenter_end());
	const ID16String *i(std::lower_bound(b, e, sid, parameter_cmp()));
	if (i == e)
		return dflt;
	return i->get_str(dflt);
}

const char *GRIB2::find_genprocesstype_str(uint16_t cid, uint8_t genproct, const char *dflt)
{
	const CenterTable *ct(find_centerid_table(cid));
	if (!ct)
		return dflt;
	const ID8String *b(ct->model_begin());
	const ID8String *e(ct->model_end());
	const ID8String *i(std::lower_bound(b, e, genproct, parameter_cmp()));
	if (i == e)
		return dflt;
	return i->get_str(dflt);
}

const char *GRIB2::find_productionstatus_str(uint8_t pstat, const char *dflt)
{
	const ID8String *b(&prodstatustable[0]);
	const ID8String *e(&prodstatustable[nr_prodstatustable]);
	const ID8String *i(std::lower_bound(b, e, pstat, parameter_cmp()));
	if (i == e)
		return dflt;
	return i->get_str(dflt);
}

const char *GRIB2::find_datatype_str(uint8_t dtype, const char *dflt)
{
	const ID8String *b(&typeofdatatable[0]);
	const ID8String *e(&typeofdatatable[nr_typeofdatatable]);
	const ID8String *i(std::lower_bound(b, e, dtype, parameter_cmp()));
	if (i == e)
		return dflt;
	return i->get_str(dflt);
}

const char *GRIB2::find_genprocess_str(uint8_t genproc, const char *dflt)
{
	const ID8String *b(&genprocesstable[0]);
	const ID8String *e(&genprocesstable[nr_genprocesstable]);
	const ID8String *i(std::lower_bound(b, e, genproc, parameter_cmp()));
	if (i == e)
		return dflt;
	return i->get_str(dflt);
}

const char *GRIB2::find_surfacetype_str(uint8_t sfctype, const char *dflt)
{
	const SurfaceTable *b(&surfacetable[0]);
	const SurfaceTable *e(&surfacetable[nr_surfacetable]);
	const SurfaceTable *i(std::lower_bound(b, e, sfctype, parameter_cmp()));
	if (i == e)
		return dflt;
	return i->get_str(dflt);
}

const char *GRIB2::find_surfaceunit_str(uint8_t sfctype, const char *dflt)
{
	const SurfaceTable *b(&surfacetable[0]);
	const SurfaceTable *e(&surfacetable[nr_surfacetable]);
	const SurfaceTable *i(std::lower_bound(b, e, sfctype, parameter_cmp()));
	if (i == e)
		return dflt;
	return i->get_unit(dflt);
}

GRIB2::LayerStatistics GRIB2::get_layerstat(void)
{
	LayerStatistics stat;
	Glib::Mutex::Lock lock(m_mutex);
	for (layers_t::const_iterator i(m_layers.begin()), e(m_layers.end()); i != e; ++i)
		stat.add(i->get_layer());
	return stat;
}

GRIB2::layerlist_t GRIB2::find_layers(void)
{
	layerlist_t l;
	Glib::Mutex::Lock lock(m_mutex);
	for (layers_t::const_iterator i(m_layers.begin()), e(m_layers.end()); i != e; ++i)
		l.push_back(i->get_layer());
	return l;
}

GRIB2::layerlist_t GRIB2::find_layers(const Parameter *param, gint64 efftime, Layer::statproc_t stproc)
{
	if (!param)
		return layerlist_t();
	gint64 eff0(0);
	gint64 eff1(std::numeric_limits<gint64>::max());
	typedef std::set<LayerPtrSurface> set_t;
	set_t lay0, lay1;
	LayerPtr layptr(boost::intrusive_ptr<Layer>(new (LayerAlloc) LayerJ2K(param, boost::intrusive_ptr<Grid>(),
									      std::numeric_limits<gint64>::min(),
									      std::numeric_limits<gint64>::min(),
									      0xffff, 0xffff, 0xff, 0xff, 0xff, 0xff,
									      0, std::numeric_limits<double>::min(),
									      0, std::numeric_limits<double>::min(),
									      stproc)));
	Glib::Mutex::Lock lock(m_mutex);
	for (layers_t::const_iterator i(m_layers.lower_bound(layptr)), e(m_layers.end()); i != e; ++i) {
		const boost::intrusive_ptr<Layer>& layer(i->get_layer());
		if (layer->get_parameter() != param)
			break;
		if (layer->get_statproc() != stproc)
			continue;
		if (layer->get_efftime() < efftime) {
			if (layer->get_efftime() > eff0) {
				lay0.clear();
				eff0 = layer->get_efftime();
			}
			std::pair<set_t::iterator,bool> ins(lay0.insert(LayerPtrSurface(layer)));
			if (!ins.second) {
				if (layer->get_reftime() > ins.first->get_layer()->get_reftime()) {
					lay0.erase(ins.first);
					ins = lay0.insert(LayerPtrSurface(layer));
				}
			}
		} else {
			if (layer->get_efftime() < eff1) {
				lay1.clear();
				eff1 = layer->get_efftime();
			}
			std::pair<set_t::iterator,bool> ins(lay1.insert(LayerPtrSurface(layer)));
			if (!ins.second) {
				if (layer->get_reftime() > ins.first->get_layer()->get_reftime()) {
					lay1.erase(ins.first);
					ins = lay1.insert(LayerPtrSurface(layer));
				}
			}
		}
	}
	layerlist_t ll;
	for (set_t::const_iterator i(lay0.begin()), e(lay0.end()); i != e; ++i)
		ll.push_back(i->get_layer());
	for (set_t::const_iterator i(lay1.begin()), e(lay1.end()); i != e; ++i)
		ll.push_back(i->get_layer());
	return ll;
}

GRIB2::layerlist_t GRIB2::find_layers(const Parameter *param, gint64 efftime, Layer::statproc_t stproc, uint8_t sfc1type, double sfc1value)
{
	if (!param)
		return layerlist_t();
	gint64 eff0(0);
	gint64 eff1(0);
	gint64 eff2(std::numeric_limits<gint64>::max());
	gint64 eff3(std::numeric_limits<gint64>::max());
	boost::intrusive_ptr<Layer> lay0, lay1, lay2, lay3;
	LayerPtr layptr(boost::intrusive_ptr<Layer>(new (LayerAlloc) LayerJ2K(param, boost::intrusive_ptr<Grid>(),
									      std::numeric_limits<gint64>::min(),
									      std::numeric_limits<gint64>::min(),
									      0xffff, 0xffff, 0xff, 0xff, 0xff, 0xff,
									      0, std::numeric_limits<double>::min(),
									      0, std::numeric_limits<double>::min(),
									      stproc)));
	Glib::Mutex::Lock lock(m_mutex);
	for (layers_t::const_iterator i(m_layers.lower_bound(layptr)), e(m_layers.end()); i != e; ++i) {
		const boost::intrusive_ptr<Layer>& layer(i->get_layer());
		if (layer->get_parameter() != param)
			break;
		if (layer->get_statproc() != stproc)
			continue;
		if (layer->get_surface1type() != sfc1type)
			continue;
		if (layer->get_efftime() < efftime) {
			if (layer->get_surface1value() < sfc1value) {
				if (!lay0 || layer->get_efftime() > eff0 ||
				    (layer->get_efftime() == eff0 && layer->get_reftime() > lay0->get_reftime()) ||
				    (layer->get_efftime() == eff0 && layer->get_reftime() == lay0->get_reftime() &&
				     layer->get_surface1value() > lay0->get_surface1value())) {
					lay0 = layer;
					eff0 = layer->get_efftime();
				}
			} else {
				if (!lay1 || layer->get_efftime() > eff1 ||
				    (layer->get_efftime() == eff1 && layer->get_reftime() > lay1->get_reftime()) ||
				    (layer->get_efftime() == eff1 && layer->get_reftime() == lay1->get_reftime() &&
				     layer->get_surface1value() < lay1->get_surface1value())) {
					lay1 = layer;
					eff1 = layer->get_efftime();
				}
			}
		} else {
			if (layer->get_surface1value() < sfc1value) {
				if (!lay2 || layer->get_efftime() < eff2 ||
				    (layer->get_efftime() == eff2 && layer->get_reftime() > lay2->get_reftime()) ||
				    (layer->get_efftime() == eff2 && layer->get_reftime() == lay2->get_reftime() &&
				     layer->get_surface1value() > lay2->get_surface1value())) {
					lay2 = layer;
					eff2 = layer->get_efftime();
				}
			} else {
				if (!lay3 || layer->get_efftime() < eff3 ||
				    (layer->get_efftime() == eff3 && layer->get_reftime() > lay3->get_reftime()) ||
				    (layer->get_efftime() == eff3 && layer->get_reftime() == lay3->get_reftime() &&
				     layer->get_surface1value() < lay3->get_surface1value())) {
					lay3 = layer;
					eff3 = layer->get_efftime();
				}
			}
		}
	}
	layerlist_t ll;
	if (lay0)
		ll.push_back(lay0);
	if (lay1)
		ll.push_back(lay1);
	if (lay2)
		ll.push_back(lay2);
	if (lay3)
		ll.push_back(lay3);
	return ll;
}

GRIB2::sfcvalues_t GRIB2::find_sfc1values(const Parameter *param, gint64 efftime, Layer::statproc_t stproc, uint8_t sfc1type)
{
	if (!param)
		return sfcvalues_t();
	typedef std::map<double, gint64> ret_t;
	ret_t ret;
	LayerPtr layptr(boost::intrusive_ptr<Layer>(new (LayerAlloc) LayerJ2K(param, boost::intrusive_ptr<Grid>(),
									      std::numeric_limits<gint64>::min(),
									      std::numeric_limits<gint64>::min(),
									      0xffff, 0xffff, 0xff, 0xff, 0xff, 0xff,
									      0, std::numeric_limits<double>::min(),
									      0, std::numeric_limits<double>::min())));
	gint64 efftime1(std::numeric_limits<gint64>::min());
	Glib::Mutex::Lock lock(m_mutex);
	for (layers_t::const_iterator i(m_layers.lower_bound(layptr)), e(m_layers.end()); i != e; ++i) {
		const boost::intrusive_ptr<Layer>& layer(i->get_layer());
		if (layer->get_parameter() != param)
			break;
		if (layer->get_statproc() != stproc)
			continue;
		if (layer->get_surface1type() != sfc1type)
			continue;
		if (layer->get_efftime() > efftime)
			continue;
		if (std::isnan(layer->get_surface1value()))
			continue;
		std::pair<ret_t::iterator,bool> ins(ret.insert(ret_t::value_type(layer->get_surface1value(), layer->get_efftime())));
		if (!ins.second && ins.first->second < layer->get_efftime())
			ins.first->second < layer->get_efftime();
		efftime1 = std::max(efftime1, layer->get_efftime());
	}
	sfcvalues_t r;
	for (ret_t::const_iterator i(ret.begin()), e(ret.end()); i != e; ++i)
		if (i->second == efftime1)
			r.insert(i->first);
	return r;
}

class GRIB2::Interpolate {
public:
	Interpolate(const Rect& bbox, const layerlist_t& layers);
	boost::intrusive_ptr<LayerInterpolateResult> interpolate(gint64 efftime) const;
	boost::intrusive_ptr<LayerInterpolateResult> interpolate(double sfc1value) const;
	boost::intrusive_ptr<LayerInterpolateResult> interpolate(gint64 efftime, double sfc1value) const;
	boost::intrusive_ptr<LayerInterpolateResult> nointerp(unsigned int idx) const;
	std::ostream& dumplayers(std::ostream& os, unsigned int indent = 0) const;

protected:
	typedef std::vector<boost::intrusive_ptr<LayerResult> > lr_t;
	lr_t m_lr;
	typedef std::vector<gint64> efftimes_t;
	efftimes_t m_efftimes;
	typedef std::vector<double> sfc1values_t;
	sfc1values_t m_sfc1values;
	gint64 m_minefftime;
	gint64 m_maxefftime;
	gint64 m_minreftime;
	gint64 m_maxreftime;
	double m_minsfc1value;
	double m_maxsfc1value;
	bool m_samesize;
};

GRIB2::Interpolate::Interpolate(const Rect& bbox, const layerlist_t& layers)
	: m_minefftime(std::numeric_limits<gint64>::max()),
	  m_maxefftime(std::numeric_limits<gint64>::min()),
	  m_minreftime(std::numeric_limits<gint64>::max()),
	  m_maxreftime(std::numeric_limits<gint64>::min()),
	  m_minsfc1value(std::numeric_limits<double>::max()),
	  m_maxsfc1value(std::numeric_limits<double>::min()),
	  m_samesize(true)
{
	for (layerlist_t::const_iterator i(layers.begin()), e(layers.end()); i != e; ++i) {
		if (!*i)
			continue;
		boost::intrusive_ptr<GRIB2::LayerResult> r((*i)->get_results(bbox));
		if (!r)
			continue;
		m_lr.push_back(r);
		gint64 efftime(r->get_efftime());
		gint64 minreftime(r->get_minreftime());
		gint64 maxreftime(r->get_maxreftime());
		double sfc1value(r->get_surface1value());
		m_efftimes.push_back(efftime);
		m_sfc1values.push_back(sfc1value);
		m_minefftime = std::min(m_minefftime, efftime);
		m_maxefftime = std::max(m_maxefftime, efftime);
		m_minreftime = std::min(m_minreftime, minreftime);
		m_maxreftime = std::max(m_maxreftime, maxreftime);
		m_minsfc1value = std::min(m_minsfc1value, sfc1value);
		m_maxsfc1value = std::max(m_maxsfc1value, sfc1value);
		if (m_lr.size() == 1)
			continue;
		m_samesize = m_samesize && m_lr.front()->get_height() == m_lr.back()->get_height() &&
			m_lr.front()->get_width() == m_lr.back()->get_width() && m_lr.front()->get_bbox() == m_lr.back()->get_bbox();
	}
}

std::ostream& GRIB2::Interpolate::dumplayers(std::ostream& os, unsigned int indent) const
{
	for (lr_t::const_iterator i(m_lr.begin()), e(m_lr.end()); i != e; ++i) {
		os << std::string(indent, ' ') << (i - m_lr.begin()) << ": ";
		const boost::intrusive_ptr<const LayerResult>& lr(*i);
		if (!lr) {
			os << '-' << std::endl;
			continue;
		}
		os << lr->get_layer()->get_parameter()->get_abbrev_nonnull()
		   << ' ' << lr->get_width() << 'x' << lr->get_height() << ' '
		   << Glib::TimeVal(lr->get_efftime(), 0).as_iso8601() << ' '
		   << GRIB2::find_surfacetype_str(lr->get_layer()->get_surface1type(), "?") << ' '
		   << lr->get_surface1value() << std::endl;
	}
	return os;
}

boost::intrusive_ptr<GRIB2::LayerInterpolateResult> GRIB2::Interpolate::nointerp(unsigned int idx) const
{
	if (false)
		dumplayers(std::cerr << "GRIB2::Interpolate::nointerp: " << idx << std::endl, 2);
	if (idx >= m_lr.size())
		return boost::intrusive_ptr<LayerInterpolateResult>();
	const boost::intrusive_ptr<LayerResult const>& lay(m_lr[idx]);
	gint64 efftime(lay->get_layer()->get_efftime());
	gint64 reftime(lay->get_layer()->get_reftime());
	double sfc1value(lay->get_layer()->get_surface1value());
	unsigned int w(lay->get_width());
	unsigned int h(lay->get_height());
	boost::intrusive_ptr<LayerInterpolateResult> r(new (LayerAlloc) LayerInterpolateResult(lay->get_layer(), lay->get_bbox(), w, h,
											       efftime, efftime, reftime, reftime, sfc1value, sfc1value,
											       LayerInterpolateResult::LinInterp(), false));
	unsigned int sz(r->get_size());
	for (unsigned int i = 0; i < sz; ++i)
		r->operator[](i) = LayerInterpolateResult::LinInterp(lay->operator[](i), 0, 0, 0);
	return r;
}

boost::intrusive_ptr<GRIB2::LayerInterpolateResult> GRIB2::Interpolate::interpolate(gint64 efftime) const
{
	if (false)
		dumplayers(std::cerr << "GRIB2::Interpolate::interpolate: "
			   << Glib::TimeVal(efftime, 0).as_iso8601() << std::endl, 2);
	if (m_lr.empty())
		return boost::intrusive_ptr<LayerInterpolateResult>();
	if (m_lr.size() == 1)
		return nointerp(0);
	if (m_minefftime >= m_maxefftime)
		return nointerp(0);
	bool interpolate(m_lr.front()->get_layer()->is_interpolatable());
	double efftimemul(1.0 / (double)(m_maxefftime - m_minefftime));
	efftime = std::max(std::min(efftime, m_maxefftime), m_minefftime);
	double sfc1value(m_lr.front()->get_layer()->get_surface1value());
	unsigned int lrsz(m_lr.size());
	Eigen::MatrixXd A(lrsz, 2);
	for (unsigned int j = 0; j < lrsz; ++j) {
		A(j, 0) = 1;
		A(j, 1) = (m_efftimes[j] - m_minefftime) * efftimemul;
	}
	Eigen::MatrixXd M((A.transpose() * A).inverse() * A.transpose());
	unsigned int w(m_lr.front()->get_width());
	unsigned int h(m_lr.front()->get_height());
	if (false)
		std::cerr << "GRIB2 Interpolate: A " << A << std::endl << "GRIB2 Interpolate: M " << M << std::endl;
	if (false) {
		std::cerr << "GRIB2 Interpolate: " << m_lr.front()->get_layer()->get_parameter()->get_abbrev_nonnull()
			  << ' ' << w << 'x' << h << " eff " << m_minefftime << ".." << m_maxefftime
			  << " eff " << Glib::DateTime::create_now_utc(efftime).format("%F %H:%M:%S") << " (" << efftime << ')';
		if (m_samesize)
			std::cerr << " (same)";
		for (unsigned int j = 0; j < lrsz; ++j)
			std::cerr << " Layer " << M(0, j) << ' ' << M(1, j)
				  << " eff " << Glib::DateTime::create_now_utc(m_lr[j]->get_efftime()).format("%F %H:%M:%S")
				  << " (" << m_lr[j]->get_efftime() << ')';
		std::cerr << std::endl;
	}
	LayerInterpolateResult::LinInterp catinterp(0, 0, 0, 0);
	if (!interpolate) {
		for (unsigned int j = 0; j < lrsz; ++j)
			catinterp += LayerInterpolateResult::LinInterp(M(0, j) * j, M(1, j) * j, 0, 0);
	}
	boost::intrusive_ptr<LayerInterpolateResult> r(new (LayerAlloc) LayerInterpolateResult(m_lr.front()->get_layer(), m_lr.front()->get_bbox(), w, h,
											       m_minefftime, m_maxefftime, m_minreftime, m_maxreftime, sfc1value, sfc1value,
											       catinterp, !interpolate));
	if (!interpolate) {
		if (m_samesize) {
			unsigned int sz(r->get_size());
			for (unsigned int i = 0; i < sz; ++i) {
				float x[4] = { 0, 0, 0, 0 };
				for (unsigned int j = 0; j < lrsz && j < 4; ++j)
					x[j] = m_lr[j]->operator[](i);
				if (lrsz > 0 && lrsz < 4)
					for (unsigned int j = lrsz; j < 4; ++j)
						x[j] = x[lrsz-1];
				r->operator[](i) = LayerInterpolateResult::LinInterp(x[0], x[1], x[2], x[3]);
			}
			return r;
		}
		for (unsigned int u = 0; u < w; ++u) {
			for (unsigned int v = 0; v < h; ++v) {
				float x[4] = { 0, 0, 0, 0 };
				Point pt(r->get_center(u, v));
				for (unsigned int j = 0; j < lrsz && j < 4; ++j)
					x[j] = m_lr[j]->operator()(pt);
				if (lrsz > 0 && lrsz < 4)
					for (unsigned int j = lrsz; j < 4; ++j)
						x[j] = x[lrsz-1];
			        r->operator()(u, v) = LayerInterpolateResult::LinInterp(x[0], x[1], x[2], x[3]);
			}
		}
		return r;
	}
	if (m_samesize) {
		unsigned int sz(r->get_size());
		for (unsigned int i = 0; i < sz; ++i) {
			LayerInterpolateResult::LinInterp z(0, 0, 0, 0);
			for (unsigned int j = 0; j < lrsz; ++j) {
				float y(m_lr[j]->operator[](i));
				z += LayerInterpolateResult::LinInterp(M(0, j) * y, M(1, j) * y, 0, 0);
			}
			r->operator[](i) = z;
		}
		return r;
	}
	for (unsigned int u = 0; u < w; ++u) {
		for (unsigned int v = 0; v < h; ++v) {
			LayerInterpolateResult::LinInterp z;
			{
				float y(m_lr.front()->operator()(u, v));
				z = LayerInterpolateResult::LinInterp(M(0, 0) * y, M(1, 0) * y, 0, 0);
			}
			Point pt(r->get_center(u, v));
			for (unsigned int j = 1; j < lrsz; ++j) {
				float y(m_lr[j]->operator()(pt));
				z += LayerInterpolateResult::LinInterp(M(0, j) * y, M(1, j) * y, 0, 0);
			}
			r->operator()(u, v) = z;
		}
	}
	return r;
}

boost::intrusive_ptr<GRIB2::LayerInterpolateResult> GRIB2::Interpolate::interpolate(double sfc1value) const
{
	if (false)
		dumplayers(std::cerr << "GRIB2::Interpolate::interpolate: " << sfc1value << std::endl, 2);
	if (m_lr.empty())
		return boost::intrusive_ptr<LayerInterpolateResult>();
	if (m_lr.size() == 1)
		return nointerp(0);
	if (m_minsfc1value >= m_maxsfc1value || (m_maxsfc1value - m_minsfc1value) < 1e-100)
		return nointerp(0);
	bool interpolate(m_lr.front()->get_layer()->is_interpolatable());
	double sfc1valuemul(1.0 / (m_maxsfc1value - m_minsfc1value));
	sfc1value = std::max(std::min(sfc1value, m_maxsfc1value), m_minsfc1value);
	gint64 efftime(m_lr.front()->get_layer()->get_efftime());
	unsigned int lrsz(m_lr.size());
	Eigen::MatrixXd A(lrsz, 2);
	for (unsigned int j = 0; j < lrsz; ++j) {
		A(j, 0) = 1;
		A(j, 1) = (m_sfc1values[j] - m_minsfc1value) * sfc1valuemul;
	}
	Eigen::MatrixXd M((A.transpose() * A).inverse() * A.transpose());
	unsigned int w(m_lr.front()->get_width());
	unsigned int h(m_lr.front()->get_height());
	if (false)
		std::cerr << "GRIB2 Interpolate: A " << A << std::endl << "GRIB2 Interpolate: M " << M << std::endl;
	if (false) {
		std::cerr << "GRIB2 Interpolate: " << m_lr.front()->get_layer()->get_parameter()->get_abbrev_nonnull()
			  << ' ' << w << 'x' << h << " sfc1 " << m_minsfc1value << ".." << m_maxsfc1value
			  << " sfc1 " << sfc1value;
		if (m_samesize)
			std::cerr << " (same)";
		for (unsigned int j = 0; j < lrsz; ++j)
			std::cerr << " Layer " << M(0, j) << ' ' << M(1, j)
				  << " sfc1 " << m_lr[j]->get_surface1value();
		std::cerr << std::endl;
	}
	LayerInterpolateResult::LinInterp catinterp(0, 0, 0, 0);
	if (!interpolate) {
		for (unsigned int j = 0; j < lrsz; ++j)
			catinterp += LayerInterpolateResult::LinInterp(M(0, j) * j, M(1, j) * j, 0, 0);
	}
	boost::intrusive_ptr<LayerInterpolateResult> r(new (LayerAlloc) LayerInterpolateResult(m_lr.front()->get_layer(), m_lr.front()->get_bbox(), w, h,
											       efftime, efftime, m_minreftime, m_maxreftime, m_minsfc1value, m_maxsfc1value,
											       catinterp, !interpolate));
	if (!interpolate) {
		if (m_samesize) {
			unsigned int sz(r->get_size());
			for (unsigned int i = 0; i < sz; ++i) {
				float x[4] = { 0, 0, 0, 0 };
				for (unsigned int j = 0; j < lrsz && j < 4; ++j)
					x[j] = m_lr[j]->operator[](i);
				if (lrsz > 0 && lrsz < 4)
					for (unsigned int j = lrsz; j < 4; ++j)
						x[j] = x[lrsz-1];
				r->operator[](i) = LayerInterpolateResult::LinInterp(x[0], x[1], x[2], x[3]);
			}
			return r;
		}
		for (unsigned int u = 0; u < w; ++u) {
			for (unsigned int v = 0; v < h; ++v) {
				float x[4] = { 0, 0, 0, 0 };
				Point pt(r->get_center(u, v));
				for (unsigned int j = 0; j < lrsz && j < 4; ++j)
					x[j] = m_lr[j]->operator()(pt);
				if (lrsz > 0 && lrsz < 4)
					for (unsigned int j = lrsz; j < 4; ++j)
						x[j] = x[lrsz-1];
			        r->operator()(u, v) = LayerInterpolateResult::LinInterp(x[0], x[1], x[2], x[3]);
			}
		}
		return r;
	}
	if (m_samesize) {
		unsigned int sz(r->get_size());
		for (unsigned int i = 0; i < sz; ++i) {
			LayerInterpolateResult::LinInterp z(0, 0, 0, 0);
			for (unsigned int j = 0; j < lrsz; ++j) {
				float y(m_lr[j]->operator[](i));
				z += LayerInterpolateResult::LinInterp(M(0, j) * y, 0, M(1, j) * y, 0);
			}
			r->operator[](i) = z;
		}
		return r;
	}
	for (unsigned int u = 0; u < w; ++u) {
		for (unsigned int v = 0; v < h; ++v) {
			LayerInterpolateResult::LinInterp z;
			{
				float y(m_lr.front()->operator()(u, v));
				z = LayerInterpolateResult::LinInterp(M(0, 0) * y, 0, M(1, 0) * y, 0);
			}
			Point pt(r->get_center(u, v));
			for (unsigned int j = 1; j < lrsz; ++j) {
				float y(m_lr[j]->operator()(pt));
				z += LayerInterpolateResult::LinInterp(M(0, j) * y, 0, M(1, j) * y, 0);
			}
			r->operator()(u, v) = z;
		}
	}
	return r;
}

boost::intrusive_ptr<GRIB2::LayerInterpolateResult> GRIB2::Interpolate::interpolate(gint64 efftime, double sfc1value) const
{
	if (false)
		dumplayers(std::cerr << "GRIB2::Interpolate::interpolate: "
			   << Glib::TimeVal(efftime, 0).as_iso8601() << ' ' << sfc1value << std::endl, 2);
	if (m_lr.empty())
		return boost::intrusive_ptr<LayerInterpolateResult>();
	if (m_lr.size() == 1)
		return nointerp(0);
	if (m_minefftime >= m_maxefftime)
		return interpolate(sfc1value);
	if (m_minsfc1value >= m_maxsfc1value || (m_maxsfc1value - m_minsfc1value) < 1e-100)
		return interpolate(efftime);
	bool interpolate(m_lr.front()->get_layer()->is_interpolatable());
	double efftimemul(1.0 / (double)(m_maxefftime - m_minefftime));
	double sfc1valuemul(1.0 / (m_maxsfc1value - m_minsfc1value));
	efftime = std::max(std::min(efftime, m_maxefftime), m_minefftime);
	sfc1value = std::max(std::min(sfc1value, m_maxsfc1value), m_minsfc1value);
	unsigned int lrsz(m_lr.size());
	Eigen::MatrixXd A(lrsz, 4);
	for (unsigned int j = 0; j < lrsz; ++j) {
		A(j, 0) = 1;
		A(j, 1) = (m_efftimes[j] - m_minefftime) * efftimemul;
		A(j, 2) = (m_sfc1values[j] - m_minsfc1value) * sfc1valuemul;
		A(j, 3) = A(j, 1) * A(j, 2);
	}
	Eigen::MatrixXd M;
	if (lrsz >= 4) {
		M = (A.transpose() * A).inverse() * A.transpose();
	} else {
		A.conservativeResize(Eigen::NoChange_t(), 3);
		M = (A.transpose() * A).inverse() * A.transpose();
		M.conservativeResize(4, Eigen::NoChange_t());
		for (unsigned int j = 0; j < lrsz; ++j)
			M(3, j) = 0;
	}
	unsigned int w(m_lr.front()->get_width());
	unsigned int h(m_lr.front()->get_height());
	if (false)
		std::cerr << "GRIB2 Interpolate: A " << A << std::endl << "GRIB2 Interpolate: M " << M << std::endl;
	if (false) {
		std::cerr << "GRIB2 Interpolate: " << m_lr.front()->get_layer()->get_parameter()->get_abbrev_nonnull()
			  << ' ' << w << 'x' << h << " eff " << m_minefftime << ".." << m_maxefftime
			  << " sfc1 " << m_minsfc1value << ".." << m_maxsfc1value
			  << " eff " << Glib::DateTime::create_now_utc(efftime).format("%F %H:%M:%S") << " (" << efftime << ')'
			  << " sfc1 " << sfc1value;
		if (m_samesize)
			std::cerr << " (same)";
		for (unsigned int j = 0; j < lrsz; ++j)
			std::cerr << " Layer " << M(0, j) << ' ' << M(1, j) << ' ' << M(2, j) << ' ' << M(3, j)
				  << " eff " << Glib::DateTime::create_now_utc(m_lr[j]->get_efftime()).format("%F %H:%M:%S")
				  << " (" << m_lr[j]->get_efftime() << ')'
				  << " sfc1 " << m_lr[j]->get_surface1value();
		std::cerr << std::endl;
	}
	LayerInterpolateResult::LinInterp catinterp(0, 0, 0, 0);
	if (!interpolate) {
		for (unsigned int j = 0; j < lrsz; ++j)
			catinterp += LayerInterpolateResult::LinInterp(M(0, j) * j, M(1, j) * j, M(2, j) * j, M(3, j) * j);
	}
	boost::intrusive_ptr<LayerInterpolateResult> r(new (LayerAlloc) LayerInterpolateResult(m_lr.front()->get_layer(), m_lr.front()->get_bbox(), w, h,
											       m_minefftime, m_maxefftime, m_minreftime, m_maxreftime, m_minsfc1value, m_maxsfc1value,
											       catinterp, !interpolate));
	if (!interpolate) {
		if (m_samesize) {
			unsigned int sz(r->get_size());
			for (unsigned int i = 0; i < sz; ++i) {
				float x[4] = { 0, 0, 0, 0 };
				for (unsigned int j = 0; j < lrsz && j < 4; ++j)
					x[j] = m_lr[j]->operator[](i);
				if (lrsz > 0 && lrsz < 4)
					for (unsigned int j = lrsz; j < 4; ++j)
						x[j] = x[lrsz-1];
				r->operator[](i) = LayerInterpolateResult::LinInterp(x[0], x[1], x[2], x[3]);
			}
			return r;
		}
		for (unsigned int u = 0; u < w; ++u) {
			for (unsigned int v = 0; v < h; ++v) {
				float x[4] = { 0, 0, 0, 0 };
				Point pt(r->get_center(u, v));
				for (unsigned int j = 0; j < lrsz && j < 4; ++j)
					x[j] = m_lr[j]->operator()(pt);
				if (lrsz > 0 && lrsz < 4)
					for (unsigned int j = lrsz; j < 4; ++j)
						x[j] = x[lrsz-1];
			        r->operator()(u, v) = LayerInterpolateResult::LinInterp(x[0], x[1], x[2], x[3]);
			}
		}
		return r;
	}
	if (m_samesize) {
		unsigned int sz(r->get_size());
		for (unsigned int i = 0; i < sz; ++i) {
			LayerInterpolateResult::LinInterp z(0, 0, 0, 0);
			for (unsigned int j = 0; j < lrsz; ++j) {
				float y(m_lr[j]->operator[](i));
				z += LayerInterpolateResult::LinInterp(M(0, j) * y, M(1, j) * y, M(2, j) * y, M(3, j) * y);
			}
			r->operator[](i) = z;
		}
		return r;
	}
	for (unsigned int u = 0; u < w; ++u) {
		for (unsigned int v = 0; v < h; ++v) {
			LayerInterpolateResult::LinInterp z;
			{
				float y(m_lr.front()->operator()(u, v));
				z = LayerInterpolateResult::LinInterp(M(0, 0) * y, M(1, 0) * y, M(2, 0) * y, M(3, 0) * y);
			}
			Point pt(r->get_center(u, v));
			for (unsigned int j = 1; j < lrsz; ++j) {
				float y(m_lr[j]->operator()(pt));
				z += LayerInterpolateResult::LinInterp(M(0, j) * y, M(1, j) * y, M(2, j) * y, M(3, j) * y);
			}
			r->operator()(u, v) = z;
		}
	}
	return r;
}

boost::intrusive_ptr<GRIB2::LayerInterpolateResult> GRIB2::interpolate_results(const Rect& bbox, const layerlist_t& layers, gint64 efftime)
{
	Interpolate interp(bbox, layers);
	return interp.interpolate(efftime);
}

boost::intrusive_ptr<GRIB2::LayerInterpolateResult> GRIB2::interpolate_results(const Rect& bbox, const layerlist_t& layers, gint64 efftime, double sfc1value)
{
	Interpolate interp(bbox, layers);
	return interp.interpolate(efftime, sfc1value);
}

GRIB2::Surface2D::Surface2D(void)
{
}

void GRIB2::Surface2D::load(GRIB2& grib, WeatherProfile& wxprof, const Rect& bbox, gint64 efftime, uint64_t param, Layer::statproc_t stproc, uint8_t surface,
			    bool first, bool trace, double wptnr)
{
	if (!m_layer) {
		if (!first)
			return;
	} else {
		if (efftime >= m_layer->get_minefftime() && efftime <= m_layer->get_maxefftime())
			return;
	}
	const Parameter *par(find_parameter(param));
	layerlist_t ll(grib.find_layers(par, efftime, stproc, surface, 0));
	m_layer = interpolate_results(bbox, ll, efftime);
	if (m_layer) {
		wxprof.add_efftime(m_layer->get_minefftime());
		wxprof.add_efftime(m_layer->get_maxefftime());
		wxprof.add_reftime(m_layer->get_minreftime());
		wxprof.add_reftime(m_layer->get_maxreftime());
	}
	if (trace) {
		std::ostringstream wnr;
		wnr << std::fixed << std::setprecision(2) << wptnr;
		std::cerr << "Loading " << par->get_str_nonnull() << " (" << find_surfacetype_str(surface, "")
			  << "): wptnr " << wnr.str() << " ll " << ll.size() << " interp " << (m_layer ? "yes" : "no")
			  << " efftime " << Glib::TimeVal(efftime, 0).as_iso8601();
		if (m_layer)
			std::cerr << " (" << Glib::TimeVal(m_layer->get_minefftime(), 0).as_iso8601()
				  << ".." << Glib::TimeVal(m_layer->get_maxefftime(), 0).as_iso8601() << ')';
		std::cerr << std::endl;
	}
}

int32_t GRIB2::Surface2D::get_alt_hPa(const Point& pt, gint64 efftime) const
{
	if (!m_layer)
		return WeatherProfilePoint::invalidalt;
	float x(m_layer->operator()(pt, efftime, 0));
	if (!WeatherProfilePoint::is_pressure_valid(x))
		return WeatherProfilePoint::invalidalt;
	float alt(0);
	IcaoAtmosphere<float>::std_pressure_to_altitude(&alt, 0, x * 0.01);
	return Point::round<int,float>(alt * Point::m_to_ft);
}

int32_t GRIB2::Surface2D::get_alt_m(const Point& pt, gint64 efftime) const
{
	if (!m_layer)
		return WeatherProfilePoint::invalidalt;
	float x(m_layer->operator()(pt, efftime, 0));
	if (std::isnan(x))
		return WeatherProfilePoint::invalidalt;
	return Point::round<int,float>(x * Point::m_to_ft);
}

float GRIB2::Surface2D::get_float(const Point& pt, gint64 efftime, float scale) const
{
	if (!m_layer)
		return WeatherProfilePoint::invalidcover;
	float x(m_layer->operator()(pt, efftime, 0));
	if (std::isnan(x))
		return WeatherProfilePoint::invalidcover;
	return scale * x;
}

bool GRIB2::Surface2D::is_inrange(const Point& pt, gint64 efftime, float min, float max, bool dflt) const
{
	if (!m_layer)
		return dflt;
	float x(m_layer->operator()(pt, efftime, 0));
	if (std::isnan(x))
		return dflt;
	return x >= min && x <= max;
}

void GRIB2::Surface2D::get_models(WeatherProfile::models_t& m) const
{
	if (m_layer)
		m.insert(m_layer->get_model());	
}

GRIB2::Surface3D::Surface3D(void)
{
}

void GRIB2::Surface3D::load(GRIB2& grib, WeatherProfile& wxprof, const Rect& bbox, gint64 efftime, uint64_t param, Layer::statproc_t stproc, double gndlevel,
			    bool first, bool trace, double wptnr)
{
	static constexpr unsigned int nrsfc(sizeof(WeatherProfilePoint::isobaric_levels)/sizeof(WeatherProfilePoint::isobaric_levels[0]));
	{
		bool haslayer(false);
		bool isinside(true);
		for (unsigned int i = 0; i < nrsfc; ++i) {
			if (!m_layer[i])
				continue;
			haslayer = true;
			if (efftime >= m_layer[i]->get_minefftime() && efftime <= m_layer[i]->get_maxefftime())
				continue;
			isinside = false;
			break;
		}
		if (!haslayer) {
			if (!first)
				return;
		} else {
			if (isinside)
				return;
		}
	}
	const Parameter *par(find_parameter(param));
	for (unsigned int i = 0; i < nrsfc; ++i) {
		const int16_t isobarlvl(WeatherProfilePoint::isobaric_levels[i]);
		const uint8_t sfc((isobarlvl < 0) ? surface_specific_height_gnd : surface_isobaric_surface);
		double sfcval((isobarlvl < 0) ? gndlevel : isobarlvl * 100.0);
		layerlist_t ll;
		if (std::isnan(sfcval)) {
			m_layer[i].reset();
		} else {
			ll = grib.find_layers(par, efftime, stproc, sfc, sfcval);
			m_layer[i] = interpolate_results(bbox, ll, efftime, sfcval);
		}
		if (m_layer[i]) {
			wxprof.add_efftime(m_layer[i]->get_minefftime());
			wxprof.add_efftime(m_layer[i]->get_maxefftime());
			wxprof.add_reftime(m_layer[i]->get_minreftime());
			wxprof.add_reftime(m_layer[i]->get_maxreftime());
		}
		if (trace) {
			std::ostringstream wnr;
			wnr << std::fixed << std::setprecision(2) << wptnr;
			std::cerr << "Loading " << par->get_str_nonnull() << " (" << find_surfacetype_str(sfc, "") << ", " << sfcval
				  << "): wptnr " << wnr.str() << " sfc " << i << " ll " << ll.size() << " interp " << (m_layer[i] ? "yes" : "no")
				  << " efftime " << Glib::TimeVal(efftime, 0).as_iso8601()
				  << "  " << Glib::TimeVal(efftime, 0).as_iso8601();
			if (m_layer[i])
				std::cerr << " (" << Glib::TimeVal(m_layer[i]->get_minefftime(), 0).as_iso8601()
					  << ".." << Glib::TimeVal(m_layer[i]->get_maxefftime(), 0).as_iso8601() << ')';
			std::cerr << std::endl;
		}
	}
}

float GRIB2::Surface3D::get_float(const Point& pt, gint64 efftime, unsigned int idx, double gndlevel, float scale) const
{
	static constexpr unsigned int nrsfc(sizeof(WeatherProfilePoint::isobaric_levels)/sizeof(WeatherProfilePoint::isobaric_levels[0]));
	if (idx >= nrsfc || !m_layer[idx])
		return WeatherProfilePoint::invalidcover;
	const int16_t isobarlvl(WeatherProfilePoint::isobaric_levels[idx]);
	double sfc1value((isobarlvl < 0) ? gndlevel : isobarlvl * 100.0);
	if (std::isnan(sfc1value))
		return WeatherProfilePoint::invalidcover;
	float x(m_layer[idx]->operator()(pt, efftime, sfc1value));
	if (std::isnan(x))
		return WeatherProfilePoint::invalidcover;
	return scale * x;
}

void GRIB2::Surface3D::get_models(WeatherProfile::models_t& m) const
{
	static constexpr unsigned int nrsfc(sizeof(WeatherProfilePoint::isobaric_levels)/sizeof(WeatherProfilePoint::isobaric_levels[0]));
	for (unsigned int i = 0; i < nrsfc; ++i)
		if (m_layer[i])
			m.insert(m_layer[i]->get_model());	
}

GRIB2::Surface3DVector::Surface3DVector(void)
{
}

void GRIB2::Surface3DVector::load(GRIB2& grib, WeatherProfile& wxprof, const Rect& bbox, gint64 efftime, uint64_t paramx, uint64_t paramy, Layer::statproc_t stproc, double gndlevel,
			    bool first, bool trace, double wptnr)
{
	static constexpr unsigned int nrsfc(sizeof(WeatherProfilePoint::isobaric_levels)/sizeof(WeatherProfilePoint::isobaric_levels[0]));
	{
		bool haslayer(false);
		bool isinside(true);
		for (unsigned int i = 0; i < nrsfc; ++i) {
			for (unsigned int j = 0; j < 2; ++j) {
				if (!m_layer[i][j])
					continue;
				haslayer = true;
				if (efftime >= m_layer[i][j]->get_minefftime() && efftime <= m_layer[i][j]->get_maxefftime())
					continue;
				isinside = false;
				break;
			}
			if (!isinside)
				break;
		}
		if (!haslayer) {
			if (!first)
				return;
		} else {
			if (isinside)
				return;
		}
	}
	const Parameter *parx(find_parameter(paramx));
	const Parameter *pary(find_parameter(paramy));
	for (unsigned int i = 0; i < nrsfc; ++i) {
		const int16_t isobarlvl(WeatherProfilePoint::isobaric_levels[i]);
		const uint8_t sfc((isobarlvl < 0) ? surface_specific_height_gnd : surface_isobaric_surface);
		double sfcval((isobarlvl < 0) ? gndlevel : isobarlvl * 100.0);
		layerlist_t llx(grib.find_layers(parx, efftime, stproc, sfc, sfcval));
		layerlist_t lly(grib.find_layers(pary, efftime, stproc, sfc, sfcval));
		m_layer[i][0] = interpolate_results(bbox, llx, efftime, sfcval);
		m_layer[i][1] = interpolate_results(bbox, lly, efftime, sfcval);
		for (unsigned int j = 0; j < 2; ++j) {
			if (!m_layer[i][j])
				continue;
			wxprof.add_efftime(m_layer[i][j]->get_minefftime());
			wxprof.add_efftime(m_layer[i][j]->get_maxefftime());
			wxprof.add_reftime(m_layer[i][j]->get_minreftime());
			wxprof.add_reftime(m_layer[i][j]->get_maxreftime());
		}
		if (trace) {
			std::ostringstream wnr;
			wnr << std::fixed << std::setprecision(2) << wptnr;
			std::cerr << "Loading " << parx->get_str_nonnull() << '/' << pary->get_str_nonnull() << " (" << find_surfacetype_str(sfc, "") << ", " << sfcval
				  << "): wptnr " << wnr.str() << " sfc " << i << " ll " << llx.size() << '/' << lly.size()
				  << " interp " << (m_layer[i][0] ? "yes" : "no") << '/' << (m_layer[i][1] ? "yes" : "no")
				  << " efftime " << Glib::TimeVal(efftime, 0).as_iso8601()
				  << "  " << Glib::TimeVal(efftime, 0).as_iso8601();
			if (m_layer[i])
				std::cerr << " (" << Glib::TimeVal(std::max(m_layer[i][0]->get_minefftime(), m_layer[i][1]->get_minefftime()), 0).as_iso8601()
					  << ".." << Glib::TimeVal(std::min(m_layer[i][0]->get_maxefftime(), m_layer[i][1]->get_maxefftime()), 0).as_iso8601() << ')';
			std::cerr << std::endl;
		}
	}
}

std::pair<float,float> GRIB2::Surface3DVector::get_vector(const Point& pt, gint64 efftime, unsigned int idx, double gndlevel) const
{
	static constexpr unsigned int nrsfc(sizeof(WeatherProfilePoint::isobaric_levels)/sizeof(WeatherProfilePoint::isobaric_levels[0]));
	if (idx >= nrsfc || !m_layer[idx][0] || !m_layer[idx][1])
		return std::pair<float,float>(WeatherProfilePoint::invalidcover, WeatherProfilePoint::invalidcover);
	const int16_t isobarlvl(WeatherProfilePoint::isobaric_levels[idx]);
	double sfc1value((isobarlvl < 0) ? gndlevel : isobarlvl * 100.0);
	if (std::isnan(sfc1value))
		return std::pair<float,float>(WeatherProfilePoint::invalidcover, WeatherProfilePoint::invalidcover);
	float u(m_layer[idx][0]->operator()(pt, efftime, sfc1value));
	float v(m_layer[idx][1]->operator()(pt, efftime, sfc1value));
	if (std::isnan(u) || std::isnan(v))
		return std::pair<float,float>(WeatherProfilePoint::invalidcover, WeatherProfilePoint::invalidcover);
	std::pair<float,float> wnd(m_layer[idx][0]->get_layer()->get_grid()->transform_axes(u, v));
	if (std::isnan(wnd.first) || std::isnan(wnd.second))
		return std::pair<float,float>(WeatherProfilePoint::invalidcover, WeatherProfilePoint::invalidcover);
	return wnd;
}

void GRIB2::Surface3DVector::get_models(WeatherProfile::models_t& m) const
{
	static constexpr unsigned int nrsfc(sizeof(WeatherProfilePoint::isobaric_levels)/sizeof(WeatherProfilePoint::isobaric_levels[0]));
	for (unsigned int i = 0; i < nrsfc; ++i)
		for (unsigned int j = 0; j < 2; ++j)
			if (m_layer[i][j])
				m.insert(m_layer[i][j]->get_model());	
}

GRIB2::WeatherProfile GRIB2::get_profile(const FPlanRoute& fpl)
{
	static constexpr unsigned int nrsfc(sizeof(WeatherProfilePoint::isobaric_levels)/sizeof(WeatherProfilePoint::isobaric_levels[0]));
	static constexpr bool trace_loads(false);
	Surface2D wxzerodegisotherm;
	Surface2D wxtropopause;
	Surface2D wxtropopauseh;
	Surface2D wxcldbdrycover;
	Surface2D wxbdrylayerheight;
	Surface2D wxcldlowcover;
	Surface2D wxcldlowbase;
	Surface2D wxcldlowtop;
	Surface2D wxcldmidcover;
	Surface2D wxcldmidbase;
	Surface2D wxcldmidtop;
	Surface2D wxcldhighcover;
	Surface2D wxcldhighbase;
	Surface2D wxcldhightop;
	Surface2D wxcldconvcover;
	Surface2D wxcldconvbase;
	Surface2D wxcldconvtop;
	Surface2D wxcldconvbaseh;
	Surface2D wxcldconvtoph;
	Surface2D wxcldconvhorext;
	Surface2D wxprecip;
	Surface2D wxpreciprate;
	Surface2D wxconvprecip;
	Surface2D wxconvpreciprate;
	Surface2D wxgridscalerain;
	Surface2D wxgridscalesnow;
	Surface2D wxconvrain;
	Surface2D wxconvsnow;
	Surface2D wxcrain;
	Surface2D wxcfrzr;
	Surface2D wxcicep;
	Surface2D wxcsnow;
	Surface2D wxlft;
	Surface2D wxcape;
	Surface2D wxcin;
	Surface2D wxprmsl;
	Surface2D wxwiww;
	Surface3D wxtemp;
	Surface3DVector wxuvgrd;
	Surface3D wxrelhum;
	Surface3D wxcldcover;
	Surface3D wxicingdeg;
	Surface3D wxeddydissr;
	Rect bbox(fpl.get_bbox().oversize_nmi(100.f));
	double dist(0);
	WeatherProfile wxprof;
	for (unsigned int wptnr = 1U, nrwpt = fpl.get_nrwpt(); wptnr < nrwpt; ++wptnr) {
		const FPlanWaypoint& wpt0(fpl[wptnr - 1U]);
		const FPlanWaypoint& wpt1(fpl[wptnr]);
		double distleg(wpt0.get_coord().spheric_distance_nmi_dbl(wpt1.get_coord()));
		gint64 timeorig(fpl[0].get_time_unix() + wpt0.get_flighttime());
		gint64 timediff(wpt1.get_flighttime() - (gint64)wpt0.get_flighttime());
		double tinc(1.0);
		if (distleg > 0)
			tinc = 1.0 / distleg;
		else if (timediff > 0)
			tinc = 600.0 / timediff;
		else
			continue;
		tinc = std::max(tinc, 1e-3);
		Point ptorig(wpt0.get_coord());
		Point ptdiff(wpt1.get_coord() - ptorig);
		int32_t altorig(wpt0.get_altitude());
		int32_t altdiff(wpt1.get_altitude() - wpt0.get_altitude());
		for (double t = 0;; t += tinc) {
			if (t >= 1.0) {
				if (wptnr + 1 < nrwpt)
					break;
				t = 1.0;
			}
			if (false)
				std::cerr << "WX profile: wpt " << wptnr << '/' << nrwpt << " t " << t << std::endl;
			Point pt(Point(ptdiff.get_lon() * t, ptdiff.get_lat() * t) + ptorig);
			gint64 efftime(timeorig + t * timediff);
			int32_t alt(altorig + t * altdiff);
			bool first(wptnr == 1U && t == 0);
			double wptnrf(wptnr - 1U + t);
			wxzerodegisotherm.load(*this, wxprof, bbox, efftime, param_meteorology_mass_hgt, Layer::statproc_t::none, surface_0degc_isotherm, first, trace_loads, wptnrf);
			wxtropopause.load(*this, wxprof, bbox, efftime, param_meteorology_mass_hgt, Layer::statproc_t::none, surface_tropopause, first, trace_loads, wptnrf);
			wxtropopauseh.load(*this, wxprof, bbox, efftime, param_meteorology_mass_icaht, Layer::statproc_t::none, surface_tropopause, first, trace_loads, wptnrf);
			wxcldbdrycover.load(*this, wxprof, bbox, efftime, param_meteorology_cloud_tcdc, Layer::statproc_t::none, surface_boundary_layer_cloud, first, trace_loads, wptnrf);
			wxbdrylayerheight.load(*this, wxprof, bbox, efftime, param_meteorology_mass_hpbl_2, Layer::statproc_t::none, surface_ground_or_water, first, trace_loads, wptnrf);
			wxcldlowcover.load(*this, wxprof, bbox, efftime, param_meteorology_cloud_tcdc, Layer::statproc_t::none, surface_low_cloud, first, trace_loads, wptnrf);
			wxcldlowbase.load(*this, wxprof, bbox, efftime, param_meteorology_mass_pres, Layer::statproc_t::none, surface_low_cloud_bottom, first, trace_loads, wptnrf);
			wxcldlowtop.load(*this, wxprof, bbox, efftime, param_meteorology_mass_pres, Layer::statproc_t::none, surface_low_cloud_top, first, trace_loads, wptnrf);
			wxcldmidcover.load(*this, wxprof, bbox, efftime, param_meteorology_cloud_tcdc, Layer::statproc_t::none, surface_middle_cloud, first, trace_loads, wptnrf);
			wxcldmidbase.load(*this, wxprof, bbox, efftime, param_meteorology_mass_pres, Layer::statproc_t::none, surface_middle_cloud_bottom, first, trace_loads, wptnrf);
			wxcldmidtop.load(*this, wxprof, bbox, efftime, param_meteorology_mass_pres, Layer::statproc_t::none, surface_middle_cloud_top, first, trace_loads, wptnrf);
			wxcldhighcover.load(*this, wxprof, bbox, efftime, param_meteorology_cloud_tcdc, Layer::statproc_t::none, surface_top_cloud, first, trace_loads, wptnrf);
			wxcldhighbase.load(*this, wxprof, bbox, efftime, param_meteorology_mass_pres, Layer::statproc_t::none, surface_top_cloud_bottom, first, trace_loads, wptnrf);
			wxcldhightop.load(*this, wxprof, bbox, efftime, param_meteorology_mass_pres, Layer::statproc_t::none, surface_top_cloud_top, first, trace_loads, wptnrf);
			wxcldconvcover.load(*this, wxprof, bbox, efftime, param_meteorology_cloud_tcdc, Layer::statproc_t::none, surface_convective_cloud, first, trace_loads, wptnrf);
			wxcldconvbase.load(*this, wxprof, bbox, efftime, param_meteorology_mass_pres, Layer::statproc_t::none, surface_convective_cloud_bottom, first, trace_loads, wptnrf);
			wxcldconvtop.load(*this, wxprof, bbox, efftime, param_meteorology_mass_pres, Layer::statproc_t::none, surface_convective_cloud_top, first, trace_loads, wptnrf);
			wxcldconvbaseh.load(*this, wxprof, bbox, efftime, param_meteorology_mass_icaht, Layer::statproc_t::none, surface_cloud_base, first, trace_loads, wptnrf);
			wxcldconvtoph.load(*this, wxprof, bbox, efftime, param_meteorology_mass_icaht, Layer::statproc_t::none, surface_cloud_top, first, trace_loads, wptnrf);
			wxcldconvhorext.load(*this, wxprof, bbox, efftime, param_meteorology_cloud_cbhe, Layer::statproc_t::none, surface_ground_or_water, first, trace_loads, wptnrf);
			wxprecip.load(*this, wxprof, bbox, efftime, param_meteorology_moisture_apcp, Layer::statproc_t::none, surface_ground_or_water, first, trace_loads, wptnrf);
			wxpreciprate.load(*this, wxprof, bbox, efftime, param_meteorology_moisture_prate, Layer::statproc_t::none, surface_ground_or_water, first, trace_loads, wptnrf);
			wxconvprecip.load(*this, wxprof, bbox, efftime, param_meteorology_moisture_acpcp, Layer::statproc_t::none, surface_ground_or_water, first, trace_loads, wptnrf);
			wxconvpreciprate.load(*this, wxprof, bbox, efftime, param_meteorology_moisture_cprat_2, Layer::statproc_t::none, surface_ground_or_water, first, trace_loads, wptnrf);
			wxgridscalerain.load(*this, wxprof, bbox, efftime, param_meteorology_moisture_lsrrate, Layer::statproc_t::none, surface_ground_or_water, first, trace_loads, wptnrf);
			wxgridscalesnow.load(*this, wxprof, bbox, efftime, param_meteorology_moisture_lssrwe, Layer::statproc_t::none, surface_ground_or_water, first, trace_loads, wptnrf);
			wxconvrain.load(*this, wxprof, bbox, efftime, param_meteorology_moisture_crrate, Layer::statproc_t::none, surface_ground_or_water, first, trace_loads, wptnrf);
			wxconvsnow.load(*this, wxprof, bbox, efftime, param_meteorology_moisture_csrwe, Layer::statproc_t::none, surface_ground_or_water, first, trace_loads, wptnrf);
			wxcrain.load(*this, wxprof, bbox, efftime, param_meteorology_moisture_crain_2, Layer::statproc_t::none, surface_ground_or_water, first, trace_loads, wptnrf);
			wxcfrzr.load(*this, wxprof, bbox, efftime, param_meteorology_moisture_cfrzr_2, Layer::statproc_t::none, surface_ground_or_water, first, trace_loads, wptnrf);
			wxcicep.load(*this, wxprof, bbox, efftime, param_meteorology_moisture_cicep_2, Layer::statproc_t::none, surface_ground_or_water, first, trace_loads, wptnrf);
			wxcsnow.load(*this, wxprof, bbox, efftime, param_meteorology_moisture_csnow_2, Layer::statproc_t::none, surface_ground_or_water, first, trace_loads, wptnrf);
			wxlft.load(*this, wxprof, bbox, efftime, param_meteorology_thermodynamic_stability_lftx_2, Layer::statproc_t::none, surface_ground_or_water, first, trace_loads, wptnrf);
			wxcape.load(*this, wxprof, bbox, efftime, param_meteorology_thermodynamic_stability_cape, Layer::statproc_t::none, surface_ground_or_water, first, trace_loads, wptnrf);
			wxcin.load(*this, wxprof, bbox, efftime, param_meteorology_thermodynamic_stability_cin, Layer::statproc_t::none, surface_ground_or_water, first, trace_loads, wptnrf);
			wxprmsl.load(*this, wxprof, bbox, efftime, param_meteorology_mass_prmsl, Layer::statproc_t::none, surface_mean_sea_level, first, trace_loads, wptnrf);
			wxwiww.load(*this, wxprof, bbox, efftime, param_meteorology_atmosphere_physics_wiww, Layer::statproc_t::none, surface_ground_or_water, first, trace_loads, wptnrf);
			wxtemp.load(*this, wxprof, bbox, efftime, param_meteorology_temperature_tmp, Layer::statproc_t::none, 2, first, trace_loads, wptnrf);
			wxuvgrd.load(*this, wxprof, bbox, efftime, param_meteorology_momentum_ugrd, param_meteorology_momentum_vgrd, Layer::statproc_t::none, 10, first, trace_loads, wptnrf);
			wxrelhum.load(*this, wxprof, bbox, efftime, param_meteorology_moisture_rh, Layer::statproc_t::none, 2, first, trace_loads, wptnrf);
			wxcldcover.load(*this, wxprof, bbox, efftime, param_meteorology_cloud_cdcc, Layer::statproc_t::none, std::numeric_limits<double>::quiet_NaN(), first, trace_loads, wptnrf);
			wxicingdeg.load(*this, wxprof, bbox, efftime, param_meteorology_atmosphere_physics_dwdwawfor_icingdegree, Layer::statproc_t::none, std::numeric_limits<double>::quiet_NaN(), first, trace_loads, wptnrf);
			wxeddydissr.load(*this, wxprof, bbox, efftime, param_meteorology_atmosphere_physics_dwdwawfor_eddydissrate, Layer::statproc_t::none, std::numeric_limits<double>::quiet_NaN(), first, trace_loads, wptnrf);
			if (first) {
				wxzerodegisotherm.get_models(wxprof.get_models());
				wxtropopause.get_models(wxprof.get_models());
				wxtropopauseh.get_models(wxprof.get_models());
				wxcldbdrycover.get_models(wxprof.get_models());
				wxbdrylayerheight.get_models(wxprof.get_models());
				wxcldlowcover.get_models(wxprof.get_models());
				wxcldlowbase.get_models(wxprof.get_models());
				wxcldlowtop.get_models(wxprof.get_models());
				wxcldmidcover.get_models(wxprof.get_models());
				wxcldmidbase.get_models(wxprof.get_models());
				wxcldmidtop.get_models(wxprof.get_models());
				wxcldhighcover.get_models(wxprof.get_models());
				wxcldhighbase.get_models(wxprof.get_models());
				wxcldhightop.get_models(wxprof.get_models());
				wxcldconvcover.get_models(wxprof.get_models());
				wxcldconvbase.get_models(wxprof.get_models());
				wxcldconvtop.get_models(wxprof.get_models());
				wxcldconvbaseh.get_models(wxprof.get_models());
				wxcldconvtoph.get_models(wxprof.get_models());
				wxcldconvhorext.get_models(wxprof.get_models());
				wxprecip.get_models(wxprof.get_models());
				wxpreciprate.get_models(wxprof.get_models());
				wxconvprecip.get_models(wxprof.get_models());
				wxconvpreciprate.get_models(wxprof.get_models());
				wxgridscalerain.get_models(wxprof.get_models());
				wxgridscalesnow.get_models(wxprof.get_models());
				wxconvrain.get_models(wxprof.get_models());
				wxconvsnow.get_models(wxprof.get_models());
				wxcrain.get_models(wxprof.get_models());
				wxcfrzr.get_models(wxprof.get_models());
				wxcicep.get_models(wxprof.get_models());
				wxcsnow.get_models(wxprof.get_models());
				wxlft.get_models(wxprof.get_models());
				wxcape.get_models(wxprof.get_models());
				wxcin.get_models(wxprof.get_models());
				wxprmsl.get_models(wxprof.get_models());
				wxwiww.get_models(wxprof.get_models());
				wxtemp.get_models(wxprof.get_models());
				wxuvgrd.get_models(wxprof.get_models());
				wxrelhum.get_models(wxprof.get_models());
				wxcldcover.get_models(wxprof.get_models());
				wxicingdeg.get_models(wxprof.get_models());
				wxeddydissr.get_models(wxprof.get_models());
			}

			int32_t zerodegisotherm(wxzerodegisotherm.get_alt_m(pt, efftime));
			int32_t tropopause(wxtropopause.get_alt_m(pt, efftime));
			if (tropopause == WeatherProfilePoint::invalidalt)
				tropopause = wxtropopauseh.get_alt_m(pt, efftime);
			float cldbdrycover(wxcldbdrycover.get_float(pt, efftime, 0.01));
			int32_t bdrylayerheight(wxbdrylayerheight.get_alt_m(pt, efftime));
			float cldlowcover(wxcldlowcover.get_float(pt, efftime, 0.01));
			int32_t cldlowbase(wxcldlowbase.get_alt_hPa(pt, efftime));
			int32_t cldlowtop(wxcldlowtop.get_alt_hPa(pt, efftime));
			float cldmidcover(wxcldmidcover.get_float(pt, efftime, 0.01));
			int32_t cldmidbase(wxcldmidbase.get_alt_hPa(pt, efftime));
			int32_t cldmidtop(wxcldmidtop.get_alt_hPa(pt, efftime));
			float cldhighcover(wxcldhighcover.get_float(pt, efftime, 0.01));
			int32_t cldhighbase(wxcldhighbase.get_alt_hPa(pt, efftime));
			int32_t cldhightop(wxcldhightop.get_alt_hPa(pt, efftime));
			float cldconvcover(wxcldconvcover.get_float(pt, efftime, 0.01));
			int32_t cldconvbase(wxcldconvbase.get_alt_hPa(pt, efftime));
			int32_t cldconvtop(wxcldconvtop.get_alt_hPa(pt, efftime));
			if (std::isnan(cldconvcover))
				cldconvcover = wxcldconvhorext.get_float(pt, efftime, 0.01);
			if (cldconvbase == WeatherProfilePoint::invalidalt)
				cldconvbase = wxcldconvbaseh.get_alt_m(pt, efftime);
			if (cldconvtop == WeatherProfilePoint::invalidalt)
				cldconvtop = wxcldconvtoph.get_alt_m(pt, efftime);
			float precip(wxprecip.get_float(pt, efftime, 1));
			float preciprate(wxpreciprate.get_float(pt, efftime, 1));
			float convprecip(wxconvprecip.get_float(pt, efftime, 1));
			float convpreciprate(wxconvpreciprate.get_float(pt, efftime, 1));
			uint16_t flags = 0;
			if (wxcrain.is_inrange(pt, efftime, 0.5, 10, false))
				flags |= WeatherProfilePoint::flags_rain;
			if (wxcfrzr.is_inrange(pt, efftime, 0.5, 10, false))
				flags |= WeatherProfilePoint::flags_freezingrain;
			if (wxcicep.is_inrange(pt, efftime, 0.5, 10, false))
				flags |= WeatherProfilePoint::flags_icepellets;
			if (wxcsnow.is_inrange(pt, efftime, 0.5, 10, false))
				flags |= WeatherProfilePoint::flags_snow;
			if (std::isnan(preciprate)) {
				// FIXME: is this /h or /s? techndocu says /h, but grib file says /s
				float gridscalerain(wxgridscalerain.get_float(pt, efftime, 1./3600.));
				float gridscalesnow(wxgridscalesnow.get_float(pt, efftime, 1./3600.));
				if (!std::isnan(gridscalerain) || !std::isnan(gridscalesnow)) {
					preciprate = 0;
					if (!std::isnan(gridscalerain) && gridscalerain > 0) {
						preciprate += gridscalerain;
						flags |= WeatherProfilePoint::flags_rain;
					}
					if (!std::isnan(gridscalesnow) && gridscalesnow > 0) {
						preciprate += gridscalesnow;
						flags |= WeatherProfilePoint::flags_snow;
					}
				}
			}
			float lft(wxlft.get_float(pt, efftime, 1));
			float cape(wxcape.get_float(pt, efftime, 1));
			float cin(wxcin.get_float(pt, efftime, 1));
			float qff(wxprmsl.get_float(pt, efftime, 0.01));
			// flags
			{
				// Table 0 20 003
				// https://www.wmo.int/pages/prog/www/WMOCodes/WMO306_vI2/LatestVERSION/WMO306_vI2_BUFRCREX_CodeFlag_en.pdf
				float flagsf(wxwiww.get_float(pt, efftime, 1));
				if (!std::isnan(flagsf)) {
					int flagsi(Point::round<int,float>(flagsf));
					if (false)
						std::cerr << "W" << wptnrf << " WIWW " << flagsf << " (" << flagsi << ')' << std::endl;
					switch (flagsi) {
					case 4: // Visibility reduced by smoke, e.g. veldt or forest fires, industrial smoke or volcanic ashes
						if (std::isnan(cldbdrycover))
							cldbdrycover = 0.3;
						if (bdrylayerheight == WeatherProfilePoint::invalidalt)
							bdrylayerheight = 200;
						break;

					case 5: // Haze
						if (std::isnan(cldbdrycover))
							cldbdrycover = 0.4;
						if (bdrylayerheight == WeatherProfilePoint::invalidalt)
							bdrylayerheight = 200;
						break;

					case 10: // Mist
					case 110: // Mist
						if (std::isnan(cldbdrycover))
							cldbdrycover = 0.3;
						if (bdrylayerheight == WeatherProfilePoint::invalidalt)
							bdrylayerheight = 200;
						break;

					case 11: // Patches of shallow fog
						if (std::isnan(cldbdrycover))
							cldbdrycover = 0.3;
						if (bdrylayerheight == WeatherProfilePoint::invalidalt)
							bdrylayerheight = 6;
						break;

					case 12: // Continuous shallow fog
						if (std::isnan(cldbdrycover))
							cldbdrycover = 1;
						if (bdrylayerheight == WeatherProfilePoint::invalidalt)
							bdrylayerheight = 6;
						break;

					case 28: // Fog or ice fog
					case 43: // Fog or ice fog, sky invisible
					case 45: // Fog or ice fog, sky invisible
					case 47: // Fog or ice fog, sky invisible
					case 49: // Fog, depositing rime, sky invisible
					case 120: // Fog
					case 130: // FOG
					case 132: // Fog or ice fog, has become thinner during the past hour
					case 133: // Fog or ice fog, no appreciable change during the past hour
					case 134: // Fog or ice fog, has begun or become thicker during the past hour
					case 135: // Fog, depositing rime
						if (std::isnan(cldbdrycover))
							cldbdrycover = 1;
						if (bdrylayerheight == WeatherProfilePoint::invalidalt)
							bdrylayerheight = 200;
						break;
						
					case 42: // Fog or ice fog, sky visible
					case 44: // Fog or ice fog, sky visible
					case 46: // Fog or ice fog, sky visible
					case 48: // Fog, depositing rime, sky visible
						if (std::isnan(cldbdrycover))
							cldbdrycover = 1;
						if (bdrylayerheight == WeatherProfilePoint::invalidalt)
							bdrylayerheight = 30;
						break;

					case 41: // Fog or ice fog in patches
					case 131: // Fog or ice fog in patches
						if (std::isnan(cldbdrycover))
							cldbdrycover = 0.3;
						if (bdrylayerheight == WeatherProfilePoint::invalidalt)
							bdrylayerheight = 200;
						break;

					case 104: // Haze or smoke, or dust in suspension in the air, visibility equal to, or greater than, 1 km
					case 128: // Blowing or drifting snow or sand, visibility equal to, or greater than, 1 km
						if (std::isnan(cldbdrycover))
							cldbdrycover = 0.2;
						if (bdrylayerheight == WeatherProfilePoint::invalidalt)
							bdrylayerheight = 200;
						break;

					case 105: // Haze or smoke, or dust in suspension in the air, visibility less than 1 km
					case 129: // Blowing or drifting snow or sand, visibility less than 1 km
						if (std::isnan(cldbdrycover))
							cldbdrycover = 0.6;
						if (bdrylayerheight == WeatherProfilePoint::invalidalt)
							bdrylayerheight = 200;
						break;

					case 206: // Thick dust haze, visibility less than 1 km
						if (std::isnan(cldbdrycover))
							cldbdrycover = 0.8;
						if (bdrylayerheight == WeatherProfilePoint::invalidalt)
							bdrylayerheight = 200;
						break;

					case 207: // Blowing spray at the station
						if (std::isnan(cldbdrycover))
							cldbdrycover = 0.5;
						if (bdrylayerheight == WeatherProfilePoint::invalidalt)
							bdrylayerheight = 20;
						break;

					case 6: // Widespread dust in suspension in the air, not raised by wind at or near the station at the time of observation
					case 7: // Dust or sand raised by wind at or near the station at the time of observation, but no well-developed
						// dust whirl(s) or sand whirl(s), and no duststorm or sandstorm seen; or,
						// in the case of sea stations and coastal stations, blowing spray at the station
					case 8: // Well-developed dust whirl(s) or sand whirl(s) seen at or near the station during the preceding hour or$
						// at the same time of observation, but no duststorm or sandstorm
					case 9: // Duststorm or sandstorm within sight at the time of observation, or at the station during the preceding hour
					case 30: // Slight or moderate duststorm orsandstorm
					case 31: // Slight or moderate duststorm orsandstorm
					case 32: // Slight or moderate duststorm orsandstorm
					case 33: // Severe duststorm or sandstorm
					case 34: // Severe duststorm or sandstorm
					case 35: // Severe duststorm or sandstorm
					case 111: // Diamond dust
					case 127: // BLOWING OR DRIFTING SNOW OR SAND
					case 204: // Volcanic ash suspended in the air aloft
					case 208: // Drifting dust (sand)
						flags |= WeatherProfilePoint::flags_sand;
						break;

					case 13: // Lightning
					case 17: // Thunderstorm
					case 18: // Squalls
					case 29: // Thunderstorm
					case 118: // Squalls
					case 126: // Thunderstorm (with or without precipitation)
					case 190: // THUNDERSTORM
					case 191: // Thunderstorm, slight or moderate, with no precipitation
					case 194: // Thunderstorm, heavy, with no precipitation
					case 213: // Lightning, cloud to surface
					case 217: // Dry thunderstorm
						flags |= WeatherProfilePoint::flags_thunderstorm;
						break;

					case 95: // Thunderstorm, slight or moderate, without hail*, but with rain and/or snow at time of observation
					case 97: // Thunderstorm, heavy, without hail*, but with rain and/or snow at time of observation time of observation
					case 192: // Thunderstorm, slight or moderate, with rain showers and/or snow showers
					case 195: // Thunderstorm, heavy, with rain showers and/or snow showers
						flags |= WeatherProfilePoint::flags_thunderstorm | WeatherProfilePoint::flags_rain;
						break;

					case 96: // Thunderstorm, slight or moderate, with hail* at time of observation
					case 99: // Thunderstorm, heavy, with hail* at time of observation
					case 193: // Thunderstorm, slight or moderate, with hail
					case 196: // Thunderstorm, heavy, with hail
						flags |= WeatherProfilePoint::flags_thunderstorm | WeatherProfilePoint::flags_icepellets;
						break;

					case 98: // Thunderstorm combined with duststorm or sandstorm at time of observation
						flags |= WeatherProfilePoint::flags_thunderstorm | WeatherProfilePoint::flags_sand;
						break;

					case 14: // Precipitation
					case 21: // Rain
					case 25: // Showers of rain
					case 60: // Rain, not freezing, intermittent
					case 61: // Rain, not freezing, continuous
					case 62: // Rain, not freezing, intermittent
					case 63: // Rain, not freezing, continuous
					case 64: // Rain, not freezing, intermittent
					case 65: // Rain, not freezing, continuous
					case 66: // Rain, freezing, slight
					case 67: // Rain, freezing, moderate or heavy
 					case 80: // Rain shower(s), slight
					case 81: // Rain shower(s), moderate or heavy
					case 82: // Rain shower(s), violent
					case 91: // Slight rain at time of observation
					case 92: // Moderate or heavy rain at time of observation
					case 121: // PRECIPITATION
					case 123: // Rain (not freezing)
					case 140: // PRECIPITATION
					case 141: // Precipitation, slight or moderate
					case 142: // Precipitation, heavy
					case 143: // Liquid precipitation, slight or moderate
					case 144: // Liquid precipitation, heavy
					case 160: // RAIN
					case 161: // Rain, not freezing, slight
					case 162: // Rain, not freezing, moderate
					case 163: // Rain, not freezing, heavy
					case 180: // SHOWER(S) OR INTERMITTENT PRECIPITATION
					case 181: // Rain shower(s) or intermittent rain, slight
					case 182: // Rain shower(s) or intermittent rain, moderate
					case 183: // Rain shower(s) or intermittent rain, heavy
					case 184: // Rain shower(s) or intermittent rain, violent
						flags |= WeatherProfilePoint::flags_rain;
						break;

					case 26: // Showers of snow
					case 36: // Slight or moderate drifting snow
					case 37: // Heavy drifting snow
					case 38: // Slight or moderate blowing snow
					case 39: // Heavy blowing snow
					case 70: // Intermittent fall of snowflakes
					case 71: // Continuous fall of snowflakes
					case 72: // Intermittent fall of snowflakes
					case 73: // Continuous fall of snowflakes
					case 74: // Intermittent fall of snowflakes
					case 75: // Continuous fall of snowflakes
					case 76: // Diamond dust (with or without fog)
					case 77: // Snow grains (with or without fog)
					case 83: // Shower(s) of rain and snow mixed, slight
					case 84: // Shower(s) of rain and snow mixed, moderate or heavy
					case 85: // Snow shower(s), slight
					case 86: // Snow shower(s), moderate or heavy
					case 87: // Shower(s) of snow pellets or small hail, with or without rain or rain and snow mixed
					case 88: // Shower(s) of snow pellets or small hail, with or without rain or rain and snow mixed
					case 93: // Slight snow, or rain and snow mixed or hail* at time of observation
					case 94: // Moderate or heavy snow, or rain and snow mixed or hail* at time of observation
					case 124: // Snow
					case 145: // Solid precipitation, slight or moderate
					case 146: // Solid precipitation, heavy
					case 170: // SNOW
					case 171: // Snow, slight
					case 172: // Snow, moderate
					case 173: // Snow, heavy
					case 177: // Snow grains
					case 185: // Snow shower(s) or intermittent snow, slight
					case 186: // Snow shower(s) or intermittent snow, moderate
					case 187: // Snow shower(s) or intermittent snow, heavy
						flags |= WeatherProfilePoint::flags_snow;
						break;


					case 199: // Tornado
						flags |= WeatherProfilePoint::flags_tornado;
						break;

					case 78: // Isolated star-like snow crystals (with or without fog)
					case 79: // Ice pellets
					case 89: // Shower(s) of hail, with or without rain or rain and snow mixed, not associated with thunder
					case 90: // Shower(s) of hail, with or without rain or rain and snow mixed, not associated with thunder
					case 174: // Ice pellets, slight
					case 175: // Ice pellets, moderate
					case 176: // Ice pellets, heavy
					case 178: // Ice crystals
					case 189: // Hail
						flags |= WeatherProfilePoint::flags_icepellets;
						break;

					case 24: // Freezing drizzle or freezing rain
					case 68: // Rain or drizzle and snow, slight
					case 69: // Rain or drizzle and snow, moderate or heavy
					case 125: // Freezing drizzle or freezing rain
					case 147: // Freezing precipitation, slight or moderate
					case 148: // Freezing precipitation, heavy
					case 164: // Rain, freezing, slight
					case 165: // Rain, freezing, moderate
					case 166: // Rain, freezing, heavy
						flags |= WeatherProfilePoint::flags_freezingrain;
						break;

					case 167: // Rain (or drizzle) and snow, slight
					case 168: // Rain (or drizzle) and snow, moderate or heavy
						flags |= WeatherProfilePoint::flags_rain | WeatherProfilePoint::flags_snow;
						break;

					case 23: // Rain and snow or ice pellets
						flags |= WeatherProfilePoint::flags_rain | WeatherProfilePoint::flags_snow | WeatherProfilePoint::flags_icepellets;
						break;

					default:
						break;
					}
				}
			}
			// day/night
			{
				Glib::DateTime dt(Glib::DateTime::create_now_utc(efftime));
				double sr, ss, twr, tws;
				int rss(SunriseSunset::sun_rise_set(dt.get_year(), dt.get_month(), dt.get_day_of_month(), pt, sr, ss));
				int rtwil(SunriseSunset::civil_twilight(dt.get_year(), dt.get_month(), dt.get_day_of_month(), pt, twr, tws));
				if (rss || rtwil) {
					int r(rtwil ? rtwil : rss);
					if (r < 0)
						flags |= WeatherProfilePoint::flags_night;
					else
						flags |= WeatherProfilePoint::flags_day;
				} else {
					int xtwr(twr * 3600), xtws(tws * 3600), xsr(sr * 3600), xss(ss * 3600);
					int x(dt.get_second() + 60 * (dt.get_minute() + 60 * dt.get_hour()));
					while (xtwr < 0)
						xtwr += 24 * 60 * 60;
					while (xsr < xtwr)
						xsr += 24 * 60 * 60;
					while (xss < xsr)
						xss += 24 * 60 * 60;
					while (xtws < xss)
						xtws += 24 * 60 * 60;
					while (x < xtwr)
						x += 24 * 60 * 60;
					if (x >= xsr && x <= xss)
						flags |= WeatherProfilePoint::flags_day;
					else if (x < xsr)
						flags |= WeatherProfilePoint::flags_dusk;
					else if (x < xtws)
						flags |= WeatherProfilePoint::flags_dawn;
					else
						flags |= WeatherProfilePoint::flags_night;
					if (false)
						std::cerr << "SR/SS: time (" << (x / (24*60*60)) << ')'
							  << std::setw(2) << std::setfill('0') << ((x / (60*60)) % 24) << ':'
							  << std::setw(2) << std::setfill('0') << ((x / 60) % 60) << ':'
							  << std::setw(2) << std::setfill('0') << (x % 60) << " day ("
							  << (xtwr / (24*60*60)) << ')'
							  << std::setw(2) << std::setfill('0') << ((xtwr / (60*60)) % 24) << ':'
							  << std::setw(2) << std::setfill('0') << ((xtwr / 60) % 60) << ':'
							  << std::setw(2) << std::setfill('0') << (xtwr % 60) << " ("
							  << (xsr / (24*60*60)) << ')'
							  << std::setw(2) << std::setfill('0') << ((xsr / (60*60)) % 24) << ':'
							  << std::setw(2) << std::setfill('0') << ((xsr / 60) % 60) << ':'
							  << std::setw(2) << std::setfill('0') << (xsr % 60) << " ("
							  << (xss / (24*60*60)) << ')'
							  << std::setw(2) << std::setfill('0') << ((xss / (60*60)) % 24) << ':'
							  << std::setw(2) << std::setfill('0') << ((xss / 60) % 60) << ':'
							  << std::setw(2) << std::setfill('0') << (xss % 60) << " ("
							  << (xtws / (24*60*60)) << ')'
							  << std::setw(2) << std::setfill('0') << ((xtws / (60*60)) % 24) << ':'
							  << std::setw(2) << std::setfill('0') << ((xtws / 60) % 60) << ':'
							  << std::setw(2) << std::setfill('0') << (xtws % 60) << " flags "
							  << (flags & WeatherProfilePoint::flags_daymask) << std::endl;
				}
			}
			double ldist(t * distleg);
			double rdist(dist + ldist);
			unsigned int wnr(wptnr - 1);
			if (t == 1.0) {
				ldist = 0;
				++wnr;
			}
			wxprof.push_back(WeatherProfilePoint(ldist, rdist, wnr, pt, efftime, alt,
							     zerodegisotherm, tropopause,
							     cldbdrycover, bdrylayerheight,
							     cldlowcover, cldlowbase, cldlowtop,
							     cldmidcover, cldmidbase, cldmidtop,
							     cldhighcover, cldhighbase, cldhightop,
							     cldconvcover, cldconvbase, cldconvtop,
							     precip, preciprate, convprecip, convpreciprate,
							     lft, cape, cin, qff, flags));
			for (unsigned int i = 0; i < nrsfc; ++i) {
				float temp(wxtemp.get_float(pt, efftime, i, 2, 1));
				std::pair<float,float> uvgrd(wxuvgrd.get_vector(pt, efftime, i, 10));
				float rh(wxrelhum.get_float(pt, efftime, i, 2, 1));
				float ccov(wxcldcover.get_float(pt, efftime, i, std::numeric_limits<double>::quiet_NaN(), 0.01));
				float icdeg(wxicingdeg.get_float(pt, efftime, i, std::numeric_limits<double>::quiet_NaN(), 1));
				float eddydissr(wxeddydissr.get_float(pt, efftime, i, std::numeric_limits<double>::quiet_NaN(), 1));
				float hwsh(WeatherProfilePoint::invalidcover);
				if (!std::isnan(uvgrd.first) && !std::isnan(uvgrd.second)) {
					float w0(sqrtf(uvgrd.first * uvgrd.first + uvgrd.second * uvgrd.second));
					hwsh = 0;
					unsigned int hwshcnt(0);
					static const double hwshdist_nmi = 50;
					static const double hwshdist_m = hwshdist_nmi * (1000.0 / Point::km_to_nmi_dbl);
					for (unsigned int j = 0; j < 4; ++j) {
						Point pt1(pt.spheric_course_distance_nmi(j * 90, hwshdist_nmi));
						std::pair<float,float> uvgrd1(wxuvgrd.get_vector(pt1, efftime, i, 10));
						if (std::isnan(uvgrd1.first) || std::isnan(uvgrd1.second))
							continue;
			        		float w1(sqrtf(uvgrd1.first * uvgrd1.first + uvgrd1.second * uvgrd1.second));
						hwsh += fabsf(w0 - w1) * (1.0 / hwshdist_m);
						++hwshcnt;
					}
					if (hwshcnt)
						hwsh /= hwshcnt;
					else
						hwsh = WeatherProfilePoint::invalidcover;
				}
				wxprof.back()[i] = WeatherProfilePoint::Surface(uvgrd.first, uvgrd.second, temp, rh, hwsh, WeatherProfilePoint::invalidcover,
										ccov, icdeg, eddydissr);
				if (!std::isnan(icdeg))
					wxprof.set_icingmode(WeatherProfile::icingmode_t::dwd);
			}
			for (unsigned int i = 0; i < nrsfc; ++i) {
				if (WeatherProfilePoint::isobaric_levels[i] < 0)
					continue;
				float vwsh(0);
				unsigned int vwshcnt(0);
				float w0(wxprof.back()[i].get_wind());
				if (std::isnan(w0))
					continue;
				for (int k = -1; k <= 1; k += 2) {
					if (k < 0 && !i)
						continue;
					if (i + k >= nrsfc)
						continue;
					if (WeatherProfilePoint::isobaric_levels[i + k] < 0)
						continue;
					float w1(wxprof.back()[i + k].get_wind());
					if (std::isnan(w1))
						continue;
					vwsh += fabsf(w0 - w1) / abs(WeatherProfilePoint::altitudes[i] - WeatherProfilePoint::altitudes[i + k]) * Point::m_to_ft;
					++vwshcnt;
				}
				if (vwshcnt)
					vwsh /= vwshcnt;
				else
					vwsh = WeatherProfilePoint::invalidcover;
				wxprof.back()[i].set_vwsh(vwsh);
			}
			if (std::isnan(wxprof.back().get_liftedindex()) || std::isnan(wxprof.back().get_cape()) || std::isnan(wxprof.back().get_cin())) {
				WeatherProfilePoint::Stability st(wxprof.back());
				if (std::isnan(wxprof.back().get_liftedindex()))
					wxprof.back().set_liftedindex(st.get_liftedindex());
				if (std::isnan(wxprof.back().get_cape()))
					wxprof.back().set_cape(st.get_cape());
				if (std::isnan(wxprof.back().get_cin()))
					wxprof.back().set_cin(st.get_cin());
			}
			if (t == 1.0)
				break;
		}
		dist += distleg;
	}
	// calculate icing
	if (wxprof.get_icingmode() == WeatherProfile::icingmode_t::autorouter) {
		// IENG_CNV = 200 * (Lwc(bottom_of_cloud) - lwc) / lwc(293K)) * sqrt((T - 253.15)/20.0);  si 253.15<=T<= 273.15
		// IENG_LYR = 100 * t *(t + 14)/ 49; where t = (T - 273.15); when  -14 <= t <= 0.
		// where:
		// lwc(293K) = 0.017281 Kg / m3  (water vapor density at 20ºC in a satured air)
		// lwc = es(T) / (Rv * T) (water vapor density at air cell conditions)
		// es: saturation vapor pressure (SVP)
		// lwc: liquid water content
		for (GRIB2::WeatherProfile::iterator pi(wxprof.begin()), pe(wxprof.end()); pi != pe; ++pi) {
			float convbasetemp(std::numeric_limits<float>::quiet_NaN());
			double convbaselwc(std::numeric_limits<double>::quiet_NaN());
			for (unsigned int i = 0; i < nrsfc; ++i) {
				GRIB2::WeatherProfilePoint::Surface& surf(pi->operator[](i));
				if (GRIB2::WeatherProfilePoint::isobaric_levels[i] < 0) {
					surf.set_icing(WeatherProfilePoint::invalidcover);
					continue;
				}
				float ieng(0);
				if (surf.get_temp() >= IcaoAtmosphere<float>::degc_to_kelvin - 14 &&
				    surf.get_temp() <= IcaoAtmosphere<float>::degc_to_kelvin) {
					float t(surf.get_temp() - IcaoAtmosphere<float>::degc_to_kelvin);
					if (!std::isnan(pi->get_cldlowcover()) &&
					    GRIB2::WeatherProfilePoint::altitudes[i] >= pi->get_cldlowbase() &&
					    GRIB2::WeatherProfilePoint::altitudes[i] <= pi->get_cldlowtop())
						ieng += pi->get_cldlowcover() * (100. / 49) * -t * (t + 14);
					if (!std::isnan(pi->get_cldmidcover()) &&
					    GRIB2::WeatherProfilePoint::altitudes[i] >= pi->get_cldmidbase() &&
					    GRIB2::WeatherProfilePoint::altitudes[i] <= pi->get_cldmidtop())
						ieng += pi->get_cldmidcover() * (100. / 49) * -t * (t + 14);
					if (!std::isnan(pi->get_cldhighcover()) &&
					    GRIB2::WeatherProfilePoint::altitudes[i] >= pi->get_cldhighbase() &&
					    GRIB2::WeatherProfilePoint::altitudes[i] <= pi->get_cldhightop())
						ieng += pi->get_cldhighcover() * (100. / 49) * -t * (t + 14);
					if (!std::isnan(surf.get_cloudcover()))
						ieng += surf.get_cloudcover() * (100. / 49) * -t * (t + 14);
				}
				if (!std::isnan(pi->get_cldconvcover())) {
					if (std::isnan(convbasetemp)) {
						if (pi->get_cldconvbase() <= GRIB2::WeatherProfilePoint::altitudes[i] || i + 1U >= nrsfc ||
						    GRIB2::WeatherProfilePoint::isobaric_levels[i + 1U] < 0) {
							convbasetemp = surf.get_temp();
						} else if (pi->get_cldconvbase() <= GRIB2::WeatherProfilePoint::altitudes[i + 1U])
							convbasetemp = surf.get_temp() + (pi->operator[](i + 1U).get_temp() - surf.get_temp())
								* (pi->get_cldconvbase() - GRIB2::WeatherProfilePoint::altitudes[i])
								/ (GRIB2::WeatherProfilePoint::altitudes[i + 1U] - GRIB2::WeatherProfilePoint::altitudes[i]);
						static constexpr double lwccldbasepoly[] = {
							1.4637e+00, 4.8000e-02, -7.5000e-04
						};
						if (!std::isnan(convbasetemp)) {
							double tt(convbasetemp - IcaoAtmosphere<double>::degc_to_kelvin), t(1);
							convbaselwc = 0;
							for (unsigned int i(0), n(sizeof(lwccldbasepoly)/sizeof(lwccldbasepoly[0])); i < n; ++i) {
								convbaselwc += lwccldbasepoly[i] * t;
								t *= tt;
							}
						}
					}
					static constexpr double lwc293k(17.281);
					// mmHg->Pa: *133.322368 ; Pa = kg·m^−1·s^−2
					// R=8.3144621 m^2·kg·s^-2·K^-1·mol^-1
					// m=18.01528 g/mol
					static constexpr double lwcc1(20.386+log(18.01528*133.322368/8.3144621));
					static constexpr double lwcc2(-5132);
					if (!std::isnan(convbaselwc) && 
					    GRIB2::WeatherProfilePoint::altitudes[i] >= pi->get_cldconvbase() &&
					    GRIB2::WeatherProfilePoint::altitudes[i] <= pi->get_cldconvtop() &&
					    surf.get_temp() >= IcaoAtmosphere<float>::degc_to_kelvin - 20 &&
					    surf.get_temp() <= IcaoAtmosphere<float>::degc_to_kelvin) {
						double x(1.0 / surf.get_temp());
						x *= exp(lwcc1+lwcc2*x);
						x = (convbaselwc - x) * (1.0 / lwc293k);
						x *= 200 * sqrt((surf.get_temp() + (20 - IcaoAtmosphere<float>::degc_to_kelvin))/20.0);
						if (x >= 0)
							ieng += pi->get_cldconvcover() * x;
					}
				}
				ieng *= 0.5;
				surf.set_icing(ieng);
			}
		}
	}
	if (false)
		wxprof.print(std::cerr);
	return wxprof;
}

// Turbulence / Eddy Dissipation Rate
// https://www.aviationweather.gov/turbulence/help?page=plot
// https://blog.foreflight.com/2015/11/03/new-turbulence-imagery/

// could use CAT turbulence and in cloud icing (ICIP) forecast from WAFS
// files WAFS_blended_...
// https://www.icao.int/safety/meteorology/WAFSOPSG/Guidance%20Material/Training%20on%20WAFS%20Grid%20point%20forecasts%20for%20CB%20%20Icing%20and%20Turbulence%20-%20English.pdf
// resolution both horizontal and vertical seems very coarse though
