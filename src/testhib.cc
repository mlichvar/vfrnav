/*****************************************************************************/

/*
 *      testhib.cc  --  Test hibernate
 *
 *      Copyright (C) 2020  Thomas Sailer (t.sailer@alumni.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/*****************************************************************************/

#include <getopt.h>
#include <iomanip>
#include <iostream>
#include <sstream>

#ifdef HAVE_SYSEXITS_H
#include <sysexits.h>
#else
#define EX_USAGE   64
#define EX_DATAERR 65
#define EX_OK      0
#endif

#include "hibernate.h"

enum class enum8_t : uint8_t {
	zero = 0,
	one,
	two,
	aa = 0xaa
};

enum class enum16_t : uint16_t {
	zero = 0,
	one,
	two,
	dead = 0xdead
};

static void test(void)
{
	std::ostringstream oss;
	{
		static constexpr uint8_t expected[] = { 0xca, 0xbe, 0xba, 0xaa, 0xad, 0xde };

		HibernateWriteStream ar(oss);
		uint8_t x8(0xca);
		uint16_t x16(0xbabe);
		enum8_t e8(enum8_t::aa);
		enum16_t e16(enum16_t::dead);
		ar.io(x8);
		ar.io(x16);
		ar.ioenum(e8);
		ar.ioenum(e16);
		if (oss.str().size() != sizeof(expected) || memcmp(oss.str().c_str(), expected, sizeof(expected))) {
			std::ostringstream msg;
			msg << "Hibernate Write Stream error: actual:" << std::hex;
			for (char ch : oss.str())
				msg << ' ' << std::setw(2) << std::setfill('0') << (ch & 0xff);
			msg << " expected:";
			for (unsigned int i(0); i < sizeof(expected); ++i)
				msg << ' ' << std::setw(2) << std::setfill('0') << (expected[i] & 0xff);
			throw std::runtime_error(msg.str());
		}
	}
	{
		std::istringstream iss(oss.str());
		HibernateReadStream ar(iss);
		uint8_t x8(0);
		uint16_t x16(0);
		enum8_t e8(enum8_t::zero);
		enum16_t e16(enum16_t::zero);
		ar.io(x8);
		ar.io(x16);
		ar.ioenum(e8);
		ar.ioenum(e16);
		if (x8 != 0xca || x16 != 0xbabe || e8 != enum8_t::aa || e16 != enum16_t::dead) {
			std::ostringstream msg;
			msg << "Hibernate Read Stream error: x8 " << std::hex << std::setw(2) << std::setfill('0') << (x8 & 0xff)
			    << " x16 " << std::setw(4) << std::setfill('0') << (x16 & 0xffff)
			    << " e8 " << std::setw(2) << std::setfill('0') << (static_cast<std::underlying_type<enum8_t>::type>(e8) & 0xff)
			    << " e16 " << std::setw(4) << std::setfill('0') << (static_cast<std::underlying_type<enum16_t>::type>(e16) & 0xffff);
			throw std::runtime_error(msg.str());
		}
	}
	{
		HibernateReadBuffer ar(reinterpret_cast<const uint8_t *>(oss.str().c_str()),
				       reinterpret_cast<const uint8_t *>(oss.str().c_str() + oss.str().size()));
		uint8_t x8(0);
		uint16_t x16(0);
		enum8_t e8(enum8_t::zero);
		enum16_t e16(enum16_t::zero);
		ar.io(x8);
		ar.io(x16);
		ar.ioenum(e8);
		ar.ioenum(e16);
		if (x8 != 0xca || x16 != 0xbabe || e8 != enum8_t::aa || e16 != enum16_t::dead) {
			std::ostringstream msg;
			msg << "Hibernate Read Buffer error: x8 " << std::hex << std::setw(2) << std::setfill('0') << (x8 & 0xff)
			    << " x16 " << std::setw(4) << std::setfill('0') << (x16 & 0xffff)
			    << " e8 " << std::setw(2) << std::setfill('0') << (static_cast<std::underlying_type<enum8_t>::type>(e8) & 0xff)
			    << " e16 " << std::setw(4) << std::setfill('0') << (static_cast<std::underlying_type<enum16_t>::type>(e16) & 0xffff);
			throw std::runtime_error(msg.str());
		}
	}
}

int main(int argc, char *argv[])
{
	static struct option long_options[] = {
		{ "xxxx", required_argument, 0, 'x' },
		{ 0, 0, 0, 0 }
	};
	int c, err(0);
	
	while ((c = getopt_long(argc, argv, "x:", long_options, 0)) != EOF) {
		switch (c) {
		default:
			++err;
			break;
		}
	}
	if (err) {
		std::cerr << "usage: testhib" << std::endl;
		return EX_USAGE;
	}

	try {
		test();
	} catch (const std::exception& e) {
		std::cerr << "Test failure: " << e.what() << std::endl;
		return EX_DATAERR;
	}
	return EX_OK;
}
