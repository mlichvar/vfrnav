/*****************************************************************************/

/*
 *      acftpoly.cc  --  Aircraft Model Polygon Class.
 *
 *      Copyright (C) 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020
 *        Thomas Sailer (t.sailer@alumni.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/*****************************************************************************/

#include "aircraft.h"

#include <iostream>
#include <iomanip>

#ifdef HAVE_EIGEN3
#include <Eigen/Dense>
#include <Eigen/Cholesky>
#include <unsupported/Eigen/Polynomials>
#endif

template <typename T> T Aircraft::Poly1D<T>::eval(T x) const
{
	if (this->empty())
		return std::numeric_limits<T>::quiet_NaN();
	T ret(0);
	for (typename Poly1D<T>::const_reverse_iterator pi(this->rbegin()), pe(this->rend()); pi != pe; ++pi)
		ret = ret * x + *pi;
	return ret;
}

template <typename T> T Aircraft::Poly1D<T>::evalderiv(T x) const
{
	if (this->empty())
		return std::numeric_limits<T>::quiet_NaN();
	T ret(0), x1(1);
	for (typename Poly1D<T>::size_type i(1); i < this->size(); ++i, x1 *= x)
		ret += i * x1 * (*this)[i];
	return ret;
}

template <typename T> T Aircraft::Poly1D<T>::evalderiv2(T x) const
{
	if (this->empty())
		return std::numeric_limits<T>::quiet_NaN();
	T ret(0), x1(1);
	for (typename Poly1D<T>::size_type i(2); i < this->size(); ++i, x1 *= x)
		ret += i * (i - 1) * x1 * (*this)[i];
	return ret;
}

template <typename T> T Aircraft::Poly1D<T>::inveval(T x, T tol, unsigned int maxiter, T t) const
{
	for (unsigned int i = 0; i < maxiter; ++i) {
		T x0(this->eval(t));
		if (std::fabs(x - x0) <= tol)
			break;
		T xd(this->evalderiv(t));
		if (xd == 0)
			break;
		if (false)
			std::cerr << "inveval: y " << x << " yest " << x0 << " yderiv " << xd
				  << " x " << t << " tol " << tol << " iter " << i << std::endl;
		t += (x - x0) / xd;
	}
	return t;
}

template <typename T> T Aircraft::Poly1D<T>::inveval(T x, T tol, unsigned int maxiter) const
{
	if (this->size() < 2 || (*this)[1] == 0)
		return std::numeric_limits<T>::quiet_NaN();
	return this->inveval(x, tol, maxiter, (x - (*this)[0]) / (*this)[1]);
}

template <typename T> T Aircraft::Poly1D<T>::inveval(T x) const
{
	static const T abstol(1e-8);
	static const T reltol(1e-4);
	return this->inveval(x, std::max(abstol, std::fabs(x) * reltol));
}

template <typename T> T Aircraft::Poly1D<T>::boundedinveval(T x, T blo, T bhi, T tol, unsigned int maxiter) const
{
	if (std::isnan(blo) || std::isnan(bhi) || bhi <= blo)
		return std::numeric_limits<T>::quiet_NaN();
	T t;
	{
		T xlo(this->eval(blo));
		T xhi(this->eval(bhi));
		if (std::isnan(xlo) || std::isnan(xhi) || fabs(xhi - xlo) < 1e-10) {
			t = 0.5 * (blo + bhi);
		} else {
			t = (x - xlo) / (xhi - xlo) * (bhi - blo) + blo;
			t = std::max(blo, std::min(bhi, t));
		}
	}
	for (unsigned int i = 0; i < maxiter; ++i) {
		T x0(this->eval(t));
		if (std::fabs(x - x0) <= tol)
			break;
		T xd(this->evalderiv(t));
		if (xd == 0)
			break;
		if (false)
			std::cerr << "inveval: y " << x << " yest " << x0 << " yderiv " << xd
				  << " x " << t << " tol " << tol << " iter " << i
				  << " bounds " << blo << "..." << bhi << std::endl;
		t += (x - x0) / xd;
		t = std::max(blo, std::min(bhi, t));
	}
	return t;
}

template <typename T> T Aircraft::Poly1D<T>::boundedinveval(T x, T blo, T bhi) const
{
	static const T abstol(1e-8);
	static const T reltol(1e-4);
	return this->boundedinveval(x, blo, bhi, std::max(abstol, std::fabs(x) * reltol));
}

template <typename T> void Aircraft::Poly1D<T>::eval(XY& x) const
{
	x.set_y(eval(x.get_x()));
}

template <typename T> void Aircraft::Poly1D<T>::evalderiv(XY& x) const
{
	x.set_y(evalderiv(x.get_x()));
}

template <typename T> void Aircraft::Poly1D<T>::evalderiv2(XY& x) const
{
	x.set_y(evalderiv2(x.get_x()));
}

template <typename T> void Aircraft::Poly1D<T>::inveval(XY& x, T tol, unsigned int maxiter, T t) const
{
	x.set_y(inveval(x.get_x(), tol, maxiter, t));
}

template <typename T> void Aircraft::Poly1D<T>::inveval(XY& x, T tol, unsigned int maxiter) const
{
	x.set_y(inveval(x.get_x(), tol, maxiter));
}

template <typename T> void Aircraft::Poly1D<T>::inveval(XY& x) const
{
	x.set_y(inveval(x.get_x()));
}

template <typename T> void Aircraft::Poly1D<T>::boundedinveval(XY& x, T blo, T bhi, T tol, unsigned int maxiter) const
{
	x.set_y(boundedinveval(x.get_x(), blo, bhi, tol, maxiter));
}

template <typename T> void Aircraft::Poly1D<T>::boundedinveval(XY& x, T blo, T bhi) const
{
	x.set_y(boundedinveval(x.get_x(), blo, bhi));
}

template <typename T> void Aircraft::Poly1D<T>::reduce(void)
{
	typename Poly1D<T>::size_type n(this->size());
	while (n > 1) {
		typename Poly1D<T>::size_type n1(n - 1);
		if ((*this)[n1] != static_cast<T>(0))
			break;
		n = n1;
	}
	this->resize(n);
}

template <typename T> Aircraft::Poly1D<T>& Aircraft::Poly1D<T>::operator+=(T x)
{
	this->resize(std::max(this->size(), (typename Poly1D<T>::size_type)1U), (T)0);
	(*this)[0] += x;
	reduce();
	return *this;
}

template <typename T> Aircraft::Poly1D<T>& Aircraft::Poly1D<T>::operator-=(T x)
{
	this->resize(std::max(this->size(), (typename Poly1D<T>::size_type)1U), (T)0);
	(*this)[0] -= x;
	reduce();
	return *this;
}

template <typename T> Aircraft::Poly1D<T>& Aircraft::Poly1D<T>::operator*=(T x)
{
	for (typename Poly1D<T>::size_type i(0); i < this->size(); ++i)
		(*this)[i] *= x;
	reduce();
	return *this;
}

template <typename T> Aircraft::Poly1D<T>& Aircraft::Poly1D<T>::operator/=(T x)
{
	for (typename Poly1D<T>::size_type i(0); i < this->size(); ++i)
		(*this)[i] /= x;
	reduce();
	return *this;
}

template <typename T> Aircraft::Poly1D<T>& Aircraft::Poly1D<T>::operator+=(const Poly1D<T>& x)
{
	this->resize(std::max(this->size(), x.size()), (T)0);
	for (typename Poly1D<T>::size_type i(0); i < x.size(); ++i)
		(*this)[i] += x[i];
	reduce();
	return *this;
}

template <typename T> Aircraft::Poly1D<T>& Aircraft::Poly1D<T>::operator-=(const Poly1D<T>& x)
{
	this->resize(std::max(this->size(), x.size()), (T)0);
	for (typename Poly1D<T>::size_type i(0); i < x.size(); ++i)
		(*this)[i] -= x[i];
	reduce();
	return *this;
}

template <typename T> Aircraft::Poly1D<T> Aircraft::Poly1D<T>::operator*(const Poly1D<T>& x) const
{
	Poly1D<T> r;
	{
		typename Poly1D<T>::size_type n(this->size() + x.size());
		if (!n)
			return r;
		--n;
		r.resize(n, (T)0);
	}
	for (typename Poly1D<T>::size_type i(0); i < this->size(); ++i)
		for (typename Poly1D<T>::size_type j(0); j < x.size(); ++j)
			r[i + j] += (*this)[i] * x[j];
	r.reduce();
	return r;
}

template <typename T> Aircraft::Poly1D<T> Aircraft::Poly1D<T>::differentiate(void) const
{
	if (this->size() < 2U)
		return Poly1D<T>();
	Poly1D<T> r(this->size() - 1);
	for (typename Poly1D<T>::size_type i(1); i < this->size(); ++i)
		r[i - 1] = i * (*this)[i];
	r.reduce();
	return r;
}

template <typename T> Aircraft::Poly1D<T> Aircraft::Poly1D<T>::integrate(void) const
{
	if (!this->size())
		return Poly1D<T>();
	Poly1D<T> r(this->size() + 1);
	for (typename Poly1D<T>::size_type i(1); i <= this->size(); ++i)
		r[i] = (*this)[i - 1] / (T)i;
	r.reduce();
	return r;
}

template <typename T> Aircraft::Poly1D<T> Aircraft::Poly1D<T>::convert(unit_t ufrom, unit_t uto) const
{
	Poly1D<T> r(*this);
	r *= Aircraft::convert(ufrom, uto, 1);
	return r;
}

template <typename T> Aircraft::Poly1D<T> Aircraft::Poly1D<T>::simplify(double absacc, double relacc, double invmaxmag) const
{
	if (this->empty() || std::isnan((*this)[0]))
		return *this;
	if (std::isnan(absacc) || std::isnan(relacc) || std::isnan(invmaxmag))
		return *this;
	invmaxmag = fabs(invmaxmag);
	absacc = std::max(fabs(absacc), fabs((*this)[0] * relacc));
	Poly1D<T> r(*this);
	for (typename Poly1D<T>::size_type i(0); i < r.size(); ++i, absacc *= invmaxmag) {
		if (fabs(r[i]) < absacc)
			r[i] = 0;
	}
	r.reduce();
	return r;
}

template <typename T> typename std::vector<std::complex<T> > Aircraft::Poly1D<T>::roots(void) const
{
	Eigen::Map<const Eigen::Matrix<T,Eigen::Dynamic,1> > poly(&(*this)[0], this->size());
	Eigen::PolynomialSolver<T,Eigen::Dynamic> roots(poly);
	typename Eigen::PolynomialSolver<T,Eigen::Dynamic>::RootsType r(roots.roots());
	return typename std::vector<std::complex<T> >(r.data(), r.data() + r.size());
}

template <typename T> typename std::vector<T> Aircraft::Poly1D<T>::real_roots(void) const
{
	typename std::vector<T> r;
	Eigen::Map<const Eigen::Matrix<T,Eigen::Dynamic,1> > poly(&(*this)[0], this->size());
	Eigen::PolynomialSolver<T,Eigen::Dynamic> roots(poly);
	roots.realRoots(r);
	return r;
}

template <typename T> typename Aircraft::Poly1D<T>::MinMax Aircraft::Poly1D<T>::get_minmax(T dmin, T dmax) const
{
	MinMax r;
	r.m_glbmin.set_x(dmin);
	r.m_glbmax.set_x(dmax);
	eval(r.m_glbmin);
	eval(r.m_glbmax);
	if (r.m_glbmin.get_y() > r.m_glbmax.get_y())
		std::swap(r.m_glbmin, r.m_glbmax);
	typedef typename std::vector<T> rroots_t;
	rroots_t rroots;
	{
		Poly1D<T> pd(this->differentiate());
		Eigen::Map<const Eigen::Matrix<T,Eigen::Dynamic,1> > poly(&pd[0], pd.size());
		Eigen::PolynomialSolver<T,Eigen::Dynamic> roots(poly);
		roots.realRoots(rroots);
	}
	for (typename rroots_t::const_iterator ri(rroots.begin()), re(rroots.end()); ri != re; ++ri) {
		XY rt(*ri);
		eval(rt);
		if (rt.get_y() > r.m_glbmax.get_y())
			r.m_glbmax = rt;
		if (rt.get_y() < r.m_glbmin.get_y())
			r.m_glbmin = rt;
		double d2(evalderiv2(*ri));
		if (d2 > 0)
			r.m_lclmax.insert(rt);
		else if (d2 < 0)
			r.m_lclmin.insert(rt);
	}
	return r;
}

template <typename T> void Aircraft::Poly1D<T>::load_xml(const xmlpp::Element *el)
{
	if (!el)
		return;
	const xmlpp::TextNode* txt(el->get_child_text());
	if (!txt)
		return;
	this->clear();
	std::istringstream iss(txt->get_content());
	while (!iss.eof()) {
		double v;
		iss >> v;
		if (iss.fail()) {
			std::cerr << "Aircraft::Poly1D::load_xml: poly parse error: "
				  << txt->get_content() << " order " << this->size() << std::endl;
			break;
		}
		this->push_back(v);
		if (iss.eof())
			break;
		char ch;
		iss >> ch;
		if (iss.fail()) {
			std::cerr << "Aircraft::Poly1D::load_xml: poly parse error: "
				  << txt->get_content() << " order " << this->size() << std::endl;
			break;
		}
		if (ch != ',') {
			std::cerr << "Aircraft::Poly1D::load_xml: poly delimiter error: "
				  << txt->get_content() << " order " << this->size() << std::endl;
			break;
		}
	}
}

template <typename T> void Aircraft::Poly1D<T>::save_xml(xmlpp::Element *el) const
{
	if (!el)
		return;
	std::ostringstream oss;
	oss << std::setprecision(10);
	bool subseq(false);
	for (typename Poly1D<T>::const_iterator pi(this->begin()), pe(this->end()); pi != pe; ++pi) {
		if (subseq)
			oss << ',';
		subseq = true;
		oss << *pi;
	}
	el->set_child_text(oss.str());
}

template <typename T> std::ostream& Aircraft::Poly1D<T>::save_octave(std::ostream& os, const Glib::ustring& name) const
{
	os << "# name: " << name << std::endl
	   << "# type: matrix" << std::endl
	   << "# rows: " << this->size() << std::endl
	   << "# columns: 1" << std::endl;
	for (typename Poly1D<T>::const_iterator i(this->begin()), e(this->end()); i != e; ++i) {
		std::ostringstream oss;
		oss << std::setprecision(10) << *i;
		os << oss.str() << std::endl;
	}
	return os;
}

template <typename T> std::ostream& Aircraft::Poly1D<T>::print(std::ostream& os) const
{
	bool subseq(false);
	for (typename Poly1D<T>::const_reverse_iterator i(this->rbegin()), e(this->rend()); i != e; ++i) {
		if (subseq)
			os << ' ';
		else
			subseq = true;
		std::ostringstream oss;
		oss << std::setprecision(10) << *i;
		os << oss.str();
	}
	return os;
}

template class Aircraft::Poly1D<double>;
