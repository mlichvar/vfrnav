/*****************************************************************************/

/*
 *      testairac.cc  --  Test AIRAC calculations.
 *
 *      Copyright (C) 2019  Thomas Sailer (t.sailer@alumni.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/*****************************************************************************/

#include <getopt.h>
#include <iomanip>
#include <iostream>
#include <sstream>

#ifdef HAVE_SYSEXITS_H
#include <sysexits.h>
#else
#define EX_USAGE   64
#define EX_DATAERR 65
#define EX_OK      0
#endif

#include "airac.h"

/*
  ICAO DOC 8126, 6th edition (2003); Paragraph 2.6.2 b):
    the AIRAC effective dates must be in accordance
    with the predetermined, internationally agreed
    schedule of effective dates based on an interval of
    28 days, including 29 January 1998
*/

static void calculate_base(void)
{
	Glib::Date dt(29, Glib::Date::JANUARY, 1998);
	std::cout << "Date " << dt.format_string("%F") << " julian " << dt.get_julian() << " base " << (dt.get_julian() % 28) << std::endl;
}

static void first_cycle_of_year(void)
{
	for (Glib::Date::Year y = 2010; y <= 2022; ++y)
		std::cout << "Year " << y << ": " << Glib::Date(AIRAC::get_first_of_year(y)).format_string("%F") << std::endl;
}

static void test_from_date(void)
{
	struct testdata {
		const char *date;
		AIRAC::Year year;
		AIRAC::Seq seq;
	};
	struct testdata testdata[] = {
		{ "1998-01-29", 1998, 2 },

		/*
			ICAO DOC 8126, 6th edition (2003); table 2-1 "Schedule of AIRAC effective date 2003-2012"
		*/
		// first airac of the year and its predecessor
		{ "2003-01-23", 2003, 1 },

		{ "2004-01-21", 2003, 13 },
		{ "2004-01-22", 2004, 1 },

		{ "2005-01-19", 2004, 13 },
		{ "2005-01-20", 2005, 1 },

		{ "2006-01-18", 2005, 13 },
		{ "2006-01-19", 2006, 1 },

		{ "2007-01-17", 2006, 13 },
		{ "2007-01-18", 2007, 1 },

		{ "2008-01-16", 2007, 13 },
		{ "2008-01-17", 2008, 1 },

		{ "2009-01-14", 2008, 13 },
		{ "2009-01-15", 2009, 1 },

		{ "2010-01-13", 2009, 13 },
		{ "2010-01-14", 2010, 1 },

		{ "2011-01-12", 2010, 13 },
		{ "2011-01-13", 2011, 1 },

		{ "2012-01-11", 2011, 13 },
		{ "2012-01-12", 2012, 1 },

		/*
			http://www.eurocontrol.int/articles/airac-adherence-monitoring-phase-1-p-03
		*/
		// first airac of the year and its predecessor (2013 2020)
		{ "2013-01-09", 2012, 13 },
		{ "2013-01-10", 2013, 1 },

		{ "2014-01-08", 2013, 13 },
		{ "2014-01-09", 2014, 1 },

		{ "2015-01-07", 2014, 13 },
		{ "2015-01-08", 2015, 1 },

		{ "2016-01-06", 2015, 13 },
		{ "2016-01-07", 2016, 1 },

		{ "2017-01-04", 2016, 13 },
		{ "2017-01-05", 2017, 1 },

		{ "2018-01-03", 2017, 13 },
		{ "2018-01-04", 2018, 1 },

		{ "2019-01-02", 2018, 13 },
		{ "2019-01-03", 2019, 1 },

		{ "2020-01-01", 2019, 13 },
		{ "2020-01-02", 2020, 1 },

		// the 'special' one:
		{ "2020-12-30", 2020, 13 },
		{ "2020-12-31", 2020, 14 },

		// calculated manually
		{ "2021-01-27", 2020, 14 },
		{ "2021-01-28", 2021, 1 },
		{ "2003-01-22", 2002, 13 },
		{ "1964-01-16", 1964, 1 },
		{ "1901-01-10", 1901, 1 },
		{ "1998-12-31", 1998, 14 },

		{ "1963-12-31", 1963, 13 },

		// end marker
		{ 0, 0, 0 }
	};

	for (const struct testdata *t = testdata; t->date; ++t) {
		AIRAC a;
		if (!a.set_iso8601(t->date)) {
			std::ostringstream oss;
			oss << "Error parsing date " << t->date;
			throw std::runtime_error(oss.str());
		}
		AIRAC::Year y;
		AIRAC::Seq s;
		if (!a.get_cycle(y, s)) {
			std::ostringstream oss;
			oss << "Cannot compute cycle for " << a.format_string("%F");
			throw std::runtime_error(oss.str());
		}
		if (y != t->year || s != t->seq) {
			std::ostringstream oss;
			oss << "Cycle for " << a.format_string("%F") << ": " << y << ' ' << static_cast<guint16>(s)
			    << " does not match expected " << t->year << ' ' << static_cast<guint16>(t->seq);
			throw std::runtime_error(oss.str());
		}
	}
}

static void test_next_prev(void)
{
	struct testdata {
		const char *date;
		AIRAC::Year pyear;
		AIRAC::Seq pseq;
		AIRAC::Year nyear;
		AIRAC::Seq nseq;
	};
	struct testdata testdata[] = {
		{ "2006-01-20", 2005, 13, 2006, 2 },
		{ "2021-01-01", 2020, 13, 2021, 1 },
		{ 0, 0, 0, 0, 0 }
	};

	for (const struct testdata *t = testdata; t->date; ++t) {
		AIRAC a;
		if (!a.set_iso8601(t->date)) {
			std::ostringstream oss;
			oss << "Error parsing date " << t->date;
			throw std::runtime_error(oss.str());
		}
		{
			AIRAC a1(a);
			a1.prev_cycle();
			AIRAC::Year y;
			AIRAC::Seq s;
			if (!a1.get_cycle(y, s)) {
				std::ostringstream oss;
				oss << "Cannot compute previous cycle for " << a.format_string("%F");
				throw std::runtime_error(oss.str());
			}
			if (y != t->pyear || s != t->pseq) {
				std::ostringstream oss;
				oss << "Previous cycle for " << a.format_string("%F") << ": " << y << ' ' << static_cast<guint16>(s)
				    << " does not match expected " << t->pyear << ' ' << static_cast<guint16>(t->pseq);
				throw std::runtime_error(oss.str());
			}
		}
		{
			AIRAC a1(a);
			a1.next_cycle();
			AIRAC::Year y;
			AIRAC::Seq s;
			if (!a1.get_cycle(y, s)) {
				std::ostringstream oss;
				oss << "Cannot compute next cycle for " << a.format_string("%F");
				throw std::runtime_error(oss.str());
			}
			if (y != t->nyear || s != t->nseq) {
				std::ostringstream oss;
				oss << "Next cycle for " << a.format_string("%F") << ": " << y << ' ' << static_cast<guint16>(s)
				    << " does not match expected " << t->nyear << ' ' << static_cast<guint16>(t->nseq);
				throw std::runtime_error(oss.str());
			}
		}
	}
}

static void test_from_string(void)
{
	struct testdata {
		const char *cycle;
		const char *date;
		AIRAC::Year year;
		AIRAC::Seq seq;
		bool valid;
	};
	struct testdata testdata[] = {
		{ "2014", "2020-12-31", 2020, 14, true },
		{ "1511", "2015-10-15", 2015, 11, true },
		{ "1514", "", 0, 0, false },
		{ "1501", "2015-01-08", 2015, 1, true },
		{ "9999", "", 0, 0, false },
		{ "6401", "1964-01-16", 1964, 1, true },
		{ "6301", "2063-01-04", 2063, 1, true },
		{ "6313", "2063-12-06", 2063, 13, true },
		{ "9913", "1999-12-30", 1999, 13, true },
		{ "09913", "", 0, 0, false },
		{ "101", "", 0, 0, false },
		{ "0000", "", 0, 0, false },
		{ "160a", "", 0, 0, false },
		{ "1a01", "", 0, 0, false },
		{ "1016", "", 0, 0, false },
		{ "10-1", "", 0, 0, false },
		{ "-101", "", 0, 0, false },
		{ "", "", 0, 0, false },
		{ "nope", "", 0, 0, false },
		{ "0", "", 0, 0, false },
		{ "-0", "", 0, 0, false },
		{ "-1", "", 0, 0, false },
		{ "11", "", 0, 0, false },
		{ "011", "", 0, 0, false },
		{ "a", "", 0, 0, false },
		{ "aa", "", 0, 0, false },
		{ "", "", 0, 0, false },
		{ 0, 0, 0, 0, false }
	};

	for (const struct testdata *t = testdata; t->date; ++t) {
		AIRAC a;
		if (a.set_cycle(t->cycle) != t->valid) {
			std::ostringstream oss;
			oss << "Error parsing cycle " << t->cycle;
			throw std::runtime_error(oss.str());
		}
		if (!t->valid)
			continue;
		{
			AIRAC a1;
			if (!a1.set_iso8601(t->date)) {
				std::ostringstream oss;
				oss << "Error parsing date " << t->date;
				throw std::runtime_error(oss.str());
			}
			if (a.get_julian() != a1.get_julian()) {
				std::ostringstream oss;
				oss << "Cycle " << t->cycle << " date " << a.format_string("%F")
				    << " does not match expected " << a1.format_string("%F");
				throw std::runtime_error(oss.str());
			}
		}
		AIRAC::Year y;
		AIRAC::Seq s;
		if (!a.get_cycle(y, s)) {
			std::ostringstream oss;
			oss << "Cannot compute cycle for " << a.format_string("%F");
			throw std::runtime_error(oss.str());
		}
		if (y != t->year || s != t->seq) {
			std::ostringstream oss;
			oss << "Cycle for " << a.format_string("%F") << ": " << y << ' ' << static_cast<guint16>(s)
			    << " does not match expected " << t->year << ' ' << static_cast<guint16>(t->seq);
			throw std::runtime_error(oss.str());
		}
	}
}

int main(int argc, char *argv[])
{
	static struct option long_options[] = {
		{ "approachcat", required_argument, 0, 'a' },
		{ 0, 0, 0, 0 }
	};
	int c, err(0);
	
	while ((c = getopt_long(argc, argv, "a:o:", long_options, 0)) != EOF) {
		switch (c) {
		default:
			++err;
			break;
		}
	}
	if (err) {
		std::cerr << "usage: testairac [-a <apchcatfile>] [-o <outfile>] [<infile>]" << std::endl;
		return EX_USAGE;
	}

	calculate_base();
	first_cycle_of_year();
	try {
		test_from_date();
		test_next_prev();
		test_from_string();
	} catch (const std::exception& e) {
		std::cerr << "Test failure: " << e.what() << std::endl;
		return EX_DATAERR;
	}
	return EX_OK;
}
