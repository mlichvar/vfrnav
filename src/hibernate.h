#ifndef HIBERNATE_H
#define HIBERNATE_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "sysdeps.h"

#include <atomic>

class HibernateWriteStream {
public:
	HibernateWriteStream(std::ostream& os) : m_os(os) {}
	static bool is_save(void) { return true; }
	static bool is_load(void) { return false; }
	template <typename T> void io(const T& v);
	template <typename T> void ioleb(T x);
	template <typename T> inline void ioint8(T v) { int8_t vv(v); io(vv); }
	template <typename T> inline void ioint16(T v) { int16_t vv(v); io(vv); }
	template <typename T> inline void ioint32(T v) { int32_t vv(v); io(vv); }
	template <typename T> inline void ioint64(T v) { int64_t vv(v); io(vv); }
	template <typename T> inline void iouint8(T v) { uint8_t vv(v); io(vv); }
	template <typename T> inline void iouint16(T v) { uint16_t vv(v); io(vv); }
	template <typename T> inline void iouint32(T v) { uint32_t vv(v); io(vv); }
	template <typename T> inline void iouint64(T v) { uint64_t vv(v); io(vv); }
	template <typename T> inline void io(const std::atomic<T>& v) { T vv(v); io(vv); }
	template <typename T> inline void ioleb(const std::atomic<T>& v) { T vv(v); ioleb(vv); }
	template <typename T> inline void ioint8(const std::atomic<T>& v) { T vv(v); ioint8(vv); }
	template <typename T> inline void ioint16(const std::atomic<T>& v) { T vv(v); ioint16(vv); }
	template <typename T> inline void ioint32(const std::atomic<T>& v) { T vv(v); ioint32(vv); }
	template <typename T> inline void ioint64(const std::atomic<T>& v) { T vv(v); ioint64(vv); }
	template <typename T> inline void iouint8(const std::atomic<T>& v) { T vv(v); iouint8(vv); }
	template <typename T> inline void iouint16(const std::atomic<T>& v) { T vv(v); iouint16(vv); }
	template <typename T> inline void iouint32(const std::atomic<T>& v) { T vv(v); iouint32(vv); }
	template <typename T> inline void iouint64(const std::atomic<T>& v) { T vv(v); iouint64(vv); }
	template <typename T> inline void ioenum(T v) { typename std::underlying_type<T>::type vv(static_cast<typename std::underlying_type<T>::type>(v)); io(vv); }

protected:
	std::ostream& m_os;
};

class HibernateReadStream {
public:
	HibernateReadStream(std::istream& is) : m_is(is) {}
	static bool is_save(void) { return false; }
	static bool is_load(void) { return true; }
	template <typename T> void io(T& v);
	template <typename T> void ioleb(T& x);
	template <typename T> inline void ioint8(T& v) { int8_t vv; io(vv); v = static_cast<T>(vv); }
	template <typename T> inline void ioint16(T& v) { int16_t vv; io(vv); v = static_cast<T>(vv); }
	template <typename T> inline void ioint32(T& v) { int32_t vv; io(vv); v = static_cast<T>(vv); }
	template <typename T> inline void ioint64(T& v) { int64_t vv; io(vv); v = static_cast<T>(vv); }
	template <typename T> inline void iouint8(T& v) { uint8_t vv; io(vv); v = static_cast<T>(vv); }
	template <typename T> inline void iouint16(T& v) { uint16_t vv; io(vv); v = static_cast<T>(vv); }
	template <typename T> inline void iouint32(T& v) { uint32_t vv; io(vv); v = static_cast<T>(vv); }
	template <typename T> inline void iouint64(T& v) { uint64_t vv; io(vv); v = static_cast<T>(vv); }
	template <typename T> inline void io(std::atomic<T>& v) { T vv; io(vv); v = vv; }
	template <typename T> inline void ioleb(std::atomic<T>& v) { T vv; ioleb(vv); v = vv; }
	template <typename T> inline void ioint8(std::atomic<T>& v) { T vv; ioint8(vv); v = vv; }
	template <typename T> inline void ioint16(std::atomic<T>& v) { T vv; ioint16(vv); v = vv; }
	template <typename T> inline void ioint32(std::atomic<T>& v) { T vv; ioint32(vv); v = vv; }
	template <typename T> inline void ioint64(std::atomic<T>& v) { T vv; ioint64(vv); v = vv; }
	template <typename T> inline void iouint8(std::atomic<T>& v) { T vv; iouint8(vv); v = vv; }
	template <typename T> inline void iouint16(std::atomic<T>& v) { T vv; iouint16(vv); v = vv; }
	template <typename T> inline void iouint32(std::atomic<T>& v) { T vv; iouint32(vv); v = vv; }
	template <typename T> inline void iouint64(std::atomic<T>& v) { T vv; iouint64(vv); v = vv; }
	template <typename T> inline void ioenum(T& v) { typename std::underlying_type<T>::type vv; io(vv); v = static_cast<T>(vv); }

protected:
	std::istream& m_is;
};

class HibernateReadBuffer {
public:
	HibernateReadBuffer(const uint8_t *ptr, const uint8_t *end) : m_ptr(ptr), m_end(end) {}
	static bool is_save(void) { return false; }
	static bool is_load(void) { return true; }
	template <typename T> void io(T& v);
	template <typename T> void ioleb(T& x);
	template <typename T> inline void ioint8(T& v) { int8_t vv; io(vv); v = static_cast<T>(vv); }
	template <typename T> inline void ioint16(T& v) { int16_t vv; io(vv); v = static_cast<T>(vv); }
	template <typename T> inline void ioint32(T& v) { int32_t vv; io(vv); v = static_cast<T>(vv); }
	template <typename T> inline void ioint64(T& v) { int64_t vv; io(vv); v = static_cast<T>(vv); }
	template <typename T> inline void iouint8(T& v) { uint8_t vv; io(vv); v = static_cast<T>(vv); }
	template <typename T> inline void iouint16(T& v) { uint16_t vv; io(vv); v = static_cast<T>(vv); }
	template <typename T> inline void iouint32(T& v) { uint32_t vv; io(vv); v = static_cast<T>(vv); }
	template <typename T> inline void iouint64(T& v) { uint64_t vv; io(vv); v = static_cast<T>(vv); }
	template <typename T> inline void io(std::atomic<T>& v) { T vv; io(vv); v = vv; }
	template <typename T> inline void ioleb(std::atomic<T>& v) { T vv; ioleb(vv); v = vv; }
	template <typename T> inline void ioint8(std::atomic<T>& v) { T vv; ioint8(vv); v = vv; }
	template <typename T> inline void ioint16(std::atomic<T>& v) { T vv; ioint16(vv); v = vv; }
	template <typename T> inline void ioint32(std::atomic<T>& v) { T vv; ioint32(vv); v = vv; }
	template <typename T> inline void ioint64(std::atomic<T>& v) { T vv; ioint64(vv); v = vv; }
	template <typename T> inline void iouint8(std::atomic<T>& v) { T vv; iouint8(vv); v = vv; }
	template <typename T> inline void iouint16(std::atomic<T>& v) { T vv; iouint16(vv); v = vv; }
	template <typename T> inline void iouint32(std::atomic<T>& v) { T vv; iouint32(vv); v = vv; }
	template <typename T> inline void iouint64(std::atomic<T>& v) { T vv; iouint64(vv); v = vv; }
	template <typename T> inline void ioenum(T& v) { typename std::underlying_type<T>::type vv; io(vv); v = static_cast<T>(vv); }

protected:
	const uint8_t *m_ptr;
	const uint8_t *m_end;
};

#endif /* HIBERNATE_H */
