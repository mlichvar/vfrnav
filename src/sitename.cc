//
// C++ Implementation: sitename
//
// Description:
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2017
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "sysdeps.h"

#include "sitename.h"

constexpr char SiteName::sitename[];
constexpr char SiteName::siteurlnoproto[];
constexpr char SiteName::siteurl[];
constexpr char SiteName::sitesecureurl[];
