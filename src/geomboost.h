//
// C++ Interface: geomboost
//
// Description:
//
//
// Author: Thomas Sailer <t.sailer@alumni.ethz.ch>, (C) 2007, 2009, 2012, 2020
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef GEOMBOOST_H
#define GEOMBOOST_H

#include "sysdeps.h"
#include "geom.h"

#ifdef HAVE_BOOST_GEOMETRY_HPP
#include <boost/config.hpp>
#include <boost/geometry.hpp>

// adapt Point to Boost.Geometry
namespace boost { namespace geometry { namespace traits {
	template<> struct tag<Point> { typedef point_tag type; };
	template<> struct coordinate_type<Point> { typedef double type; };
	//template<> struct coordinate_system<Point> { typedef cs::spherical_equatorial<radian> type; };
	template<> struct coordinate_system<Point> { typedef cs::geographic<radian> type; };
	template<> struct dimension<Point> : boost::mpl::int_<2> {};

	template<> struct access<Point, 0> {
                static inline double get(const Point& p) { return p.get_lon_rad_dbl(); }
                static inline void set(Point& p, double value) { p.set_lon_rad_dbl(value); }
	};

 	template<> struct access<Point, 1> {
                static inline double get(const Point& p) { return p.get_lat_rad_dbl(); }
                static inline void set(Point& p, double value) { p.set_lat_rad_dbl(value); }
	};
} } }

inline Point to_geom(const boost::geometry::model::point<boost::geometry::traits::coordinate_type<Point>::type, 2,
		     boost::geometry::traits::coordinate_system<Point>::type>& p) {
	Point pr;
	pr.set_lon_rad_dbl(p.get<0>());
	pr.set_lat_rad_dbl(p.get<1>());
	return pr;
}

// adapt Rect to Boost.Geometry
namespace boost { namespace geometry { namespace traits {
	template<> struct tag<Rect> { typedef box_tag type; };
	template<> struct point_type<Rect> { typedef Point type; };

	template<> struct indexed_access<Rect, min_corner, 0> {
		typedef Rect box_type;
		static inline double get(const box_type& b) { return b.get_southwest().get_lon_rad_dbl(); }
		static inline void set(box_type& b, double value) { b.get_southwest().set_lon_rad_dbl(value); }
	};

	template<> struct indexed_access<Rect, min_corner, 1> {
		typedef Rect box_type;
		static inline double get(const box_type& b) { return b.get_southwest().get_lat_rad_dbl(); }
		static inline void set(box_type& b, double value) { b.get_southwest().set_lat_rad_dbl(value); }
	};

	template<> struct indexed_access<Rect, max_corner, 0> {
		typedef Rect box_type;
		static inline double get(const box_type& b) { return b.get_northeast().get_lon_rad_dbl(); }
		static inline void set(box_type& b, double value) { b.get_northeast().set_lon_rad_dbl(value); }
	};

	template<> struct indexed_access<Rect, max_corner, 1> {
		typedef Rect box_type;
		static inline double get(const box_type& b) { return b.get_northeast().get_lat_rad_dbl(); }
		static inline void set(box_type& b, double value) { b.get_northeast().set_lat_rad_dbl(value); }
	};
} } }

inline Rect to_geom(const boost::geometry::model::box<boost::geometry::model::point<boost::geometry::traits::coordinate_type<Point>::type, 2,
		    boost::geometry::traits::coordinate_system<Point>::type> >& r) {
	Point p0, p1;
	p0.set_lon_rad_dbl(r.min_corner().get<0>());
	p0.set_lat_rad_dbl(r.min_corner().get<1>());
	p1.set_lon_rad_dbl(r.max_corner().get<0>());
	p1.set_lat_rad_dbl(r.max_corner().get<1>());
	return Rect(p0, p1);
}

// adapt LineString to Boost.Geometry
namespace boost { namespace geometry { namespace traits {
	template<> struct tag<LineString> { typedef linestring_tag type; };
	template<> struct point_type<LineString> { typedef Point type; };
} } }

namespace boost {
	template<> struct range_iterator<LineString> { typedef LineString::iterator type; };
	template<> struct range_const_iterator<LineString> { typedef LineString::const_iterator type; };
}

inline LineString::iterator range_begin(LineString& ls) { return ls.begin(); }
inline LineString::iterator range_end(LineString& ls) { return ls.end(); }
inline LineString::const_iterator range_begin(const LineString& ls) { return ls.begin(); }
inline LineString::const_iterator range_end(const LineString& ls) { return ls.end(); }

// adapt PolygonSimple to Boost.Geometry
namespace boost { namespace geometry { namespace traits {
	template<> struct tag<PolygonSimple> { typedef ring_tag type; };
	template<> struct point_type<PolygonSimple> { typedef Point type; };
	template<> struct point_order<PolygonSimple> { static const order_selector value = counterclockwise; };
	template<> struct closure<PolygonSimple> { static const closure_selector value = open; };
} } }

namespace boost {
	template<> struct range_iterator<PolygonSimple> { typedef PolygonSimple::iterator type; };
	template<> struct range_const_iterator<PolygonSimple> { typedef PolygonSimple::const_iterator type; };
}

inline PolygonSimple::iterator range_begin(PolygonSimple& ps) { return ps.begin(); }
inline PolygonSimple::iterator range_end(PolygonSimple& ps) { return ps.end(); }
inline PolygonSimple::const_iterator range_begin(const PolygonSimple& ps) { return ps.begin(); }
inline PolygonSimple::const_iterator range_end(const PolygonSimple& ps) { return ps.end(); }

// adapt PolygonHole to Boost.Geometry
namespace boost { namespace geometry { namespace traits {
	template<> struct tag<PolygonHole> { typedef polygon_tag type; };
	template<> struct point_type<PolygonHole> { typedef Point type; };
	template<> struct ring_const_type<PolygonHole> { typedef const PolygonSimple& type; };
	template<> struct ring_mutable_type<PolygonHole> { typedef PolygonSimple& type; };
	template<> struct interior_const_type<PolygonHole> { typedef const PolygonHole::interior_t& type; };
	template<> struct interior_mutable_type<PolygonHole> { typedef PolygonHole::interior_t& type; };

	template<> struct exterior_ring<PolygonHole> {
		static inline PolygonSimple& get(PolygonHole& p) { return p.get_exterior(); }
		static inline const PolygonSimple& get(PolygonHole const& p) { return p.get_exterior();	}
	};

	template<> struct interior_rings<PolygonHole> {
		static inline PolygonHole::interior_t& get(PolygonHole& p) { return p.get_interior(); }
		static inline const PolygonHole::interior_t& get(PolygonHole const& p) { return p.get_interior(); }
	};
} } }

namespace boost {
	template<> struct range_iterator<PolygonHole::interior_t> { typedef PolygonSimple::iterator type; };
	template<> struct range_const_iterator<PolygonHole::interior_t> { typedef PolygonSimple::const_iterator type; };
}

inline PolygonHole::interior_t::iterator range_begin(PolygonHole::interior_t& ps) { return ps.begin(); }
inline PolygonHole::interior_t::iterator range_end(PolygonHole::interior_t& ps) { return ps.end(); }
inline PolygonHole::interior_t::const_iterator range_begin(const PolygonHole::interior_t& ps) { return ps.begin(); }
inline PolygonHole::interior_t::const_iterator range_end(const PolygonHole::interior_t& ps) { return ps.end(); }

#endif /* HAVE_BOOST_GEOMETRY_HPP */

#endif /* GEOMBOOST_H */
