/*****************************************************************************/

/*
 *      acftcruise.cc  --  Aircraft Model Cruise Performance.
 *
 *      Copyright (C) 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020
 *        Thomas Sailer (t.sailer@alumni.ethz.ch)
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

/*****************************************************************************/

#include "aircraft.h"

#include <iostream>
#include <iomanip>

#ifdef HAVE_EIGEN3
#include <Eigen/Dense>
#include <Eigen/Cholesky>
#include <unsupported/Eigen/Polynomials>
#endif

#ifdef HAVE_JSONCPP
#include <json/json.h>
#endif

#ifdef HAVE_EIGEN3

namespace {

#ifdef __WIN32__
__attribute__((force_align_arg_pointer))
#endif

Eigen::VectorXd least_squares_solution(const Eigen::MatrixXd& A, const Eigen::VectorXd& b)
{
	if (false) {
		// traditional
		return (A.transpose() * A).inverse() * A.transpose() * b;
	} else if (true) {
		// cholesky
		return (A.transpose() * A).ldlt().solve(A.transpose() * b);
	} else if (false) {
		// householder QR
		return A.colPivHouseholderQr().solve(b);
	} else if (false) {
		// jacobi SVD
		return A.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(b);
	} else {
		typedef Eigen::JacobiSVD<Eigen::MatrixXd, Eigen::FullPivHouseholderQRPreconditioner> jacobi_t;
		jacobi_t jacobi(A, Eigen::ComputeFullU | Eigen::ComputeFullV);
		return jacobi.solve(b);
	}
}

};

#endif

Aircraft::Cruise::Point::Point(double pa, double bhp, double tas, double fuelflow)
	: m_pa(pa), m_bhp(bhp), m_tas(tas), m_fuelflow(fuelflow)
{
}

void Aircraft::Cruise::Point::load_xml(const xmlpp::Element *el, PistonPower& pp, double altfactor, double tasfactor, double fuelfactor, double isaoffs)
{
	if (!el)
		return;
	m_pa = m_bhp = m_tas = m_fuelflow = std::numeric_limits<double>::quiet_NaN();
	double rpm(std::numeric_limits<double>::quiet_NaN());
	double mp(std::numeric_limits<double>::quiet_NaN());
	xmlpp::Attribute *attr;
	if ((attr = el->get_attribute("pa")) && attr->get_value() != "")
		m_pa = Glib::Ascii::strtod(attr->get_value()) * altfactor;
	else if ((attr = el->get_attribute("da")) && attr->get_value() != "")
		m_pa = Glib::Ascii::strtod(attr->get_value()) * altfactor;
	if ((attr = el->get_attribute("rpm")) && attr->get_value() != "")
		rpm = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("mp")) && attr->get_value() != "")
		mp = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("bhp")) && attr->get_value() != "")
		m_bhp = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("tas")) && attr->get_value() != "")
		m_tas = Glib::Ascii::strtod(attr->get_value()) * tasfactor;
	if ((attr = el->get_attribute("fuelflow")) && attr->get_value() != "")
		m_fuelflow = Glib::Ascii::strtod(attr->get_value()) * fuelfactor;
	pp.add(m_pa, isaoffs, m_bhp, rpm, mp);
}

void Aircraft::Cruise::Point::save_xml(xmlpp::Element *el, double altfactor, double tasfactor, double fuelfactor) const
{
	if (!el)
		return;
	if (std::isnan(m_pa) || altfactor == 0) {
		el->remove_attribute("pa");
	} else {
		std::ostringstream oss;
		oss << (m_pa / altfactor);
		el->set_attribute("pa", oss.str());
	}
	if (std::isnan(m_bhp)) {
		el->remove_attribute("bhp");
	} else {
		std::ostringstream oss;
		oss << m_bhp;
		el->set_attribute("bhp", oss.str());
	}
	if (std::isnan(m_tas) || tasfactor == 0) {
		el->remove_attribute("tas");
	} else {
		std::ostringstream oss;
		oss << (m_tas / tasfactor);
		el->set_attribute("tas", oss.str());
	}
	if (std::isnan(m_fuelflow) || fuelfactor == 0) {
		el->remove_attribute("fuelflow");
	} else {
		std::ostringstream oss;
		oss << (m_fuelflow / fuelfactor);
		el->set_attribute("fuelflow", oss.str());
	}
}

int Aircraft::Cruise::Point::compare(const Point& x) const
{
	if (get_pressurealt() < x.get_pressurealt())
		return -1;
	if (x.get_pressurealt() < get_pressurealt())
		return 1;
	return 0;
}

Aircraft::Cruise::PointRPMMP::PointRPMMP(double pa, double rpm, double mp, double bhp, double tas, double fuelflow)
	: Point(pa, bhp, tas, fuelflow), m_rpm(rpm), m_mp(mp)
{
}

void Aircraft::Cruise::PointRPMMP::load_xml(const xmlpp::Element *el, PistonPower& pp, double altfactor, double tasfactor, double fuelfactor, double isaoffs)
{
	if (!el)
		return;
	m_pa = m_bhp = m_tas = m_fuelflow = m_rpm = m_mp = std::numeric_limits<double>::quiet_NaN();
	xmlpp::Attribute *attr;
	if ((attr = el->get_attribute("pa")) && attr->get_value() != "")
		m_pa = Glib::Ascii::strtod(attr->get_value()) * altfactor;
	else if ((attr = el->get_attribute("da")) && attr->get_value() != "")
		m_pa = Glib::Ascii::strtod(attr->get_value()) * altfactor;
	if ((attr = el->get_attribute("rpm")) && attr->get_value() != "")
		m_rpm = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("mp")) && attr->get_value() != "")
		m_mp = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("bhp")) && attr->get_value() != "")
		m_bhp = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("tas")) && attr->get_value() != "")
		m_tas = Glib::Ascii::strtod(attr->get_value()) * tasfactor;
	if ((attr = el->get_attribute("fuelflow")) && attr->get_value() != "")
		m_fuelflow = Glib::Ascii::strtod(attr->get_value()) * fuelfactor;
	pp.add(m_pa, isaoffs, m_bhp, m_rpm, m_mp);
}

void Aircraft::Cruise::PointRPMMP::save_xml(xmlpp::Element *el, double altfactor, double tasfactor, double fuelfactor) const
{
	if (!el)
		return;
	if (std::isnan(m_pa) || altfactor == 0) {
		el->remove_attribute("pa");
	} else {
		std::ostringstream oss;
		oss << (m_pa / altfactor);
		el->set_attribute("pa", oss.str());
	}
	if (std::isnan(m_rpm)) {
		el->remove_attribute("rpm");
	} else {
		std::ostringstream oss;
		oss << m_rpm;
		el->set_attribute("rpm", oss.str());
	}
	if (std::isnan(m_mp)) {
		el->remove_attribute("mp");
	} else {
		std::ostringstream oss;
		oss << m_mp;
		el->set_attribute("mp", oss.str());
	}
	if (std::isnan(m_bhp)) {
		el->remove_attribute("bhp");
	} else {
		std::ostringstream oss;
		oss << m_bhp;
		el->set_attribute("bhp", oss.str());
	}
	if (std::isnan(m_tas) || tasfactor == 0) {
		el->remove_attribute("tas");
	} else {
		std::ostringstream oss;
		oss << (m_tas / tasfactor);
		el->set_attribute("tas", oss.str());
	}
	if (std::isnan(m_fuelflow) || fuelfactor == 0) {
		el->remove_attribute("fuelflow");
	} else {
		std::ostringstream oss;
		oss << (m_fuelflow / fuelfactor);
		el->set_attribute("fuelflow", oss.str());
	}
}

const Aircraft::Cruise::Curve::FlagName Aircraft::Cruise::Curve::flagnames[] =
{
	{ "interpolate", flags_interpolate },
	{ "hold", flags_hold },
	{ 0, flags_none }
};

Aircraft::Cruise::Curve::Curve(const std::string& name, flags_t flags, double mass, double isaoffs, double rpm)
	: m_name(name), m_mass(mass), m_isaoffset(isaoffs), m_rpm(rpm), m_altfactor(1), m_tasfactor(1), m_fuelfactor(1), m_flags(flags)
{
}

void Aircraft::Cruise::Curve::add_point(const Point& pt)
{
	insert(pt);
}

void Aircraft::Cruise::Curve::clear_points(void)
{
	clear();
}

void Aircraft::Cruise::Curve::load_xml(const xmlpp::Element *el, PistonPower& pp, double altfactor, double tasfactor, double fuelfactor)
{
	clear();
	m_name.clear();
	m_mass = std::numeric_limits<double>::quiet_NaN();
	m_isaoffset = 0;
	m_rpm = std::numeric_limits<double>::quiet_NaN();
	m_altfactor = altfactor;
	m_tasfactor = tasfactor;
	m_fuelfactor = fuelfactor;
	m_flags = flags_none;
	xmlpp::Attribute *attr;
	if ((attr = el->get_attribute("name")))
		m_name = attr->get_value();
	if ((attr = el->get_attribute("mass")) && attr->get_value() != "")
		m_mass = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("isaoffset")) && attr->get_value() != "")
		m_isaoffset = Glib::Ascii::strtod(attr->get_value());
	else if ((attr = el->get_attribute("isaoffs")) && attr->get_value() != "")
		m_isaoffset = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("rpm")) && attr->get_value() != "")
		m_rpm = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("altfactor")) && attr->get_value() != "")
		m_altfactor = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("tasfactor")) && attr->get_value() != "")
		m_tasfactor = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("fuelfactor")) && attr->get_value() != "")
		m_fuelfactor = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("flags"))) {
		Glib::ustring val(attr->get_value());
		if (false)
			std::cerr << "Curve::load_xml: name " << get_name() << " mass " << get_mass()
				  << " isaoffset " << get_isaoffset() << " flags \"" << val << "\"" << std::endl;
		Glib::ustring::size_type vn(0);
		while (vn < val.size()) {
			Glib::ustring::size_type vn2(val.find('|', vn));
			Glib::ustring v1(val.substr(vn, vn2 - vn));
			vn = vn2;
			if (vn < val.size())
				++vn;
			if (false)
				std::cerr << "Curve::load_xml: name " << get_name() << " mass " << get_mass()
					  << " isaoffset " << get_isaoffset() << " flag \"" << v1 << "\"" << std::endl;
			if (v1.empty())
				continue;
			if (*std::min_element(v1.begin(), v1.end()) >= (Glib::ustring::value_type)'0' &&
			    *std::max_element(v1.begin(), v1.end()) <= (Glib::ustring::value_type)'9') {
				m_flags |= (flags_t)strtol(v1.c_str(), 0, 10);
				continue;
			}
			const FlagName *fn = flagnames;
			for (; fn->m_name; ++fn)
				if (fn->m_name == v1)
					break;
			if (!fn->m_name) {
				std::cerr << "Curve::load_xml: name " << get_name() << " mass " << get_mass()
					  << " isaoffset " << get_isaoffset() << " invalid flag " << v1 << std::endl;
				continue;
			}
			m_flags |= fn->m_flag;
		}
	}
	{
		std::pair<double,double> rpmr(std::numeric_limits<double>::max(), -std::numeric_limits<double>::max());
		xmlpp::Node::NodeList nl(el->get_children("point"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni) {
			PointRPMMP pt;
			pt.load_xml(static_cast<xmlpp::Element *>(*ni), pp, m_altfactor, m_tasfactor, m_fuelfactor);
			if (std::isnan(pt.get_pressurealt()))
				continue;
			if (std::isnan(pt.get_rpm())) {
				rpmr.first = rpmr.second = std::numeric_limits<double>::quiet_NaN();
			} else if (!std::isnan(rpmr.first) && !std::isnan(rpmr.second)) {
				rpmr.first = std::min(rpmr.first, pt.get_rpm());
				rpmr.second = std::max(rpmr.second, pt.get_rpm());
			}
			insert(pt);
		}
		if (!std::isnan(rpmr.first) && !std::isnan(rpmr.second) && std::isnan(m_rpm) && rpmr.first > 0 && rpmr.second <= rpmr.first * 1.01)
			m_rpm = rpmr.first;
	}
	{
		std::pair<double,double> bhp(get_bhp_range());
		if (std::isnan(bhp.first) || std::isnan(bhp.second))
			m_flags &= ~flags_interpolate;
	}
}

void Aircraft::Cruise::Curve::save_xml(xmlpp::Element *el, double altfactor, double tasfactor, double fuelfactor) const
{
	if (!el)
		return;
	{
		xmlpp::Node::NodeList nl(el->get_children());
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni)
			el->remove_child(*ni);
	}
	if (m_name.empty())
		el->remove_attribute("name");
	else
		el->set_attribute("name", m_name);
	if (std::isnan(m_mass)) {
		el->remove_attribute("mass");
	} else {
		std::ostringstream oss;
		oss << m_mass;
		el->set_attribute("mass", oss.str());
	}
	if (m_isaoffset == 0) {
		el->remove_attribute("isaoffset");
	} else {
		std::ostringstream oss;
		oss << m_isaoffset;
		el->set_attribute("isaoffset", oss.str());
	}
	if (std::isnan(m_rpm)) {
		el->remove_attribute("rpm");
	} else {
		std::ostringstream oss;
		oss << m_rpm;
		el->set_attribute("rpm", oss.str());
	}
	if (m_altfactor == altfactor) {
		el->remove_attribute("altfactor");
	} else {
		std::ostringstream oss;
		oss << m_altfactor;
		el->set_attribute("altfactor", oss.str());
	}
	if (m_tasfactor == tasfactor) {
		el->remove_attribute("tasfactor");
	} else {
		std::ostringstream oss;
		oss << m_tasfactor;
		el->set_attribute("tasfactor", oss.str());
	}
	if (m_fuelfactor == fuelfactor) {
		el->remove_attribute("fuelfactor");
	} else {
		std::ostringstream oss;
		oss << m_fuelfactor;
		el->set_attribute("fuelfactor", oss.str());
	}
	{
		Glib::ustring flg;
		flags_t flags(m_flags);
		for (const FlagName *fn = flagnames; fn->m_name; ++fn) {
			if (fn->m_flag & ~flags)
				continue;
			flags &= ~fn->m_flag;
			flg += "|";
			flg += fn->m_name;
		}
		if (flags) {
			std::ostringstream oss;
			oss << '|' << (unsigned int)flags;
			flg += oss.str();
		}
		if (flg.empty())
			el->remove_attribute("flags");
		else
			el->set_attribute("flags", flg.substr(1));
	}
	for (const_iterator pi(begin()), pe(end()); pi != pe; ++pi)
		pi->save_xml(el->add_child("point"), m_altfactor, m_tasfactor, m_fuelfactor);
}

#ifdef HAVE_JSONCPP

bool Aircraft::Cruise::Curve::load_garminpilot(const Json::Value& root, const Aircraft& acft, unsigned int version)
{
	if (root.isMember("fuelMeasureType")) {
		const Json::Value& fmt(root["fuelMeasureType"]);
		if (fmt.isString()) {
			if (fmt.asString() == "volume") {
				// ??
			} else if (fmt.asString() == "weight") {
				// ??
			}
		}
	}
	double pwrmul(1);
	if (root.isMember("powerSettingType")) {
		const Json::Value& pst(root["powerSettingType"]);
		if (pst.isString()) {
			if (pst.asString() == "percentPower") {
				pwrmul = acft.get_maxbhp();
			}
		}
	}
	if (root.isMember("samples")) {
		const Json::Value& samp(root["samples"]);
		if (samp.isArray()) {
			for (Json::ArrayIndex i = 0; i < samp.size(); ++i) {
				const Json::Value& samp1(samp[i]);
				if (!samp1.isObject())
					continue;
				double pa(std::numeric_limits<double>::quiet_NaN());
				double bhp(std::numeric_limits<double>::quiet_NaN());
				double tas(std::numeric_limits<double>::quiet_NaN());
				double fuelflow(std::numeric_limits<double>::quiet_NaN());
				if (samp1.isMember("altitude")) {
					const Json::Value& x(root["altitude"]);
					if (x.isNumeric())
						pa = x.asDouble();
				}
				if (samp1.isMember("trueAirspeed")) {
					const Json::Value& x(root["trueAirspeed"]);
					if (x.isNumeric())
						tas = x.asDouble();
				}
				if (samp1.isMember("fuelFlow")) {
					const Json::Value& x(root["fuelFlow"]);
					if (x.isNumeric())
						fuelflow = acft.convert_fuel(unit_lb, acft.get_fuelunit(), x.asDouble());
				}
				if (samp1.isMember("powerSetting")) {
					const Json::Value& x(root["powerSetting"]);
					if (x.isNumeric())
						bhp = x.asDouble() * pwrmul;
				}
				if (!std::isnan(pa) && !std::isnan(tas))
					insert(Point(pa, bhp, tas, fuelflow));
			}
		}
	}
	return !empty();
}

#endif

int Aircraft::Cruise::Curve::compare(const Curve& x) const
{
	return get_name().compare(x.get_name());
}

std::pair<double,double> Aircraft::Cruise::Curve::get_bhp_range(void) const
{
	std::pair<double,double> r(std::numeric_limits<double>::max(), -std::numeric_limits<double>::max());
	for (const_iterator i(begin()), e(end()); i != e; ++i) {
		if (std::isnan(i->get_bhp()))
			return std::pair<double,double>(std::numeric_limits<double>::quiet_NaN(),
							std::numeric_limits<double>::quiet_NaN());
		r.first = std::min(r.first, i->get_bhp());
		r.second = std::max(r.second, i->get_bhp());
	}
	if (r.first > r.second)
		return std::pair<double,double>(std::numeric_limits<double>::quiet_NaN(),
						std::numeric_limits<double>::quiet_NaN());
	return r;
}

bool Aircraft::Cruise::Curve::is_bhp_const(void) const
{
	std::pair<double,double> r(get_bhp_range());
	if (std::isnan(r.first) || r.first <= 0 || r.second > r.first * 1.01)
		return false;
	return true;
}

void Aircraft::Cruise::Curve::calculate(double& tas, double& fuelflow, double& pa, CruiseEngineParams& ep) const
{
	static constexpr bool debug = false;
	ep.set_rpm(std::numeric_limits<double>::quiet_NaN());
	ep.set_mp(std::numeric_limits<double>::quiet_NaN());
	if (std::isnan(pa) || empty()) {
		tas = fuelflow = std::numeric_limits<double>::quiet_NaN();
		ep.set_bhp(std::numeric_limits<double>::quiet_NaN());
		if (debug)
			std::cerr << "Cruise::Curve::calculate: name " << get_name() << " mass " << get_mass() << " isaoffs " << get_isaoffset()
				  << " pa " << pa << " tas " << tas << " ff " << fuelflow << " bhp " << ep.get_bhp() << std::endl;
		return;
	}
	const_iterator i(lower_bound(Point(pa)));
	if (i == begin() || i == end()) {
		if (i == end())
			--i;
		tas = i->get_tas();
		fuelflow = i->get_fuelflow();
		pa = i->get_pressurealt();
		ep.set_bhp(i->get_bhp());
		ep.set_rpm(get_rpm());
		if (debug)
			std::cerr << "Cruise::Curve::calculate: name " << get_name() << " mass " << get_mass() << " isaoffs " << get_isaoffset()
				  << " pa " << pa << " tas " << tas << " ff " << fuelflow << " bhp " << ep.get_bhp() << std::endl;
		return;
	}
	const_iterator i0(i);
	--i0;
	double x((pa - i0->get_pressurealt()) / (i->get_pressurealt() - i0->get_pressurealt()));
	double x0(1.0 - x);
	tas = i->get_tas() * x + i0->get_tas() * x0;
	fuelflow = i->get_fuelflow() * x + i0->get_fuelflow() * x0;
	ep.set_bhp(i->get_bhp() * x + i0->get_bhp() * x0);
	ep.set_rpm(get_rpm());
	if (debug)
		std::cerr << "Cruise::Curve::calculate: name " << get_name() << " mass " << get_mass() << " isaoffs " << get_isaoffset()
			  << " pa " << pa << " tas " << tas << " ff " << fuelflow << " bhp " << ep.get_bhp()
			  << " pa0 " << i0->get_pressurealt() << " (" << x0 << ") pa1 " << i->get_pressurealt() << " (" << x << ')'
			  << " tas0 " << i0->get_tas() << " tas1 " << i->get_tas()
			  << " ff0 " << i0->get_fuelflow() << " ff1 " << i->get_fuelflow() << std::endl;
}

void Aircraft::Cruise::Curve::set_mass(double mass)
{
	if (std::isnan(mass) || mass <= 0)
		return;
	m_mass = mass;
}

Aircraft::CheckErrors Aircraft::Cruise::Curve::check(double minmass, double maxmass) const
{
	CheckErrors r;
	if (get_mass() < minmass * 0.9)
		r.add(*this, CheckError::severity_warning) << "mass less than empty mass - 10%";
	if (get_mass() > maxmass)
		r.add(*this, CheckError::severity_warning) << "mass more than maximum takeoff mass";
	if (empty()) {
		r.add(*this, CheckError::severity_error) << "no points";
		return r;
	}
	if (begin()->get_pressurealt() > 2000)
		r.add(*this, CheckError::severity_error) << "does not include a low level point";
	return r;
}

Aircraft::Cruise::CurveRPMMP::CurveRPMMP(const std::string& name)
	: m_name(name)
{
}

void Aircraft::Cruise::CurveRPMMP::load_xml(const xmlpp::Element *el, PistonPower& pp, double altfactor, double tasfactor, double fuelfactor)
{
	clear();
	m_name.clear();
	xmlpp::Attribute *attr;
	if ((attr = el->get_attribute("name")))
		m_name = attr->get_value();
	xmlpp::Node::NodeList nl(el->get_children("point"));
	for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni) {
		PointRPMMP pt;
		pt.load_xml(static_cast<xmlpp::Element *>(*ni), pp, altfactor, tasfactor, fuelfactor);
		if (std::isnan(pt.get_pressurealt()))
			continue;
		insert(pt);
	}
}

void Aircraft::Cruise::CurveRPMMP::save_xml(xmlpp::Element *el, double altfactor, double tasfactor, double fuelfactor) const
{
	if (!el)
		return;
	{
		xmlpp::Node::NodeList nl(el->get_children());
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni)
			el->remove_child(*ni);
	}
	if (m_name.empty())
		el->remove_attribute("name");
	else
		el->set_attribute("name", m_name);
	for (const_iterator pi(begin()), pe(end()); pi != pe; ++pi)
		pi->save_xml(el->add_child("point"));
}

bool Aircraft::Cruise::CurveRPMMP::is_mp(void) const
{
	for (const_iterator i(begin()), e(end()); i != e; ++i)
		if (std::isnan(i->get_mp()))
			return false;
	return !empty();
}

std::pair<double,double> Aircraft::Cruise::CurveRPMMP::get_bhp_range(void) const
{
	std::pair<double,double> r(std::numeric_limits<double>::max(), -std::numeric_limits<double>::max());
	for (const_iterator i(begin()), e(end()); i != e; ++i) {
		if (std::isnan(i->get_bhp()))
			return std::pair<double,double>(std::numeric_limits<double>::quiet_NaN(),
							std::numeric_limits<double>::quiet_NaN());
		r.first = std::min(r.first, i->get_bhp());
		r.second = std::max(r.second, i->get_bhp());
	}
	if (r.first > r.second)
		return std::pair<double,double>(std::numeric_limits<double>::quiet_NaN(),
						std::numeric_limits<double>::quiet_NaN());
	return r;
}

std::pair<double,double> Aircraft::Cruise::CurveRPMMP::get_rpm_range(void) const
{
	std::pair<double,double> r(std::numeric_limits<double>::max(), -std::numeric_limits<double>::max());
	for (const_iterator i(begin()), e(end()); i != e; ++i) {
		if (std::isnan(i->get_rpm()))
			return std::pair<double,double>(std::numeric_limits<double>::quiet_NaN(),
							std::numeric_limits<double>::quiet_NaN());
		r.first = std::min(r.first, i->get_rpm());
		r.second = std::max(r.second, i->get_rpm());
	}
	if (r.first > r.second)
		return std::pair<double,double>(std::numeric_limits<double>::quiet_NaN(),
						std::numeric_limits<double>::quiet_NaN());
	return r;
}

bool Aircraft::Cruise::CurveRPMMP::is_bhp_const(void) const
{
	std::pair<double,double> r(get_bhp_range());
	if (std::isnan(r.first) || r.first <= 0 || r.second > r.first * 1.01)
		return false;
	return true;
}

bool Aircraft::Cruise::CurveRPMMP::is_rpm_const(void) const
{
	std::pair<double,double> r(get_rpm_range());
	if (std::isnan(r.first) || r.first <= 0 || r.second > r.first * 1.01)
		return false;
	return true;
}

Aircraft::Cruise::CurveRPMMP::operator Curve(void) const
{
	double rpm(std::numeric_limits<double>::quiet_NaN());
	if (is_rpm_const())
		rpm = get_rpm_range().first;
	Curve c(get_name(), Curve::flags_interpolate,
		std::numeric_limits<double>::quiet_NaN(), 0, rpm);
	for (const_iterator i(begin()), e(end()); i != e; ++i)
		c.insert(*i);
	return c;
}

Aircraft::Cruise::OptimalAltitudePoint::OptimalAltitudePoint(double mass, double isaoffs, double pa)
	: m_pa(pa), m_mass(mass), m_isaoffs(isaoffs)
{
}

void Aircraft::Cruise::OptimalAltitudePoint::load_xml(const xmlpp::Element *el, double altfactor, double massfactor)
{
	if (!el)
		return;
	m_pa = m_mass = m_isaoffs = std::numeric_limits<double>::quiet_NaN();
	xmlpp::Attribute *attr;
	if ((attr = el->get_attribute("pa")) && attr->get_value() != "")
		m_pa = Glib::Ascii::strtod(attr->get_value()) * altfactor;
	if ((attr = el->get_attribute("mass")) && attr->get_value() != "")
		m_mass = Glib::Ascii::strtod(attr->get_value()) * massfactor;
	if ((attr = el->get_attribute("isaoffset")) && attr->get_value() != "")
		m_isaoffs = Glib::Ascii::strtod(attr->get_value());
}

void Aircraft::Cruise::OptimalAltitudePoint::save_xml(xmlpp::Element *el, double altfactor, double massfactor) const
{
	if (!el)
		return;
	if (std::isnan(m_pa) || altfactor == 0) {
		el->remove_attribute("pa");
	} else {
		std::ostringstream oss;
		oss << (m_pa / altfactor);
		el->set_attribute("pa", oss.str());
	}
	if (std::isnan(m_mass) || massfactor == 0) {
		el->remove_attribute("mass");
	} else {
		std::ostringstream oss;
		oss << (m_mass / massfactor);
		el->set_attribute("mass", oss.str());
	}
	if (std::isnan(m_isaoffs)) {
		el->remove_attribute("isaoffset");
	} else {
		std::ostringstream oss;
		oss << m_isaoffs;
		el->set_attribute("isaoffset", oss.str());
	}
}

int Aircraft::Cruise::OptimalAltitudePoint::compare(const OptimalAltitudePoint& x) const
{
	if (get_mass() < x.get_mass())
		return -1;
	if (x.get_mass() < get_mass())
		return 1;
	if (get_isaoffs() < x.get_isaoffs())
		return -1;
	if (x.get_isaoffs() < get_isaoffs())
		return 1;
	return 0;
}

Aircraft::Cruise::OptimalAltitude::OptimalAltitude(void)
{
}

#ifdef __WIN32__
__attribute__((force_align_arg_pointer))
#endif
bool Aircraft::Cruise::OptimalAltitude::recalculatepoly(bool force)
{
#ifdef HAVE_EIGEN3
	if (!force && !m_isaoffspoly.empty())
		return false;
	if (empty()) {
		m_isaoffspoly.clear();
		return false;
	}
	std::set<double> isaoffs;
	for (const_iterator i(begin()), e(end()); i != e; ++i) {
		if (std::isnan(i->get_isaoffs()) || std::isnan(i->get_mass()) || std::isnan(i->get_pressurealt()))
			continue;
		isaoffs.insert(i->get_isaoffs());
	}
	for (std::set<double>::const_iterator ioi(isaoffs.begin()), ioe(isaoffs.end()); ioi != ioe; ++ioi) {
		unsigned int polyorder(4);
		unsigned int pt(0);
		Eigen::MatrixXd m(size(), polyorder);
		Eigen::VectorXd v(size());
		for (const_iterator i(begin()), e(end()); i != e; ++i) {
			if (i->get_isaoffs() != *ioi || std::isnan(i->get_mass()) || std::isnan(i->get_pressurealt()))
				continue;
			double mass(i->get_mass()), mass1(1);
			for (unsigned int i = 0; i < polyorder; ++i, mass1 *= mass)
				m(pt, i) = mass1;
			v(pt) = i->get_pressurealt();
                        ++pt;
		}
		if (!pt)
			continue;
		m.conservativeResize(pt, std::min(pt, polyorder));
		v.conservativeResize(pt);
		Eigen::VectorXd x(least_squares_solution(m, v));
		poly_t p(x.data(), x.data() + x.size());
		m_isaoffspoly.insert(isaoffspoly_t::value_type(*ioi, p));
	}
	return true;
#else
	return false;
#endif
}

Aircraft::CheckErrors Aircraft::Cruise::OptimalAltitude::check(double minmass, double maxmass) const
{
	CheckErrors r;
	for (const_iterator di(begin()), de(end()); di != de; ++di) {
		if (di->get_mass() < minmass * 0.9)
			r.add(*di, CheckError::severity_warning) << "mass less than empty mass - 10%";
		if (di->get_mass() > maxmass)
			r.add(*di, CheckError::severity_warning) << "mass greater than maximum takeoff mass";
	}
	for (isaoffspoly_t::const_iterator pi(m_isaoffspoly.begin()), pe(m_isaoffspoly.end()); pi != pe; ++pi) {
		poly_t pderiv(pi->second.differentiate());
		double maltlmin(std::numeric_limits<double>::max()), maltlmax(std::numeric_limits<double>::min());
		double malthmin(maltlmin), malthmax(maltlmax), mderivmin(maltlmin), mderivmax(maltlmax);
		for (double mass(minmass), minc(0.001 * 0.5 * (minmass + maxmass)); mass <= maxmass; mass += minc) {
			if (pi->second.eval(mass) < 2000) {
				maltlmin = std::min(maltlmin, mass);
				maltlmax = std::max(maltlmax, mass);
			}
			if (pi->second.eval(mass) > 99900) {
				malthmin = std::min(malthmin, mass);
				malthmax = std::max(malthmax, mass);
			}
			if (pderiv.eval(mass) > 0) {
				mderivmin = std::min(mderivmin, mass);
				mderivmax = std::min(mderivmax, mass);
			}
		}
		if (maltlmin <= maltlmax)
			r.add(*this, pi->first, CheckError::severity_error) << "optimal altitude less than FL020, mass "
			<< maltlmin << "..." << maltlmax;
		if (malthmin <= malthmax)
			r.add(*this, pi->first, CheckError::severity_error) << "optimal altitude less than FL020, mass "
			<< malthmin << "..." << malthmax;
		if (mderivmin <= mderivmax)
			r.add(*this, pi->first, CheckError::severity_error) << "optimal altitude derivative positive, mass "
			<< mderivmin << "..." << mderivmax;
	}
	return r;
}

Aircraft::Cruise::OptimalAltitude::poly_t Aircraft::Cruise::OptimalAltitude::calculate(double isaoffs) const
{
	if (std::isnan(isaoffs) || m_isaoffspoly.empty())
		return poly_t();
	isaoffspoly_t::const_iterator iu(m_isaoffspoly.lower_bound(isaoffs));
	if (iu == m_isaoffspoly.begin())
		return iu->second;
	isaoffspoly_t::const_iterator il(iu);
	--il;
	if (iu == m_isaoffspoly.end())
		return il->second;
	double t(isaoffs - il->first);
	t /= (iu->first - il->first);
	return iu->second * t + il->second * (1 - t);
}

void Aircraft::Cruise::OptimalAltitude::load_xml(const xmlpp::Element *el, double altfactor, double massfactor)
{
	if (!el)
		return;
	m_isaoffspoly.clear();
	clear();
	{
		xmlpp::Node::NodeList nl(el->get_children("point"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni) {
			OptimalAltitudePoint p;
			p.load_xml(static_cast<xmlpp::Element *>(*ni), altfactor, massfactor);
			insert(p);
		}
	}
	{
		xmlpp::Node::NodeList nl(el->get_children("poly"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni) {
			xmlpp::Element *el(static_cast<xmlpp::Element *>(*ni));
			double isaoffs(std::numeric_limits<double>::quiet_NaN());
			xmlpp::Attribute *attr;
			if ((attr = el->get_attribute("isaoffset")))
				isaoffs = Glib::Ascii::strtod(attr->get_value());
			poly_t p;
			p.load_xml(el);
			if (!std::isnan(isaoffs))
				m_isaoffspoly.insert(isaoffspoly_t::value_type(isaoffs, p));
		}
 	}
}

void Aircraft::Cruise::OptimalAltitude::save_xml(xmlpp::Element *el, double altfactor, double massfactor) const
{
	if (!el)
		return;
	for (const_iterator di(begin()), de(end()); di != de; ++di)
		di->save_xml(el->add_child("point"), altfactor, massfactor);
	{
		xmlpp::Node::NodeList nl(el->get_children("poly"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni)
			el->remove_child(*ni);
	}
	for (isaoffspoly_t::const_iterator pi(m_isaoffspoly.begin()), pe(m_isaoffspoly.end()); pi != pe; ++pi) {
		if (std::isnan(pi->first) || pi->second.empty())
			continue;
		xmlpp::Element *el2(el->add_child("poly"));
		{
			std::ostringstream oss;
			oss << pi->first;
			el->set_attribute("isaoffset", oss.str());
		}
		pi->second.save_xml(el2);
	}
}

Aircraft::Cruise::CruiseEngineParams::CruiseEngineParams(double bhp, double rpm, double mp, const std::string& name, Curve::flags_t flags)
	: m_name(name), m_bhp(bhp), m_rpm(rpm), m_mp(mp), m_flags(flags)
{
}

Aircraft::Cruise::CruiseEngineParams::CruiseEngineParams(const std::string& name, Curve::flags_t flags)
	: m_name(name), m_bhp(std::numeric_limits<double>::quiet_NaN()), m_rpm(std::numeric_limits<double>::quiet_NaN()),
	  m_mp(std::numeric_limits<double>::quiet_NaN()), m_flags(flags)
{
}

bool Aircraft::Cruise::CruiseEngineParams::is_unset(void) const
{
	return std::isnan(get_bhp()) && std::isnan(get_rpm()) && std::isnan(get_mp()) &&
		get_name().empty() && (get_flags() & ~Curve::flags_interpolate) == Curve::flags_none;
}

int Aircraft::Cruise::CruiseEngineParams::compare(const CruiseEngineParams& x) const
{
	int c(get_name().compare(x.get_name()));
	if (c)
		return c;
	if (std::isnan(get_bhp())) {
		if (!std::isnan(x.get_bhp()))
			return -1;
	} else {
		if (std::isnan(x.get_bhp()))
			return 1;
		if (get_bhp() < x.get_bhp())
			return -1;
		if (x.get_bhp() < get_bhp())
			return 1;
	}
	if (std::isnan(get_rpm())) {
		if (!std::isnan(x.get_rpm()))
			return -1;
	} else {
		if (std::isnan(x.get_rpm()))
			return 1;
		if (get_rpm() < x.get_rpm())
			return -1;
		if (x.get_rpm() < get_rpm())
			return 1;
	}
	if (std::isnan(get_mp())) {
		if (!std::isnan(x.get_mp()))
			return -1;
	} else {
		if (std::isnan(x.get_mp()))
			return 1;
		if (get_mp() < x.get_mp())
			return -1;
		if (x.get_mp() < get_mp())
			return 1;
	}
	if (get_flags() < x.get_flags())
		return -1;
	if (x.get_flags() < get_flags())
		return 1;
	return 0;
}

std::ostream& Aircraft::Cruise::CruiseEngineParams::print(std::ostream& os) const
{
	bool any(false);
	if (!std::isnan(get_bhp())) {
		os << "bhp " << get_bhp();
		any = true;
	}
	if (!std::isnan(get_rpm())) {
		if (any)
			os << ' ';
		os << "rpm " << get_rpm();
		any = true;
	}
	if (!std::isnan(get_mp())) {
		if (any)
			os << ' ';
		os << "mp " << get_mp();
		any = true;
	}
	if (!get_name().empty()) {
		if (any)
			os << ' ';
		os << "name " << get_name();
		any = true;
	}
	if (!any)
		os << '-';
	if (Curve::flags_interpolate & get_flags())
		os << " interpolate";
	if (Curve::flags_hold & get_flags())
		os << " hold";
	return os;
}

std::string Aircraft::Cruise::CruiseEngineParams::to_lua(void) const
{
	std::ostringstream oss;
	oss << "bhp = " << to_luastring(get_bhp())
	    << ", rpm = " << to_luastring(get_rpm())
	    << ", mp = " << to_luastring(get_mp())
	    << ", name = " << to_luastring(get_name())
	    << ", interpolate = " << to_luastring((bool)(get_flags() & Curve::flags_interpolate))
	    << ", hold = " << to_luastring((bool)(get_flags() & Curve::flags_hold));
	return oss.str();
}

Aircraft::Cruise::EngineParams::EngineParams(double bhp, double rpm, double mp, const std::string& name, Curve::flags_t flags,
					     const std::string& climbname, const std::string& descentname)
	: CruiseEngineParams(bhp, rpm, mp, name, flags), m_climbname(climbname), m_descentname(descentname)
{
}

Aircraft::Cruise::EngineParams::EngineParams(const std::string& name, Curve::flags_t flags,
					     const std::string& climbname, const std::string& descentname)
	: CruiseEngineParams(name, flags), m_climbname(climbname), m_descentname(descentname)
{
}

bool Aircraft::Cruise::EngineParams::is_unset(void) const
{
	return get_climbname().empty() && get_descentname().empty() && CruiseEngineParams::is_unset();
}

int Aircraft::Cruise::EngineParams::compare(const EngineParams& x) const
{
	int c(get_name().compare(x.get_name()));
	if (c)
		return c;
	c = get_climbname().compare(x.get_climbname());
	if (c)
		return c;
	c = get_descentname().compare(x.get_descentname());
	if (c)
		return c;
	if (std::isnan(get_bhp())) {
		if (!std::isnan(x.get_bhp()))
			return -1;
	} else {
		if (std::isnan(x.get_bhp()))
			return 1;
		if (get_bhp() < x.get_bhp())
			return -1;
		if (x.get_bhp() < get_bhp())
			return 1;
	}
	if (std::isnan(get_rpm())) {
		if (!std::isnan(x.get_rpm()))
			return -1;
	} else {
		if (std::isnan(x.get_rpm()))
			return 1;
		if (get_rpm() < x.get_rpm())
			return -1;
		if (x.get_rpm() < get_rpm())
			return 1;
	}
	if (std::isnan(get_mp())) {
		if (!std::isnan(x.get_mp()))
			return -1;
	} else {
		if (std::isnan(x.get_mp()))
			return 1;
		if (get_mp() < x.get_mp())
			return -1;
		if (x.get_mp() < get_mp())
			return 1;
	}
	if (get_flags() < x.get_flags())
		return -1;
	if (x.get_flags() < get_flags())
		return 1;
	return 0;
}

std::ostream& Aircraft::Cruise::EngineParams::print(std::ostream& os) const
{
	bool any(false);
	if (!std::isnan(get_bhp())) {
		os << "bhp " << get_bhp();
		any = true;
	}
	if (!std::isnan(get_rpm())) {
		if (any)
			os << ' ';
		os << "rpm " << get_rpm();
		any = true;
	}
	if (!std::isnan(get_mp())) {
		if (any)
			os << ' ';
		os << "mp " << get_mp();
		any = true;
	}
	if (!get_name().empty()) {
		if (any)
			os << ' ';
		os << "name " << get_name();
		any = true;
	}
	if (!get_climbname().empty()) {
		if (any)
			os << ' ';
		os << "climb " << get_climbname();
		any = true;
	}
	if (!get_descentname().empty()) {
		if (any)
			os << ' ';
		os << "descent " << get_descentname();
		any = true;
	}
	if (!any)
		os << '-';
	if (Curve::flags_interpolate & get_flags())
		os << " interpolate";
	if (Curve::flags_hold & get_flags())
		os << " hold";
	return os;
}

std::string Aircraft::Cruise::EngineParams::to_lua(void) const
{
	std::ostringstream oss;
	oss << CruiseEngineParams::to_lua()
	    << ", climbname = " << to_luastring(get_climbname())
	    << ", descentname = " << to_luastring(get_descentname());
	return oss.str();
}

Aircraft::Cruise::PistonPowerBHPRPM::PistonPowerBHPRPM(double bhp, double rpm)
	: m_bhp(bhp), m_rpm(rpm)
{
}

int Aircraft::Cruise::PistonPowerBHPRPM::compare(const PistonPowerBHPRPM& x) const
{
	if (get_bhp() < x.get_bhp())
		return -1;
	if (x.get_bhp() < get_bhp())
		return 1;
	if (get_rpm() < x.get_rpm())
		return -1;
	if (x.get_rpm() < get_rpm())
		return 1;
	return 0;
}

void Aircraft::Cruise::PistonPowerBHPRPM::save_xml(xmlpp::Element *el, double alt, double isaoffs, double bhpfactor) const
{
	if (!el)
		return;
	el = el->add_child("point");
	if (std::isnan(alt)) {
		el->remove_attribute("pa");
	} else {
		std::ostringstream oss;
		oss << alt;
		el->set_attribute("pa", oss.str());
	}
	if (std::isnan(isaoffs)) {
		el->remove_attribute("isaoffs");
	} else {
		std::ostringstream oss;
		oss << isaoffs;
		el->set_attribute("isaoffs", oss.str());
	}
	if (std::isnan(get_bhp()) || bhpfactor == 0) {
		el->remove_attribute("bhp");
	} else {
		std::ostringstream oss;
		oss << (get_bhp() / bhpfactor);
		el->set_attribute("bhp", oss.str());
	}
	if (std::isnan(get_rpm())) {
		el->remove_attribute("rpm");
	} else {
		std::ostringstream oss;
		oss << get_rpm();
		el->set_attribute("rpm", oss.str());
	}
}

Aircraft::Cruise::PistonPowerBHPMP::PistonPowerBHPMP(double bhp, double mp)
	: m_bhp(bhp), m_mp(mp)
{
}

int Aircraft::Cruise::PistonPowerBHPMP::compare(const PistonPowerBHPMP& x) const
{
	if (get_bhp() < x.get_bhp())
		return -1;
	if (x.get_bhp() < get_bhp())
		return 1;
	if (get_mp() < x.get_mp())
		return -1;
	if (x.get_mp() < get_mp())
		return 1;
	return 0;
}

void Aircraft::Cruise::PistonPowerBHPMP::save_xml(xmlpp::Element *el, double alt, double isaoffs, double rpm, double bhpfactor) const
{
	if (!el)
		return;
	el = el->add_child("point");
	if (std::isnan(alt)) {
		el->remove_attribute("pa");
	} else {
		std::ostringstream oss;
		oss << alt;
		el->set_attribute("pa", oss.str());
	}
	if (std::isnan(isaoffs)) {
		el->remove_attribute("isaoffs");
	} else {
		std::ostringstream oss;
		oss << isaoffs;
		el->set_attribute("isaoffs", oss.str());
	}
	if (std::isnan(get_bhp()) || bhpfactor == 0) {
		el->remove_attribute("bhp");
	} else {
		std::ostringstream oss;
		oss << (get_bhp() / bhpfactor);
		el->set_attribute("bhp", oss.str());
	}
	if (std::isnan(rpm)) {
		el->remove_attribute("rpm");
	} else {
		std::ostringstream oss;
		oss << rpm;
		el->set_attribute("rpm", oss.str());
	}
	if (std::isnan(get_mp())) {
		el->remove_attribute("mp");
	} else {
		std::ostringstream oss;
		oss << get_mp();
		el->set_attribute("mp", oss.str());
	}
}

Aircraft::Cruise::PistonPowerRPM::PistonPowerRPM(double rpm)
	: m_rpm(rpm)
{
}

int Aircraft::Cruise::PistonPowerRPM::compare(const PistonPowerRPM& x) const
{
	if (get_rpm() < x.get_rpm())
		return -1;
	if (x.get_rpm() < get_rpm())
		return 1;
	return 0;
}

void Aircraft::Cruise::PistonPowerRPM::calculate(double& bhp, double& mp) const
{
	if (!std::isnan(bhp)) {
		const_iterator iu(lower_bound(PistonPowerBHPMP(bhp, 0)));
		if (iu == end()) {
			if (iu == begin()) {
				bhp = mp = std::numeric_limits<double>::quiet_NaN();
				return;
			}
			--iu;
			bhp = iu->get_bhp();
			mp = iu->get_mp();
			return;
		}
		const_iterator il(iu);
		if (iu == begin()) {
			++iu;
			if (iu == end()) {
				bhp = il->get_bhp();
				mp = il->get_mp();
				return;
			}
		} else {
			--il;
		}
		double t((bhp - il->get_bhp()) / (iu->get_bhp() - il->get_bhp()));
		if (t <= 0) {
			bhp = il->get_bhp();
			mp = il->get_mp();
			return;
		}
		if (t >= 1) {
			bhp = iu->get_bhp();
			mp = iu->get_mp();
			return;
		}
		bhp = iu->get_bhp() * t + il->get_bhp() * (1 - t);
		mp = iu->get_mp() * t + il->get_mp() * (1 - t);
		return;
	}
	if (std::isnan(mp)) {
		bhp = mp = std::numeric_limits<double>::quiet_NaN();
		return;
	}
	const_iterator iu(begin());
	while (iu != end() && iu->get_mp() < mp)
		++iu;
	if (iu == end()) {
		if (iu == begin()) {
			bhp = mp = std::numeric_limits<double>::quiet_NaN();
			return;
		}
		--iu;
		bhp = iu->get_bhp();
		mp = iu->get_mp();
		return;
	}
	const_iterator il(iu);
	if (iu == begin()) {
		++iu;
		if (iu == end()) {
			bhp = il->get_bhp();
			mp = il->get_mp();
			return;
		}
	} else {
		--il;
	}
	double t((mp - il->get_mp()) / (iu->get_mp() - il->get_mp()));
	if (t <= 0) {
		bhp = il->get_bhp();
		mp = il->get_mp();
		return;
	}
	if (t >= 1) {
		bhp = iu->get_bhp();
		mp = iu->get_mp();
		return;
	}
	bhp = iu->get_bhp() * t + il->get_bhp() * (1 - t);
	mp = iu->get_mp() * t + il->get_mp() * (1 - t);
}

void Aircraft::Cruise::PistonPowerRPM::save_xml(xmlpp::Element *el, double alt, double isaoffs, double bhpfactor) const
{
	for (const_iterator i(begin()), e(end()); i != e; ++i)
		i->save_xml(el, alt, isaoffs, get_rpm(), bhpfactor);
}

Aircraft::Cruise::PistonPowerTemp::PistonPowerTemp(double isaoffs)
	: m_isaoffs(isaoffs)
{
}

int Aircraft::Cruise::PistonPowerTemp::compare(const PistonPowerTemp& x) const
{
	if (get_isaoffs() < x.get_isaoffs())
		return -1;
	if (x.get_isaoffs() < get_isaoffs())
		return 1;
	return 0;
}

void Aircraft::Cruise::PistonPowerTemp::calculate(double& bhp, double& rpm, double& mp) const
{
	if (!std::isnan(rpm) && !get_variablepitch().empty()) {
		double xbhp(bhp), xrpm(rpm), xmp(mp);
		variablepitch_t::const_iterator iu(get_variablepitch().lower_bound(PistonPowerRPM(xrpm)));
		if (iu == get_variablepitch().end()) {
			if (iu == get_variablepitch().begin()) {
				xbhp = xrpm = xmp = std::numeric_limits<double>::quiet_NaN();
			} else {
				--iu;
				xrpm = iu->get_rpm();
				iu->calculate(xbhp, xmp);
			}
		} else {
			bool doit(true);
			variablepitch_t::const_iterator il(iu);
			if (iu == get_variablepitch().begin()) {
				++iu;
				if (iu == get_variablepitch().end()) {
					xrpm = il->get_rpm();
					il->calculate(xbhp, xmp);
					doit = false;
				}
			} else {
				--il;
			}
			if (doit) {
				double t((xrpm - il->get_rpm()) / (iu->get_rpm() - il->get_rpm()));
				if (t <= 0) {
					il->calculate(xbhp, xmp);
				} else if (t >= 1) {
					iu->calculate(xbhp, xmp);
				} else {
					double lbhp(xbhp), lmp(xmp);
					il->calculate(lbhp, lmp);
					double ubhp(xbhp), ump(xmp);
					iu->calculate(ubhp, ump);
					xbhp = ubhp * t + lbhp * (1 - t);
					xmp = ump * t + lmp * (1 - t);
				}
			}
		}
		if (std::isnan(bhp) || xbhp >= 0.99 * bhp) {
			bhp = xbhp;
			rpm = xrpm;
			mp = xmp;
			return;
		}
		for (variablepitch_t::const_iterator i(get_variablepitch().begin()), e(get_variablepitch().end()); i != e; ++i) {
			double zbhp(bhp), zmp(mp);
			i->calculate(zbhp, zmp);
			if (zbhp >= 0.99 * bhp) {
				bhp = zbhp;
				rpm = i->get_rpm();
				mp = zmp;
				return;
			}
		}
		bhp = xbhp;
		rpm = xrpm;
		mp = xmp;
		return;
	}
	mp = std::numeric_limits<double>::quiet_NaN();
	if (get_fixedpitch().empty()) {
		bhp = rpm = std::numeric_limits<double>::quiet_NaN();
		return;
	}
	if (!std::isnan(bhp)) {
		fixedpitch_t::const_iterator iu(get_fixedpitch().lower_bound(PistonPowerBHPRPM(bhp, 0)));
		if (iu == get_fixedpitch().end()) {
			if (iu == get_fixedpitch().begin()) {
				bhp = rpm = std::numeric_limits<double>::quiet_NaN();
				return;
			}
			--iu;
			bhp = iu->get_bhp();
			rpm = iu->get_rpm();
			return;
		}
		fixedpitch_t::const_iterator il(iu);
		if (iu == get_fixedpitch().begin()) {
			++iu;
			if (iu == get_fixedpitch().end()) {
				bhp = il->get_bhp();
				rpm = il->get_rpm();
				return;
			}
		} else {
			--il;
		}
		double t((bhp - il->get_bhp()) / (iu->get_bhp() - il->get_bhp()));
		if (t <= 0) {
			bhp = il->get_bhp();
			rpm = il->get_rpm();
			return;
		}
		if (t >= 1) {
			bhp = iu->get_bhp();
			rpm = iu->get_rpm();
			return;
		}
		bhp = iu->get_bhp() * t + il->get_bhp() * (1 - t);
		rpm = iu->get_rpm() * t + il->get_rpm() * (1 - t);
		return;
	}
	if (std::isnan(rpm)) {
		bhp = rpm = std::numeric_limits<double>::quiet_NaN();
		return;
	}
	fixedpitch_t::const_iterator iu(get_fixedpitch().begin());
	while (iu != get_fixedpitch().end() && iu->get_rpm() < rpm)
		++iu;
	if (iu == get_fixedpitch().end()) {
		if (iu == get_fixedpitch().begin()) {
			bhp = rpm = std::numeric_limits<double>::quiet_NaN();
			return;
		}
		--iu;
		bhp = iu->get_bhp();
		rpm = iu->get_rpm();
		return;
	}
	fixedpitch_t::const_iterator il(iu);
	if (iu == get_fixedpitch().begin()) {
		++iu;
		if (iu == get_fixedpitch().end()) {
			bhp = il->get_bhp();
			rpm = il->get_rpm();
			return;
		}
	} else {
		--il;
	}
	double t((rpm - il->get_rpm()) / (iu->get_rpm() - il->get_rpm()));
	if (t <= 0) {
		bhp = il->get_bhp();
		rpm = il->get_rpm();
		return;
	}
	if (t >= 1) {
		bhp = iu->get_bhp();
		rpm = iu->get_rpm();
		return;
	}
	bhp = iu->get_bhp() * t + il->get_bhp() * (1 - t);
	rpm = iu->get_rpm() * t + il->get_rpm() * (1 - t);
}

void Aircraft::Cruise::PistonPowerTemp::save_xml(xmlpp::Element *el, double alt, double tempfactor, double bhpfactor) const
{
	if (std::isnan(tempfactor) || tempfactor == 0)
		tempfactor = std::numeric_limits<double>::quiet_NaN();
	else
		tempfactor = get_isaoffs() / tempfactor;
	for (fixedpitch_t::const_iterator i(get_fixedpitch().begin()), e(get_fixedpitch().end()); i != e; ++i)
		i->save_xml(el, alt, tempfactor, bhpfactor);
	for (variablepitch_t::const_iterator i(get_variablepitch().begin()), e(get_variablepitch().end()); i != e; ++i)
		i->save_xml(el, alt, tempfactor, bhpfactor);
}

Aircraft::Cruise::PistonPowerPA::PistonPowerPA(double pa)
	: m_pa(pa)
{
}

int Aircraft::Cruise::PistonPowerPA::compare(const PistonPowerPA& x) const
{
	if (get_pa() < x.get_pa())
		return -1;
	if (x.get_pa() < get_pa())
		return 1;
	return 0;
}

void Aircraft::Cruise::PistonPowerPA::calculate(double& isaoffs, double& bhp, double& rpm, double& mp) const
{
	if (std::isnan(isaoffs)) {
		isaoffs = bhp = rpm = mp = std::numeric_limits<double>::quiet_NaN();
		return;
	}
	const_iterator iu(lower_bound(PistonPowerTemp(isaoffs)));
	if (iu == end()) {
		if (iu == begin()) {
			isaoffs = bhp = rpm = mp = std::numeric_limits<double>::quiet_NaN();
			return;
		}
		--iu;
		isaoffs = iu->get_isaoffs();
		iu->calculate(bhp, rpm, mp);
		return;
	}
	const_iterator il(iu);
	if (iu == begin()) {
		++iu;
		if (iu == end()) {
			isaoffs = il->get_isaoffs();
			il->calculate(bhp, rpm, mp);
			return;
		}
	} else {
		--il;
	}
	double t((isaoffs - il->get_isaoffs()) / (iu->get_isaoffs() - il->get_isaoffs()));
	if (t <= 0) {
		il->calculate(bhp, rpm, mp);
		return;
	}
	if (t >= 1) {
		iu->calculate(bhp, rpm, mp);
		return;
	}
	double lbhp(bhp), lrpm(rpm), lmp(mp);
	il->calculate(lbhp, lrpm, lmp);
	double ubhp(bhp), urpm(rpm), ump(mp);
	iu->calculate(ubhp, urpm, ump);
	bhp = ubhp * t + lbhp * (1 - t);
	rpm = urpm * t + lrpm * (1 - t);
	mp = ump * t + lmp * (1 - t);
}

void Aircraft::Cruise::PistonPowerPA::save_xml(xmlpp::Element *el, double altfactor, double tempfactor, double bhpfactor) const
{
	if (std::isnan(altfactor) || altfactor == 0)
		altfactor = std::numeric_limits<double>::quiet_NaN();
	else
		altfactor = get_pa() / altfactor;
	for (const_iterator i(begin()), e(end()); i != e; ++i)
		i->save_xml(el, altfactor, tempfactor, bhpfactor);
}

bool Aircraft::Cruise::PistonPowerPA::has_fixedpitch(void) const
{
	for (const_iterator i(begin()), e(end()); i != e; ++i)
		if (i->has_fixedpitch())
			return true;
	return false;
}

bool Aircraft::Cruise::PistonPowerPA::has_variablepitch(void) const
{
	for (const_iterator i(begin()), e(end()); i != e; ++i)
		if (i->has_variablepitch())
			return true;
	return false;
}

Aircraft::Cruise::PistonPower::PistonPower(void)
{
}

void Aircraft::Cruise::PistonPower::add(double pa, double isaoffs, double bhp, double rpm, double mp)
{
	if (std::isnan(pa) || std::isnan(isaoffs) || std::isnan(bhp) || std::isnan(rpm))
		return;
	iterator ip(insert(PistonPowerPA(pa)).first);
	PistonPowerPA::iterator it(const_cast<PistonPowerPA&>(*ip).insert(PistonPowerTemp(isaoffs)).first);
	if (std::isnan(mp)) {
		const_cast<PistonPowerTemp::fixedpitch_t&>(it->get_fixedpitch()).insert(PistonPowerBHPRPM(bhp, rpm));
	} else {
		PistonPowerTemp::variablepitch_t::iterator ir(const_cast<PistonPowerTemp::variablepitch_t&>(it->get_variablepitch()).insert(PistonPowerRPM(rpm)).first);
		const_cast<PistonPowerRPM&>(*ir).insert(PistonPowerBHPMP(bhp, mp));
	}
}

void Aircraft::Cruise::PistonPower::calculate(double& pa, double& isaoffs, double& bhp, double& rpm, double& mp) const
{
	if (std::isnan(pa) || std::isnan(isaoffs)) {
		pa = isaoffs = bhp = rpm = mp = std::numeric_limits<double>::quiet_NaN();
		return;
	}
	const_iterator iu(lower_bound(PistonPowerPA(pa)));
	if (iu == end()) {
		if (iu == begin()) {
			pa = isaoffs = bhp = rpm = mp = std::numeric_limits<double>::quiet_NaN();
			return;
		}
		--iu;
		pa = iu->get_pa();
		return iu->calculate(isaoffs, bhp, rpm, mp);
	}
	const_iterator il(iu);
	if (iu == begin()) {
		++iu;
		if (iu == end()) {
			pa = il->get_pa();
			il->calculate(isaoffs, bhp, rpm, mp);
			return;
		}
	} else {
		--il;
	}
	double t((pa - il->get_pa()) / (iu->get_pa() - il->get_pa()));
	if (t <= 0) {
		il->calculate(isaoffs, bhp, rpm, mp);
		return;
	}
	if (t >= 1) {
		iu->calculate(isaoffs, bhp, rpm, mp);
		return;
	}
	double lisaoffs(isaoffs), lbhp(bhp), lrpm(rpm), lmp(mp);
	il->calculate(lisaoffs, lbhp, lrpm, lmp);
	double uisaoffs(isaoffs), ubhp(bhp), urpm(rpm), ump(mp);
	iu->calculate(uisaoffs, ubhp, urpm, ump);
	isaoffs = uisaoffs * t + lisaoffs * (1 - t);
	bhp = ubhp * t + lbhp * (1 - t);
	rpm = urpm * t + lrpm * (1 - t);
	mp = ump * t + lmp * (1 - t);
}

void Aircraft::Cruise::PistonPower::load_xml(const xmlpp::Element *el, double altfactor, double tempfactor, double bhpfactor)
{
	xmlpp::Node::NodeList nl(el->get_children("point"));
	for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni) {
		const xmlpp::Element *el2(static_cast<xmlpp::Element *>(*ni));
		double pa(std::numeric_limits<double>::quiet_NaN());
		double isaoffs(std::numeric_limits<double>::quiet_NaN());
		double bhp(std::numeric_limits<double>::quiet_NaN());
		double rpm(std::numeric_limits<double>::quiet_NaN());
		double mp(std::numeric_limits<double>::quiet_NaN());
		xmlpp::Attribute *attr;
		if ((attr = el2->get_attribute("pa")) && attr->get_value() != "")
			pa = Glib::Ascii::strtod(attr->get_value()) * altfactor;
		if ((attr = el2->get_attribute("isaoffs")) && attr->get_value() != "")
			isaoffs = Glib::Ascii::strtod(attr->get_value()) * tempfactor;
		if ((attr = el2->get_attribute("bhp")) && attr->get_value() != "")
			bhp = Glib::Ascii::strtod(attr->get_value()) * bhpfactor;
		if ((attr = el2->get_attribute("rpm")) && attr->get_value() != "")
			rpm = Glib::Ascii::strtod(attr->get_value());
		if ((attr = el2->get_attribute("mp")) && attr->get_value() != "")
			mp = Glib::Ascii::strtod(attr->get_value());
		add(pa, isaoffs, bhp, rpm, mp);
	}
}

void Aircraft::Cruise::PistonPower::save_xml(xmlpp::Element *el, double altfactor, double tempfactor, double bhpfactor) const
{
	if (!el)
		return;
	if (empty())
		return;
	for (const_iterator i(begin()), e(end()); i != e; ++i)
		i->save_xml(el, altfactor, tempfactor, bhpfactor);
}

void Aircraft::Cruise::PistonPower::set_default(void)
{
	static const double tbl[][4] = {
		{     0, 110, 2100, 22.9 },
		{  1000, 110, 2100, 22.7 },
		{  2000, 110, 2100, 22.4 },
		{  3000, 110, 2100, 22.2 },
		{  4000, 110, 2100, 21.9 },
		{  5000, 110, 2100, 21.7 },
		{  6000, 110, 2100, 21.4 },
		{  7000, 110, 2100, 21.2 },
		{  8000, 110, 2100, 21.0 },
		{  9000, 110, 2100, 20.8 },
		{     0, 110, 2400, 20.4 },
		{  1000, 110, 2400, 20.2 },
		{  2000, 110, 2400, 20.0 },
		{  3000, 110, 2400, 19.8 },
		{  4000, 110, 2400, 19.5 },
		{  5000, 110, 2400, 19.3 },
		{  6000, 110, 2400, 19.1 },
		{  7000, 110, 2400, 18.9 },
		{  8000, 110, 2400, 18.7 },
		{  9000, 110, 2400, 18.5 },
		{ 10000, 110, 2400, 18.3 },
		{ 11000, 110, 2400, 18.1 },
		{ 12000, 110, 2400, 17.8 },
		{ 13000, 110, 2400, 17.6 },
		{ 14000, 110, 2400, 17.4 },
		{     0, 130, 2100, 25.9 },
		{  1000, 130, 2100, 25.6 },
		{  2000, 130, 2100, 25.4 },
		{  3000, 130, 2100, 25.1 },
		{  4000, 130, 2100, 24.8 },
		{  5000, 130, 2100, 24.6 },
		{     0, 130, 2400, 22.9 },
		{  1000, 130, 2400, 22.7 },
		{  2000, 130, 2400, 22.5 },
		{  3000, 130, 2400, 22.2 },
		{  4000, 130, 2400, 22.0 },
		{  5000, 130, 2400, 21.7 },
		{  6000, 130, 2400, 21.5 },
		{  7000, 130, 2400, 21.3 },
		{  8000, 130, 2400, 21.0 },
		{  9000, 130, 2400, 20.8 },
		{     0, 150, 2400, 25.5 },
		{  1000, 150, 2400, 25.2 },
		{  2000, 150, 2400, 25.0 },
		{  3000, 150, 2400, 24.7 },
		{  4000, 150, 2400, 24.4 },
		{  5000, 150, 2400, 24.2 },
		// extrapolated
		{     0,  90, 2400, 17.9 },
		{  1000,  90, 2400, 17.7 },
		{  2000,  90, 2400, 17.5 },
		{  3000,  90, 2400, 17.3 },
		{  4000,  90, 2400, 17.0 },
		{  5000,  90, 2400, 16.8 },
		{  6000,  90, 2400, 16.6 },
		{  7000,  90, 2400, 16.4 },
		{  8000,  90, 2400, 16.2 },
		{  9000,  90, 2400, 16.0 },
		{ 10000,  90, 2400, 15.8 },
		{ 11000,  90, 2400, 15.6 },
		{ 12000,  90, 2400, 15.3 },
		{ 13000,  90, 2400, 15.1 },
		{ 14000,  90, 2400, 14.9 },
		{ 15000,  90, 2400, 14.7 },
		{ 16000,  90, 2400, 14.5 },
		{ 17000,  90, 2400, 14.3 }
	};

	clear();
	for (unsigned int i = 0; i < sizeof(tbl)/sizeof(tbl[0]); ++i)
		for (int t = -2; t <= 2; ++t)
			add(tbl[i][0], t * (50./9.), tbl[i][1], tbl[i][2], tbl[i][3] + t * 0.16);
}

bool Aircraft::Cruise::PistonPower::has_fixedpitch(void) const
{
	for (const_iterator i(begin()), e(end()); i != e; ++i)
		if (i->has_fixedpitch())
			return true;
	return false;
}

bool Aircraft::Cruise::PistonPower::has_variablepitch(void) const
{
	for (const_iterator i(begin()), e(end()); i != e; ++i)
		if (i->has_variablepitch())
			return true;
	return false;
}

Aircraft::Cruise::Cruise(double altfactor, double tasfactor, double fuelfactor, double tempfactor, double bhpfactor, double massfactor)
	: m_altfactor(altfactor), m_tasfactor(tasfactor), m_fuelfactor(fuelfactor), m_tempfactor(tempfactor), m_bhpfactor(bhpfactor), m_massfactor(massfactor)
{
	// PA28R-200
	{
		Curve c("55%, 2100 RPM", Curve::flags_interpolate, std::numeric_limits<double>::quiet_NaN(), 0, 2100);
		c.insert(Point(    0, 110, 131.5 * 0.86897624, 54.0/6));
		c.insert(Point( 1000, 110, 133.0 * 0.86897624, 54.0/6));
		c.insert(Point( 2000, 110, 134.5 * 0.86897624, 54.0/6));
		c.insert(Point( 3000, 110, 136.0 * 0.86897624, 54.0/6));
		c.insert(Point( 4000, 110, 137.5 * 0.86897624, 54.0/6));
		c.insert(Point( 5000, 110, 139.0 * 0.86897624, 54.0/6));
		c.insert(Point( 6000, 110, 140.5 * 0.86897624, 54.0/6));
		c.insert(Point( 7000, 110, 142.0 * 0.86897624, 54.0/6));
		c.insert(Point( 8000, 110, 143.5 * 0.86897624, 54.0/6));
		c.insert(Point( 9000, 110, 145.0 * 0.86897624, 54.0/6));
		add_curve(c);
	}
	{
		Curve c("55%, 2400 RPM", Curve::flags_interpolate, std::numeric_limits<double>::quiet_NaN(), 0, 2400);
		c.insert(Point(    0, 110, 131.5 * 0.86897624, 57.0/6));
		c.insert(Point( 1000, 110, 133.0 * 0.86897624, 57.0/6));
		c.insert(Point( 2000, 110, 134.5 * 0.86897624, 57.0/6));
		c.insert(Point( 3000, 110, 136.0 * 0.86897624, 57.0/6));
		c.insert(Point( 4000, 110, 137.5 * 0.86897624, 57.0/6));
		c.insert(Point( 5000, 110, 139.0 * 0.86897624, 57.0/6));
		c.insert(Point( 6000, 110, 140.5 * 0.86897624, 57.0/6));
		c.insert(Point( 7000, 110, 142.0 * 0.86897624, 57.0/6));
		c.insert(Point( 8000, 110, 143.5 * 0.86897624, 57.0/6));
		c.insert(Point( 9000, 110, 145.0 * 0.86897624, 57.0/6));
		c.insert(Point(10000, 110, 146.5 * 0.86897624, 57.0/6));
		c.insert(Point(11000, 110, 148.0 * 0.86897624, 57.0/6));
		c.insert(Point(12000, 110, 149.5 * 0.86897624, 57.0/6));
		c.insert(Point(13000, 110, 151.0 * 0.86897624, 57.0/6));
		c.insert(Point(14000, 110, 152.5 * 0.86897624, 57.0/6));
		add_curve(c);
	}
	{
		Curve c("65%, 2100 RPM", Curve::flags_interpolate, std::numeric_limits<double>::quiet_NaN(), 0, 2100);
		c.insert(Point(    0, 130, 143.0 * 0.86897624, 61.0/6));
		c.insert(Point( 1000, 130, 144.8 * 0.86897624, 61.0/6));
		c.insert(Point( 2000, 130, 146.6 * 0.86897624, 61.0/6));
		c.insert(Point( 3000, 130, 148.3 * 0.86897624, 61.0/6));
		c.insert(Point( 4000, 130, 150.1 * 0.86897624, 61.0/6));
		c.insert(Point( 5000, 130, 151.9 * 0.86897624, 61.0/6));
		add_curve(c);
	}
	{
		Curve c("65%, 2400 RPM", Curve::flags_interpolate, std::numeric_limits<double>::quiet_NaN(), 0, 2400);
		c.insert(Point(    0, 130, 143.0 * 0.86897624, 64.0/6));
		c.insert(Point( 1000, 130, 144.8 * 0.86897624, 64.0/6));
		c.insert(Point( 2000, 130, 146.6 * 0.86897624, 64.0/6));
		c.insert(Point( 3000, 130, 148.3 * 0.86897624, 64.0/6));
		c.insert(Point( 4000, 130, 150.1 * 0.86897624, 64.0/6));
		c.insert(Point( 5000, 130, 151.9 * 0.86897624, 64.0/6));
		c.insert(Point( 6000, 130, 153.7 * 0.86897624, 64.0/6));
		c.insert(Point( 7000, 130, 155.4 * 0.86897624, 64.0/6));
		c.insert(Point( 8000, 130, 157.2 * 0.86897624, 64.0/6));
		c.insert(Point( 9000, 130, 159.0 * 0.86897624, 64.0/6));
		add_curve(c);
	}
	{
		Curve c("75%, 2400 RPM", Curve::flags_interpolate, std::numeric_limits<double>::quiet_NaN(), 0, 2400);
		c.insert(Point(    0, 150, 153.0 * 0.86897624, 72.0/6));
		c.insert(Point( 1000, 150, 155.0 * 0.86897624, 72.0/6));
		c.insert(Point( 2000, 150, 157.0 * 0.86897624, 72.0/6));
		c.insert(Point( 3000, 150, 159.0 * 0.86897624, 72.0/6));
		c.insert(Point( 4000, 150, 161.0 * 0.86897624, 72.0/6));
		c.insert(Point( 5000, 150, 163.0 * 0.86897624, 72.0/6));
		add_curve(c);
	}
	// Extrapolated
	{
		Curve c("45%, 2400 RPM", Curve::flags_interpolate, std::numeric_limits<double>::quiet_NaN(), 0, 2400);
		c.insert(Point(    0,  90, 120.5 * 0.86897624, 50.0/6));
		c.insert(Point( 1000,  90, 122.0 * 0.86897624, 50.0/6));
		c.insert(Point( 2000,  90, 123.5 * 0.86897624, 50.0/6));
		c.insert(Point( 3000,  90, 125.0 * 0.86897624, 50.0/6));
		c.insert(Point( 4000,  90, 126.5 * 0.86897624, 50.0/6));
		c.insert(Point( 5000,  90, 128.0 * 0.86897624, 50.0/6));
		c.insert(Point( 6000,  90, 129.5 * 0.86897624, 50.0/6));
		c.insert(Point( 7000,  90, 131.0 * 0.86897624, 50.0/6));
		c.insert(Point( 8000,  90, 132.5 * 0.86897624, 50.0/6));
		c.insert(Point( 9000,  90, 134.0 * 0.86897624, 50.0/6));
		c.insert(Point(10000,  90, 135.5 * 0.86897624, 50.0/6));
		c.insert(Point(11000,  90, 137.0 * 0.86897624, 50.0/6));
		c.insert(Point(12000,  90, 138.5 * 0.86897624, 50.0/6));
		c.insert(Point(13000,  90, 140.0 * 0.86897624, 50.0/6));
		c.insert(Point(14000,  90, 141.5 * 0.86897624, 50.0/6));
		c.insert(Point(15000,  90, 143.0 * 0.86897624, 50.0/6));
		c.insert(Point(16000,  90, 144.5 * 0.86897624, 50.0/6));
		c.insert(Point(17000,  90, 145.0 * 0.86897624, 50.0/6));
		add_curve(c);
	}
	m_pistonpower.set_default();
}

void Aircraft::Cruise::set_points(const rpmmppoints_t& pts, double maxbhp)
{
	typedef std::map<int,CurveRPMMP> intcurve_t;
	typedef std::map<int,intcurve_t> intintcurve_t;
	CurveRPMMP c0("Normal");
	intcurve_t c1;
	intcurve_t c2;
	intintcurve_t c3;
	unsigned int missing[4] = { 0, 0, 0, 0 };
	unsigned int nrcurves[4] = { 1, 0, 0, 0 };
	m_curves.clear();
	double bhpmul(100.0 / maxbhp);
	for (rpmmppoints_t::const_iterator pi(pts.begin()), pe(pts.end()); pi != pe; ++pi) {
		m_pistonpower.add(pi->get_pressurealt(), 0, pi->get_bhp(), pi->get_rpm(), pi->get_mp());
		int bhp(-1), rpm(-1);
		{
			double x(bhpmul * pi->get_bhp());
			if (!std::isnan(x))
				bhp = ::Point::round<int,double>(x);
		}
		{
			double x(pi->get_rpm());
			if (!std::isnan(x))
				rpm = ::Point::round<int,double>(x);
		}
		{
			std::pair<CurveRPMMP::iterator,bool> itp(c0.insert(*pi));
			missing[0] += !itp.second;
		}
		if (rpm >= 0) {
			std::pair<intcurve_t::iterator,bool> it(c1.insert(intcurve_t::value_type(rpm, CurveRPMMP(""))));
			CurveRPMMP& c(it.first->second);
			if (it.second) {
				std::ostringstream oss;
				oss << rpm << " RPM";
				c.set_name(oss.str());
				++nrcurves[1];
			}
			std::pair<CurveRPMMP::iterator,bool> itp(c.insert(*pi));
			missing[1] += !itp.second;
		} else {
			++missing[1];
		}
		if (bhp >= 0) {
			std::pair<intcurve_t::iterator,bool> it(c2.insert(intcurve_t::value_type(bhp, CurveRPMMP(""))));
			CurveRPMMP& c(it.first->second);
			if (it.second) {
				std::ostringstream oss;
				oss << bhp << '%';
				c.set_name(oss.str());
				++nrcurves[2];
			}
			std::pair<CurveRPMMP::iterator,bool> itp(c.insert(*pi));
			missing[2] += !itp.second;
		} else {
			++missing[2];
		}
		if (rpm >= 0 && bhp >= 0) {
			std::pair<intintcurve_t::iterator,bool> it1(c3.insert(intintcurve_t::value_type(rpm, intcurve_t())));
			std::pair<intcurve_t::iterator,bool> it2(it1.first->second.insert(intcurve_t::value_type(bhp, CurveRPMMP(""))));
			CurveRPMMP& c(it2.first->second);
			if (it2.second) {
				std::ostringstream oss;
				oss << bhp << "%, " << rpm << " RPM";
				c.set_name(oss.str());
				++nrcurves[3];
			}
			std::pair<CurveRPMMP::iterator,bool> itp(c.insert(*pi));
			missing[3] += !itp.second;
		} else {
			++missing[3];
		}
	}
	unsigned int bidx(~0U);
	for (unsigned int i = 0; i < 4; ++i) {
		if (missing[i])
			continue;
		if (bidx >= 4) {
			bidx = i;
			continue;
		}
		if (nrcurves[i] >= nrcurves[bidx])
			continue;
		bidx = i;
	}
	if (bidx >= 4U) {
		bidx = 0;
		for (unsigned int i = 1; i < 4; ++i) {
			if (missing[i] > missing[bidx])
				continue;
			if (missing[i] == missing[bidx] && nrcurves[i] >= nrcurves[bidx])
				continue;
			bidx = i;
		}
	}
	switch (bidx) {
	default:
	case 0:
		add_curve(c0);
		break;

	case 1:
		for (intcurve_t::const_iterator ci(c1.begin()), ce(c1.end()); ci != ce; ++ci)
			add_curve(ci->second);
		break;

	case 2:
		for (intcurve_t::const_iterator ci(c2.begin()), ce(c2.end()); ci != ce; ++ci)
			add_curve(ci->second);
		break;

	case 3:
		for (intintcurve_t::const_iterator xi(c3.begin()), xe(c3.end()); xi != xe; ++xi)
			for (intcurve_t::const_iterator ci(xi->second.begin()), ce(xi->second.end()); ci != ce; ++ci)
				add_curve(ci->second);
		break;
	}
}

void Aircraft::Cruise::load_xml(const xmlpp::Element *el, double maxbhp)
{
	if (!el)
		return;
	m_remark.clear();
	m_altfactor = 1;
	m_tasfactor = 1;
	m_fuelfactor = 1;
	m_tempfactor = 1;
	m_bhpfactor = 1;
	m_massfactor = 1;
	clear_curves();
	m_pistonpower.set_default();
	m_optimalaltitude.clear();
	m_optimalaltitude.recalculatepoly(true);
	xmlpp::Attribute *attr;
	if ((attr = el->get_attribute("altfactor")) && attr->get_value() != "")
		m_altfactor = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("tasfactor")) && attr->get_value() != "")
		m_tasfactor = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("fuelfactor")) && attr->get_value() != "")
		m_fuelfactor = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("tempfactor")) && attr->get_value() != "")
		m_tempfactor = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("bhpfactor")) && attr->get_value() != "")
		m_bhpfactor = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("massfactor")) && attr->get_value() != "")
		m_massfactor = Glib::Ascii::strtod(attr->get_value());
	if ((attr = el->get_attribute("remark")))
		m_remark = attr->get_value();
	{
		PistonPower pp;
		xmlpp::Node::NodeList nl(el->get_children("curve"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni) {
			Curve c;
			c.load_xml(static_cast<xmlpp::Element *>(*ni), pp, m_altfactor, m_tasfactor, m_fuelfactor);
			add_curve(c);
		}
		if (m_curves.empty()) {
			rpmmppoints_t pts;
			xmlpp::Node::NodeList nl(el->get_children("point"));
			for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni) {
				PointRPMMP pt;
				pt.load_xml(static_cast<xmlpp::Element *>(*ni), pp, m_altfactor, m_tasfactor, m_fuelfactor);
				if (std::isnan(pt.get_pressurealt()))
					continue;
				pts.push_back(pt);
			}
			set_points(pts, maxbhp);
		}
		if (!pp.empty())
			m_pistonpower.swap(pp);
	}
	{
		xmlpp::Node::NodeList nl(el->get_children("PistonPower"));
		if (!nl.empty()) {
			PistonPower pp;
			for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni) {
				const xmlpp::Element *el2(static_cast<xmlpp::Element *>(*ni));
				pp.load_xml(el2, m_altfactor, m_tempfactor, m_bhpfactor);
			}
			m_pistonpower.swap(pp);
		}
	}
	{
		xmlpp::Node::NodeList nl(el->get_children("optimalaltitude"));
		for (xmlpp::Node::NodeList::const_iterator ni(nl.begin()), ne(nl.end()); ni != ne; ++ni)
			m_optimalaltitude.load_xml(static_cast<xmlpp::Element *>(*ni), m_altfactor, m_massfactor);
	}
}

void Aircraft::Cruise::save_xml(xmlpp::Element *el, double maxbhp) const
{
	if (!el)
		return;
	if (m_altfactor == 1) {
		el->remove_attribute("altfactor");
	} else {
		std::ostringstream oss;
		oss << m_altfactor;
		el->set_attribute("altfactor", oss.str());
	}
	if (m_tasfactor == 1) {
		el->remove_attribute("tasfactor");
	} else {
		std::ostringstream oss;
		oss << m_tasfactor;
		el->set_attribute("tasfactor", oss.str());
	}
	if (m_fuelfactor == 1) {
		el->remove_attribute("fuelfactor");
	} else {
		std::ostringstream oss;
		oss << m_fuelfactor;
		el->set_attribute("fuelfactor", oss.str());
	}
	if (m_tempfactor == 1) {
		el->remove_attribute("tempfactor");
	} else {
		std::ostringstream oss;
		oss << m_tempfactor;
		el->set_attribute("tempfactor", oss.str());
	}
	if (m_bhpfactor == 1) {
		el->remove_attribute("bhpfactor");
	} else {
		std::ostringstream oss;
		oss << m_bhpfactor;
		el->set_attribute("bhpfactor", oss.str());
	}
	if (m_remark.empty())
		el->remove_attribute("remark");
	else
		el->set_attribute("remark", m_remark);
	{
		typedef std::set<unsigned int> order_t;
		order_t order;
		for (curves_t::const_iterator ci(m_curves.begin()), ce(m_curves.end()); ci != ce; ++ci)
			order.insert(ci->second.second);
		for (order_t::const_iterator oi(order.begin()), oe(order.end()); oi != oe; ++oi)
			for (curves_t::const_iterator ci(m_curves.begin()), ce(m_curves.end()); ci != ce; ++ci) {
				if (ci->second.second != *oi)
					continue;
				for (massmap_t::const_iterator mi(ci->second.first.begin()), me(ci->second.first.end()); mi != me; ++mi)
					for (isamap_t::const_iterator ii(mi->second.begin()), ie(mi->second.end()); ii != ie; ++ii)
						ii->second.save_xml(el->add_child("curve"), m_altfactor, m_tasfactor, m_fuelfactor);
			}
	}
	m_pistonpower.save_xml(el->add_child("PistonPower"), m_altfactor, m_tempfactor, m_bhpfactor);
	m_optimalaltitude.save_xml(el->add_child("optimalaltitude"), m_altfactor, m_massfactor);
}

#ifdef HAVE_JSONCPP

void Aircraft::Cruise::save_garminpilot(Json::Value& root, const Aircraft& acft, unsigned int version)
{
	root["phase"] = "cruise";
	{
		Json::Value& pdu(root["parameterDisplayUnits"]);
		pdu["temperature"] = "Degrees Celsius";
	}
	root["tempType"] = "isa";
	{
		Json::Value& pd(root["parameterDefaults"]);
		pd = Json::Value(Json::objectValue);
	}
	{
		Json::Value& par(root["inputParameters"]);
		par.append(Json::Value("altitude"));
		par.append(Json::Value("powerSetting"));
	}
	root["powerSettingType"] = "percentPower";
	Json::Value& samp(root["samples"]);
	bool first(true), interpolate(false);
	double mass(0);
	for (curves_t::const_iterator ci(m_curves.begin()), ce(m_curves.end()); ci != ce; ++ci) {
		massmap_t::const_iterator mi;
		if (first) {
			mi = ci->second.first.end();
			if (mi == ci->second.first.begin())
				continue;
			--mi;
		} else {
			mi = ci->second.first.find(mass);
			if (mi == ci->second.first.end())
				continue;
		}
		isamap_t::const_iterator ii(mi->second.find(0.0));
		if (ii == mi->second.end())
			continue;
		const Curve& curve(ii->second);
		if (curve.get_flags() & Curve::flags_hold)
			continue;
		if (curve.get_flags() & Curve::flags_interpolate) {
			if (first)
				interpolate = true;
		} else {
			if (!first)
				continue;
		}
		if (first)
			mass = mi->first;
		for (Curve::const_iterator pi(curve.begin()), pe(curve.end()); pi != pe; ++pi) {
			Json::Value x;
			x["altitude"] = pi->get_pressurealt();
			x["powerSetting"] = pi->get_bhp() / acft.get_maxbhp();
			x["trueAirspeed"] = pi->get_tas();
			x["fuelFlow"] = acft.convert_fuel(acft.get_fuelunit(), unit_usgal, pi->get_fuelflow());
			samp.append(x);
		}
		first = false;
		if (!interpolate)
			break;
	}
}

#endif

void Aircraft::Cruise::add_curve(const Curve& c)
{
	double m(c.get_mass());
	if (std::isnan(m) || m <= 0)
		m = 0;
	std::pair<curves_t::iterator,bool> ins(m_curves.insert(curves_t::value_type(c.get_name(), massmaporder_t(massmap_t(), 0U))));
	if (ins.second) {
		unsigned int o(0U);
		for (curves_t::const_iterator i(m_curves.begin()), e(m_curves.end()); i != e; ++i)
			if (i != ins.first)
				o = std::max(o, i->second.second + 1U);
		ins.first->second.second = o;
	}
	ins.first->second.first[m][c.get_isaoffset()] = c;
}

void Aircraft::Cruise::clear_curves(void)
{
	m_curves.clear();
}

unsigned int Aircraft::Cruise::count_curves(void) const
{
	unsigned int ret(0);
	for (curves_t::const_iterator ci(m_curves.begin()), ce(m_curves.end()); ci != ce; ++ci)
		for (massmap_t::const_iterator mi(ci->second.first.begin()), me(ci->second.first.end()); mi != me; ++mi)
			ret += mi->second.size();
	return ret;
}

bool Aircraft::Cruise::has_hold(void) const
{
	for (curves_t::const_iterator ci(m_curves.begin()), ce(m_curves.end()); ci != ce; ++ci)
		for (massmap_t::const_iterator mi(ci->second.first.begin()), me(ci->second.first.end()); mi != me; ++mi)
			for (isamap_t::const_iterator ii(mi->second.begin()), ie(mi->second.end()); ii != ie; ++ii)
				if (ii->second.get_flags() & Curve::flags_hold)
					return true;
	return false;
}

std::set<std::string> Aircraft::Cruise::get_curves(void) const
{
	std::set<std::string> r;
	for (curves_t::const_iterator i(m_curves.begin()), e(m_curves.end()); i != e; ++i)
		r.insert(i->first);
	return r;
}

Aircraft::Cruise::Curve::flags_t Aircraft::Cruise::get_curve_flags(const std::string& name) const
{
	Curve::flags_t r(Curve::flags_none);
	curves_t::const_iterator ci(m_curves.find(name));
	if (ci == m_curves.end())
		return Curve::flags_none;
	for (massmap_t::const_iterator mi(ci->second.first.begin()), me(ci->second.first.end()); mi != me; ++mi)
		for (isamap_t::const_iterator ii(mi->second.begin()), ie(mi->second.end()); ii != ie; ++ii)
			r |= ii->second.get_flags();
	return r;
}

void Aircraft::Cruise::build_altmap(propulsion_t prop)
{
	if (prop != propulsion_fixedpitch && prop != propulsion_constantspeed)
		m_pistonpower.clear();
	m_bhpmap.clear();
	for (curves_t::const_iterator ci(m_curves.begin()), ce(m_curves.end()); ci != ce; ++ci) {
		for (massmap_t::const_iterator mi(ci->second.first.begin()), me(ci->second.first.end()); mi != me; ++mi) {
			for (isamap_t::const_iterator ii(mi->second.begin()), ie(mi->second.end()); ii != ie; ++ii) {
				if (!(ii->second.get_flags() & Curve::flags_interpolate))
					continue;
				for (Curve::const_iterator pi(ii->second.begin()), pe(ii->second.end()); pi != pe; ++pi) {
					if (std::isnan(pi->get_pressurealt()) || std::isnan(pi->get_bhp()))
						continue;
					m_bhpmap[ii->second.get_mass()][ii->second.get_isaoffset()][pi->get_pressurealt()][pi->get_bhp()] = Point1(pi->get_tas(), pi->get_fuelflow());
				}
			}
		}
	}
	if (false) {
		for (bhpmap_t::const_iterator i(m_bhpmap.begin()), e(m_bhpmap.end()); i != e; ++i) {
			std::cout << "Mass = " << i->first << std::endl;
			for (bhpisamap_t::const_iterator i2(i->second.begin()), e2(i->second.end()); i2 != e2; ++i2) {
				std::cout << "  ISA = " << i2->first << std::endl;
				for (bhpaltmap_t::const_iterator i3(i2->second.begin()), e3(i2->second.end()); i3 != e3; ++i3) {
					std::cout << "    PA = " << i3->first;
					for (bhpbhpmap_t::const_iterator i4(i3->second.begin()), e4(i3->second.end()); i4 != e4; ++i4) {
						std::cout << "    BHP = " << i4->first << " TAS = " << i4->second.get_tas()
							  << " FF = " << i4->second.get_fuelflow() << std::endl;
					}
				}
			}
		}
	}
}

void Aircraft::Cruise::calculate(double& mass, double& isaoffs, double& pa, double& bhp, double& tas, double& ff) const
{
	if (std::isnan(mass)) {
		mass = isaoffs = pa = bhp = tas = ff = std::numeric_limits<double>::quiet_NaN();
		return;
	}
	bhpmap_t::const_iterator iu(m_bhpmap.lower_bound(mass));
	if (iu == m_bhpmap.end()) {
		if (iu == m_bhpmap.begin()) {
			mass = isaoffs = pa = bhp = tas = ff = std::numeric_limits<double>::quiet_NaN();
			return;
		}
		--iu;
		mass = iu->first;
		calculate(iu, isaoffs, pa, bhp, tas, ff);
		return;
	}
	bhpmap_t::const_iterator il(iu);
	if (iu == m_bhpmap.begin()) {
		++iu;
		if (iu == m_bhpmap.end()) {
			mass = il->first;
			calculate(il, isaoffs, pa, bhp, tas, ff);
			return;
		}
	} else {
		--il;
	}
	double t((mass - il->first) / (iu->first - il->first));
	if (t <= 0) {
		calculate(il, isaoffs, pa, bhp, tas, ff);
		return;
	}
	if (t >= 1) {
		calculate(iu, isaoffs, pa, bhp, tas, ff);
		return;
	}
	double lisaoffs(isaoffs), lpa(pa), lbhp(bhp), ltas(tas), lff(ff);
	calculate(il, lisaoffs, lpa, lbhp, ltas, lff);
	double uisaoffs(isaoffs), upa(pa), ubhp(bhp), utas(tas), uff(ff);
	calculate(iu, uisaoffs, upa, ubhp, utas, uff);
	isaoffs = uisaoffs * t + lisaoffs * (1 - t);
	pa = upa * t + lpa * (1 - t);
	bhp = ubhp * t + lbhp * (1 - t);
	tas = utas * t + ltas * (1 - t);
	ff = uff * t + lff * (1 - t);
}

void Aircraft::Cruise::calculate(bhpmap_t::const_iterator it, double& isaoffs, double& pa, double& bhp, double& tas, double& ff) const
{
	if (std::isnan(isaoffs)) {
		isaoffs = pa = bhp = tas = ff = std::numeric_limits<double>::quiet_NaN();
		return;
	}
	bhpisamap_t::const_iterator iu(it->second.lower_bound(isaoffs));
	if (iu == it->second.end()) {
		if (iu == it->second.begin()) {
			isaoffs = pa = bhp = tas = ff = std::numeric_limits<double>::quiet_NaN();
			return;
		}
		--iu;
		isaoffs = iu->first;
		calculate(iu, pa, bhp, tas, ff);
		return;
	}
	bhpisamap_t::const_iterator il(iu);
	if (iu == it->second.begin()) {
		++iu;
		if (iu == it->second.end()) {
			isaoffs = il->first;
			calculate(il, pa, bhp, tas, ff);
			return;
		}
	} else {
		--il;
	}
	double t((isaoffs - il->first) / (iu->first - il->first));
	if (t <= 0) {
		calculate(il, pa, bhp, tas, ff);
		return;
	}
	if (t >= 1) {
		calculate(iu, pa, bhp, tas, ff);
		return;
	}
	double lpa(pa), lbhp(bhp), ltas(tas), lff(ff);
	calculate(il, lpa, lbhp, ltas, lff);
	double upa(pa), ubhp(bhp), utas(tas), uff(ff);
	calculate(iu, upa, ubhp, utas, uff);
	pa = upa * t + lpa * (1 - t);
	bhp = ubhp * t + lbhp * (1 - t);
	tas = utas * t + ltas * (1 - t);
	ff = uff * t + lff * (1 - t);
}

void Aircraft::Cruise::calculate(bhpisamap_t::const_iterator it, double& pa, double& bhp, double& tas, double& ff) const
{
	if (std::isnan(pa)) {
		pa = bhp = tas = ff = std::numeric_limits<double>::quiet_NaN();
		return;
	}
	bhpaltmap_t::const_iterator iu(it->second.lower_bound(pa));
	if (iu == it->second.end()) {
		if (iu == it->second.begin()) {
			pa = bhp = tas = ff = std::numeric_limits<double>::quiet_NaN();
			return;
		}
		--iu;
		pa = iu->first;
		calculate(iu, bhp, tas, ff);
		return;
	}
	bhpaltmap_t::const_iterator il(iu);
	if (iu == it->second.begin()) {
		++iu;
		if (iu == it->second.end()) {
			pa = il->first;
			calculate(il, bhp, tas, ff);
			return;
		}
	} else {
		--il;
	}
	double t((pa - il->first) / (iu->first - il->first));
	if (t <= 0) {
		calculate(il, bhp, tas, ff);
		return;
	}
	if (t >= 1) {
		calculate(iu, bhp, tas, ff);
		return;
	}
	double lbhp(bhp), ltas(tas), lff(ff);
	calculate(il, lbhp, ltas, lff);
	double ubhp(bhp), utas(tas), uff(ff);
	calculate(iu, ubhp, utas, uff);
	bhp = ubhp * t + lbhp * (1 - t);
	tas = utas * t + ltas * (1 - t);
	ff = uff * t + lff * (1 - t);
}

void Aircraft::Cruise::calculate(bhpaltmap_t::const_iterator it, double& bhp, double& tas, double& ff) const
{
	if (std::isnan(bhp)) {
		bhp = tas = ff = std::numeric_limits<double>::quiet_NaN();
		return;
	}
	bhpbhpmap_t::const_iterator iu(it->second.lower_bound(bhp));
	if (iu == it->second.end()) {
		if (iu == it->second.begin()) {
			bhp = tas = ff = std::numeric_limits<double>::quiet_NaN();
			return;
		}
		--iu;
		bhp = iu->first;
		tas = iu->second.get_tas();
		ff = iu->second.get_fuelflow();
		return;
	}
	bhpbhpmap_t::const_iterator il(iu);
	if (iu == it->second.begin()) {
		++iu;
		if (iu == it->second.end()) {
			bhp = il->first;
			tas = il->second.get_tas();
			ff = il->second.get_fuelflow();
			return;
		}
	} else {
		--il;
	}
	double t((bhp - il->first) / (iu->first - il->first));
	if (t <= 0) {
		tas = il->second.get_tas();
		ff = il->second.get_fuelflow();
		return;
	}
	if (t >= 1) {
		tas = iu->second.get_tas();
		ff = iu->second.get_fuelflow();
		return;
	}
	tas = iu->second.get_tas() * t + il->second.get_tas() * (1 - t);
	ff = iu->second.get_fuelflow() * t + il->second.get_fuelflow() * (1 - t);
}

Aircraft::Cruise::Curve::flags_t Aircraft::Cruise::calculate(curves_t::const_iterator it, double& tas, double& ff, double& pa, double& mass, double& isaoffs, CruiseEngineParams& ep) const
{
	static constexpr bool debug = false;
	ep.set_name(it->first);
	if (std::isnan(mass)) {
		mass = isaoffs = pa = tas = ff = std::numeric_limits<double>::quiet_NaN();
		ep.set_bhp(std::numeric_limits<double>::quiet_NaN());
		if (debug)
			std::cerr << "Cruise::calculate: name " << ep.get_name() << " mass " << mass << " isaoffs " << isaoffs
				  << " pa " << pa << " tas " << tas << " ff " << ff << " bhp " << ep.get_bhp() << std::endl;
		return Curve::flags_none;
	}
	massmap_t::const_iterator iu(it->second.first.lower_bound(mass));
	if (iu == it->second.first.end()) {
		if (iu == it->second.first.begin()) {
			mass = isaoffs = pa = tas = ff = std::numeric_limits<double>::quiet_NaN();
			ep.set_bhp(std::numeric_limits<double>::quiet_NaN());
			if (debug)
				std::cerr << "Cruise::calculate: name " << ep.get_name() << " mass " << mass << " isaoffs " << isaoffs
					  << " pa " << pa << " tas " << tas << " ff " << ff << " bhp " << ep.get_bhp() << std::endl;
			return Curve::flags_none;
		}
		--iu;
		mass = iu->first;
		return calculate(iu, tas, ff, pa, isaoffs, ep);
	}
	massmap_t::const_iterator il(iu);
	if (iu == it->second.first.begin()) {
		++iu;
		if (iu == it->second.first.end()) {
			mass = il->first;
			return calculate(il, tas, ff, pa, isaoffs, ep);
		}
	} else {
		--il;
	}
	double t((mass - il->first) / (iu->first - il->first));
	if (t <= 0)
		return calculate(il, tas, ff, pa, isaoffs, ep);
	if (t >= 1)
		return calculate(iu, tas, ff, pa, isaoffs, ep);
	CruiseEngineParams lep(ep);
	double lisaoffs(isaoffs), lpa(pa), ltas(tas), lff(ff);
	Curve::flags_t lflags(calculate(il, ltas, lff, lpa, lisaoffs, lep));
	CruiseEngineParams uep(ep);
	double uisaoffs(isaoffs), upa(pa), utas(tas), uff(ff);
	Curve::flags_t uflags(calculate(iu, utas, uff, upa, uisaoffs, uep));
	isaoffs = uisaoffs * t + lisaoffs * (1 - t);
	pa = upa * t + lpa * (1 - t);
	ep.set_bhp(uep.get_bhp() * t + lep.get_bhp() * (1 - t));
	ep.set_rpm(uep.get_rpm() * t + lep.get_rpm() * (1 - t));
	tas = utas * t + ltas * (1 - t);
	ff = uff * t + lff * (1 - t);
	if (debug)
		std::cerr << "Cruise::calculate: name " << ep.get_name() << " mass " << mass << " isaoffs " << isaoffs
			  << " pa " << pa << " tas " << tas << " ff " << ff << " bhp " << ep.get_bhp() << " t " << t << std::endl;
	return lflags | uflags;
}

Aircraft::Cruise::Curve::flags_t Aircraft::Cruise::calculate(massmap_t::const_iterator it, double& tas, double& ff, double& pa, double& isaoffs, CruiseEngineParams& ep) const
{
	static constexpr bool debug = false;
	if (std::isnan(isaoffs)) {
		isaoffs = pa = tas = ff = std::numeric_limits<double>::quiet_NaN();
		ep.set_bhp(std::numeric_limits<double>::quiet_NaN());
		if (debug)
			std::cerr << "Cruise::calculate: name " << ep.get_name() << " mass " << it->first << " isaoffs " << isaoffs
				  << " pa " << pa << " tas " << tas << " ff " << ff << " bhp " << ep.get_bhp() << " (1)" << std::endl;
		return Curve::flags_none;
	}
	isamap_t::const_iterator iu(it->second.lower_bound(isaoffs));
	if (iu == it->second.end()) {
		if (iu == it->second.begin()) {
			isaoffs = pa = tas = ff = std::numeric_limits<double>::quiet_NaN();
			ep.set_bhp(std::numeric_limits<double>::quiet_NaN());
			if (debug)
				std::cerr << "Cruise::calculate: name " << ep.get_name() << " mass " << it->first << " isaoffs " << isaoffs
					  << " pa " << pa << " tas " << tas << " ff " << ff << " bhp " << ep.get_bhp() << " (2)" << std::endl;
			return Curve::flags_none;
		}
		--iu;
		isaoffs = iu->first;
		iu->second.calculate(tas, ff, pa, ep);
		if (debug)
			std::cerr << "Cruise::calculate: name " << ep.get_name() << " mass " << it->first << " isaoffs " << isaoffs
				  << " pa " << pa << " tas " << tas << " ff " << ff << " bhp " << ep.get_bhp() << " (3)" << std::endl;
		return iu->second.get_flags();
	}
	isamap_t::const_iterator il(iu);
	if (iu == it->second.begin()) {
		++iu;
		if (iu == it->second.end()) {
			isaoffs = il->first;
			il->second.calculate(tas, ff, pa, ep);
			if (debug)
				std::cerr << "Cruise::calculate: name " << ep.get_name() << " mass " << it->first << " isaoffs " << isaoffs
					  << " pa " << pa << " tas " << tas << " ff " << ff << " bhp " << ep.get_bhp() << " (4)" << std::endl;
			return il->second.get_flags();
		}
	} else {
		--il;
	}
	double t((isaoffs - il->first) / (iu->first - il->first));
	if (t <= 0) {
		il->second.calculate(tas, ff, pa, ep);
		if (debug)
			std::cerr << "Cruise::calculate: name " << ep.get_name() << " mass " << it->first << " isaoffs " << isaoffs
				  << " pa " << pa << " tas " << tas << " ff " << ff << " bhp " << ep.get_bhp() << " t " << t << std::endl;
		return il->second.get_flags();
	}
	if (t >= 1) {
		iu->second.calculate(tas, ff, pa, ep);
		if (debug)
			std::cerr << "Cruise::calculate: name " << ep.get_name() << " mass " << it->first << " isaoffs " << isaoffs
				  << " pa " << pa << " tas " << tas << " ff " << ff << " bhp " << ep.get_bhp() << " t " << t << std::endl;
		return iu->second.get_flags();
	}
	CruiseEngineParams lep(ep);
	double lpa(pa), ltas(tas), lff(ff);
	il->second.calculate(ltas, lff, lpa, lep);
	CruiseEngineParams uep(ep);
	double upa(pa), utas(tas), uff(ff);
	iu->second.calculate(utas, uff, upa, uep);
	pa = upa * t + lpa * (1 - t);
	ep.set_bhp(uep.get_bhp() * t + lep.get_bhp() * (1 - t));
	ep.set_rpm(uep.get_rpm() * t + lep.get_rpm() * (1 - t));
	tas = utas * t + ltas * (1 - t);
	ff = uff * t + lff * (1 - t);
	if (debug)
		std::cerr << "Cruise::calculate: name " << ep.get_name() << " mass " << it->first << " isaoffs " << isaoffs
			  << " pa " << pa << " tas " << tas << " ff " << ff << " bhp " << ep.get_bhp() << " t " << t << std::endl;
	return il->second.get_flags() | iu->second.get_flags();
}

std::pair<double,double> Aircraft::Cruise::get_bhp_range(curves_t::const_iterator it) const
{
	std::pair<double,double> r(std::numeric_limits<double>::max(), -std::numeric_limits<double>::max());
	for (massmap_t::const_iterator i(it->second.first.begin()), e(it->second.first.end()); i != e; ++i) {
		std::pair<double,double> r1(get_bhp_range(i));
		if (std::isnan(r1.first) || std::isnan(r1.second))
			return std::pair<double,double>(std::numeric_limits<double>::quiet_NaN(),
							std::numeric_limits<double>::quiet_NaN());
		r.first = std::min(r.first, r1.first);
		r.second = std::max(r.second, r1.second);
	}
	if (r.first > r.second)
		return std::pair<double,double>(std::numeric_limits<double>::quiet_NaN(),
						std::numeric_limits<double>::quiet_NaN());
	return r;
}

std::pair<double,double> Aircraft::Cruise::get_bhp_range(massmap_t::const_iterator it) const
{
	std::pair<double,double> r(std::numeric_limits<double>::max(), -std::numeric_limits<double>::max());
	for (isamap_t::const_iterator i(it->second.begin()), e(it->second.end()); i != e; ++i) {
		std::pair<double,double> r1(i->second.get_bhp_range());
		if (std::isnan(r1.first) || std::isnan(r1.second))
			return std::pair<double,double>(std::numeric_limits<double>::quiet_NaN(),
							std::numeric_limits<double>::quiet_NaN());
		r.first = std::min(r.first, r1.first);
		r.second = std::max(r.second, r1.second);
	}
	if (r.first > r.second)
		return std::pair<double,double>(std::numeric_limits<double>::quiet_NaN(),
						std::numeric_limits<double>::quiet_NaN());
	return r;
}

void Aircraft::Cruise::calculate(propulsion_t prop, double& tas, double& fuelflow, double& pa, double& mass, double& isaoffs, CruiseEngineParams& ep) const
{
	static constexpr bool debug = false;
	if (debug)
		ep.print(std::cerr << "Cruise::calculate: prop " << prop << " pa " << pa << " mass " << mass << " isaoffs " << isaoffs << " ep ") << std::endl;
	if (std::isnan(pa) || std::isnan(mass) || std::isnan(isaoffs)) {
		tas = fuelflow = pa = mass = isaoffs = std::numeric_limits<double>::quiet_NaN();
		return;
	}
	{
		double bhp(ep.get_bhp()), rpm(ep.get_rpm()), mp(ep.get_mp());
		if (std::isnan(bhp) && !std::isnan(rpm))
			m_pistonpower.calculate(pa, isaoffs, bhp, rpm, mp);
		if (!std::isnan(bhp) && !m_bhpmap.empty()) {
			calculate(mass, isaoffs, pa, bhp, tas, fuelflow);
			double pa1(pa), isaoffs1(isaoffs), bhp1(bhp);
			m_pistonpower.calculate(pa1, isaoffs1, bhp1, rpm, mp);
			ep.set_bhp(bhp);
			ep.set_rpm(rpm);
			ep.set_mp(mp);
			return;
		}
	}
	double pax(pa);
	double tas1(std::numeric_limits<double>::quiet_NaN());
	double fuelflow1(std::numeric_limits<double>::quiet_NaN());
	double pa1(std::numeric_limits<double>::quiet_NaN());
	double mass1(std::numeric_limits<double>::quiet_NaN());
	double isaoffs1(std::numeric_limits<double>::quiet_NaN());
	CruiseEngineParams ep1(ep);
	ep1.set_bhp(std::numeric_limits<double>::quiet_NaN());
	ep1.set_rpm(std::numeric_limits<double>::quiet_NaN());
	ep1.set_mp(std::numeric_limits<double>::quiet_NaN());
	if (ep.get_flags() & Curve::flags_hold) {
		bool ishold(false);
		for (curves_t::const_iterator ci(m_curves.begin()), ce(m_curves.end()); ci != ce; ++ci) {
			double tas2, fuelflow2, pa2(pax), mass2(mass), isaoffs2(isaoffs);
			CruiseEngineParams ep2(ep);
			Curve::flags_t flags(calculate(ci, tas2, fuelflow2, pa2, mass2, isaoffs2, ep2));
			if (std::isnan(pa2))
				continue;
			if (false)
				ep2.print(std::cerr << "holdcalc: " << ci->first << ' ') << " mass " << mass2
						    << " isaoffs " << isaoffs2 << " pa " << pa2
						    << " tas " << tas2 << " ff " << fuelflow2 << std::endl;
			if (((flags & Curve::flags_hold) && !ishold) ||
			    std::isnan(pa1) || (fabs(pa1 - pax) > 1 && fabs(pa2 - pax) < fabs(pa1 - pax)) ||
			    (fabs(pa2 - pax) <= 1 && fuelflow2 < fuelflow1)) {
				tas1 = tas2;
				fuelflow1 = fuelflow2;
				pa1 = pa2;
				mass1 = mass2;
				isaoffs1 = isaoffs2;
				ep1 = ep2;
				ishold = !!(flags & Curve::flags_hold);
			}
		}
		tas = tas1;
		fuelflow = fuelflow1;
		pa = pa1;
		mass = mass1;
		double isaoffspp(isaoffs);
		isaoffs = isaoffs1;
		{
			double bhp1(ep1.get_bhp()), rpm1(ep1.get_rpm()), mp1(ep1.get_mp());
			if (std::isnan(rpm1))
				rpm1 = ep.get_rpm();
			if (std::isnan(mp1))
				mp1 = ep.get_mp();
			m_pistonpower.calculate(pa1, isaoffspp, bhp1, rpm1, mp1);
			ep1.set_rpm(rpm1);
			ep1.set_mp(mp1);
		}
		ep = ep1;
		return;
	}
	double cbhp(std::numeric_limits<double>::quiet_NaN());
	{
		double tas2, fuelflow2, pa2, mass2, isaoffs2;
		CruiseEngineParams ep2;
		curves_t::const_iterator ci(m_curves.begin()), ce(m_curves.end());
		for (; ci != ce; ++ci) {
			if (ci->first != ep1.get_name())
				continue;
			pa2 = pax;
			mass2 = mass;
			isaoffs2 = isaoffs;
			ep2 = ep;
			if (calculate(ci, tas2, fuelflow2, pa2, mass2, isaoffs2, ep2) & Curve::flags_hold)
				continue;
			break;
		}
		if (ci != ce) {
			if (debug)
				ep2.print(std::cerr << "Cruise::calculate: named calc: pa2 " << pa2 << " tas2 " << tas2
					  << " fuelflow2 " << fuelflow2 << " mass2 " << mass2 << " isaoffs2 " << isaoffs2 << " ep ") << std::endl;
			if (!std::isnan(pa2) && (fabs(pa2 - pax) <= 50 || (false && pa2 < 2050 && pax < 2050))) {
				tas = tas2;
				fuelflow = fuelflow2;
				pa = pa2;
				mass = mass2;
				double isaoffspp(isaoffs);
				isaoffs = isaoffs2;
				{
					double bhp1(ep2.get_bhp()), rpm1(ep2.get_rpm()), mp1(ep2.get_mp());
					if (std::isnan(rpm1))
						rpm1 = ep.get_rpm();
					if (std::isnan(mp1))
						mp1 = ep.get_mp();
					m_pistonpower.calculate(pa2, isaoffspp, bhp1, rpm1, mp1);
					ep2.set_rpm(rpm1);
					ep2.set_mp(mp1);
				}
				ep = ep2;
				return;
			}
			std::pair<double,double> br(get_bhp_range(ci));
			if (!std::isnan(br.first) && br.first > 0 && br.second <= 1.01 * br.first)
				cbhp = 0.5 * (br.first + br.second);
		}
	}
	ep1.set_name("");
	for (curves_t::const_iterator ci(m_curves.begin()), ce(m_curves.end()); ci != ce; ++ci) {
		double tas2, fuelflow2, pa2(pax), mass2(mass), isaoffs2(isaoffs);
		CruiseEngineParams ep2(ep);
		if (calculate(ci, tas2, fuelflow2, pa2, mass2, isaoffs2, ep2) & Curve::flags_hold)
			continue;
		if (std::isnan(pa2))
			continue;
		if (std::isnan(pa1) ||
		    (ep1.get_name() != ep.get_name() && ep2.get_name() == ep.get_name() && fabs(pa2 - pa1) <= 50) ||
		    (fabs(pa1 - pax) > 50 && fabs(pa2 - pax) < fabs(pa1 - pax)) ||
		    (!std::isnan(cbhp) && fabs(pa2 - pax) <= 50 && fabs(ep1.get_bhp() - cbhp) > cbhp * 0.01 &&
		     fabs(ep2.get_bhp() - cbhp) < fabs(ep1.get_bhp() - cbhp))) {
			tas1 = tas2;
			fuelflow1 = fuelflow2;
			pa1 = pa2;
			mass1 = mass2;
			isaoffs1 = isaoffs2;
			ep1 = ep2;
		}
	}
	tas = tas1;
	fuelflow = fuelflow1;
	pa = pa1;
	mass = mass1;
	double isaoffspp(isaoffs);
	isaoffs = isaoffs1;
	{
		double bhp1(ep1.get_bhp()), rpm1(ep1.get_rpm()), mp1(ep1.get_mp());
		if (std::isnan(rpm1))
			rpm1 = ep.get_rpm();
		if (std::isnan(mp1))
			mp1 = ep.get_mp();
		m_pistonpower.calculate(pa1, isaoffspp, bhp1, rpm1, mp1);
		ep1.set_rpm(rpm1);
		ep1.set_mp(mp1);
	}
	ep = ep1;
}

void Aircraft::Cruise::calculate_maxtas(propulsion_t prop, double& tas, double& fuelflow, double& pa, double& mass, double& isaoffs, CruiseEngineParams& ep) const
{
	static constexpr bool debug = false;
	if (debug)
		ep.print(std::cerr << "Cruise::calculate_maxtas: prop " << prop << " pa " << pa << " mass " << mass << " isaoffs " << isaoffs << " ep ") << std::endl;
	if (std::isnan(pa) || std::isnan(mass) || std::isnan(isaoffs)) {
		tas = fuelflow = pa = mass = isaoffs = std::numeric_limits<double>::quiet_NaN();
		return;
	}
	double pax(pa);
	double tas1(std::numeric_limits<double>::quiet_NaN());
	double fuelflow1(std::numeric_limits<double>::quiet_NaN());
	double pa1(std::numeric_limits<double>::quiet_NaN());
	double mass1(std::numeric_limits<double>::quiet_NaN());
	double isaoffs1(std::numeric_limits<double>::quiet_NaN());
	CruiseEngineParams ep1(ep);
	for (curves_t::const_iterator ci(m_curves.begin()), ce(m_curves.end()); ci != ce; ++ci) {
		double tas2, fuelflow2, pa2(pax), mass2(mass), isaoffs2(isaoffs);
		CruiseEngineParams ep2(ep);
		calculate(ci, tas2, fuelflow2, pa2, mass2, isaoffs2, ep2);
		if (std::isnan(pa2) || std::isnan(tas2) || tas2 > tas)
			continue;
		if (std::isnan(pa1) ||
		    (fabs(pa1 - pax) > 50 && fabs(pa2 - pax) < fabs(pa1 - pax)) ||
		    (fabs(tas - tas2) < fabs(tas - tas1))) {
			tas1 = tas2;
			fuelflow1 = fuelflow2;
			pa1 = pa2;
			mass1 = mass2;
			isaoffs1 = isaoffs2;
			ep1 = ep2;
		}
	}
	tas = tas1;
	fuelflow = fuelflow1;
	pa = pa1;
	mass = mass1;
	double isaoffspp(isaoffs);
	isaoffs = isaoffs1;
	{
		double bhp1(ep1.get_bhp()), rpm1(ep1.get_rpm()), mp1(ep1.get_mp());
		if (std::isnan(rpm1))
			rpm1 = ep.get_rpm();
		if (std::isnan(mp1))
			mp1 = ep.get_mp();
		m_pistonpower.calculate(pa1, isaoffspp, bhp1, rpm1, mp1);
		ep1.set_rpm(rpm1);
		ep1.set_mp(mp1);
	}
	ep = ep1;
}

void Aircraft::Cruise::set_mass(double mass)
{
	if (std::isnan(mass) || mass <= 0)
		return;
	for (curves_t::iterator ci(m_curves.begin()), ce(m_curves.end()); ci != ce; ++ci) {
		massmap_t::iterator mi(ci->second.first.find(0));
		if (mi == ci->second.first.end())
			continue;
		for (isamap_t::const_iterator ii(mi->second.begin()), ie(mi->second.end()); ii != ie; ++ii) {
			Curve c(ii->second);
			c.set_mass(mass);
			add_curve(c);
		}
		ci->second.first.erase(mi);
	}
}

Aircraft::CheckErrors Aircraft::Cruise::check(double minmass, double maxmass) const
{
	CheckErrors r;
	typedef std::map<double,double> cmassmap_t;
	typedef std::map<double,cmassmap_t> cisamap_t;
	cisamap_t ceilingsg;
	bool hascurve(false);
	typedef std::set<double> altset_t;
	altset_t altset;
	altset_t altset250;
	for (curves_t::const_iterator ci(m_curves.begin()), ce(m_curves.end()); ci != ce; ++ci) {
		bool hold(false);
		cisamap_t ceilings;
		for (massmap_t::const_iterator mi(ci->second.first.begin()), me(ci->second.first.end()); mi != me; ++mi)
			for (isamap_t::const_iterator ii(mi->second.begin()), ie(mi->second.end()); ii != ie; ++ii) {
				for (Curve::const_iterator pi(ii->second.begin()), pe(ii->second.end()); pi != pe; ++pi) {
					if (std::isnan(pi->get_pressurealt()) || pi->get_pressurealt() < 0 || std::isnan(pi->get_tas()) || pi->get_tas() < 0)
						continue;
					altset.insert(pi->get_pressurealt());
					double taslimit(250);
					{
						AirData<double> ad;
						ad.set_pressure_altitude(pi->get_pressurealt());
						ad.set_cas(taslimit);
						taslimit = ad.get_tas();
						if (std::isnan(taslimit) || taslimit <= 0)
							taslimit = 250;
					}
					if (pi->get_tas() <= taslimit)
						altset250.insert(pi->get_pressurealt());
				}
				r.add(ii->second.check(minmass, maxmass));
				if (ii->second.get_flags() & Curve::flags_hold) {
					hold = true;
					continue;
				}
				if (ii->second.empty()) {
					r.add(CheckError::type_cruise, CheckError::severity_warning, ci->first, mi->first, ii->first)
						<< "curve has no defining points";
					continue;
				}
				std::pair<cmassmap_t::iterator, bool> ins(((ii->second.get_flags() & Curve::flags_interpolate) ? ceilingsg : ceilings)[ii->second.get_isaoffset()].
									 insert(cmassmap_t::value_type(ii->second.get_mass(), ii->second.rbegin()->get_pressurealt())));
				if (!ins.second)
					ins.first->second = std::max(ins.first->second, ii->second.rbegin()->get_pressurealt());
			}
		if (hold)
			continue;
		hascurve = hascurve || !ceilings.empty();
		for (cisamap_t::const_iterator ii(ceilings.begin()), ie(ceilings.end()); ii != ie; ++ii) {
			for (cmassmap_t::const_iterator mi(ii->second.begin()), me(ii->second.end()); mi != me; ) {
				if (mi->second < 7000)
					r.add(CheckError::type_cruise, CheckError::severity_warning, ci->first, mi->first, ii->first)
						<< "curve has unrealistically low ceiling";
				cmassmap_t::const_iterator mi2(mi);
				++mi;
				if (mi == me)
					break;
				if (mi2->second >= mi->second)
					continue;
				r.add(CheckError::type_cruise, CheckError::severity_warning, ci->first, mi2->first, ii->first)
					<< "curve has lower ceiling than higher mass " << mi->first;
			}
		}
	}
	hascurve = hascurve || !ceilingsg.empty();
	for (cisamap_t::const_iterator ii(ceilingsg.begin()), ie(ceilingsg.end()); ii != ie; ++ii) {
		for (cmassmap_t::const_iterator mi(ii->second.begin()), me(ii->second.end()); mi != me; ) {
			if (mi->second < 7000)
				r.add(CheckError::type_cruise, CheckError::severity_warning, "*interpolated*", mi->first, ii->first)
					<< "curve has unrealistically low ceiling";
			cmassmap_t::const_iterator mi2(mi);
			++mi;
			if (mi == me)
				break;
			if (mi2->second >= mi->second)
				continue;
			r.add(CheckError::type_cruise, CheckError::severity_warning, "*interpolated*", mi2->first, ii->first)
				<< "curve has lower ceiling than higher mass " << mi->first;
		}
	}
        if (altset.empty() || *altset.begin() >= 10000) {
		r.add(CheckError::type_cruise, CheckError::severity_error) << "no cruise points below FL100";
	} else {
		if (altset250.empty() || *altset250.begin() >= 10000)
			r.add(CheckError::type_cruise, CheckError::severity_error) << "no cruise points with IAS <= 250kts below FL100";
		altset_t as250uncov;
		std::set_difference(altset.begin(), altset.end(), altset250.begin(), altset250.end(), std::inserter(as250uncov, as250uncov.begin()));
		if (!as250uncov.empty() && *as250uncov.begin() <= 10000) {
			CheckError::MessageOStream os(r.add(CheckError::type_cruise, CheckError::severity_error));
			os << "Cruise altitude points" << std::fixed << std::setprecision(0);
			for (altset_t::const_iterator i(as250uncov.begin()), e(as250uncov.end()); i != e; ++i) {
				if (*i > 10000)
					break;
				os << ' ' << *i;
			}
			os << " have no corresponding curve point with IAS <= 250kts";
		}
	}
	{
		CheckErrors r2(m_optimalaltitude.check(minmass, maxmass));
		r.add(r2);
	}
	return r;
}
